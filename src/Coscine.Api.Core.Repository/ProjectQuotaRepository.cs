using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class ProjectQuotaRepository : IProjectQuotaRepository
{
    private readonly RepositoryBase<ProjectQuota> _projectQuota;

    public ProjectQuotaRepository(RepositoryContext repositoryContext)
    {
        _projectQuota = new(repositoryContext);
    }

    public async Task<PagedEnumerable<ProjectQuota>> GetPagedAsync(Guid projectId, ProjectQuotaParameters projectQuotaParameters, bool trackChanges)
    {
        var projectQuotaQuery = _projectQuota
            .FindByCondition(x => x.ProjectId == projectId, trackChanges)
            .Include(x => x.ResourceType)
            .Include(x => x.Project).ThenInclude(x => x.ProjectResources);

        var projectQuotas = await projectQuotaQuery
            .SortProjectQuotas(projectQuotaParameters.OrderBy)
            .Skip((projectQuotaParameters.PageNumber - 1) * projectQuotaParameters.PageSize)
            .Take(projectQuotaParameters.PageSize)
            .ToListAsync();

        var count = await projectQuotaQuery.CountAsync();

        return new PagedEnumerable<ProjectQuota>(projectQuotas, count, projectQuotaParameters.PageNumber, projectQuotaParameters.PageSize);
    }

    public async Task<IEnumerable<ProjectQuota>> GetAllAsync(Guid projectId, bool trackChanges)
    {
        return await _projectQuota
            .FindByCondition(x => x.ProjectId == projectId, trackChanges)
            .Include(x => x.ResourceType)
            .Include(x => x.Project).ThenInclude(x => x.ProjectResources)
            .ToListAsync();
    }

    public async Task<IEnumerable<ProjectQuota>> GetAllAsync(Guid projectId, IEnumerable<Guid> resourceTypeIds, ProjectQuotaParameters projectQuotaParameters, bool trackChanges)
    {
        return await _projectQuota
            .FindByCondition(
                x => x.ProjectId == projectId && resourceTypeIds.Contains(x.ResourceTypeId),
                trackChanges
            )
            .Include(x => x.ResourceType)
            .Include(x => x.Project).ThenInclude(x => x.ProjectResources)
            .SortProjectQuotas(projectQuotaParameters.OrderBy)
            .ToListAsync();
    }

    public async Task<ProjectQuota?> GetAsync(Guid projectId, Guid resourceTypeId, bool trackChanges)
    {
        return await _projectQuota
            .FindByCondition(x => x.ProjectId == projectId && x.ResourceTypeId == resourceTypeId, trackChanges)
            .Include(x => x.ResourceType)
            .Include(x => x.Project).ThenInclude(x => x.ProjectResources)
            .SingleOrDefaultAsync();
    }
}