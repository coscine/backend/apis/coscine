﻿using Coscine.Api.Core.Entities.Models.QuadStore;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.Extensions.Logging;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Repository;

public class RawDataCatalogRepository(IStore store, ILogger<RawDataCatalogRepository> logger) : IRawDataCatalogRepository
{
    private readonly IStore _store = store;
    private readonly ILogger<RawDataCatalogRepository> _logger = logger;

    public RawDataCatalog? Get(Uri rawDataCatalogId)
    {
        try
        {
            // Load model
            var model = new Model(_store, new UriRef(rawDataCatalogId.AbsoluteUri));

            // Load Resource
            return model?.GetResource<RawDataCatalog>(rawDataCatalogId);
        }
        catch (Exception e)
        {
            _logger.LogWarning(e, "Exception while loading from the quad store");
            return null;
        }
    }
}
