using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Repository.Extensions.Utility;
using Coscine.Api.Core.Repository.HelperModels;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.Helpers;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Data;
using VDS.RDF;
using VDS.RDF.Query;


namespace Coscine.Api.Core.Repository;

public sealed class ProjectRepository(RepositoryContext repositoryContext,
                                      IRdfRepositoryBase rdfRepositoryBase,
                                      IOptionsMonitor<PidConfiguration> pidConfigurationOptions) : IProjectRepository
{
    private readonly RepositoryBase<Project> _project = new(repositoryContext);

    public void Create(Project project)
    {
        _project.Create(project);
    }

    public void Create(Project project, Guid creatorId)
    {
        project.Creator = creatorId;
        _project.Create(project);
    }

    public void Delete(Project project)
    {
        project.Deleted = true;
    }

    public Uri GenerateGraphName(Project project)
    {
        return RdfUris.CoscineProjects.AppendToPath(project.Id.ToString());
    }

    public async Task DeleteGraphsAsync(Project project)
    {
        var projectGraphName = GenerateGraphName(project);

        // Delete project graph
        await rdfRepositoryBase.ClearGraphAsync(projectGraphName);

        var sparql = new SparqlParameterizedString
        {
            CommandText = @"
                DELETE WHERE {
                    GRAPH @graph {
                        @subject ?p ?o
                    }
                }"
        };

        sparql.SetUri("graph", RdfUris.TrellisGraph);
        sparql.SetUri("subject", projectGraphName);

        // Delete from trellis graph
        await rdfRepositoryBase.RunUpdateAsync(sparql);
    }

    public async Task UpdateGraphsAsync(Project project)
    {
        // Delete the graph
        await DeleteGraphsAsync(project);

        // Add the graph back
        var projectGraph = GenerateProjectGraph(project);
        await rdfRepositoryBase.AddGraphAsync(projectGraph);

        // Add the graph back
        var trellisGraph = GenerateTrellisGraph(project);
        await rdfRepositoryBase.AddGraphAsync(trellisGraph);
    }

    public async Task CreateGraphsAsync(Project project)
    {
        // Produce the graphs
        var projectGraph = GenerateProjectGraph(project);
        var trellisGraph = GenerateTrellisGraph(project);

        // Store the graphs        
        await rdfRepositoryBase.AddGraphAsync(projectGraph);
        await rdfRepositoryBase.AddGraphAsync(trellisGraph);
    }

    private Graph GenerateProjectGraph(Project project)
    {
        var coscineHandlePrefix = new Uri(RdfUris.HandlePrefix, pidConfigurationOptions.CurrentValue.Prefix);
        var projectGraphName = GenerateGraphName(project);

        var projectGraph = new Graph(projectGraphName)
        {
            BaseUri = projectGraphName
        };

        projectGraph.AssertToGraph(projectGraphName, RdfUris.A, RdfUris.DcatCatalogClass);
        projectGraph.AssertToGraph(projectGraphName, RdfUris.A, RdfUris.OrgOrganizationalCollaborationClass);
        projectGraph.AssertToGraph(projectGraphName, RdfUris.A, RdfUris.VcardGroupClass);
        projectGraph.AssertToGraph(projectGraphName, RdfUris.A, RdfUris.ProvEntityClass);
        projectGraph.AssertToGraph(projectGraphName, RdfUris.DcTermsTitle, project.ProjectName);
        projectGraph.AssertToGraph(projectGraphName, RdfUris.DcTermsDescription, project.Description);
        projectGraph.AssertToGraph(projectGraphName, RdfUris.DcTermsStartDate, project.StartDate.ToString(), RdfUris.XsdDateTime);
        projectGraph.AssertToGraph(projectGraphName, RdfUris.DcTermsEndDate, project.EndDate.ToString(), RdfUris.XsdDateTime);

        var projectKeywords = project.Keywords?.Split(';', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries) ?? [];

        foreach (var keyword in projectKeywords)
        {
            projectGraph.AssertToGraph(projectGraphName, RdfUris.DcTermsSubject, keyword);
        }

        if (project.DisplayName is not null)
        {
            projectGraph.AssertToGraph(projectGraphName, RdfUris.DcTermsAlternative, project.DisplayName);
        }

        if (project.PrincipleInvestigators is not null)
        {
            projectGraph.AssertToGraph(projectGraphName, RdfUris.DcTermsRightsHolder, project.PrincipleInvestigators);
        }

        if (project.GrantId is not null)
        {
            projectGraph.AssertToGraph(projectGraphName, RdfUris.SchemaFunding, project.GrantId);
        }

        if (project.Visibility?.DisplayName.Contains("Public", StringComparison.InvariantCultureIgnoreCase) == true)
        {
            projectGraph.AssertToGraph(projectGraphName, RdfUris.CoscineTermsProjectVisibility, RdfUris.CoscineTermsVisibilityPublic);
        }
        else
        {
            projectGraph.AssertToGraph(projectGraphName, RdfUris.CoscineTermsProjectVisibility, RdfUris.CoscineTermsVisibilityProjectMember);
        }

        projectGraph.AssertToGraph(projectGraphName, RdfUris.CoscineTermsProjectDeleted, project.Deleted.ToString().ToLower(), RdfUris.XsdBoolean);
        projectGraph.AssertToGraph(projectGraphName, RdfUris.CoscineTermsProjectSlug, project.Slug);
        projectGraph.AssertToGraph(projectGraphName, RdfUris.FoafHomepage, coscineHandlePrefix.AppendToPath(project.Id.ToString())); // Project PID

        foreach (var role in project.ProjectRoles)
        {
            var userGraphName = RdfUris.CoscineUsers.AppendToPath(role.UserId.ToString());
            projectGraph.AssertToGraph(projectGraphName, RdfUris.VcardHasMember, userGraphName);

            var blankNode = projectGraph.CreateBlankNode();

            projectGraph.AssertToGraph(blankNode, RdfUris.A, RdfUris.OrgMembershipClass);
            projectGraph.AssertToGraph(blankNode, RdfUris.OrgOrganization, projectGraphName);

            var roleGraphName = RdfUris.CoscineRoles.AppendToPath(role.RoleId.ToString());
            projectGraph.AssertToGraph(blankNode, RdfUris.OrgRole, roleGraphName);
            projectGraph.AssertToGraph(blankNode, RdfUris.OrgMember, userGraphName);
        }

        foreach (var projectResources in project.ProjectResources)
        {
            projectGraph.AssertToGraph(projectGraphName, RdfUris.DcatCatalog, RdfUris.CoscineResources.AppendToPath(projectResources.ProjectId.ToString()));
        }

        foreach (var organization in project.ProjectInstitutes)
        {
            var organizationUri = new Uri(organization.OrganizationUrl);
            projectGraph.AssertToGraph(projectGraphName, RdfUris.OrgOrganization, organizationUri);
            if (organization.Responsible)
            {
                // Use prov:wasAttributedTo to indicate primary responsibility:
                // the project Entity is attributed to (owned by) this organization.
                projectGraph.AssertToGraph(projectGraphName, RdfUris.ProvWasAttributedTo, organizationUri);
            }
            else
            {
                // Use prov:wasAssociatedWith to indicate additional involvement
                // or contribution from this organization (not primary responsibility).
                projectGraph.AssertToGraph(projectGraphName, RdfUris.ProvWasAssociatedWith, organizationUri);
            }
        }

        if (project.Creator is not null && project.Creator.HasValue)
        {
            var creatorGraphName = RdfUris.CoscineUsers.AppendToPath(project.Creator.Value.ToString());
            projectGraph.AssertToGraph(projectGraphName, RdfUris.DcTermsCreator, creatorGraphName);
        }

        if (project.DateCreated is not null && project.DateCreated.HasValue)
        {
            projectGraph.AssertToGraph(projectGraphName, RdfUris.DcTermsCreated, project.DateCreated.Value.ToString(), RdfUris.XsdDateTime);
        }

        return projectGraph;
    }

    private PatchGraph GenerateTrellisGraph(Project project)
    {
        var projectGraphName = GenerateGraphName(project);

        var trellisGraph = PatchGraph.Empty(RdfUris.TrellisGraph);

        // Add data to the trellisGraph
        trellisGraph.AssertToGraph(projectGraphName, RdfUris.A, RdfUris.LdpBasicContainerClass);
        MetadataUtility.AddModifiedDate(trellisGraph, projectGraphName.ToString());

        trellisGraph.AssertToGraph(projectGraphName, RdfUris.DcTermsIsPartOf, RdfUris.CoscineProjects);
        trellisGraph.AssertToGraph(RdfUris.CoscineProjects, RdfUris.A, RdfUris.LdpBasicContainerClass);
        MetadataUtility.AddModifiedDate(trellisGraph, RdfUris.CoscineProjects.ToString());

        trellisGraph.AssertToGraph(RdfUris.CoscineProjects.ToString(), RdfUris.DcTermsIsPartOf.ToString(), RdfUris.CoscinePrefix);
        trellisGraph.AssertToGraph(RdfUris.CoscinePrefix, RdfUris.A, RdfUris.LdpBasicContainerClass);
        MetadataUtility.AddModifiedDate(trellisGraph, RdfUris.CoscinePrefix.ToString());

        return trellisGraph;
    }


    public async Task<PagedEnumerable<Project>> GetPagedAsync(ProjectAdminParameters projectParameters, bool trackChanges)
    {
        var projectQuery = _project.FindAll(trackChanges)
            .Include(x => x.Visibility)
            .Include(x => x.ProjectDisciplines).ThenInclude(x => x.Discipline)
            .Include(x => x.ProjectInstitutes)
            .Include(x => x.ProjectResources.Where(pr => projectParameters.IncludeDeleted == true || !pr.Resource.Deleted))
            .Include(x => x.ProjectRoles).ThenInclude(x => x.User)
            .Include(x => x.SubProjectSubProjectNavigations)
            .AsSplitQuery()
            .Where(project => projectParameters.IncludeDeleted == true || !project.Deleted);

        var projects = await projectQuery
            .SortProjects(projectParameters.OrderBy)
            .Skip((projectParameters.PageNumber - 1) * projectParameters.PageSize)
            .Take(projectParameters.PageSize)
            .ToListAsync();

        var count = await projectQuery.CountAsync();

        return new PagedEnumerable<Project>(projects, count, projectParameters.PageNumber, projectParameters.PageSize);
    }

    public async Task<PagedEnumerable<Project>> GetPagedAsync(Guid userId, ProjectParameters projectParameters, bool trackChanges)
    {
        var projectQuery = _project.FindAll(trackChanges)
            .Include(x => x.Visibility)
            .Include(x => x.ProjectDisciplines).ThenInclude(x => x.Discipline)
            .Include(x => x.ProjectInstitutes)
            .Include(x => x.ProjectRoles).ThenInclude(x => x.User)
            .Include(x => x.SubProjectSubProjectNavigations)
            .AsSplitQuery()
            .Where(project => project.ProjectRoles.Any(x => x.UserId == userId) && !project.Deleted);

        var projects = await projectQuery
            .SortProjects(projectParameters.OrderBy)
            .Skip((projectParameters.PageNumber - 1) * projectParameters.PageSize)
            .Take(projectParameters.PageSize)
            .ToListAsync();

        var count = await projectQuery.CountAsync();

        return new PagedEnumerable<Project>(projects, count, projectParameters.PageNumber, projectParameters.PageSize);
    }

    public async Task<PagedEnumerable<Project>> GetPagedTopLevelProjectsAsync(ProjectAdminParameters projectParameters, bool trackChanges)
    {
        var topLevelProjectQuery = _project.FindAll(trackChanges)
            .Include(x => x.Visibility)
            .Include(x => x.ProjectDisciplines).ThenInclude(x => x.Discipline)
            .Include(x => x.ProjectInstitutes)
            .Include(x => x.ProjectResources.Where(pr => projectParameters.IncludeDeleted == true || !pr.Resource.Deleted))
            .Include(x => x.ProjectRoles).ThenInclude(x => x.User)
            .Include(x => x.SubProjectSubProjectNavigations)
            .AsSplitQuery()
            .Where(project =>
                (projectParameters.IncludeDeleted == true || !project.Deleted)  // Ensure that the project is not marked as deleted
                && (!project.SubProjectSubProjectNavigations.Any())
            );

        var projects = await topLevelProjectQuery
            .SortProjects(projectParameters.OrderBy)
            .Skip((projectParameters.PageNumber - 1) * projectParameters.PageSize)
            .Take(projectParameters.PageSize)
            .ToListAsync();

        var count = await topLevelProjectQuery.CountAsync();

        return new PagedEnumerable<Project>(projects, count, projectParameters.PageNumber, projectParameters.PageSize);
    }

    // For Search
    public async Task<IEnumerable<Guid>> GetAllProjectIdsAsync(Guid userId, bool trackChanges)
    {
        var projectIdsQuery = _project.FindAll(trackChanges)
            .Include(x => x.Visibility)
            .Include(x => x.ProjectDisciplines).ThenInclude(x => x.Discipline)
            .Include(x => x.ProjectInstitutes)
            .Include(x => x.ProjectRoles).ThenInclude(x => x.User)
            .Include(x => x.SubProjectSubProjectNavigations)
            .AsSplitQuery()
            .Where(project => project.ProjectRoles.Any(x => x.UserId == userId) && !project.Deleted);

        var projectIds = await projectIdsQuery
            .Select(x => x.Id).ToListAsync();

        return projectIds;
    }

    public async Task<PagedEnumerable<Project>> GetPagedTopLevelProjectsOfUserAsync(Guid userId, ProjectParameters projectParameters, bool trackChanges)
    {
        var topLevelProjectQuery = _project.FindAll(trackChanges)
            .Include(x => x.Visibility)
            .Include(x => x.ProjectDisciplines).ThenInclude(x => x.Discipline)
            .Include(x => x.ProjectInstitutes)
            .Include(x => x.ProjectRoles).ThenInclude(x => x.User)
            .Include(x => x.SubProjectSubProjectNavigations)
            .AsSplitQuery()
            .Where(project =>
                project.ProjectRoles.Any(pr => pr.UserId == userId)  // Check if the project has at least one project role associated with the specified user
                && !project.Deleted  // Ensure that the project is not marked as deleted
                && (!project.SubProjectSubProjectNavigations.Any()  // Check if the project has no subprojects
                    || project.SubProjectSubProjectNavigations.Any(subProject => !subProject.Project.ProjectRoles.Any(x => x.UserId == userId))));  // Check if any of the subprojects' parent projects have no project role associated with the user

        var projects = await topLevelProjectQuery
            .SortProjects(projectParameters.OrderBy)
            .Skip((projectParameters.PageNumber - 1) * projectParameters.PageSize)
            .Take(projectParameters.PageSize)
            .ToListAsync();

        var count = await topLevelProjectQuery.CountAsync();

        return new PagedEnumerable<Project>(projects, count, projectParameters.PageNumber, projectParameters.PageSize);
    }

    public async Task<IEnumerable<Project>> GetAllSubProjectsOfProjectAsync(Guid userId, Guid projectId, bool trackChanges)
    {
        return await _project.FindAll(trackChanges)
            .Include(x => x.Visibility)
            .Include(x => x.ProjectDisciplines).ThenInclude(x => x.Discipline)
            .Include(x => x.ProjectInstitutes)
            .Include(x => x.SubProjectSubProjectNavigations)
            .AsSplitQuery()
            .Where(project => project.ProjectRoles.Any(pr => pr.UserId == userId) && project.SubProjectSubProjectNavigations.Any(sub => sub.ProjectId == projectId) && !project.Deleted)
            .ToListAsync();
    }

    public async Task<Project?> GetAsync(Guid projectId, Guid userId, bool trackChanges)
    {
        return await _project.FindAll(trackChanges)
            .Include(x => x.Visibility)
            .Include(x => x.ProjectDisciplines).ThenInclude(x => x.Discipline)
            .Include(x => x.ProjectInstitutes)
            .Include(x => x.ProjectRoles).ThenInclude(x => x.User)
            .AsSplitQuery()
            .Where(project =>
                project.Id == projectId
                && project.ProjectRoles.Any(x => x.UserId == userId)
                && !project.Deleted)
            .SingleOrDefaultAsync();
    }

    public async Task<Project?> GetAsync(Guid projectId, bool trackChanges)
    {
        return await _project.FindAll(trackChanges)
            .Include(x => x.Visibility)
            .Include(x => x.ProjectDisciplines).ThenInclude(x => x.Discipline)
            .Include(x => x.ProjectInstitutes)
            .Include(x => x.ProjectRoles).ThenInclude(x => x.User)
            .Include(x => x.SubProjectSubProjectNavigations)
            .AsSplitQuery()
            .Where(project =>
                project.Id == projectId
                && !project.Deleted)
            .SingleOrDefaultAsync();
    }

    public async Task<Project?> GetProjectForPidRecordAsync(Guid projectId, bool trackChanges)
    {
        return await _project.FindAll(trackChanges)
            .Include(x => x.Visibility)
            .Include(x => x.ProjectRoles).ThenInclude(x => x.User)
            .Include(x => x.SubProjectSubProjectNavigations)
            .Include(x => x.ProjectDisciplines).ThenInclude(x => x.Discipline)
            .Include(x => x.ProjectInstitutes)
            .Where(project =>
                project.Id == projectId
                && !project.Deleted)
            .SingleOrDefaultAsync();
    }

    public async Task<Project?> GetAsync(string slug, bool trackChanges)
    {
        return await _project.FindAll(trackChanges)
            .Include(x => x.Visibility)
            .Include(x => x.ProjectDisciplines).ThenInclude(x => x.Discipline)
            .Include(x => x.ProjectInstitutes)
            .Include(x => x.ProjectRoles).ThenInclude(x => x.User)
            .AsSplitQuery()
            .Where(project =>
                project.Slug == slug
                && !project.Deleted)
            .SingleOrDefaultAsync();
    }

    public async Task<Project?> GetIncludingDeletedAsync(Guid projectId, bool trackChanges)
    {
        return await _project.FindByCondition(x => x.Id == projectId, trackChanges)
            .Include(x => x.ProjectRoles).ThenInclude(x => x.User)
            .AsSplitQuery()
            .SingleOrDefaultAsync();
    }
}