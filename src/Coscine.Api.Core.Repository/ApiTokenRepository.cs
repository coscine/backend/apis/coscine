﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace Coscine.Api.Core.Repository;

public sealed class ApiTokenRepository(RepositoryContext repositoryContext) : IApiTokenRepository
{
    private readonly RepositoryBase<ApiToken> _apiToken = new(repositoryContext);
    private readonly MemoryCacheEntryOptions _cacheEntryOptions = new MemoryCacheEntryOptions()
                        .SetSlidingExpiration(TimeSpan.FromMinutes(2))
                        .SetAbsoluteExpiration(TimeSpan.FromMinutes(10))
                        .SetPriority(CacheItemPriority.Normal)
                        .SetSize(1024);

    public async Task<PagedEnumerable<ApiToken>> GetPagedAsync(Guid userId, ApiTokenParameters apiTokenParameters, bool trackChanges)
    {
        var apiTokenQuery = _apiToken.FindByCondition(x => x.UserId == userId, trackChanges);

        var apiTokens = await apiTokenQuery
            .SortApiTokens(apiTokenParameters.OrderBy)
            .Skip((apiTokenParameters.PageNumber - 1) * apiTokenParameters.PageSize)
            .Take(apiTokenParameters.PageSize)
            .ToListAsync();

        var count = await apiTokenQuery.CountAsync();

        return new PagedEnumerable<ApiToken>(apiTokens, count, apiTokenParameters.PageNumber, apiTokenParameters.PageSize);
    }

    public async Task<ApiToken?> GetAsync(Guid apiTokenId, bool trackChanges)
    {
        var token = await _apiToken
        .FindByCondition(x => x.Id == apiTokenId, trackChanges)
        .SingleOrDefaultAsync();
        return token;
    }

    public async Task<ApiToken?> GetAsync(Guid apiTokenId, Guid userId, bool trackChanges)
    {
        var cacheKey = $"ApiToken_{apiTokenId}";

        var token = await _apiToken
        .FindByCondition(x => x.Id == apiTokenId && x.UserId == userId, trackChanges)
        .SingleOrDefaultAsync();

        return token;
    }

    public void Delete(ApiToken apiToken)
    {
        _apiToken.Delete(apiToken);
    }

    public void Create(ApiToken apiToken)
    {
        _apiToken.Create(apiToken);
    }

    public async Task MergeAsync(Guid fromUserId, Guid _)
    {
        foreach (var token in await _apiToken
            .FindByCondition(x => x.UserId == fromUserId, trackChanges: true)
            .ToListAsync())
        {
            // Any API Token would be invalid, as the corresponding user id has been deleted.
            Delete(token);
        }
    }
}