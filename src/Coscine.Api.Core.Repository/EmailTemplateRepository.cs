﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.EmbeddedResources;
using System.Text.Json;

namespace Coscine.Api.Core.Repository;

public sealed class EmailTemplateRepository : IEmailTemplateRepository
{
    public EmailTemplate Get(string language, string templateName)
    {
        var resource = EmbeddedResourceLoader.ReadResource($"EmailTemplates.{language}.{templateName}");

        var template = JsonSerializer.Deserialize<EmailTemplate>(resource) ?? throw new NullReferenceException("Could not deserialize the template.");

        template.Header = EmbeddedResourceLoader.ReadResource($"EmailTemplates.{language}.header.html.tpl");
        template.Footer = EmbeddedResourceLoader.ReadResource($"EmailTemplates.{language}.footer.html.tpl");

        return template;
    }
}