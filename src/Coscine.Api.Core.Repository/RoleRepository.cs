// Ignore Spelling: Admin

using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using VDS.RDF.Nodes;
using VDS.RDF.Query;

namespace Coscine.Api.Core.Repository;

public sealed class RoleRepository(ILogger<RoleRepository> logger, RepositoryContext repositoryContext, IRdfRepositoryBase rdfRepositoryBase, IHostEnvironment hostEnvironment) : IRoleRepository
{
    private readonly RepositoryBase<Role> _role = new(repositoryContext);
    private readonly IRdfRepositoryBase _rdfRepositoryBase = rdfRepositoryBase;
    private readonly IHostEnvironment _hostEnvironment = hostEnvironment;
    private readonly ILogger<RoleRepository> _logger = logger;

    public async Task<PagedEnumerable<Role>> GetPagedAsync(RoleParameters roleParameters, bool trackChanges)
    {
        _logger.LogDebug("Get all paged roles.");
        var roleQuery = _role.FindAll(trackChanges);

        var roles = await roleQuery
            .SortRoles(roleParameters.OrderBy)
            .Skip((roleParameters.PageNumber - 1) * roleParameters.PageSize)
            .Take(roleParameters.PageSize)
            .ToListAsync();

        var count = await roleQuery.CountAsync();

        return new PagedEnumerable<Role>(roles, count, roleParameters.PageNumber, roleParameters.PageSize);
    }

    public async Task<Role?> GetAsync(Guid roleId, bool trackChanges)
    {
        return await _role.FindByCondition(x => x.Id == roleId, trackChanges).SingleOrDefaultAsync();
    }

    public Role GetOwnerRole() => RoleConfiguration.Owner;

    public async Task<Role?> GetAsync(string role, bool trackChanges)
    {
        return await _role.FindByCondition(x => x.DisplayName == role, trackChanges).SingleOrDefaultAsync();
    }

    // For some reason the ask query did not work on live...
    public async Task<bool> IsUserQuotaAdmin(Guid userId)
    {
        if (_hostEnvironment.IsDevelopment() || _hostEnvironment.IsStaging() || _hostEnvironment.IsEnvironment("Nightly"))
        {
            return true;
        }
        
        _logger.LogDebug("Entering IsUserQuotaAdmin for user ID: {UserId}", userId);

        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT (COUNT(?membership) AS ?count)
                WHERE {
                    ?membership a org:Membership ;
                                org:member @userUri ;
                                org:role coscinerole:supportAdmin .
                }
            "
        };
        // Set namespaces, literals and URIs
        query.Namespaces.AddNamespace("org", RdfUris.OrgPrefix);
        query.Namespaces.AddNamespace("coscinerole", RdfUris.CoscineTermsRole);
        query.SetUri("userUri", new Uri($"{RdfUris.CoscineUserLegacy}{userId.ToString().ToUpper()}")); // User ID must be uppercase

        _logger.LogDebug("Preparing to execute SPARQL query in IsUserQuotaAdmin for user ID: {UserId}", userId);
        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        var count = resultSet.Select(e => e.Value("count").AsValuedNode().AsInteger()).SingleOrDefault();
        _logger.LogDebug("SPARQL query executed in IsUserQuotaAdmin for user ID: {UserId}. Result: {Result}", userId, count > 0);
        return count > 0;
    }
}