﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared;
using VDS.RDF.Nodes;
using VDS.RDF.Query;

namespace Coscine.Api.Core.Repository;

public sealed class DefaultProjectQuotaRepository : IDefaultProjectQuotaRepository
{
    private readonly IRdfRepositoryBase _rdfRepositoryBase;

    public DefaultProjectQuotaRepository(IRdfRepositoryBase rdfRepositoryBase)
    {
        _rdfRepositoryBase = rdfRepositoryBase;
    }

    public async Task<IEnumerable<DefaultProjectQuota>> GetAllAsync(Guid userId)
    {
        var userGraphName = $"{RdfUris.CoscineUsers}{userId}";

        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT ?resourceType MAX(?dQuota) as ?defaultQuota MAX(?dMaxQuota) as ?defaultMaxQuota
                WHERE
                {
                    ?bNode2 coscineresource:type ?resourceType .
                    ?bNode2 coscineresource:defaultQuota ?dQuota .
                    ?bNode2 coscineresource:defaultMaxQuota ?dMaxQuota .
                    ?organization coscineresource:typeSpecification ?bNode2 .
                    ?bNode1 org:organization ?organization .

                    GRAPH @userGraph
                    {
                        ?bNode1 org:member @userGraph .
                    }
                }
            "
        };

        query.Namespaces.AddNamespace("org", RdfUris.OrgPrefix);
        query.Namespaces.AddNamespace("coscineresource", RdfUris.CoscineTermsResource);
        query.SetUri("userGraph", new Uri(userGraphName));

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        return resultSet.Select(result =>
        {
            var resourceType = result.Value("resourceType").ToString();
            resourceType = resourceType[(resourceType.LastIndexOf("#") + 1)..];

            var dqp = int.TryParse(result.Value("defaultQuota").AsValuedNode().AsString(), out var defaultQuota);
            var dqmp = int.TryParse(result.Value("defaultMaxQuota").AsValuedNode().AsString(), out var defaultMaxQuota);

            return new DefaultProjectQuota
            {
                ResourceType = resourceType,
                DefaultQuota = dqp ? defaultQuota : 0,
                DefaultMaxQuota = dqmp ? defaultMaxQuota : 0,
            };
        });
    }
}