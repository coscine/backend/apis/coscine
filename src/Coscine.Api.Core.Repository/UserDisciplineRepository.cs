﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class UserDisciplineRepository(RepositoryContext repositoryContext) : IUserDisciplineRepository
{
    private readonly RepositoryBase<UserDiscipline> _userDiscipline = new(repositoryContext);

    public void Delete(UserDiscipline userDiscipline)
    {
        _userDiscipline.Delete(userDiscipline);
    }

    public async Task MergeAsync(Guid fromUserId, Guid toUserId)
    {
        foreach (var userDiscipline in await _userDiscipline
                .FindByCondition(x => x.UserId == fromUserId, trackChanges: true)
                .ToListAsync())
        {
            // Make sure, that the disciplines are not duplicated
            if (!await _userDiscipline.FindByCondition(x => x.UserId == toUserId && x.DisciplineId == userDiscipline.DisciplineId, trackChanges: false)
                    .AnyAsync())
            {
                // Not already part of the user
                userDiscipline.UserId = toUserId;
            }
            else
            {
                // Discipline is already part of the user, delete old reference
                Delete(userDiscipline);
            }
        }
    }
}