﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class TitleRepository : ITitleRepository
{
    private readonly RepositoryBase<Title> _title;

    public TitleRepository(RepositoryContext repositoryContext)
    {
        _title = new(repositoryContext);
    }

    public async Task<IEnumerable<Title>> GetAsync(TitleParameters titleParameters, bool trackChanges)
    {
        return await _title.FindAll(trackChanges)
            .SortTitles(titleParameters.OrderBy)
            .ToListAsync();
    }

    public async Task<Title?> GetAsync(Guid titleId, bool trackChanges)
    {
        return await _title.FindByCondition(t => titleId == t.Id, trackChanges)
            .SingleOrDefaultAsync();
    }
}