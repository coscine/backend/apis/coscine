using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class ProjectRoleRepository(RepositoryContext repositoryContext) : IProjectRoleRepository
{
    private readonly RepositoryBase<ProjectRole> _projectRole = new(repositoryContext);

    public async Task<PagedEnumerable<ProjectRole>> GetPagedAsync(Guid projectId, ProjectRoleParameters projectRoleParameters, bool trackChanges)
    {
        var projectRoleQuery = _projectRole.FindByCondition(x => x.ProjectId == projectId, trackChanges);

        var projectRoles = await projectRoleQuery
            .Include(x => x.User).ThenInclude(u => u.Title) // NOTE: Load navigation property to avoid lazy loading exceptions in mapper
            .Include(x => x.Role)
            .AsSplitQuery()
            .SortProjectRoles(projectRoleParameters.OrderBy)
            .Skip((projectRoleParameters.PageNumber - 1) * projectRoleParameters.PageSize)
            .Take(projectRoleParameters.PageSize)
            .ToListAsync();

        var count = await projectRoleQuery.CountAsync();

        return new PagedEnumerable<ProjectRole>(projectRoles, count, projectRoleParameters.PageNumber, projectRoleParameters.PageSize);
    }

    public async Task<ProjectRole?> GetAsync(Guid projectId, Guid projectRoleId, bool trackChanges)
    {
        return await _projectRole
            .FindByCondition(x => x.RelationId == projectRoleId
                && x.ProjectId == projectId, trackChanges)
            .Include(x => x.User).ThenInclude(u => u.Title) // NOTE: Load navigation property to avoid lazy loading exceptions in mapper
            .Include(x => x.Role)
            .AsSplitQuery()
            .SingleOrDefaultAsync();
    }

    public void Delete(ProjectRole projectRole)
    {
        _projectRole.Delete(projectRole);
    }

    public void Create(ProjectRole projectRole)
    {
        _projectRole.Create(projectRole);
    }

    public async Task<IEnumerable<ProjectRole>> GetAllByResourceAsync(Guid resourceId, Guid userId, bool trackChanges)
    {
        return await _projectRole
            .FindByCondition(projectRole => projectRole.Project.ProjectResources.Any(projectResource => projectResource.ResourceId == resourceId)
                && projectRole.UserId == userId, trackChanges)
            .Include(x => x.Project).ThenInclude(x => x.ProjectResources)
            .AsSplitQuery()
            .ToListAsync();
    }

    public async Task<IEnumerable<ProjectRole>> GetAllByProjectAsync(Guid projectId, Guid userId, bool trackChanges)
    {
        return await _projectRole
            .FindByCondition(x => x.ProjectId == projectId && x.UserId == userId, trackChanges)
            .ToListAsync();
    }

    public async Task<IEnumerable<ProjectRole>> GetAllByRoleAsync(Guid projectId, Guid roleId, bool trackChanges)
    {
        return await _projectRole
            .FindByCondition(x => x.ProjectId == projectId && x.RoleId == roleId, trackChanges)
            .ToListAsync();
    }

    public async Task MergeAsync(Guid fromUserId, Guid toUserId)
    {
        foreach (var projectRole in await _projectRole
            .FindByCondition(x => x.UserId == fromUserId, trackChanges: true)
            .ToListAsync())
        {
            // Make sure, that the users are not duplicated
            if (!await _projectRole.FindByCondition(x => x.UserId == toUserId && x.ProjectId == projectRole.ProjectId, trackChanges: false)
                    .AnyAsync())
            {
                // Not already part of the project
                projectRole.UserId = toUserId;
            }
            else
            {
                // User is already part of the project, delete old reference
                Delete(projectRole);
            }
        }
    }
}