﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class ProjectInstituteRepository : IProjectInstituteRepository
{
    private readonly RepositoryBase<ProjectInstitute> _projectInstitute;

    public ProjectInstituteRepository(RepositoryContext repositoryContext)
    {
        _projectInstitute = new(repositoryContext);
    }

    public async Task<ProjectInstitute?> GetAsync(Guid projectId, string url, bool trackChanges)
    {
        return await _projectInstitute.FindByCondition(x => x.OrganizationUrl == url && x.ProjectId == projectId, trackChanges).SingleOrDefaultAsync();
    }

    public void Delete(ProjectInstitute projectInstitute)
    {
        _projectInstitute.Delete(projectInstitute);
    }
}