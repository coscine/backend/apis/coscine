﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.Enums;
using Microsoft.Extensions.Logging;
using VDS.RDF.Nodes;
using VDS.RDF.Query;

namespace Coscine.Api.Core.Repository;

public class PublicationAdvisoryServiceRepository(IRdfRepositoryBase rdfRepositoryBase, ILogger<PublicationAdvisoryServiceRepository> logger) : IPublicationAdvisoryServiceRepository
{
    private readonly IRdfRepositoryBase _rdfRepositoryBase = rdfRepositoryBase;
    private readonly ILogger<PublicationAdvisoryServiceRepository> _logger = logger;

    /// <summary>
    /// Retrieves the data publication advisory service of an organization based on its ROR (Research Organization Registry) URL.
    /// </summary>
    /// <param name="organizationRorUri">The ROR URL of the organization.</param>
    /// <param name="language">The language chosen by the user.</param>
    /// <returns>The data publication advisory service of the organization, or <c>null</c> if not found.</returns>
    public virtual async Task<PublicationAdvisoryService?> GetPublicationAdvisoryServiceAsync(Uri organizationRorUri, AcceptedLanguage language)
    {
        var query = new SparqlParameterizedString
        {
            // TODO: Define a fallback langauge for the ?name property. See ApplicationProfileRepository for an example.
            CommandText = @$"
            SELECT ?name, ?mailbox, ?description
            WHERE {{
                GRAPH @rorUrl {{
                    @rorUrl coscineorganization:dataPublicationService ?service .
                        ?service foaf:name ?name . FILTER (LANG(?name) = @lang) .
                        ?service foaf:mbox ?mailbox .
                        ?service foaf:text ?description .
                }}
            }}
        "
        };
        query.Namespaces.AddNamespace("coscineorganization", RdfUris.CoscineTermsOrganizations);
        query.Namespaces.AddNamespace("foaf", RdfUris.FoafPrefix);
        query.SetLiteral("lang", language.ToString(), normalizeValue: false);

        query.SetUri("rorUrl", organizationRorUri);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        if (resultSet is null || resultSet.IsEmpty)
        {
            return null;
        }

        var displayName = resultSet.FirstOrDefault(e => e.Value("name") is not null)?.Value("name")?.AsValuedNode().AsString();
        var email = resultSet.FirstOrDefault(e => e.Value("mailbox") is not null)?.Value("mailbox")?.AsValuedNode().AsString().Replace("mailto:", "");

        if (displayName is null || email is null)
        {
            // If the name or email is missing, the advisory service is considered invalid, as they are required fields.
            // This is a data integrity issue, so we return null. The corresponding graph should be fixed.
            _logger.LogWarning("The data publication advisory service of the organization with ROR URL {RorUrl} is invalid, as it is missing the name or email. Fix the corresponding graph.", organizationRorUri);
            return null;
        }

        // Assuming there is only one advisory service per organization.
        var pubAdvService = new PublicationAdvisoryService
        {
            DisplayName = displayName,
            Email = email,
            Description = resultSet.FirstOrDefault(e => e.Value("description") is not null)?.Value("description")?.AsValuedNode().AsString(),
        };

        return pubAdvService;
    }
}