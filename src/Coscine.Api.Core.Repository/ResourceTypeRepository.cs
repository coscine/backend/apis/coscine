using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class ResourceTypeRepository(RepositoryContext repositoryContext) : IResourceTypeRepository
{
    private readonly RepositoryBase<ResourceType> _resourceType = new(repositoryContext);

    public async Task<ResourceType?> GetAsync(Guid resourceTypeId, bool trackChanges)
    {
        return await _resourceType.FindByCondition(x => x.Id == resourceTypeId, trackChanges).SingleOrDefaultAsync();
    }

    public async Task<IEnumerable<ResourceType>> GetAllAsync(bool trackChanges)
    {
        return await _resourceType.FindAll(trackChanges).ToListAsync();
    }

    public async Task<PagedEnumerable<ResourceType>> GetPagedAsync(RequestParameters requestParameters, bool trackChanges)
    {
        var resourceTypeQuery = _resourceType
            .FindAll(trackChanges);

        var resourceTypes = await resourceTypeQuery
            .Skip((requestParameters.PageNumber - 1) * requestParameters.PageSize)
            .Take(requestParameters.PageSize)
            .ToListAsync();

        var count = await resourceTypeQuery.CountAsync();

        return new PagedEnumerable<ResourceType>(resourceTypes, count, requestParameters.PageNumber, requestParameters.PageSize);
    }
}