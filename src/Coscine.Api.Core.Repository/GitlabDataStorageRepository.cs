using System.Text;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions.Utility;
using Coscine.Api.Core.Shared.Enums;
using GitLabApiClient;
using GitLabApiClient.Models.Commits.Requests.CreateCommitRequest;
using Microsoft.Extensions.Options;

namespace Coscine.Api.Core.Repository;

public class GitlabDataStorageRepository(IOptionsSnapshot<GitLabResourceTypeConfiguration> gitLabResourceTypeConfiguration,
                                         IGitlabResourceTypeRepository gitlabResourceTypeRepository,
                                         IHttpClientFactory httpClientFactory) : IDataStorageRepository
{
    private readonly IGitlabResourceTypeRepository _gitlabResourceTypeRepository = gitlabResourceTypeRepository;
    private readonly IHttpClientFactory _httpClientFactory = httpClientFactory;
    private readonly ResourceTypeConfiguration _resourceTypeConfiguration = gitLabResourceTypeConfiguration.Get("gitlab");

    public Guid TypeId => Configurations.ResourceTypeConfiguration.ResourceTypeGitlab.Id;

    public IEnumerable<Capability> Capabilities
    {
        get
        {
            yield return Capability.CanWrite;
            yield return Capability.CanRead;
            yield return Capability.CanUpdate;
            yield return Capability.CanDelete;
            yield return Capability.SupportsLocalMetadataCopy;
            yield return Capability.SupportsReadOnlyMode;
        }
    }

    public Task CreateAsync(Resource resource, CreateDataStorageParameters parameters)
    {
        CreateGitLabDataStorageOptions createGitLabDataStorageOptions = parameters.CreateGitLabDataStorageOptions ?? throw new NullReferenceException();

        var gitlabResourceType = new GitlabResourceType()
        {
            Id = Guid.NewGuid(), // Set the ID manually, object does not exist in the db until saved (even after the .Create())
            Branch = createGitLabDataStorageOptions.Branch,
            ProjectAccessToken = createGitLabDataStorageOptions.ProjectAccessToken,
            GitlabProjectId = createGitLabDataStorageOptions.GitlabProjectId,
            RepoUrl = createGitLabDataStorageOptions.RepoUrl,
            TosAccepted = createGitLabDataStorageOptions.TosAccepted
        };

        _gitlabResourceTypeRepository.Create(gitlabResourceType);

        resource.ResourceTypeOptionId = gitlabResourceType.Id;

        return Task.CompletedTask;
    }

    public async Task CreateAsync(CreateBlobParameters parameters)
    {
        string commitMessage = "Coscine Upload";

        byte[] bytes;

        using (var ms = new MemoryStream())
        {
            await parameters.Blob.CopyToAsync(ms);
            bytes = ms.ToArray();
        }

        var actionType = CreateCommitRequestActionType.Create;

        var actions = new List<CreateCommitRequestAction>()
         {
            new(actionType, parameters.Path)
            {
                Content = Convert.ToBase64String(bytes),
                Encoding = CreateCommitRequestActionEncoding.Base64
            }
         };

        var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var gitlabResourceType = await _gitlabResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new GitlabResourceTypeNotFoundException(resourceTypeOptionId);

        var client = new GitLabClient(new Uri(gitlabResourceType.RepoUrl).ToString(), gitlabResourceType.ProjectAccessToken);

        await client.Commits.CreateAsync(gitlabResourceType.GitlabProjectId, new CreateCommitRequest(gitlabResourceType.Branch, commitMessage, actions));
    }

    public Task CreateAsync(CreateTreeParameters parameters)
    {
        throw new NotImplementedException();
    }

    public async Task DeleteAsync(DeleteBlobParameters parameters)
    {
        if (parameters == null)
        {
            throw new ArgumentNullException(nameof(parameters), "Parameter object cannot be null.");
        }

        // Equivalent to deleting a file from Gitlab
        string commitMessage = "Coscine Delete";

        var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var gitlabResourceType = await _gitlabResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new GitlabResourceTypeNotFoundException(resourceTypeOptionId);

        var client = new GitLabClient(new Uri(gitlabResourceType.RepoUrl).ToString(), gitlabResourceType.ProjectAccessToken);

        // Equivalent to uploading a file in GitLab (committing)
        await client.Commits.CreateAsync(
            gitlabResourceType.GitlabProjectId,
            new CreateCommitRequest(
                gitlabResourceType.Branch,
                commitMessage,
                [
                    new(CreateCommitRequestActionType.Delete, parameters.Path),
                ])
        );
    }

    public Task DeleteAsync(DeleteTreeParameters parameters)
    {
        throw new NotImplementedException();
    }

    public Task<ResourceTypeInformation> GetResourceTypeInformationAsync()
    {
        var resourceTypeInformation = new ResourceTypeInformation
        {

            CanCreate = true,
            CanRead = true,
            CanSetResourceReadonly = false,
            CanUpdate = true,
            CanUpdateResource = false,
            CanDelete = true,
            CanDeleteResource = false,
            CanList = true,
            CanCreateLinks = false,
            CanCopyLocalMetadata = true,
            IsArchived = false,
            IsQuotaAvailable = false,
            IsQuotaAdjustable = false,
            ResourceCreate = new ResourceCreate
            {
                Components =
                [
                    [], // First step
                    [], // Second step
                    [], // Third step
                    []  // Fourth step
                ]
            },
            ResourceContent = new ResourceContent
            {
                ReadOnly = false,
                MetadataView = new MetadataView
                {
                    EditableDataUrl = false,
                    EditableKey = false
                },
                EntriesView = new EntriesView
                {
                    Columns = new ColumnsObject
                    {
                        Always = ["name", "size"]
                    }
                }
            },
            Status = _resourceTypeConfiguration?.SpecificType?.Status ?? ResourceTypeStatus.Hidden,
            GeneralType = _resourceTypeConfiguration?.SpecificType?.Type,
            SpecificType = _resourceTypeConfiguration?.SpecificType?.SpecificTypeName
        };

        return Task.FromResult(resourceTypeInformation);

    }

    public IEnumerable<SpecificType> GetSpecificResourceTypes()
    {
        return [_resourceTypeConfiguration.SpecificType];
    }

    /// <summary>
    /// Retrieves specific resource types with a particular status.
    /// </summary>
    /// <param name="status">The status of the resource types to retrieve.</param>
    /// <returns>An IEnumerable of specific resource types that match the provided status.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(ResourceTypeStatus status)
    {
        return GetSpecificResourceTypes().Where(st => st.Status == status);
    }

    /// <summary>
    /// Retrieves specific resource types that are supported by a particular organization.
    /// </summary>
    /// <param name="OrganizationRor">The ROR of the organization to match against the supported organizations of the resource types.</param>
    /// <returns>An IEnumerable of specific resource types that are supported by the organization specified by the provided ROR.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor)
    {
        return GetSpecificResourceTypes().Where(st => st.SupportedOrganizations is not null
            && (st.SupportedOrganizations.Contains(OrganizationRor) || st.SupportedOrganizations.Contains("*")));
    }

    /// <summary>
    /// Retrieves specific resource types that are supported by a particular organization and have a particular status.
    /// </summary>
    /// <param name="OrganizationRor">The ROR of the organization to match against the supported organizations of the resource types.</param>
    /// <param name="status">The status of the resource types to retrieve.</param>
    /// <returns>An IEnumerable of specific resource types that match the provided status and are supported by the organization specified by the provided ROR.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor, ResourceTypeStatus status)
    {
        return GetSpecificResourceTypes(OrganizationRor).Where(st => st.Status == status);
    }

    public async Task<DataStorageInfo> GetStorageInfoAsync(StorageInfoParameters parameters)
    {
        var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var gitlabResourceType = await _gitlabResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new GitlabResourceTypeNotFoundException(resourceTypeOptionId);

        var info = new DataStorageInfo
        {
            GitlabStorageInfo = new GitlabStorageInfo
            {
                AccessToken = gitlabResourceType.ProjectAccessToken,
                Branch = gitlabResourceType.Branch,
                ProjectId = gitlabResourceType.GitlabProjectId,
                RepoUrl = gitlabResourceType.RepoUrl,
            }
        };

        return info;
    }

    public async Task<IEnumerable<StorageItemInfo>> ListAsync(ListTreeParameters parameters)
    {
        // Equivalent to showing folder contents

        var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var gitlabResourceType = await _gitlabResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new GitlabResourceTypeNotFoundException(resourceTypeOptionId);

        var gitClient = new GitLabClient(new Uri(gitlabResourceType.RepoUrl).ToString(), gitlabResourceType.ProjectAccessToken);

        var tree = await gitClient.Trees.GetAsync(gitlabResourceType.GitlabProjectId, options =>
        {
            options.Reference = gitlabResourceType.Branch;
            options.Path = parameters.TreePath;
        });

        var entries = new List<StorageItemInfo>();

        var httpClient = _httpClientFactory.CreateGitLabHttpClient(gitlabResourceType);

        foreach (var entry in tree)
        {
            if (entry.Type.Equals("blob"))
            {
                ArgumentException.ThrowIfNullOrEmpty(gitlabResourceType.RepoUrl);

                // secureHostUrl is a new string with the scheme set to "https"
                var secureHostUrl = gitlabResourceType.RepoUrl.ToLower().Trim().Replace("http://", "https://").TrimEnd('/');

                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Head,
                    RequestUri = new Uri($"{secureHostUrl}/api/v4/projects/{gitlabResourceType.GitlabProjectId}/repository/files/{Uri.EscapeDataString(entry.Path)}?ref={gitlabResourceType.Branch}"),
                };

                using var response = await httpClient.SendAsync(request);
                response.EnsureSuccessStatusCode();

                var sizeInBytes = response.Headers.GetValues("X-Gitlab-Size").FirstOrDefault();
                var lastCommitId = response.Headers.GetValues("X-Gitlab-Last-Commit-Id").FirstOrDefault();
                var commit = await gitClient.Commits.GetAsync(gitlabResourceType.GitlabProjectId, lastCommitId);

                entries.Add(new StorageItemInfo
                {
                    Path = entry.Path,
                    IsBlob = true,
                    ContentLength = sizeInBytes is null ? 0 : long.Parse(sizeInBytes),
                    CreatedDate = commit.AuthoredDate,
                    LastModifiedDate = commit.CreatedAt,
                });
            }
            else if (entry.Type.Equals("tree"))
            {
                // Folder
                var absolutePath = entry.Path.Last().Equals("/") ? entry.Path : $"{entry.Path}/";

                entries.Add(new StorageItemInfo
                {
                    Path = absolutePath,
                    IsBlob = false,
                    ContentLength = 0,
                });
            }
        }

        return entries;
    }

    public async Task<Stream?> ReadAsync(ReadBlobParameters parameters)
    {
        // Equivalent to downloading a file; loading a file's contents

        var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var gitlabResourceType = await _gitlabResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new GitlabResourceTypeNotFoundException(resourceTypeOptionId);

        var gitClient = new GitLabClient(new Uri(gitlabResourceType.RepoUrl).ToString(), gitlabResourceType.ProjectAccessToken);

        var file = await gitClient.Files.GetAsync(gitlabResourceType.GitlabProjectId, parameters.Path, gitlabResourceType.Branch);

        if (file.Encoding.Equals("base64"))
        {
            return new MemoryStream(Convert.FromBase64String(file.Content));
        }
        else
        {
            return new MemoryStream(Encoding.ASCII.GetBytes(file.ContentDecoded));
        }
    }

    public async Task<StorageItemInfo?> ReadAsync(ReadTreeParameters parameters)
    {
        var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var gitlabResourceType = await _gitlabResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
            ?? throw new GitlabResourceTypeNotFoundException(resourceTypeOptionId);

        var gitClient = new GitLabClient(new Uri(gitlabResourceType.RepoUrl).ToString(), gitlabResourceType.ProjectAccessToken);

        try
        {
            var file = await gitClient.Files.GetAsync(gitlabResourceType.GitlabProjectId, parameters.Path, gitlabResourceType.Branch);
            var commit = await gitClient.Commits.GetAsync(gitlabResourceType.GitlabProjectId, file.LastCommitId);

            return new StorageItemInfo
            {
                Path = parameters.Path,
                IsBlob = true,
                ContentLength = file.Size,
                CreatedDate = commit.AuthoredDate,
                LastModifiedDate = commit.CreatedAt,
            };
        }
        catch (GitLabException)
        {
            return null;
        }
    }

    public Task UpdateAsync(Resource resource, UpdateDataStorageParameters parameters)
    {
        return Task.CompletedTask;
    }

    public async Task UpdateAsync(UpdateBlobParameters parameters)
    {
        string commitMessage = "Coscine Update";

        byte[] bytes;

        using (var ms = new MemoryStream())
        {
            await parameters.Blob.CopyToAsync(ms);
            bytes = ms.ToArray();
        }

        var actionType = CreateCommitRequestActionType.Update;

        var actions = new List<CreateCommitRequestAction>()
         {
            new(actionType, parameters.Path)
            {
                Content = Convert.ToBase64String(bytes),
                Encoding = CreateCommitRequestActionEncoding.Base64
            }
         };

        var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var gitlabResourceType = await _gitlabResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
            ?? throw new GitlabResourceTypeNotFoundException(resourceTypeOptionId);

        var gitClient = new GitLabClient(new Uri(gitlabResourceType.RepoUrl).ToString(), gitlabResourceType.ProjectAccessToken);

        await gitClient.Commits.CreateAsync(gitlabResourceType.GitlabProjectId, new CreateCommitRequest(gitlabResourceType.Branch, commitMessage, actions));
    }

    public Task UpdateAsync(UpdateTreeParameters parameters)
    {
        throw new NotImplementedException();
    }
}
