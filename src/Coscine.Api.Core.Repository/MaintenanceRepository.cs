using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.Extensions.Options;
using System.Globalization;
using System.ServiceModel.Syndication;
using System.Xml;

namespace Coscine.Api.Core.Repository;

[Obsolete($"This interface is obsolete and will be removed in the future. Use {nameof(NocRepository)} instead.")]
public sealed class MaintenanceRepository : IMaintenanceRepository
{
    private readonly MaintenanceConfiguration _maintenanceConfiguration;

    public MaintenanceRepository(IOptionsMonitor<MaintenanceConfiguration> maintenanceConfiguration)
    {
        _maintenanceConfiguration = maintenanceConfiguration.CurrentValue;
    }

    public async Task<List<Maintenance>> GetAllAsync(bool trackChanges)
    {
        var maintenances = new List<Maintenance>();

        // The maintenace request are prone to failure 
        // and are able to somehow mow down other requests to api, when failing.
        try {
        // We allow the usage of Task.Run here explicitly.
        // Another solution would still be preferable
        var rssfeed = await Task.Run(() => SyndicationFeed.Load(XmlReader.Create(_maintenanceConfiguration.RssUrl)));

        foreach (var syndicationItem in rssfeed.Items.OrderByDescending(x => x.PublishDate))
        {
            try
            {
                if (syndicationItem?.Summary != null && !string.IsNullOrWhiteSpace(syndicationItem.Summary.Text))
                {
                    var current = new Maintenance
                    {
                        DisplayName = syndicationItem.Title.Text,
                        Url = syndicationItem.Links[0].Uri
                    };

                    var summary = syndicationItem.Summary.Text;
                    var splitter = summary.IndexOf('-');
                    if (splitter != -1)
                    {
                        current.Body = summary.Substring(splitter + 1, summary.Length - splitter - 1).Trim();
                        var firstPart = summary[..splitter].Trim();
                        splitter = firstPart.IndexOf("von");
                        if (splitter != -1)
                        {
                            current.Type = firstPart[..splitter].Trim();
                            var timespan = firstPart.Substring(splitter + 3, firstPart.Length - splitter - 3).Trim();

                            splitter = timespan.IndexOf("bis");
                            if (splitter != -1)
                            {
                                var startsString = timespan[..splitter].Trim();
                                var endsString = timespan.Substring(splitter + 3, timespan.Length - splitter - 3).Trim();

                                var dayList = new List<string> { "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag" };

                                foreach (var dayString in dayList)
                                {
                                    startsString = startsString.Replace(dayString, "").Trim();
                                    startsString = startsString.Replace(dayString.ToLower(), "").Trim();
                                }

                                foreach (var dayString in dayList)
                                {
                                    endsString = endsString.Replace(dayString, "").Trim();
                                    endsString = endsString.Replace(dayString.ToLower(), "").Trim();
                                }

                                if (string.Equals(startsString, "unbekannt", StringComparison.OrdinalIgnoreCase))
                                {
                                    current.StartsDate = DateTime.MinValue;
                                }
                                else
                                {
                                    current.StartsDate = DateTime.ParseExact(startsString, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture);
                                }

                                if (string.Equals(endsString, "unbekannt", StringComparison.OrdinalIgnoreCase))
                                {
                                    current.EndsDate = DateTime.MaxValue;
                                }
                                else
                                {
                                    current.EndsDate = DateTime.ParseExact(endsString, "dd.MM.yyyy HH:mm", CultureInfo.InvariantCulture);
                                }

                                maintenances.Add(current);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                // ignore not correct parsed messages
            }
        }
        } catch(Exception){
            // Ignore any other problem
        }

        return maintenances;
    }

    public async Task<Maintenance?> GetCurrent(bool trackChanges)
    {
        Maintenance? current = null;

        var resultOrderPos = _maintenanceConfiguration.RelevanceList.Count + 1;
        var maintenances = await GetAllAsync(trackChanges);
        foreach (var maintenance in maintenances)
        {
            if (maintenance.EndsDate > DateTime.Now && maintenance.StartsDate < DateTime.Now)
            {
                // first listed valid message
                if (current == null)
                {
                    current = maintenance;
                    resultOrderPos = GetOrderPos(current.Type);
                }
                else
                {
                    // check if the current message is higher ranged as the previous result
                    var maintenanceOrderPos = GetOrderPos(maintenance.Type);
                    // higher rang of type
                    if (maintenanceOrderPos < resultOrderPos)
                    {
                        current = maintenance;
                        resultOrderPos = maintenanceOrderPos;
                        // same type but longer active
                    }
                    else if (maintenanceOrderPos < resultOrderPos && maintenance.EndsDate > current.EndsDate)
                    {
                        current = maintenance;
                        resultOrderPos = maintenanceOrderPos;
                    }
                }
            }
        }

        return current;
    }

    private int GetOrderPos(string type)
    {
        int res = _maintenanceConfiguration.RelevanceList.FindIndex(x => string.Equals(x, type, StringComparison.CurrentCultureIgnoreCase));

        if (res == -1)
        {
            res = _maintenanceConfiguration.RelevanceList.FindIndex(x =>
                string.Equals(x, _maintenanceConfiguration.UndefinedString, StringComparison.CurrentCultureIgnoreCase));
        }

        if (res == -1)
        {
            res = _maintenanceConfiguration.RelevanceList.Count + 1;
        }

        return res;
    }
}