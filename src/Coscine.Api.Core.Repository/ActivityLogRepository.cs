﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class ActivityLogRepository(RepositoryContext repositoryContext) : IActivityLogRepository
{
    private readonly RepositoryBase<ActivityLog> _activityLog = new(repositoryContext);

    public async Task<PagedEnumerable<ActivityLog>> GetPagedAsync(ActivityLogParameters activityLogParameters, bool trackChanges)
    {
        var activityLogQuery = _activityLog.FindAll(trackChanges)
            .SortActivityLogs(activityLogParameters.OrderBy)
            .ExcludeAfterTimestamp(activityLogParameters.ActivityTimestampBefore)
            .ExcludeBeforeTimestamp(activityLogParameters.ActivityTimestampAfter)
            .FilterByGuid(activityLogParameters.Guid)
            .FilterByUserId(activityLogParameters.UserId)
            .FilterByHttpMethod(activityLogParameters.HttpMethod);

        // As regular expressions are not supported by the database directly, we have to do the filtering in-memory
        var activityLogs = await activityLogQuery.ToListAsync();

        // Apply in-memory filtering
        var filteredActivityLogs = activityLogs
            .FilterByRegularExpression(activityLogParameters.RegularExpression)
            .ToList();

        // Manual pagination on the in-memory filtered list
        var pagedActivityLogs = filteredActivityLogs
            .Skip((activityLogParameters.PageNumber - 1) * activityLogParameters.PageSize)
            .Take(activityLogParameters.PageSize)
            .ToList();

        var count = filteredActivityLogs.Count;

        return new PagedEnumerable<ActivityLog>(pagedActivityLogs, count, activityLogParameters.PageNumber, activityLogParameters.PageSize);
    }

    public async Task<ActivityLog?> GetAsync(Guid activityLogId, bool trackChanges)
    {
        return await _activityLog
            .FindByCondition(x => x.Id == activityLogId, trackChanges)
            .SingleOrDefaultAsync();
    }

    public async Task<ActivityLog?> GetAsync(Guid activityLogId, Guid userId, bool trackChanges)
    {
        return await _activityLog
            .FindByCondition(x => x.Id == activityLogId && x.UserId == userId, trackChanges)
            .SingleOrDefaultAsync();
    }

    public void Delete(ActivityLog activityLog)
    {
        _activityLog.Delete(activityLog);
    }

    public void DeleteAllBeforeAsync(DateTime? timestamp)
    {
        var activityLogQuery = _activityLog.FindAll(trackChanges: true);

        var activityLogsToDelete = activityLogQuery
            .ExcludeAfterTimestamp(timestamp); // Exclude all activity logs after the timestamp

        _activityLog.DeleteRange(activityLogsToDelete);
    }

    public void Create(ActivityLog activityLog)
    {
        _activityLog.Create(activityLog);
    }

    public async Task MergeAsync(Guid fromUserId, Guid toUserId)
    {
        foreach (var al in await _activityLog
            .FindByCondition(x => x.UserId == fromUserId, trackChanges: true)
            .ToListAsync())
        {
            // Update log
            al.UserId = toUserId;
        }
    }
}