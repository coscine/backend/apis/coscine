using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Enums.Utility;
using Coscine.Api.Core.Shared.RequestFeatures;
using VDS.RDF;
using VDS.RDF.Nodes;
using VDS.RDF.Query;

namespace Coscine.Api.Core.Repository;

public sealed class ApplicationProfileRepository(IRdfRepositoryBase rdfRepositoryBase) : IApplicationProfileRepository
{
    private readonly IRdfRepositoryBase _rdfRepositoryBase = rdfRepositoryBase;

    public async Task<PagedEnumerable<ApplicationProfile>> GetPagedAsync(ApplicationProfileParameters applicationProfileParameters)
    {
        // TODO: Avoid string interpolation, look into a better way to do that!
        var moduleFilter = (applicationProfileParameters.Modules != true) ? "FILTER NOT EXISTS { ?apUri a coscinetype:Module }" : "";

        // TODO: Replace hardcoded 'https://purl.org/coscine/ap/' with 'RdfUris.CoscineApplicationProfile'
        var query = new SparqlParameterizedString
        {
            CommandText = @$"
                SELECT ?apUri (SAMPLE(?apName)) as ?name (SAMPLE(?apDesc)) as ?desc
                WHERE
                {{
                    ?apUri a sh:NodeShape .
                    {moduleFilter}

                    OPTIONAL {{ ?apUri (rdfs:label|dcterms:title|dc:title) ?n . FILTER (lang(?n) = @lang) }}
                    OPTIONAL {{ ?apUri (rdfs:label|dcterms:title|dc:title) ?n_fb . FILTER (lang(?n_fb) = @fallbackLang) }}
                    BIND(COALESCE(?n, ?n_fb) AS ?apName)

                    OPTIONAL {{ ?apUri (rdfs:comment|dcterms:description|dc:description) ?d . FILTER (lang(?d) = @lang) }}
                    OPTIONAL {{ ?apUri (rdfs:comment|dcterms:description|dc:description) ?d_fb . FILTER (lang(?d_fb) = @fallbackLang) }}
                    BIND(COALESCE(?d, ?d_fb) AS ?apDesc)

                    FILTER (REGEX (?apUri, ""https://purl.org/coscine/ap/"", ""i""))

                    FILTER (
                        REGEX(?apUri, @searchTerm, ""i"") ||
                        REGEX(?apName, @searchTerm, ""i"") ||
                        REGEX(?apDesc, @searchTerm, ""i"")
                    )
                }}
                GROUP BY ?apUri ?name ?desc
            "
        };

        // Set namespaces, literals and URIs
        query.Namespaces.AddNamespace("sh", RdfUris.ShaclPrefix);
        query.Namespaces.AddNamespace("rdfs", RdfUris.RdfsUrlPrefix);
        query.Namespaces.AddNamespace("dcterms", RdfUris.DcTermsPrefix);
        query.Namespaces.AddNamespace("dc", RdfUris.DcPrefix);
        query.Namespaces.AddNamespace("coscinetype", RdfUris.CoscineTermsTypes);
        query.SetLiteral("lang", applicationProfileParameters.Language.GetEnumMemberValue() ?? applicationProfileParameters.Language.ToString(), normalizeValue: false);
        var fallbackLang = applicationProfileParameters.Language == AcceptedLanguage.en ? AcceptedLanguage.de : AcceptedLanguage.en;
        query.SetLiteral("fallbackLang", fallbackLang.GetEnumMemberValue() ?? fallbackLang.ToString(), normalizeValue: false);
        query.SetLiteral("searchTerm", applicationProfileParameters.SearchTerm ?? string.Empty, normalizeValue: false);

        // Build and execute count query
        var count = await query.GetCountOrDefaultAsync(_rdfRepositoryBase);

        // Always sort before adding pagination, otherwise the query breaks!
        query.Sort<ApplicationProfile>(applicationProfileParameters.OrderBy,
            new()
            {
                {"apUri", ap => ap.Uri },
                {"name", ap => ap.DisplayName },
                {"desc", ap => ap.Description }
            }
        );
        query.Paginate(applicationProfileParameters.PageSize, applicationProfileParameters.PageNumber);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        // Extract the object
        var applicationProfiles = resultSet
            .Select(r => new ApplicationProfile
            {
                Uri = new Uri(r.Value("apUri").ToString()),
                DisplayName = r.Value("name")?.AsValuedNode().AsString(),
                Description = r.Value("desc")?.AsValuedNode().AsString(),
            });

        return new PagedEnumerable<ApplicationProfile>(applicationProfiles, count, applicationProfileParameters.PageNumber, applicationProfileParameters.PageSize);
    }

    public async Task<ApplicationProfile?> GetAsync(Uri applicationProfileUri, RdfFormat rdfFormat, AcceptedLanguage language)
    {
        var query = new SparqlParameterizedString
        {
            // TODO: Consider using CONSTRUCT for ?s ?p ?o and splitting the below query into two separate queries
            // TODO 2: Consolidate the (fallback-)language query with the rest of the queries found in the repositories
            CommandText = @"
                SELECT ?s ?p ?o (SAMPLE(?n)) AS ?name (SAMPLE(?d)) AS ?desc
                WHERE {
                    @graph rdf:type sh:NodeShape .

                    GRAPH @graph {
                        ?s ?p ?o
                    }

                    OPTIONAL { ?s (rdfs:label|dcterms:title|dc:title) ?n . FILTER (lang(?n) = @lang) }
                    OPTIONAL { ?s (rdfs:label|dcterms:title|dc:title) ?n_fb . FILTER (lang(?n_fb) = @fallbackLang) }
                    BIND(COALESCE(?n, ?n_fb) AS ?apName)

                    OPTIONAL { ?s (rdfs:comment|dcterms:description|dc:description) ?d . FILTER (lang(?d) = @lang) }
                    OPTIONAL { ?s (rdfs:comment|dcterms:description|dc:description) ?d_fb . FILTER (lang(?d_fb) = @fallbackLang) }
                    BIND(COALESCE(?d, ?d_fb) AS ?apDesc)
                }
                GROUP BY ?s ?p ?o
            "
        };

        // Set namespaces, literals and URIs
        query.Namespaces.AddNamespace("sh", RdfUris.ShaclPrefix);
        query.Namespaces.AddNamespace("rdf", RdfUris.RdfUrlPrefix);
        query.Namespaces.AddNamespace("rdfs", RdfUris.RdfsUrlPrefix);
        query.Namespaces.AddNamespace("dcterms", RdfUris.DcTermsPrefix);
        query.Namespaces.AddNamespace("dc", RdfUris.DcPrefix);
        query.SetLiteral("lang", language.GetEnumMemberValue(), normalizeValue: false);
        query.SetLiteral("fallbackLang", "en", normalizeValue: false);

        query.SetUri("graph", applicationProfileUri);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        if (resultSet.IsEmpty)
        {
            return null;
        }

        // Populate as graph to get definition
        var graph = new Graph
        {
            BaseUri = applicationProfileUri,
        };

        // Add most common namespaces for pretty print
        graph.NamespaceMap.AddNamespace("csmd", RdfUris.CsmdPrefix);
        graph.NamespaceMap.AddNamespace("dcterms", RdfUris.DcTermsPrefix);
        graph.NamespaceMap.AddNamespace("dc", RdfUris.DcPrefix);
        graph.NamespaceMap.AddNamespace("dcmitype", RdfUris.DcmiTypePrefix);
        graph.NamespaceMap.AddNamespace("rdf", RdfUris.RdfUrlPrefix);
        graph.NamespaceMap.AddNamespace("rdfs", RdfUris.RdfsUrlPrefix);
        graph.NamespaceMap.AddNamespace("sh", RdfUris.ShaclPrefix);

        foreach (var result in resultSet)
        {
            graph.Assert(result.Value("s"), result.Value("p"), result.Value("o"));
        }

        // Extract the object
        return new ApplicationProfile
        {
            Uri = graph.BaseUri,
            Format = rdfFormat,
            Definition = _rdfRepositoryBase.WriteGraph(graph, rdfFormat),
            DisplayName = resultSet.FirstOrDefault(e => e.Value("name") is not null)?.Value("name")?.AsValuedNode().AsString(),
            Description = resultSet.FirstOrDefault(e => e.Value("desc") is not null)?.Value("desc")?.AsValuedNode().AsString(),
            Graph = graph
        };
    }
}