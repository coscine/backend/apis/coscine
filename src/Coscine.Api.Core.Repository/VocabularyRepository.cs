﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Enums.Utility;
using Coscine.Api.Core.Shared.RequestFeatures;
using VDS.RDF.Nodes;
using VDS.RDF.Query;

namespace Coscine.Api.Core.Repository;

public sealed class VocabularyRepository : IVocabularyRepository
{
    private readonly IRdfRepositoryBase _rdfRepositoryBase;

    public VocabularyRepository(IRdfRepositoryBase rdfRepositoryBase)
    {
        _rdfRepositoryBase = rdfRepositoryBase;
    }

    public async Task<PagedEnumerable<Vocabulary>> GetPagedTopLevelVocabulariesAsync(VocabularyParameters vocabularyParameters)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT DISTINCT ?vocab ?name ?desc
                WHERE {
                    GRAPH ?vocab {
                        ?vocab rdf:type coscinetype:Vocabulary .

                        OPTIONAL {
                            ?vocab (rdfs:label|dcterms:title|dc:title) ?l1 .
                            FILTER LANGMATCHES(LANG(?l1), @lang)
                        }
                        # OPTIONAL {
                        #     FILTER(!BOUND(?l1))
                        #     ?vocab (rdfs:label|dcterms:title|dc:title) ?l2 .
                        #     FILTER LANGMATCHES(LANG(?l2), """")
                        # }
                        OPTIONAL {
                            FILTER(!BOUND(?l2))
                            ?vocab (rdfs:label|dcterms:title|dc:title) ?l3 .
                            FILTER LANGMATCHES(LANG(?l3), @fallbackLang)
                        }
                        BIND(COALESCE(?undef, ?l1, ?l2, ?l3) AS ?name)

                        OPTIONAL {
                            ?vocab (rdfs:comment|dcterms:description|dc:description) ?d1 .
                            FILTER LANGMATCHES(LANG(?d1), @lang)
                        }
                        # OPTIONAL {
                        #     FILTER(!BOUND(?d1))
                        #     ?vocab (rdfs:comment|dcterms:description|dc:description) ?d2 .
                        #     FILTER LANGMATCHES(LANG(?d2), """")
                        # }
                        OPTIONAL {
                            FILTER(!BOUND(?d2))
                            ?vocab (rdfs:comment|dcterms:description|dc:description) ?d3 .
                            FILTER LANGMATCHES(LANG(?d3), @fallbackLang)
                        }
                        BIND(COALESCE(?undef, ?d1, ?d2, ?d3) AS ?desc)

                        FILTER (
                            REGEX(?vocab, @searchTerm, ""i"") ||
                            REGEX(?name, @searchTerm, ""i"") ||
                            REGEX(?desc, @searchTerm, ""i"")
                        )
                    }
                }
            "
        };

        // Set namespaces, literals and URIs
        query.Namespaces.AddNamespace("rdf", RdfUris.RdfUrlPrefix);
        query.Namespaces.AddNamespace("rdfs", RdfUris.RdfsUrlPrefix);
        query.Namespaces.AddNamespace("dcterms", RdfUris.DcTermsPrefix);
        query.Namespaces.AddNamespace("dc", RdfUris.DcPrefix);
        query.Namespaces.AddNamespace("coscinetype", RdfUris.CoscineTermsTypes);
        query.SetLiteral("lang", vocabularyParameters.Language.GetEnumMemberValue() ?? vocabularyParameters.Language.ToString(), normalizeValue: false);
        query.SetLiteral("fallbackLang", vocabularyParameters.FallbackLanguage.GetEnumMemberValue() ?? vocabularyParameters.FallbackLanguage.ToString(), normalizeValue: false);
        query.SetLiteral("searchTerm", vocabularyParameters.SearchTerm ?? string.Empty, normalizeValue: false);

        // Build and execute count query
        var count = await query.GetCountOrDefaultAsync(_rdfRepositoryBase);

        // Always sort before adding pagination, otherwise the query breaks!
        query.Sort<Vocabulary>(vocabularyParameters.OrderBy,
            new()
            {
                {"vocab", vi => vi.GraphUri },
                {"name", vi => vi.DisplayName },
                {"desc", vi => vi.Description }
            }
        );
        query.Paginate(vocabularyParameters.PageSize, vocabularyParameters.PageNumber);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        // Extract the object
        var vocabularies = resultSet
            .Select(r => new Vocabulary
            {
                GraphUri = new Uri(r.Value("vocab").ToString()),
                ClassUri = new Uri(r.Value("vocab").ToString()),
                DisplayName = r.Value("name")?.AsValuedNode().AsString(),
                Description = r.Value("desc")?.AsValuedNode().AsString(),
            });

        return new PagedEnumerable<Vocabulary>(vocabularies, count, vocabularyParameters.PageNumber, vocabularyParameters.PageSize);
    }

    public async Task<PagedEnumerable<VocabularyInstance>> GetPagedVocabularyInstancesAsync(Uri parentGraphUri, VocabularyInstancesParameters instanceParameters)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT DISTINCT ?inst ?name (SAMPLE(?iDesc) AS ?desc) (SAMPLE(?iType) AS ?type) (SAMPLE(?iSubClassOf) AS ?subClassOf)
                FROM @parentGraphUri
                WHERE {
                    ?inst rdf:type/rdfs:subClassOf* @class .
                    @class rdfs:subClassOf* ?vocab .

                    ?vocab rdf:type coscinetype:Vocabulary .

                    ?inst rdf:type ?iType .
                    OPTIONAL { ?inst rdf:type/rdfs:subClassOf ?iSubClassOf . }

                    OPTIONAL {
                        ?inst (rdfs:label|dcterms:title|dc:title) ?l1 .
                        FILTER LANGMATCHES(LANG(?l1), @lang)
                    }
                    # OPTIONAL {
                    #     FILTER(!BOUND(?l1))
                    #     ?inst (rdfs:label|dcterms:title|dc:title) ?l2 .
                    #     FILTER LANGMATCHES(LANG(?l2), """")
                    # }
                    OPTIONAL {
                        FILTER(!BOUND(?l2))
                        ?inst (rdfs:label|dcterms:title|dc:title) ?l3 .
                        FILTER LANGMATCHES(LANG(?l3), @fallbackLang)
                    }
                    BIND(COALESCE(?undef, ?l1, ?l2, ?l3) AS ?name)

                    OPTIONAL {
                        ?inst (rdfs:comment|dcterms:description|dc:description) ?d1 .
                        FILTER LANGMATCHES(LANG(?d1), @lang)
                    }
                    # OPTIONAL {
                    #     FILTER(!BOUND(?d1))
                    #     ?inst (rdfs:comment|dcterms:description|dc:description) ?d2 .
                    #     FILTER LANGMATCHES(LANG(?d2), """")
                    # }
                    OPTIONAL {
                        FILTER(!BOUND(?d2))
                        ?inst (rdfs:comment|dcterms:description|dc:description) ?d3 .
                        FILTER LANGMATCHES(LANG(?d3), @fallbackLang)
                    }
                    BIND(COALESCE(?undef, ?d1, ?d2, ?d3) AS ?iDesc)

                    FILTER (
                        REGEX(?inst, @searchTerm, ""i"") ||
                        REGEX(?name, @searchTerm, ""i"") ||
                        REGEX(?iDesc, @searchTerm, ""i"")
                    )
                }
                GROUP BY ?inst ?name
            "
        };

        // Set namespaces, literals and URIs
        query.Namespaces.AddNamespace("rdf", RdfUris.RdfUrlPrefix);
        query.Namespaces.AddNamespace("rdfs", RdfUris.RdfsUrlPrefix);
        query.Namespaces.AddNamespace("dcterms", RdfUris.DcTermsPrefix);
        query.Namespaces.AddNamespace("dc", RdfUris.DcPrefix);
        query.Namespaces.AddNamespace("coscinetype", RdfUris.CoscineTermsTypes);
        query.SetUri("parentGraphUri", parentGraphUri);
        query.SetUri("class", instanceParameters.Class);
        query.SetLiteral("lang", instanceParameters.Language.GetEnumMemberValue() ?? instanceParameters.Language.ToString(), normalizeValue: false);
        query.SetLiteral("fallbackLang", instanceParameters.FallbackLanguage.GetEnumMemberValue() ?? instanceParameters.FallbackLanguage.ToString(), normalizeValue: false);
        query.SetLiteral("searchTerm", instanceParameters.SearchTerm ?? string.Empty, normalizeValue: false);

        // Build and execute count query
        var count = await query.GetCountOrDefaultAsync(_rdfRepositoryBase);

        // Always sort before adding pagination, otherwise the query breaks!
        query.Sort<VocabularyInstance>(instanceParameters.OrderBy,
            new()
            {
                {"inst", vi => vi.InstanceUri },
                {"name", vi => vi.DisplayName },
                {"desc", vi => vi.Description }
            }
        );
        query.Paginate(instanceParameters.PageSize, instanceParameters.PageNumber);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        // Extract the object
        var vocabularyInstances = resultSet
            .Select(r => new VocabularyInstance
            {
                GraphUri = parentGraphUri,
                InstanceUri = new Uri(r.Value("inst").ToString()),
                TypeUri = Uri.TryCreate(r.Value("type")?.ToString(), UriKind.Absolute, out var typeUri) ? typeUri : null,
                SubClassOfUri = Uri.TryCreate(r.Value("subClassOf")?.ToString(), UriKind.Absolute, out var subClassUri) ? subClassUri : null,
                DisplayName = r.Value("name")?.AsValuedNode().AsString(),
                Description = r.Value("desc")?.AsValuedNode().AsString(),
            });

        return new PagedEnumerable<VocabularyInstance>(vocabularyInstances, count, instanceParameters.PageNumber, instanceParameters.PageSize);
    }

    public async Task<VocabularyInstance?> GetAsync(Uri parentGraphUri, Uri classUri, Uri instanceUri, AcceptedLanguage language, AcceptedLanguage fallbackLanguage)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT DISTINCT ?inst ?name (?iDesc AS ?desc) (?iType AS ?type) (?iSubClassOf AS ?subClassOf)
                FROM @parentGraphUri
                WHERE {
                    ?inst rdf:type/rdfs:subClassOf* @class .

                    ?inst rdf:type ?iType .

                    OPTIONAL { ?inst rdf:type/rdfs:subClassOf ?iSubClassOf . }

                    OPTIONAL {
                        ?inst (rdfs:label|dcterms:title|dc:title) ?l1 .
                        FILTER LANGMATCHES(LANG(?l1), @lang)
                    }
                    # OPTIONAL {
                    #     FILTER(!BOUND(?l1))
                    #     ?inst (rdfs:label|dcterms:title|dc:title) ?l2 .
                    #     FILTER LANGMATCHES(LANG(?l2), """")
                    # }
                    OPTIONAL {
                        FILTER(!BOUND(?l2))
                        ?inst (rdfs:label|dcterms:title|dc:title) ?l3 .
                        FILTER LANGMATCHES(LANG(?l3), @fallbackLang)
                    }
                    BIND(COALESCE(?undef, ?l1, ?l2, ?l3) AS ?name)

                    OPTIONAL {
                        ?inst (rdfs:comment|dcterms:description|dc:description) ?d1 .
                        FILTER LANGMATCHES(LANG(?d1), @lang)
                    }
                    # OPTIONAL {
                    #     FILTER(!BOUND(?d1))
                    #     ?inst (rdfs:comment|dcterms:description|dc:description) ?d2 .
                    #     FILTER LANGMATCHES(LANG(?d2), """")
                    # }
                    OPTIONAL {
                        FILTER(!BOUND(?d2))
                        ?inst (rdfs:comment|dcterms:description|dc:description) ?d3 .
                        FILTER LANGMATCHES(LANG(?d3), @fallbackLang)
                    }
                    BIND(COALESCE(?undef, ?d1, ?d2, ?d3) AS ?iDesc)

                    FILTER(?inst = @inst)               # Using filter, because it fails with BIND
                }
                LIMIT 1
            "
        };

        // Set namespaces, literals and URIs
        query.Namespaces.AddNamespace("rdf", RdfUris.RdfUrlPrefix);
        query.Namespaces.AddNamespace("rdfs", RdfUris.RdfsUrlPrefix);
        query.Namespaces.AddNamespace("dcterms", RdfUris.DcTermsPrefix);
        query.Namespaces.AddNamespace("dc", RdfUris.DcPrefix);
        query.Namespaces.AddNamespace("coscinetype", RdfUris.CoscineTermsTypes);
        query.SetUri("parentGraphUri", parentGraphUri);
        query.SetUri("class", classUri);
        query.SetUri("inst", instanceUri);
        query.SetLiteral("lang", language.GetEnumMemberValue() ?? language.ToString(), normalizeValue: false);
        query.SetLiteral("fallbackLang", fallbackLanguage.GetEnumMemberValue() ?? fallbackLanguage.ToString(), normalizeValue: false);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        // Extract the object
        var vocabularyInstance = resultSet
            .Select(r => new VocabularyInstance
            {
                GraphUri = parentGraphUri,
                InstanceUri = new Uri(r.Value("inst").ToString()),
                TypeUri = Uri.TryCreate(r.Value("type")?.ToString(), UriKind.Absolute, out var typeUri) ? typeUri : null,
                SubClassOfUri = Uri.TryCreate(r.Value("subClassOf")?.ToString(), UriKind.Absolute, out var subClassUri) ? subClassUri : null,
                DisplayName = r.Value("name")?.AsValuedNode().AsString(),
                Description = r.Value("desc")?.AsValuedNode().AsString(),
            })
            .SingleOrDefault();

        return vocabularyInstance;
    }

    public async Task<Vocabulary?> GetAsync(VocabularyInstancesParameters instanceParameters)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT ?vocabulary ?name ?desc
                WHERE {
                    ?class rdfs:subClassOf* ?vocabulary .
                    GRAPH ?vocabulary {
                        ?vocabulary rdf:type coscinetype:Vocabulary .

                        BIND (@class as ?class)

                        OPTIONAL {
                            ?vocabulary (rdfs:label|dcterms:title|dc:title) ?l1 .
                            FILTER LANGMATCHES(LANG(?l1), @lang)
                        }
                        # OPTIONAL {
                        #     FILTER(!BOUND(?l1))
                        #     ?vocabulary (rdfs:label|dcterms:title|dc:title) ?l2 .
                        #     FILTER LANGMATCHES(LANG(?l2), """")
                        # }
                        OPTIONAL {
                            FILTER(!BOUND(?l2))
                            ?vocabulary (rdfs:label|dcterms:title|dc:title) ?l3 .
                            FILTER LANGMATCHES(LANG(?l3), @fallbackLang)
                        }
                        BIND(COALESCE(?undef, ?l1, ?l2, ?l3) AS ?name)

                        OPTIONAL {
                            ?vocabulary (rdfs:comment|dcterms:description|dc:description) ?d1 .
                            FILTER LANGMATCHES(LANG(?d1), @lang)
                        }
                        # OPTIONAL {
                        #     FILTER(!BOUND(?d1))
                        #     ?vocabulary (rdfs:comment|dcterms:description|dc:description) ?d2 .
                        #     FILTER LANGMATCHES(LANG(?d2), """")
                        # }
                        OPTIONAL {
                            FILTER(!BOUND(?d2))
                            ?vocabulary (rdfs:comment|dcterms:description|dc:description) ?d3 .
                            FILTER LANGMATCHES(LANG(?d3), @fallbackLang)
                        }
                        BIND(COALESCE(?undef, ?d1, ?d2, ?d3) AS ?desc)
                    }
                }
                LIMIT 1
            "
        };

        // Set namespaces, literals and URIs
        query.Namespaces.AddNamespace("rdf", RdfUris.RdfUrlPrefix);
        query.Namespaces.AddNamespace("rdfs", RdfUris.RdfsUrlPrefix);
        query.Namespaces.AddNamespace("dcterms", RdfUris.DcTermsPrefix);
        query.Namespaces.AddNamespace("dc", RdfUris.DcPrefix);
        query.Namespaces.AddNamespace("coscinetype", RdfUris.CoscineTermsTypes);
        query.SetUri("class", instanceParameters.Class);
        query.SetLiteral("lang", instanceParameters.Language.GetEnumMemberValue() ?? instanceParameters.Language.ToString(), normalizeValue: false);
        query.SetLiteral("fallbackLang", instanceParameters.FallbackLanguage.GetEnumMemberValue() ?? instanceParameters.FallbackLanguage.ToString(), normalizeValue: false);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        // Extract the object
        var vocabulary = resultSet
            .Select(r => new Vocabulary
            {
                GraphUri = new Uri(r.Value("vocabulary").ToString()),
                ClassUri = new Uri(r.Value("vocabulary").ToString()),
                DisplayName = r.Value("name")?.AsValuedNode().AsString(),
                Description = r.Value("desc")?.AsValuedNode().AsString(),
            })
            .SingleOrDefault();

        return vocabulary;
    }

    public async Task<Vocabulary?> GetTopLevelAsync(Uri instanceUri, AcceptedLanguage language, AcceptedLanguage fallbackLanguage)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT ?vocabulary ?name ?desc
                WHERE {
                    ?inst rdf:type/rdfs:subClassOf* ?vocabulary .
                    GRAPH ?vocabulary {
                        ?vocabulary rdf:type coscinetype:Vocabulary .

                        BIND (@inst as ?inst)

                        OPTIONAL {
                            ?vocabulary (rdfs:label|dcterms:title|dc:title) ?l1 .
                            FILTER LANGMATCHES(LANG(?l1), @lang)
                        }
                        # OPTIONAL {
                        #     FILTER(!BOUND(?l1))
                        #     ?vocabulary (rdfs:label|dcterms:title|dc:title) ?l2 .
                        #     FILTER LANGMATCHES(LANG(?l2), """")
                        # }
                        OPTIONAL {
                            FILTER(!BOUND(?l2))
                            ?vocabulary (rdfs:label|dcterms:title|dc:title) ?l3 .
                            FILTER LANGMATCHES(LANG(?l3), @fallbackLang)
                        }
                        BIND(COALESCE(?undef, ?l1, ?l2, ?l3) AS ?name)

                        OPTIONAL {
                            ?vocabulary (rdfs:comment|dcterms:description|dc:description) ?d1 .
                            FILTER LANGMATCHES(LANG(?d1), @lang)
                        }
                        # OPTIONAL {
                        #     FILTER(!BOUND(?d1))
                        #     ?vocabulary (rdfs:comment|dcterms:description|dc:description) ?d2 .
                        #     FILTER LANGMATCHES(LANG(?d2), """")
                        # }
                        OPTIONAL {
                            FILTER(!BOUND(?d2))
                            ?vocabulary (rdfs:comment|dcterms:description|dc:description) ?d3 .
                            FILTER LANGMATCHES(LANG(?d3), @fallbackLang)
                        }
                        BIND(COALESCE(?undef, ?d1, ?d2, ?d3) AS ?desc)
                    }
                }
                LIMIT 1
            "
        };

        // Set namespaces, literals and URIs
        query.Namespaces.AddNamespace("rdf", RdfUris.RdfUrlPrefix);
        query.Namespaces.AddNamespace("rdfs", RdfUris.RdfsUrlPrefix);
        query.Namespaces.AddNamespace("dcterms", RdfUris.DcTermsPrefix);
        query.Namespaces.AddNamespace("dc", RdfUris.DcPrefix);
        query.Namespaces.AddNamespace("coscinetype", RdfUris.CoscineTermsTypes);
        query.SetUri("inst", instanceUri);
        query.SetLiteral("lang", language.GetEnumMemberValue() ?? language.ToString(), normalizeValue: false);
        query.SetLiteral("fallbackLang", fallbackLanguage.GetEnumMemberValue() ?? fallbackLanguage.ToString(), normalizeValue: false);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        // Extract the object
        var vocabulary = resultSet
            .Select(r => new Vocabulary
            {
                GraphUri = new Uri(r.Value("vocabulary").ToString()),
                ClassUri = new Uri(r.Value("vocabulary").ToString()),
                DisplayName = r.Value("name")?.AsValuedNode().AsString(),
                Description = r.Value("desc")?.AsValuedNode().AsString(),
            })
            .SingleOrDefault();

        return vocabulary;
    }
}