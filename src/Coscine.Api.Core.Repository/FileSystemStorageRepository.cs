using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Repository;

public class FileSystemStorageRepository : IDataStorageRepository
{
    public Guid TypeId => Configurations.ResourceTypeConfiguration.LocalResourceType.Id;

    private readonly string baseDirectory = Path.GetTempPath();
    private readonly Dictionary<string, string> fileVersions = [];

    public FileSystemStorageRepository()
    {
    }

    // Blob operations (CRUD)

    public async Task CreateAsync(CreateBlobParameters parameters)
    {
        var filePath = GetFilePath(parameters.Resource, parameters.Path, parameters.Version);
        var dirPath = Path.GetDirectoryName(filePath);

        if (dirPath is not null && !Directory.Exists(dirPath))
        {
            Directory.CreateDirectory(dirPath);
        }

        using (var fileStream = new FileStream(filePath, FileMode.CreateNew, FileAccess.Write))
        {
            await parameters.Blob.CopyToAsync(fileStream);
        }

        AddVersion(parameters.Path, parameters.Version);
    }

    public async Task<Stream?> ReadAsync(ReadBlobParameters parameters)
    {
        var filePath = GetFilePath(parameters.Resource, parameters.Path, parameters.Version);
        if (File.Exists(filePath))
        {
            var memoryStream = new MemoryStream();
            using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                await fileStream.CopyToAsync(memoryStream);
            }
            memoryStream.Position = 0; // Reset the stream position for reading
            return memoryStream;
        }
        return null;
    }

    public async Task UpdateAsync(UpdateBlobParameters parameters)
    {
        var filePath = GetFilePath(parameters.Resource, parameters.Path, parameters.Version);
        using (var fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
        {
            await parameters.Blob.CopyToAsync(fileStream);
        }
        AddVersion(parameters.Path, parameters.Version);
    }

    public Task DeleteAsync(DeleteBlobParameters parameters)
    {
        var filePath = GetFilePath(parameters.Resource, parameters.Path, parameters.Version);
        if (File.Exists(filePath))
        {
            File.Delete(filePath);
        }
        RemoveVersion(parameters.Path, parameters.Version);
        return Task.CompletedTask;
    }

    // Tree operations (CRUD + List)

    public Task CreateAsync(CreateTreeParameters parameters)
    {
        var dirPath = GetDirectoryPath(parameters.Resource, parameters.Path, parameters.Version);
        Directory.CreateDirectory(dirPath);
        return Task.CompletedTask;
    }

    public Task<StorageItemInfo?> ReadAsync(ReadTreeParameters parameters)
    {
        var path = GetDirectoryPath(parameters.Resource, parameters.Path, parameters.Version);
        if (Directory.Exists(path))
        {
            var dirInfo = new DirectoryInfo(path);
            return Task.FromResult<StorageItemInfo?>(new StorageItemInfo
            {
                Path = dirInfo.FullName,
                IsBlob = false,
                CreatedDate = dirInfo.CreationTime,
                LastModifiedDate = dirInfo.LastWriteTime,
                Version = parameters.Version
            });
        }

        if (File.Exists(path))
        {
            var fileInfo = new DirectoryInfo(path);
            return Task.FromResult<StorageItemInfo?>(new StorageItemInfo
            {
                Path = fileInfo.FullName,
                IsBlob = true,
                CreatedDate = fileInfo.CreationTime,
                LastModifiedDate = fileInfo.LastWriteTime,
                Version = parameters.Version
            });
        }

        return Task.FromResult<StorageItemInfo?>(null);
    }

    public Task UpdateAsync(UpdateTreeParameters parameters)
    {
        // In file system, updating a directory usually implies moving it.
        var oldDirPath = GetDirectoryPath(parameters.Resource, parameters.Path, null);
        var newDirPath = GetDirectoryPath(parameters.Resource, parameters.Path, parameters.Version);
        if (Directory.Exists(oldDirPath))
        {
            Directory.Move(oldDirPath, newDirPath);
        }
        return Task.CompletedTask;
    }

    public Task DeleteAsync(DeleteTreeParameters parameters)
    {
        var dirPath = GetDirectoryPath(parameters.Resource, parameters.Path, parameters.Version);
        if (Directory.Exists(dirPath))
        {
            if (parameters.Recursive == true)
            {
                Directory.Delete(dirPath, true);
            }
            else
            {
                Directory.Delete(dirPath, false);
            }
        }
        return Task.CompletedTask;
    }

    public Task<IEnumerable<StorageItemInfo>> ListAsync(ListTreeParameters parameters)
    {
        var dirPath = Path.Combine(baseDirectory, parameters.Resource.Id.ToString(), parameters.TreePath);

        if (Directory.Exists(dirPath))
        {
            var items = new DirectoryInfo(dirPath)
                .GetFileSystemInfos()
                .Select(item => new StorageItemInfo
                {
                    Path = Path.GetRelativePath(Path.Combine(baseDirectory, parameters.Resource.Id.ToString()), item.FullName),
                    IsBlob = (item.Attributes & FileAttributes.Directory) == 0,
                    ContentLength = item is FileInfo fileInfo ? fileInfo.Length : null,
                    CreatedDate = item.CreationTime,
                    LastModifiedDate = item.LastWriteTime
                });

            return Task.FromResult(items);
        }

        return Task.FromResult(Enumerable.Empty<StorageItemInfo>());
    }

    // IDataStorageRepository additional methods

    public Task CreateAsync(Resource resource, CreateDataStorageParameters parameters)
    {
        // Implementing data storage creation (e.g., setting quotas or metadata)
        // For now, we can create a base directory for the resource
        var resourcePath = Path.Combine(baseDirectory, resource.Id.ToString());
        Directory.CreateDirectory(resourcePath);
        return Task.CompletedTask;
    }

    public Task UpdateAsync(Resource resource, UpdateDataStorageParameters parameters)
    {
        // For now, we can handle quota or readonly logic in-memory or persist in metadata
        // This implementation is simplified
        return Task.CompletedTask;
    }

    public IEnumerable<Capability> Capabilities =>
    [
        Capability.CanRead,
        Capability.CanWrite,
        Capability.CanUpdate,
        Capability.CanDelete,
        //Capability.UsesQuota
    ];

    public Task<DataStorageInfo> GetStorageInfoAsync(StorageInfoParameters parameters)
    {
        var storageInfo = new DataStorageInfo();

        if (parameters.FetchUsedSize)
        {
            storageInfo.UsedSize = Directory.GetFiles(Path.Combine(baseDirectory, parameters.Resource.Id.ToString()), "*.*", SearchOption.AllDirectories)
                .Select(file => new FileInfo(file).Length)
                .Sum();
        }

        if (parameters.FetchObjectCount)
        {
            storageInfo.ObjectCount = Directory.GetFiles(Path.Combine(baseDirectory, parameters.Resource.Id.ToString()), "*.*", SearchOption.AllDirectories).Length;
        }

        if (parameters.FetchAllocatedSize)
        {
            // Assuming allocated size is the used size for this simple implementation
            storageInfo.AllocatedSize = storageInfo.UsedSize;
        }

        storageInfo.FileSystemStorageInfo = new FileSystemStorageInfo { Directory = Path.Combine(baseDirectory, parameters.Resource.Id.ToString()) };

        return Task.FromResult(storageInfo);
    }

    // SpecificType related methods (dummy implementations)
    public IEnumerable<SpecificType> GetSpecificResourceTypes()
    {
        return []; // Placeholder implementation
    }

    public IEnumerable<SpecificType> GetSpecificResourceTypes(ResourceTypeStatus status)
    {
        return []; // Placeholder implementation
    }

    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor)
    {
        return []; // Placeholder implementation
    }

    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor, ResourceTypeStatus status)
    {
        return []; // Placeholder implementation
    }

    // Helper methods

    private string GetFilePath(Resource resource, string relativePath, string? version)
    {
        return Path.Combine(baseDirectory, resource.Id.ToString(), version != null ? $"{relativePath}_v{version}" : relativePath);
    }

    private string GetDirectoryPath(Resource resource, string relativePath, string? version)
    {
        return Path.Combine(baseDirectory, resource.Id.ToString(), version != null ? $"{relativePath}_v{version}" : relativePath);
    }

    private void AddVersion(string path, string? version)
    {
        if (version != null)
        {
            fileVersions[path] = version;
        }
    }

    private void RemoveVersion(string path, string? version)
    {
        if (version != null && fileVersions.ContainsKey(path))
        {
            fileVersions.Remove(path);
        }
    }

    public Task<ResourceTypeInformation> GetResourceTypeInformationAsync()
    {
        return Task.FromResult(new ResourceTypeInformation());
    }
}