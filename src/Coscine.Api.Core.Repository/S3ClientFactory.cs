using Amazon.S3;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.Extensions.Options;

namespace Coscine.Api.Core.Repository;

public class S3ClientFactory(IOptionsSnapshot<RdsS3ResourceTypeConfiguration> rdsS3ResourceTypeConfigurations,
                             IOptionsSnapshot<RdsResourceTypeConfiguration> rdsResourceTypeConfigurations,
                             IOptionsSnapshot<RdsS3WormResourceTypeConfiguration> rdsS3WormResourceTypeConfigurations,
                             IHttpClientFactory httpClientFactory) : IS3ClientFactory
{
    public IAmazonS3 GetClient(string accessKey, string secretKey, string endpoint)
    {
        var config = new AmazonS3Config
        {
            ServiceURL = endpoint,
            ForcePathStyle = true,
            HttpClientFactory = new CustomHttpClientFactory(httpClientFactory),
        };

        return new AmazonS3Client(accessKey, secretKey, config);
    }

    public IAmazonS3 GetRdsS3Client(string configKey)
    {
        try
        {
            var rdsS3Config = rdsS3ResourceTypeConfigurations.Get(configKey) ?? throw new ArgumentException($"No configuration found for key: {configKey}");

            return GetClient(rdsS3Config.AccessKey ?? throw new ArgumentException(@"Config does not contain an ""AccessKey"""),
                             rdsS3Config.SecretKey ?? throw new ArgumentException(@"Config does not contain n ""SecretKey"""),
                             rdsS3Config.Endpoint);
        }
        catch (Exception ex)
        {
            throw new InvalidOperationException($"Failed to create S3 client for config key: {configKey}", ex);
        }
    }

    public IAmazonS3 GetRdsClient(string configKey)
    {
        try
        {
            var rdsConfig = rdsResourceTypeConfigurations.Get(configKey) ?? throw new ArgumentException($"No configuration found for key: {configKey}");

            return GetClient(rdsConfig.AccessKey ?? throw new ArgumentException(@"Config does not contain an ""AccessKey"""),
                             rdsConfig.SecretKey ?? throw new ArgumentException(@"Config does not contain n ""SecretKey"""),
                             rdsConfig.Endpoint ?? throw new ArgumentException(@"Config does not contain n ""Endpoint"""));
        }
        catch (Exception ex)
        {
            throw new InvalidOperationException($"Failed to create S3 client for config key: {configKey}", ex);
        }
    }

    public IAmazonS3 GetRdsS3WormClient(string configKey)
    {
        try
        {
            var rdsS3WormConfig = rdsS3WormResourceTypeConfigurations.Get(configKey) ?? throw new ArgumentException($"No configuration found for key: {configKey}");

            return GetClient(rdsS3WormConfig.AccessKey ?? throw new ArgumentException(@"Config does not contain an ""AccessKey"""),
                             rdsS3WormConfig.SecretKey ?? throw new ArgumentException(@"Config does not contain n ""SecretKey"""),
                             rdsS3WormConfig.Endpoint);
        }
        catch (Exception ex)
        {
            throw new InvalidOperationException($"Failed to create S3 client for config key: {configKey}", ex);
        }
    }

    public IAmazonS3 GetClient(RdsS3ResourceType rdsS3ResourceType)
    {
        return GetClient(rdsS3ResourceType.AccessKey, rdsS3ResourceType.SecretKey, rdsS3ResourceType.Endpoint);
    }

    public IAmazonS3 GetClient(RdsResourceType rdsResourceType)
    {
        return GetClient(rdsResourceType.AccessKey, rdsResourceType.SecretKey, rdsResourceType.Endpoint);
    }

    public IAmazonS3 GetClient(RdsS3WormResourceType rdsS3WormResourceType)
    {
        return GetClient(rdsS3WormResourceType.AccessKey, rdsS3WormResourceType.SecretKey, rdsS3WormResourceType.Endpoint);
    }
}