using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class ProjectInvitationRepository : IProjectInvitationRepository
{
    private readonly RepositoryBase<Invitation> _projectInvitation;

    public ProjectInvitationRepository(RepositoryContext repositoryContext)
    {
        _projectInvitation = new(repositoryContext);
    }

    public void Create(Invitation projectInvitation)
    {
        _projectInvitation.Create(projectInvitation);
    }

    public void Delete(Invitation projectInvitation)
    {
        _projectInvitation.Delete(projectInvitation);
    }

    public async Task<PagedEnumerable<Invitation>> GetPagedAsync(Guid projectId, ProjectInvitationParameters projectInvitationParameters, bool trackChanges)
    {
        var projectInvitationQuery = _projectInvitation.FindByCondition(x => x.Project == projectId, trackChanges);

        var projectInvitations = await projectInvitationQuery
            .SortProjectInvitations(projectInvitationParameters.OrderBy)
            .Skip((projectInvitationParameters.PageNumber - 1) * projectInvitationParameters.PageSize)
            .Take(projectInvitationParameters.PageSize)
            .ToListAsync();

        var count = await projectInvitationQuery.CountAsync();

        return new PagedEnumerable<Invitation>(projectInvitations, count, projectInvitationParameters.PageNumber, projectInvitationParameters.PageSize);
    }

    public async Task<Invitation?> GetAsync(Guid projectId, Guid projectInvitationId, bool trackChanges)
    {
        return await _projectInvitation.FindByCondition(
            x => x.Id == projectInvitationId && x.Project == projectId,
            trackChanges)
            .SingleOrDefaultAsync();
    }

    public async Task<bool> HasValidInvitationAsync(Guid projectId, string email, bool trackChanges)
    {
        return await _projectInvitation.FindByCondition(
                x => x.Project == projectId &&
                     x.InviteeEmail == email &&
                     x.Expiration > DateTime.UtcNow, trackChanges)
            .AnyAsync();
    }

    public async Task<IEnumerable<Invitation>> GetAllExpiredInvitationsAsync(Guid projectId, bool trackChanges)
    {
        return await _projectInvitation.FindByCondition(
                x => x.Project == projectId &&
                     x.Expiration <= DateTime.UtcNow, trackChanges)
            .ToListAsync();
    }

    public async Task<Invitation?> GetAsync(Guid token, bool trackChanges)
    {
        return await _projectInvitation
            .FindByCondition(x => x.Token == token, trackChanges)
            .SingleOrDefaultAsync();
    }

    public async Task MergeAsync(Guid fromUserId, Guid toUserId)
    {
        foreach (var invitaion in await _projectInvitation
            .FindByCondition(x => x.Issuer == fromUserId, trackChanges: true)
            .ToListAsync())
        {
            // Make sure, that the users are not duplicated
            // Check, if the from user invited someone to the same project, as the to user did.
            // Should this not be, set move the invitation over.
            if (!await _projectInvitation.FindByCondition(x => x.Issuer == toUserId
                && x.Project == invitaion.Project
                && x.InviteeEmail == invitaion.InviteeEmail, trackChanges: false)
                    .AnyAsync())
            {
                // Not already invited to the project
                invitaion.Issuer = toUserId;
            }
            else
            {
                // User is already invited to the project, delete old reference
                Delete(invitaion);
            }
        }
    }
}