using Amazon.S3;
using Amazon.S3.Model;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.EmbeddedResources;
using Coscine.Api.Core.Repository.ResourceTypeDefinitions;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Helpers;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Coscine.Api.Core.Repository;

public class RdsS3RwthDataStorageRepository(IOptionsSnapshot<RdsS3ResourceTypeConfiguration> rdsS3ResourceTypeConfigurations,
                                      IRdsS3ResourceTypeRepository rdsS3ResourceTypeRepository,
                                      IHttpClientFactory httpClientFactory,
                                      ILogger<RdsS3DataStorageRepository> logger,
                                      IS3ClientFactory s3ClientFactory,
                                      IEcsManagerFactory ecsManagerFactory) : RdsS3DataStorageRepository(rdsS3ResourceTypeRepository,
                                                                                         httpClientFactory,
                                                                                         logger,
                                                                                         s3ClientFactory, ecsManagerFactory)
{
    protected override string ConfigKey => "rdss3rwth";
    public override Guid TypeId => Configurations.ResourceTypeConfiguration.ResourceTypeRdss3.Id;
    protected override RdsS3ResourceTypeConfiguration RdsS3ResourceTypeConfiguration => rdsS3ResourceTypeConfigurations.Get(ConfigKey);
}

public class RdsS3NrwDataStorageRepository(IOptionsSnapshot<RdsS3ResourceTypeConfiguration> rdsS3ResourceTypeConfigurations,
                                      IRdsS3ResourceTypeRepository rdsS3ResourceTypeRepository,
                                      IHttpClientFactory httpClientFactory,
                                      ILogger<RdsS3DataStorageRepository> logger,
                                      IS3ClientFactory s3ClientFactory,
                                      IEcsManagerFactory ecsManagerFactory) : RdsS3DataStorageRepository(rdsS3ResourceTypeRepository,
                                                                                         httpClientFactory,
                                                                                         logger,
                                                                                         s3ClientFactory,
                                                                                         ecsManagerFactory)
{
    protected override string ConfigKey => "rdss3nrw";
    public override Guid TypeId => Configurations.ResourceTypeConfiguration.ResourceTypeRdss3Nrw.Id;
    protected override RdsS3ResourceTypeConfiguration RdsS3ResourceTypeConfiguration => rdsS3ResourceTypeConfigurations.Get(ConfigKey);
}

public class RdsS3UdeDataStorageRepository(IOptionsSnapshot<RdsS3ResourceTypeConfiguration> rdsS3ResourceTypeConfigurations,
                                      IRdsS3ResourceTypeRepository rdsS3ResourceTypeRepository,
                                      IHttpClientFactory httpClientFactory,
                                      ILogger<RdsS3DataStorageRepository> logger,
                                      IS3ClientFactory s3ClientFactory,
                                      IEcsManagerFactory ecsManagerFactory) : RdsS3DataStorageRepository(rdsS3ResourceTypeRepository,
                                                                                         httpClientFactory,
                                                                                         logger,
                                                                                         s3ClientFactory,
                                                                                         ecsManagerFactory)
{
    protected override string ConfigKey => "rdss3ude";
    public override Guid TypeId => Configurations.ResourceTypeConfiguration.ResourceTypeRdss3Ude.Id;
    protected override RdsS3ResourceTypeConfiguration RdsS3ResourceTypeConfiguration => rdsS3ResourceTypeConfigurations.Get(ConfigKey);
}

public class RdsS3TudoDataStorageRepository(IOptionsSnapshot<RdsS3ResourceTypeConfiguration> rdsS3ResourceTypeConfigurations,
                                      IRdsS3ResourceTypeRepository rdsS3ResourceTypeRepository,
                                      IHttpClientFactory httpClientFactory,
                                      ILogger<RdsS3DataStorageRepository> logger,
                                      IS3ClientFactory s3ClientFactory,
                                      IEcsManagerFactory ecsManagerFactory) : RdsS3DataStorageRepository(rdsS3ResourceTypeRepository,
                                                                                         httpClientFactory,
                                                                                         logger,
                                                                                         s3ClientFactory,
                                                                                         ecsManagerFactory)
{
    protected override string ConfigKey => "rdss3tudo";
    public override Guid TypeId => Configurations.ResourceTypeConfiguration.ResourceTypeRdss3Tudo.Id;
    protected override RdsS3ResourceTypeConfiguration RdsS3ResourceTypeConfiguration => rdsS3ResourceTypeConfigurations.Get(ConfigKey);
}

public class RdsS3RubDataStorageRepository(IOptionsSnapshot<RdsS3ResourceTypeConfiguration> rdsS3ResourceTypeConfigurations,
                                      IRdsS3ResourceTypeRepository rdsS3ResourceTypeRepository,
                                      IHttpClientFactory httpClientFactory,
                                      ILogger<RdsS3DataStorageRepository> logger,
                                      IS3ClientFactory s3ClientFactory,
                                      IEcsManagerFactory ecsManagerFactory) : RdsS3DataStorageRepository(rdsS3ResourceTypeRepository,
                                                                                         httpClientFactory,
                                                                                         logger,
                                                                                         s3ClientFactory,
                                                                                         ecsManagerFactory)
{
    protected override string ConfigKey => "rdss3rub";
    public override Guid TypeId => Configurations.ResourceTypeConfiguration.ResourceTypeRdss3Rub.Id;
    protected override RdsS3ResourceTypeConfiguration RdsS3ResourceTypeConfiguration => rdsS3ResourceTypeConfigurations.Get(ConfigKey);
}

public abstract class RdsS3DataStorageRepository(IRdsS3ResourceTypeRepository rdsS3ResourceTypeRepository,
                                        IHttpClientFactory httpClientFactory,
                                        ILogger<RdsS3DataStorageRepository> logger,
                                        IS3ClientFactory s3ClientFactory,
                                        IEcsManagerFactory ecsManagerFactory) : IDataStorageRepository
{
    public abstract Guid TypeId { get; }
    protected abstract RdsS3ResourceTypeConfiguration RdsS3ResourceTypeConfiguration { get; }
    protected abstract string ConfigKey { get; }
    public IEnumerable<Capability> Capabilities
    {
        get
        {
            yield return Capability.CanWrite;
            yield return Capability.CanRead;
            yield return Capability.CanUpdate;
            yield return Capability.CanDelete;
            yield return Capability.SupportsLocalMetadataCopy;
            yield return Capability.SupportsReadOnlyMode;
            yield return Capability.UsesQuota;
            yield return Capability.SupportsLinks;
        }
    }
    private readonly IRdsS3ResourceTypeRepository _rdsS3ResourceTypeRepository = rdsS3ResourceTypeRepository;
    private readonly IHttpClientFactory _httpClientFactory = httpClientFactory;
    private readonly ILogger<RdsS3DataStorageRepository> _logger = logger;
    private readonly IS3ClientFactory _s3ClientFactory = s3ClientFactory;
    private readonly IEnumerable<string> _readRights = ["read", "read_acl"];
    private readonly IEnumerable<string> _writeRights = ["read", "read_acl", "write", "execute", "privileged_write", "delete"];

    public async Task CreateAsync(Resource resource, CreateDataStorageParameters parameters)
    {
        ArgumentNullException.ThrowIfNull(parameters.Quota);

        var rdsS3ResourceType = new RdsS3ResourceType
        {
            Id = Guid.NewGuid(), // Set the ID manually, object does not exist in the db until saved (even after the .Create())
            BucketName = resource.Id.ToString(),
            AccessKey = RdsS3ResourceTypeConfiguration.AccessKey!,
            SecretKey = RdsS3ResourceTypeConfiguration.SecretKey!,
            AccessKeyRead = $"read_{resource.Id}",
            SecretKeyRead = RandomHelper.GenerateRandomChunk(32),
            AccessKeyWrite = $"write_{resource.Id}",
            SecretKeyWrite = RandomHelper.GenerateRandomChunk(32),
            Endpoint = RdsS3ResourceTypeConfiguration.Endpoint
        };

        _rdsS3ResourceTypeRepository.Create(rdsS3ResourceType);
        resource.ResourceTypeOptionId = rdsS3ResourceType.Id;

        var rdsS3EcsManager = ecsManagerFactory.GetRdsS3EcsManager(ConfigKey);
        var userEcsManager = ecsManagerFactory.GetRdsS3UserEcsManager(ConfigKey);

        await rdsS3EcsManager.CreateBucket(resource.Id.ToString(), parameters.Quota.Value);
        await userEcsManager.CreateObjectUser(rdsS3ResourceType.AccessKeyRead, rdsS3ResourceType.SecretKeyRead);
        await userEcsManager.CreateObjectUser(rdsS3ResourceType.AccessKeyWrite, rdsS3ResourceType.SecretKeyWrite);

        await rdsS3EcsManager.SetUserAcl(rdsS3ResourceType.AccessKeyRead, resource.Id.ToString(), _readRights);
        await rdsS3EcsManager.SetUserAcl(rdsS3ResourceType.AccessKeyWrite, resource.Id.ToString(), _writeRights);

        // Use the internal function to skip loading the data, that is not yet in the db
        await UpdateInternalAsync(resource, rdsS3ResourceType, new UpdateDataStorageParameters { ReadOnly = false });
    }

    public async Task CreateAsync(CreateBlobParameters parameters)
    {
        var url = await GetPreSignedUrl(parameters.Resource, parameters.Path, CoscineHttpMethod.PUT);
        var response = await _httpClientFactory.CreateClient(ClientName.RdsS3Client).PutAsync(url, new StreamContent(parameters.Blob));

        if (!response.IsSuccessStatusCode)
        {
            throw new AggregateException();
        }
    }

    public Task CreateAsync(CreateTreeParameters parameters)
    {
        throw new NotImplementedException();
    }

    public async Task DeleteAsync(DeleteBlobParameters parameters)
    {
        var url = await GetPreSignedUrl(parameters.Resource, parameters.Path, CoscineHttpMethod.DELETE);
        var responseMessage = await _httpClientFactory.CreateClient(ClientName.RdsS3Client).DeleteAsync(url);
        responseMessage.Content.ReadAsStream();
    }

    public Task DeleteAsync(DeleteTreeParameters parameters)
    {
        throw new NotImplementedException();
    }

    public Task<ResourceTypeInformation> GetResourceTypeInformationAsync()
    {
        var resourceTypeInformation = new ResourceTypeInformation
        {
            CanCreate = true,
            CanRead = true,
            CanSetResourceReadonly = true,
            CanUpdate = true,
            CanUpdateResource = false,
            CanDelete = true,
            CanDeleteResource = false,
            CanList = true,
            CanCreateLinks = true,
            CanCopyLocalMetadata = true,
            IsArchived = false,
            IsQuotaAvailable = true,
            IsQuotaAdjustable = true,
            ResourceCreate = new ResourceCreate
            {
                Components =
                [
                    ["Size"],    // First step with "Size"
                    [],          // Second step
                    [],          // Third step
                    []           // Fourth step
                ]
            },
            ResourceContent = new ResourceContent
            {
                ReadOnly = false,
                MetadataView = new MetadataView
                {
                    EditableDataUrl = false,
                    EditableKey = false
                },
                EntriesView = new EntriesView
                {
                    Columns = new ColumnsObject
                    {
                        Always = ["name", "size"]
                    }
                }
            },
            Status = RdsS3ResourceTypeConfiguration?.SpecificType?.Status ?? ResourceTypeStatus.Hidden,
            GeneralType = RdsS3ResourceTypeConfiguration?.SpecificType?.Type,
            SpecificType = RdsS3ResourceTypeConfiguration?.SpecificType?.SpecificTypeName
        };
        return Task.FromResult(resourceTypeInformation);
    }

    public IEnumerable<SpecificType> GetSpecificResourceTypes()
    {
        //TODO: this has to be moved
        return [RdsS3ResourceTypeConfiguration.SpecificType];
    }

    /// <summary>
    /// Retrieves specific resource types with a particular status.
    /// </summary>
    /// <param name="status">The status of the resource types to retrieve.</param>
    /// <returns>An IEnumerable of specific resource types that match the provided status.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(ResourceTypeStatus status)
    {
        return GetSpecificResourceTypes().Where(st => st.Status == status);
    }

    /// <summary>
    /// Retrieves specific resource types that are supported by a particular organization.
    /// </summary>
    /// <param name="OrganizationRor">The ROR of the organization to match against the supported organizations of the resource types.</param>
    /// <returns>An IEnumerable of specific resource types that are supported by the organization specified by the provided ROR.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor)
    {
        return GetSpecificResourceTypes().Where(st => st.SupportedOrganizations is not null
            && (st.SupportedOrganizations.Contains(OrganizationRor) || st.SupportedOrganizations.Contains("*")));
    }

    /// <summary>
    /// Retrieves specific resource types that are supported by a particular organization and have a particular status.
    /// </summary>
    /// <param name="OrganizationRor">The ROR of the organization to match against the supported organizations of the resource types.</param>
    /// <param name="status">The status of the resource types to retrieve.</param>
    /// <returns>An IEnumerable of specific resource types that match the provided status and are supported by the organization specified by the provided ROR.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor, ResourceTypeStatus status)
    {
        return GetSpecificResourceTypes(OrganizationRor).Where(st => st.Status == status);
    }

    public async Task<DataStorageInfo> GetStorageInfoAsync(StorageInfoParameters parameters)
    {
        var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var rdsS3ResourceType = await _rdsS3ResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new RdsS3ResourceTypeNotFoundException(resourceTypeOptionId);

        var info = new DataStorageInfo
        {
            RdsS3StorageInfo = new RdsS3StorageInfo
            {
                AccessKeyRead = rdsS3ResourceType.AccessKeyRead,
                AccessKeyWrite = rdsS3ResourceType.AccessKeyWrite,
                BucketName = rdsS3ResourceType.BucketName,
                Endpoint = rdsS3ResourceType.Endpoint,
                SecretKeyRead = rdsS3ResourceType.SecretKeyRead,
                SecretKeyWrite = rdsS3ResourceType.SecretKeyWrite,
            }
        };

        if (parameters.FetchUsedSize)
        {
            info.UsedSize = 0;

            var rdsS3EcsManager = ecsManagerFactory.GetRdsS3EcsManager(ConfigKey);
            info.UsedSize = await rdsS3EcsManager.GetBucketTotalUsedQuota(parameters.Resource.Id.ToString());

        }

        if (parameters.FetchObjectCount)
        {

        }

        if (parameters.FetchAllocatedSize)
        {

            var rdsS3EcsManager = ecsManagerFactory.GetRdsS3EcsManager(ConfigKey);
            info.AllocatedSize = await rdsS3EcsManager.GetBucketQuota(parameters.Resource.Id.ToString()) * 1024 * 1024 * 1024;
        }

        return info;
    }

    public async Task<IEnumerable<StorageItemInfo>> ListAsync(ListTreeParameters parameters)
    {
        // List all objects
        var listRequest = new ListObjectsRequest
        {
            BucketName = parameters.Resource.Id.ToString(),
            Prefix = parameters.TreePath,
            Delimiter = "/"
        };

        var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var rdsS3ResourceType = await _rdsS3ResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new RdsS3ResourceTypeNotFoundException(resourceTypeOptionId);


        using var s3Client = _s3ClientFactory.GetClient(rdsS3ResourceType);

        var entries = new List<StorageItemInfo>();

        ListObjectsResponse listResponse;
        do
        {
            // Get a list of objects
            listResponse = await s3Client.ListObjectsAsync(listRequest);

            foreach (var commonPrefix in listResponse.CommonPrefixes)
            {
                entries.Add(new StorageItemInfo
                {
                    Path = commonPrefix,
                    IsBlob = false,
                    ContentLength = 0,
                });

            }

            foreach (var obj in listResponse.S3Objects)
            {
                // Ensure not adding the same folder, as the prefix to the response
                // Happens, when an empty folder is added through and empty object with an delimiter as the last char of the key.
                if (!obj.Key.EndsWith('/') || obj.Size != 0 || obj.Key != parameters.TreePath)
                {

                    var entry = new StorageItemInfo
                    {
                        Path = obj.Key,
                        IsBlob = obj.Size > 0,
                        ContentLength = obj.Size,
                        CreatedDate = obj.LastModified,
                        LastModifiedDate = obj.LastModified,
                    };

                    if (parameters.GeneratePresignedLinks)
                    {
                        var expires = DateTime.UtcNow.AddHours(24);
                        var presignedUrl = s3Client.GetPreSignedURL(new GetPreSignedUrlRequest()
                        {
                            BucketName = parameters.Resource.Id.ToString(),
                            Key = obj.Key,
                            Verb = HttpVerb.GET,
                            Protocol = Protocol.HTTPS,
                            // For now, expiry of a day is set, but this might be up to debate
                            Expires = expires
                        });

                        entry.Links = new StorageLinks
                        {
                            Download = new InteractionLink
                            {
                                Url = presignedUrl,
                                Expiry = expires,
                                Method = CoscineHttpMethod.GET
                            }
                        };
                    }

                    entries.Add(entry);
                }
            }

            // Set the marker property
            listRequest.Marker = listResponse.NextMarker;

        } while (listResponse.IsTruncated);

        return entries;
    }

    public async Task<Stream?> ReadAsync(ReadBlobParameters parameters)
    {
        var url = await GetPreSignedUrl(parameters.Resource, parameters.Path, CoscineHttpMethod.GET);
        var responseMessage = await _httpClientFactory.CreateClient(ClientName.RdsS3Client).GetAsync(url, HttpCompletionOption.ResponseHeadersRead);

        if (responseMessage.IsSuccessStatusCode)
        {
            return await responseMessage.Content.ReadAsStreamAsync();
        }
        else
        {
            return null;
        }
    }

    public async Task<StorageItemInfo?> ReadAsync(ReadTreeParameters parameters)
    {
        var url = await GetPreSignedUrl(parameters.Resource, parameters.Path, CoscineHttpMethod.HEAD);
        var responseMessage = await _httpClientFactory.CreateClient(ClientName.RdsS3Client).SendAsync(new HttpRequestMessage(HttpMethod.Head, url));
        var contentLength = responseMessage.Content.Headers.ContentLength;

        if (!responseMessage.IsSuccessStatusCode)
        {
            return null;
        }

        if (!contentLength.HasValue)
        {
            throw new Exception("Content-Length could not be extracted.");
        }

        var entry = new StorageItemInfo
        {
            Path = parameters.Path,
            IsBlob = contentLength > 0,
            ContentLength = contentLength,
            CreatedDate = DateTime.Now,
            LastModifiedDate = DateTime.Now,
        };

        if (parameters.GeneratePresignedLinks)
        {
            var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
               ?? throw new ArgumentNullException();

            var rdsS3ResourceType = await _rdsS3ResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                           ?? throw new RdsS3ResourceTypeNotFoundException(resourceTypeOptionId);

            using var s3Client = _s3ClientFactory.GetClient(rdsS3ResourceType);

            var expires = DateTime.UtcNow.AddHours(24);

            var presignedUrl = s3Client.GetPreSignedURL(new GetPreSignedUrlRequest()
            {
                BucketName = parameters.Resource.Id.ToString(),
                Key = parameters.Path,
                Verb = HttpVerb.GET,
                Protocol = Protocol.HTTPS,
                // For now, expiry of a day is set, but this might be up to debate
                Expires = expires
            });

            entry.Links = new StorageLinks
            {
                Download = new InteractionLink
                {
                    Url = presignedUrl,
                    Expiry = expires,
                    Method = CoscineHttpMethod.GET
                }
            };
        }
        return entry;
    }

    // Used to skip loading the config from the db
    private async Task UpdateInternalAsync(Resource resource, RdsS3ResourceType rdsS3ResourceType, UpdateDataStorageParameters parameters)
    {
        using var s3Client = _s3ClientFactory.GetClient(rdsS3ResourceType);

        // Maintenance mode check
        if (parameters.MaintenanceMode is true)
        {
            var policy = GenerateMaintenanceModePolicy(
                rdsS3ResourceType.AccessKey,
                rdsS3ResourceType.AccessKeyWrite,
                rdsS3ResourceType.AccessKey,
                resource.Id.ToString()
            );
            var putRequest = new PutBucketPolicyRequest
            {
                BucketName = resource.Id.ToString(),
                Policy = policy
            };

            try
            {
                _logger.LogDebug("Updating bucket {resourceId} with maintenance mode policy:\n{policy}",
                    resource.Id.ToString(), policy);
                await s3Client.PutBucketPolicyAsync(putRequest);
            }
            catch (Exception exception)
            {
                _logger.LogWarning(exception, "Failed while updating the bucket policy for maintenance mode.");
            }
        }
        else
        {
            // Not in maintenance mode, fall back to possible read-only logic
            if (parameters.ReadOnly is not null)
            {
                var policy = GenerateReadonlyAccessPolicy(
                    rdsS3ResourceType.AccessKey,
                    rdsS3ResourceType.AccessKeyWrite,
                    rdsS3ResourceType.AccessKeyRead,
                    resource.Id.ToString(),
                    parameters.ReadOnly.Value
                );

                var putRequest = new PutBucketPolicyRequest
                {
                    BucketName = resource.Id.ToString(),
                    Policy = policy
                };

                try
                {
                    _logger.LogDebug("Updating bucket {resourceId} with policy: \n{policy}",
                        resource.Id.ToString(), policy);
                    await s3Client.PutBucketPolicyAsync(putRequest);
                }
                catch (Exception exception)
                {
                    _logger.LogWarning(exception, "Silent fail while updating the read-only policy.");
                }
            }
        }

        if (parameters.Quota is not null)
        {
            var rdsS3EcsManager = ecsManagerFactory.GetRdsS3EcsManager(ConfigKey);
            await rdsS3EcsManager.SetBucketQuota(resource.Id.ToString(), parameters.Quota.Value);
        }

        var putLifecycleRequest = new PutLifecycleConfigurationRequest
        {
            BucketName = resource.Id.ToString(),
            Configuration = GenerateLifecycleConfiguration()
        };

        try
        {
            _logger.LogDebug("Applying lifecycle configuration to bucket {resourceId} with configuration: \n{configuration}",
                resource.Id.ToString(), putLifecycleRequest.Configuration);
            await s3Client.PutLifecycleConfigurationAsync(putLifecycleRequest);
        }
        catch (Exception exception)
        {
            _logger.LogWarning(exception, "Silent fail while updating the lifecycle policy.");
        }
    }

    public async Task UpdateAsync(Resource resource, UpdateDataStorageParameters parameters)
    {
        var resourceTypeOptionId = resource.ResourceTypeOptionId
            ?? throw new ArgumentNullException();

        var rdsS3ResourceType = await _rdsS3ResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
            ?? throw new RdsS3ResourceTypeNotFoundException(resourceTypeOptionId);

        await UpdateInternalAsync(resource, rdsS3ResourceType, parameters);
    }

    public async Task UpdateAsync(UpdateBlobParameters parameters)
    {
        var url = await GetPreSignedUrl(parameters.Resource, parameters.Path, CoscineHttpMethod.PUT);
        var response = await _httpClientFactory.CreateClient(ClientName.RdsS3Client).PutAsync(url, new StreamContent(parameters.Blob));

        if (!response.IsSuccessStatusCode)
        {
            throw new AggregateException();
        }
    }

    public Task UpdateAsync(UpdateTreeParameters parameters)
    {
        throw new NotImplementedException();
    }

    private async Task<Uri> GetPreSignedUrl(Resource resource, string key, CoscineHttpMethod httpVerb, Dictionary<string, string>? options = null)
    {

        var resourceTypeOptionId = resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var rdsS3ResourceType = await _rdsS3ResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new RdsS3ResourceTypeNotFoundException(resourceTypeOptionId);

        using var s3Client = _s3ClientFactory.GetClient(rdsS3ResourceType);

        var presignedUrl = s3Client.GetPreSignedURL(new GetPreSignedUrlRequest()
        {
            BucketName = resource.Id.ToString(),
            Key = key,
            Verb = Helper.GetVerb(httpVerb),
            // The Simulator uses HTTP and the live system HTTPS
            Protocol = rdsS3ResourceType.Endpoint.Contains(Helper.ECS_SIM_URL) ? Protocol.HTTP : Protocol.HTTPS,
            // For now, expiry of a day is set, but this might be up to debate
            Expires = DateTime.UtcNow.AddHours(24)
        });

        return new Uri(presignedUrl);
    }

    /// <summary>
    /// Generates a policy for enabling maintenance mode on an S3 bucket. 
    /// This policy denies all S3 actions for the specified principal.
    /// </summary>
    /// <param name="accessKey">The access key of the principal to deny access.</param>
    /// <param name="accessKeyWrite">The access key of a principal with write access.</param>
    /// <param name="accessKeyRead">The access key of a principal with read-only access.</param>
    /// <param name="bucketName">The name of the S3 bucket.</param>
    /// <returns>A JSON string representing the maintenance mode policy.</returns>
    private static string GenerateMaintenanceModePolicy(string accessKey, string accessKeyWrite, string accessKeyRead, string bucketName)
    {
        // Deny *all* S3 actions for everyone
        var policy = new Amazon.Auth.AccessControlPolicy.Policy
        {
            Id = "MaintenanceModePolicy",
            Version = "2012-10-17",
            Statements = [
                new(Amazon.Auth.AccessControlPolicy.Statement.StatementEffect.Deny)
                {
                    Id = "DenyAll",
                    Principals = [ new(accessKey), new(accessKeyRead), new(accessKeyWrite) ],
                    Actions = [ new("s3:*") ],
                    Resources = [ new($"arn:aws:s3:::{bucketName}"), new($"arn:aws:s3:::{bucketName}/*") ]
                }
            ]
        };
        return policy.ToJson();
    }

    /// <summary>
    /// Generates a policy for read-only or write access to an S3 bucket with multiple access keys.
    /// The policy adjusts the permissions based on the <paramref name="isReadonly"/> flag.
    /// </summary>
    /// <param name="accessKey">The access key of a principal to grant or deny access.</param>
    /// <param name="writeKey">The access key of a principal with write access.</param>
    /// <param name="accessKeyRead">The access key of a principal with read-only access.</param>
    /// <param name="bucketname">The name of the S3 bucket.</param>
    /// <param name="isReadonly">
    /// A flag indicating whether the policy should provide read-only access (<c>true</c>) or write access (<c>false</c>).
    /// </param>
    /// <returns>A JSON string representing the generated access policy.</returns>
    /// <exception cref="Exception">Thrown when the policy JSON cannot be parsed.</exception>
    private static string GenerateReadonlyAccessPolicy(string accessKey, string writeKey, string accessKeyRead, string bucketname, bool isReadonly)
    {
        if (isReadonly)
        {
            var json = EmbeddedResourceLoader.ReadResource("RdsS3.BucketReadPolicy.json");
            var jObject = JsonConvert.DeserializeObject<JObject>(json)
                ?? throw new Exception("BucketReadPolicy.json could not be parsed.");
            {
                var allowStatement = jObject["Statement"]?.First(x => x["Sid"]?.ToString() == "AllowStatement") as JObject;
                (allowStatement?["Resource"] as JArray)?.Add($"{bucketname}/*");
                (allowStatement?["Principal"] as JArray)?.Add($"{writeKey}");
                (allowStatement?["Principal"] as JArray)?.Add($"{accessKey}");
                (allowStatement?["Principal"] as JArray)?.Add($"{accessKeyRead}");
            }
            {
                var denyStatement = jObject["Statement"]?.First(x => x["Sid"]?.ToString() == "DenyStatement") as JObject;
                (denyStatement?["Resource"] as JArray)?.Add($"{bucketname}/*");
                (denyStatement?["Principal"] as JArray)?.Add($"{writeKey}");
                (denyStatement?["Principal"] as JArray)?.Add($"{accessKey}");
                (denyStatement?["Principal"] as JArray)?.Add($"{accessKeyRead}");
            }

            return jObject.ToString();
        }
        else
        {
            var json = EmbeddedResourceLoader.ReadResource("RdsS3.BucketWritePolicy.json");
            var jObject = JsonConvert.DeserializeObject<JObject>(json)
                ?? throw new Exception("BucketWritePolicy.json could not be parsed.");
            {
                var allowStatement = jObject["Statement"]?.First(x => x["Sid"]?.ToString() == "WriteStatement") as JObject;
                (allowStatement?["Resource"] as JArray)?.Add($"{bucketname}/*");
                (allowStatement?["Principal"] as JArray)?.Add($"{writeKey}");
                (allowStatement?["Principal"] as JArray)?.Add($"{accessKey}");
            }
            {
                var denyStatement = jObject["Statement"]?.First(x => x["Sid"]?.ToString() == "ReadStatement") as JObject;
                (denyStatement?["Resource"] as JArray)?.Add($"{bucketname}/*");
                (denyStatement?["Principal"] as JArray)?.Add($"{accessKeyRead}");
            }

            return jObject.ToString();
        }
    }

    private static LifecycleConfiguration GenerateLifecycleConfiguration() => new()
    {
        Rules = [
            new LifecycleRule
                {
                    Id = "AbortIncompleteMultipartUploads",
                    Status = LifecycleRuleStatus.Enabled,
                    AbortIncompleteMultipartUpload = new LifecycleRuleAbortIncompleteMultipartUpload
                    {
                        DaysAfterInitiation = 7
                    },
                    Filter = new () {
                        LifecycleFilterPredicate = new LifecyclePrefixPredicate
                        {
                            Prefix = "" // Applies the rule to all objects in the bucket
                        }
                    }
                }
        ]
    };

}
