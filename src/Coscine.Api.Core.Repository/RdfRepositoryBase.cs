﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Contracts.Helpers;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Repository.Extensions.Utility;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Enums.Utility;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Diagnostics;
using VDS.RDF;
using VDS.RDF.Nodes;
using VDS.RDF.Query;
using VDS.RDF.Storage;
using VDS.RDF.Update;
using VDS.RDF.Writing;

namespace Coscine.Api.Core.Repository;

public class RdfRepositoryBase : IRdfRepositoryBase
{
    public ReadWriteSparqlConnector _sparqlConnector;

    private readonly SparqlQueryClient _readClient;
    private readonly SparqlUpdateClient _updateClient;
    private readonly ILogger<RdfRepositoryBase> _logger;

    public int QUERY_LIMIT { get; } = 1000;

    public RdfRepositoryBase(IHttpClientFactory clientFactory, IOptionsMonitor<SparqlConfiguration> sparqlConfigurationMonitor, ILogger<RdfRepositoryBase> logger)
    {
        var sparqlConfiguration = sparqlConfigurationMonitor.CurrentValue;
        ArgumentNullException.ThrowIfNull(sparqlConfiguration);
        ArgumentException.ThrowIfNullOrEmpty(sparqlConfiguration.ReadSparqlEndpoint);
        ArgumentException.ThrowIfNullOrEmpty(sparqlConfiguration.UpdateSparqlEndpoint);

        _readClient = new(clientFactory.CreateClient(ClientName.VirtuosoReadClient), new Uri(sparqlConfiguration.ReadSparqlEndpoint))
        {
            // Workaround to allow weird control characters in metadata that the XML parser fails to parse (e.g., \u001C)
            ResultsAcceptHeader = "application/json"
        };

        _updateClient = new(clientFactory.CreateClient(ClientName.VirtuosoUpdateClient), new Uri(sparqlConfiguration.UpdateSparqlEndpoint));
        _sparqlConnector = new(_readClient, _updateClient);
        _logger = logger;
    }

    public async Task<SparqlResultSet> RunQueryAsync(SparqlParameterizedString query)
    {
        _logger.LogInformation("Running Set SPARQL query: \n{QueryText}", query.ToString());
        var sw = new Stopwatch();
        sw.Start();
        var result = await _readClient.QueryWithResultSetAsync(query.ToString());
        sw.Stop();
        var elapsed = sw.Elapsed;
        _logger.LogInformation("Finished SPARQL query ({elapsed}ms)", elapsed.TotalMilliseconds);
        return result;
    }

    public async Task RunUpdateAsync(SparqlParameterizedString query)
    {
        _logger.LogInformation("Running Update SPARQL query: \n{QueryText}", query.ToString());
        var sw = new Stopwatch();
        sw.Start();
        await _updateClient.UpdateAsync(query.ToString());
        sw.Stop();
        var elapsed = sw.Elapsed;
        _logger.LogInformation("Finished SPARQL query ({elapsed}ms)", elapsed.TotalMilliseconds);
    }

    public async Task<IGraph> RunGraphQueryAsync(SparqlParameterizedString query)
    {
        _logger.LogInformation("Running Graph SPARQL query: \n{QueryText}", query.ToString());
        var sw = new Stopwatch();
        sw.Start();
        var result = await _readClient.QueryWithResultGraphAsync(query.ToString());
        sw.Stop();
        var elapsed = sw.Elapsed;
        _logger.LogInformation("Finished SPARQL query ({elapsed}ms)", elapsed.TotalMilliseconds);
        return result;
    }

    public async Task<bool> HasGraphAsync(Uri graphUri)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                ASK WHERE {
                    GRAPH @graph {
                        ?s ?p ?o
                    }
                }
            "
        };

        query.SetAbsoluteUri("graph", graphUri);

        var result = await RunQueryAsync(query);

        return result.Result;
    }

    [Obsolete("Investigate if .AddGraphAsync does the job! If yes, remove this method.")]
    public async Task CreateGraphAsync(Uri graphUri)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                CREATE GRAPH @graph
            "
        };
        query.SetAbsoluteUri("graph", graphUri);

        _logger.LogInformation("Running CreateGraphAsync SPARQL query: \n{QueryText}", query.ToString());
        await _updateClient.UpdateAsync(query.ToString());
    }

    public async Task ClearGraphAsync(Uri graphUri)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                CLEAR GRAPH @graph
            "
        };
        query.SetAbsoluteUri("graph", graphUri);

        _logger.LogInformation("Running ClearGraphAsync SPARQL query: \n{QueryText}", query.ToString());
        await _updateClient.UpdateAsync(query.ToString());
    }

    public async Task<IGraph> GetGraph(Uri graphId)
    {
        var graph = new Graph
        {
            BaseUri = graphId
        };

        // Get the number of triples inside the graph
        var queryString = new SparqlParameterizedString
        {
            CommandText = "SELECT (COUNT(*) AS ?count) WHERE { GRAPH @graph { ?s ?p ?o } }"
        };
        queryString.SetAbsoluteUri("graph", graphId);
        var countResult = await RunQueryAsync(queryString);

        var literalNode = (ILiteralNode?)countResult.FirstOrDefault()?.Value("count");
        var nodeNumberOfResult = literalNode?.Value;

        if (!long.TryParse(nodeNumberOfResult, out var numberOfResult))
        {
            return graph;
        }

        // iterates over results because a query limit exists (pagination)
        for (var offset = 0; offset < numberOfResult; offset += QUERY_LIMIT)
        {
            queryString = new SparqlParameterizedString
            {
                CommandText = $"SELECT ?s ?p ?o WHERE {{ GRAPH @graph {{ ?s ?p ?o }} }} LIMIT {QUERY_LIMIT} OFFSET {offset}"
            };
            queryString.SetAbsoluteUri("graph", graphId);
            using var results = await RunQueryAsync(queryString);

            if (results is null)
            {
                continue;
            }

            foreach (var result in results)
            {
                graph.Assert(result.Value("s"), result.Value("p"), result.Value("o"));
            }
        }

        return graph;
    }

    // TODO: This seems like to be ill fitting here, move this to a fitting space
    public async Task<PagedEnumerable<DeployedGraph>> GetDeployedGraphsAsync(DeployedGraphParameters deployedGraphParameters)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT ?graph (GROUP_CONCAT(?fileHash; separator="";"") AS ?fileHashes)
                WHERE {
                    GRAPH ?graph {
                        @coscineGraphDeployed @coscineDeployedVersion ?fileHash
                    }
                }
                GROUP BY ?graph
            "
        };

        // Set namespaces, literals and URIs
        query.SetAbsoluteUri("coscineGraphDeployed", RdfUris.CoscineEntitiesGraphDeployed);
        query.SetAbsoluteUri("coscineDeployedVersion", RdfUris.CoscineTermsDeployedVersion);

        // Build and execute count query
        var count = await query.GetCountOrDefaultAsync(this);

        // Always sort before adding pagination, otherwise the query breaks!
        query.Sort<DeployedGraph>(deployedGraphParameters.OrderBy,
            new()
            {
                {"graph", dg => dg.Uri },
            }
        );
        query.Paginate(deployedGraphParameters.PageSize, deployedGraphParameters.PageNumber);
        var resultSet = await RunQueryAsync(query);

        // Extract the objects
        var deployedGraphs = resultSet
            .GroupBy(r => r.Value("graph").AsValuedNode().AsString())
            .Select(g => new DeployedGraph
            {
                Uri = new Uri(g.Key, UriKind.Absolute),
                FileHashes = g
                    .Select(r => r.Value("fileHashes").AsValuedNode().AsString())
                    .FirstOrDefault()
                    ?.Split(';') // Split the concatenated string into individual file hashes (see separator in query)
                    .Select(hash => hash.Trim())
            });

        return new PagedEnumerable<DeployedGraph>(deployedGraphs, count, deployedGraphParameters.PageNumber, deployedGraphParameters.PageSize);
    }

    public async Task AddGraphAsync(IGraph graph)
    {
        if (graph is IPatchGraph patchGraph)
        {
            AddToGraph(graph, patchGraph.AssertList.AbsoluteUrisForTriples());
            RemoveFromGraph(graph, patchGraph.RetractList.AbsoluteUrisForTriples());
        }
        else
        {
            var exists = await HasGraphAsync(graph.BaseUri);

            if (exists)
            {
                // Clear the existing graph from the store
                await ClearGraphAsync(graph.BaseUri);
            }

            AddToGraph(graph, graph.Triples.AbsoluteUrisForTriples());
        }
    }

    /// <summary>
    /// Adds the given triples to the specified graph in the metadata store.
    /// </summary>
    /// <param name="graph">The graph to which the triples are added.</param>
    /// <param name="triples">The collection of triples to add to the graph.</param>
    private void AddToGraph(IGraph graph, IEnumerable<Triple> triples)
    {
        var groupedTriples = triples.GroupTriplesByBlankNodes();
        var chunks = groupedTriples.ChunkGrouped(100).ToList(); // Convert to list to avoid multiple enumeration

        _logger.LogInformation("Adding triples to graph {GraphUri}. Total {ChunkCount} chunks, each with approximately 100 triples, based on the grouping.", graph.BaseUri, chunks.Count);

        // Deal with encoding issues
        var baseUri = graph.BaseUri is not AlwaysAbsoluteUri ? new AlwaysAbsoluteUri(graph.BaseUri.ToString()) : graph.BaseUri;

        // Chunking since the size otherwise can be too large
        foreach (var triplesChunk in chunks)
        {
            // Note: Avoid logging the content of each chunk to prevent excessive log size.
            _logger.LogInformation("Adding chunk with {TripleCount} triples to graph {GraphUri}", triplesChunk.Count, graph.BaseUri);

            // You have to write your own insert and retract statement,
            // if you want to use the async version of the update client.
            _sparqlConnector.UpdateGraph(baseUri, triplesChunk, []);
        }
    }

    /// <summary>
    /// Retracts the given triples from the specified graph in the metadata store.
    /// </summary>
    /// <param name="graph">The graph from which the triples are retracted.</param>
    /// <param name="triples">The collection of triples to remove from the graph.</param>
    public void RemoveFromGraph(IGraph graph, IEnumerable<Triple> triples)
    {
        var chunks = triples.Chunk(100).ToList(); // Convert to list to avoid multiple enumeration
        _logger.LogInformation("Removing triples from graph {GraphUri}. Total {ChunkCount} chunks, each with up to 100 triples.", graph.BaseUri, chunks.Count);

        // Deal with encoding issues
        var baseUri = graph.BaseUri is not AlwaysAbsoluteUri ? new AlwaysAbsoluteUri(graph.BaseUri.ToString()) : graph.BaseUri;

        // Chunking since the size otherwise can be too large
        foreach (var triplesChunk in chunks)
        {
            // Note: Avoid logging the content of each chunk to prevent excessive log size.
            _logger.LogInformation("Removing chunk with {TripleCount} triples from graph {GraphUri}", triplesChunk.Length, graph.BaseUri);

            // You have to write your own insert and retract statement,
            // if you want to use the async version of the update client.
            _sparqlConnector.UpdateGraph(baseUri, [], triplesChunk);
        }
    }

    public IGraph ParseGraph(string rdfData, RdfFormat rdfFormat, Uri? graphUri = null)
    {
        var contentType = rdfFormat.GetEnumMemberValue();

        var graph = graphUri is not null ? new Graph(graphUri) : new Graph();
        var rdfParser = MimeTypesHelper.GetParser(contentType);
        rdfParser.Load(graph, new StringReader(rdfData));
        if (graphUri is not null)
        {
            graph.BaseUri = graphUri;
        }
        return graph;
    }

    public ITripleStore ParseTripleStore(string rdfData, RdfFormat rdfFormat)
    {
        var contentType = rdfFormat.GetEnumMemberValue();

        var tripleStore = new TripleStore();
        var storeParser = MimeTypesHelper.GetStoreParser(contentType);
        tripleStore.LoadFromString(rdfData, storeParser);

        return tripleStore;
    }

    public string WriteGraph(IGraph graph, RdfFormat rdfFormat)
    {
        var contentType = rdfFormat.GetEnumMemberValue();

        switch (rdfFormat)
        {
            case RdfFormat.JsonLd:
            case RdfFormat.TriG:
                // JSON-LD & TriG not meant for a single graph
                var tripleStore = new TripleStore();
                tripleStore.Add(graph);
                var storeWriter = MimeTypesHelper.GetStoreWriter(contentType);
                return VDS.RDF.Writing.StringWriter.Write(tripleStore, storeWriter);

            case RdfFormat.Turtle:
                var turtleWriter = new CompressingTurtleWriter(WriterCompressionLevel.High);
                return VDS.RDF.Writing.StringWriter.Write(graph, turtleWriter);

            default:

                var rdfWriter = MimeTypesHelper.GetWriter(contentType);
                return VDS.RDF.Writing.StringWriter.Write(graph, rdfWriter);
        }
    }
}