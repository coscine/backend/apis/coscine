using Coscine.Api.Core.Repository.Contracts;

namespace Coscine.Api.Core.Repository;

public class DataStorageRepositoryFactory(IEnumerable<IDataStorageRepository> dataStorageRepositories) : IDataStorageRepositoryFactory
{
    public IDataStorageRepository? Resolve(string resourceTypeId)
    {
        return dataStorageRepositories.SingleOrDefault(x => x.TypeId.ToString() == resourceTypeId);
    }

    public IDataStorageRepository? Create(Guid resourceTypeId)
    {
        return Resolve(resourceTypeId.ToString());
    }
}
