﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class TermsOfServiceRepository(RepositoryContext repositoryContext) : ITermsOfServiceRepository
{
    private readonly RepositoryBase<Tosaccepted> _termsOfService = new(repositoryContext);

    public void Create(Tosaccepted termsOfService)
    {
        _termsOfService.Create(termsOfService);
    }

    public void Delete(Tosaccepted termsOfService)
    {
        _termsOfService.Delete(termsOfService);
    }

    public async Task<bool> HasAccepted(Guid userId, string version, bool trackChanges)
    {
        return await _termsOfService.FindByCondition(x => x.UserId == userId && x.Version == version, trackChanges).AnyAsync();
    }

    public async Task<IEnumerable<Tosaccepted>> GetAllAsync(
        Guid userId,
        TermsOfServiceParameters termsOfServiceParameters,
        bool trackChanges
    )
    {
        return await _termsOfService
            .FindByCondition(tos => userId == tos.UserId, trackChanges)
            .FilterByVersion(termsOfServiceParameters.Version)
            .SortTermsOfService(termsOfServiceParameters.OrderBy)
            .ToListAsync();
    }

    public async Task MergeAsync(Guid fromUserId, Guid toUserId)
    {
        foreach (var tos in await _termsOfService
                    .FindByCondition(x => x.UserId == fromUserId, trackChanges: true)
                    .ToListAsync())
        {
            // Make sure, that the tos are not duplicated
            if (await _termsOfService.FindByCondition(x => x.UserId == toUserId, trackChanges: false)
                    .AnyAsync())
            {
                // User is already part of the tos, delete old reference
                Delete(tos);
            }
            else
            {
                // Not already part of the tos
                tos.UserId = toUserId;
            }
        }
    }
}
