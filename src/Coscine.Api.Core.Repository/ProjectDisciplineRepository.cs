﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;

namespace Coscine.Api.Core.Repository;

public sealed class ProjectDisciplineRepository : IProjectDisciplineRepository
{
    private readonly RepositoryBase<ProjectDiscipline> _projectDiscipline;

    public ProjectDisciplineRepository(RepositoryContext repositoryContext)
    {
        _projectDiscipline = new(repositoryContext);
    }

    public void Delete(ProjectDiscipline projectDiscipline)
    {
        _projectDiscipline.Delete(projectDiscipline);
    }
}