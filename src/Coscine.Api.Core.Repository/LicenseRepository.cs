﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class LicenseRepository : ILicenseRepository
{
    private readonly RepositoryBase<License> _license;

    public LicenseRepository(RepositoryContext repositoryContext)
    {
        _license = new(repositoryContext);
    }

    public async Task<PagedEnumerable<License>> GetPagedAsync(LicenseParameters licenseParameters, bool trackChanges)
    {
        var licensesQuery = _license.FindAll(trackChanges);

        var disciplines = await licensesQuery
            .SortLicenses(licenseParameters.OrderBy)
            .Skip((licenseParameters.PageNumber - 1) * licenseParameters.PageSize)
            .Take(licenseParameters.PageSize)
            .ToListAsync();

        var count = await licensesQuery.CountAsync();

        return new PagedEnumerable<License>(disciplines, count, licenseParameters.PageNumber, licenseParameters.PageSize);
    }

    public async Task<License?> GetAsync(Guid licenseId, bool trackChanges)
    {
        return await _license.FindByCondition(x => x.Id == licenseId, trackChanges).SingleOrDefaultAsync();
    }
}