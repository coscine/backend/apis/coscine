﻿using Coscine.Api.Core.Entities.Models.QuadStore;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.Extensions.Logging;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Repository;

public class DataCatalogRepository(IStore store, ILogger<DataCatalogRepository> logger) : IDataCatalogRepository
{
    private readonly IStore _store = store;
    private readonly ILogger<DataCatalogRepository> _logger = logger;

    public DataCatalog? Get(Uri dataCatalogId)
    {
        try
        {
            // Load model
            var model = new Model(_store, new UriRef(dataCatalogId.AbsoluteUri));

            // Load Resource
            return model?.GetResource<DataCatalog>(dataCatalogId);
        }
        catch (Exception e)
        {
            _logger.LogWarning(e, "Exception while loading from the quad store");
            return null;
        }
    }
}
