﻿using Amazon.S3;
using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Repository.ResourceTypeDefinitions;

internal static class Helper
{
    public const string ECS_SIM_URL = "ecs-sim01.itc.rwth-aachen.de";

    public static HttpVerb GetVerb(CoscineHttpMethod httpMethod)
    {
        return httpMethod switch
        {
            CoscineHttpMethod.GET => HttpVerb.GET,
            CoscineHttpMethod.PUT => HttpVerb.PUT,
            CoscineHttpMethod.HEAD => HttpVerb.HEAD,
            CoscineHttpMethod.DELETE => HttpVerb.DELETE,
            _ => throw new ArgumentException($@"""{httpMethod}"" is not a valid option. Must be either CoscineHttpMethod.GET, CoscineHttpMethod.PUT, CoscineHttpMethod.HEAD or CoscineHttpMethod.DELETE.", nameof(httpMethod)),
        };
    }
}