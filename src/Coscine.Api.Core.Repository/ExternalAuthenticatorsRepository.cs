﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class ExternalAuthenticatorsRepository : IExternalAuthenticatorsRepository
{
    private readonly RepositoryBase<ExternalAuthenticator> _externalAuthenticator;

    public ExternalAuthenticatorsRepository(RepositoryContext repositoryContext)
    {
        _externalAuthenticator = new(repositoryContext);
    }

    public async Task<IEnumerable<ExternalAuthenticator>> GetAllAsync(ExternalAuthParameters externalAuthParameters, bool trackChanges)
    {
        return await _externalAuthenticator.FindAll(trackChanges)
            .SortTitles(externalAuthParameters.OrderBy)
            .ToListAsync();
    }

    public async Task<ExternalAuthenticator?> GetAsync(Guid externalAuthId, bool trackChanges)
    {
        return await _externalAuthenticator.FindByCondition(ea => externalAuthId == ea.Id, trackChanges)
            .SingleOrDefaultAsync();
    }
}