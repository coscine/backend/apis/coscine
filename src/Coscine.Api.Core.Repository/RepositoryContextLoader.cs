﻿using Coscine.Api.Core.Repository.Contracts;
using Microsoft.EntityFrameworkCore.Storage;

namespace Coscine.Api.Core.Repository;

public class RepositoryContextLoader(RepositoryContext repositoryContext) : IRepositoryContextLoader
{
    private readonly RepositoryContext _repositoryContext = repositoryContext;

    public virtual async Task SaveAsync() => await _repositoryContext.SaveChangesAsync();

    public virtual async Task<IDbContextTransaction> BeginTransactionAsync() => await _repositoryContext.Database.BeginTransactionAsync();
    
    public IExecutionStrategy CreateExecutionStrategy() => _repositoryContext.Database.CreateExecutionStrategy();
}