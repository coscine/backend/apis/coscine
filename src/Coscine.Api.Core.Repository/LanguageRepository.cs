﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class LanguageRepository : ILanguageRepository
{
    private readonly RepositoryBase<Language> _language;

    public LanguageRepository(RepositoryContext repositoryContext)
    {
        _language = new(repositoryContext);
    }

    public async Task<IEnumerable<Language>> GetAllAsync(LanguageParameters languageParameters, bool trackChanges)
    {
        return await _language.FindAll(trackChanges)
            .SortLanguages(languageParameters.OrderBy)
            .ToListAsync();
    }

    public async Task<Language?> GetAsync(Guid languageId, bool trackChanges)
    {
        return await _language.FindByCondition(l => languageId == l.Id, trackChanges)
            .SingleOrDefaultAsync();
    }
}