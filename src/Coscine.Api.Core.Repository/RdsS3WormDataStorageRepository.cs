using Amazon.S3;
using Amazon.S3.Model;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.ResourceTypeDefinitions;
using Coscine.Api.Core.Shared.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Coscine.Api.Core.Repository;

public class RdsS3WormRwthDataStorageRepository(IOptionsSnapshot<RdsS3WormResourceTypeConfiguration> rdsS3WormResourceTypeConfigurations,
                                      IRdsS3WormResourceTypeRepository rdsS3WormResourceTypeRepository,
                                      IHttpClientFactory httpClientFactory,
                                      ILogger<RdsS3WormDataStorageRepository> logger,
                                      IS3ClientFactory s3ClientFactory,
                                      IEcsManagerFactory ecsManagerFactory) : RdsS3WormDataStorageRepository(rdsS3WormResourceTypeRepository,
                                                                                         httpClientFactory,
                                                                                         logger,
                                                                                         s3ClientFactory, ecsManagerFactory)
{
    protected override string ConfigKey => "rdss3wormrwth";
    public override Guid TypeId => Configurations.ResourceTypeConfiguration.ResourceTypeRdss3Worm.Id;
    protected override RdsS3WormResourceTypeConfiguration RdsS3WormResourceTypeConfiguration => rdsS3WormResourceTypeConfigurations.Get(ConfigKey);
}

public abstract class RdsS3WormDataStorageRepository(IRdsS3WormResourceTypeRepository rdsS3ResourceTypeRepository,
                                        IHttpClientFactory httpClientFactory,
                                        ILogger<RdsS3WormDataStorageRepository> logger,
                                        IS3ClientFactory s3ClientFactory,
                                        IEcsManagerFactory ecsManagerFactory) : IDataStorageRepository
{
    public abstract Guid TypeId { get; }
    protected abstract RdsS3WormResourceTypeConfiguration RdsS3WormResourceTypeConfiguration { get; }
    protected abstract string ConfigKey { get; }
    public IEnumerable<Capability> Capabilities
    {
        get
        {
            yield return Capability.CanWrite;
            yield return Capability.CanRead;
            yield return Capability.CanUpdate;
            yield return Capability.CanDelete;
            yield return Capability.SupportsLocalMetadataCopy;
            yield return Capability.SupportsReadOnlyMode;
            yield return Capability.UsesQuota;
            yield return Capability.SupportsLinks;
        }
    }
    private readonly IRdsS3WormResourceTypeRepository _rdsS3WormResourceTypeRepository = rdsS3ResourceTypeRepository;
    private readonly IHttpClientFactory _httpClientFactory = httpClientFactory;
    private readonly ILogger<RdsS3WormDataStorageRepository> _logger = logger;
    private readonly IS3ClientFactory _s3ClientFactory = s3ClientFactory;

    public Task CreateAsync(Resource resource, CreateDataStorageParameters parameters)
    {
        throw new NotImplementedException();
    }

    public Task CreateAsync(CreateBlobParameters parameters)
    {
        throw new NotImplementedException();
    }

    public Task CreateAsync(CreateTreeParameters parameters)
    {
        throw new NotImplementedException();
    }

    public Task DeleteAsync(DeleteBlobParameters parameters)
    {
        throw new NotImplementedException();
    }

    public Task DeleteAsync(DeleteTreeParameters parameters)
    {
        throw new NotImplementedException();
    }

    public Task<ResourceTypeInformation> GetResourceTypeInformationAsync()
    {
        var resourceTypeInformation = new ResourceTypeInformation
        {
            CanCreate = false,
            CanRead = true,
            CanSetResourceReadonly = false,
            CanUpdate = false,
            CanUpdateResource = false,
            CanDelete = false,
            CanDeleteResource = false,
            CanList = true,
            CanCreateLinks = true,
            CanCopyLocalMetadata = false,
            IsArchived = false,
            IsQuotaAvailable = true,
            IsQuotaAdjustable = true,
            ResourceCreate = new ResourceCreate
            {
                Components =
                [
                    ["Size"],    // First step with "Size"
                    [],          // Second step
                    [],          // Third step
                    []           // Fourth step
                ]
            },
            ResourceContent = new ResourceContent
            {
                ReadOnly = false,
                MetadataView = new MetadataView
                {
                    EditableDataUrl = false,
                    EditableKey = false
                },
                EntriesView = new EntriesView
                {
                    Columns = new ColumnsObject
                    {
                        Always = ["name", "size"]
                    }
                }
            },
            Status = RdsS3WormResourceTypeConfiguration?.SpecificType?.Status ?? ResourceTypeStatus.Hidden,
            GeneralType = RdsS3WormResourceTypeConfiguration?.SpecificType?.Type,
            SpecificType = RdsS3WormResourceTypeConfiguration?.SpecificType?.SpecificTypeName
        };
        return Task.FromResult(resourceTypeInformation);
    }

    public IEnumerable<SpecificType> GetSpecificResourceTypes()
    {
        //TODO: this has to be moved
        return [RdsS3WormResourceTypeConfiguration.SpecificType];
    }

    /// <summary>
    /// Retrieves specific resource types with a particular status.
    /// </summary>
    /// <param name="status">The status of the resource types to retrieve.</param>
    /// <returns>An IEnumerable of specific resource types that match the provided status.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(ResourceTypeStatus status)
    {
        return GetSpecificResourceTypes().Where(st => st.Status == status);
    }

    /// <summary>
    /// Retrieves specific resource types that are supported by a particular organization.
    /// </summary>
    /// <param name="OrganizationRor">The ROR of the organization to match against the supported organizations of the resource types.</param>
    /// <returns>An IEnumerable of specific resource types that are supported by the organization specified by the provided ROR.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor)
    {
        return GetSpecificResourceTypes().Where(st => st.SupportedOrganizations is not null
            && (st.SupportedOrganizations.Contains(OrganizationRor) || st.SupportedOrganizations.Contains("*")));
    }

    /// <summary>
    /// Retrieves specific resource types that are supported by a particular organization and have a particular status.
    /// </summary>
    /// <param name="OrganizationRor">The ROR of the organization to match against the supported organizations of the resource types.</param>
    /// <param name="status">The status of the resource types to retrieve.</param>
    /// <returns>An IEnumerable of specific resource types that match the provided status and are supported by the organization specified by the provided ROR.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor, ResourceTypeStatus status)
    {
        return GetSpecificResourceTypes(OrganizationRor).Where(st => st.Status == status);
    }

    public async Task<DataStorageInfo> GetStorageInfoAsync(StorageInfoParameters parameters)
    {
        var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var rdsS3WormResourceType = await _rdsS3WormResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new RdsS3ResourceTypeNotFoundException(resourceTypeOptionId);

        var info = new DataStorageInfo
        {
            RdsS3StorageInfo = new RdsS3StorageInfo
            {
                AccessKeyRead = rdsS3WormResourceType.AccessKeyRead,
                AccessKeyWrite = rdsS3WormResourceType.AccessKeyWrite,
                BucketName = rdsS3WormResourceType.BucketName,
                Endpoint = rdsS3WormResourceType.Endpoint,
                SecretKeyRead = rdsS3WormResourceType.SecretKeyRead,
                SecretKeyWrite = rdsS3WormResourceType.AccessKeyWrite,
            }
        };

        if (parameters.FetchUsedSize)
        {
            info.UsedSize = 0;

            var rdsS3WormEcsManager = ecsManagerFactory.GetRdsS3WormEcsManager(ConfigKey);
            info.UsedSize = await rdsS3WormEcsManager.GetBucketTotalUsedQuota(parameters.Resource.Id.ToString());
        }

        if (parameters.FetchObjectCount)
        {

        }

        if (parameters.FetchAllocatedSize)
        {

            var rdsS3WormEcsManager = ecsManagerFactory.GetRdsS3WormEcsManager(ConfigKey);
            info.AllocatedSize = await rdsS3WormEcsManager.GetBucketQuota(parameters.Resource.Id.ToString()) * 1024 * 1024 * 1024;
        }

        return info;
    }

    public async Task<IEnumerable<StorageItemInfo>> ListAsync(ListTreeParameters parameters)
    {
        // List all objects
        var listRequest = new ListObjectsRequest
        {
            BucketName = parameters.Resource.Id.ToString(),
            Prefix = parameters.TreePath,
            Delimiter = "/"
        };

        var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var rdsS3WormResourceType = await _rdsS3WormResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new RdsS3ResourceTypeNotFoundException(resourceTypeOptionId);

        using var s3Client = _s3ClientFactory.GetClient(rdsS3WormResourceType);

        var entries = new List<StorageItemInfo>();

        ListObjectsResponse listResponse;
        do
        {
            // Get a list of objects
            listResponse = await s3Client.ListObjectsAsync(listRequest);

            foreach (var commonPrefix in listResponse.CommonPrefixes)
            {
                entries.Add(new StorageItemInfo
                {
                    Path = commonPrefix,
                    IsBlob = false,
                    ContentLength = 0,
                });

            }

            foreach (var obj in listResponse.S3Objects)
            {
                // Ensure not adding the same folder, as the prefix to the response
                // Happens, when an empty folder is added through and empty object with an delimiter as the last char of the key.
                if (!obj.Key.EndsWith('/') || obj.Size != 0 || obj.Key != parameters.TreePath)
                {
                    var entry = new StorageItemInfo
                    {
                        Path = obj.Key,
                        IsBlob = obj.Size > 0,
                        ContentLength = obj.Size,
                        CreatedDate = obj.LastModified,
                        LastModifiedDate = obj.LastModified,
                    };

                    if (parameters.GeneratePresignedLinks)
                    {
                        var expires = DateTime.UtcNow.AddHours(24);
                        var presignedUrl = s3Client.GetPreSignedURL(new GetPreSignedUrlRequest()
                        {
                            BucketName = parameters.Resource.Id.ToString(),
                            Key = obj.Key,
                            Verb = HttpVerb.GET,
                            Protocol = Protocol.HTTPS,
                            // For now, expiry of a day is set, but this might be up to debate
                            Expires = expires
                        });

                        entry.Links = new StorageLinks
                        {
                            Download = new InteractionLink
                            {
                                Url = presignedUrl,
                                Expiry = expires,
                                Method = CoscineHttpMethod.GET
                            }
                        };
                    }

                    entries.Add(entry);
                }
            }

            // Set the marker property
            listRequest.Marker = listResponse.NextMarker;

        } while (listResponse.IsTruncated);

        return entries;
    }

    public async Task<Stream?> ReadAsync(ReadBlobParameters parameters)
    {
        var url = await GetPreSignedUrl(parameters.Resource, parameters.Path, CoscineHttpMethod.GET);
        var responseMessage = await _httpClientFactory.CreateClient(ClientName.RdsS3Client).GetAsync(url, HttpCompletionOption.ResponseHeadersRead);

        if (responseMessage.IsSuccessStatusCode)
        {
            return await responseMessage.Content.ReadAsStreamAsync();
        }
        else
        {
            return null;
        }
    }

    public async Task<StorageItemInfo?> ReadAsync(ReadTreeParameters parameters)
    {
        var url = await GetPreSignedUrl(parameters.Resource, parameters.Path, CoscineHttpMethod.HEAD);
        var responseMessage = await _httpClientFactory.CreateClient(ClientName.RdsS3Client).SendAsync(new HttpRequestMessage(HttpMethod.Head, url));
        var contentLength = responseMessage.Content.Headers.ContentLength;

        if (!responseMessage.IsSuccessStatusCode)
        {
            return null;
        }

        if (!contentLength.HasValue)
        {
            throw new Exception("Content-Length could not be extracted.");
        }

        var entry = new StorageItemInfo
        {
            Path = parameters.Path,
            IsBlob = contentLength > 0,
            ContentLength = contentLength,
            CreatedDate = DateTime.Now,
            LastModifiedDate = DateTime.Now,
        };

        if (parameters.GeneratePresignedLinks)
        {
            var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
               ?? throw new ArgumentNullException();

            var rdsS3ResourceType = await _rdsS3WormResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                           ?? throw new RdsS3ResourceTypeNotFoundException(resourceTypeOptionId);

            using var s3Client = _s3ClientFactory.GetClient(rdsS3ResourceType);

            var expires = DateTime.UtcNow.AddHours(24);

            var presignedUrl = s3Client.GetPreSignedURL(new GetPreSignedUrlRequest()
            {
                BucketName = parameters.Resource.Id.ToString(),
                Key = parameters.Path,
                Verb = HttpVerb.GET,
                Protocol = Protocol.HTTPS,
                // For now, expiry of a day is set, but this might be up to debate
                Expires = expires
            });

            entry.Links = new StorageLinks
            {
                Download = new InteractionLink
                {
                    Url = presignedUrl,
                    Expiry = expires,
                    Method = CoscineHttpMethod.GET
                }
            };
        }
        return entry;
    }

    public Task UpdateAsync(Resource resource, UpdateDataStorageParameters parameters)
    {
        // TODO: Implement maintenance mode and readonly mode
        throw new NotImplementedException();
    }

    public Task UpdateAsync(UpdateBlobParameters parameters)
    {
        throw new NotImplementedException();
    }

    public Task UpdateAsync(UpdateTreeParameters parameters)
    {
        throw new NotImplementedException();
    }

    private async Task<Uri> GetPreSignedUrl(Resource resource, string key, CoscineHttpMethod httpVerb, Dictionary<string, string>? options = null)
    {

        var resourceTypeOptionId = resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var rdsS3WormResourceType = await _rdsS3WormResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new RdsS3ResourceTypeNotFoundException(resourceTypeOptionId);

        using var s3Client = _s3ClientFactory.GetClient(rdsS3WormResourceType);

        var presignedUrl = s3Client.GetPreSignedURL(new GetPreSignedUrlRequest()
        {
            BucketName = resource.Id.ToString(),
            Key = key,
            Verb = Helper.GetVerb(httpVerb),
            // The Simulator uses HTTP and the live system HTTPS
            Protocol = rdsS3WormResourceType.Endpoint.Contains(Helper.ECS_SIM_URL) ? Protocol.HTTP : Protocol.HTTPS,
            // For now, expiry of a day is set, but this might be up to debate
            Expires = DateTime.UtcNow.AddHours(24)
        });

        return new Uri(presignedUrl);
    }
}
