﻿using Coscine.Api.Core.Repository.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Coscine.Api.Core.Repository;

public class RepositoryBase<T> : IRepositoryBase<T> where T : class
{
    protected RepositoryContext RepositoryContext;

    public RepositoryBase(RepositoryContext repositoryContext)
    {
        RepositoryContext = repositoryContext;
    }

    public void Create(T entity)
    {
        RepositoryContext.Set<T>().Add(entity);
    }

    public void Delete(T entity)
    {
        RepositoryContext.Set<T>().Remove(entity);
    }

    public void DeleteRange(IEnumerable<T> entries)
    {
        RepositoryContext.Set<T>().RemoveRange(entries);
    }

    public IQueryable<T> FindAll(bool trackChanges)
    {
        return !trackChanges
            ? RepositoryContext
                .Set<T>()
                .AsNoTracking()
            : RepositoryContext.Set<T>();
    }

    public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression, bool trackChanges)
    {
        return !trackChanges
            ? RepositoryContext
                .Set<T>()
                .Where(expression)
                .AsNoTracking()
            : RepositoryContext
                .Set<T>()
                .Where(expression);
    }

    public void Update(T entity)
    {
        RepositoryContext.Set<T>().Update(entity);
    }
}