using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed partial class GitlabResourceTypeRepository(RepositoryContext repositoryContext) : IGitlabResourceTypeRepository
{
    private readonly RepositoryBase<GitlabResourceType> _gitlabResourceType = new(repositoryContext);

    public void Create(GitlabResourceType gitlabResourceType)
    {
        _gitlabResourceType.Create(gitlabResourceType);
    }

    public async Task<GitlabResourceType?> GetAsync(Guid gitlabResourceTypeId, bool trackChanges)
    {
        return await _gitlabResourceType.FindByCondition(x => x.Id == gitlabResourceTypeId, trackChanges)
            .SingleOrDefaultAsync();
    }
}