using System.Text.Json;
using System.Web;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Repository.Extensions.Utility;
using Coscine.Api.Core.Shared.RequestFeatures;
using Coscine.Api.Core.Shared.Helpers;
using Microsoft.Extensions.Options;

namespace Coscine.Api.Core.Repository;

/// <remarks>
/// NOC API Documentation available at https://noc-portal.rz.rwth-aachen.de/ticket/api-tokens, or under section <i>API</i> in the NOC-Portal. 
/// The same API, but public, is located under https://maintenance.itc.rwth-aachen.de/.
/// </remarks>
public sealed class NocRepository(IHttpClientFactory clientFactory, IOptionsMonitor<NocConfiguration> nocConfiguration) : INocRepository
{
    private readonly NocConfiguration _nocConfiguration = nocConfiguration.CurrentValue;

    private async Task<IEnumerable<NocTicket>> GetAllFromNocAsync()
    {
        var nocClient = clientFactory.CreateNocClient(_nocConfiguration);

        // Make the GET request
        var query = HttpUtility.ParseQueryString(string.Empty);
        query["ticket_queue_id"] = _nocConfiguration.TicketQueueId;
        var targetUri = nocClient.BaseAddress ?? _nocConfiguration.Endpoint;
        var uriBuilder = new UriBuilder(targetUri)
        {
            Path = Path.Combine(targetUri.PathAndQuery, "list_tickets"),
            Port = -1, // Explicitly set the port to -1 to avoid adding :443 to the URL
            Query = query.ToString()
        };

        var response = await nocClient.GetAsync(uriBuilder.ToString());
        if (!response.IsSuccessStatusCode)
        {
            // Throw an unhandled exception if the request was not successful
            throw new Exception("Failed to retrieve tickets from NOC-Portal. Check the credentials or API endpoints and try again.");
        }

        // Deserialize the response
        string responseBody = await response.Content.ReadAsStringAsync();
        var nocTickets = JsonSerializer.Deserialize<List<NocTicket>>(responseBody) ?? [];

        // Add the direct link to the ticket
        nocTickets.ForEach(t =>
        {
            var domain = new Uri (_nocConfiguration.Endpoint.GetLeftPart(UriPartial.Authority));
            // Href pattern: `{api_endpoint}/ticket/status/messages/{ticket_queue_id}/show_ticket/{ticket_id}`
            var href = domain.AppendToPath("ticket/status/messages", _nocConfiguration.TicketQueueId, "show_ticket", t.Id.ToString());
            t.Href = href;
        });
        return nocTickets;
    }

    public async Task<PagedEnumerable<NocTicket>> GetPagedFromNocAsync(MessageParameters messageParameters)
    {
        var tickets = await GetAllFromNocAsync();

        // Filter the tickets based on the message parameters
        tickets = tickets
            .TakeAfterStartDate(messageParameters.StartDateAfter)
            .TakeBeforeStartDate(messageParameters.StartDateBefore)
            .TakeAfterEndDate(messageParameters.EndDateAfter)
            .TakeBeforeEndDate(messageParameters.EndDateBefore)
            .FilterByMessageType(messageParameters.Type)
            .SearchNocTickets(messageParameters.SearchTerm)
            .SortNocTickets(messageParameters.OrderBy);

        // Paginate the results
        var offset = (messageParameters.PageNumber - 1) * messageParameters.PageSize;
        var pagedTickets = tickets.Skip(offset).Take(messageParameters.PageSize);

        return new PagedEnumerable<NocTicket>(pagedTickets, tickets.Count(), messageParameters.PageNumber, messageParameters.PageSize);
    }
}
