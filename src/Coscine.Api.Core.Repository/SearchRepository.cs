﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;
using Elasticsearch.Net;
using Microsoft.Extensions.Options;
using Nest;

namespace Coscine.Api.Core.Repository;

public sealed class SearchRepository : ISearchRepository
{
    private readonly ElasticClient _client;
    private readonly ElasticSearchConfiguration _elasticSearchConfiguration;

    public SearchRepository(IOptionsMonitor<ElasticSearchConfiguration> elasticSearchConfiguration)
    {
        _elasticSearchConfiguration = elasticSearchConfiguration.CurrentValue;
        var settings = new ConnectionSettings(_elasticSearchConfiguration.Endpoint)
           .DefaultIndex(_elasticSearchConfiguration.DefaultIndex)
           .EnableDebugMode()
           .DisableDirectStreaming(true); // Enable request and response streaming
        _client = new ElasticClient(settings);
    }

    public async Task<(IEnumerable<dynamic> searchResults, long totalCount)> SearchAsync(SearchParameters searchParameters, IEnumerable<Guid>? projectIds)
    {
        var searchResponse = await _client.SearchAsync<dynamic>(s => s
            .Size(searchParameters.PageSize)
            .From((searchParameters.PageNumber - 1) * searchParameters.PageSize)
            .Query(q => CreateQuery(searchParameters, searchParameters.Category, projectIds))
            .Sort(st => CreateSortQuery(st, searchParameters))
        );
        return (searchResults: searchResponse.Documents.ToList(), totalCount: searchResponse.Total);
    }

    /// <summary>
    /// Asynchronously counts the number of documents that match the given search parameters and category type, with optional project IDs for filtering.
    /// </summary>
    /// <param name="searchParameters">The search parameters to use for the count query.</param>
    /// <param name="categoryType">The category type to filter the count by.</param>
    /// <param name="projectIds">A collection of optional project IDs to further filter the count.</param>
    /// <returns>The total count of documents matching the criteria.</returns>
    public async Task<long> CountAsync(SearchParameters searchParameters, SearchCategoryType categoryType, IEnumerable<Guid>? projectIds)
    {
        var countResponse = await _client.CountAsync<dynamic>(c =>
            c.Query(q => CreateQuery(searchParameters, categoryType, projectIds))
        );

        return countResponse.Count;
    }

    /// <summary>
    /// Creates a query for ElasticSearch based on the specified search parameters, category type, and optional project IDs.
    /// </summary>
    /// <param name="searchParameters">The search parameters to use for creating the query.</param>
    /// <param name="categoryType">The category type to apply to the query.</param>
    /// <param name="projectIds">A collection of optional project IDs to include in the query.</param>
    /// <returns>A <see cref="QueryContainer"/> representing the ElasticSearch query.</returns>
    private static QueryContainer CreateQuery(SearchParameters searchParameters, SearchCategoryType categoryType, IEnumerable<Guid>? projectIds)
    {
        var projectUris = projectIds?.Select(p => $"{RdfUris.CoscineProjects}{p}") ?? new List<string>();

        var queryMust = new List<QueryContainer>();

        // Apply Search Category type
        if (categoryType != SearchCategoryType.None)
        {
            queryMust.Add(new MatchQuery()
            {
                Field = "structureType",
                Query = categoryType.ToString()
            });
        }

        // Apply Advanced or Simple Query
        if (searchParameters.UseAdvancedSyntax)
        {
            queryMust.Add(new QueryStringQuery()
            {
                Query = searchParameters.Query
                // Enables partial string search
                // Query = $"*{searchParameters.Query}*"
            });
        }
        else
        {
            queryMust.Add(new SimpleQueryStringQuery()
            {
                Query = searchParameters.Query
            });
        }

        var query = new BoolQuery
        {
            Must = queryMust,
            Filter = new QueryContainer[]
            {
                new BoolQuery
                {
                    Should = new QueryContainer[]
                    {
                        new TermsQuery
                        {
                            IsVerbatim = true,
                            Field = "belongsToProject",
                            Terms = projectUris
                        },
                        new TermQuery
                        {
                            IsVerbatim = true,
                            Field = "isPublic",
                            Value = true
                        }
                    }
                }
            }
        };
        return query;
    }

    /// <summary>
    /// Creates a sorting query for ElasticSearch based on the specified search parameters.
    /// </summary>
    /// <param name="st">The sort descriptor to be used for creating the sort query.</param>
    /// <param name="searchParameters">The search parameters to use for defining the sorting.</param>
    /// <returns>A <see cref="SortDescriptor{T}"/> representing the ElasticSearch sort query.</returns>
    private static SortDescriptor<dynamic> CreateSortQuery(SortDescriptor<dynamic> st, SearchParameters searchParameters)
    {
        var sortTokens = searchParameters?.OrderBy?.Split(',', StringSplitOptions.RemoveEmptyEntries);

        // Preflight check
        if (sortTokens is null
            || sortTokens.Length == 0
            || !sortTokens.Any(t => t.Split('_', StringSplitOptions.RemoveEmptyEntries).Length > 0
                && t.Split('_', StringSplitOptions.RemoveEmptyEntries).Length < 3))
        {
            st.Descending(SortSpecialField.Score.GetStringValue());
            return st;
        }

        foreach (var token in sortTokens)
        {
            var parts = token.Split('_', StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length > 0 && parts.Length < 3)
            {
                var sortOrder = SortOrder.Ascending;

                if (parts.Length == 2
                    && (string.Equals(parts[1], "asc", StringComparison.OrdinalIgnoreCase)
                    || string.Equals(parts[1], "desc", StringComparison.OrdinalIgnoreCase)))
                {
                    sortOrder = string.Equals(parts[1], "asc", StringComparison.OrdinalIgnoreCase) ? SortOrder.Ascending : SortOrder.Descending;
                }

                if (string.Equals(parts[0], "name", StringComparison.OrdinalIgnoreCase))
                {
                    st.Script(s => s.Script(x => x.Source("if (doc['fileName.keyword'].size() != 0) { doc['fileName.keyword'].value.toLowerCase()} else { doc['title.keyword'].value.toLowerCase()}")).Type("string").Order(sortOrder));
                }
                else
                {
                    if (string.Equals(parts[0], "score", StringComparison.OrdinalIgnoreCase))
                    {
                        parts[0] = SortSpecialField.Score.GetStringValue();
                    }
                    else
                    {
                        parts[0] = $"{parts[0].ToLower()}_created";
                    }

                    st.Field(f => f.Field(parts[0]).Order(sortOrder));
                }
            }
        }

        return st;
    }
}