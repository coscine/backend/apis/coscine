using Amazon.S3;
using Amazon.S3.Model;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.EmbeddedResources;
using Coscine.Api.Core.Repository.ResourceTypeDefinitions;
using Coscine.Api.Core.Shared.Enums;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Coscine.Api.Core.Repository;

public class RdsRwthDataStorageRepository(IOptionsSnapshot<RdsResourceTypeConfiguration> rdsResourceTypeConfigurations,
                                      IRdsResourceTypeRepository rdsResourceTypeRepository,
                                      IHttpClientFactory httpClientFactory,
                                      ILogger<RdsDataStorageRepository> logger,
                                      IS3ClientFactory s3ClientFactory,
                                      IEcsManagerFactory ecsManagerFactory) : RdsDataStorageRepository(rdsResourceTypeRepository,
                                                                                         httpClientFactory,
                                                                                         logger,
                                                                                         s3ClientFactory, ecsManagerFactory)
{
    protected override string ConfigKey => "rdsrwth";
    public override Guid TypeId => Configurations.ResourceTypeConfiguration.ResourceTypeRds.Id;
    protected override RdsResourceTypeConfiguration RdsResourceTypeConfiguration => rdsResourceTypeConfigurations.Get(ConfigKey);
}

public class RdsNrwDataStorageRepository(IOptionsSnapshot<RdsResourceTypeConfiguration> rdsResourceTypeConfigurations,
                                      IRdsResourceTypeRepository rdsResourceTypeRepository,
                                      IHttpClientFactory httpClientFactory,
                                      ILogger<RdsDataStorageRepository> logger,
                                      IS3ClientFactory s3ClientFactory,
                                      IEcsManagerFactory ecsManagerFactory) : RdsDataStorageRepository(rdsResourceTypeRepository,
                                                                                         httpClientFactory,
                                                                                         logger,
                                                                                         s3ClientFactory, ecsManagerFactory)
{
    protected override string ConfigKey => "rdsnrw";
    public override Guid TypeId => Configurations.ResourceTypeConfiguration.ResourceTypeRdsNrw.Id;
    protected override RdsResourceTypeConfiguration RdsResourceTypeConfiguration => rdsResourceTypeConfigurations.Get(ConfigKey);
}

public class RdsUdeDataStorageRepository(IOptionsSnapshot<RdsResourceTypeConfiguration> rdsResourceTypeConfigurations,
                                      IRdsResourceTypeRepository rdsResourceTypeRepository,
                                      IHttpClientFactory httpClientFactory,
                                      ILogger<RdsDataStorageRepository> logger,
                                      IS3ClientFactory s3ClientFactory,
                                      IEcsManagerFactory ecsManagerFactory) : RdsDataStorageRepository(rdsResourceTypeRepository,
                                                                                         httpClientFactory,
                                                                                         logger,
                                                                                         s3ClientFactory, ecsManagerFactory)
{
    protected override string ConfigKey => "rdsude";
    public override Guid TypeId => Configurations.ResourceTypeConfiguration.ResourceTypeRdsUde.Id;
    protected override RdsResourceTypeConfiguration RdsResourceTypeConfiguration => rdsResourceTypeConfigurations.Get(ConfigKey);
}

public class RdsTudoDataStorageRepository(IOptionsSnapshot<RdsResourceTypeConfiguration> rdsResourceTypeConfigurations,
                                      IRdsResourceTypeRepository rdsResourceTypeRepository,
                                      IHttpClientFactory httpClientFactory,
                                      ILogger<RdsDataStorageRepository> logger,
                                      IS3ClientFactory s3ClientFactory,
                                      IEcsManagerFactory ecsManagerFactory) : RdsDataStorageRepository(rdsResourceTypeRepository,
                                                                                         httpClientFactory,
                                                                                         logger,
                                                                                         s3ClientFactory, ecsManagerFactory)
{
    protected override string ConfigKey => "rdstudo";
    public override Guid TypeId => Configurations.ResourceTypeConfiguration.ResourceTypeRdsTudo.Id;
    protected override RdsResourceTypeConfiguration RdsResourceTypeConfiguration => rdsResourceTypeConfigurations.Get(ConfigKey);
}

public class RdsRubDataStorageRepository(IOptionsSnapshot<RdsResourceTypeConfiguration> rdsResourceTypeConfigurations,
                                      IRdsResourceTypeRepository rdsResourceTypeRepository,
                                      IHttpClientFactory httpClientFactory,
                                      ILogger<RdsDataStorageRepository> logger,
                                      IS3ClientFactory s3ClientFactory,
                                      IEcsManagerFactory ecsManagerFactory) : RdsDataStorageRepository(rdsResourceTypeRepository,
                                                                                         httpClientFactory,
                                                                                         logger,
                                                                                         s3ClientFactory, ecsManagerFactory)
{
    protected override string ConfigKey => "rdsrub";
    public override Guid TypeId => Configurations.ResourceTypeConfiguration.ResourceTypeRdsRub.Id;
    protected override RdsResourceTypeConfiguration RdsResourceTypeConfiguration => rdsResourceTypeConfigurations.Get(ConfigKey);
}

public abstract class RdsDataStorageRepository(IRdsResourceTypeRepository rdsResourceTypeRepository,
                                        IHttpClientFactory httpClientFactory,
                                        ILogger<RdsDataStorageRepository> logger,
                                        IS3ClientFactory s3ClientFactory,
                                        IEcsManagerFactory ecsManagerFactory) : IDataStorageRepository
{
    public abstract Guid TypeId { get; }
    protected abstract RdsResourceTypeConfiguration RdsResourceTypeConfiguration { get; }
    protected abstract string ConfigKey { get; }
    public IEnumerable<Capability> Capabilities
    {
        get
        {
            yield return Capability.CanWrite;
            yield return Capability.CanRead;
            yield return Capability.CanUpdate;
            yield return Capability.CanDelete;
            yield return Capability.SupportsLocalMetadataCopy;
            yield return Capability.SupportsReadOnlyMode;
            yield return Capability.UsesQuota;
            yield return Capability.SupportsLinks;
        }
    }
    private readonly IRdsResourceTypeRepository _rdsResourceTypeRepository = rdsResourceTypeRepository;
    private readonly IHttpClientFactory _httpClientFactory = httpClientFactory;
    private readonly ILogger<RdsDataStorageRepository> _logger = logger;
    private readonly IS3ClientFactory _s3ClientFactory = s3ClientFactory;

    public async Task CreateAsync(Resource resource, CreateDataStorageParameters parameters)
    {
        ArgumentNullException.ThrowIfNull(parameters.Quota);

        var rdsResourceType = new RdsResourceType
        {
            Id = Guid.NewGuid(), // Set the ID manually, object does not exist in the db until saved (even after the .Create())
            BucketName = resource.Id.ToString(),
            AccessKey = RdsResourceTypeConfiguration.AccessKey!,
            SecretKey = RdsResourceTypeConfiguration.SecretKey!,
            Endpoint = RdsResourceTypeConfiguration.Endpoint!
        };

        _rdsResourceTypeRepository.Create(rdsResourceType);
        resource.ResourceTypeOptionId = rdsResourceType.Id;

        var rdsEcsManager = ecsManagerFactory.GetRdsEcsManager(ConfigKey);

        await rdsEcsManager.CreateBucket(resource.Id.ToString(), parameters.Quota.Value);

        // Use the internal function to skip loading the data, that is not yet in the db
        await UpdateInternalAsync(resource, rdsResourceType, new UpdateDataStorageParameters { ReadOnly = false });
    }

    public async Task CreateAsync(CreateBlobParameters parameters)
    {
        var url = await GetPreSignedUrl(parameters.Resource, parameters.Path, CoscineHttpMethod.PUT);
        var response = await _httpClientFactory.CreateClient(ClientName.RdsS3Client).PutAsync(url, new StreamContent(parameters.Blob));

        if (!response.IsSuccessStatusCode)
        {
            throw new AggregateException();
        }
    }

    public Task CreateAsync(CreateTreeParameters parameters)
    {
        throw new NotImplementedException();
    }

    public async Task DeleteAsync(DeleteBlobParameters parameters)
    {
        var url = await GetPreSignedUrl(parameters.Resource, parameters.Path, CoscineHttpMethod.DELETE);
        var responseMessage = await _httpClientFactory.CreateClient(ClientName.RdsS3Client).DeleteAsync(url);
        responseMessage.Content.ReadAsStream();
    }

    public Task DeleteAsync(DeleteTreeParameters parameters)
    {
        throw new NotImplementedException();
    }

    public Task<ResourceTypeInformation> GetResourceTypeInformationAsync()
    {
        var resourceTypeInformation = new ResourceTypeInformation
        {
            CanCreate = true,
            CanRead = true,
            CanSetResourceReadonly = true,
            CanUpdate = true,
            CanUpdateResource = false,
            CanDelete = true,
            CanDeleteResource = false,
            CanList = true,
            CanCreateLinks = true,
            CanCopyLocalMetadata = true,
            IsArchived = false,
            IsQuotaAvailable = true,
            IsQuotaAdjustable = true,
            ResourceCreate = new ResourceCreate
            {
                Components =
                [
                    ["Size"],    // First step with "Size"
                    [],          // Second step
                    [],          // Third step
                    []           // Fourth step
                ]
            },
            ResourceContent = new ResourceContent
            {
                ReadOnly = false,
                MetadataView = new MetadataView
                {
                    EditableDataUrl = false,
                    EditableKey = false
                },
                EntriesView = new EntriesView
                {
                    Columns = new ColumnsObject
                    {
                        Always = ["name", "size"]
                    }
                }
            },
            Status = RdsResourceTypeConfiguration?.SpecificType?.Status ?? ResourceTypeStatus.Hidden,
            GeneralType = RdsResourceTypeConfiguration?.SpecificType?.Type,
            SpecificType = RdsResourceTypeConfiguration?.SpecificType?.SpecificTypeName
        };
        return Task.FromResult(resourceTypeInformation);
    }

    public IEnumerable<SpecificType> GetSpecificResourceTypes()
    {
        //TODO: this has to be moved
        return [RdsResourceTypeConfiguration.SpecificType];
    }

    /// <summary>
    /// Retrieves specific resource types with a particular status.
    /// </summary>
    /// <param name="status">The status of the resource types to retrieve.</param>
    /// <returns>An IEnumerable of specific resource types that match the provided status.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(ResourceTypeStatus status)
    {
        return GetSpecificResourceTypes().Where(st => st.Status == status);
    }

    /// <summary>
    /// Retrieves specific resource types that are supported by a particular organization.
    /// </summary>
    /// <param name="OrganizationRor">The ROR of the organization to match against the supported organizations of the resource types.</param>
    /// <returns>An IEnumerable of specific resource types that are supported by the organization specified by the provided ROR.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor)
    {
        return GetSpecificResourceTypes().Where(st => st.SupportedOrganizations is not null
            && (st.SupportedOrganizations.Contains(OrganizationRor) || st.SupportedOrganizations.Contains("*")));
    }

    /// <summary>
    /// Retrieves specific resource types that are supported by a particular organization and have a particular status.
    /// </summary>
    /// <param name="OrganizationRor">The ROR of the organization to match against the supported organizations of the resource types.</param>
    /// <param name="status">The status of the resource types to retrieve.</param>
    /// <returns>An IEnumerable of specific resource types that match the provided status and are supported by the organization specified by the provided ROR.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor, ResourceTypeStatus status)
    {
        return GetSpecificResourceTypes(OrganizationRor).Where(st => st.Status == status);
    }

    public async Task<DataStorageInfo> GetStorageInfoAsync(StorageInfoParameters parameters)
    {
        var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var rdsResourceType = await _rdsResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new RdsResourceTypeNotFoundException(resourceTypeOptionId);

        var info = new DataStorageInfo
        {
            RdsStorageInfo = new RdsStorageInfo
            {
                BucketName = rdsResourceType.BucketName,
                Endpoint = rdsResourceType.Endpoint,
            }
        };

        if (parameters.FetchUsedSize)
        {
            info.UsedSize = 0;

            var rdsEcsManager = ecsManagerFactory.GetRdsEcsManager(ConfigKey);
            info.UsedSize = await rdsEcsManager.GetBucketTotalUsedQuota(parameters.Resource.Id.ToString());
        }

        if (parameters.FetchObjectCount)
        {

        }

        if (parameters.FetchAllocatedSize)
        {

            var rdsEcsManager = ecsManagerFactory.GetRdsEcsManager(ConfigKey);
            info.AllocatedSize = await rdsEcsManager.GetBucketQuota(parameters.Resource.Id.ToString()) * 1024 * 1024 * 1024;
        }

        return info;
    }

    public async Task<IEnumerable<StorageItemInfo>> ListAsync(ListTreeParameters parameters)
    {
        // List all objects
        var listRequest = new ListObjectsRequest
        {
            BucketName = parameters.Resource.Id.ToString(),
            Prefix = parameters.TreePath,
            Delimiter = "/"
        };

        var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var rdsResourceType = await _rdsResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new RdsResourceTypeNotFoundException(resourceTypeOptionId);


        using var s3Client = _s3ClientFactory.GetClient(rdsResourceType);

        var entries = new List<StorageItemInfo>();

        ListObjectsResponse listResponse;
        do
        {
            // Get a list of objects
            listResponse = await s3Client.ListObjectsAsync(listRequest);

            foreach (var commonPrefix in listResponse.CommonPrefixes)
            {
                entries.Add(new StorageItemInfo
                {
                    Path = commonPrefix,
                    IsBlob = false,
                    ContentLength = 0,
                });

            }

            foreach (var obj in listResponse.S3Objects)
            {
                // Ensure not adding the same folder, as the prefix to the response
                // Happens, when an empty folder is added through and empty object with an delimiter as the last char of the key.
                if (!obj.Key.EndsWith('/') || obj.Size != 0 || obj.Key != parameters.TreePath)
                {

                    var entry = new StorageItemInfo
                    {
                        Path = obj.Key,
                        IsBlob = obj.Size > 0,
                        ContentLength = obj.Size,
                        CreatedDate = obj.LastModified,
                        LastModifiedDate = obj.LastModified,
                    };

                    if (parameters.GeneratePresignedLinks)
                    {
                        var expires = DateTime.UtcNow.AddHours(24);
                        var presignedUrl = s3Client.GetPreSignedURL(new GetPreSignedUrlRequest()
                        {
                            BucketName = parameters.Resource.Id.ToString(),
                            Key = obj.Key,
                            Verb = HttpVerb.GET,
                            Protocol = Protocol.HTTPS,
                            // For now, expiry of a day is set, but this might be up to debate
                            Expires = expires
                        });

                        entry.Links = new StorageLinks
                        {
                            Download = new InteractionLink
                            {
                                Url = presignedUrl,
                                Expiry = expires,
                                Method = CoscineHttpMethod.GET
                            }
                        };
                    }

                    entries.Add(entry);
                }
            }

            // Set the marker property
            listRequest.Marker = listResponse.NextMarker;

        } while (listResponse.IsTruncated);

        return entries;
    }

    public async Task<Stream?> ReadAsync(ReadBlobParameters parameters)
    {
        var url = await GetPreSignedUrl(parameters.Resource, parameters.Path, CoscineHttpMethod.GET);
        var responseMessage = await _httpClientFactory.CreateClient(ClientName.RdsS3Client).GetAsync(url, HttpCompletionOption.ResponseHeadersRead);

        if (responseMessage.IsSuccessStatusCode)
        {
            return await responseMessage.Content.ReadAsStreamAsync();
        }
        else
        {
            return null;
        }
    }

    public async Task<StorageItemInfo?> ReadAsync(ReadTreeParameters parameters)
    {
        var url = await GetPreSignedUrl(parameters.Resource, parameters.Path, CoscineHttpMethod.HEAD);
        var responseMessage = await _httpClientFactory.CreateClient(ClientName.RdsS3Client).SendAsync(new HttpRequestMessage(HttpMethod.Head, url));
        var contentLength = responseMessage.Content.Headers.ContentLength;

        if (!responseMessage.IsSuccessStatusCode)
        {
            return null;
        }

        if (!contentLength.HasValue)
        {
            throw new Exception("Content-Length could not be extracted.");
        }

        var entry = new StorageItemInfo
        {
            Path = parameters.Path,
            IsBlob = contentLength > 0,
            ContentLength = contentLength,
            CreatedDate = DateTime.Now,
            LastModifiedDate = DateTime.Now,
        };

        if (parameters.GeneratePresignedLinks)
        {
            var resourceTypeOptionId = parameters.Resource.ResourceTypeOptionId
               ?? throw new ArgumentNullException();

            var rdsResourceType = await _rdsResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                           ?? throw new RdsResourceTypeNotFoundException(resourceTypeOptionId);

            using var s3Client = _s3ClientFactory.GetClient(rdsResourceType);

            var expires = DateTime.UtcNow.AddHours(24);

            var presignedUrl = s3Client.GetPreSignedURL(new GetPreSignedUrlRequest()
            {
                BucketName = parameters.Resource.Id.ToString(),
                Key = parameters.Path,
                Verb = HttpVerb.GET,
                Protocol = Protocol.HTTPS,
                // For now, expiry of a day is set, but this might be up to debate
                Expires = expires
            });

            entry.Links = new StorageLinks
            {
                Download = new InteractionLink
                {
                    Url = presignedUrl,
                    Expiry = expires,
                    Method = CoscineHttpMethod.GET
                }
            };
        }
        return entry;
    }

    // Used to skip loading the config from the db
    private async Task UpdateInternalAsync(Resource resource, RdsResourceType rdsResourceType, UpdateDataStorageParameters parameters)
    {
        using var s3Client = _s3ClientFactory.GetClient(rdsResourceType);

        // Maintenance mode check
        if (parameters.MaintenanceMode is true)
        {
            var policy = GenerateMaintenanceModePolicy(rdsResourceType.AccessKey, resource.Id.ToString());
            var putRequest = new PutBucketPolicyRequest
            {
                BucketName = resource.Id.ToString(),
                Policy = policy
            };

            try
            {
                _logger.LogDebug("Updating bucket {resourceId} with maintenance mode policy:\n{policy}",
                    resource.Id.ToString(), policy);
                await s3Client.PutBucketPolicyAsync(putRequest);
            }
            catch (Exception exception)
            {
                _logger.LogWarning(exception, "Failed while updating the bucket policy for maintenance mode.");
            }
        }
        else
        {
            // Not in maintenance mode, fall back to possible read-only logic
            if (parameters.ReadOnly is not null)
            {
                var policy = GenerateReadonlyAccessPolicy(
                    rdsResourceType.AccessKey,
                    resource.Id.ToString(),
                    parameters.ReadOnly.Value
                );

                var putRequest = new PutBucketPolicyRequest
                {
                    BucketName = resource.Id.ToString(),
                    Policy = policy
                };

                try
                {
                    _logger.LogDebug("Updating bucket {resourceId} with policy: \n{policy}",
                        resource.Id.ToString(), policy);
                    await s3Client.PutBucketPolicyAsync(putRequest);
                }
                catch (Exception exception)
                {
                    _logger.LogWarning(exception, "Silent fail while updating the read-only policy.");
                }
            }
        }

        if (parameters.Quota is not null)
        {
            var rdsEcsManager = ecsManagerFactory.GetRdsEcsManager(ConfigKey);
            await rdsEcsManager.SetBucketQuota(resource.Id.ToString(), parameters.Quota.Value);
        }

        var putLifecycleRequest = new PutLifecycleConfigurationRequest
        {
            BucketName = resource.Id.ToString(),
            Configuration = GenerateLifecycleConfiguration()
        };

        try
        {
            _logger.LogDebug("Applying lifecycle configuration to bucket {resourceId} with configuration: \n{configuration}",
                resource.Id.ToString(), putLifecycleRequest.Configuration);
            await s3Client.PutLifecycleConfigurationAsync(putLifecycleRequest);
        }
        catch (Exception exception)
        {
            _logger.LogWarning(exception, "Silent fail while updating the lifecycle policy.");
        }
    }

    public async Task UpdateAsync(Resource resource, UpdateDataStorageParameters parameters)
    {
        var resourceTypeOptionId = resource.ResourceTypeOptionId
            ?? throw new ArgumentNullException();

        var rdsResourceType = await _rdsResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
            ?? throw new RdsResourceTypeNotFoundException(resourceTypeOptionId);

        await UpdateInternalAsync(resource, rdsResourceType, parameters);
    }

    public async Task UpdateAsync(UpdateBlobParameters parameters)
    {
        var url = await GetPreSignedUrl(parameters.Resource, parameters.Path, CoscineHttpMethod.PUT);
        var response = await _httpClientFactory.CreateClient(ClientName.RdsS3Client).PutAsync(url, new StreamContent(parameters.Blob));

        if (!response.IsSuccessStatusCode)
        {
            throw new AggregateException();
        }
    }

    public Task UpdateAsync(UpdateTreeParameters parameters)
    {
        throw new NotImplementedException();
    }

    private async Task<Uri> GetPreSignedUrl(Resource resource, string key, CoscineHttpMethod httpVerb, Dictionary<string, string>? options = null)
    {
        var resourceTypeOptionId = resource.ResourceTypeOptionId
           ?? throw new ArgumentNullException();

        var rdsResourceType = await _rdsResourceTypeRepository.GetAsync(resourceTypeOptionId, trackChanges: false)
                       ?? throw new RdsResourceTypeNotFoundException(resourceTypeOptionId);

        using var s3Client = _s3ClientFactory.GetClient(rdsResourceType);

        var presignedUrl = s3Client.GetPreSignedURL(new GetPreSignedUrlRequest()
        {
            BucketName = resource.Id.ToString(),
            Key = key,
            Verb = Helper.GetVerb(httpVerb),
            // The Simulator uses HTTP and the live system HTTPS
            Protocol = rdsResourceType.Endpoint.Contains(Helper.ECS_SIM_URL) ? Protocol.HTTP : Protocol.HTTPS,
            // For now, expiry of a day is set, but this might be up to debate
            Expires = DateTime.UtcNow.AddHours(24)
        });

        return new Uri(presignedUrl);
    }

    /// <summary>
    /// Generates a policy for enabling maintenance mode on an S3 bucket. 
    /// This policy denies all S3 actions for the specified principal.
    /// </summary>
    /// <param name="accessKey">The access key of the principal to deny access.</param>
    /// <param name="bucketName">The name of the S3 bucket.</param>
    /// <returns>A JSON string representing the maintenance mode policy.</returns>
    private static string GenerateMaintenanceModePolicy(string accessKey, string bucketName)
    {
        // Deny *all* S3 actions for everyone
        var policy = new Amazon.Auth.AccessControlPolicy.Policy
        {
            Id = "MaintenanceModePolicy",
            Version = "2012-10-17",
            Statements = [
                new(Amazon.Auth.AccessControlPolicy.Statement.StatementEffect.Deny)
                {
                    Id = "DenyAll",
                    Principals = [ new(accessKey) ],
                    Actions = [ new("s3:*") ],
                    Resources = [ new($"arn:aws:s3:::{bucketName}"), new($"arn:aws:s3:::{bucketName}/*") ]
                }
            ]
        };
        return policy.ToJson();
    }

    /// <summary>
    /// Generates a policy for read-only or write access to an S3 bucket.
    /// The policy is adjusted based on the provided <paramref name="isReadonly"/> flag.
    /// </summary>
    /// <param name="accessKey">The access key of the principal to grant or deny access.</param>
    /// <param name="bucketname">The name of the S3 bucket.</param>
    /// <param name="isReadonly">
    /// A flag indicating whether the policy should provide read-only access (<c>true</c>) or write access (<c>false</c>).
    /// </param>
    /// <returns>A JSON string representing the generated access policy.</returns>
    /// <exception cref="Exception">Thrown when the policy JSON cannot be parsed.</exception>
    private static string GenerateReadonlyAccessPolicy(string accessKey, string bucketname, bool isReadonly)
    {
        if (isReadonly)
        {
            var json = EmbeddedResourceLoader.ReadResource("Rds.BucketReadPolicy.json");
            var jObject = JsonConvert.DeserializeObject<JObject>(json)
                ?? throw new Exception("BucketReadPolicy.json could not be parsed.");
            {
                var allowStatement = jObject["Statement"]?.First(x => x["Sid"]?.ToString() == "AllowStatement") as JObject;
                (allowStatement?["Resource"] as JArray)?.Add($"{bucketname}/*");
                (allowStatement?["Principal"] as JArray)?.Add($"{accessKey}");
            }
            {
                var denyStatement = jObject["Statement"]?.First(x => x["Sid"]?.ToString() == "DenyStatement") as JObject;
                (denyStatement?["Resource"] as JArray)?.Add($"{bucketname}/*");
                (denyStatement?["Principal"] as JArray)?.Add($"{accessKey}");
            }

            return jObject.ToString();
        }
        else
        {
            var json = EmbeddedResourceLoader.ReadResource("Rds.BucketWritePolicy.json");
            var jObject = JsonConvert.DeserializeObject<JObject>(json)
                ?? throw new Exception("BucketWritePolicy.json could not be parsed.");
            {
                var allowStatement = jObject["Statement"]?.First(x => x["Sid"]?.ToString() == "AllowStatement") as JObject;
                (allowStatement?["Resource"] as JArray)?.Add($"{bucketname}/*");
                (allowStatement?["Principal"] as JArray)?.Add($"{accessKey}");
            }
            {
                var denyStatement = jObject["Statement"]?.First(x => x["Sid"]?.ToString() == "DenyStatement") as JObject;
                (denyStatement?["Resource"] as JArray)?.Add($"{bucketname}/*");
            }

            return jObject.ToString();
        }
    }

    private static LifecycleConfiguration GenerateLifecycleConfiguration() => new()
    {
        Rules = [
            new LifecycleRule
                {
                    Id = "AbortIncompleteMultipartUploads",
                    Status = LifecycleRuleStatus.Enabled,
                    AbortIncompleteMultipartUpload = new LifecycleRuleAbortIncompleteMultipartUpload
                    {
                        DaysAfterInitiation = 7
                    },
                    Filter = new () {
                        LifecycleFilterPredicate = new LifecyclePrefixPredicate
                        {
                            Prefix = "" // Applies the rule to all objects in the bucket
                        }
                    }
                }
        ]
    };

}
