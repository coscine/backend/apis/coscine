﻿using Coscine.Api.Core.Entities.Models.QuadStore;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.Extensions.Logging;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Repository;

public class RawDataEntityRepository(IStore store, ILogger<RawDataEntityRepository> logger) : IRawDataEntityRepository
{
    private readonly IStore _store = store;
    private readonly ILogger<RawDataEntityRepository> _logger = logger;

    public RawDataEntity? Get(Uri rawDataEntityId)
    {
        try
        {
            // Load model
            var model = new Model(_store, new UriRef(rawDataEntityId.AbsoluteUri));

            // Load Resource
            return model?.GetResource<RawDataEntity>(rawDataEntityId);
        }
        catch (Exception e)
        {
            _logger.LogWarning(e, "Exception while loading from the quad store");
            return null;
        }
    }
}
