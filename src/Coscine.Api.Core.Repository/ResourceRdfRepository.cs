﻿using Coscine.Api.Core.Entities.Models.QuadStore;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.Extensions.Logging;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Repository;

public class ResourceRdfRepository(IStore store, ILogger<ResourceRdfRepository> logger) : IResourceRdfRepository
{
    private readonly IStore _store = store;
    private readonly ILogger<ResourceRdfRepository> _logger = logger;

    public ResourceRdf? Get(Uri resourceRdfId)
    {
        try
        {
            // Load model
            var model = new Model(_store, new UriRef(resourceRdfId.AbsoluteUri));

            // Load Resource
            return model?.GetResource<ResourceRdf>(resourceRdfId);
        }
        catch (Exception e)
        {
            _logger.LogWarning(e, "Exception while loading from the quad store");
            return null;
        }
    }

    public ResourceRdf? Get(Guid resourceId)
    {
        var resourceRdfId = new Uri($"https://purl.org/coscine/resources/{resourceId}");

        return Get(resourceRdfId);
    }
}
