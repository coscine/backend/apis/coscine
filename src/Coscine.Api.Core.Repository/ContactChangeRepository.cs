﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class ContactChangeRepository(RepositoryContext repositoryContext) : IContactChangeRepository

{
    private readonly RepositoryBase<ContactChange> _contactChange = new(repositoryContext);

    public void Create(ContactChange contactChange)
    {
        _contactChange.Create(contactChange);
    }

    public void Delete(ContactChange contactChange)
    {
        _contactChange.Delete(contactChange);
    }

    public async Task<IEnumerable<ContactChange>> GetAllAsync(Guid userId, ContactChangeParameters contactChangeParameters, bool trackChanges)
    {
        return await _contactChange.FindAll(trackChanges)
            .Where(cc => userId == cc.UserId)
            .FilterByEditDateBefore(contactChangeParameters.EditDateBefore)
            .ToListAsync();
    }

    public async Task<ContactChange?> GetAsync(Guid confirmationTokenId, bool trackChanges)
    {
        return await _contactChange.FindByCondition(cc => confirmationTokenId == cc.ConfirmationToken, trackChanges)
            .SingleOrDefaultAsync();
    }

    public async Task MergeAsync(Guid fromUserId, Guid toUserId)
    {
        foreach (var cc in await _contactChange.FindByCondition(x => x.UserId == fromUserId, trackChanges: true)
            .ToListAsync())
        {
            _contactChange.Delete(cc);
        }

    }
}