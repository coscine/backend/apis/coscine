using System.Text;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Helpers;
using Microsoft.Extensions.Options;
using VDS.RDF;
using VDS.RDF.Nodes;

namespace Coscine.Api.Core.Repository;

public class LinkedDataStorageRepository(IOptionsSnapshot<LinkedResourceTypeConfiguration> linkedResourceTypeConfigurations,
                                         ILinkedResourceTypeRepository linkedResourceTypeRepository,
                                         IProvenanceRepository provenanceRepository,
                                         ITreeRepository treeRepository,
                                         IRdfRepositoryBase rdfRepositoryBase) : IDataStorageRepository
{
    private readonly ResourceTypeConfiguration _resourceTypeConfiguration = linkedResourceTypeConfigurations.Get("linked");
    private readonly ILinkedResourceTypeRepository _linkedResourceTypeRepository = linkedResourceTypeRepository;
    private readonly IProvenanceRepository _provenanceRepository = provenanceRepository;
    private readonly ITreeRepository _treeRepository = treeRepository;
    private readonly IRdfRepositoryBase _rdfRepositoryBase = rdfRepositoryBase;

    public IEnumerable<Capability> Capabilities
    {
        get
        {
            yield return Capability.CanRead;
            yield return Capability.CanWrite;
            yield return Capability.CanDelete;
            yield return Capability.SupportsReadOnlyMode;
        }
    }

    public Guid TypeId => Configurations.ResourceTypeConfiguration.ResourceTypeLinked.Id;


    /// <summary>
    /// Retrieves all specific resource types.
    /// </summary>
    /// <returns>An IEnumerable of specific resource types.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes()
    {
        return [_resourceTypeConfiguration.SpecificType];
    }

    /// <summary>
    /// Retrieves specific resource types with a particular status.
    /// </summary>
    /// <param name="status">The status of the resource types to retrieve.</param>
    /// <returns>An IEnumerable of specific resource types that match the provided status.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(ResourceTypeStatus status)
    {
        return GetSpecificResourceTypes().Where(st => st.Status == status);
    }

    /// <summary>
    /// Retrieves specific resource types that are supported by a particular organization.
    /// </summary>
    /// <param name="OrganizationRor">The ROR of the organization to match against the supported organizations of the resource types.</param>
    /// <returns>An IEnumerable of specific resource types that are supported by the organization specified by the provided ROR.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor)
    {
        return GetSpecificResourceTypes().Where(st => st.SupportedOrganizations is not null
            && (st.SupportedOrganizations.Contains(OrganizationRor) || st.SupportedOrganizations.Contains("*")));
    }

    /// <summary>
    /// Retrieves specific resource types that are supported by a particular organization and have a particular status.
    /// </summary>
    /// <param name="OrganizationRor">The ROR of the organization to match against the supported organizations of the resource types.</param>
    /// <param name="status">The status of the resource types to retrieve.</param>
    /// <returns>An IEnumerable of specific resource types that match the provided status and are supported by the organization specified by the provided ROR.</returns>
    public IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor, ResourceTypeStatus status)
    {
        return GetSpecificResourceTypes(OrganizationRor).Where(st => st.Status == status);
    }

    public Task CreateAsync(Resource resource, CreateDataStorageParameters parameters)
    {
        var linkedResourceType = new LinkedResourceType
        {
            Id = Guid.NewGuid(),
        };

        _linkedResourceTypeRepository.Create(linkedResourceType);
        resource.ResourceTypeOptionId = linkedResourceType.Id;

        return Task.CompletedTask;
    }

    public async Task CreateAsync(CreateBlobParameters parameters)
    {
        // 4kb + 1
        byte[] buffer = new byte[(1024 * 4) + 1];
        var size = await parameters.Blob.ReadAsync(buffer);

        if (size > buffer.Length - 1)
        {
            throw new Exception("Cannot save stream. The stream has more than 4 KB of data.");
        }

        Array.Resize(ref buffer, size);

        var newestData = (await _treeRepository.GetNewestPagedDataAsync(parameters.Resource.Id, new() { Path = parameters.Path })).FirstOrDefault()
            ?? throw new DataGraphNotFoundException(parameters.Resource.Id, parameters.Path);

        var graph = await _rdfRepositoryBase.GetGraph(newestData.Id);

        if (!graph.IsEmpty)
        {
            var triples = graph.GetTriplesWithPredicate(graph.CreateUriNode(RdfUris.CoscineTermsLinkedBody));
            graph.Retract(triples.ToArray());
        }
        else
        {
            // TODO: Consider using AddGraphAsync
            await _rdfRepositoryBase.CreateGraphAsync(newestData.Id);
        }

        var content = Encoding.UTF8.GetString(buffer);
        graph.Assert(new Triple(graph.CreateUriNode(newestData.Id), graph.CreateUriNode(RdfUris.CoscineTermsLinkedBody), graph.CreateLiteralNode(content)));

        await _rdfRepositoryBase.AddGraphAsync(graph);
    }

    public Task CreateAsync(CreateTreeParameters parameters)
    {
        throw new NotImplementedException();
    }


    public async Task DeleteAsync(DeleteBlobParameters parameters)
    {
        // NOTE: Check the logic when data graphs are invalidated in the blob service. This method may become obsolete and return Task.CompletedTask.
        var newestData = (await _treeRepository.GetNewestPagedDataAsync(parameters.Resource.Id, new() { Path = parameters.Path })).FirstOrDefault()
            ?? throw new DataGraphNotFoundException(parameters.Resource.Id, parameters.Path);
        await _provenanceRepository.CreateInvalidation(parameters.Resource.Id, parameters.Path, newestData.Id, "data");
    }

    public Task DeleteAsync(DeleteTreeParameters parameters)
    {
        return Task.CompletedTask;
    }

    public Task<DataStorageInfo> GetStorageInfoAsync(StorageInfoParameters parameters)
    {
        return Task.FromResult(new DataStorageInfo());
    }

    public async Task<IEnumerable<StorageItemInfo>> ListAsync(ListTreeParameters parameters)
    {
        var resultList = new List<StorageItemInfo>();

        // Ensure to get all data trees, as the method definition expects to return the entire collection
        var currentData = PaginationHelper.GetAllAsync(
            (currentPage) =>
                _treeRepository.GetNewestPagedDataAsync(parameters.Resource.Id, new() { Path = parameters.TreePath, PageNumber = currentPage })
        );

        // Filter the results
        // Remove all paths, that are deeper than 1 level from the prefix
        // Also don't query virtuoso for the folders

        const char delimiter = '/';

        // HashSets, as we only want distinct values
        var filePaths = new HashSet<string>();
        var folderPaths = new HashSet<string>();

        await foreach (var data in currentData)
        {
            string prefixFreePath = data.Path;

            if (data.Path.StartsWith(parameters.TreePath))
            {
                prefixFreePath = data.Path.Remove(0, parameters.TreePath.Length);
            }

            prefixFreePath = prefixFreePath.TrimStart(delimiter);

            var indexOfSlash = prefixFreePath.IndexOf('/');

            if (indexOfSlash != -1)
            {
                // Folder path
                prefixFreePath = prefixFreePath[..(indexOfSlash + 1)];
                folderPaths.Add(parameters.TreePath.Length > 0 ? $"{parameters.TreePath.TrimEnd(delimiter)}{delimiter}{prefixFreePath}" : prefixFreePath);
            }
            else
            {
                // File Path
                filePaths.Add(parameters.TreePath.Length > 0 ? $"{parameters.TreePath.TrimEnd(delimiter)}{(prefixFreePath.Length > 0 ? delimiter : "")}{prefixFreePath}" : prefixFreePath);
            }
        }

        // Load the actual value from virtuoso
        foreach (var path in filePaths)
        {
            var dataEntry = await ReadAsync(new ReadTreeParameters
            {
                Resource = parameters.Resource,
                Path = path
            });

            if (dataEntry is not null)
            {
                resultList.Add(dataEntry);
            }
        }

        // Just attach the empty folder
        foreach (var path in folderPaths)
        {
            resultList.Add(new StorageItemInfo
            {
                Path = path,
                IsBlob = false,
                ContentLength = 0,
            });
        }

        return resultList;
    }


    public async Task<Stream?> ReadAsync(ReadBlobParameters parameters)
    {
        var newestData = (await _treeRepository.GetNewestPagedDataAsync(parameters.Resource.Id, new() { Path = parameters.Path })).FirstOrDefault()
            ?? throw new DataGraphNotFoundException(parameters.Resource.Id, parameters.Path);

        var graph = await _rdfRepositoryBase.GetGraph(newestData.Id);

        if (graph.IsEmpty)
        {
            return await Task.FromResult<Stream?>(null);
        }

        var triples = graph.GetTriplesWithPredicate(graph.CreateUriNode(RdfUris.CoscineTermsLinkedBody));

        if (!triples.Any())
        {
            return await Task.FromResult<Stream?>(null);
        }

        byte[] byteArray = Encoding.UTF8.GetBytes(triples.First().Object.AsValuedNode().AsString());
        var stream = new MemoryStream(byteArray);

        return await Task.FromResult<Stream?>(stream);
    }

    public async Task<StorageItemInfo?> ReadAsync(ReadTreeParameters parameters)
    {
        var newestData = (await _treeRepository.GetNewestPagedDataAsync(parameters.Resource.Id, new() { Path = parameters.Path })).FirstOrDefault();

        if(newestData is null){
            return null;
        }

        var graph = await _rdfRepositoryBase.GetGraph(newestData.Id);

        if (graph.IsEmpty)
        {
            return await Task.FromResult<StorageItemInfo?>(null);
        }

        var triples = graph.GetTriplesWithPredicate(graph.CreateUriNode(RdfUris.CoscineTermsLinkedBody));

        if (!triples.Any())
        {
            return await Task.FromResult<StorageItemInfo?>(null);
        }

        var result = new StorageItemInfo
        {
            Path = parameters.Path,
            IsBlob = true,
            ContentLength = Encoding.UTF8.GetBytes(triples.First().Object.ToString()).Length,
        };

        return result;
    }

    public async Task UpdateAsync(UpdateBlobParameters parameters)
    {
        await CreateAsync(new CreateBlobParameters
        {
            Resource = parameters.Resource,
            Blob = parameters.Blob,
            Path = parameters.Path,
            Version = parameters.Version
        });
    }

    public async Task UpdateAsync(UpdateTreeParameters parameters)
    {
        await CreateAsync(new CreateTreeParameters
        {
            Resource = parameters.Resource,
            Path = parameters.Path,
            Version = parameters.Version
        });
    }

    public Task UpdateAsync(Resource resource, UpdateDataStorageParameters parameters)
    {
        return Task.CompletedTask;
    }

    public Task<ResourceTypeInformation> GetResourceTypeInformationAsync()
    {
        var resourceTypeInformation = new ResourceTypeInformation
        {

            CanCreate = true,
            CanRead = true,
            CanSetResourceReadonly = false,
            CanUpdate = true,
            CanUpdateResource = false,
            CanDelete = true,
            CanDeleteResource = false,
            CanList = true,
            CanCreateLinks = false,
            CanCopyLocalMetadata = false,
            IsArchived = false,
            IsQuotaAvailable = false,
            IsQuotaAdjustable = false,
            ResourceCreate = new ResourceCreate
            {
                Components =
                [
                    [], // First step
                        [], // Second step
                        [], // Third step
                        []  // Fourth step
                ]
            },
            ResourceContent = new ResourceContent
            {
                ReadOnly = false,
                MetadataView = new MetadataView
                {
                    EditableDataUrl = true,
                    EditableKey = true
                },
                EntriesView = new EntriesView
                {
                    Columns = new ColumnsObject
                    {
                        Always = ["name", "size"]
                    }
                }
            },

            Status = _resourceTypeConfiguration?.SpecificType?.Status ?? ResourceTypeStatus.Hidden,
            GeneralType = _resourceTypeConfiguration?.SpecificType?.Type,
            SpecificType = _resourceTypeConfiguration?.SpecificType?.SpecificTypeName
        };


        return Task.FromResult(resourceTypeInformation);
    }
}
