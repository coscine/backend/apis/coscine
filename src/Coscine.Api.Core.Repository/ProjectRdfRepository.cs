﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Models.QuadStore;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Microsoft.Extensions.Logging;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Repository;

public class ProjectRdfRepository(IStore store, ILogger<ProjectRdfRepository> logger) : IProjectRdfRepository
{
    private readonly IStore _store = store;
    private readonly ILogger<ProjectRdfRepository> _logger = logger;

    public ProjectRdf? Get(Uri projectRdfId)
    {
        try
        {
            // Load model
            var model = new Model(_store, new UriRef(projectRdfId.AbsoluteUri));

            // Load Resource
            return model?.GetResource<ProjectRdf>(projectRdfId);
        }
        catch (Exception e)
        {
            _logger.LogWarning(e, "Exception while loading from the quad store");
            return null;
        }
    }

    public ProjectRdf? Get(Guid projectId)
    {
        var projectRdfId = new Uri($"https://purl.org/coscine/projects/{projectId}");

        return Get(projectRdfId);
    }

    public ProjectRdf? Create(Project project)
    {
        try
        {
            // Load model
            var model = new Model(_store, new UriRef(new Uri($"https://purl.org/coscine/projects/{project.Id}")));

            var resource = model.GetOrCreateResource<ProjectRdf>(model.Uri);

            // Load Resource
            model?.UpdateResource(resource);
            return resource;
        }
        catch (Exception e)
        {
            _logger.LogWarning(e, "Exception while loading from the quad store");
            return null;
        }
    }
}
