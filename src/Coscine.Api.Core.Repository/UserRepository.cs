﻿using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.Helpers;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using VDS.RDF;

namespace Coscine.Api.Core.Repository;

public sealed class UserRepository(RepositoryContext repositoryContext, IRdfRepositoryBase rdfRepositoryBase) : IUserRepository
{
    private readonly RepositoryBase<User> _user = new(repositoryContext);

    public Uri GenerateGraphName(User user)
    {
        return RdfUris.CoscineUsers.AppendToPath(user.Id.ToString());
    }

    public async Task DeleteGraphAsync(User user)
    {
        var userGraphName = GenerateGraphName(user);
        await rdfRepositoryBase.ClearGraphAsync(userGraphName);
    }

    public async Task UpdateGraphAsync(User user)
    {
        var userGraphName = GenerateGraphName(user);
        await rdfRepositoryBase.ClearGraphAsync(userGraphName);
        await CreateGraphAsync(user);
    }

    public async Task<IGraph> CreateGraphAsync(User user)
    {
        // Create the user graph
        var userGraph = GenerateUserGraph(user);

        // Store the graph
        await rdfRepositoryBase.AddGraphAsync(userGraph);

        return userGraph;
    }

    private Graph GenerateUserGraph(User user)
    {
        var userGraphName = GenerateGraphName(user);

        // Create the user graph
        var userGraph = new Graph(userGraphName)
        {
            BaseUri = userGraphName
        };

        userGraph.AssertToGraph(userGraphName, RdfUris.A, RdfUris.FoafPersonClass);
        userGraph.AssertToGraph(userGraphName, RdfUris.FoafName, user.DisplayName);

        return userGraph;
    }


    public async Task<PagedEnumerable<User>> GetPagedAsync(UserAdminParameters userParameters, bool trackChanges)
    {
        var userQuery = _user.FindAll(trackChanges)
            .Include(u => u.ContactChanges)
            .Include(u => u.Language)
            .Include(u => u.Title)
            .Include(u => u.Tosaccepteds)
            .Include(u => u.ExternalIds).ThenInclude(eid => eid.ExternalAuthenticator)
            .Include(u => u.UserDisciplines).ThenInclude(ud => ud.Discipline)
            .Where(u => userParameters.TosAccepted != true || u.Tosaccepteds.Count > 0)
            .AsSplitQuery();

        var users = await userQuery
            .SortUsers(userParameters.OrderBy)
            .Skip((userParameters.PageNumber - 1) * userParameters.PageSize)
            .Take(userParameters.PageSize)
            .ToListAsync();

        var count = await userQuery.CountAsync();

        return new PagedEnumerable<User>(users, count, userParameters.PageNumber, userParameters.PageSize);
    }

    public async Task<IEnumerable<User>> GetFirstTenUsersAsync(UserSearchParameters userParameters, bool trackChanges)
    {
        var userQuery = _user.FindAll(trackChanges)
            .Include(u => u.Title)
            .Where(u => !string.IsNullOrWhiteSpace(u.Givenname)
                        && !string.IsNullOrWhiteSpace(u.Surname)
                        && u.Tosaccepteds.Count > 0)
            .SearchUsers(userParameters.SearchTerm)
            .AsSplitQuery();

        return await userQuery
            .SortUsers(userParameters.OrderBy)
            .Take(10)
            .ToListAsync();
    }

    public async Task<User?> GetAsync(Guid userId, bool trackChanges)
    {
        return await _user.FindByCondition(x => x.Id == userId, trackChanges)
            .Include(u => u.ContactChanges)
            .Include(u => u.Language)
            .Include(u => u.Title)
            .Include(u => u.Tosaccepteds)
            .Include(u => u.ExternalIds).ThenInclude(eid => eid.ExternalAuthenticator)
            .Include(u => u.UserDisciplines).ThenInclude(ud => ud.Discipline)
            .AsSplitQuery()
            .SingleOrDefaultAsync();
    }

    public async Task<User?> GetAsync(Guid userId, bool trackChanges, params Expression<Func<User, object?>>[] includeProperties)
    {
        var query = _user.FindByCondition(u => userId == u.Id, trackChanges);

        foreach (var includeProperty in includeProperties)
        {
            query = query.Include(includeProperty);
        }

        query = query
            .Include(u => u.ContactChanges)
            .Include(u => u.Language)
            .Include(u => u.Title)
            .Include(u => u.Tosaccepteds)
            .Include(u => u.ExternalIds).ThenInclude(eid => eid.ExternalAuthenticator)
            .Include(u => u.UserDisciplines).ThenInclude(ud => ud.Discipline)
            .AsSplitQuery()
            .AsSplitQuery();

        return await query.SingleOrDefaultAsync();
    }

    public void Create(User user)
    {
        _user.Create(user);
    }
    public void SoftDelete(User user)
    {
        user.DeletedAt = DateTime.UtcNow;
    }

    public async Task<User> MergeAsync(Guid fromUserId, Guid toUserId)
    {
        // Rare case, where the repository function checks for null!
        // This is not the norm, but needed in this special case.
        var fromUser = await _user.FindByCondition(x => x.Id == fromUserId, trackChanges: true)
            .SingleOrDefaultAsync()
            ?? throw new UserNotFoundException(fromUserId);
        var toUser = await _user.FindByCondition(x => x.Id == toUserId, trackChanges: true)
            .SingleOrDefaultAsync()
            ?? throw new UserNotFoundException(toUserId);

        if (string.IsNullOrEmpty(toUser.EmailAddress) && !string.IsNullOrEmpty(fromUser.EmailAddress))
        {
            toUser.EmailAddress = fromUser.EmailAddress;
        }

        if (string.IsNullOrEmpty(toUser.Surname) && !string.IsNullOrEmpty(fromUser.Surname))
        {
            toUser.Surname = fromUser.Surname;
        }

        if (string.IsNullOrEmpty(toUser.Givenname) && !string.IsNullOrEmpty(fromUser.Givenname))
        {
            toUser.Givenname = fromUser.Givenname;
        }

        if (toUser.TitleId is null && fromUser.TitleId is not null)
        {
            toUser.TitleId = fromUser.TitleId;
        }

        if (string.IsNullOrEmpty(toUser.Organization) && !string.IsNullOrEmpty(fromUser.Organization))
        {
            toUser.Organization = fromUser.Organization;
        }

        if (string.IsNullOrEmpty(toUser.DisplayName) && !string.IsNullOrEmpty(fromUser.DisplayName))
        {
            toUser.DisplayName = fromUser.DisplayName;
        }

        if (toUser.LanguageId is null && fromUser.LanguageId is not null)
        {
            toUser.LanguageId = fromUser.LanguageId;
        }

        return fromUser;
    }
}