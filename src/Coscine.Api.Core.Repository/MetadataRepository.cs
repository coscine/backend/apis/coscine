﻿using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.HelperModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Enums.Utility;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository;

public class MetadataRepository(IRdfRepositoryBase rdfRepositoryBase) : IMetadataRepository
{
    private readonly IRdfRepositoryBase _rdfRepositoryBase = rdfRepositoryBase;

    public async Task<RdfDefinitionDto> GetAsync(Uri graphUri, MetadataGetAdminParameters metadataGetAdminParameters, bool trackChanges)
    {
        var graph = await _rdfRepositoryBase.GetGraph(graphUri);
        var format = metadataGetAdminParameters.Format ?? RdfFormat.Turtle;
        var definition = _rdfRepositoryBase.WriteGraph(graph, format);
        return new RdfDefinitionDto()
        {
            Content = definition,
            Type = format.GetEnumMemberValue()
        };
    }

    public async Task UpdateAsync(Uri graphUri, MetadataUpdateAdminParameters metadataUpdateAdminParameters, bool trackChanges)
    {
        var graph = _rdfRepositoryBase.ParseGraph(metadataUpdateAdminParameters.Definition.Content, metadataUpdateAdminParameters.Definition.Type, graphUri);
        graph.BaseUri = graphUri;
        await _rdfRepositoryBase.AddGraphAsync(graph);
    }

    public async Task PatchAsync(Uri graphUri, RdfPatchDocumentDto rdfPatchDoc)
    {
        // Note that we're not validating the graph's existance here
        var patchGraph = PatchGraph.Empty(graphUri);

        foreach (var operation in rdfPatchDoc.Operations)
        {
            var graph = _rdfRepositoryBase.ParseGraph(operation.Changes.Content, operation.Changes.Type);
            switch (operation.OperationType)
            {
                case RdfPatchOperationType.Add:
                    patchGraph.AssertList.AddRange(graph.Triples);
                    break;
                case RdfPatchOperationType.Delete:
                    patchGraph.RetractList.AddRange(graph.Triples);
                    break;
                default:
                    // Do nothing, as we don't support other operations yet
                    break;
            }
        }

        await _rdfRepositoryBase.AddGraphAsync(patchGraph);
    }
}