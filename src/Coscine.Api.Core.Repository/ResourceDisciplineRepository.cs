﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;

namespace Coscine.Api.Core.Repository;

public sealed class ResourceDisciplineRepository : IResourceDisciplineRepository
{
    private readonly RepositoryBase<ResourceDiscipline> _resourceDiscipline;

    public ResourceDisciplineRepository(RepositoryContext repositoryContext)
    {
        _resourceDiscipline = new(repositoryContext);
    }

    public void Delete(ResourceDiscipline resourceDiscipline)
    {
        _resourceDiscipline.Delete(resourceDiscipline);
    }
}