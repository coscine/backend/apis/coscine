﻿using Coscine.Api.Core.Entities.Models.QuadStore;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.Extensions.Logging;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Repository;

public class MetadataEntityRepository(IStore store, ILogger<MetadataEntityRepository> logger) : IMetadataEntityRepository
{
    private readonly IStore _store = store;
    private readonly ILogger<MetadataEntityRepository> _logger = logger;

    public MetadataEntity? Get(Uri metadataCatalogId, Uri metadataEntityId)
    {
        try
        {
            // Load model
            var model = new Model(_store, new UriRef(metadataCatalogId.AbsoluteUri));

            // Load Resource
            return model?.GetResource<MetadataEntity>(metadataEntityId);
        }
        catch (Exception e)
        {
            _logger.LogWarning(e, "Exception while loading from the quad store");
            return null;
        }
    }
}
