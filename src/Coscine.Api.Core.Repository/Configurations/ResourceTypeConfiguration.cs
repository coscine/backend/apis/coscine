﻿using Coscine.Api.Core.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Coscine.Api.Core.Repository.Configurations;

public class ResourceTypeConfiguration : IEntityTypeConfiguration<ResourceType>
{
    // Read-only properties for each ResourceType instance
    public static ResourceType ResourceTypeRdss3 => new()
    {
        Id = Guid.Parse("A3539377-F7A4-4751-9F7A-08B9DE4F0C8D"),
        DisplayName = "rdss3",
        Type = "rdss3",
        SpecificType = "rdss3rwth"
    };

    public static ResourceType ResourceTypeLinked => new()

    {
        Id = Guid.Parse("2629A6F9-2129-432A-9304-123DBBFA7685"),
        DisplayName = "linked",
        Type = "linked",
        SpecificType = "linked"
    };

    public static ResourceType ResourceTypeRdss3Worm => new()

    {
        Id = Guid.Parse("BCB34A02-9B01-40B7-BCE1-14CB9EB543F5"),
        DisplayName = "rdss3worm",
        Type = "rdss3worm",
        SpecificType = "rdss3wormrwth"
    };

    public static ResourceType ResourceTypeRds => new()

    {
        Id = Guid.Parse("D0CE723A-EDF8-49F8-B2AC-34D7251CF104"),
        DisplayName = "rds",
        Type = "rds",
        SpecificType = "rdsrwth"
    };

    public static ResourceType ResourceTypeRdsTudo => new()

    {
        Id = Guid.Parse("23580290-2D3E-4C12-94E7-7CE1E50AE094"),
        DisplayName = "rdstudo",
        Type = "rds",
        SpecificType = "rdstudo"
    };

    public static ResourceType ResourceTypeRdsRub => new()

    {
        Id = Guid.Parse("198C9BA4-E6BB-4B76-8C37-16AAD43E61E8"),
        DisplayName = "rdsrub",
        Type = "rds",
        SpecificType = "rdsrub"
    };

    public static ResourceType ResourceTypeGitlab => new()

    {
        Id = Guid.Parse("649A5035-C798-4136-9B93-974F79B8D548"),
        DisplayName = "gitlab",
        Type = "gitlab",
        SpecificType = "gitlab"
    };

    public static ResourceType ResourceTypeRdss3Nrw => new()

    {
        Id = Guid.Parse("BC9DEC17-A893-4670-BD2C-AE8D736FD4DE"),
        DisplayName = "rdss3nrw",
        Type = "rdss3",
        SpecificType = "rdss3nrw"
    };

    public static ResourceType ResourceTypeRdss3Ude => new()

    {
        Id = Guid.Parse("8DBC1E2D-432D-4320-B98C-BAC41C8AF630"),
        DisplayName = "rdss3ude",
        Type = "rdss3",
        SpecificType = "rdss3ude"
    };

    public static ResourceType ResourceTypeRdsUde => new()

    {
        Id = Guid.Parse("78E6AEF3-8450-494D-AB86-D033BB9B015B"),
        DisplayName = "rdsude",
        Type = "rds",
        SpecificType = "rdsude"
    };

    public static ResourceType ResourceTypeRdss3Tudo => new()

    {
        Id = Guid.Parse("2D9100E3-F4C2-4307-B679-E234E37FFDE7"),
        DisplayName = "rdss3tudo",
        Type = "rdss3",
        SpecificType = "rdss3tudo"
    };

    public static ResourceType ResourceTypeRdss3Rub => new()

    {
        Id = Guid.Parse("C52D5419-9556-42F5-97B2-860AB359B03F"),
        DisplayName = "rdss3rub",
        Type = "rdss3",
        SpecificType = "rdss3rub"
    };

    public static ResourceType ResourceTypeRdsNrw => new()

    {
        Id = Guid.Parse("A9C0EA37-83C8-4C0B-B159-FCCDAFED75F0"),
        DisplayName = "rdsnrw",
        Type = "rds",
        SpecificType = "rdsnrw"
    };

    // Don't add this to the DB!
    public static ResourceType LocalResourceType => new()
    {
        Id = Guid.Parse("9e176e70-8b50-470e-8422-7668ec841a4f"),
        DisplayName = "localstorage",
        SpecificType = "localstorage",
        Type = "localstorage"
    };

    // Configure method for EntityTypeConfiguration
    public void Configure(EntityTypeBuilder<ResourceType> builder)
    {
        // Use the properties to seed data using HasData
        builder.HasData(
            ResourceTypeRdss3,
            ResourceTypeLinked,
            ResourceTypeRdss3Worm,
            ResourceTypeRds,
            ResourceTypeRdsTudo,
            ResourceTypeRdsRub,
            ResourceTypeGitlab,
            ResourceTypeRdss3Nrw,
            ResourceTypeRdss3Ude,
            ResourceTypeRdsUde,
            ResourceTypeRdss3Tudo,
            ResourceTypeRdss3Rub,
            ResourceTypeRdsNrw
        );
    }
}
