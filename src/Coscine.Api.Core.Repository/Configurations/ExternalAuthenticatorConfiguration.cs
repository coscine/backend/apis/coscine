﻿using Coscine.Api.Core.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Coscine.Api.Core.Repository.Configurations;

public class ExternalAuthenticatorConfiguration : IEntityTypeConfiguration<ExternalAuthenticator>
{
    public static ExternalAuthenticator DFN_AAI { get; set; } = new ExternalAuthenticator
    {
        Id = Guid.Parse("64509960-17A6-4ED6-A54E-0833AD5453D1"),
        DisplayName = "Shibboleth",
    };

    public static ExternalAuthenticator ORCiD { get; set; } = new ExternalAuthenticator
    {
        Id = Guid.Parse("F383F83C-26CB-47EB-80C8-639ED05BD0DE"),
        DisplayName = "ORCiD",
    };

    public static ExternalAuthenticator GitLab_RWTH { get; set; } = new ExternalAuthenticator
    {
        Id = Guid.Parse("4043104a-52bd-4dfd-9e6b-d818e2ea0094"),
        DisplayName = "GitLab RWTH",
    };

    public static ExternalAuthenticator NFDI4Ing_AAI { get; set; } = new ExternalAuthenticator
    {
        Id = Guid.Parse("3a1e7185-0328-47ed-afd7-a4ffd7f03a79"),
        DisplayName = "NFDI4Ing AAI",
    };
    public void Configure(EntityTypeBuilder<ExternalAuthenticator> builder)
    {
        builder.HasData(
            DFN_AAI,
            ORCiD,
            GitLab_RWTH,
            NFDI4Ing_AAI
       );
    }
}