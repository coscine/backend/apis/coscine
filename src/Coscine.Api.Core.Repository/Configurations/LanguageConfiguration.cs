﻿using Coscine.Api.Core.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Coscine.Api.Core.Repository.Configurations;

public class LanguageConfiguration : IEntityTypeConfiguration<Language>
{
    public void Configure(EntityTypeBuilder<Language> builder)
    {
        // Seed data using HasData
        builder.HasData(
            new Language
            {
                Id = Guid.Parse("922C0FEF-D9AB-4F36-BD77-EED0384D19B9"),
                DisplayName = "Deutsch",
                Abbreviation = "de"
            },
            new Language
            {
                Id = Guid.Parse("781823EA-01DF-4566-AA58-FB44B5CC1D4D"),
                DisplayName = "English",
                Abbreviation = "en"
            }
        );
    }
}