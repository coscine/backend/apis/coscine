﻿using Coscine.Api.Core.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Coscine.Api.Core.Repository.Configurations;

public class RoleConfiguration : IEntityTypeConfiguration<Role>
{
    public static Role Owner { get; set; } = new Role
    {
        Id = Guid.Parse("BE294C5E-4E42-49B3-BEC4-4B15F49DF9A5"),
        DisplayName = "Owner",
        Description = "Owner of the project."
    };

    public static Role Member { get; set; } = new Role
    {
        Id = Guid.Parse("508B6D4E-C6AC-4AA5-8A8D-CAA31DD39527"),
        DisplayName = "Member",
        Description = "Member of the project."
    };

    public static Role Guest { get; set; } = new Role
    {
        Id = Guid.Parse("9184A442-4419-4E30-9FE6-0CFE32C9A81F"),
        DisplayName = "Guest",
        Description = "Guest of the project."
    };

    public void Configure(EntityTypeBuilder<Role> builder)
    {
        builder.HasData(Owner,
            Member,
            Guest
        );
    }
}