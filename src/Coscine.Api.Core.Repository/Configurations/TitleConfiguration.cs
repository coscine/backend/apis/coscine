﻿using Coscine.Api.Core.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Coscine.Api.Core.Repository.Configurations;

public class TitleConfiguration : IEntityTypeConfiguration<Title>
{
    public void Configure(EntityTypeBuilder<Title> builder)
    {
        builder.HasData(
            new Title
            {
                Id = Guid.Parse("98D96229-4A1B-4844-9EC2-925B6A33A696"),
                DisplayName = "Dr."
            },
            new Title
            {
                Id = Guid.Parse("F47098E0-3CC6-4F53-BBCD-D9AB48EB8899"),
                DisplayName = "Prof."
            }
        );
    }
}