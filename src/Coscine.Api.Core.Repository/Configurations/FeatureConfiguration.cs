﻿using Coscine.Api.Core.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Coscine.Api.Core.Repository.Configurations;

public class FeatureConfiguration : IEntityTypeConfiguration<Feature>
{
    public void Configure(EntityTypeBuilder<Feature> builder)
    {
        // Seed data using HasData
        builder.HasData(
            new Feature
            {
                Id = Guid.Parse("6589DB7B-FE26-4739-9A92-3E3AD86C3A69"),
                SharepointId = "MSOZoneCell_WebPartWPQ3",
                DisplaynameEn = "Documents",
                DisplaynameDe = "Dokumente"
            },
            new Feature
            {
                Id = Guid.Parse("D43E457F-A35F-43A3-8BDB-543048E6F87C"),
                SharepointId = "MSOZoneCell_WebPartWPQ2",
                DisplaynameEn = "Discussion Board",
                DisplaynameDe = "Discussion Board"
            },
            new Feature
            {
                Id = Guid.Parse("1E237667-34BA-4889-BF12-D56EEA266341"),
                SharepointId = "MSOZoneCell_WebPartWPQ4",
                DisplaynameEn = "Announcement Board",
                DisplaynameDe = "Announcement Board"
            }
        );
    }
}