﻿using Coscine.Api.Core.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Coscine.Api.Core.Repository.Configurations;

public class DisciplineConfiguration : IEntityTypeConfiguration<Discipline>
{
    public void Configure(EntityTypeBuilder<Discipline> builder)
    {
        // Seeding data
        builder.HasData(
            new Discipline
            {
                Id = Guid.Parse("FFCF2E27-28FA-48FD-981D-095A63DB39E8"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=113",
                DisplayNameDe = "Rechtswissenschaften 113",
                DisplayNameEn = "Jurisprudence 113"
            },
            new Discipline
            {
                Id = Guid.Parse("8BC6DD9E-046F-4D5C-96B5-105740ED32A0"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=316",
                DisplayNameDe = "Geochemie, Mineralogie und Kristallographie 316",
                DisplayNameEn = "Geochemistry, Mineralogy and Crystallography 316"
            },
            new Discipline
            {
                Id = Guid.Parse("BB6BE7BC-1DC4-4D88-95CF-11BCD3AE57B7"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=308",
                DisplayNameDe = "Optik, Quantenoptik und Physik der Atome, Moleküle und Plasmen 308",
                DisplayNameEn = "Optics, Quantum Optics and Physics of Atoms, Molecules and Plasmas 308"
            },
            new Discipline
            {
                Id = Guid.Parse("E61C76C1-5EE4-43A3-B740-19A673C4CE64"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=406",
                DisplayNameDe = "Materialwissenschaft 406",
                DisplayNameEn = "Materials Science 406"
            },
            new Discipline
            {
                Id = Guid.Parse("187FD242-9347-46AD-A43B-1D383D700849"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=410",
                DisplayNameDe = "Bauwesen und Architektur 410",
                DisplayNameEn = "Construction Engineering and Architecture 410"
            },
            new Discipline
            {
                Id = Guid.Parse("BA3BD5AF-A490-4102-B925-1FA94708263F"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=303",
                DisplayNameDe = "Physikalische und Theoretische Chemie 303",
                DisplayNameEn = "Physical and Theoretical Chemistry 303"
            },
            new Discipline
            {
                Id = Guid.Parse("D5EABB9E-E3AB-41AD-80F9-2CF2D25FD4C1"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=405",
                DisplayNameDe = "Werkstofftechnik 405",
                DisplayNameEn = "Materials Engineering 405"
            },
            new Discipline
            {
                Id = Guid.Parse("86304353-4B24-4746-BEAF-2F6E7BDFAB18"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=404",
                DisplayNameDe = "Wärmeenergietechnik, Thermische Maschinen, Strömungsmechanik 404",
                DisplayNameEn = "Heat Energy Technology, Thermal Machines, Fluid Mechanics 404"
            },
            new Discipline
            {
                Id = Guid.Parse("CFD6B656-F4BA-48C6-B5D8-2F98A7B60D1F"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=409",
                DisplayNameDe = "Informatik 409",
                DisplayNameEn = "Computer Science 409"
            },
            new Discipline
            {
                Id = Guid.Parse("86454A0C-40AB-4A41-9E5E-3694A0CE0580"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=205",
                DisplayNameDe = "Medizin 205",
                DisplayNameEn = "Medicine 205"
            },
            new Discipline
            {
                Id = Guid.Parse("8AFA849A-1CB2-48C6-91FD-397C481FF624"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=104",
                DisplayNameDe = "Sprachwissenschaften 104",
                DisplayNameEn = "Linguistics 104"
            },
            new Discipline
            {
                Id = Guid.Parse("64EB5F3C-533D-4B77-B7E0-3AFEAA72D315"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=407",
                DisplayNameDe = "Systemtechnik 407",
                DisplayNameEn = "Systems Engineering 407"
            },
            new Discipline
            {
                Id = Guid.Parse("B4250311-9995-4743-BE14-3B1B34BA5392"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=311",
                DisplayNameDe = "Astrophysik und Astronomie 311",
                DisplayNameEn = "Astrophysics and Astronomy 311"
            },
            new Discipline
            {
                Id = Guid.Parse("34E51E82-BC4A-466E-8649-3C27E45C94D6"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=306",
                DisplayNameDe = "Polymerforschung 306",
                DisplayNameEn = "Polymer Research 306"
            },
            new Discipline
            {
                Id = Guid.Parse("D9DB2D55-BEF5-4C2D-839F-45627ED88747"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=402",
                DisplayNameDe = "Mechanik und Konstruktiver Maschinenbau 402",
                DisplayNameEn = "Mechanics and Constructive Mechanical Engineering 402"
            },
            new Discipline
            {
                Id = Guid.Parse("83C9306C-F985-4F13-84E3-460CF69901EF"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=106",
                DisplayNameDe = "Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft 106",
                DisplayNameEn = "Social and Cultural Anthropology, Non-European Cultures, Jewish Studies and Religious Studies 106"
            },
            new Discipline
            {
                Id = Guid.Parse("01192BCA-1804-4750-9696-464B94FD20E3"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=101",
                DisplayNameDe = "Alte Kulturen 101",
                DisplayNameEn = "Ancient Cultures 101"
            },
            new Discipline
            {
                Id = Guid.Parse("FB170C05-9B63-472A-8851-4D22EF80622C"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=110",
                DisplayNameDe = "Psychologie 110",
                DisplayNameEn = "Psychology 110"
            },
            new Discipline
            {
                Id = Guid.Parse("73483467-AFC7-4AF8-8E94-5401A96CE5DF"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=317",
                DisplayNameDe = "Geographie 317",
                DisplayNameEn = "Geography 317"
            },
            new Discipline
            {
                Id = Guid.Parse("4D03241B-587A-4CDF-B6AC-58ED14477A0A"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=202",
                DisplayNameDe = "Pflanzenwissenschaften 202",
                DisplayNameEn = "Plant Sciences 202"
            },
            new Discipline
            {
                Id = Guid.Parse("ED1E8854-8CDA-4357-B9FD-65144A6208E1"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=312",
                DisplayNameDe = "Mathematik 312",
                DisplayNameEn = "Mathematics 312"
            },
            new Discipline
            {
                Id = Guid.Parse("BBCAADEC-501B-491B-90E5-6BD814151B31"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=310",
                DisplayNameDe = "Statistische Physik, Weiche Materie, Biologische Physik, Nichtlineare Dynamik 310",
                DisplayNameEn = "Statistical Physics, Soft Matter, Biological Physics, Nonlinear Dynamics 310"
            },
            new Discipline
            {
                Id = Guid.Parse("801564C8-AC8E-42E9-8FF5-6CA919CC9786"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=201",
                DisplayNameDe = "Grundlagen der Biologie und Medizin 201",
                DisplayNameEn = "Basic Research in Biology and Medicine 201"
            },
            new Discipline
            {
                Id = Guid.Parse("20DBC09F-9304-44EB-9F9F-7636536D5FCA"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=302",
                DisplayNameDe = "Chemische Festkörper- und Oberflächenforschung 302",
                DisplayNameEn = "Chemical Solid State and Surface Research 302"
            },
            new Discipline
            {
                Id = Guid.Parse("9CBB9163-9F09-4AAC-95B2-776374C856D0"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=203",
                DisplayNameDe = "Zoologie 203",
                DisplayNameEn = "Zoology 203"
            },
            new Discipline
            {
                Id = Guid.Parse("E2CDE3D3-0966-4832-A482-7F5A9A3E15A1"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=403",
                DisplayNameDe = "Verfahrenstechnik, Technische Chemie 403",
                DisplayNameEn = "Process Engineering, Technical Chemistry 403"
            },
            new Discipline
            {
                Id = Guid.Parse("CDF74163-3895-4307-B6BD-800D94FDFDAC"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=313",
                DisplayNameDe = "Atmosphären-, Meeres- und Klimaforschung 313",
                DisplayNameEn = "Atmospheric Science, Oceanography and Climate Research 313"
            },
            new Discipline
            {
                Id = Guid.Parse("EBB0D09A-0B2D-4737-BA2F-85F9DD45D4DD"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=107",
                DisplayNameDe = "Theologie 107",
                DisplayNameEn = "Theology 107"
            },
            new Discipline
            {
                Id = Guid.Parse("2B09F6CE-8483-4C53-BD73-860317C11AB5"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=102",
                DisplayNameDe = "Geschichtswissenschaften 102",
                DisplayNameEn = "History 102"
            },
            new Discipline
            {
                Id = Guid.Parse("9A6F509F-439B-4664-89EC-92A5E09845F9"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=318",
                DisplayNameDe = "Wasserforschung 318",
                DisplayNameEn = "Water Research 318"
            },
            new Discipline
            {
                Id = Guid.Parse("DC4194D3-9E4B-45A2-8484-93314069339A"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=309",
                DisplayNameDe = "Teilchen, Kerne und Felder 309",
                DisplayNameEn = "Particles, Nuclei and Fields 309"
            },
            new Discipline
            {
                Id = Guid.Parse("45ED6C72-166E-4358-BDDF-95EC9FEB792E"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=305",
                DisplayNameDe = "Biologische Chemie und Lebensmittelchemie 305",
                DisplayNameEn = "Biological Chemistry and Food Chemistry 305"
            },
            new Discipline
            {
                Id = Guid.Parse("9739635D-8103-4729-889F-98954F841053"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=401",
                DisplayNameDe = "Produktionstechnik 401",
                DisplayNameEn = "Production Technology 401"
            },
            new Discipline
            {
                Id = Guid.Parse("B5D7CDF2-B757-4BE2-973F-9C922D399B89"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=207",
                DisplayNameDe = "Agrar-, Forstwissenschaften und Tiermedizin 207",
                DisplayNameEn = "Agriculture, Forestry and Veterinary Medicine 207"
            },
            new Discipline
            {
                Id = Guid.Parse("A61F2AC4-3443-4A76-8DF6-9F3FF95EA18C"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=204",
                DisplayNameDe = "Mikrobiologie, Virologie und Immunologie 204",
                DisplayNameEn = "Microbiology, Virology and Immunology 204"
            },
            new Discipline
            {
                Id = Guid.Parse("FF07EEAE-E133-414C-AEAA-A1E8C9188F05"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=112",
                DisplayNameDe = "Wirtschaftswissenschaften 112",
                DisplayNameEn = "Economics 112"
            },
            new Discipline
            {
                Id = Guid.Parse("69D0D1DF-6D4A-4AC9-89EC-B82CF95C9295"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=206",
                DisplayNameDe = "Neurowissenschaft 206",
                DisplayNameEn = "Neurosciences 206"
            },
            new Discipline
            {
                Id = Guid.Parse("18CFD5A8-7B5E-4A77-9417-BB1B31FD0B60"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=408",
                DisplayNameDe = "Elektrotechnik und Informationstechnik 408",
                DisplayNameEn = "Electrical Engineering and Information Technology 408"
            },
            new Discipline
            {
                Id = Guid.Parse("6809A857-494C-4202-AB58-C649C43D10BD"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=315",
                DisplayNameDe = "Geophysik und Geodäsie 315",
                DisplayNameEn = "Geophysics and Geodesy 315"
            },
            new Discipline
            {
                Id = Guid.Parse("60119210-390D-4A36-9D09-C821C3612A4E"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=301",
                DisplayNameDe = "Molekülchemie 301",
                DisplayNameEn = "Molecular Chemistry 301"
            },
            new Discipline
            {
                Id = Guid.Parse("1A2BCB3B-969B-402E-ABA1-CE83E8BC5035"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=111",
                DisplayNameDe = "Sozialwissenschaften 111",
                DisplayNameEn = "Social Sciences 111"
            },
            new Discipline
            {
                Id = Guid.Parse("11C3FCB3-8004-4168-AAA5-D0C06F10FAAC"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=109",
                DisplayNameDe = "Erziehungswissenschaft und Bildungsforschung 109",
                DisplayNameEn = "Educational Research 109"
            },
            new Discipline
            {
                Id = Guid.Parse("D3F3E972-515D-4A99-A633-D31C984E959C"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=307",
                DisplayNameDe = "Physik der kondensierten Materie 307",
                DisplayNameEn = "Condensed Matter Physics 307"
            },
            new Discipline
            {
                Id = Guid.Parse("ACB1C755-232F-4143-815F-D78FCEA202A9"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=105",
                DisplayNameDe = "Literaturwissenschaft 105",
                DisplayNameEn = "Literary Studies 105"
            },
            new Discipline
            {
                Id = Guid.Parse("4FF1833E-D2F3-41F2-BFFA-DB1F0AB661FC"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=108",
                DisplayNameDe = "Philosophie 108",
                DisplayNameEn = "Philosophy 108"
            },
            new Discipline
            {
                Id = Guid.Parse("DBAE6F72-0596-40F3-9246-E4F1E1F9D79D"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=314",
                DisplayNameDe = "Geologie und Paläontologie 314",
                DisplayNameEn = "Geology and Palaeontology 314"
            },
            new Discipline
            {
                Id = Guid.Parse("338C270B-6613-42E0-BE9F-E71C45BFD83D"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=304",
                DisplayNameDe = "Analytik, Methodenentwicklung (Chemie) 304",
                DisplayNameEn = "Analytical Chemistry, Method Development (Chemistry) 304"
            },
            new Discipline
            {
                Id = Guid.Parse("77F8990D-4053-4C35-BC48-F7402E7324B8"),
                Url = "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=103",
                DisplayNameDe = "Kunst-, Musik-, Theater- und Medienwissenschaften 103",
                DisplayNameEn = "Fine Arts, Music, Theatre and Media Studies 103"
            }
        );
    }
}