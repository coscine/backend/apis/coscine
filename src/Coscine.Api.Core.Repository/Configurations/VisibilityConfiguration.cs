﻿using Coscine.Api.Core.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Coscine.Api.Core.Repository.Configurations;

public class VisibilityConfiguration : IEntityTypeConfiguration<Visibility>
{
    public void Configure(EntityTypeBuilder<Visibility> builder)
    {
        // Seed data using HasData
        builder.HasData(
            new Visibility
            {
                Id = Guid.Parse("8AB9C883-EB0D-4402-AAAD-2E4007BADCE6"),
                DisplayName = "Project Members"
            },
            new Visibility
            {
                Id = Guid.Parse("451788B0-AAA1-4419-999D-77A7FA25A080"),
                DisplayName = "Public"
            }
            // Add more seed data as needed
        );
    }
}