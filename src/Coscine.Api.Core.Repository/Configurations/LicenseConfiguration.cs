﻿using Coscine.Api.Core.Entities.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Coscine.Api.Core.Repository.Configurations;

public class LicenseConfiguration : IEntityTypeConfiguration<License>
{
    public void Configure(EntityTypeBuilder<License> builder)
    {
        // Seed data using HasData
        builder.HasData(
            new License
            {
                Id = Guid.Parse("1CBBE05E-4DD8-4933-9B32-03DBA59CAA5D"),
                DisplayName = "Mozilla Public License 2.0",
                Url = new("http://spdx.org/licenses/MPL-2.0")
            },
            new License
            {
                Id = Guid.Parse("48593A5B-1DCE-40E2-A269-0588C59EEB36"),
                DisplayName = "CC BY NC 4.0 (Attribution-NonCommercial)",
                Url = new("http://spdx.org/licenses/CC-BY-NC-4.0")
            },
            new License
            {
                Id = Guid.Parse("66EBFA84-6874-49AB-8517-3B92814EF28C"),
                DisplayName = "CC BY 4.0 (Attribution)",
                Url = new("http://spdx.org/licenses/CC-BY-4.0")
            },
            new License
            {
                Id = Guid.Parse("569D79FE-7AC4-4CB0-A968-4196E3ED11BE"),
                DisplayName = "CC BY NC SA 4.0 (Attribution-NonCommercial-ShareAlike)",
                Url = new("http://spdx.org/licenses/CC-BY-NC-SA-4.0")
            },
            new License
            {
                Id = Guid.Parse("A439949F-96D2-4DCF-9275-48D78A5AC028"),
                DisplayName = "GNU General Public License v3.0",
                Url = new("http://spdx.org/licenses/GPL-3.0-only")
            },
            new License
            {
                Id = Guid.Parse("7F2D774D-1EB3-4CEC-AB3D-4D7BD299785A"),
                DisplayName = "MIT License",
                Url = new("http://spdx.org/licenses/MIT")
            },
            new License
            {
                Id = Guid.Parse("0935A00D-0546-4297-B64F-5277BDC944CC"),
                DisplayName = "Eclipse Public License 1.0",
                Url = new("http://spdx.org/licenses/EPL-1.0")
            },
            new License
            {
                Id = Guid.Parse("5B1AC517-FB6F-416E-9C1A-5CF75FBA9F1F"),
                DisplayName = "CC BY NC ND 4.0 (Attribution-NonCommercial-NoDerivatives)",
                Url = new("http://spdx.org/licenses/CC-BY-NC-ND-4.0")
            },
            new License
            {
                Id = Guid.Parse("0EE2948B-ED07-4ECE-9F32-75C06D8C112C"),
                DisplayName = "GNU General Public License v2.0",
                Url = new("http://spdx.org/licenses/GPL-2.0-only")
            },
            new License
            {
                Id = Guid.Parse("4E7AF4F3-FB52-4914-8457-970721FCC09F"),
                DisplayName = "CC BY SA 4.0 (Attribution-ShareAlike)",
                Url = new("http://spdx.org/licenses/CC-BY-SA-4.0")
            },
            new License
            {
                Id = Guid.Parse("F9EB4F7E-6BA8-4CCD-A08B-A3C8D9C07835"),
                DisplayName = "BSD 3-clause 'New' or 'Revised' License",
                Url = new("http://spdx.org/licenses/BSD-3-Clause")
            },
            new License
            {
                Id = Guid.Parse("9A98A097-B1B1-4E15-8871-ACC9E74EBEB2"),
                DisplayName = "BSD 2-clause 'Simplified' License",
                Url = new("http://spdx.org/licenses/BSD-2-Clause")
            },
            new License
            {
                Id = Guid.Parse("F6110B01-835E-41FD-942E-D099783ADAD7"),
                DisplayName = "Apache License 2.0",
                Url = new("http://spdx.org/licenses/Apache-2.0")
            },
            new License
            {
                Id = Guid.Parse("323DA6CC-B9DC-4D2A-B512-F0689262B840"),
                DisplayName = "GNU Lesser General Public License v3.0",
                Url = new("http://spdx.org/licenses/LGPL-3.0-only")
            },
            new License
            {
                Id = Guid.Parse("74D9911B-398B-4241-90B9-F213EBDEE2A9"),
                DisplayName = "GNU Affero General Public License v3.0",
                Url = new("http://spdx.org/licenses/AGPL-3.0-only")
            },
            new License
            {
                Id = Guid.Parse("1882C0F2-70B0-4A5A-8114-F440E333DE9A"),
                DisplayName = "The Unlicense",
                Url = new("http://spdx.org/licenses/Unlicense")
            },
            new License
            {
                Id = Guid.Parse("B414C963-C5B4-44E7-96D8-F6DF55EE2507"),
                DisplayName = "CC BY ND 4.0 (Attribution-NoDerivatives)",
                Url = new("http://spdx.org/licenses/CC-BY-ND-4.0")
            },
            new License
            {
                Id = Guid.Parse("B974004B-DF00-4872-92DC-F93F1448AB98"),
                DisplayName = "GNU Lesser General Public License v2.1",
                Url = new("http://spdx.org/licenses/LGPL-2.1-only")
            }
        );
    }
}