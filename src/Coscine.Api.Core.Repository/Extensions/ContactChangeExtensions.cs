﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class ContactChangeExtensions
{
    public static IQueryable<ContactChange> FilterByEditDateBefore(this IQueryable<ContactChange> contactChanges, DateTime? editDateBefore)
    {
        if (editDateBefore is null)
        {
            return contactChanges;
        }
        return contactChanges.Where(cc => cc.EditDate < editDateBefore);
    }

    public static IQueryable<ContactChange> SortContactChanges(this IQueryable<ContactChange> contactChanges, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return contactChanges.OrderByDescending(e => e.RelationId);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<ContactChange>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return contactChanges.OrderByDescending(e => e.RelationId);
        }

        return contactChanges.OrderBy(orderQuery);
    }
}