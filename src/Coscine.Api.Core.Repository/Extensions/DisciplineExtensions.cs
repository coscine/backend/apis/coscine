﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class DisciplineExtensions
{
    public static IQueryable<Discipline> SortDisciplines(this IQueryable<Discipline> disciplines, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return disciplines.OrderBy(e => e.Id);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<Discipline>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return disciplines.OrderBy(e => e.Id);
        }

        return disciplines.OrderBy(orderQuery);
    }
}