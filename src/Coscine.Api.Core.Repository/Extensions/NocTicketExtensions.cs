using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Extensions.Utility;
using Coscine.Api.Core.Shared.Enums;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class NocTicketExtensions
{
    public static IEnumerable<NocTicket> TakeAfterStartDate(this IEnumerable<NocTicket> nocTickets, DateTimeOffset? date)
        => date is not null ? nocTickets.Where(e => e.StartAt > date) : nocTickets;

    public static IEnumerable<NocTicket> TakeAfterEndDate(this IEnumerable<NocTicket> nocTickets, DateTimeOffset? date)
        => date is not null ? nocTickets.Where(e => e.EndAt > date) : nocTickets;

    public static IEnumerable<NocTicket> TakeBeforeStartDate(this IEnumerable<NocTicket> nocTickets, DateTimeOffset? date)
        => date is not null ? nocTickets.Where(e => e.StartAt < date) : nocTickets;

    public static IEnumerable<NocTicket> TakeBeforeEndDate(this IEnumerable<NocTicket> nocTickets, DateTimeOffset? date)
        => date is not null ? nocTickets.Where(e => e.EndAt < date) : nocTickets;

    public static IEnumerable<NocTicket> FilterByMessageType(this IEnumerable<NocTicket> nocTickets, MessageType? messageType)
        => messageType is null ? nocTickets : nocTickets.Where(e => e.Type == messageType);

    public static IEnumerable<NocTicket> SearchNocTickets(this IEnumerable<NocTicket> nocTickets, string? searchTerm)
    {
        if (string.IsNullOrWhiteSpace(searchTerm))
        {
            return nocTickets;
        }

        return nocTickets.Where(e => 
            e.Message.Contains(searchTerm, StringComparison.InvariantCultureIgnoreCase) || 
            e.ShortMessage.Contains(searchTerm, StringComparison.InvariantCultureIgnoreCase) ||
            e.TecMessage.Contains(searchTerm, StringComparison.InvariantCultureIgnoreCase) ||
            e.Title.Contains(searchTerm, StringComparison.InvariantCultureIgnoreCase)
        );
    }

    public static IEnumerable<NocTicket> SortNocTickets(this IEnumerable<NocTicket> nocTickets, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return nocTickets.OrderBy(e => e.Id);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<NocTicket>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return nocTickets.OrderBy(e => e.Id);
        }

        return nocTickets.AsQueryable().OrderBy(orderQuery);
    }
}