﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class TitleExtensions
{
    public static IQueryable<Title> SortTitles(this IQueryable<Title> titles, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return titles.OrderBy(e => e.Id);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<Title>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return titles.OrderBy(e => e.Id);
        }

        return titles.OrderBy(orderQuery);
    }
}