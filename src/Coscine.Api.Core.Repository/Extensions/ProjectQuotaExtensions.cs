using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class ProjectQuotaExtensions
{
    public static IQueryable<ProjectQuota> SortProjectQuotas(this IQueryable<ProjectQuota> projectQuotas, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return projectQuotas.OrderBy(e => e.RelationId);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<ProjectQuota>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return projectQuotas.OrderBy(e => e.RelationId);
        }

        return projectQuotas.OrderBy(orderQuery);
    }
}