using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.Enums;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class MessageExtensions
{
    public static IEnumerable<Message> TakeAfterStartDate(this IEnumerable<Message> messages, DateTimeOffset? date)
        => date is not null ? messages.Where(e => e.StartDate > date) : messages;

    public static IEnumerable<Message> TakeAfterEndDate(this IEnumerable<Message> messages, DateTimeOffset? date)
        => date is not null ? messages.Where(e => e.EndDate > date) : messages;

    public static IEnumerable<Message> TakeBeforeStartDate(this IEnumerable<Message> messages, DateTimeOffset? date)
        => date is not null ? messages.Where(e => e.StartDate < date) : messages;

    public static IEnumerable<Message> TakeBeforeEndDate(this IEnumerable<Message> messages, DateTimeOffset? date)
        => date is not null ? messages.Where(e => e.EndDate < date) : messages;

    public static IEnumerable<Message> FilterByMessageType(this IEnumerable<Message> messages, MessageType? messageType)
        => messageType is null ? messages : messages.Where(e => e.Type == messageType);

    public static IEnumerable<Message> SearchMessages(this IEnumerable<Message> messages, string? searchTerm)
    {
        if (string.IsNullOrWhiteSpace(searchTerm))
        {
            return messages;
        }

        return messages.Where(e => 
            e.Body.Any(e => e.Value.Contains(searchTerm, StringComparison.InvariantCultureIgnoreCase)) || 
            (e.Title != null && e.Title.Contains(searchTerm, StringComparison.InvariantCultureIgnoreCase))
        );
    }
}