﻿using VDS.RDF;

namespace Coscine.Api.Core.Repository.Extensions;

/// <summary>
/// Utility functions to simplify interactions with graphs.
/// </summary>
internal static class GraphExtensions
{
    /// <summary>
    /// Asserts the given triple using <see cref="string"/> parameters to the graph.
    /// </summary>
    /// <param name="graph">The graph to which the triple will be asserted.</param>
    /// <param name="subject">The subject of the triple as a string URI.</param>
    /// <param name="predicate">The predicate of the triple as a string URI.</param>
    /// <param name="uriObject">The object of the triple as a URI.</param>
    public static void AssertToGraph(this IGraph graph, string subject, string predicate, Uri uriObject)
    {
        AssertToGraph(graph, new Uri(subject), new Uri(predicate), uriObject);
    }

    /// <summary>
    /// Asserts the given triple using <see cref="Uri"/> parameters to the graph.
    /// </summary>
    /// <param name="graph">The graph to which the triple will be asserted.</param>
    /// <param name="subject">The subject of the triple as a URI.</param>
    /// <param name="predicate">The predicate of the triple as a URI.</param>
    /// <param name="uriObject">The object of the triple as a URI.</param>
    public static void AssertToGraph(this IGraph graph, Uri subject, Uri predicate, Uri uriObject)
    {
        graph.Assert(
            new Triple(
                graph.CreateUriNode(subject),
                graph.CreateUriNode(predicate),
                graph.CreateUriNode(uriObject)
            )
        );
    }

    /// <summary>
    /// Asserts the given triple using <see cref="Uri"/> parameters to the graph.
    /// </summary>
    /// <param name="graph">The graph to which the triple will be asserted.</param>
    /// <param name="subject">The subject of the triple as a URI.</param>
    /// <param name="predicate">The predicate of the triple as a URI.</param>
    /// <param name="uriObject">The object of the triple as a INode.</param>
    public static void AssertToGraph(this IGraph graph, Uri subject, Uri predicate, INode uriObject)
    {
        graph.Assert(
            new Triple(
                graph.CreateUriNode(subject),
                graph.CreateUriNode(predicate),
                uriObject
            )
        );
    }

    /// <summary>
    /// Asserts the given triple using <see cref="Uri"/> parameters to the graph.
    /// </summary>
    /// <param name="graph">The graph to which the triple will be asserted.</param>
    /// <param name="subject">The subject of the triple as a INode.</param>
    /// <param name="predicate">The predicate of the triple as a URI.</param>
    /// <param name="uriObject">The object of the triple as a Uri.</param>
    public static void AssertToGraph(this IGraph graph, INode subject, Uri predicate, Uri uriObject)
    {
        graph.Assert(
            new Triple(
                subject,
                graph.CreateUriNode(predicate),
                graph.CreateUriNode(uriObject)
            )
        );
    }

    /// <summary>
    /// Asserts the given triple using <see cref="Uri"/> parameters to the graph.
    /// </summary>
    /// <param name="graph">The graph to which the triple will be asserted.</param>
    /// <param name="subject">The subject of the triple as a INode.</param>
    /// <param name="predicate">The predicate of the triple as a URI.</param>
    /// <param name="uriObject">The object of the triple as a INode.</param>
    public static void AssertToGraph(this IGraph graph, INode subject, Uri predicate, INode uriObject)
    {
        graph.Assert(
            new Triple(
                subject,
                graph.CreateUriNode(predicate),
                uriObject
            )
        );
    }

    /// <summary>
    /// Asserts the given triple using <see cref="string"/> parameters and a <i>literal object</i> to the graph.
    /// </summary>
    /// <param name="graph">The graph to which the triple will be asserted.</param>
    /// <param name="subject">The subject of the triple as a string URI.</param>
    /// <param name="predicate">The predicate of the triple as a string URI.</param>
    /// <param name="literalObject">The object of the triple as a literal string.</param>
    /// <param name="objectType">Optional URI that represents the datatype of the literal object.</param>
    public static void AssertToGraph(this IGraph graph, string subject, string predicate, string literalObject, Uri? objectType = null)
    {
        AssertToGraph(graph, new Uri(subject), new Uri(predicate), literalObject, objectType);
    }

    /// <summary>
    /// Asserts the given triple using <see cref="Uri"/> parameters and a <i>literal object</i> to the graph.
    /// </summary>
    /// <param name="graph">The graph to which the triple will be asserted.</param>
    /// <param name="subject">The subject of the triple as a URI.</param>
    /// <param name="predicate">The predicate of the triple as a URI.</param>
    /// <param name="literalObject">The object of the triple as a literal string.</param>
    /// <param name="objectType">Optional URI that represents the datatype of the literal object.</param>
    public static void AssertToGraph(this IGraph graph, Uri subject, Uri predicate, string literalObject, Uri? objectType = null)
    {
        graph.Assert(
            new Triple(
                graph.CreateUriNode(subject),
                graph.CreateUriNode(predicate),
                objectType != null ? graph.CreateLiteralNode(literalObject, objectType) : graph.CreateLiteralNode(literalObject)
            )
        );
    }

    /// <summary>
    /// Asserts the given triple using <see cref="Uri"/> parameters and a <i>literal object</i> to the graph.
    /// </summary>
    /// <param name="graph">The graph to which the triple will be asserted.</param>
    /// <param name="subject">The subject of the triple as a INode.</param>
    /// <param name="predicate">The predicate of the triple as a URI.</param>
    /// <param name="literalObject">The object of the triple as a literal string.</param>
    /// <param name="objectType">Optional URI that represents the datatype of the literal object.</param>
    public static void AssertToGraph(this IGraph graph, INode subject, Uri predicate, string literalObject, Uri? objectType = null)
    {
        graph.Assert(
            new Triple(
                subject,
                graph.CreateUriNode(predicate),
                objectType != null ? graph.CreateLiteralNode(literalObject, objectType) : graph.CreateLiteralNode(literalObject)
            )
        );
    }
}