﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using Coscine.Api.Core.Shared.Enums;
using System.Linq.Dynamic.Core;
using System.Text.RegularExpressions;

namespace Coscine.Api.Core.Repository.Extensions;

public static class ActivityLogExtensions
{
    /// <summary>
    /// Filters the provided collection of activity logs to exclude entries before the specified timestamp.
    /// </summary>
    /// <param name="activityLogs">The collection of activity logs to filter.</param>
    /// <param name="beforeTimestamp">The timestamp before which activity logs should be excluded. If <c>null</c>, no filtering is applied.</param>
    /// <returns>An <see cref="IQueryable{ActivityLog}"/> that contains activity logs on or after the specified timestamp.</returns>
    public static IQueryable<ActivityLog> ExcludeBeforeTimestamp(this IQueryable<ActivityLog> activityLogs, DateTime? beforeTimestamp)
    {
        if (beforeTimestamp is null)
        {
            return activityLogs;
        }

        return activityLogs.Where(e => e.ActivityTimestamp >= beforeTimestamp);
    }

    /// <summary>
    /// Filters the provided collection of activity logs to exclude entries after the specified timestamp.
    /// </summary>
    /// <param name="activityLogs">The collection of activity logs to filter.</param>
    /// <param name="afterTimestamp">The timestamp after which activity logs should be excluded. If <c>null</c>, no filtering is applied.</param>
    /// <returns>An <see cref="IQueryable{ActivityLog}"/> that contains activity logs on or before the specified timestamp.</returns>
    public static IQueryable<ActivityLog> ExcludeAfterTimestamp(this IQueryable<ActivityLog> activityLogs, DateTime? afterTimestamp)
    {
        if (afterTimestamp is null)
        {
            return activityLogs;
        }

        return activityLogs.Where(e => e.ActivityTimestamp <= afterTimestamp);
    }

    /// <summary>
    /// Filters the provided collection of activity logs based on a regex pattern.
    /// </summary>
    /// <param name="activityLogs">The collection of activity logs to filter.</param>
    /// <param name="regularExpression">The regex pattern to match against the API path, action name and controller name. If <c>null</c> or whitespace, no filtering is applied.</param>
    /// <returns>An <see cref="IEnumerable{ActivityLog}"/> that contains activity logs matching the regex pattern.</returns>
    /// <remarks>Note that this method can only be used on an in-memory enumerable, that contains data already fetched from the database and comes with a higher performance overhead.</remarks>
    public static IEnumerable<ActivityLog> FilterByRegularExpression(this IEnumerable<ActivityLog> activityLogs, string? regularExpression)
    {
        if (string.IsNullOrWhiteSpace(regularExpression))
        {
            return activityLogs;
        }

        var regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant;
        // We want to filter by the API path, action name and controller name
        return activityLogs.Where(e =>
            Regex.IsMatch(e.ApiPath, regularExpression, regexOptions) ||
            e.ActionName != null && Regex.IsMatch(e.ActionName, regularExpression, regexOptions) ||
            e.ControllerName != null && Regex.IsMatch(e.ControllerName, regularExpression, regexOptions)
        );
    }

    /// <summary>
    /// Filters the provided collection of activity logs to include only those with a specified GUID in their ID or API path.
    /// </summary>
    /// <param name="activityLogs">The collection of activity logs to filter.</param>
    /// <param name="guid">The GUID to filter by. If <c>null</c>, no filtering is applied.</param>
    /// <returns>An <see cref="IQueryable{ActivityLog}"/> that contains activity logs with the specified GUID.</returns>
    public static IQueryable<ActivityLog> FilterByGuid(this IQueryable<ActivityLog> activityLogs, Guid? guid)
    {
        if (guid is null)
        {
            return activityLogs;
        }

        // We want to filter by both the identified GUID and the GUID as part of the API path
        return activityLogs.Where(e =>
            e.Id.Equals(guid) ||
            e.ApiPath.Contains(guid.Value.ToString())
        );
    }

    /// <summary>
    /// Filters the provided collection of activity logs to include only those associated with a specified user ID.
    /// </summary>
    /// <param name="activityLogs">The collection of activity logs to filter.</param>
    /// <param name="userId">The user ID to filter by. If <c>null</c>, no filtering is applied.</param>
    /// <returns>An <see cref="IQueryable{ActivityLog}"/> that contains activity logs associated with the specified user ID.</returns>
    public static IQueryable<ActivityLog> FilterByUserId(this IQueryable<ActivityLog> activityLogs, Guid? userId)
    {
        if (userId is null)
        {
            return activityLogs;
        }

        return activityLogs.Where(e => e.UserId == userId.Value);
    }

    /// <summary>
    /// Filters the provided collection of activity logs to include only those that match a specified HTTP method.
    /// </summary>
    /// <param name="activityLogs">The collection of activity logs to filter.</param>
    /// <param name="httpMethod">The HTTP method to filter by. If <c>null</c> or whitespace, no filtering is applied.</param>
    /// <returns>An <see cref="IQueryable{ActivityLog}"/> that contains activity logs with the specified HTTP method.</returns>
    public static IQueryable<ActivityLog> FilterByHttpMethod(this IQueryable<ActivityLog> activityLogs, CoscineHttpMethod? httpMethod)
    {
        var method = httpMethod?.ToString();
        if (string.IsNullOrWhiteSpace(method))
        {
            return activityLogs;
        }

        return activityLogs.Where(e => e.HttpAction == method);
    }

    public static IQueryable<ActivityLog> SortActivityLogs(this IQueryable<ActivityLog> activityLogs, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return activityLogs.OrderByDescending(e => e.ActivityTimestamp);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<ActivityLog>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return activityLogs.OrderByDescending(e => e.ActivityTimestamp);
        }

        return activityLogs.OrderBy(orderQuery);
    }
}