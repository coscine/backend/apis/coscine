﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class ExternalAuthenticatorExtensions
{
    public static IQueryable<ExternalAuthenticator> SortTitles(this IQueryable<ExternalAuthenticator> externalAuths, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return externalAuths.OrderBy(e => e.Id);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<ExternalAuthenticator>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return externalAuths.OrderBy(e => e.Id);
        }

        return externalAuths.OrderBy(orderQuery);
    }
}