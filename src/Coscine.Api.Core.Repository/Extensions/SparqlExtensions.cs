﻿using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Expressions;
using VDS.RDF.Nodes;
using VDS.RDF.Query;

namespace Coscine.Api.Core.Repository.Extensions;

public static class SparqlExtensions
{
    public static void SetAbsoluteUri(this SparqlParameterizedString query, string name, Uri value)
    {
        query.SetUri(name, new AlwaysAbsoluteUri(value.ToString()));
    }

    /// <summary>
    /// Executes a count query based on the provided SPARQL query and returns the count of results.
    /// </summary>
    /// <param name="query">A SparqlParameterizedString instance representing the SPARQL query.</param>
    /// <param name="rdfRepositoryBase">An instance of IRdfRepository representing the repository where the query will be executed.</param>
    /// <returns>An integer representing the count of results of the query.</returns>
    public static async Task<long> GetCountOrDefaultAsync(this SparqlParameterizedString query, IRdfRepositoryBase rdfRepositoryBase)
    {
        // Create a copy of the original query for the count
        var countQuery = new SparqlParameterizedString
        {
            CommandText = $"SELECT (COUNT(*) AS ?count) WHERE {{ {query.CommandText} }}",
            // Set the namespaces and parameters from the original query
            Namespaces = query.Namespaces
        };

        foreach (var parameter in query.Parameters.ToList())
        {
            countQuery.SetParameter(parameter.Key, parameter.Value);
        }

        // Execute count query
        var countResultSet = await rdfRepositoryBase.RunQueryAsync(countQuery);

        // Get the count result
        var countResult = countResultSet?.FirstOrDefault();
        if (countResult is not null && countResult.TryGetValue("count", out var countNode))
        {
            var value = countNode.AsValuedNode();
            return value.AsInteger();
        }
        else
        {
            return 0;
        }
    }

    /// <summary>
    /// Adds pagination to the SPARQL query, specifying the page size and page number.
    /// </summary>
    /// <param name="query">The original SPARQL query.</param>
    /// <param name="pageSize">The number of results per page.</param>
    /// <param name="pageNumber">The page number.</param>
    /// <returns>The paginated SPARQL query.</returns>
    public static SparqlParameterizedString Paginate(this SparqlParameterizedString query, int pageSize, int pageNumber)
    {
        // Define the pagination sub-query
        var pagination = new SparqlParameterizedString
        {
            CommandText = @"
            LIMIT @numberOfResults
            OFFSET @offset
            "
        };

        // Build paginated value query
        query.Append(pagination);
        query.SetLiteral("numberOfResults", pageSize);
        query.SetLiteral("offset", (pageNumber - 1) * pageSize);

        return query;
    }

    /// <summary>
    /// Sorts the SPARQL query by the specified order.
    /// </summary>
    /// <typeparam name="T">The type of the object being queried.</typeparam>
    /// <param name="query">The original SPARQL query.</param>
    /// <param name="orderByQueryString">The order by query string.</param>
    /// <param name="propertyMapping">The mapping of SPARQL query variables to entity object properties.</param>
    /// <returns>The sorted SPARQL query.</returns>
    /// <remarks>For the ordering to work properly, all <c>string</c> SPARQL variables must be outputted as <c>STR(?variable)</c>.</remarks>
    public static SparqlParameterizedString Sort<T>(this SparqlParameterizedString query, string? orderByQueryString, Dictionary<string, Expression<Func<T, object?>>> propertyMapping)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return query;
        }
        var orderQueryString = OrderQueryBuilder.CreateSparqlOrderQuery(query, orderByQueryString, propertyMapping);

        if (string.IsNullOrWhiteSpace(orderQueryString))
        {
            return query;
        }

        // Define the order sub-query
        var orderQuery = new SparqlParameterizedString
        {
            CommandText = $@"
            ORDER BY {orderQueryString}
            "
        };

        // Build ordered value query
        query.Append(orderQuery);

        return query;
    }
}