﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class FileTreeExtensions
{
    public static IEnumerable<FileTree> SortFileTrees(this IEnumerable<FileTree> fileTree, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return fileTree.OrderBy(x => x.HasChildren).ThenBy(x => x.Path);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<FileTree>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return fileTree.OrderBy(x => x.HasChildren).ThenBy(x => x.Path);
        }

        return fileTree.AsQueryable().OrderBy(orderQuery);
    }
}