﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class LanguageExtensions
{
    public static IQueryable<Language> SortLanguages(this IQueryable<Language> languages, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return languages.OrderBy(e => e.Id);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<Language>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return languages.OrderBy(e => e.Id);
        }

        return languages.OrderBy(orderQuery);
    }
}