﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class LicenseExtensions
{
    public static IQueryable<License> SortLicenses(this IQueryable<License> licenses, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return licenses.OrderBy(e => e.Id);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<License>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return licenses.OrderBy(e => e.Id);
        }

        return licenses.OrderBy(orderQuery);
    }
}