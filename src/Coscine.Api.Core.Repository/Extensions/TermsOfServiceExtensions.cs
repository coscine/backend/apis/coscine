﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class TermsOfServiceExtensions
{
    public static IQueryable<Tosaccepted> FilterByVersion(this IQueryable<Tosaccepted> termsOfService, string? version)
    {
        if (string.IsNullOrWhiteSpace(version))
        {
            return termsOfService;
        }
        return termsOfService.Where(tos => version == tos.Version);
    }

    public static IQueryable<Tosaccepted> SortTermsOfService(this IQueryable<Tosaccepted> termsOfService, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return termsOfService.OrderBy(e => e.RelationId);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<Tosaccepted>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return termsOfService.OrderBy(e => e.RelationId);
        }

        return termsOfService.OrderBy(orderQuery);
    }
}