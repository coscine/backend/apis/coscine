using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class VisibilityExtensions
{
    public static IQueryable<Visibility> SortVisibilities(this IQueryable<Visibility> visibilities, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return visibilities.OrderBy(e => e.Id);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<Visibility>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return visibilities.OrderBy(e => e.Id);
        }

        return visibilities.OrderBy(orderQuery);
    }
}