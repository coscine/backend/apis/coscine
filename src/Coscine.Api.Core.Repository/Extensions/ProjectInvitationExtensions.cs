using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class ProjectInvitationExtensions
{
    public static IQueryable<Invitation> SortProjectInvitations(this IQueryable<Invitation> projectInvitations, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return projectInvitations.OrderBy(e => e.Id);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<Invitation>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return projectInvitations.OrderBy(e => e.Id);
        }

        return projectInvitations.OrderBy(orderQuery);
    }
}