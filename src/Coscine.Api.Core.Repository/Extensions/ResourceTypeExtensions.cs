using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class ResourceTypeExtensions
{
    public static IQueryable<ResourceType> SortResourceTypes(this IQueryable<ResourceType> resourceTypes, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return resourceTypes.OrderBy(e => e.Id);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<ResourceType>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return resourceTypes.OrderBy(e => e.Id);
        }

        return resourceTypes.OrderBy(orderQuery);
    }
}