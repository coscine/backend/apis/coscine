﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

/// <summary>
/// Provides extension methods for <see cref="IEnumerable{}"/> of <see cref="HandleValue"/> entities.
/// </summary>
public static class HandleValueExtensions
{
    /// <summary>
    /// Sorts a collection of <see cref="HandleValue"/> objects based on a specified order query string.
    /// </summary>
    /// <param name="handleValues">The collection of <see cref="HandleValue"/> objects to be sorted.</param>
    /// <param name="orderByQueryString">The order query string used to determine the sorting order of the collection. If the string is <c>null</c> or whitespace, the collection is sorted by the <c>Idx</c> property in ascending order.</param>
    /// <returns>A sorted <see cref="IEnumerable{HandleValue}"/> based on the order query string. If the order query string is not provided or invalid, the collection is returned sorted by the <c>Idx</c> property in ascending order.</returns>
    public static IEnumerable<HandleValue> SortHandleValues(this IEnumerable<HandleValue> handleValues, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return handleValues.OrderBy(e => e.Idx);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<HandleValue>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return handleValues.OrderBy(e => e.Idx);
        }

        return handleValues
            .AsQueryable()
            .OrderBy(orderQuery)
            .AsEnumerable();
    }
}