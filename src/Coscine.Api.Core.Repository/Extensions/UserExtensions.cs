﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class UserExtensions
{
    public static IQueryable<User> SearchUsers(this IQueryable<User> users, string? searchTerm)
    {
        if (string.IsNullOrWhiteSpace(searchTerm))
        {
            return users;
        }

        var lowercaseSearchTerm = searchTerm.ToLower().Trim();

        return users.Where(u =>
            (u.Givenname != null && u.Givenname.ToLower().Contains(lowercaseSearchTerm))
            || (u.Surname != null && u.Surname.ToLower().Contains(lowercaseSearchTerm))
            || (u.EmailAddress != null && u.EmailAddress.ToLower().Contains(lowercaseSearchTerm))
            || ((u.Givenname + " " + u.Surname).ToLower().Contains(lowercaseSearchTerm))
            || (u.Title != null && (u.Title.DisplayName + " " + u.Givenname + " " + u.Surname).ToLower().Contains(lowercaseSearchTerm))
        );
    }

    public static IQueryable<User> SortUsers(this IQueryable<User> users, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return users.OrderBy(e => e.Givenname);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<User>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return users.OrderBy(e => e.Givenname);
        }

        return users.OrderBy(orderQuery);
    }
}