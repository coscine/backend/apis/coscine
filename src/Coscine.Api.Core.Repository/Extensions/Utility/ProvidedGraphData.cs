﻿using VDS.RDF;

namespace Coscine.Api.Core.Repository.Extensions.Utility;

public class ProvidedGraphData
{
    /// <summary>
    /// The provenance graph.
    /// </summary>
    public IGraph? TrellisGraph { get; set; } = null;

    /// <summary>
    /// Triples the old data graph contained.
    /// </summary>
    public IGraph? OldDataGraph { get; set; } = null;

    /// <summary>
    /// Triples the old metadata graph contained.
    /// </summary>
    public IGraph? OldMetadataGraph { get; set; } = null;

    /// <summary>
    /// The current data version graph.
    /// </summary>
    public IGraph? CurrentDataVersionGraph { get; set; } = null;

    /// <summary>
    /// The current data version.
    /// </summary>
    public Uri? RecentDataVersion { get; set; } = null;

    /// <summary>
    /// The current metadata version graph.
    /// </summary>
    public IGraph? CurrentMetadataVersionGraph { get; set; } = null;

    /// <summary>
    /// The current metadata version graph.
    /// </summary>
    public Uri? RecentMetadataVersion { get; set; } = null;

    /// <summary>
    /// The new version.
    /// </summary>
    public long? NewVersion { get; set; } = null;
}