﻿using System.Diagnostics.CodeAnalysis;

namespace Coscine.Api.Core.Repository.Extensions.Utility;

/// <summary>
/// A <see cref="Uri"/> that always returns the absolute URI when <c>.ToString()</c> is called. 
/// This ensures that the returned URI is always encoded correctly.
/// </summary>
/// <param name="uriString">The string to parse into a <see cref="Uri"/>.</param>
internal class AlwaysAbsoluteUri([StringSyntax("Uri")] string uriString) : Uri(uriString)
{
    public override string ToString() => AbsoluteUri;
}