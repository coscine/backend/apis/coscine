﻿namespace Coscine.Api.Core.Repository.Extensions.Utility;

/// <summary>
/// Utility class for managing and determining versions of research data and metadata.
/// </summary>
internal static class MetadataVersionUtility
{
    /// <summary>
    /// The maximum file size for research data which should be analyzed by e.g. Metadata Extractor or Tracker.
    /// </summary>
    /// <remarks>
    /// This property defines the maximum number of bytes that research data files can have to be eligible for analysis
    /// by components like the Metadata Extractor or Tracker.
    /// The value is set to 16,000,000 bytes, which is equivalent to 16 MB.
    /// </remarks>
    /// <value>The maximum file size, in bytes, for research data that can be analyzed.</value>
    public static long DetectionByteLimit => 16 * 1000 * 1000;

    /// <summary>
    /// Gets the most recent version of a graph based on the given parameters.
    /// </summary>
    /// <param name="graphs">A collection of graph URIs.</param>
    /// <param name="filter">Optional filter for the type of the graph (e.g., 'data' or 'metadata').</param>
    /// <param name="notFilterExtracted">If <c>true</c>, the method filters out graphs with 'extracted' parameter. If <c>false</c>, it includes them.</param>
    /// <returns>The URI of the most recent graph version based on the given parameters, or <c>null</c> if no suitable version is found.</returns>
    public static Uri? GetRecentVersion(IEnumerable<Uri>? graphs, string? filter = null, bool notFilterExtracted = true)
    {
        if (graphs is null)
        {
            return null;
        }
        var recentVersion = GetRecentVersion(graphs.Select((graph) => graph.AbsoluteUri), filter, notFilterExtracted);
        if (recentVersion is not null)
        {
            return new Uri(recentVersion);
        }
        return null;
    }

    /// <summary>
    /// Retrieves the recent version of a graph based on the given parameters.
    /// </summary>
    /// <param name="graphs">A collection of graph URIs represented as strings.</param>
    /// <param name="filter">Optional filter for the type of the graph (e.g., 'data' or 'metadata').</param>
    /// <param name="notFilterExtracted">If <c>true</c>, the method filters out graphs with 'extracted' parameter. If <c>false</c>, it includes them.</param>
    /// <returns>The string representation of the most recent graph version based on the given parameters, or <c>null</c> if no suitable version is found.</returns>
    public static string? GetRecentVersion(IEnumerable<string>? graphs, string? filter = null, bool notFilterExtracted = true)
    {
        if (graphs is null)
        {
            return null;
        }
        string? currentBest = null;
        var currentBestVersion = -1L;
        foreach (var graph in graphs)
        {
            var queryDictionary = System.Web.HttpUtility.ParseQueryString(
                new Uri(graph.Replace("@", "?")).Query);
            var version = queryDictionary["version"];
            if (version is null || !long.TryParse(version, out long longVersion))
            {
                continue;
            }
            if (longVersion > currentBestVersion
                && (filter is null || queryDictionary["type"] == filter)
                && ((notFilterExtracted && queryDictionary["extracted"] is null)
                    || (!notFilterExtracted && queryDictionary["extracted"] is not null))
            )
            {
                currentBestVersion = longVersion;
                currentBest = graph;
            }
        }
        return currentBest;
    }

    /// <summary>
    /// Retrieves the most recent data version of a graph.
    /// </summary>
    /// <param name="graphs">A collection of graph URIs.</param>
    /// <returns>The URI of the most recent data graph version, or <c>null</c> if no suitable version is found.</returns>
    /// <seealso cref="GetRecentVersion(IEnumerable{Uri}?, string?, bool)"/>
    public static Uri? GetRecentDataVersion(IEnumerable<Uri>? graphs)
    {
        return GetRecentVersion(graphs, "data");
    }

    /// <summary>
    /// Retrieves the most recent data version based on the provided graph URIs.
    /// </summary>
    /// <param name="graphs">A collection of graph URIs represented as strings.</param>
    /// <returns>The string representation of the most recent data graph version, or <c>null</c> if no suitable version is found.</returns>
    /// <seealso cref="GetRecentVersion(IEnumerable{Uri}?, string?, bool)"/>
    public static string? GetRecentDataVersion(IEnumerable<string>? graphs)
    {
        return GetRecentVersion(graphs, "data");
    }

    /// <summary>
    /// Retrieves the most recent extracted data version based on the provided graph URIs.
    /// </summary>
    /// <param name="graphs">A collection of graph URIs.</param>
    /// <returns>The URI of the most recent extracted data graph version, or <c>null</c> if no suitable version is found.</returns>
    /// <seealso cref="GetRecentVersion(IEnumerable{Uri}?, string?, bool)"/>
    public static Uri? GetRecentDataExtractedVersion(IEnumerable<Uri> graphs)
    {
        return GetRecentVersion(graphs, "data", false);
    }

    /// <summary>
    /// Retrieves the most recent extracted data version based on the provided graph URIs represented as strings.
    /// </summary>
    /// <param name="graphs">A collection of graph URIs represented as strings.</param>
    /// <returns>The string representation of the most recent extracted data graph version, or <c>null</c> if no suitable version is found.</returns>
    /// <seealso cref="GetRecentVersion(IEnumerable{Uri}?, string?, bool)"/>
    public static string? GetRecentDataExtractedVersion(IEnumerable<string> graphs)
    {
        return GetRecentVersion(graphs, "data", false);
    }

    /// <summary>
    /// Retrieves the most recent metadata version based on the provided graph URIs.
    /// </summary>
    /// <param name="graphs">A collection of graph URIs.</param>
    /// <returns>The URI of the most recent metadata graph version, or <c>null</c> if no suitable version is found.</returns>
    /// <seealso cref="GetRecentVersion(IEnumerable{Uri}?, string?, bool)"/>
    public static Uri? GetRecentMetadataVersion(IEnumerable<Uri>? graphs)
    {
        return GetRecentVersion(graphs, "metadata");
    }

    /// <summary>
    /// Retrieves the most recent metadata version based on the provided graph URIs represented as strings.
    /// </summary>
    /// <param name="graphs">A collection of graph URIs represented as strings.</param>
    /// <returns>The string representation of the most recent metadata graph version, or <c>null</c> if no suitable version is found.</returns>
    /// <seealso cref="GetRecentVersion(IEnumerable{Uri}?, string?, bool)"/>
    public static string? GetRecentMetadataVersion(IEnumerable<string>? graphs)
    {
        return GetRecentVersion(graphs, "metadata");
    }

    /// <summary>
    /// Retrieves the most recent extracted metadata version based on the provided graph URIs.
    /// </summary>
    /// <param name="graphs">A collection of graph URIs.</param>
    /// <returns>The URI of the most recent extracted metadata graph version, or <c>null</c> if no suitable version is found.</returns>
    /// <seealso cref="GetRecentVersion(IEnumerable{Uri}?, string?, bool)"/>
    public static Uri? GetRecentMetadataExtractedVersion(IEnumerable<Uri>? graphs)
    {
        return GetRecentVersion(graphs, "metadata", false);
    }

    /// <summary>
    /// Retrieves the most recent extracted metadata version based on the provided graph URIs represented as strings.
    /// </summary>
    /// <param name="graphs">A collection of graph URIs represented as strings.</param>
    /// <returns>The string representation of the most recent extracted metadata graph version, or <c>null</c> if no suitable version is found.</returns>
    /// <seealso cref="GetRecentVersion(IEnumerable{Uri}?, string?, bool)"/>
    public static string? GetRecentMetadataExtractedVersion(IEnumerable<string>? graphs)
    {
        return GetRecentVersion(graphs, "metadata", false);
    }

    /// <summary>
    /// Creates a new version identifier based on the current timestamp.
    /// </summary>
    /// <returns>A <c>long</c> representing the version identifier.</returns>
    /// <seealso cref="CalculateVersion(DateTime)"/>
    public static long GetNewVersion()
    {
        // UTC Timestamp
        return CalculateVersion(DateTime.UtcNow);
    }

    /// <summary>
    /// Calculates a new version number based on the given timestamp.
    /// </summary>
    /// <param name="dateTime">The DateTime object representing the timestamp.</param>
    /// <returns>A <c>long</c> integer representing the new version number.</returns>
    public static long CalculateVersion(DateTime dateTime)
    {
        return long.Parse(Convert.ToString((int)dateTime.Subtract(new DateTime(1970, 1, 1)).TotalSeconds));
    }

    /// <summary>
    /// Extracts the version number from the given graph URI represented as a string.
    /// </summary>
    /// <param name="graphUri">String representation of the given graph URI.</param>
    /// <returns>A nullable <c>long</c> integer representing the version number, or <c>null</c> if the parsing fails or input is <c>null</c>.</returns>
    public static long? GetVersion(string? graphUri)
    {
        if (graphUri is null)
        {
            return null;
        }
        var queryDictionary = System.Web.HttpUtility.ParseQueryString(
            new Uri(graphUri.Replace("@", "?")).Query);
        var version = queryDictionary["version"];
        var couldParse = long.TryParse(version, out long longVersion);
        return couldParse ? longVersion : null;
    }

    /// <summary>
    /// Extracts the version number from the given graph URI.
    /// </summary>
    /// <param name="graphUri">The Uri object representing the graph URI.</param>
    /// <returns>A nullable <c>long</c> integer representing the version number, or <c>null</c> if input is <c>null</c>.</returns>
    public static long? GetVersion(Uri? graphUri)
    {
        if (graphUri is null)
        {
            return null;
        }
        return GetVersion(graphUri.AbsoluteUri);
    }
}