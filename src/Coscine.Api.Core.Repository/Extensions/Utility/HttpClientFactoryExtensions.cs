﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using GitLabApiClient;
using Microsoft.Net.Http.Headers;
using System.Net.Http.Headers;
using System.Text;

namespace Coscine.Api.Core.Repository.Extensions.Utility;

/// <summary>
/// Provides extension methods for the <see cref="IHttpClientFactory"/> to create and configure instances of <see cref="HttpClient"/>.
/// </summary>
public static class HttpClientFactoryExtensions
{
    /// <summary>
    /// Creates and configures an <see cref="HttpClient"/> using the provided PID (Persistent Identifier) configuration settings.
    /// </summary>
    /// <param name="httpClientFactory">The factory instance used to create <see cref="HttpClient"/> instances.</param>
    /// <param name="pidConfiguration">The PID configuration settings, including URL, user, and password, used to configure the <see cref="HttpClient"/> instance.</param>
    /// <returns>A configured <see cref="HttpClient"/> instance that is ready to use for making HTTP requests.</returns>
    public static HttpClient CreateHandleClient(this IHttpClientFactory httpClientFactory, PidConfiguration pidConfiguration)
    {
        var apiUrl = new Uri($"{pidConfiguration.Url.TrimEnd('/')}/");

        var handleClient = httpClientFactory.CreateClient(ClientName.HandleClient);

        handleClient.BaseAddress = apiUrl;

        var byteArray = Encoding.ASCII.GetBytes($"{pidConfiguration.User}:{pidConfiguration.Password}");
        handleClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
        handleClient.DefaultRequestHeaders.Add(HeaderNames.Accept, "application/json, */*");

        return handleClient;
    }

    /// <summary>
    /// Creates and configures an <see cref="HttpClient"/> using the provided NOC-Portal (Network Operations Center) configuration settings.
    /// </summary>
    /// <param name="httpClientFactory">The factory instance used to create <see cref="HttpClient"/> instances.</param>
    /// <param name="nocConfiguration">The NOC-Portal configuration settings, including URL, user, password and basic auth token, used to configure the <see cref="HttpClient"/> instance.</param>
    /// <returns>A configured <see cref="HttpClient"/> instance that is ready to use for making HTTP requests.</returns>
    public static HttpClient CreateNocClient(this IHttpClientFactory httpClientFactory, NocConfiguration nocConfiguration)
    {
        var nocClient = httpClientFactory.CreateClient(ClientName.NocClient);

        nocClient.BaseAddress = nocConfiguration.Endpoint;
        nocClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", $"{nocConfiguration.BasicAuthToken}");
        nocClient.DefaultRequestHeaders.Add(HeaderNames.Accept, "application/json, */*");

        return nocClient;
    }

    public static HttpClient CreateGitLabHttpClient(this IHttpClientFactory httpClientFactory, GitlabResourceType gitlabResourceType)
    {
        //TODO: Add client definition
        var httpClient = httpClientFactory.CreateClient();

        httpClient.DefaultRequestHeaders.Add("PRIVATE-TOKEN", gitlabResourceType.ProjectAccessToken);

        return httpClient;
    }
}