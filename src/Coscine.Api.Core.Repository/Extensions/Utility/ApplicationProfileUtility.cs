﻿using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared;
using System.Text.RegularExpressions;
using VDS.RDF;
using VDS.RDF.Shacl;

namespace Coscine.Api.Core.Repository.Extensions.Utility;

internal partial class ApplicationProfileUtility
{
    private readonly IRdfRepositoryBase _rdfRepositoryBase;
    private readonly IResourceRepository _resourceRepository;

    public ApplicationProfileUtility(IRdfRepositoryBase rdfRepositoryBase, IResourceRepository resourceRepository)
    {
        _rdfRepositoryBase = rdfRepositoryBase;
        _resourceRepository = resourceRepository;
    }

    public async Task<IGraph> GetApplicationProfile(Guid resourceId)
    {
        IGraph applicationProfileGraph = new Graph();
        var resource = await _resourceRepository.GetAsync(resourceId, trackChanges: false);
        if (resource?.ApplicationProfile is null)
        {
            return applicationProfileGraph;
        }

        return await _rdfRepositoryBase.GetGraph(new Uri(resource.ApplicationProfile));
    }

    /// <summary>
    /// Asynchronously retrieves the full application profile, resolving all owl imports statements.
    /// </summary>
    /// <param name="applicationProfileUri">The URI of the SHACL application profile against which the graph is to be validated.</param>
    /// <returns>A task returning the full application profile.</returns>
    public async Task<IGraph> GetFullApplicationProfile(Uri applicationProfileUri)
    {
        var shapesGraph = await _rdfRepositoryBase.GetGraph(applicationProfileUri);
        await ResolveOwlImports(shapesGraph);
        return shapesGraph;
    }

    /// <summary>
    /// Asynchronously validates the conformance of a given RDF graph to a specified SHACL application profile.
    /// </summary>
    /// <param name="graph">The RDF graph that needs to be validated.</param>
    /// <param name="shapesGraph">The SHACL application profile against which the graph is to be validated.</param>
    /// <returns>A Task returning a boolean value that indicates whether the graph conforms to the SHACL application profile.</returns>
    public async Task<bool> ValidateApplicationProfile(IGraph graph, IGraph shapesGraph)
    {
        // Create a copy of the input graph
        var dataGraph = new Graph();

        dataGraph.Merge(graph);

        foreach (var classElement in GetClasses(shapesGraph))
        {
            var classGraph = await _rdfRepositoryBase.GetGraph(classElement);
            dataGraph.Merge(classGraph);
        }

        var processor = new ShapesGraph(shapesGraph);
        return processor.Conforms(dataGraph);
    }

    [GeneratedRegex("""\{\{(.*?)\}\}""")]
    public static partial Regex TemplateRegex();
    /// <summary>
    /// This method deactivates specific SHACL rules if a template value is provided.
    /// </summary>
    /// <param name="dataGraph">The RDF graph that needs to be validated.</param>
    /// <param name="shapesGraph">The SHACL application profile against which the graph is to be validated.</param>
    public void DeactivateSpecificRules(IGraph dataGraph, IGraph shapesGraph)
    {
        // Usage of ToArray since dataGraph is being mutated in the foreach loop
        foreach (var triple in dataGraph.Triples.ToArray())
        {
            var objectValue = triple.Object as ILiteralNode;
            if (objectValue is null)
            {
                continue;
            }
            if (!TemplateRegex().IsMatch(objectValue.Value))
            {
                continue;
            }

            // Make sure that the given value is a string (otherwise storing later breaks)
            dataGraph.Retract(triple);
            dataGraph.Assert(triple.Subject, triple.Predicate, dataGraph.CreateLiteralNode(objectValue.Value));

            // Get the relevant rules in the shapes graph and deactivate them if they occur as templated metadata
            var relevantRules = shapesGraph.GetTriplesWithPredicateObject(
                shapesGraph.CreateUriNode(RdfUris.ShaclPath),
                triple.Predicate
            ).Select(x => x.Subject).ToList();
            foreach (var subject in relevantRules)
            {
                shapesGraph.Assert(
                    subject,
                    shapesGraph.CreateUriNode(RdfUris.ShaclDeactivated),
                    shapesGraph.CreateLiteralNode("true", RdfUris.XsdBoolean)
                );
            }
        }
    }

    /// <summary>
    /// Asynchronously resolves and imports external RDF graphs referenced in the given shapes graph through owl:imports.
    /// </summary>
    /// <param name="shapesGraph">The RDF graph that contains shapes and may reference other graphs using owl:imports.</param>
    /// <returns>A Task representing the asynchronous operation.</returns>
    private async Task ResolveOwlImports(IGraph shapesGraph)
    {
        var toAnalyzeGraphs = new Queue<IGraph>();
        toAnalyzeGraphs.Enqueue(shapesGraph);
        var visitedGraphs = new List<Uri> { shapesGraph.BaseUri };
        var owlImports = RdfUris.OwlImports;
        while (toAnalyzeGraphs.Count > 0)
        {
            var currentGraph = toAnalyzeGraphs.Dequeue();
            foreach (var nextGraphNode in currentGraph
                                            .GetTriplesWithPredicate(owlImports)
                                            .Select((triple) => triple.Object)
                                            .ToList()
            )
            {
                if (nextGraphNode is IUriNode)
                {
                    var nextGraphUri = (nextGraphNode as IUriNode)?.Uri;
                    if (nextGraphUri is not null && !visitedGraphs.Contains(nextGraphUri) && await _rdfRepositoryBase.HasGraphAsync(nextGraphUri))
                    {
                        visitedGraphs.Add(nextGraphUri);
                        var nextGraph = await _rdfRepositoryBase.GetGraph(nextGraphUri);
                        shapesGraph.Merge(nextGraph);
                        toAnalyzeGraphs.Enqueue(nextGraph);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Retrieves the URIs of RDF classes defined in a given graph.
    /// </summary>
    /// <param name="graph">The RDF graph from which to extract the URIs of RDF classes.</param>
    /// <returns>An enumerable collection of URIs representing the RDF classes defined in the given graph.</returns>
    public static IEnumerable<Uri> GetClasses(IGraph graph)
    {
        return graph.GetTriplesWithPredicate(RdfUris.Class)
            .Where(x => x.Object.NodeType == NodeType.Uri)
            .Select(x => (x.Object as IUriNode)?.Uri)
            .Where(x => x is not null)
            .Cast<Uri>()
            .Distinct();
    }
}