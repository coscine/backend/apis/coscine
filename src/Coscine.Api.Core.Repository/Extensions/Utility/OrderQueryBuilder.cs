﻿using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using VDS.RDF.Query;

namespace Coscine.Api.Core.Repository.Extensions.Utility;

public static class OrderQueryBuilder
{
    public static string CreateOrderQuery<T>(string orderByQueryString)
    {
        var orderParams = orderByQueryString.Trim().Split(',');
        var propertyInfos = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        var orderQueryBuilder = new StringBuilder();

        foreach (var param in orderParams)
        {
            if (string.IsNullOrWhiteSpace(param))
            {
                continue;
            }

            var propertyFromQueryName = param.Trim().Split(" ")[0];
            var objectProperty = Array.Find(propertyInfos, pi => pi.Name.Equals(propertyFromQueryName, StringComparison.InvariantCultureIgnoreCase));

            if (objectProperty is null)
            {
                continue;
            }

            var direction = param.EndsWith(" desc") ? "descending" : "ascending";
            orderQueryBuilder.Append(objectProperty.Name).Append(' ').Append(direction).Append(", ");
        }

        return orderQueryBuilder.ToString().TrimEnd(',', ' ');
    }

    public static string CreateSparqlOrderQuery<T>(SparqlParameterizedString query, string orderByQueryString, Dictionary<string, Expression<Func<T, object?>>> propertyMapping)
    {
        var orderParams = orderByQueryString.Trim().Split(',');
        var propertyInfos = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        var orderQueryBuilder = new StringBuilder();

        foreach (var param in orderParams)
        {
            if (string.IsNullOrWhiteSpace(param))
            {
                continue;
            }

            var propertyFromQueryName = param.Trim().Split(" ")[0];
            var objectProperty = Array.Find(propertyInfos, pi => pi.Name.Equals(propertyFromQueryName, StringComparison.InvariantCultureIgnoreCase));

            if (objectProperty is null)
            {
                continue;
            }

            var direction = param.EndsWith(" desc") ? "DESC" : "ASC";

            var sparqlVariable = propertyMapping.FirstOrDefault(p => GetPropertyInfo(p.Value).Equals(objectProperty)).Key;

            if (sparqlVariable is null) { continue; }

            // ASC(LCASE(STR(?name)))
            orderQueryBuilder
                .Append(direction)
                .Append('(')
                .Append("LCASE(STR(")
                .Append('?')
                .Append(sparqlVariable)
                .Append(")))")
                .Append(' ');
        }

        return orderQueryBuilder.ToString().Trim();
    }

    private static PropertyInfo GetPropertyInfo<T>(Expression<Func<T, object?>> expression)
    {
        if (expression.Body is MemberExpression memberExpression)
        {
            return (PropertyInfo)memberExpression.Member;
        }
        else if (expression.Body is UnaryExpression unaryExpression
            && unaryExpression.Operand is MemberExpression nestedMemberExpression)
        {
            // If the property is not a reference type (for example, int, Enum, etc.),
            // the body of the lambda expression will be a UnaryExpression
            return (PropertyInfo)nestedMemberExpression.Member;
        }
        else
        {
            throw new ArgumentException($"The expression: {expression} is invalid.", nameof(expression));
        }
    }
}