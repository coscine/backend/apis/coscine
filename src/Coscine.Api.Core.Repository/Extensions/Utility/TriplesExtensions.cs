﻿using VDS.RDF;

namespace Coscine.Api.Core.Repository.Extensions.Utility;

/// <summary>
/// Class containing helper methods for working with triples.
/// </summary>
public static class TriplesExtensions
{
    /// <summary>
    /// Converts triples with relative URIs to absolute URIs.
    /// </summary>
    /// <param name="triples">The collection of triples to convert.</param>
    /// <returns>An enumerable of triples with absolute URIs.</returns>
    public static IEnumerable<Triple> AbsoluteUrisForTriples(this IEnumerable<Triple> triples)
    {
        return triples.Select(t => new Triple(
            t.Subject is IUriNode ? new UriNode(new AlwaysAbsoluteUri(t.Subject.ToString())) : t.Subject,
            t.Predicate is IUriNode ? new UriNode(new AlwaysAbsoluteUri(t.Predicate.ToString())) : t.Predicate,
            t.Object is IUriNode ? new UriNode(new AlwaysAbsoluteUri(t.Object.ToString())) : t.Object));
    }

    /// <summary>
    /// Chunks the grouped triples into smaller chunks.
    /// </summary>
    /// <param name="groupedTriples">The collection of grouped triples.</param>
    /// <param name="chunkSize">The maximum size of each chunk.</param>
    /// <returns>A collection of chunks of triples.</returns>
    public static IEnumerable<List<Triple>> ChunkGrouped(this IEnumerable<IEnumerable<Triple>> groupedTriples, int chunkSize)
    {
        var currentChunk = new List<Triple>(); // Initialize an empty chunk

        foreach (var group in groupedTriples)
        {
            if (group.All(t => t.IsGroundTriple) && group.Count() > chunkSize)
            {
                // Special handling for the group containing only grounded triples
                foreach (var chunk in group.Chunk(chunkSize))
                {
                    yield return chunk.ToList(); // Yield each sub-chunk as a list
                }
            }
            else
            {
                // Check if adding the current group would exceed the chunk size
                if (currentChunk.Count + group.Count() > chunkSize)
                {
                    yield return currentChunk; // Yield the current chunk
                    currentChunk = []; // Start a new chunk
                }

                currentChunk.AddRange(group); // Add the current group to the chunk
            }
        }

        if (currentChunk.Count > 0)
        {
            yield return currentChunk; // Yield the last chunk if it's not empty
        }
    }

    /// <summary>
    /// Groups triples by blank nodes and their descendants to ensure that all related triples stay together.
    /// </summary>
    /// <param name="triples">The collection of triples to group.</param>
    /// <returns>A collection of groups of triples.</returns>
    /// <remarks>
    /// This grouping is necessary because splitting triples that reference the same blank nodes across different chunks
    /// can break the graph, as the references to blank nodes would be lost.
    /// TODO: Explore the usage of context to manage blank nodes more effectively.
    /// </remarks>
    public static IEnumerable<IEnumerable<Triple>> GroupTriplesByBlankNodes(this IEnumerable<Triple> triples)
    {
        var blankNodeGroups = new Dictionary<INode, HashSet<Triple>>();
        var processedNodes = new HashSet<INode>();  // Track nodes already processed
        var groundTriples = new List<Triple>();     // List to hold ground triples (without blank nodes)

        // Index triples by subjects and objects for faster lookup
        var triplesBySubject = triples.GroupBy(t => t.Subject).ToDictionary(g => g.Key, g => g.ToList());
        var triplesByObject = triples.GroupBy(t => t.Object).ToDictionary(g => g.Key, g => g.ToList());

        // Recursive method to add triples to the correct group based on blank nodes
        void AddToGroup(INode node, HashSet<Triple> group, HashSet<INode> visitedNodes)
        {
            if (!visitedNodes.Add(node)) return; // Prevent infinite recursion by avoiding re-processing the same node

            // Add triples where the node is the subject
            if (triplesBySubject.TryGetValue(node, out var subjectTriples))
            {
                foreach (var triple in subjectTriples)
                {
                    if (group.Add(triple)) // Add the triple to the group if it's not already present
                    {
                        // Recursively process connected blank nodes
                        if (triple.Subject.NodeType == NodeType.Blank && !processedNodes.Contains(triple.Subject))
                        {
                            AddToGroup(triple.Subject, group, visitedNodes);
                        }
                        if (triple.Object.NodeType == NodeType.Blank && !processedNodes.Contains(triple.Object))
                        {
                            AddToGroup(triple.Object, group, visitedNodes);
                        }
                    }
                }
            }

            // Add triples where the node is the object
            if (triplesByObject.TryGetValue(node, out var objectTriples))
            {
                foreach (var triple in objectTriples)
                {
                    if (group.Add(triple)) // Add the triple to the group if it's not already present
                    {
                        // Recursively process connected blank nodes
                        if (triple.Subject.NodeType == NodeType.Blank && !processedNodes.Contains(triple.Subject))
                        {
                            AddToGroup(triple.Subject, group, visitedNodes);
                        }
                        if (triple.Object.NodeType == NodeType.Blank && !processedNodes.Contains(triple.Object))
                        {
                            AddToGroup(triple.Object, group, visitedNodes);
                        }
                    }
                }
            }
        }

        // Iterate through all triples to group them based on blank nodes
        foreach (var triple in triples)
        {
            if (!triple.IsGroundTriple) // Process only triples containing blank nodes
            {
                if (triple.Subject.NodeType == NodeType.Blank && !processedNodes.Contains(triple.Subject))
                {
                    var group = new HashSet<Triple>();
                    AddToGroup(triple.Subject, group, []);
                    // Mark nodes as processed
                    foreach (var t in group)
                    {
                        processedNodes.Add(t.Subject);
                        processedNodes.Add(t.Object);
                    }
                    blankNodeGroups[triple.Subject] = group; // Store the group
                }
                else if (triple.Object.NodeType == NodeType.Blank && !processedNodes.Contains(triple.Object))
                {
                    var group = new HashSet<Triple>();
                    AddToGroup(triple.Object, group, []);
                    // Mark nodes as processed
                    foreach (var t in group)
                    {
                        processedNodes.Add(t.Subject);
                        processedNodes.Add(t.Object);
                    }
                    blankNodeGroups[triple.Object] = group; // Store the group
                }
            }
            else
            {
                groundTriples.Add(triple); // Collect ground triples separately
            }
        }

        // Combine the results into a list
        var result = blankNodeGroups.Values.Cast<IEnumerable<Triple>>().ToList();
        if (groundTriples.Count != 0)
        {
            result.Add(groundTriples); // Add ground triples as a separate group if they exist
        }

        return result;
    }
}