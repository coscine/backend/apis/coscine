﻿// Ignore Spelling: versioned ldp

using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.HelperModels;
using Coscine.Api.Core.Shared;
using System.Globalization;
using VDS.RDF;
using VDS.RDF.Parsing;
using VDS.RDF.Query;

namespace Coscine.Api.Core.Repository.Extensions.Utility;

/// <summary>
/// Utility class for managing the creation and setup of RDF graphs based on metadata and data associated with a resource.
/// </summary>
/// <remarks>
/// This class provides a suite of methods for creating and setting up various types of RDF graphs
/// that are related to a specific resource. The graphs can represent the metadata and data associated with the resource,
/// as well as different versions of this metadata and data.
/// </remarks>
/// <remarks>
/// Initializes a new instance of the <see cref="MetadataUtility"/> class with the specified RDF repository.
/// </remarks>
/// <param name="rdfRepository">The RDF repository that this utility will interact with.</param>
internal class MetadataUtility(IRdfRepositoryBase rdfRepository)
{
    private readonly IRdfRepositoryBase _rdfRepositoryBase = rdfRepository;


    /// <summary>
    /// Asynchronously creates a collection of RDF graphs representing the resource structure and metadata.
    /// 
    /// The method generates the following graphs:
    /// <list type="bullet">
    ///   <item>
    ///     <description>
    ///       <b>New File Graph</b>: Represents the new file or folder within the resource (URI: <c>https://purl.org/coscine/resources/{resourceId}/{path}</c>). 
    ///       Initialized as a DCAT Catalog and FDP Metadata Service by asserting appropriate RDF types.
    ///     </description>
    ///   </item>
    ///   <item>
    ///     <description>
    ///       <strong>Resource Graph</strong>: Represents the resource itself within the system (URI: <c>https://purl.org/coscine/resources/{resourceId}</c>).
    ///     </description>
    ///   </item>
    ///   <item>
    ///     <description>
    ///       <b>Metadata File Graph</b>: Contains metadata specific to the new file or folder, such as descriptions and annotations (URI: <c>https://purl.org/coscine/resources/{resourceId}/{path}/@type=metadata</c>).
    ///     </description>
    ///   </item>
    ///   <item>
    ///     <description>
    ///       <b>Data File Graph</b>: Contains data associated with the new file, such as content references or data properties (URI: <c>https://purl.org/coscine/resources/{resourceId}/{path}/@type=data</c>).
    ///     </description>
    ///   </item>
    ///   <item>
    ///     <description>
    ///       <b>Trellis Graph</b>: Contains server-managed properties and system-level metadata, following the Trellis Linked Data Platform specifications (URI: Defined by <c>http://www.trellisldp.org/ns/trellis#PreferServerManaged</c>). 
    ///       Provided via the <paramref name="providedGraphData"/> parameter.
    ///     </description>
    ///   </item>
    /// </list>
    /// 
    /// The graphs are constructed to represent the hierarchical structure of resources and their associated metadata.
    /// Additional RDF triples are asserted to establish relationships and types, facilitating metadata management and retrieval.
    /// </summary>
    /// <param name="resourceId">The unique identifier for the resource.</param>
    /// <param name="path">The relative path used to construct the graph names.</param>
    /// <param name="providedGraphData">
    /// Optional data used for graph creation, such as the Trellis graph and version information. Default is <c>null</c>.
    /// </param>
    /// <returns>
    /// A task that represents the asynchronous operation.
    /// The task result contains an <see cref="IEnumerable{T}"/> of <see cref="IGraph"/> objects representing the created graphs.
    /// </returns>
    public async Task<IEnumerable<IGraph>> CreateGraphsAsync(
        Guid resourceId,
        string path,
        ProvidedGraphData? providedGraphData = null
    )
    {
        var graphs = new List<IGraph>();

        var resourceGraphName = $"{RdfUris.CoscineResources}{resourceId}";

        var newFileGraphName = $"{resourceGraphName}/{path}";

        var newMetadataFileGraphName = $"{newFileGraphName}/@type=metadata";
        var newDataFileGraphName = $"{newFileGraphName}/@type=data";

        var newFileGraph = await _rdfRepositoryBase.GetGraph(new Uri(newFileGraphName));
        graphs.Add(newFileGraph);

        newFileGraph.AssertToGraph(newFileGraph.BaseUri, RdfUris.A, RdfUris.DcatCatalogClass);
        newFileGraph.AssertToGraph(newFileGraph.BaseUri, RdfUris.A, RdfUris.FdpMetadataServiceClass);
        await AddFilesToAFolderAsync(new Uri(newFileGraphName), resourceGraphName, graphs, providedGraphData);

        var metadataFileGraph = await SetMetadataGraphAsync(graphs, newMetadataFileGraphName, newFileGraph, newFileGraphName, providedGraphData);
        await SetDataGraphAsync(graphs, newDataFileGraphName, newFileGraph, newFileGraphName, metadataFileGraph.BaseUri.AbsoluteUri, providedGraphData);

        if (providedGraphData?.TrellisGraph != null)
        {
            graphs.Add(providedGraphData.TrellisGraph);
        }

        return graphs;
    }

    /// <summary>
    /// Asynchronously sets up the metadata graph.
    /// </summary>
    /// <param name="graphs">A list of graphs to which the new metadata graph will be added.</param>
    /// <param name="newMetadataFileGraphName">The name for the new metadata file graph.</param>
    /// <param name="newFileGraph">The new file graph associated with this metadata.</param>
    /// <param name="fileUri">The URI for the file associated with this metadata.</param>
    /// <param name="providedGraphData">Optional data used for graph creation.</param>
    /// <returns>
    /// A task that represents the asynchronous operation.
    /// The task result contains the <see cref="IGraph"/> representing the created metadata graph.
    /// </returns>
    private async Task<IGraph> SetMetadataGraphAsync(List<IGraph> graphs, string newMetadataFileGraphName, IGraph newFileGraph, string fileUri, ProvidedGraphData? providedGraphData)
    {
        var metadataUri = new Uri(newMetadataFileGraphName);
        var metadataFileGraph = PatchGraph.Empty(newMetadataFileGraphName);
        graphs.Add(metadataFileGraph);
        await AddToTrellisAsync(RdfUris.LdpRDFSourceClass, fileUri, newMetadataFileGraphName, providedGraphData?.TrellisGraph);
        newFileGraph.AssertToGraph(new Uri(fileUri), RdfUris.DcatCatalog, metadataUri);
        newFileGraph.AssertToGraph(new Uri(fileUri), RdfUris.FdpHasMetadata, metadataUri);
        metadataFileGraph.AssertToGraph(metadataUri, RdfUris.A, RdfUris.DcatCatalogClass);
        return metadataFileGraph;
    }

    /// <summary>
    /// Asynchronously sets up the data graph.
    /// </summary>
    /// <param name="graphs">A list of graphs to which the new data graph will be added.</param>
    /// <param name="newDataFileGraphName">The name for the new data file graph.</param>
    /// <param name="newFileGraph">The new file graph associated with this data.</param>
    /// <param name="fileUri">The URI for the file associated with this data.</param>
    /// <param name="metadataFileUriString">The URI for the metadata file associated with this data.</param>
    /// <param name="providedGraphData">Optional data used for graph creation.</param>
    /// <returns>
    /// A task that represents the asynchronous operation. The task result contains the <see cref="IGraph"/> representing the created data graph.
    /// </returns>
    private async Task<IGraph> SetDataGraphAsync(List<IGraph> graphs, string newDataFileGraphName, IGraph newFileGraph, string fileUri, string metadataFileUriString, ProvidedGraphData? providedGraphData)
    {
        var dataUri = new Uri(newDataFileGraphName);
        var dataFileGraph = PatchGraph.Empty(newDataFileGraphName);
        graphs.Add(dataFileGraph);
        await AddToTrellisAsync(RdfUris.LdpNonRDFSourceClass, fileUri, newDataFileGraphName, providedGraphData?.TrellisGraph);
        newFileGraph.AssertToGraph(new Uri(fileUri), RdfUris.DcatCatalog, dataUri);
        dataFileGraph.AssertToGraph(dataUri, RdfUris.LdpDescribedBy, new Uri(metadataFileUriString));
        dataFileGraph.AssertToGraph(dataUri, RdfUris.A, RdfUris.DcatCatalogClass);
        return dataFileGraph;
    }

    /// <summary>
    /// Sets the linked data information in the given data version graph.
    /// </summary>
    /// <param name="linkedGraph">The graph containing linked data information.</param>
    /// <param name="currentDataVersionGraph">The current data version graph where the linked data should be set.</param>
    /// <param name="otherURICandidate">The URI candidate for linking data.</param>
    public void SetLinkedData(IGraph linkedGraph, IGraph currentDataVersionGraph, Uri otherURICandidate)
    {
        var linkedBody = currentDataVersionGraph.GetTriplesWithPredicate(RdfUris.CoscineTermsLinkedBody);
        if (linkedGraph != null)
        {
            var setLinkedBody = linkedGraph.GetTriplesWithPredicate(RdfUris.CoscineTermsLinkedBody);
            if (!linkedBody.Any()
                    || linkedBody.First().Object.ToString()
                        != setLinkedBody.First().Object.ToString())
            {
                var oldDataVersionNode = currentDataVersionGraph.CreateUriNode(currentDataVersionGraph.BaseUri);
                var currentDataVersionNode = currentDataVersionGraph.CreateUriNode(otherURICandidate);
                currentDataVersionGraph.BaseUri = otherURICandidate;
                foreach (var currentLinkedBody in linkedBody)
                {
                    currentDataVersionGraph.Retract(currentLinkedBody);
                }
                foreach (var oldTriple in currentDataVersionGraph.GetTriplesWithSubject(oldDataVersionNode))
                {
                    currentDataVersionGraph.Retract(oldTriple);
                    currentDataVersionGraph.Assert(new Triple(
                        currentDataVersionNode,
                        oldTriple.Predicate,
                        oldTriple.Object
                    ));
                }
                foreach (var currentSetLinkedBody in setLinkedBody)
                {
                    currentDataVersionGraph.Assert(new Triple(
                        currentDataVersionNode,
                        currentDataVersionGraph.CreateUriNode(RdfUris.CoscineTermsLinkedBody),
                        currentSetLinkedBody.Object
                    ));
                }
            }
        }
    }

    /// <summary>
    /// Asynchronously adds files to a folder represented by a graph in RDF.
    /// </summary>
    /// <param name="fileGraph">The URI representing the file graph.</param>
    /// <param name="resourceGraphName">The name of the resource graph.</param>
    /// <param name="graphs">A list of IGraph instances representing the graphs to add.</param>
    /// <param name="providedGraphData">Optional data containing graphs and version information.</param>
    /// <returns>A Task representing the asynchronous operation.</returns>
    private async Task AddFilesToAFolderAsync(Uri fileGraph, string resourceGraphName, List<IGraph> graphs, ProvidedGraphData? providedGraphData)
    {
        string rootUri;
        var fileGraphUri = fileGraph.AbsoluteUri;
        do
        {
            rootUri = fileGraphUri[..fileGraphUri.LastIndexOf('/')];
            var rootGraph = PatchGraph.Empty(rootUri);
            rootGraph.AssertToGraph(new Uri(rootUri), RdfUris.DcatCatalog, new Uri(fileGraphUri));
            rootGraph.AssertToGraph(new Uri(rootUri), RdfUris.A, RdfUris.DcatCatalogClass);
            await AddToTrellisAsync(RdfUris.LdpBasicContainerClass, rootUri, fileGraphUri, providedGraphData?.TrellisGraph);
            graphs.Add(rootGraph);
            fileGraphUri = rootUri;
        } while (rootUri != resourceGraphName && rootUri.Contains('/'));
    }

    /// <summary>
    /// Asynchronously retrieves a list of graph URIs from a Trellis graph based on a specified graph name.
    /// </summary>
    /// <param name="graphId">The name of the graph to search for within the Trellis graph.</param>
    /// <returns>A Task returning an IEnumerable of URIs representing the list of graph URIs found.</returns>
    public async Task<IEnumerable<Uri>> ListGraphsFromTrellisGraphAsync(string graphId, Uri? ldpClass = null)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT ?s
                WHERE {
                    GRAPH @trellisGraph {
                        ?s a @ldpRdfSource .

                        FILTER(
                            CONTAINS(STR(?s), @graphId)
                        ) .
                    }
                }
            "
        };

        // Set namespaces, literals and URIs
        query.SetUri("trellisGraph", RdfUris.TrellisGraph);
        query.SetUri("ldpRdfSource", ldpClass ?? RdfUris.LdpRDFSourceClass);
        query.SetLiteral("graphId", graphId, normalizeValue: true);

        // Build and execute count query
        var count = await query.GetCountOrDefaultAsync(_rdfRepositoryBase);

        // Set page size and calculate total number of available pages
        var pageSize = _rdfRepositoryBase.QUERY_LIMIT;
        var totalPages = (int)Math.Ceiling((double)count / pageSize);

        var trellisIds = new List<Uri>();
        // Iterate through each page and retrieve results
        for (var pageNumber = 1; pageNumber <= totalPages; pageNumber++)
        {
            query.Paginate(pageSize, pageNumber);
            var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

            // Extract URIs from the result set and add to the list
            trellisIds.AddRange(
                resultSet.Select(r => new Uri(r.Value("s").ToString()))
            );
        }

        return trellisIds;
    }

    /// <summary>
    /// Asynchronously adds a graph to Trellis with specified LDP (Linked Data Platform) assignment URI.
    /// </summary>
    /// <param name="ldpAssignmentUri">The LDP assignment URI for the graph.</param>
    /// <param name="thePartUri">The part URI string for the graph.</param>
    /// <param name="graphUri">The URI string for the graph.</param>
    /// <param name="trellisGraph">Optional graph data to be used; a new graph is created if none is provided.</param>
    /// <returns>A Task representing the asynchronous operation.</returns>
    public async Task AddToTrellisAsync(Uri ldpAssignmentUri, string thePartUri, string graphUri, IGraph? trellisGraph = null)
    {
        var graphSet = trellisGraph != null;

        trellisGraph ??= PatchGraph.Empty(RdfUris.TrellisGraph);

        var setGraphNode = trellisGraph.CreateUriNode(new Uri(graphUri));
        var setThePartNode = trellisGraph.CreateUriNode(new Uri(thePartUri));
        var triple = new Triple(
            setGraphNode,
            trellisGraph.CreateUriNode(RdfUris.DcTermsIsPartOf),
            setThePartNode
        );

        if (!trellisGraph.ContainsTriple(triple))
        {
            trellisGraph.Assert(triple);
            var assignmentTriple = new Triple(
                setGraphNode,
                trellisGraph.CreateUriNode(RdfUris.A),
                trellisGraph.CreateUriNode(ldpAssignmentUri)
            );
            trellisGraph.Assert(assignmentTriple);
            AddModifiedDate(trellisGraph, graphUri);
        }

        if (!graphSet)
        {
            await _rdfRepositoryBase.AddGraphAsync(trellisGraph);
        }
    }

    /// <summary>
    /// Adds a modification date to the given graph.
    /// </summary>
    /// <param name="graph">The graph to which the modification date should be added.</param>
    /// <param name="root">The root URI string for the graph.</param>
    public static void AddModifiedDate(IGraph graph, string root)
    {
        var dcTermsModifiedNode = graph.CreateUriNode(RdfUris.DcTermsModified);
        var rootNode = graph.CreateUriNode(new Uri(root));
        if (!graph.GetTriplesWithSubjectPredicate(rootNode, dcTermsModifiedNode).Any())
        {
            var triple = new Triple(
                rootNode,
                dcTermsModifiedNode,
                graph.CreateLiteralNode(
                    DateTime.UtcNow.ToString("o", CultureInfo.InvariantCulture),
                    new Uri(XmlSpecsHelper.XmlSchemaDataTypeDateTime)));
            graph.Assert(triple);
        }
    }
}