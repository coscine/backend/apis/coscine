using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class RoleExtensions
{
    public static IQueryable<Role> SortRoles(this IQueryable<Role> roles, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return roles.OrderBy(e => e.Id);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<Role>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return roles.OrderBy(e => e.Id);
        }

        return roles.OrderBy(orderQuery);
    }
}