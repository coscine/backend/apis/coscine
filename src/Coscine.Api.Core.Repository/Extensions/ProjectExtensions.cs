﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class ProjectExtensions
{
    public static IQueryable<Project> SortProjects(this IQueryable<Project> projects, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return projects.OrderBy(e => e.DisplayName);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<Project>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return projects.OrderBy(e => e.DisplayName);
        }

        return projects.OrderBy(orderQuery);
    }
}