﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class ApiTokenExtensions
{
    public static IQueryable<ApiToken> SortApiTokens(this IQueryable<ApiToken> apiTokens, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return apiTokens.OrderBy(e => e.IssuedAt);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<ApiToken>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return apiTokens.OrderBy(e => e.IssuedAt);
        }

        return apiTokens.OrderBy(orderQuery);
    }
}