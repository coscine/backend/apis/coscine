using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class ResourceExtensions
{
    public static IQueryable<Resource> SortResources(this IQueryable<Resource> resources, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return resources.OrderBy(e => e.DisplayName);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<Resource>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return resources.OrderBy(e => e.DisplayName);
        }

        return resources.OrderBy(orderQuery);
    }
}