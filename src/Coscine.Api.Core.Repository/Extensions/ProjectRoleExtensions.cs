using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class ProjectRoleExtensions
{
    public static IQueryable<ProjectRole> SortProjectRoles(this IQueryable<ProjectRole> projectRoles, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return projectRoles.OrderBy(e => e.RelationId);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<ProjectRole>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return projectRoles.OrderBy(e => e.RelationId);
        }

        return projectRoles.OrderBy(orderQuery);
    }
}