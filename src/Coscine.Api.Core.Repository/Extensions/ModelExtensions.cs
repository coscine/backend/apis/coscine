﻿using Semiodesk.Trinity;

namespace Coscine.Api.Core.Repository.Extensions;

public static class ModelExtensions
{
    public static T GetOrCreateResource<T>(this Model model, Uri uri) where T : Resource
    {
        if (model.ContainsResource(uri))
        {
            return model.GetResource<T>(uri);
        }
        return model.CreateResource<T>(uri);
    }
}
