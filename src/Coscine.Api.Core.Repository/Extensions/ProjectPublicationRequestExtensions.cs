using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Extensions.Utility;
using System.Linq.Dynamic.Core;

namespace Coscine.Api.Core.Repository.Extensions;

public static class ProjectPublicationRequestExtensions
{
    public static IQueryable<ProjectPublicationRequest> SortPublicationRequests(this IQueryable<ProjectPublicationRequest> projectPublicationRequests, string? orderByQueryString)
    {
        if (string.IsNullOrWhiteSpace(orderByQueryString))
        {
            return projectPublicationRequests.OrderBy(e => e.DateCreated);
        }

        var orderQuery = OrderQueryBuilder.CreateOrderQuery<Project>(orderByQueryString);

        if (string.IsNullOrWhiteSpace(orderQuery))
        {
            return projectPublicationRequests.OrderBy(e => e.DateCreated);
        }

        return projectPublicationRequests.OrderBy(orderQuery);
    }
}