﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public class ProjectPublicationRequestRepository(RepositoryContext repositoryContext) : IProjectPublicationRequestRepository
{
    private readonly RepositoryBase<ProjectPublicationRequest> _projectPublicationRequest = new(repositoryContext);

    public void Create(ProjectPublicationRequest projectPublicationRequest)
        => _projectPublicationRequest.Create(projectPublicationRequest);

    public void Delete(ProjectPublicationRequest projectPublicationRequest)
        => _projectPublicationRequest.Delete(projectPublicationRequest);

    public async Task<IEnumerable<ProjectPublicationRequest>> GetAllAsync(Guid projectId, bool trackChanges)
    {
        var publicationRequestQuery = _projectPublicationRequest.FindByCondition(ppr => ppr.ProjectId == projectId, trackChanges)
            .Include(ppr => ppr.Creator)
            .Include(prr => prr.Resources)
            .Include(ppr => ppr.Project)
            .AsSplitQuery();

        return await publicationRequestQuery.ToListAsync();
    }

    public async Task<PagedEnumerable<ProjectPublicationRequest>> GetPagedAsync(Guid projectId, ProjectPublicationRequestAdminParameters projectPublicationRequestAdminParameters, bool trackChanges)
    {
        var publicationRequestQuery = _projectPublicationRequest.FindByCondition(ppr => ppr.ProjectId == projectId, trackChanges)
            .Include(ppr => ppr.Creator)
            .Include(prr => prr.Resources)
            .Include(ppr => ppr.Project)
            .AsSplitQuery();

        var publicationRequests = await publicationRequestQuery
            .SortPublicationRequests(projectPublicationRequestAdminParameters.OrderBy)
            .Skip((projectPublicationRequestAdminParameters.PageNumber - 1) * projectPublicationRequestAdminParameters.PageSize)
            .Take(projectPublicationRequestAdminParameters.PageSize)
            .ToListAsync();

        var count = await publicationRequestQuery.CountAsync();

        return new PagedEnumerable<ProjectPublicationRequest>(publicationRequests, count, projectPublicationRequestAdminParameters.PageNumber, projectPublicationRequestAdminParameters.PageSize);
    }

    public async Task<ProjectPublicationRequest?> GetAsync(Guid projectId, Guid publicationRequestId, bool trackChanges)
    {
        return await _projectPublicationRequest
            .FindByCondition(ppr => ppr.Id == publicationRequestId && ppr.ProjectId == projectId, trackChanges)
            .Include(ppr => ppr.Creator)
            .Include(ppr => ppr.Resources).ThenInclude(r => r.ResourceDisciplines).ThenInclude(rd => rd.Discipline)
            .Include(ppr => ppr.Project).ThenInclude(p => p.ProjectDisciplines).ThenInclude(pd => pd.Discipline)
            .Include(ppr => ppr.Project).ThenInclude(p => p.ProjectRoles).ThenInclude(pr => pr.User)
            .AsSplitQuery()
            .SingleOrDefaultAsync();
    }
}
