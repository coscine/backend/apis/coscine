﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class ExternalIdRepository(RepositoryContext repositoryContext) : IExternalIdRepository
{
    private readonly RepositoryBase<ExternalId> _externalId = new(repositoryContext);

    public void Create(ExternalId externalId)
    {
        _externalId.Create(externalId);
    }

    public void Delete(ExternalId externalId)
    {
        _externalId.Delete(externalId);
    }

    public async Task<IEnumerable<ExternalId>> GetAllAsync(Guid userId, bool trackChanges)
    {
        var externalIdQuery = _externalId.FindAll(trackChanges)
            .Where(eId => eId.UserId.Equals(userId))
            .Include(eId => eId.ExternalAuthenticator);

        var externalIds = await externalIdQuery
            .ToListAsync();

        return externalIds;
    }

    public async Task<ExternalId?> GetAsync(Guid externalAuthenticatorId, string extenalId, bool trackChanges)
    {
        var externalIdQuery = _externalId.FindByCondition(
                                eId => eId.ExternalAuthenticatorId.Equals(externalAuthenticatorId) &&
                                eId.ExternalId1 == extenalId, trackChanges
                            );

        return await externalIdQuery.SingleOrDefaultAsync();
    }

    public async Task<ExternalId?> GetAsync(Guid userId, bool trackChanges)
    {
        var externalIdQuery = _externalId.FindByCondition(eId => eId.UserId.Equals(userId), trackChanges);

        var externalId = await externalIdQuery
            .SingleOrDefaultAsync();

        return externalId;
    }



    public async Task MergeAsync(Guid fromUserId, Guid toUserId)
    {
        foreach (var pr in await _externalId
            .FindByCondition(x => x.UserId == fromUserId, trackChanges: true)
            .ToListAsync())
        {
            // Make sure, that the users are not duplicated
            if (await _externalId.FindByCondition(x => x.UserId == toUserId, trackChanges: false)
                    .AnyAsync())
            {
                // Not already part of the user
                pr.UserId = toUserId;
            }
            else
            {
                // ExternalId is already part of the user, delete old reference
                Delete(pr);
            }
        }
    }
}