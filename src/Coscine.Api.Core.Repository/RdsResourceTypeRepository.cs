using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class RdsResourceTypeRepository : IRdsResourceTypeRepository
{
    private readonly RepositoryBase<RdsResourceType> _rdsResourceType;

    public RdsResourceTypeRepository(RepositoryContext repositoryContext)
    {
        _rdsResourceType = new(repositoryContext);
    }

    public void Create(RdsResourceType rdsResourceType)
    {
        _rdsResourceType.Create(rdsResourceType);
    }

    public async Task<RdsResourceType?> GetAsync(Guid rDSResourceTypeId, bool trackChanges)
    {
        return await _rdsResourceType.FindByCondition(x => x.Id == rDSResourceTypeId, trackChanges)
            .SingleOrDefaultAsync();
    }
}