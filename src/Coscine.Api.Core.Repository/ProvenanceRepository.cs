﻿using Coscine.Api.Core.Entities.Models.QuadStore;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Repository.Extensions.Utility;
using Coscine.Api.Core.Repository.HelperModels;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Enums.Utility;
using Semiodesk.Trinity;
using VDS.RDF;

namespace Coscine.Api.Core.Repository;

public class ProvenanceRepository : IProvenanceRepository
{
    private readonly IRdfRepositoryBase _rdfRepositoryBase;
    private readonly MetadataUtility _metadataUtility;
    private readonly IStore _store;

    /// <summary>
    /// Initializes a new instance of the <see cref="ProvenanceRepository"/> class with the specified RDF repository.
    /// </summary>
    /// <param name="rdfRepository">The RDF repository that this repository will interact with.</param>
    /// <param name="store">The Trinity Store that this repository will interact with.</param>
    public ProvenanceRepository(IRdfRepositoryBase rdfRepository, IStore store)
    {
        _rdfRepositoryBase = rdfRepository;
        _metadataUtility = new(_rdfRepositoryBase);

        _store = store;
    }

    /// <inheritdoc/>
    public async Task<IEnumerable<IGraph>> AddProvenanceInformation(
        Uri metadataVersionId,
        bool createNewDataGraph,
        bool createNewMetadataGraph,
        Uri? existingMetadataGraphUri = null,
        IGraph? extractedDataGraph = null,
        IGraph? extractedMetadataGraph = null,
        Provenance? provenance = null)
    {
        var generalGraphNameUri = TreeUriResolver.CreateGraphUri(metadataVersionId);
        var generalGraphName = generalGraphNameUri.AbsoluteUri;

        var dataProvenanceModel = GetProvenanceModel(metadataVersionId, GraphType.Data);
        var metadataProvenanceModel = GetProvenanceModel(metadataVersionId, GraphType.Metadata);

        var dataProvenanceEntity = metadataProvenanceModel.GetOrCreateResource<RawDataCatalog>(dataProvenanceModel.Uri);
        var metadataProvenanceEntity = metadataProvenanceModel.GetOrCreateResource<MetadataCatalog>(metadataProvenanceModel.Uri);

        var existingIds = await _metadataUtility.ListGraphsFromTrellisGraphAsync(dataProvenanceModel.Uri.AbsoluteUri, RdfUris.LdpNonRDFSourceClass);
        var oldDataVersionId = MetadataVersionUtility.GetRecentDataVersion(existingIds);
        var dataVersionId = oldDataVersionId;

        if (createNewDataGraph)
        {
            dataVersionId = new AlwaysAbsoluteUri(metadataVersionId.AbsoluteUri.Replace("@type=metadata", "@type=data"));
            await CreateDataVersionGraph(dataVersionId, generalGraphName, dataProvenanceModel, dataProvenanceEntity, oldDataVersionId, metadataVersionId);
        }

        if (createNewMetadataGraph)
        {
            await CreateMetadataVersionGraph(metadataVersionId, existingMetadataGraphUri, generalGraphName, metadataProvenanceModel, metadataProvenanceEntity, dataVersionId);
        }

        AddProvenanceTriples(metadataVersionId, provenance, metadataProvenanceModel, metadataProvenanceEntity);

        if (extractedDataGraph is not null)
        {
            // Set information about the data extraction version in the data provenance graph
            await CreateExtractionDataGraph(extractedDataGraph, provenance, generalGraphName, dataProvenanceModel, dataProvenanceEntity);
        }

        if (extractedMetadataGraph is not null)
        {
            // Set information about the metadata extraction version in the metadata provenance graph
            await CreateExtractionMetadataGraph(extractedMetadataGraph, provenance, generalGraphName, metadataProvenanceModel, metadataProvenanceEntity);
        }

        dataProvenanceModel.UpdateResource(dataProvenanceEntity);
        metadataProvenanceModel.UpdateResource(metadataProvenanceEntity);

        return [
            await _rdfRepositoryBase.GetGraph(dataProvenanceModel.Uri),
            await _rdfRepositoryBase.GetGraph(metadataProvenanceModel.Uri),
        ];
    }

    /// <summary>
    /// Creates and updates the data provenance model with information about an extracted data graph.
    /// </summary>
    /// <param name="extractedDataGraph">The extracted data graph to process.</param>
    /// <param name="provenance">Optional provenance information containing details such as the extractor version and hash parameters.</param>
    /// <param name="generalGraphName">The general graph name URI as a string.</param>
    /// <param name="dataProvenanceModel">The data provenance model to be updated.</param>
    /// <param name="dataProvenanceEntity">The data provenance entity representing the raw data catalog.</param>
    /// <returns>A task representing the asynchronous operation.</returns>
    private async Task CreateExtractionDataGraph(IGraph extractedDataGraph, Provenance? provenance, string generalGraphName, Model dataProvenanceModel, RawDataCatalog dataProvenanceEntity)
    {
        var dataVersionId = new AlwaysAbsoluteUri(extractedDataGraph.BaseUri.AbsoluteUri.Replace("&extracted=true", ""));

        await _metadataUtility.AddToTrellisAsync(RdfUris.LdpRDFSourceClass, generalGraphName, extractedDataGraph.BaseUri.AbsoluteUri);

        dataProvenanceEntity.ExtractedIds.Add(extractedDataGraph.BaseUri);

        var rawDataEntity = dataProvenanceModel.GetOrCreateResource<RawDataEntity>(dataVersionId);
        var rawDataExtractedEntity = dataProvenanceModel.GetOrCreateResource<RawDataExtractedEntity>(extractedDataGraph.BaseUri);

        rawDataExtractedEntity.ExtractedVersion = provenance?.MetadataExtractorVersion ?? "NaN";
        rawDataExtractedEntity.GeneratedAt = DateTime.UtcNow;

        rawDataEntity.ExtractedId = extractedDataGraph.BaseUri;

        var hashDataVersionId = new AlwaysAbsoluteUri($"{dataVersionId.AbsoluteUri}&hash=true");
        var hashEntity = dataProvenanceModel.GetOrCreateResource<HashEntity>(hashDataVersionId);

        hashEntity.HashFunction = provenance?.HashParameters?.AlgorithmName.Name ?? "NaN";
        hashEntity.HashValue = provenance?.HashParameters?.Value ?? "NaN";

        rawDataEntity.Hash = hashEntity.Uri;

        dataProvenanceModel.UpdateResource(hashEntity);
        dataProvenanceModel.UpdateResource(rawDataEntity);
        dataProvenanceModel.UpdateResource(rawDataExtractedEntity);
    }

    /// <summary>
    /// Creates and updates the metadata provenance model with information about an extracted metadata graph.
    /// </summary>
    /// <param name="extractedMetadataGraph">The extracted metadata graph to process.</param>
    /// <param name="provenance">Optional provenance information containing details such as the extractor version.</param>
    /// <param name="generalGraphName">The general graph name URI as a string.</param>
    /// <param name="metadataProvenanceModel">The metadata provenance model to be updated.</param>
    /// <param name="metadataProvenanceEntity">The metadata provenance entity representing the metadata catalog.</param>
    /// <returns>A task representing the asynchronous operation.</returns>
    private async Task CreateExtractionMetadataGraph(IGraph extractedMetadataGraph, Provenance? provenance, string generalGraphName, Model metadataProvenanceModel, MetadataCatalog metadataProvenanceEntity)
    {
        var metadataVersionId = new Uri(extractedMetadataGraph.BaseUri.AbsoluteUri.Replace("&extracted=true", ""));

        await _metadataUtility.AddToTrellisAsync(RdfUris.LdpRDFSourceClass, generalGraphName, extractedMetadataGraph.BaseUri.AbsoluteUri);

        var metadataEntity = metadataProvenanceModel.GetOrCreateResource<MetadataEntity>(metadataVersionId);
        var metadataExtractedEntity = metadataProvenanceModel.GetOrCreateResource<MetadataExtractedEntity>(extractedMetadataGraph.BaseUri);

        metadataProvenanceEntity.EntityIds.Add(extractedMetadataGraph.BaseUri);

        metadataExtractedEntity.ExtractedVersion = provenance?.MetadataExtractorVersion ?? "NaN";
        metadataExtractedEntity.GeneratedAt = DateTime.UtcNow;

        metadataEntity.ExtractedId = extractedMetadataGraph.BaseUri;

        metadataProvenanceModel.UpdateResource(metadataEntity);
        metadataProvenanceModel.UpdateResource(metadataExtractedEntity);
    }

    /// <summary>
    /// Adds provenance triples to the metadata provenance model based on the provided provenance information.
    /// </summary>
    /// <param name="metadataVersionId">The URI of the metadata version graph.</param>
    /// <param name="provenance">Optional provenance information containing details such as revisions, variants, invalidations, and similarity metrics.</param>
    /// <param name="metadataProvenanceModel">The metadata provenance model to update with provenance information.</param>
    /// <param name="metadataProvenanceEntity">The metadata provenance entity representing the metadata catalog.</param>
    private void AddProvenanceTriples(Uri metadataVersionId, Provenance? provenance, Model metadataProvenanceModel, MetadataCatalog metadataProvenanceEntity)
    {
        var metadataEntity = metadataProvenanceModel.GetOrCreateResource<MetadataEntity>(metadataVersionId);

        if (provenance?.WasRevisionOf.Any() == true)
        {
            foreach (var version in provenance.WasRevisionOf)
            {
                metadataEntity.WasRevisionOf.Add(version);
            }
        }
        if (provenance?.Variants.Any() == true)
        {
            foreach (var variant in provenance.Variants)
            {
                var variantUri = new AlwaysAbsoluteUri($"{metadataVersionId.AbsoluteUri}&variant=true");

                var variantResource = metadataProvenanceModel.GetOrCreateResource<Variant>(variantUri);

                variantResource.VariantOf = variant.GraphName;
                variantResource.Similarity = variant.Similarity + "";

                metadataEntity.HasVariant.Add(variantResource.Uri);
                metadataProvenanceModel.UpdateResource(variantResource);
            }
        }
        if (provenance?.WasInvalidatedBy is not null)
        {
            metadataEntity.InvalidatedBy = provenance.WasInvalidatedBy;
        }
        if (provenance?.SimilarityToLastVersion is not null)
        {
            metadataEntity.SimilarityToLastVersion = provenance.SimilarityToLastVersion + "";
        }

        metadataProvenanceModel.UpdateResource(metadataEntity);
    }

    /// <summary>
    /// Creates a new metadata version graph and updates the metadata provenance model with versioning information.
    /// </summary>
    /// <param name="metadataVersionId">The URI of the new metadata version graph.</param>
    /// <param name="existingMetadataGraphUri">The URI of an existing metadata graph, if any.</param>
    /// <param name="generalGraphName">The general graph name URI as a string.</param>
    /// <param name="metadataProvenanceModel">The metadata provenance model to update.</param>
    /// <param name="metadataProvenanceEntity">The metadata provenance entity representing the metadata catalog.</param>
    /// <param name="dataVersionId">The URI of the associated data version graph, if any.</param>
    /// <returns>A task representing the asynchronous operation.</returns>
    private async Task CreateMetadataVersionGraph(Uri metadataVersionId, Uri? existingMetadataGraphUri, string generalGraphName, Model metadataProvenanceModel, MetadataCatalog metadataProvenanceEntity, Uri? dataVersionId)
    {
        // Add the newly created metadata version to the metadata provenance graph
        metadataProvenanceEntity.EntityIds.Add(metadataVersionId);

        var graphUriData = new TreeUriResolver(metadataVersionId);

        var metadataEntity = metadataProvenanceModel.GetOrCreateResource<MetadataEntity>(metadataVersionId);

        metadataEntity.GeneratedAt = DateTime.UtcNow;
        metadataEntity.Version = graphUriData.Version;

        if (dataVersionId is not null)
        {
            metadataEntity.RawDataId = dataVersionId;
        }

        await _metadataUtility.AddToTrellisAsync(RdfUris.LdpRDFSourceClass, generalGraphName, metadataVersionId.AbsoluteUri);

        if (existingMetadataGraphUri is not null && existingMetadataGraphUri.AbsoluteUri != metadataVersionId.AbsoluteUri)
        {
            metadataEntity.WasRevisionOf.Add(existingMetadataGraphUri);
        }

        metadataProvenanceModel.UpdateResource(metadataEntity);

        if (graphUriData.IsFolder)
        {
            var folderEntity = metadataProvenanceModel.GetOrCreateResource<FolderEntity>(metadataVersionId);
            folderEntity.Path = graphUriData.Path;
            metadataProvenanceModel.UpdateResource(folderEntity);
        }
        else
        {
            var fileEntity = metadataProvenanceModel.GetOrCreateResource<FileEntity>(metadataVersionId);
            fileEntity.Path = graphUriData.Path;
            metadataProvenanceModel.UpdateResource(fileEntity);
        }
    }

    /// <summary>
    /// Creates a new data version graph and updates the data provenance model with versioning information.
    /// </summary>
    /// <param name="dataVersionId">The URI of the new data version graph.</param>
    /// <param name="generalGraphName">The general graph name URI as a string.</param>
    /// <param name="dataProvenanceModel">The data provenance model to update.</param>
    /// <param name="dataProvenanceEntity">The data provenance entity representing the raw data catalog.</param>
    /// <param name="oldDataVersionId">The URI of the previous data version graph, if any.</param>
    /// <param name="metadataVersionId">The URI of the associated metadata version graph.</param>
    /// <returns>A task representing the asynchronous operation.</returns>
    private async Task CreateDataVersionGraph(Uri dataVersionId, string generalGraphName, Model dataProvenanceModel, RawDataCatalog dataProvenanceEntity, Uri? oldDataVersionId, Uri metadataVersionId)
    {
        var currentDataVersionGraph = new Graph(dataVersionId)
        {
            BaseUri = dataVersionId
        };
        if (oldDataVersionId is not null)
        {
            var oldDataGraph = await _rdfRepositoryBase.GetGraph(oldDataVersionId);
            _metadataUtility.SetLinkedData(oldDataGraph, currentDataVersionGraph, dataVersionId);
        }

        currentDataVersionGraph.AssertToGraph(dataVersionId, RdfUris.DcTermsIdentifier, dataVersionId.AbsoluteUri);
        await _rdfRepositoryBase.AddGraphAsync(currentDataVersionGraph);

        var graphUriData = new TreeUriResolver(dataVersionId);

        var rawDataEntity = dataProvenanceModel.GetOrCreateResource<RawDataEntity>(dataVersionId);
        rawDataEntity.GeneratedAt = DateTime.UtcNow;
        rawDataEntity.MetadataOf = metadataVersionId;
        rawDataEntity.Version = graphUriData.Version;

        dataProvenanceEntity.EntityIds.Add(dataVersionId);

        await _metadataUtility.AddToTrellisAsync(RdfUris.LdpNonRDFSourceClass, generalGraphName, dataVersionId.AbsoluteUri);

        if (oldDataVersionId is not null && oldDataVersionId.AbsoluteUri != dataVersionId.AbsoluteUri)
        {
            rawDataEntity.WasRevisionOf.Add(oldDataVersionId);
        }

        dataProvenanceModel.UpdateResource(rawDataEntity);

        if (graphUriData.IsFolder)
        {
            var folderEntity = dataProvenanceModel.GetOrCreateResource<FolderEntity>(dataVersionId);
            folderEntity.Path = graphUriData.Path;
            dataProvenanceModel.UpdateResource(folderEntity);
        }
        else
        {
            var fileEntity = dataProvenanceModel.GetOrCreateResource<FileEntity>(dataVersionId);
            fileEntity.Path = graphUriData.Path;
            dataProvenanceModel.UpdateResource(fileEntity);
        }
    }

    /// <inheritdoc/>
    public async Task<IGraph> CreateInvalidation(Guid resourceId, string path, Uri graphId, string type = "metadata", Uri? responsibleAgent = null)
    {
        // No user was provided, set the default value
        responsibleAgent ??= RdfUris.CoscineTermsUserAgent;

        var provenanceGraph = PatchGraph.Empty(new Uri($"{RdfUris.CoscineResources}{resourceId}/{path}/@type={type}"));
        provenanceGraph.AssertToGraph(graphId, RdfUris.ProvWasInvalidatedBy, responsibleAgent);

        await _rdfRepositoryBase.AddGraphAsync(provenanceGraph);

        return provenanceGraph;
    }

    /// <summary>
    /// Generates a provenance graph for a given versioned graph URI.
    /// </summary>
    /// <param name="versionedGraphUri">The URI of the versioned graph.</param>
    /// <param name="graphType">The type of graph, e.g., '<c>data</c>' or '<c>metadata</c>' (default is '<c>metadata</c>').</param>
    /// <returns>A <see cref="Model"/> instance representing the provenance graph.</returns>
    private Model GetProvenanceModel(Uri versionedGraphUri, GraphType graphType = GraphType.Metadata)
    {
        // Always follows the pattern: 'https://purl.org/coscine/resources/{resourceId}/{path}/@type={graphType}'
        var typeParameter = $"@type={graphType.GetEnumMemberValue()}";

        // Extract the resource ID and path from the URI and append the '@type' parameter.
        var provenanceGraphUriString = versionedGraphUri.AbsoluteUri[..versionedGraphUri.AbsoluteUri.LastIndexOf('/')] + '/' + typeParameter;

        // Validate the constructed URI is well-formed for safety.
        if (!Uri.IsWellFormedUriString(provenanceGraphUriString, UriKind.Absolute))
        {
            throw new ArgumentException($"The constructed provenance URI is not well-formed. The provenance graph URI was resolved to \"{provenanceGraphUriString}\".", nameof(versionedGraphUri));
        }

        return new Model(_store, new UriRef(provenanceGraphUriString));
    }
}
