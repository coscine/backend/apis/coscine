using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Helpers;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.Extensions.Options;

namespace Coscine.Api.Core.Repository;

public sealed class MessageRepository(IOptionsMonitor<MessageConfiguration> messageConfiguration) : IMessageRepository
{
    private readonly MessageConfiguration _messageConfiguration = messageConfiguration.CurrentValue;

    /// <remarks>
    /// Even though we currently support only one internal message, we still use pagination to keep the API consistent and future-proof.
    /// </remarks>
    public async Task<PagedEnumerable<Message>> GetPagedFromConsulAsync(MessageParameters messageParameters)
    {
        // Build the message entity from the configuration
        var messagesFromConsul = (_messageConfiguration.De is null && _messageConfiguration.En is null)
            ? new List<Message>() : [ExtractMessageFromConfiguration(_messageConfiguration)];

        // Filter the messages
        var filteredMessages = messagesFromConsul
            .TakeAfterStartDate(messageParameters.StartDateAfter)
            .TakeBeforeStartDate(messageParameters.StartDateBefore)
            .TakeAfterEndDate(messageParameters.EndDateAfter)
            .TakeBeforeEndDate(messageParameters.EndDateBefore)
            .FilterByMessageType(messageParameters.Type)
            .ToList();

        // Paginate the results
        var offset = (messageParameters.PageNumber - 1) * messageParameters.PageSize;
        var pagedMessages = filteredMessages.Skip(offset).Take(messageParameters.PageSize);

        return await Task.FromResult(
            new PagedEnumerable<Message>(pagedMessages, filteredMessages.Count, messageParameters.PageNumber, messageParameters.PageSize)
        );
    }

    private static Message ExtractMessageFromConfiguration(MessageConfiguration messageConfiguration)
    {
        // Build the message entity from the configuration
        var message = new Message
        {
            Type = messageConfiguration.Type is not null ? MessageTypeExtensions.FromString(messageConfiguration.Type) : MessageType.Information,
            Body = new Dictionary<string, string>
            {
                { "de", messageConfiguration.De ?? string.Empty },
                { "en", messageConfiguration.En ?? string.Empty }
            },
            StartDate = messageConfiguration.StartDate,
            EndDate = messageConfiguration.EndDate
        };

        // Remove all empty values from the body.
        var keysToRemove = message.Body
            .Where(kvp => string.IsNullOrWhiteSpace(kvp.Value))
            .Select(kvp => kvp.Key);

        foreach (var key in keysToRemove)
        {
            message.Body.Remove(key);
        }
        
        return message;
    }
}