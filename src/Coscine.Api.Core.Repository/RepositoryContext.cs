﻿// Ignore Spelling: Rdsresource Tosaccepteds

using Coscine.Api.Core.Entities.Models;
using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public partial class RepositoryContext : DbContext, IDataProtectionKeyContext
{
    public RepositoryContext()
    {
    }

    public RepositoryContext(DbContextOptions<RepositoryContext> options)
        : base(options)
    {
    }

    public virtual DbSet<ActivatedFeature> ActivatedFeatures { get; set; }

    public virtual DbSet<ApiToken> ApiTokens { get; set; }

    public virtual DbSet<ActivityLog> ActivityLogs { get; set; }

    public virtual DbSet<ContactChange> ContactChanges { get; set; }

    public virtual DbSet<Discipline> Disciplines { get; set; }

    public virtual DbSet<ExternalAuthenticator> ExternalAuthenticators { get; set; }

    public virtual DbSet<ExternalId> ExternalIds { get; set; }

    public virtual DbSet<Feature> Features { get; set; }

    public virtual DbSet<GitlabResourceType> GitlabResourceTypes { get; set; }

    public virtual DbSet<Group> Groups { get; set; }

    public virtual DbSet<GroupMembership> GroupMemberships { get; set; }

    public virtual DbSet<Invitation> Invitations { get; set; }

    public virtual DbSet<Language> Languages { get; set; }

    public virtual DbSet<License> Licenses { get; set; }

    public virtual DbSet<LinkedResourceType> LinkedResourceTypes { get; set; }

    public virtual DbSet<Log> Logs { get; set; }

    public virtual DbSet<MetadataExtraction> MetadataExtractions { get; set; }

    public virtual DbSet<Project> Projects { get; set; }

    public virtual DbSet<ProjectDiscipline> ProjectDisciplines { get; set; }

    public virtual DbSet<ProjectInstitute> ProjectInstitutes { get; set; }

    public virtual DbSet<ProjectPublicationRequest> ProjectPublicationRequests { get; set; }

    public virtual DbSet<ProjectQuota> ProjectQuotas { get; set; }

    public virtual DbSet<ProjectResource> ProjectResources { get; set; }

    public virtual DbSet<ProjectRole> ProjectRoles { get; set; }

    public virtual DbSet<RdsS3ResourceType> RdsS3resourceTypes { get; set; }

    public virtual DbSet<RdsS3WormResourceType> RdsS3wormResourceTypes { get; set; }

    public virtual DbSet<RdsResourceType> RdsresourceTypes { get; set; }

    public virtual DbSet<Resource> Resources { get; set; }

    public virtual DbSet<ResourceDiscipline> ResourceDisciplines { get; set; }

    public virtual DbSet<ResourceType> ResourceTypes { get; set; }

    public virtual DbSet<Role> Roles { get; set; }

    public virtual DbSet<SubProject> SubProjects { get; set; }

    public virtual DbSet<Title> Titles { get; set; }

    public virtual DbSet<Tosaccepted> Tosaccepteds { get; set; }

    public virtual DbSet<User> Users { get; set; }

    public virtual DbSet<UserDiscipline> UserDisciplines { get; set; }

    public virtual DbSet<VersionInfo> VersionInfos { get; set; }

    public virtual DbSet<Visibility> Visibilities { get; set; }

    public DbSet<DataProtectionKey> DataProtectionKeys { get; set; } = null!;

    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ActivatedFeature>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

            entity.HasOne(d => d.Feature).WithMany(p => p.ActivatedFeatures)
                .HasForeignKey(d => d.FeatureId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ActivatedFeatures_FeatureId_Features_Id");

            entity.HasOne(d => d.Project).WithMany(p => p.ActivatedFeatures)
                .HasForeignKey(d => d.ProjectId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ActivatedFeatures_ProjectId_Projects_Id");
        });

        modelBuilder.Entity<ActivityLog>(entity =>
        {
            entity.HasKey(e => e.Id);
            entity.Property(e => e.ActivityTimestamp);
            entity.Property(e => e.ApiPath).HasMaxLength(255);
            entity.Property(e => e.HttpAction).HasMaxLength(10).HasDefaultValue("NA");
            entity.Property(e => e.ControllerName).HasMaxLength(255).HasDefaultValue("NA");
            entity.Property(e => e.ActionName).HasMaxLength(255).HasDefaultValue("NA");

            entity.HasOne(d => d.User).WithMany(u => u.ActivityLogs)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull);

            // Note: Consider indexing the API path for faster lookups of activity logs, if it turns out to be slow;
            // Potentially investigate performance impact for write operations due to the frequency of those calls.
        });

        modelBuilder.Entity<ApiToken>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.Expiration).HasColumnType("datetime");
            entity.Property(e => e.IssuedAt).HasColumnType("datetime");
            entity.Property(e => e.Name).HasMaxLength(255);

            entity.HasOne(d => d.User).WithMany(p => p.ApiTokens)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ApiTokens_UserId_Users_Id");
        });

        modelBuilder.Entity<ContactChange>(entity =>
        {
            entity.HasKey(e => e.RelationId);

            entity.ToTable("ContactChange");

            entity.Property(e => e.RelationId).HasDefaultValueSql("(newid())");
            entity.Property(e => e.ConfirmationToken).HasDefaultValueSql("(newid())");
            entity.Property(e => e.EditDate).HasColumnType("datetime");
            entity.Property(e => e.NewEmail).HasMaxLength(200);

            entity.HasOne(d => d.User).WithMany(p => p.ContactChanges)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ContactChange_UserId_Users_Id");
        });

        modelBuilder.Entity<Discipline>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.DisplayNameDe).HasMaxLength(200);
            entity.Property(e => e.DisplayNameEn).HasMaxLength(200);
            entity.Property(e => e.Url).HasMaxLength(200);
        });

        modelBuilder.Entity<ExternalAuthenticator>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.DisplayName).HasMaxLength(50);
        });

        modelBuilder.Entity<ExternalId>(entity =>
        {
            entity.HasKey(e => e.RelationId);

            entity.Property(e => e.RelationId).HasDefaultValueSql("(newid())");
            entity.Property(e => e.ExternalId1)
                .HasMaxLength(255)
                .HasColumnName("ExternalId");
            entity.Property(e => e.Organization).HasMaxLength(255);

            entity.HasOne(d => d.ExternalAuthenticator).WithMany(p => p.ExternalIds)
                .HasForeignKey(d => d.ExternalAuthenticatorId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ExternalIds_ResourceTypeId_ExternalAuthenticators_Id");

            entity.HasOne(d => d.User).WithMany(p => p.ExternalIds)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ExternalIds_UserId_Users_Id");
        });

        modelBuilder.Entity<Feature>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.DisplaynameDe).HasMaxLength(200);
            entity.Property(e => e.DisplaynameEn).HasMaxLength(200);
            entity.Property(e => e.SharepointId).HasMaxLength(200);
        });

        modelBuilder.Entity<GitlabResourceType>(entity =>
        {
            entity.ToTable("GitlabResourceType");

            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.Branch).HasMaxLength(200);
            entity.Property(e => e.ProjectAccessToken).HasMaxLength(200);
            entity.Property(e => e.RepoUrl).HasMaxLength(200);
        });

        modelBuilder.Entity<Group>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.DisplayName).HasMaxLength(255);
        });

        modelBuilder.Entity<GroupMembership>(entity =>
        {
            entity.HasKey(e => e.RelationId);

            entity.Property(e => e.RelationId).HasDefaultValueSql("(newid())");

            entity.HasOne(d => d.Group).WithMany(p => p.GroupMemberships)
                .HasForeignKey(d => d.GroupId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_GroupMemberships_GroupId_Groups_Id");

            entity.HasOne(d => d.User).WithMany(p => p.GroupMemberships)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_GroupMemberships_UserId_Users_Id");
        });

        modelBuilder.Entity<Invitation>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.Expiration).HasColumnType("datetime");
            entity.Property(e => e.InviteeEmail).HasMaxLength(200);
            entity.Property(e => e.Token).HasDefaultValueSql("(newid())");

            entity.HasOne(d => d.IssuerNavigation).WithMany(p => p.Invitations)
                .HasForeignKey(d => d.Issuer)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Invitations_Issuer_Users_Id");

            entity.HasOne(d => d.ProjectNavigation).WithMany(p => p.Invitations)
                .HasForeignKey(d => d.Project)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Invitations_Project_Projects_Id");

            entity.HasOne(d => d.RoleNavigation).WithMany(p => p.Invitations)
                .HasForeignKey(d => d.Role)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Invitations_Role_Roles_Id");
        });

        modelBuilder.Entity<Language>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.Abbreviation).HasMaxLength(50);
            entity.Property(e => e.DisplayName).HasMaxLength(50);
        });

        modelBuilder.Entity<License>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.DisplayName).HasMaxLength(200);
        });

        modelBuilder.Entity<LinkedResourceType>(entity =>
        {
            entity.ToTable("LinkedResourceType");

            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
        });

        modelBuilder.Entity<Log>(entity =>
        {
            entity.ToTable("Log");

            entity.HasIndex(e => new { e.UserId, e.LogLevel, e.ServerTimestamp }, "log_idx_userid_loglevel_servertime");

            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.ClientTimestamp)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.LogLevel).HasMaxLength(20);
            entity.Property(e => e.Server).HasMaxLength(200);
            entity.Property(e => e.ServerTimestamp)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Source).HasMaxLength(200);
            entity.Property(e => e.Status).HasMaxLength(200);
            entity.Property(e => e.Uri)
                .HasMaxLength(500)
                .HasColumnName("URI");
        });

        modelBuilder.Entity<MetadataExtraction>(entity =>
        {
            entity.ToTable("MetadataExtraction");

            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

            entity.HasOne(d => d.Resource).WithMany(p => p.MetadataExtractions)
                .HasForeignKey(d => d.ResourceId)
                .HasConstraintName("FK_MetadataExtraction_ResourceId_Resources_Id");
        });

        modelBuilder.Entity<Project>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.DateCreated)
                .HasDefaultValueSql("(getutcdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.DisplayName).HasMaxLength(25);
            entity.Property(e => e.EndDate).HasColumnType("datetime");
            entity.Property(e => e.GrantId).HasMaxLength(500);
            entity.Property(e => e.Keywords).HasMaxLength(1000);
            entity.Property(e => e.PrincipleInvestigators).HasMaxLength(500);
            entity.Property(e => e.ProjectName).HasMaxLength(200);
            entity.Property(e => e.Slug).HasMaxLength(63);
            entity.Property(e => e.StartDate)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");

            entity.HasOne(d => d.Visibility).WithMany(p => p.Projects)
                .HasForeignKey(d => d.VisibilityId)
                .HasConstraintName("FK_Projects_VisibilityId_Visibilities_Id");
        });

        modelBuilder.Entity<ProjectDiscipline>(entity =>
        {
            entity.HasKey(e => e.RelationId);

            entity.ToTable("ProjectDiscipline");

            entity.Property(e => e.RelationId).HasDefaultValueSql("(newid())");

            entity.HasOne(d => d.Discipline).WithMany(p => p.ProjectDisciplines)
                .HasForeignKey(d => d.DisciplineId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ProjectDiscipline_DisciplineId_Disciplines_Id");

            entity.HasOne(d => d.Project).WithMany(p => p.ProjectDisciplines)
                .HasForeignKey(d => d.ProjectId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ProjectDiscipline_ProjectId_Projects_Id");
        });

        modelBuilder.Entity<ProjectInstitute>(entity =>
        {
            entity.HasKey(e => e.RelationId);

            entity.ToTable("ProjectInstitute");

            entity.Property(e => e.RelationId).HasDefaultValueSql("(newid())");
            entity.Property(e => e.OrganizationUrl)
                .HasMaxLength(255)
                .HasDefaultValueSql("(N'https://www.rwth-aachen.de/22000')");

            entity.HasOne(d => d.Project).WithMany(p => p.ProjectInstitutes)
                .HasForeignKey(d => d.ProjectId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ProjectInstitute_ProjectId_Projects_Id");

            entity.Property(e => e.Responsible)
                .IsRequired()
                .HasDefaultValue(false);
        });

        modelBuilder.Entity<ProjectPublicationRequest>(entity =>
        {
            entity.HasKey(e => e.Id);
            entity.HasOne(e => e.Project)
                .WithMany(e => e.PublicationRequests)
                .HasForeignKey(e => e.ProjectId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            entity.Property(e => e.DateCreated)
                .HasDefaultValueSql("(getutcdate())")
                .HasColumnType("datetime");
            entity.HasOne(e => e.Creator)
                .WithMany(e => e.PublicationRequests)
                .HasForeignKey(e => e.CreatorId)
                .OnDelete(DeleteBehavior.ClientSetNull);
            entity.HasMany(ppr => ppr.Resources)
                .WithMany(r => r.PublicationRequests);
        });

        modelBuilder.Entity<ProjectQuota>(entity =>
        {
            entity.HasKey(e => e.RelationId);

            entity.HasIndex(e => new { e.ProjectId, e.ResourceTypeId }, "ProjectIdResourceTypeId").IsUnique();

            entity.Property(e => e.RelationId).HasDefaultValueSql("(newid())");
            entity.Property(e => e.MaxQuota).HasDefaultValueSql("(N'0')");

            entity.HasOne(d => d.Project).WithMany(p => p.ProjectQuota)
                .HasForeignKey(d => d.ProjectId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ProjectQuotas_ProjectId_Projects_Id");

            entity.HasOne(d => d.ResourceType).WithMany(p => p.ProjectQuota)
                .HasForeignKey(d => d.ResourceTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ProjectQuotas_ResourceTypeId_ResourceTypes_Id");
        });

        modelBuilder.Entity<ProjectResource>(entity =>
        {
            entity.HasKey(e => e.RelationId);

            entity.ToTable("ProjectResource");

            entity.Property(e => e.RelationId).HasDefaultValueSql("(newid())");

            entity.HasOne(d => d.Project).WithMany(p => p.ProjectResources)
                .HasForeignKey(d => d.ProjectId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ProjectResource_ProjectId_Projects_Id");

            entity.HasOne(d => d.Resource).WithMany(p => p.ProjectResources)
                .HasForeignKey(d => d.ResourceId)
                .HasConstraintName("FK_ProjectResource_ResourceId_Resources_Id");
        });

        modelBuilder.Entity<ProjectRole>(entity =>
        {
            entity.HasKey(e => e.RelationId);

            entity.Property(e => e.RelationId).HasDefaultValueSql("(newid())");

            entity.HasOne(d => d.Project).WithMany(p => p.ProjectRoles)
                .HasForeignKey(d => d.ProjectId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ProjectRoles_ProjectId_Projects_Id");

            entity.HasOne(d => d.Role).WithMany(p => p.ProjectRoles)
                .HasForeignKey(d => d.RoleId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ProjectRoles_RoleId_Roles_Id");

            entity.HasOne(d => d.User).WithMany(p => p.ProjectRoles)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ProjectRoles_UserId_Users_Id");
        });

        modelBuilder.Entity<RdsS3ResourceType>(entity =>
        {
            entity.ToTable("RdsS3ResourceType");

            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.AccessKey).HasMaxLength(200);
            entity.Property(e => e.AccessKeyRead).HasMaxLength(200);
            entity.Property(e => e.AccessKeyWrite).HasMaxLength(200);
            entity.Property(e => e.BucketName).HasMaxLength(63);
            entity.Property(e => e.Endpoint).HasMaxLength(200);
            entity.Property(e => e.SecretKey).HasMaxLength(200);
            entity.Property(e => e.SecretKeyRead).HasMaxLength(200);
            entity.Property(e => e.SecretKeyWrite).HasMaxLength(200);
        });

        modelBuilder.Entity<RdsS3WormResourceType>(entity =>
        {
            entity.ToTable("RdsS3WormResourceType");

            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.AccessKey).HasMaxLength(200);
            entity.Property(e => e.AccessKeyRead).HasMaxLength(200);
            entity.Property(e => e.AccessKeyWrite).HasMaxLength(200);
            entity.Property(e => e.BucketName).HasMaxLength(63);
            entity.Property(e => e.Endpoint).HasMaxLength(200);
            entity.Property(e => e.SecretKey).HasMaxLength(200);
            entity.Property(e => e.SecretKeyRead).HasMaxLength(200);
            entity.Property(e => e.SecretKeyWrite).HasMaxLength(200);
        });

        modelBuilder.Entity<RdsResourceType>(entity =>
        {
            entity.ToTable("RDSResourceType");

            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.AccessKey).HasMaxLength(200);
            entity.Property(e => e.BucketName).HasMaxLength(63);
            entity.Property(e => e.Endpoint).HasMaxLength(200);
            entity.Property(e => e.SecretKey).HasMaxLength(200);
        });

        modelBuilder.Entity<Resource>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.ApplicationProfile).HasMaxLength(500);
            entity.Property(e => e.Archived)
                .HasMaxLength(255)
                .HasDefaultValueSql("(N'0')");
            entity.Property(e => e.DateCreated)
                .HasDefaultValueSql("(getutcdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.DisplayName).HasMaxLength(25);
            entity.Property(e => e.Keywords).HasMaxLength(1000);
            entity.Property(e => e.ResourceName).HasMaxLength(200);
            entity.Property(e => e.UsageRights).HasMaxLength(200);
            entity.Property(e => e.MetadataLocalCopy).HasDefaultValue(false);

            entity.HasOne(d => d.License).WithMany(p => p.Resources)
                .HasForeignKey(d => d.LicenseId)
                .HasConstraintName("FK_Resources_LicenseId_Licenses_Id");

            entity.HasOne(d => d.Type).WithMany(p => p.Resources)
                .HasForeignKey(d => d.TypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Resources_TypeId_ResourceTypes_Id");

            entity.HasOne(d => d.Visibility).WithMany(p => p.Resources)
                .HasForeignKey(d => d.VisibilityId)
                .HasConstraintName("FK_Resources_VisibilityId_Visibilities_Id");
        });

        modelBuilder.Entity<ResourceDiscipline>(entity =>
        {
            entity.HasKey(e => e.RelationId);

            entity.ToTable("ResourceDiscipline");

            entity.Property(e => e.RelationId).HasDefaultValueSql("(newid())");

            entity.HasOne(d => d.Discipline).WithMany(p => p.ResourceDisciplines)
                .HasForeignKey(d => d.DisciplineId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_ResourceDiscipline_DisciplineId_Disciplines_Id");

            entity.HasOne(d => d.Resource).WithMany(p => p.ResourceDisciplines)
                .HasForeignKey(d => d.ResourceId)
                .HasConstraintName("FK_ResourceDiscipline_ResourceId_Resources_Id");
        });

        modelBuilder.Entity<ResourceType>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.DisplayName).HasMaxLength(50);
            entity.Property(e => e.SpecificType).HasMaxLength(200);
            entity.Property(e => e.Type).HasMaxLength(200);
        });

        modelBuilder.Entity<Role>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.Description).HasMaxLength(50);
            entity.Property(e => e.DisplayName).HasMaxLength(50);
        });

        modelBuilder.Entity<SubProject>(entity =>
        {
            entity.HasKey(e => e.RelationId);

            entity.Property(e => e.RelationId).HasDefaultValueSql("(newid())");

            entity.HasOne(d => d.Project).WithMany(p => p.SubProjectProjects)
                .HasForeignKey(d => d.ProjectId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SubProjects_ProjectId_Projects_Id");

            entity.HasOne(d => d.SubProjectNavigation).WithMany(p => p.SubProjectSubProjectNavigations)
                .HasForeignKey(d => d.SubProjectId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_SubProjects_SubProjectId_Projects_Id");
        });

        modelBuilder.Entity<Title>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.DisplayName).HasMaxLength(50);
        });

        modelBuilder.Entity<Tosaccepted>(entity =>
        {
            entity.HasKey(e => e.RelationId);

            entity.ToTable("TOSAccepted");

            entity.Property(e => e.RelationId).HasDefaultValueSql("(newid())");
            entity.Property(e => e.Version).HasMaxLength(10);

            entity.HasOne(d => d.User).WithMany(p => p.Tosaccepteds)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_TOSAccepted_UserId_Users_Id");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.DisplayName).HasMaxLength(255);
            entity.Property(e => e.EmailAddress).HasMaxLength(200);
            entity.Property(e => e.Entitlement).HasMaxLength(200);
            entity.Property(e => e.Givenname).HasMaxLength(200);
            entity.Property(e => e.Organization).HasMaxLength(200);
            entity.Property(e => e.Surname).HasMaxLength(200);
            entity.Property(e => e.LatestActivity)
                .HasDefaultValueSql("(getutcdate())")
                .HasColumnType("datetime");

            entity.HasOne(d => d.Language).WithMany(p => p.Users)
                .HasForeignKey(d => d.LanguageId)
                .HasConstraintName("FK_Users_LanguageId_Languages_Id");

            entity.HasOne(d => d.Title).WithMany(p => p.Users)
                .HasForeignKey(d => d.TitleId)
                .HasConstraintName("FK_Users_TitleId_Titles_Id");
            entity.HasQueryFilter(r => r.DeletedAt == null);
        });

        modelBuilder.Entity<UserDiscipline>(entity =>
        {
            entity.HasKey(e => e.RelationId);

            entity.Property(e => e.RelationId).HasDefaultValueSql("(newid())");

            entity.HasOne(d => d.Discipline).WithMany(p => p.UserDisciplines)
                .HasForeignKey(d => d.DisciplineId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UserDisciplines_DisciplineId_Disciplines_Id");

            entity.HasOne(d => d.User).WithMany(p => p.UserDisciplines)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UserDisciplines_UserId_Users_Id");
        });

        modelBuilder.Entity<VersionInfo>(entity =>
        {
            entity
                .HasNoKey()
                .ToTable("VersionInfo");

            entity.HasIndex(e => e.Version, "UC_Version")
                .IsUnique()
                .IsClustered();

            entity.Property(e => e.AppliedOn).HasColumnType("datetime");
            entity.Property(e => e.Description).HasMaxLength(1024);
        });

        modelBuilder.Entity<Visibility>(entity =>
        {
            entity.Property(e => e.Id).HasDefaultValueSql("(newid())");
            entity.Property(e => e.DisplayName).HasMaxLength(50);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}