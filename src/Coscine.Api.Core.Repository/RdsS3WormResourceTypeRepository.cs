using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class RdsS3WormResourceTypeRepository : IRdsS3WormResourceTypeRepository
{
    private readonly RepositoryBase<RdsS3WormResourceType> _rdsS3WormResourceType;

    public RdsS3WormResourceTypeRepository(RepositoryContext repositoryContext)
    {
        _rdsS3WormResourceType = new(repositoryContext);
    }

    public void Create(RdsS3WormResourceType rdsS3wormResourceType)
    {
        _rdsS3WormResourceType.Create(rdsS3wormResourceType);
    }

    public async Task<RdsS3WormResourceType?> GetAsync(Guid rdsS3WormResourceTypeId, bool trackChanges)
    {
        return await _rdsS3WormResourceType.FindByCondition(x => x.Id == rdsS3WormResourceTypeId, trackChanges)
            .SingleOrDefaultAsync();
    }
}