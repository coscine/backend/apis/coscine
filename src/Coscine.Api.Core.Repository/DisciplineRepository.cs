﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class DisciplineRepository(RepositoryContext repositoryContext) : IDisciplineRepository
{
    private readonly RepositoryBase<Discipline> _discipline = new(repositoryContext);

    public async Task<PagedEnumerable<Discipline>> GetPagedAsync(DisciplineParameters disciplineParameters, bool trackChanges)
    {
        var disciplineQuery = _discipline.FindAll(trackChanges);

        var disciplines = await disciplineQuery
            .SortDisciplines(disciplineParameters.OrderBy)
            .Skip((disciplineParameters.PageNumber - 1) * disciplineParameters.PageSize)
            .Take(disciplineParameters.PageSize)
            .ToListAsync();

        var count = await disciplineQuery.CountAsync();

        return new PagedEnumerable<Discipline>(disciplines, count, disciplineParameters.PageNumber, disciplineParameters.PageSize);
    }

    public async Task<Discipline?> GetAsync(Guid disciplineId, bool trackChanges)
    {
        return await _discipline.FindByCondition(x => x.Id == disciplineId, trackChanges).SingleOrDefaultAsync();
    }
}