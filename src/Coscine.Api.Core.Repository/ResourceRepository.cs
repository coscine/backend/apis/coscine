﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Repository.Extensions.Utility;
using Coscine.Api.Core.Repository.HelperModels;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.Helpers;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Text.Json;
using VDS.RDF;
using VDS.RDF.Query;

namespace Coscine.Api.Core.Repository;

public sealed class ResourceRepository(RepositoryContext repositoryContext, IRdfRepositoryBase rdfRepositoryBase) : IResourceRepository
{
    private readonly RepositoryBase<Resource> _resource = new(repositoryContext);

    public void Create(Resource resource, Guid creatorId)
    {
        resource.Creator = creatorId;
        _resource.Create(resource);
    }

    public void Delete(Resource resource)
    {
        resource.Deleted = true;
    }

    public Uri GenerateGraphName(Resource resource)
    {
        return RdfUris.CoscineResources.AppendToPath(resource.Id.ToString());
    }

    public Uri GenerateAclGraphName(Resource resource)
    {
        return GenerateGraphName(resource).AppendToPath("?ext=acl");
    }

    public async Task DeleteGraphsAsync(Resource resource)
    {
        var resourceGraphName = GenerateGraphName(resource);
        var aclGraphName = GenerateAclGraphName(resource);

        // Remove the resource graphs
        await rdfRepositoryBase.ClearGraphAsync(resourceGraphName);
        await rdfRepositoryBase.ClearGraphAsync(aclGraphName);

        var sparql = new SparqlParameterizedString
        {
            CommandText = @"
                DELETE WHERE {
                    GRAPH @graph {
                        @subject ?p ?o
                    }
                }"
        };

        sparql.SetUri("graph", RdfUris.TrellisGraph);
        sparql.SetUri("subject", resourceGraphName);

        // Delete from trellis graph
        await rdfRepositoryBase.RunUpdateAsync(sparql);
    }

    public async Task UpdateGraphsAsync(Resource resource)
    {
        // Delete the graphs
        await DeleteGraphsAsync(resource);

        // Create the graphs
        var resourceGraph = await GenerateResourceGraphAsync(resource);
        var aclGraph = GenerateAclGraph(resource, resourceGraph);

        // Add the graphs back
        await rdfRepositoryBase.AddGraphAsync(resourceGraph);
        await rdfRepositoryBase.AddGraphAsync(aclGraph);

        // Add the graph back
        var trellisGraph = GenerateTrellisGraph(resource);
        await rdfRepositoryBase.AddGraphAsync(trellisGraph);
    }

    private async Task<Graph> GenerateResourceGraphAsync(Resource resource)
    {
        var resourceGraphName = GenerateGraphName(resource);

        // Create the resource graph
        var resourceGraph = new Graph(resourceGraphName)
        {
            BaseUri = resourceGraphName
        };

        resourceGraph.AssertToGraph(resourceGraphName, RdfUris.A, RdfUris.DcatCatalogClass);
        resourceGraph.AssertToGraph(resourceGraphName, RdfUris.A, RdfUris.PimStorageClass);

        var resourceTypeUri = RdfUris.CoscineResourceTypes.AppendToPath(resource.Type.Id.ToString());
        resourceGraph.AssertToGraph(resourceGraphName, RdfUris.DcatService, resourceTypeUri);

        if (resource.ResourceName is not null)
        {
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.DcTermsTitle, resource.ResourceName);
        }

        if (resource.DisplayName is not null)
        {
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.DcTermsAlternative, resource.DisplayName);
        }

        if (resource.Visibility?.DisplayName.Contains("Public", StringComparison.InvariantCultureIgnoreCase) == true)
        {
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.CoscineTermsResourceVisibility, RdfUris.CoscineTermsVisibilityPublic);
        }

        else
        {
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.CoscineTermsResourceVisibility, RdfUris.CoscineTermsVisibilityProjectMember);
        }

        if (resource?.License?.DisplayName is not null)
        {
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.DcTermsLicense, resource.License.DisplayName);
        }

        var resourceKeywords = resource?.Keywords?.Split(';', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries) ?? [];

        foreach (var keyword in resourceKeywords)
        {
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.DcTermsSubject, keyword);
        }

        if (resource?.UsageRights is not null)
        {
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.DcTermsRights, resource.UsageRights);
        }

        if (resource?.Description is not null)
        {
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.DcTermsDescription, resource.Description);
        }

        if (resource?.ApplicationProfile is not null)
        {
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.DcTermsConformsTo, new Uri(resource.ApplicationProfile));
        }

        if (resource?.FixedValues is not null)
        {
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.CoscineTermsResourceFixedValues, JsonSerializer.Serialize(resource.FixedValues));
        }

        if (resource?.Creator is not null && resource.Creator.HasValue)
        {
            var creatorId = resource.Creator.Value.ToString();
            var resourceCreatorUri = RdfUris.CoscineUsers.AppendToPath(creatorId);
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.DcTermsCreator, resourceCreatorUri);
        }

        if (resource?.Archived is not null)
        {
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.CoscineTermsResourceArchived, resource.Archived.ToString().ToLower(), RdfUris.XsdBoolean);
        }

        if (resource?.Id is not null)
        {
            var resourceHandleName = RdfUris.CoscineResources.AppendToPath(resource.Id.ToString());
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.FoafHomepage, resourceHandleName);
        }

        if (resource?.Deleted is not null)
        {
            resourceGraph.AssertToGraph(resourceGraphName, RdfUris.CoscineTermsResourceDeleted, resource.Deleted.ToString().ToLower(), RdfUris.XsdBoolean);
        }

        // Reinstate the catalog assignments
        var oldResourceGraph = await rdfRepositoryBase.GetGraph(new Uri(resourceGraphName.AbsoluteUri));
        if (oldResourceGraph.Triples.Count != 0)
        {
            foreach (var result in oldResourceGraph.GetTriplesWithPredicate(RdfUris.DcatCatalog))
            {
                if (result.Object.NodeType == NodeType.Uri)
                {
                    if (result.Object is IUriNode catalogedUri)
                    {
                        resourceGraph.AssertToGraph(resourceGraphName, RdfUris.DcatCatalog, catalogedUri.Uri);
                    }
                }
            }
        }

        if (resource?.ProjectResources is not null)
        {
            foreach (var projectResource in resource.ProjectResources)
            {
                var blankNode = resourceGraph.CreateBlankNode();
                var projectUri = RdfUris.CoscineProjects.AppendToPath(projectResource.ProjectId.ToString());

                resourceGraph.AssertToGraph(blankNode, RdfUris.A, RdfUris.AclAuthorizationClass);
                resourceGraph.AssertToGraph(blankNode, RdfUris.AclAgentGroup, projectUri);
                resourceGraph.AssertToGraph(blankNode, RdfUris.AclAccessTo, resourceGraphName);
                resourceGraph.AssertToGraph(blankNode, RdfUris.AclDefault, resourceGraphName);
                resourceGraph.AssertToGraph(blankNode, RdfUris.AclMode, RdfUris.AclReadClass);
                resourceGraph.AssertToGraph(blankNode, RdfUris.AclMode, RdfUris.AclWriteClass);
            }

            if (resource.DateCreated is not null && resource.DateCreated.HasValue)
            {
                resourceGraph.AssertToGraph(resourceGraphName, RdfUris.DcTermsCreated, resource.DateCreated.Value.ToString(), RdfUris.XsdDateTime);
            }
        }

        return resourceGraph;
    }

    private PatchGraph GenerateTrellisGraph(Resource resource)
    {
        var resourceGraphName = GenerateGraphName(resource);

        var trellisGraph = PatchGraph.Empty(RdfUris.TrellisGraph);
        // Add data to the trellis graph
        trellisGraph.AssertToGraph(resourceGraphName, RdfUris.A, RdfUris.LdpBasicContainerClass);
        MetadataUtility.AddModifiedDate(trellisGraph, resourceGraphName.ToString());

        trellisGraph.AssertToGraph(
                resourceGraphName,
                RdfUris.DcTermsIsPartOf,
                RdfUris.CoscineResources);
        trellisGraph.AssertToGraph(
                RdfUris.CoscineResources,
                RdfUris.A,
                RdfUris.LdpBasicContainerClass);
        MetadataUtility.AddModifiedDate(trellisGraph, RdfUris.CoscineResources.ToString());

        trellisGraph.AssertToGraph(
                RdfUris.CoscineResources,
                RdfUris.DcTermsIsPartOf,
                RdfUris.CoscinePrefix);
        trellisGraph.AssertToGraph(
                RdfUris.CoscinePrefix,
                RdfUris.A,
                RdfUris.LdpBasicContainerClass);
        MetadataUtility.AddModifiedDate(trellisGraph, RdfUris.CoscinePrefix.ToString());

        return trellisGraph;
    }

    private Graph GenerateAclGraph(Resource resource, Graph resourceGraph)
    {
        var resourceACLGraphName = GenerateAclGraphName(resource);
        var aclGraph = new Graph(resourceACLGraphName)
        {
            BaseUri = resourceACLGraphName
        };

        var aclSubjects = resourceGraph.GetTriplesWithObject(RdfUris.AclAuthorizationClass).Select((triple) => triple.Subject);
        foreach (var subject in aclSubjects)
        {
            var subjectNode = aclGraph.CreateBlankNode();
            foreach (var triple in resourceGraph.GetTriplesWithSubject(subject))
            {
                aclGraph.Assert(new Triple(
                    subjectNode,
                    triple.Predicate,
                    triple.Object
                ));
            }
        }

        return aclGraph;
    }

    public async Task CreateGraphsAsync(Resource resource)
    {
        // Create the graphs
        var resourceGraph = await GenerateResourceGraphAsync(resource);
        var trellisGraph = GenerateTrellisGraph(resource);
        var aclGraph = GenerateAclGraph(resource, resourceGraph);

        // Store the graphs
        await rdfRepositoryBase.AddGraphAsync(resourceGraph);
        await rdfRepositoryBase.AddGraphAsync(trellisGraph);
        await rdfRepositoryBase.AddGraphAsync(aclGraph);
    }


    public async Task<PagedEnumerable<Resource>> GetPagedAsync(
        ResourceAdminParameters resourceParameters,
        bool trackChanges
    )
    {
        var resourceQuery = _resource
            .FindAll(trackChanges)
            .Include(r => r.Visibility)
            .Include(r => r.ResourceDisciplines)
            .ThenInclude(rd => rd.Discipline)
            .Include(r => r.ProjectResources)
            .Include(r => r.License)
            .Include(r => r.Type)
            .Include(r => r.MetadataExtractions)
            .Where(r => resourceParameters.IncludeDeleted == true || !r.Deleted)
            .AsSplitQuery();

        var resources = await resourceQuery
            .SortResources(resourceParameters.OrderBy)
            .Skip((resourceParameters.PageNumber - 1) * resourceParameters.PageSize)
            .Take(resourceParameters.PageSize)
            .ToListAsync();

        var count = await resourceQuery.CountAsync();

        return new PagedEnumerable<Resource>(
            resources,
            count,
            resourceParameters.PageNumber,
            resourceParameters.PageSize
        );
    }

    public async Task<PagedEnumerable<Resource>> GetPagedAsync(
        Guid projectId,
        ResourceParameters resourceParameters,
        bool trackChanges
    )
    {
        var resourceQuery = _resource
            .FindAll(trackChanges)
            .Include(r => r.Visibility)
            .Include(r => r.ResourceDisciplines)
            .ThenInclude(rd => rd.Discipline)
            .Include(r => r.ProjectResources)
            .Include(r => r.License)
            .Include(r => r.Type)
            .Include(r => r.MetadataExtractions)
            .Where(r => r.ProjectResources.Any(pr => pr.ProjectId == projectId) && !r.Deleted)
            .AsSplitQuery();

        var resources = await resourceQuery
            .SortResources(resourceParameters.OrderBy)
            .Skip((resourceParameters.PageNumber - 1) * resourceParameters.PageSize)
            .Take(resourceParameters.PageSize)
            .ToListAsync();

        var count = await resourceQuery.CountAsync();

        return new PagedEnumerable<Resource>(
            resources,
            count,
            resourceParameters.PageNumber,
            resourceParameters.PageSize
        );
    }

    public async Task<IEnumerable<Resource>> GetAllAsync(
        Guid projectId,
        Guid resourceTypeId,
        bool trackChanges
    )
    {
        return await _resource
            .FindAll(trackChanges)
            .Include(r => r.ProjectResources)
            .Include(r => r.MetadataExtractions)
            .Where(r =>
                r.ProjectResources.Any(pr => pr.ProjectId == projectId)
                && !r.Deleted
                && r.TypeId == resourceTypeId
            )
            .ToListAsync();
    }

    public async Task<Resource?> GetAsync(Guid projectId, Guid resourceId, bool trackChanges)
    {
        return await _resource
            .FindByCondition(x => x.Id == resourceId, trackChanges)
            .Include(r => r.Visibility)
            .Include(r => r.ResourceDisciplines)
            .ThenInclude(rd => rd.Discipline)
            .Include(r => r.ProjectResources)
            .Include(r => r.License)
            .Include(r => r.Type)
            .Include(r => r.MetadataExtractions)
            .Where(r => r.ProjectResources.Any(pr => pr.ProjectId == projectId) && !r.Deleted)
            .AsSplitQuery()
            .SingleOrDefaultAsync();
    }

    public async Task<Resource?> GetAsync(Guid resourceId, bool trackChanges)
    {
        return await _resource
            .FindByCondition(x => x.Id == resourceId, trackChanges)
            .Include(r => r.Visibility)
            .Include(r => r.ResourceDisciplines)
            .ThenInclude(rd => rd.Discipline)
            .Include(r => r.ProjectResources)
            .Include(r => r.License)
            .Include(r => r.Type)
            .Include(r => r.MetadataExtractions)
            .Where(r => !r.Deleted)
            .AsSplitQuery()
            .SingleOrDefaultAsync();
    }

    public async Task<Resource?> GetIncludingDeletedAsync(Guid resourceId, bool trackChanges)
    {
        return await _resource
            .FindByCondition(x => x.Id == resourceId, trackChanges)
            .Include(r => r.ProjectResources)
            .ThenInclude(pr => pr.Project)
            .ThenInclude(p => p.ProjectRoles)
            .ThenInclude(pr => pr.User)
            .AsSplitQuery()
            .SingleOrDefaultAsync();
    }

    public async Task MergeAsync(Guid fromUserId, Guid toUserId)
    {
        foreach (var r in await _resource.FindByCondition(x => x.Creator == fromUserId, trackChanges: true).ToListAsync())
        {
            r.Creator = toUserId;
        }
    }
}
