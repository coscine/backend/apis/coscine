using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class VisibilityRepository : IVisibilityRepository
{
    private readonly RepositoryBase<Visibility> _visibility;

    public VisibilityRepository(RepositoryContext repositoryContext)
    {
        _visibility = new(repositoryContext);
    }

    public async Task<PagedEnumerable<Visibility>> GetPagedAsync(VisibilityParameters visibilityParameters, bool trackChanges)
    {
        var visibilityQuery = _visibility.FindAll(trackChanges);

        var visibilities = await visibilityQuery
            .SortVisibilities(visibilityParameters.OrderBy)
            .Skip((visibilityParameters.PageNumber - 1) * visibilityParameters.PageSize)
            .Take(visibilityParameters.PageSize)
            .ToListAsync();

        var count = await visibilityQuery.CountAsync();

        return new PagedEnumerable<Visibility>(visibilities, count, visibilityParameters.PageNumber, visibilityParameters.PageSize);
    }

    public async Task<Visibility?> GetAsync(Guid visibilityId, bool trackChanges)
    {
        return await _visibility.FindByCondition(x => x.Id == visibilityId, trackChanges).SingleOrDefaultAsync();
    }
}