﻿using Coscine.Api.Core.Entities.Models.QuadStore;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.Extensions.Logging;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Repository;

public class MetadataCatalogRepository(IStore store, ILogger<MetadataCatalogRepository> logger) : IMetadataCatalogRepository
{
    private readonly IStore _store = store;
    private readonly ILogger<MetadataCatalogRepository> _logger = logger;

    public MetadataCatalog? Get(Uri rawDataId)
    {
        try
        {
            // Load model
            var model = new Model(_store, new UriRef(rawDataId.AbsoluteUri));

            // Load Resource
            return model?.GetResource<MetadataCatalog>(rawDataId);
        }
        catch (Exception e)
        {
            _logger.LogWarning(e, "Exception while loading from the quad store");
            return null;
        }
    }
}
