using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.ECSManager;
using Microsoft.Extensions.Options;

namespace Coscine.Api.Core.Repository;

public class EcsManagerFactory(IOptionsSnapshot<RdsS3ResourceTypeConfiguration> rdsS3ResourceTypeConfigurations,
                               IOptionsSnapshot<RdsS3WormResourceTypeConfiguration> rdsS3WormResourceTypeConfigurations,
                               IOptionsSnapshot<RdsResourceTypeConfiguration> rdsResourceTypeConfigurations) : IEcsManagerFactory
{
    public EcsManager GetRdsS3EcsManager(string configKey)
    {
        try
        {
            var rdsS3Config = rdsS3ResourceTypeConfigurations.Get(configKey)
                ?? throw new ArgumentException($"No configuration found for key: {configKey}");

            if (rdsS3Config.RdsS3EcsManagerConfiguration == null)
            {
                throw new ArgumentException(@"Config does not contain a ""RdsS3EcsManagerConfiguration""");
            }

            return new EcsManager
            {
                EcsManagerConfiguration = rdsS3Config.RdsS3EcsManagerConfiguration
            };
        }
        catch (Exception ex)
        {
            throw new InvalidOperationException($"Failed to create RdsS3EcsManager for config key: {configKey}", ex);
        }
    }

    public EcsManager GetRdsS3WormEcsManager(string configKey)
    {
        try
        {
            var rdsS3WormConfig = rdsS3WormResourceTypeConfigurations.Get(configKey)
                ?? throw new ArgumentException($"No configuration found for key: {configKey}");

            if (rdsS3WormConfig.RdsS3EcsManagerConfiguration == null)
            {
                throw new ArgumentException(@"Config does not contain a ""RdsS3WormEcsManagerConfiguration""");
            }

            return new EcsManager
            {
                EcsManagerConfiguration = rdsS3WormConfig.RdsS3EcsManagerConfiguration
            };
        }
        catch (Exception ex)
        {
            throw new InvalidOperationException($"Failed to create RdsS3EcsManager for config key: {configKey}", ex);
        }
    }

    public EcsManager GetRdsEcsManager(string configKey)
    {
        try
        {
            var rdsConfig = rdsResourceTypeConfigurations.Get(configKey)
                ?? throw new ArgumentException($"No configuration found for key: {configKey}");

            if (rdsConfig.EcsManagerConfiguration == null)
            {
                throw new ArgumentException(@"Config does not contain a ""RdsS3EcsManagerConfiguration""");
            }

            return new EcsManager
            {
                EcsManagerConfiguration = rdsConfig.EcsManagerConfiguration
            };
        }
        catch (Exception ex)
        {
            throw new InvalidOperationException($"Failed to create RdsS3EcsManager for config key: {configKey}", ex);
        }
    }

    public EcsManager GetRdsS3UserEcsManager(string configKey)
    {
        try
        {
            var rdsS3Config = rdsS3ResourceTypeConfigurations.Get(configKey)
                ?? throw new ArgumentException($"No configuration found for key: {configKey}");

            if (rdsS3Config.UserEcsManagerConfiguration == null)
            {
                throw new ArgumentException(@"Config does not contain a ""UserEcsManagerConfiguration""");
            }

            return new EcsManager
            {
                EcsManagerConfiguration = rdsS3Config.UserEcsManagerConfiguration
            };
        }
        catch (Exception ex)
        {
            throw new InvalidOperationException($"Failed to create UserEcsManager for config key: {configKey}", ex);
        }
    }
}
