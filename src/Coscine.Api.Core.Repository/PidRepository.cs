﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Repository.Extensions.Utility;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Coscine.Api.Core.Repository;

public sealed class PidRepository : IPidRepository
{
    private readonly PidConfiguration _pidConfiguration;
    private readonly ConnectionConfiguration _connectionConfiguration;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly IProjectRepository _projectRepository;
    private readonly IResourceRepository _resourceRepository;
    private readonly RepositoryBase<Project> _project;
    private readonly RepositoryBase<Resource> _resource;

    public PidRepository(
        IOptionsMonitor<PidConfiguration> pidConfiguration,
        IOptionsMonitor<ConnectionConfiguration> connectionConfiguration,
        IHttpClientFactory httpClientFactory,
        RepositoryContext repositoryContext,
        IProjectRepository projectRepository,
        IResourceRepository resourceRepository
    )
    {
        _pidConfiguration = pidConfiguration.CurrentValue;
        _connectionConfiguration = connectionConfiguration.CurrentValue;

        // Ensure the configuration keys are set
        ArgumentNullException.ThrowIfNull(connectionConfiguration);
        ArgumentException.ThrowIfNullOrEmpty(_connectionConfiguration.ServiceUrl);
        ArgumentNullException.ThrowIfNull(pidConfiguration);
        ArgumentException.ThrowIfNullOrEmpty(_pidConfiguration.Prefix);
        ArgumentException.ThrowIfNullOrEmpty(_pidConfiguration.Url);
        ArgumentException.ThrowIfNullOrEmpty(_pidConfiguration.User);
        ArgumentException.ThrowIfNullOrEmpty(_pidConfiguration.Password);

        _httpClientFactory = httpClientFactory;
        _projectRepository = projectRepository;
        _resourceRepository = resourceRepository;

        _project = new(repositoryContext);
        _resource = new(repositoryContext);
    }

    public async Task<Pid?> GetAsync(string prefix, string suffix, bool verifyHandle, bool trackChanges)
    {
        // Step 1: Verify, that we can resolve something with the prefix and suffix using the handle API
        if (verifyHandle)
        {
            var handleClient = _httpClientFactory.CreateHandleClient(_pidConfiguration);

            // Make the GET request
            var response = await handleClient.GetAsync($"{prefix}/{suffix}");

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }
        }

        // Step 2: Verify, that the PID could be from us; The suffix must be a valid GUID if it is a Coscine PID
        if (!Guid.TryParse(suffix, out var guid))
        {
            // The suffix is not a valid GUID, so it cannot be a Coscine PID
            return new Pid
            {
                Prefix = prefix,
                Suffix = suffix,
                Type = null,
                IsEntityValid = false
            };
        }

        // Step 3: Check if the related entity exists and assign the type, if possible
        var projectEntity = await _projectRepository.GetIncludingDeletedAsync(guid, trackChanges);
        var resourceEntity = projectEntity is null
            ? await _resourceRepository.GetIncludingDeletedAsync(guid, trackChanges)
            : null;

        if (projectEntity is not null && !projectEntity.Deleted || resourceEntity is not null && !resourceEntity.Deleted)
        {
            var type = projectEntity is not null ? PidType.Project : PidType.Resource;
            return new Pid
            {
                Prefix = prefix,
                Suffix = suffix,
                Type = type,
                IsEntityValid = true // Entity exists and is NOT deleted
            };
        }

        return new Pid
        {
            Prefix = prefix,
            Suffix = suffix,
            Type = null,
            IsEntityValid = false // Entity does not exist or is deleted
        };
    }

    public async Task<PagedEnumerable<Pid>> GetPagedAsync(string prefix, PidParameters pidParameters, bool trackChanges)
    {
        // Perform for projects only
        if (pidParameters.IncludeProjects && !pidParameters.IncludeResources)
        {
            var projectQuery = _project
                .FindAll(trackChanges)
                .Where(project => pidParameters.IncludeDeleted || !project.Deleted);

            var projectPids = await projectQuery
                .SortProjects(pidParameters.OrderBy)
                .Skip((pidParameters.PageNumber - 1) * pidParameters.PageSize)
                .Take(pidParameters.PageSize)
                .Select(project => new Pid
                {
                    Prefix = prefix,
                    Suffix = project.Id.ToString(),
                    Type = PidType.Project,
                    IsEntityValid = !project.Deleted
                })
                .ToListAsync();

            var count = await projectQuery.CountAsync();

            return new PagedEnumerable<Pid>(projectPids, count, pidParameters.PageNumber, pidParameters.PageSize);
        }

        // Perform for resources only
        if (!pidParameters.IncludeProjects && pidParameters.IncludeResources)
        {
            var resourceQuery = _resource
                .FindAll(trackChanges)
                .Where(resource => pidParameters.IncludeDeleted || !resource.Deleted);

            var resourcePids = await resourceQuery
                .SortResources(pidParameters.OrderBy)
                .Skip((pidParameters.PageNumber - 1) * pidParameters.PageSize)
                .Take(pidParameters.PageSize)
                .Select(resource => new Pid
                {
                    Prefix = prefix,
                    Suffix = resource.Id.ToString(),
                    Type = PidType.Resource,
                    IsEntityValid = !resource.Deleted
                })
                .ToListAsync();

            var count = await resourceQuery.CountAsync();

            return new PagedEnumerable<Pid>(resourcePids, count, pidParameters.PageNumber, pidParameters.PageSize);
        }

        // Should both projects and resources be needed, create a union over both SQL tables and directly page over the union table
        if (pidParameters.IncludeProjects && pidParameters.IncludeResources)
        {
            var projectAndResourceQuery = _project
                .FindAll(trackChanges)
                .Select(project => new
                {
                    project.Id,
                    EntityType = PidType.Project,
                    project.Deleted
                })
                .Where(project => pidParameters.IncludeDeleted || !project.Deleted)
                .Union(
                    _resource
                        .FindAll(trackChanges)
                        .Select(resource => new
                        {
                            resource.Id,
                            EntityType = PidType.Resource,
                            resource.Deleted
                        })
                        .Where(resource => pidParameters.IncludeDeleted || !resource.Deleted)
                );

            var projectAndResourcePids = await projectAndResourceQuery
                .OrderBy(x => x.Id)
                .Skip((pidParameters.PageNumber - 1) * pidParameters.PageSize)
                .Take(pidParameters.PageSize)
                .Select(x => new Pid
                {
                    Prefix = prefix,
                    Suffix = x.Id.ToString(),
                    Type = x.EntityType,
                    IsEntityValid = !x.Deleted
                })
                .ToListAsync();

            var count = await projectAndResourceQuery.CountAsync();

            return new PagedEnumerable<Pid>(projectAndResourcePids, count, pidParameters.PageNumber, pidParameters.PageSize);
        }

        return new PagedEnumerable<Pid>(new List<Pid>(), 0, pidParameters.PageNumber, pidParameters.PageSize);
    }
}