﻿using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.Conflict;
using Coscine.Api.Core.Entities.Exceptions.InternalServerError;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Models.QuadStore;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Repository.Extensions.Utility;
using Coscine.Api.Core.Repository.HelperModels;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Enums.Utility;
using Coscine.Api.Core.Shared.Helpers;
using Coscine.Api.Core.Shared.QueryParameters;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.Extensions.Logging;
using Semiodesk.Trinity;
using System.Security.Cryptography;
using System.Text.Json;
using System.Text.RegularExpressions;
using VDS.RDF;
using VDS.RDF.Nodes;
using VDS.RDF.Parsing;
using VDS.RDF.Query;
using Resource = Coscine.Api.Core.Entities.Models.Resource;
using ResourceNotFoundException = Coscine.Api.Core.Entities.Exceptions.NotFound.ResourceNotFoundException;

namespace Coscine.Api.Core.Repository;

public class TreeRepository : ITreeRepository
{
    private readonly IRdfRepositoryBase _rdfRepositoryBase;
    private readonly ApplicationProfileUtility _applicationProfileUtility;
    private readonly MetadataUtility _metadataUtility;
    private readonly ILogger<TreeRepository> _logger;
    private readonly IResourceRdfRepository _resourceRdfRepository;
    private readonly IProvenanceRepository _provenanceRepository;
    private readonly IStore _store;

    public TreeRepository(
        IRdfRepositoryBase rdfRepositoryBase,
        IResourceRepository resourceRepository,
        ILogger<TreeRepository> logger,
        IResourceRdfRepository resourceRdfRepository,
        IProvenanceRepository provenanceRepository,
        IStore store
    )
    {
        _rdfRepositoryBase = rdfRepositoryBase;
        _applicationProfileUtility = new(_rdfRepositoryBase, resourceRepository);
        _metadataUtility = new(_rdfRepositoryBase);
        _logger = logger;
        _resourceRdfRepository = resourceRdfRepository;
        _provenanceRepository = provenanceRepository;
        _store = store;
    }

    /// <inheritdoc/>
    public async Task<PagedEnumerable<MetadataTree>> GetNewestPagedMetadataAsync(
        Guid resourceId,
        MetadataTreeParameters treeParameters
    )
    {
        var resourceGraphPrefix = $"{RdfUris.CoscineResources}{resourceId}";
        var pathGraphUri = new Uri(
            $"{resourceGraphPrefix}/{(treeParameters.Path?.StartsWith('/') == true ? treeParameters.Path[1..] : treeParameters.Path)}"
        );

        var query = new SparqlParameterizedString
        {
            CommandText = """

                SELECT (?version AS ?currentVersion) (GROUP_CONCAT(DISTINCT ?version2; SEPARATOR=";") AS ?versions) (GROUP_CONCAT(DISTINCT ?child ; SEPARATOR=";") AS ?children)
                {
                    GRAPH @resourceId {
                        @resourceId @catalog ?g .
                    }

                    GRAPH ?g{
                        ?g a @metadataService .
                        ?g @hasMetadata ?m .
                    }

                    OPTIONAL {
                        ?g @catalog ?child .
                        ?child  a @metadataService.
                    }

                    GRAPH ?m {
                        ?m @hasMetadata ?version .
                        ?m @hasMetadata ?version2 .
                        FILTER NOT EXISTS {
                            ?s @revisionOf ?version .
                        }
                    }

                    FILTER(CONTAINS(STR(?m), @path) && !CONTAINS(STR(?version), "extracted=true"))  .
                    FILTER(!CONTAINS(STR(?version2), "extracted=true"))  .

                    OPTIONAL {
                        ?version @invalidatedBy ?invalidator
                    }

                    OPTIONAL {
                        ?parentVersion @revisionOf* ?version .
                        ?parentVersion @invalidatedBy ?parentInvalidator
                    }

                    FILTER (!BOUND(?invalidator) && !BOUND(?parentInvalidator)) .
                }
                GROUP BY ?version

"""
        };

        query.SetUri("resourceId", new Uri(resourceGraphPrefix));
        query.SetUri("catalog", RdfUris.DcatCatalog);
        query.SetUri("metadataService", RdfUris.FdpMetadataServiceClass);
        query.SetUri("hasMetadata", RdfUris.FdpHasMetadata);
        query.SetUri("revisionOf", RdfUris.ProvWasRevisionOf);
        query.SetUri("invalidatedBy", RdfUris.ProvWasInvalidatedBy);
        query.SetLiteral("path", pathGraphUri.AbsoluteUri, normalizeValue: true);

        var count = await query.GetCountOrDefaultAsync(_rdfRepositoryBase);

        query.Sort<MetadataTree>(treeParameters.OrderBy, new() { { "version", ap => ap.Version }, });

        query.Paginate(treeParameters.PageSize, treeParameters.PageNumber);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        var applicationProfileGraph = await _applicationProfileUtility.GetApplicationProfile(resourceId);

        var metadata = new List<MetadataTree>();
        foreach (var result in resultSet)
        {
            // Parse SPARQL result set to a strongly typed object
            var metadataTreeWithVersionId = new Uri(result.Value("currentVersion").ToString());

            var graph = await _rdfRepositoryBase.GetGraph(metadataTreeWithVersionId);

            var idUriData = new TreeUriResolver(metadataTreeWithVersionId);
            var versionsUriData = result
                .Value("versions")
                .AsValuedNode()
                .AsString()
                .Split(";", StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries)
                .Select(uriString => new TreeUriResolver(new Uri(uriString)));

            // TODO: currently there is a bug in the old metadata generation.
            // As such, loading of children doesn't work and folders have no metadata.
            // This has to be fixed, once it is fixed in the old API.
            var hasChildren =
                result
                    .Value("children")
                    .AsValuedNode()
                    .AsString()
                    .Trim()
                    .Split(";", StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries)
                    .Length > 0;

            FixDatatypes(graph, applicationProfileGraph);

            // Add trailing forward slash '/', if metadata tree has children
            var path = hasChildren ? $"{idUriData.Path}/" : idUriData.Path;

            MetadataTreeExtracted? extractedTree = null;
            Provenance? provenance = null;

            // Get the extracted graph and provenance information, if desired
            if (treeParameters.IncludeExtractedMetadata || treeParameters.IncludeProvenance)
            {
                var metadataCatalogId = new Uri(
                    $"{metadataTreeWithVersionId.ToString()[..metadataTreeWithVersionId.ToString().IndexOf("&version=")]}"
                );

                var metadataCatalogModel = new Model(_store, new UriRef(metadataCatalogId.AbsoluteUri));

                try
                {
                    var entity = metadataCatalogModel.GetResource<MetadataEntity>(metadataTreeWithVersionId);
                    provenance = treeParameters.IncludeProvenance ? CreateProvenanceObject(entity, metadataCatalogModel) : null;

                    if (treeParameters.IncludeExtractedMetadata)
                    {
                        extractedTree = await GetExtractedTree(entity, path, treeParameters.Format, hasChildren);
                    }
                }
                catch (ArgumentException)
                {
                    // Skipping metadataset that might be broken
                    _logger.LogDebug("Skipping for {metadataTreeWithVersionId}", metadataTreeWithVersionId);
                }
            }

            metadata.Add(
                new MetadataTree
                {
                    Id = metadataTreeWithVersionId,
                    Path = path,
                    Version = idUriData.Version.ToString(),
                    AvailableVersions = versionsUriData.Select(v => v.Version.ToString()),
                    Definition = _rdfRepositoryBase.WriteGraph(graph, treeParameters.Format),
                    Format = treeParameters.Format,
                    HasChildren = hasChildren,
                    Extracted = extractedTree,
                    Provenance = provenance
                }
            );
        }

        return new PagedEnumerable<MetadataTree>(metadata, count, treeParameters.PageNumber, treeParameters.PageSize);
    }

    /// <summary>
    /// Gets metadata based on a given query.
    /// </summary>
    /// <param name="resourceId">Targeted resource</param>
    /// <param name="metadataTreeQueryParameters">Query parameters</param>
    /// <returns>Found metadata tree</returns>
    /// <exception cref="ResourceNotFoundException"></exception>
    public async Task<MetadataTree?> GetMetadataAsync(Guid resourceId, MetadataTreeQueryParameters metadataTreeQueryParameters)
    {
        try
        {
            // Get the resource in rdf
            var resourceRdf = _resourceRdfRepository.Get(resourceId) ?? throw new ResourceNotFoundException(resourceId);

            // Construct model group
            var dataCatalogModelGroup = new ModelGroup(
                _store,
                resourceRdf.DataCatalogIds.Select(x => new Model(_store, new UriRef(x.AbsoluteUri)))
            );

            var resourceGraphPrefix = $"{RdfUris.CoscineResources}{resourceId}";
            var pathGraphUri = new Uri(
                $"{resourceGraphPrefix}/{(metadataTreeQueryParameters.Path.StartsWith('/') == true ? metadataTreeQueryParameters.Path[1..] : metadataTreeQueryParameters.Path)}"
            );
            // Get the data catalog and extract the metadata catalog ID.
            var dataCatalogList = dataCatalogModelGroup
                .AsQueryable<DataCatalog>()
                .OrderByDescending(x => x.MetadataCatalogId)
                .ToList();
            var dataCatalog = dataCatalogList.First(x => x.ToString().Contains(pathGraphUri.AbsoluteUri));

            // Construct new model group
            var metadataCatalogModel = new Model(_store, new UriRef(dataCatalog?.MetadataCatalogId?.AbsoluteUri));

            var versions = metadataCatalogModel.AsQueryable<MetadataEntity>().ToList().OrderByDescending(x => x.ComputedVersion);

            MetadataEntity? entity = null;

            if (metadataTreeQueryParameters.Version is null)
            {
                // Return the newest version of each element
                entity = versions.FirstOrDefault();
            }
            else
            {
                // Return items with the specified version
                entity = versions.FirstOrDefault(x => x.ComputedVersion == metadataTreeQueryParameters.Version);
            }

            if (entity is null)
            {
                return null;
            }

            var provenance = metadataTreeQueryParameters.IncludeProvenance ? CreateProvenanceObject(entity, metadataCatalogModel) : null;

            var applicationProfileGraph = await _applicationProfileUtility.GetApplicationProfile(resourceId);
            var graph = await _rdfRepositoryBase.GetGraph(entity.Uri);
            var hasChildren = metadataTreeQueryParameters.Path.EndsWith('/');

            FixDatatypes(graph, applicationProfileGraph);

            MetadataTreeExtracted? extractedTree = null;
            if (metadataTreeQueryParameters.IncludeExtractedMetadata)
            {
                extractedTree = await GetExtractedTree(entity, metadataTreeQueryParameters.Path, metadataTreeQueryParameters.Format, hasChildren);
            }

            return new MetadataTree
            {
                Id = entity.Uri,
                Path = metadataTreeQueryParameters.Path,
                Version = entity.ComputedVersion.ToString(),
                AvailableVersions = versions.Select(v => v.ComputedVersion.ToString()),
                Definition = _rdfRepositoryBase.WriteGraph(graph, metadataTreeQueryParameters.Format),
                Format = metadataTreeQueryParameters.Format,
                HasChildren = hasChildren,
                Extracted = extractedTree,
                Provenance = provenance
            };
        }
        catch (Exception e)
        {
            _logger.LogWarning(e, "Exception while loading from the quad store");
            return null;
        }
    }

    private Provenance? CreateProvenanceObject(MetadataEntity entity, Model metadataCatalogModel)
    {
        HashParameters? hashParameters = null;
        if (entity.RawDataId is not null)
        {
            var dataCatalogId = new Uri(
                $"{entity.RawDataId.ToString()[..entity.RawDataId.ToString().IndexOf("&version=")]}"
            );
            var dataCatalogModel = new Model(_store, new UriRef(dataCatalogId.AbsoluteUri));

            var rawDataEntity = dataCatalogModel.GetResource<RawDataEntity>(entity.RawDataId);
            if (rawDataEntity.Hash is not null)
            {
                var hashEntity = dataCatalogModel.GetResource<HashEntity>(rawDataEntity.Hash);
                hashParameters = new HashParameters()
                {
                    AlgorithmName = hashEntity.HashFunction != null
                        ? new HashAlgorithmName(hashEntity.HashFunction)
                        : HashAlgorithmName.SHA512,
                    Value = hashEntity.HashValue ?? "",
                };
            }
        }

        return new Provenance()
        {
            Id = entity.Uri,
            GeneratedAt = entity.GeneratedAt,
            HashParameters = hashParameters,
            Variants = entity.HasVariant
                    .Select((variant) => metadataCatalogModel.GetResource<Variant>(variant))
                    // Filter out values with null => Therefore disable next line
                    .Where((variant) => variant.VariantOf is not null && variant.Similarity is not null)
                    .Select((variant) => new Provenance.Variant(variant.VariantOf!, double.Parse(variant.Similarity!))) ?? [],
            WasInvalidatedBy = entity.InvalidatedBy,
            WasRevisionOf = entity.WasRevisionOf,
            SimilarityToLastVersion = decimal.Parse(entity.SimilarityToLastVersion ?? "0")
        };
    }

    private async Task<MetadataTreeExtracted?> GetExtractedTree(MetadataEntity? entity, string path, RdfFormat format, bool hasChildren)
    {
        MetadataTreeExtracted? extractedTree = null;

        // Get the extracted graph, if desired
        if (entity is not null)
        {
            // TODO: Create a back reference as a property in MetadataEntity & RawDataEntity
            var metadataCatalogId = new Uri(
                $"{entity.Uri.ToString()[..entity.Uri.ToString().IndexOf("&version=")]}"
            );
            var metadataCatalogModel = new Model(_store, new UriRef(metadataCatalogId.AbsoluteUri));

            // Get the metadata catalog with the most recent extracted metadata
            var metadataEntityList = metadataCatalogModel
                .AsQueryable<MetadataEntity>()
                .OrderByDescending(x => x.Uri)
                .ToList();
            var metadataCatalog = metadataEntityList
                .Where(x => x.ComputedVersion <= entity.ComputedVersion && x.ExtractedId is not null)
                .FirstOrDefault();

            if (metadataCatalog is not null && metadataCatalog.ExtractedId is not null)
            {
                var metadataExtractedGraph = await _rdfRepositoryBase.GetGraph(metadataCatalog.ExtractedId);

                if (!metadataExtractedGraph.IsEmpty)
                {
                    // Don't fix the data types of the extracted graph, as they are disjointed from the app profile.

                    RawDataEntity? rawDataEntity = null;
                    if (metadataCatalog.RawDataId is not null)
                    {
                        var dataCatalogId = new Uri(
                            $"{metadataCatalog.RawDataId.ToString()[..metadataCatalog.RawDataId.ToString().IndexOf("&version=")]}"
                        );
                        var dataCatalogModel = new Model(_store, new UriRef(dataCatalogId.AbsoluteUri));

                        rawDataEntity = dataCatalogModel.GetResource<RawDataEntity>(metadataCatalog.RawDataId);

                        if (rawDataEntity is not null && rawDataEntity.ExtractedId is not null)
                        {
                            var rawDataExtractedGraph = await _rdfRepositoryBase.GetGraph(rawDataEntity.ExtractedId);
                            metadataExtractedGraph.Assert(rawDataExtractedGraph.Triples);
                        }
                    }

                    extractedTree = new MetadataTreeExtracted
                    {
                        MetadataId = metadataCatalog.ExtractedId,
                        RawDataId = rawDataEntity?.ExtractedId,
                        Path = path,
                        Definition = _rdfRepositoryBase.WriteGraph(metadataExtractedGraph, format),
                        Format = format,
                        HasChildren = hasChildren,
                    };
                }
            }
        }

        return extractedTree;
    }

    /// <summary>
    /// This method was necessary to fix the datatypes from Virtuoso (e.g., an "4"^^xsd:decimal gets truncated to "4"^^xsd:integer)
    /// </summary>
    /// <hint>
    /// Relevant issue: https://git.rwth-aachen.de/coscine/issues/-/issues/2381
    /// </hint>
    /// <param name="graph">Provided Metadata Graph</param>
    /// <param name="applicationProfileGraph">Application Profile Graph</param>
    private static void FixDatatypes(IGraph graph, IGraph applicationProfileGraph)
    {
        var propertyTriples = applicationProfileGraph.GetTriplesWithPredicate(
            applicationProfileGraph.CreateUriNode(RdfUris.ShaclProperty)
        );
        var properties = propertyTriples.Select(
            (propertyTriple) => applicationProfileGraph.GetTriplesWithSubject(propertyTriple.Object)
        );
        foreach (var property in properties)
        {
            var shPath = property.FirstOrDefault(
                (triple) => (triple.Predicate as UriNode)?.Uri.AbsoluteUri == RdfUris.ShaclPath.AbsoluteUri
            );
            if (shPath is not null)
            {
                var values = graph.GetTriplesWithPredicate(shPath.Object).ToList();
                var shDatatype = property.FirstOrDefault(
                    (triple) => (triple.Predicate as UriNode)?.Uri.AbsoluteUri == RdfUris.ShaclDatatype.AbsoluteUri
                );
                if (shDatatype is not null)
                {
                    foreach (var value in values)
                    {
                        graph.Retract(value);
                        var fullValue = (value.Object as LiteralNode)?.Value;
                        if (
                            (shDatatype.Object as UriNode)?.Uri.AbsoluteUri == RdfUris.XsdDuration.AbsoluteUri
                            && double.TryParse(fullValue, out double fullDoubleValue)
                        )
                        {
                            var duration = new XsdDuration(TimeSpan.FromSeconds(fullDoubleValue / 10));
                            fullValue = duration.ToString();
                        }
                        graph.Assert(
                            value.Subject,
                            value.Predicate,
                            graph.CreateLiteralNode(fullValue, (shDatatype.Object as UriNode)?.Uri)
                        );
                    }
                }
            }
        }
    }

    /// <inheritdoc/>
    public async Task<PagedEnumerable<DataTree>> GetNewestPagedDataAsync(Guid resourceId, DataTreeParameters dataTreeParameters)
    {
        var resourceGraphPrefix = $"{RdfUris.CoscineResources}{resourceId}";
        var pathGraphUri = new Uri(
            $"{resourceGraphPrefix}/{(dataTreeParameters.Path?.StartsWith('/') == true ? dataTreeParameters.Path[1..] : dataTreeParameters.Path)}"
        );

        var query = new SparqlParameterizedString
        {
            CommandText = """

                SELECT (?version AS ?currentVersion) (GROUP_CONCAT(DISTINCT ?version2; SEPARATOR=";") AS ?versions) (GROUP_CONCAT(DISTINCT ?child ; SEPARATOR=";") AS ?children)
                {
                    GRAPH @resourceId {
                        @resourceId @catalog ?g .
                    }

                    GRAPH ?g{
                        ?g a @metadataService .
                        ?g @catalog ?m .
                    }

                    OPTIONAL {
                        ?g @catalog ?child .
                        ?child  a @metadataService.
                    }

                    GRAPH ?m {
                        ?m @dataset ?version .
                        ?m @dataset ?version2 .
                        FILTER NOT EXISTS {
                            ?s @revisionOf ?version .
                        }
                    }

                    FILTER(CONTAINS(STR(?m), @path)) .

                    OPTIONAL {
                        ?version @invalidatedBy ?invalidator
                    }

                    OPTIONAL {
                        ?parentVersion @revisionOf* ?version .
                        ?parentVersion @invalidatedBy ?parentInvalidator
                    }

                    FILTER (!BOUND(?invalidator) && !BOUND(?parentInvalidator)) .
                }
                GROUP BY ?version

"""
        };

        query.SetUri("resourceId", new Uri(resourceGraphPrefix));
        query.SetUri("catalog", RdfUris.DcatCatalog);
        query.SetUri("metadataService", RdfUris.FdpMetadataServiceClass);
        query.SetUri("dataset", RdfUris.DcatDataset);
        query.SetUri("revisionOf", RdfUris.ProvWasRevisionOf);
        query.SetUri("invalidatedBy", RdfUris.ProvWasInvalidatedBy);
        query.SetLiteral("path", pathGraphUri.AbsoluteUri, normalizeValue: true);

        var count = await query.GetCountOrDefaultAsync(_rdfRepositoryBase);

        query.Sort<DataTree>(dataTreeParameters.OrderBy, new() { { "version", ap => ap.Version }, });

        query.Paginate(dataTreeParameters.PageSize, dataTreeParameters.PageNumber);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        var data = new List<DataTree>();
        foreach (var result in resultSet)
        {
            // Parse SPARQL result set to a strongly typed object
            var dataTreeId = new Uri(result.Value("currentVersion").ToString());
            var graph = await _rdfRepositoryBase.GetGraph(dataTreeId);

            var idUriData = new TreeUriResolver(dataTreeId);
            var versionsUriData = result
                .Value("versions")
                .AsValuedNode()
                .AsString()
                .Split(";", StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries)
                .Select(uriString => new TreeUriResolver(new Uri(uriString)));

            // TODO: currently there is a bug in the old metadata generation.
            // As such, loading of children doesn't work and folders have no metadata.
            // This has to be fixed, once it is fixed in the old API.
            var hasChildren =
                result
                    .Value("children")
                    .AsValuedNode()
                    .AsString()
                    .Trim()
                    .Split(";", StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries)
                    .Length > 0;

            // Add trailing forward slash '/', if metadata tree has children
            var path = hasChildren ? $"{idUriData.Path}/" : idUriData.Path;

            data.Add(
                new DataTree
                {
                    Id = dataTreeId,
                    Path = path,
                    Version = idUriData.Version.ToString(),
                    AvailableVersions = versionsUriData.Select(v => v.Version.ToString()),
                    HasChildren = hasChildren,
                }
            );
        }

        return new PagedEnumerable<DataTree>(data, count, dataTreeParameters.PageNumber, dataTreeParameters.PageSize);
    }

    /// <inheritdoc/>
    /// <exception cref="TreeAlreadyExistsConflictException">
    /// Thrown when a graph with the specified URI exists in Trellis.
    /// </exception>
    public async Task<IEnumerable<IGraph>> CreateMetadataGraphAsync(Resource resource, User? user, MetadataTree metadataTree)
    {
        // Generate new tree ID (includes type and new version information) and parse the entry metadata into a graph
        var metadataId = GetOrCreateMetadataId(resource.Id, metadataTree.Path, existingId: metadataTree.Id);
        var graph = metadataTree.Definition is not null
             ? _rdfRepositoryBase.ParseGraph(metadataTree.Definition, metadataTree.Format, metadataId)
             : new Graph(metadataId);
        graph.BaseUri = metadataId;

        // Parse and resolve components of the base URI of the input graph
        var graphUriData = new TreeUriResolver(graph.BaseUri);

        // Check if the graph with the specified URI exists in our metadata store and is not invalidated
        var existingMetadataTrees = PaginationHelper.GetAllAsync(
            (currentPage) =>
                GetNewestPagedMetadataAsync(graphUriData.ResourceId, new() { Path = graphUriData.Path, PageNumber = currentPage })
        );
        if (await existingMetadataTrees.AnyAsync(mt => mt.Path == graphUriData.Path))
        {
            throw new TreeAlreadyExistsConflictException(graphUriData.GraphId);
        }

        // Handle resource fixed values
        ProcessFixedValues(resource, user, graph);

        if (!metadataTree.SkipValidation)
        {
            await ProcessRDFValidationAsync(graph, resource);
        }

        ParseExtractedGraphs(metadataTree, out IGraph? extractedDataGraph, out IGraph? extractedMetadataGraph);

        var graphs = await ProcessGraphsOnCreateAsync(graph, graphUriData, extractedDataGraph, extractedMetadataGraph, metadataTree.Provenance);

        return graphs.Concat([graph]);
    }

    /// <inheritdoc/>
    public Uri GetOrCreateMetadataId(Guid resourceId, string path, GraphType graphType = GraphType.Metadata, long? version = null, Uri? existingId = null)
    {
        if (existingId is not null)
        {
            return new AlwaysAbsoluteUri(existingId.AbsoluteUri); // Use 'AlwaysAbsoluteUri' to handle path encoding
        }
        version ??= MetadataVersionUtility.GetNewVersion();
        var newId = $"{RdfUris.CoscineResources}{resourceId}/{path}/@type={graphType.GetEnumMemberValue()}&version={version}";
        return new AlwaysAbsoluteUri(newId); // Use 'AlwaysAbsoluteUri' to handle path encoding
    }


    /// <inheritdoc/>
    /// <exception cref="TreeNotFoundException">
    /// Thrown when no graph with the specified URI exists in Trellis.
    /// </exception>
    public async Task<IEnumerable<IGraph>> UpdateMetadataGraphAsync(Resource resource, User? user, MetadataTree metadataTree)
    {
        var createNewMetadataGraph = metadataTree.Definition is not null || metadataTree.ForceNewMetadataVersion;

        var metadataId = GetOrCreateMetadataId(resource.Id, metadataTree.Path, existingId: metadataTree.Id);

        var graph = metadataTree.Definition is not null
             ? _rdfRepositoryBase.ParseGraph(metadataTree.Definition, metadataTree.Format, metadataId)
             : new Graph(metadataId);

        // Generate new tree ID (includes type and new version information)
        graph.BaseUri = metadataId;

        // Parse and resolve components of the base URI of the input graph
        var graphUriData = new TreeUriResolver(graph.BaseUri);

        // Check if the graph with the specified URI exists in our metadata store and is not invalidated
        var existingMetadataTrees = PaginationHelper.GetAllAsync(
            (currentPage) =>
                GetNewestPagedMetadataAsync(graphUriData.ResourceId, new() { Path = graphUriData.Path, PageNumber = currentPage })
        );
        // Order by descending version to ensure that the first element is the newest version
        var relevantMetadataTree =
            await existingMetadataTrees
                .OrderByDescending(mt => int.Parse(mt.Version))
                .FirstOrDefaultAsync(mt => mt.Path == graphUriData.Path)
            ?? throw new TreeNotFoundException(graphUriData.GraphId);

        // When no new metadata is provided, take the old metadata
        if (metadataTree.Definition is null)
        {
            graph = relevantMetadataTree.Definition is not null
              ? _rdfRepositoryBase.ParseGraph(relevantMetadataTree.Definition, relevantMetadataTree.Format, metadataId)
              : new Graph(metadataId);

            graph.BaseUri = metadataId;
        }

        // Handle resource fixed values
        ProcessFixedValues(resource, user, graph);

        if (!metadataTree.SkipValidation)
        {
            await ProcessRDFValidationAsync(graph, resource);
        }

        ParseExtractedGraphs(metadataTree, out IGraph? extractedDataGraph, out IGraph? extractedMetadataGraph);

        if (metadataTree.Extracted is not null)
        {
            createNewMetadataGraph = ApplyTemplateValues(graph, extractedDataGraph, extractedMetadataGraph) || createNewMetadataGraph;
        }

        var provenanceGraphs = await ProcessGraphsOnUpdateAsync(
            graph,
            relevantMetadataTree,
            // Create new data graph if a new metadata graph is also created (forced)
            createNewMetadataGraph && metadataTree.ForceNewMetadataVersion,
            createNewMetadataGraph,
            extractedDataGraph,
            extractedMetadataGraph,
            metadataTree.Provenance
        );

        return [graph, .. provenanceGraphs];
    }

    /// <summary>
    /// Applies templating to the metadata graph from the extracted graphs.
    /// </summary>
    /// <param name="graph">Current Metadata Graph.</param>
    /// <param name="extractedDataGraph">Extracted Data Graph.</param>
    /// <param name="extractedMetadataGraph">Extracted Metadata Graph.</param>
    /// <returns>True if a templating value has been set.</returns>
    private bool ApplyTemplateValues(IGraph graph, IGraph? extractedDataGraph, IGraph? extractedMetadataGraph)
    {
        var valueChanged = false;
        // Usage of ToArray since graph is being mutated in the foreach loop
        foreach (var triple in graph.Triples.ToArray())
        {
            var objectValue = triple.Object as ILiteralNode;
            if (objectValue is null)
            {
                continue;
            }
            if (!ApplicationProfileUtility.TemplateRegex().IsMatch(objectValue.Value))
            {
                continue;
            }

            var matches = ApplicationProfileUtility.TemplateRegex().Matches(objectValue.Value);
            var adapted = false;
            var replacedString = objectValue.Value;
            foreach (Match match in matches)
            {
                var matchingValue = match.Value;
                // Use the regex group inside the {{ }} brackets
                var filter = match.Groups[1].Value;
                if (Uri.TryCreate(filter, UriKind.RelativeOrAbsolute, out Uri? filterUrl))
                {
                    var foundValues = new List<ILiteralNode>();

                    // Search for the filter in the extracted data graph
                    var foundInData = extractedDataGraph?
                        .GetTriplesWithPredicate(extractedDataGraph.CreateUriNode(filterUrl))
                        .Select(x => x.Object as ILiteralNode)
                        .Where(x => x is not null);
                    if (foundInData is not null)
                    {
                        foundValues = [.. foundInData];
                    }

                    // Search for the filter in the extracted metadata graph
                    var foundInMetadata = extractedMetadataGraph?
                        .GetTriplesWithPredicate(extractedMetadataGraph.CreateUriNode(filterUrl))
                        .Select(x => x.Object as ILiteralNode)
                        .Where(x => x is not null);
                    if (foundInMetadata is not null)
                    {
                        foundValues = [.. foundValues, .. foundInMetadata];
                    }

                    // If found, replace the filter with the first found value (rest are ignored for now)
                    if (foundValues.Count > 0)
                    {
                        adapted = true;
                        replacedString = replacedString.Replace(matchingValue, foundValues.First().Value);
                    }
                }
            }

            // If templating can be applied, remove the current triple and add the new one
            if (adapted)
            {
                valueChanged = true;
                graph.Retract(triple);
                graph.Assert(triple.Subject, triple.Predicate, graph.CreateLiteralNode(replacedString));
            }
        }
        return valueChanged;
    }

    private void ParseExtractedGraphs(MetadataTree metadataTree, out IGraph? extractedDataGraph, out IGraph? extractedMetadataGraph)
    {
        extractedDataGraph = null;
        extractedMetadataGraph = null;
        if (metadataTree.Extracted is not null)
        {
            var extractedDataId = metadataTree.Extracted.RawDataId?.AbsoluteUri + "&extracted=true";
            var extractedMetadataId = metadataTree.Extracted.MetadataId?.AbsoluteUri + "&extracted=true";

            var tripleStore = _rdfRepositoryBase.ParseTripleStore(metadataTree.Extracted.Definition, metadataTree.Extracted.Format);

            extractedDataGraph = tripleStore.Graphs.FirstOrDefault(
                x => (x.BaseUri?.AbsoluteUri ?? (x.Name as IUriNode)?.Uri.AbsoluteUri) == extractedDataId);
            if (extractedDataGraph is not null)
            {
                extractedDataGraph.BaseUri = new AlwaysAbsoluteUri(extractedDataId);
            }
            extractedMetadataGraph = tripleStore.Graphs.FirstOrDefault(
                x => (x.BaseUri?.AbsoluteUri ?? (x.Name as IUriNode)?.Uri.AbsoluteUri) == extractedMetadataId);
            if (extractedMetadataGraph is not null)
            {
                extractedMetadataGraph.BaseUri = new AlwaysAbsoluteUri(extractedMetadataId);
            }
        }
    }

    /// <summary>
    /// Processes and validates the RDF graph against the provided application profile.
    /// This method includes sanity checks for the application profile, handles resource fixed values,
    /// and validates the graph using SHACL.
    /// </summary>
    /// <param name="graph">The graph object representing the RDF graph to be validated.</param>
    /// <param name="resource">The resource entity associated with the RDF graph.</param>
    /// <returns>A Task representing the asynchronous validation operation.</returns>
    /// <exception cref="ApplicationProfilepublicServerErrorException">
    /// Thrown when the application profile URI in the resource entity is not valid.
    /// </exception>
    /// <exception cref="ApplicationProfileValidationBadRequestException">
    /// Thrown when the SHACL validation of the graph against the ApplicationProfile fails.
    /// </exception>
    private async Task ProcessRDFValidationAsync(IGraph graph, Resource resource)
    {
        // Sanity check application profile for a resource
        if (!Uri.TryCreate(resource.ApplicationProfile, UriKind.Absolute, out var applicationProfileUri))
        {
            throw new ApplicationProfileInternalServerErrorException(resource.Id);
        }

        var shapesGraph = await _applicationProfileUtility.GetFullApplicationProfile(applicationProfileUri);

        var templatingEnabled = resource.MetadataExtractions.Any(x => x.Activated);
        if (templatingEnabled)
        {
            _applicationProfileUtility.DeactivateSpecificRules(graph, shapesGraph);
        }

        // Validate SHACL
        var isValid = await _applicationProfileUtility.ValidateApplicationProfile(graph, shapesGraph);
        if (!isValid)
        {
            throw new ApplicationProfileValidationBadRequestException(resource.ApplicationProfile);
        }
    }

    /// <summary>
    /// Processes the fixed values of a given resource, modifying the provided graph accordingly.
    /// </summary>
    /// <param name="resource">The resource entity containing the fixed values and application profile URI.</param>
    /// <param name="user">The user entity whose information may be used in the graph, depending on fixed value tokens.</param>
    /// <param name="graph">The RDF graph to which the processed fixed values will be applied.</param>
    private void ProcessFixedValues(Resource resource, User? user, IGraph graph)
    {
        if (resource.FixedValues is null)
        {
            return;
        }

        var fixedValuesDict = new Dictionary<string, Dictionary<string, List<FixedValueForResourceManipulationDto>>>();
        try
        {
            fixedValuesDict = JsonSerializer.Deserialize<
                Dictionary<string, Dictionary<string, List<FixedValueForResourceManipulationDto>>>
            >(resource.FixedValues, new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
        }
        catch (Exception e)
        {
            _logger.LogWarning(e, "Error while processing fixed values for resource {resourceId}.", resource.Id);
            return;
        }

        if (fixedValuesDict is null)
        {
            return;
        }

        var fixedValueIdentifier = RdfUris.CoscineFixedValue.ToString();

        // Handle the fixed values
        foreach (var fixedValueEntries in fixedValuesDict)
        {
            if (!fixedValueEntries.Value.ContainsKey(fixedValueIdentifier))
            {
                continue;
            }
            foreach (var fixedValueEntry in fixedValueEntries.Value[fixedValueIdentifier])
            {
                var targetClass = fixedValueEntry.TargetClass;
                var identifier = fixedValueEntries.Key;
                var type = fixedValueEntry.Type;

                if (type == "bnode")
                {
                    continue;
                }

                var relevantSubjects = graph
                    .Triples.Where(
                        (apt) => apt.Predicate.ToString() == RdfUris.A.ToString() && apt.Object.ToString() == targetClass
                    )
                    .Select(x => x.Subject.ToString());

                // Keep the ToList to avoid issues with the retract step
                var existingGraphTriples = graph
                    .Triples.Where(
                        (apt) => relevantSubjects.Contains(apt.Subject.ToString()) && apt.Predicate.ToString() == identifier
                    )
                    .ToList();

                // Remove any existing triples in the graph that match the current subject and object
                foreach (var existingGraphTriple in existingGraphTriples)
                {
                    // Represents an individual triple in the main graph that has the same subject and object
                    // as those specified by 'matchingProfileTriple' from the application profile graph.
                    // This loop is iterating over these triples and removing them from the main graph to avoid conflicts.
                    graph.Retract(existingGraphTriple);
                }
            }

            foreach (var fixedValueEntry in fixedValueEntries.Value[fixedValueIdentifier])
            {
                var targetClass = fixedValueEntry.TargetClass;
                var identifier = fixedValueEntries.Key;
                var fixedValue = fixedValueEntry.Value;
                var dataType = fixedValueEntry.DataType;
                var type = fixedValueEntry.Type;

                INode tripleObject;
                if (type == "bnode" || fixedValue is null)
                {
                    continue;
                }
                else if (type == "literal")
                {
                    if (dataType is not null)
                    {
                        tripleObject = graph.CreateLiteralNode(fixedValue, dataType);
                    }
                    else
                    {
                        tripleObject = graph.CreateLiteralNode(fixedValue);
                    }
                }
                else if (type == "uri")
                {
                    tripleObject = graph.CreateUriNode(new Uri(fixedValue));
                }
                else
                {
                    continue;
                }

                // Check for special literal values and modify them as necessary
                if (tripleObject is ILiteralNode)
                {
                    // Replace {ME} with the user's full name
                    var tripleObjectString = (tripleObject as ILiteralNode)?.Value;
                    if (tripleObjectString?.Equals("{ME}") == true && user is not null)
                    {
                        tripleObjectString = tripleObjectString.Replace("{ME}", $"{user.Givenname} {user.Surname}");
                        tripleObject = graph.CreateLiteralNode(
                            tripleObjectString,
                            new Uri(XmlSpecsHelper.XmlSchemaDataTypeString)
                        );
                    }
                    // Replace {TODAY} with the current date
                    else if (tripleObjectString?.Equals("{TODAY}") == true)
                    {
                        tripleObjectString = tripleObjectString.Replace("{TODAY}", DateTime.Today.ToString("yyyy-MM-dd"));
                        tripleObject = graph.CreateLiteralNode(tripleObjectString, new Uri(XmlSpecsHelper.XmlSchemaDataTypeDate));
                    }
                }

                // Keep the ToList to avoid issues with the assert step
                var relevantSubjects = graph
                    .Triples.Where(
                        (apt) => apt.Predicate.ToString() == RdfUris.A.ToString() && apt.Object.ToString() == targetClass
                    )
                    .Select(x => x.Subject)
                    .ToList();

                foreach (var relevantSubject in relevantSubjects)
                {
                    // Add the new triple to the graph
                    graph.Assert(relevantSubject, graph.CreateUriNode(new Uri(identifier)), tripleObject);
                }
            }
        }
    }

    /// <summary>
    /// Processes and manages the creation of new graphs in the RDF repository.
    /// </summary>
    /// <param name="graph">The source RDF graph.</param>
    /// <param name="graphUriData">The resolved tree graph URI components.</param>
    /// <returns>A Task representing the asynchronous operation of creating and storing the graph.</returns>
    private async Task<IEnumerable<IGraph>> ProcessGraphsOnCreateAsync(
        IGraph graph,
        TreeUriResolver graphUriData,
        IGraph? extractedDataGraph,
        IGraph? extractedMetadataGraph,
        Provenance? provenance
    )
    {
        // There will be no existing valid graphs, create them
        var trellisGraph = PatchGraph.Empty(RdfUris.TrellisGraph);
        var graphs = await _metadataUtility.CreateGraphsAsync(
            graphUriData.ResourceId,
            graphUriData.Path,
            new ProvidedGraphData
            {
                TrellisGraph = trellisGraph,
                NewVersion = graphUriData.Version,
            }
        );

        foreach (var newGraph in graphs)
        {
            // Add the new graphs to the RDF repository
            await _rdfRepositoryBase.AddGraphAsync(newGraph);
        }

        // Add Provenance information, which includes the version or variant of the new data in relation to existing data
        await _provenanceRepository.AddProvenanceInformation(
            graph.BaseUri,
            createNewDataGraph: true,
            createNewMetadataGraph: true,
            existingMetadataGraphUri: null,
            extractedDataGraph,
            extractedMetadataGraph,
            provenance
        );

        // Add the input graph to the RDF repository
        await _rdfRepositoryBase.AddGraphAsync(graph);

        if (extractedDataGraph is not null)
        {
            await _rdfRepositoryBase.AddGraphAsync(extractedDataGraph);
        }
        if (extractedMetadataGraph is not null)
        {
            await _rdfRepositoryBase.AddGraphAsync(extractedMetadataGraph);
        }

        return graphs;
    }

    /// <summary>
    /// Processes and manages the updating of existing graphs in the RDF repository.
    /// </summary>
    /// <param name="graph">The source RDF graph.</param>
    /// <param name="existingMetadataTree">An existing metadata trees for the graph.</param>
    /// <param name="createNewDataGraph">If a new data graph should be created.</param>
    /// <param name="createNewMetadataGraph">If a new metadata graph should be created.</param>
    /// <param name="extractedDataGraph">The extracted data graph.</param>
    /// <param name="extractedMetadataGraph">The extracted metadata graph.</param>
    /// <param name="provenance">The provenance information.</param>
    /// <returns>A Task representing the asynchronous operation of updating and storing the graph.</returns>
    private async Task<IEnumerable<IGraph>> ProcessGraphsOnUpdateAsync(
        IGraph graph,
        MetadataTree existingMetadataTree,
        bool createNewDataGraph,
        bool createNewMetadataGraph,
        IGraph? extractedDataGraph,
        IGraph? extractedMetadataGraph,
        Provenance? provenance
    )
    {
        // Add Provenance information, which includes the version or variant of the new data in relation to existing data
        var provenanceGraphs = await _provenanceRepository.AddProvenanceInformation(
            graph.BaseUri,
            createNewDataGraph: createNewDataGraph,
            createNewMetadataGraph: createNewMetadataGraph,
            existingMetadataTree.Id,
            extractedDataGraph,
            extractedMetadataGraph,
            provenance
        );

        if (createNewMetadataGraph)
        {
            // Add the input graph to the RDF repository
            await _rdfRepositoryBase.AddGraphAsync(graph);
        }

        if (extractedDataGraph is not null)
        {
            await _rdfRepositoryBase.AddGraphAsync(extractedDataGraph);
        }
        if (extractedMetadataGraph is not null)
        {
            await _rdfRepositoryBase.AddGraphAsync(extractedMetadataGraph);
        }

        return provenanceGraphs;
    }

    /// <inheritdoc/>
    public async Task<IGraph> InvalidateMetadataGraphAsync(Guid resourceId, string path, Uri graphId, User invalidatedByUser)
    {
        var userGraphId = new Uri($"{RdfUris.CoscineUsers}{invalidatedByUser.Id}");
        return await _provenanceRepository.CreateInvalidation(resourceId, path, graphId, "metadata", userGraphId);
    }

    /// <inheritdoc/>
    public async Task<IGraph> InvalidateDataGraphAsync(Guid resourceId, string path, Uri graphId, User invalidatedByUser)
    {
        var userGraphId = new Uri($"{RdfUris.CoscineUsers}{invalidatedByUser.Id}");
        return await _provenanceRepository.CreateInvalidation(resourceId, path, graphId, "data", userGraphId);
    }
}