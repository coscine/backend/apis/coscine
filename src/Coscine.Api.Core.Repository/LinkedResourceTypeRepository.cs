using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class LinkedResourceTypeRepository : ILinkedResourceTypeRepository
{
    private readonly IRdfRepositoryBase _rdfRepositoryBase;
    private readonly RepositoryBase<LinkedResourceType> _linkedResourceType;

    public LinkedResourceTypeRepository(RepositoryContext repositoryContext, IRdfRepositoryBase rdfRepositoryBase)
    {
        _rdfRepositoryBase = rdfRepositoryBase;
        _linkedResourceType = new(repositoryContext);
    }

    public void Create(LinkedResourceType linkedResourceType)
    {
        _linkedResourceType.Create(linkedResourceType);
    }

    public async Task<LinkedResourceType?> GetAsync(Guid linkedResourceTypeId, bool trackChanges)
    {
        return await _linkedResourceType.FindByCondition(x => x.Id == linkedResourceTypeId, trackChanges)
            .SingleOrDefaultAsync();
    }
}