﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions.Utility;
using Coscine.Api.Core.Shared;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Text;
using System.Text.Json;
using System.Web;

namespace Coscine.Api.Core.Repository;

public class HandleRepository : IHandleRepository
{
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly ILogger<HandleRepository> _logger;
    private readonly IPidRepository _pidRepository;
    private readonly PidConfiguration _pidConfiguration;
    private readonly ConnectionConfiguration _connectionConfiguration;

    public HandleRepository(
        IHttpClientFactory httpClientFactory,
        IOptionsMonitor<PidConfiguration> pidConfiguration,
        ILogger<HandleRepository> logger,
        IOptionsMonitor<ConnectionConfiguration> connectionConfiguration,
        IPidRepository pidRepository
    )
    {
        _pidConfiguration = pidConfiguration.CurrentValue;
        _connectionConfiguration = connectionConfiguration.CurrentValue;

        // Ensure the configuration keys are set
        ArgumentNullException.ThrowIfNull(connectionConfiguration);
        ArgumentException.ThrowIfNullOrEmpty(_connectionConfiguration.ServiceUrl);
        ArgumentNullException.ThrowIfNull(pidConfiguration);
        ArgumentException.ThrowIfNullOrEmpty(_pidConfiguration.Prefix);
        ArgumentException.ThrowIfNullOrEmpty(_pidConfiguration.Url);
        ArgumentException.ThrowIfNullOrEmpty(_pidConfiguration.User);
        ArgumentException.ThrowIfNullOrEmpty(_pidConfiguration.Password);
        ArgumentException.ThrowIfNullOrEmpty(_pidConfiguration.DigitalObjectLocationUrl);

        _httpClientFactory = httpClientFactory;
        _logger = logger;
        _pidRepository = pidRepository;
    }

    public async Task<Handle?> GetAsync(string prefix, string suffix)
    {
        IEnumerable<HandleValue>? result = null;

        var handleClient = _httpClientFactory.CreateHandleClient(_pidConfiguration);

        // Make the GET request
        var response = await handleClient.GetAsync($"{prefix}/{suffix}");

        if (response.IsSuccessStatusCode)
        {
            // Parse the JSON response to ApiResponse class
            string responseBody = await response.Content.ReadAsStringAsync();
            result = JsonSerializer.Deserialize<IEnumerable<HandleValue>>(responseBody);
        }

        return result is null
            ? null
            : new Handle
            {
                // We just verified the handle, no need to do it again (verifyHandle: false).
                Pid = await _pidRepository.GetAsync(prefix, suffix, verifyHandle: false, trackChanges: false),
                Values = result
            };
    }

    public async Task UpdateAsync(string prefix, string suffix, Handle handle)
    {
        // Remove any trailing or leading slashes to avoid double slashes in the URL
        prefix = prefix.Trim('/');
        suffix = suffix.Trim('/');

        var handleClient = _httpClientFactory.CreateHandleClient(_pidConfiguration);

        // We can not update the following values; set them to null explicitly
        foreach (var handleValue in handle.Values)
        {
            handleValue.Data = null;
            handleValue.Timestamp = null;
            handleValue.TtlType = null;
            handleValue.Ttl = null;
            handleValue.Refs = null;
            handleValue.Privs = null;
        }

        // Make the PUT request
        var requestContent = new StringContent(JsonSerializer.Serialize(handle.Values), Encoding.UTF8, "application/json");
        var response = await handleClient.PutAsync($"{prefix}/{suffix}", requestContent);

        if (!response.IsSuccessStatusCode)
        {
            _logger.LogWarning("Failed to update the handle. Status Code: {statusCode}, Response: {content}", response.StatusCode, response.Content);
            throw new HandleCouldNotBeUpdatedBadRequestException();
        }
    }

    public IEnumerable<HandleValue> GenerateProjectHandleValues(Project project)
    {
        var handles = new List<HandleValue>();

        // Use the configuration to build necessary URLs and prefixes
        var pid = $"{_pidConfiguration.Prefix}/{project.Id}";
        var baseUri = new Uri(_connectionConfiguration.ServiceUrl, UriKind.Absolute);
        var digitalObjectLocationUri = new Uri(_pidConfiguration.DigitalObjectLocationUrl, UriKind.Absolute);
        var isProjectPublic = project.Visibility?.DisplayName.Equals("public", StringComparison.OrdinalIgnoreCase) ?? false;

        var idx = 1;

        // Create the URL handle value
        handles.Add(new()
        {
            Idx = idx++,
            Type = "URL",
            ParsedData = new Uri(baseUri, $"/pid/?pid={HttpUtility.UrlEncode(pid)}")
        });

        // Create the kernel information profile handle value
        handles.Add(new()
        {
            Idx = idx++,
            Type = PidHandles.KernelInformationProfileHandle,
            ParsedData = PidHandles.CoscineKernelInformationProfileHandle
        });

        // Create the date created handle value
        if (project.DateCreated is not null && isProjectPublic)
        {
            handles.Add(new()
            {
                Idx = idx++,
                Type = PidHandles.DateCreatedHandle,
                ParsedData = project.DateCreated
            });
        }

        // Create the digital object location handle value
        handles.Add(new()
        {
            Idx = idx++,
            Type = PidHandles.DigitalObjectLocationHandle,
            ParsedData = new Uri(digitalObjectLocationUri, $"/coscine/api/v2/projects/{project.Id}")
        });

        // Create the digital object type handle value
        handles.Add(new()
        {
            Idx = idx++,
            Type = PidHandles.DigitalObjectTypeHandle,
            ParsedData = PidHandles.DigitalObjectTypeProjectHandle
        });

        // Create the topic (discipline) handle value
        var disciplines = project.ProjectDisciplines.Where(pd => pd.Discipline.DisplayNameEn is not null).ToList();
        if (disciplines.Count != 0 && isProjectPublic)
        {
            handles.Add(new()
            {
                Idx = idx++,
                Type = PidHandles.TopicHandle,
                // NOTE: Consider adding all disciplines as a comma-separated string
                ParsedData = disciplines.First().Discipline.DisplayNameEn ?? string.Empty
            });
        }

        // Create the contact (organizations) handle value
        if (project.ProjectInstitutes.Count != 0 && isProjectPublic)
        {
            handles.Add(new()
            {
                Idx = idx++,
                Type = PidHandles.ContactHandle,
                // NOTE: Consider adding all institutes as a comma-separated string
                ParsedData = project.ProjectInstitutes.First().OrganizationUrl
            });
        }

        return handles;
    }

    public IEnumerable<HandleValue> GenerateResourceHandleValues(Resource resource)
    {
        var handles = new List<HandleValue>();

        // Use the configuration to build necessary URLs and prefixes
        var pid = $"{_pidConfiguration.Prefix}/{resource.Id}";
        var baseUri = new Uri(_connectionConfiguration.ServiceUrl, UriKind.Absolute);
        var digitalObjectLocationUri = new Uri(_pidConfiguration.DigitalObjectLocationUrl, UriKind.Absolute);
        var isResourcePublic = resource.Visibility?.DisplayName.Equals("public", StringComparison.OrdinalIgnoreCase) ?? false;

        var idx = 1;

        // Create the URL handle value
        handles.Add(new()
        {
            Idx = idx++,
            Type = "URL",
            ParsedData = new Uri(baseUri, $"/pid/?pid={HttpUtility.UrlEncode(pid)}")
        });

        // Create the kernel information profile handle value
        handles.Add(new()
        {
            Idx = idx++,
            Type = PidHandles.KernelInformationProfileHandle,
            ParsedData = PidHandles.CoscineKernelInformationProfileHandle
        });

        // Create the date created handle value
        if (resource.DateCreated is not null && isResourcePublic)
        {
            handles.Add(new()
            {
                Idx = idx++,
                Type = PidHandles.DateCreatedHandle,
                ParsedData = resource.DateCreated
            });
        }

        // Create the digital object location handle value
        handles.Add(new()
        {
            Idx = idx++,
            Type = PidHandles.DigitalObjectLocationHandle,
            ParsedData = new Uri(digitalObjectLocationUri, $"/coscine/api/v2/resources/{resource.Id}")
        });

        // Create the digital object type handle value
        handles.Add(new()
        {
            Idx = idx++,
            Type = PidHandles.DigitalObjectTypeHandle,
            ParsedData = PidHandles.DigitalObjectTypeResourceHandle
        });

        // Create the topic (discipline) handle value
        var disciplines = resource.ResourceDisciplines.Where(rd => rd.Discipline.DisplayNameEn is not null).ToList();
        if (disciplines.Count != 0 && isResourcePublic)
        {
            handles.Add(new()
            {
                Idx = idx++,
                Type = PidHandles.TopicHandle,
                // NOTE: Consider adding all disciplines as a comma-separated string
                ParsedData = disciplines.First().Discipline.DisplayNameEn ?? string.Empty
            });
        }

        // Create the license handle value
        if (resource.License?.Url is not null && isResourcePublic)
        {
            handles.Add(new()
            {
                Idx = idx++,
                Type = PidHandles.LicenseHandle,
                ParsedData = resource.License.Url.AbsoluteUri
            });
        }

        return handles;
    }
}