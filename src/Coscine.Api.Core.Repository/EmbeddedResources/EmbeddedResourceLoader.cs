﻿using System.Reflection;

namespace Coscine.Api.Core.Repository.EmbeddedResources;

/// <summary>
/// Provides methods to load embedded resources.
/// </summary>
public static class EmbeddedResourceLoader
{
    /// <summary>
    /// Reads the content of an embedded resource.
    /// </summary>
    /// <param name="name">The name (or suffix) of the embedded resource.</param>
    /// <returns>The content of the embedded resource.</returns>
    /// <exception cref="NullReferenceException">Thrown if the executing assembly or the ManifestResourceStream is null.</exception>
    public static string ReadResource(string name)
    {
        // Determine path
        var assembly = Assembly.GetExecutingAssembly() ?? throw new NullReferenceException("The executing assembly is null!");

        string resourcePath = name;
        // Format: "{Namespace}.{Folder}.{filename}.{Extension}"
        if (!name.StartsWith(nameof(EmbeddedResources)))
        {
            resourcePath = assembly.GetManifestResourceNames()
                .Single(str => str.EndsWith(name));
        }

        using var stream = assembly.GetManifestResourceStream(resourcePath);
        using StreamReader reader = new(stream ?? throw new NullReferenceException("The ManifestResourceStream is null!"));
        return reader.ReadToEnd();
    }

    /// <summary>
    /// Asynchronously reads the content of an embedded resource.
    /// </summary>
    /// <param name="name">The name (or suffix) of the embedded resource.</param>
    /// <returns>A task representing the asynchronous operation. The value of the TResult parameter contains the content of the embedded resource.</returns>
    /// <exception cref="NullReferenceException">Thrown if the executing assembly or the ManifestResourceStream is null.</exception>
    public static async Task<string> ReadResourceAsync(string name)
    {
        // Determine path
        var assembly = Assembly.GetExecutingAssembly() ?? throw new NullReferenceException("The executing assembly is null!");

        string resourcePath = name;
        // Format: "{Namespace}.{Folder}.{filename}.{Extension}"
        if (!name.StartsWith(nameof(EmbeddedResources)))
        {
            resourcePath = assembly.GetManifestResourceNames()
                .Single(str => str.EndsWith(name));
        }

        using var stream = assembly.GetManifestResourceStream(resourcePath);
        using StreamReader reader = new(stream ?? throw new NullReferenceException("The ManifestResourceStream is null!"));
        return await reader.ReadToEndAsync();
    }
}