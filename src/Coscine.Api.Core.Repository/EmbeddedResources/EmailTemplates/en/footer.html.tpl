﻿                                        <p style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;"><a href="https://coscine.rwth-aachen.de" class="btn btn-primary" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; text-decoration: none; padding: 10px 15px; display: inline-block; border-radius: 5px; background: #00549F; color: #ffffff;">Log In</a></p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr><!-- end tr -->
                <!-- 1 Column Text + Button : END -->
            </table>
            <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; border-collapse: collapse; table-layout: fixed; margin: 0 auto;">
                <tr style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
                    <td valign="middle" class="bg_light footer email-section" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; background: #fafafa; padding: 2.5em; border-top: 1px solid rgba(0,0,0,.05); color: rgba(0,0,0,.5); mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                        <table style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; border-collapse: collapse; table-layout: fixed; margin: 0 auto;">
                            <tr style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
                                <td valign="top" width="50%" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 20px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; border-collapse: collapse; table-layout: fixed; margin: 0 auto;">
                                        <tr style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
                                            <td style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; text-align: left; padding-right: 10px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" align="left">
                                                <h3 class="heading" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: &#39;Lato&#39;, sans-serif; margin-top: 0; font-weight: 400; color: #000; font-size: 20px;">About</h3>
                                                <p style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">Coscine is a research data platform hosted at RWTH Aachen University.</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" width="50%" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; padding-top: 20px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;">
                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; border-collapse: collapse; table-layout: fixed; margin: 0 auto;">
                                        <tr style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
                                            <td style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; text-align: right; padding-left: 5px; padding-right: 5px; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" align="right">
                                                <h3 class="heading" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; font-family: &#39;Lato&#39;, sans-serif; margin-top: 0; font-weight: 400; color: #000; font-size: 20px;">Contact Info</h3>
                                                <ul style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; margin: 0; padding: 0;">
                                                    <li style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; list-style: none; margin-bottom: 10px;"><span class="text" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">IT Center of RWTH Aachen University</span></li>
                                                    <li style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; list-style: none; margin-bottom: 10px;"> <a href="mailto:servicedesk@itc.rwth-aachen.de" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; text-decoration: none; color: rgba(0,0,0,1);">servicedesk@itc.rwth-aachen.de</a></li>
                                                    <li style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; list-style: none; margin-bottom: 10px;"><span class="text" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">+49 241 80 24687</span></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- end: tr -->
        </div>
    </center>
</body>
</html>