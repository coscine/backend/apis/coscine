using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Coscine.Api.Core.Repository;

public sealed class RdsS3ResourceTypeRepository(RepositoryContext repositoryContext) : IRdsS3ResourceTypeRepository
{
    private readonly RepositoryBase<RdsS3ResourceType> _rdsS3ResourceType = new(repositoryContext);

    public void Create(RdsS3ResourceType rdsS3ResourceType)
    {
        _rdsS3ResourceType.Create(rdsS3ResourceType);
    }

    public async Task<RdsS3ResourceType?> GetAsync(Guid rdsS3ResourceTypeId, bool trackChanges)
    {
        return await _rdsS3ResourceType.FindByCondition(x => x.Id == rdsS3ResourceTypeId, trackChanges)
            .SingleOrDefaultAsync();
    }
}