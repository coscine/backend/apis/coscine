﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Extensions;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Enums.Utility;
using Coscine.Api.Core.Shared.RequestFeatures;
using VDS.RDF;
using VDS.RDF.Nodes;
using VDS.RDF.Query;

namespace Coscine.Api.Core.Repository;

public class OrganizationRepository(IRdfRepositoryBase rdfRepositoryBase) : IOrganizationRepository
{
    private readonly IRdfRepositoryBase _rdfRepositoryBase = rdfRepositoryBase;

    /// <summary>
    /// Retrieves a list of organizations by a specified organization shibboleth URL.
    /// </summary>
    /// <param name="shibbolethUrl">The organization shibboleth URL to search for.</param>
    /// <returns>A list of organizations with the specified organization shibboleth URL.</returns>
    /// <example>
    /// Example value for RWTH shibboleth URL:
    /// <code>
    /// shibbolethUrl = "https://login.rz.rwth-aachen.de/shibboleth";
    /// </code>
    /// </example>
    public async Task<IEnumerable<Organization>> GetByShibbolethUrlAsync(string shibbolethUrl)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT DISTINCT ?ror (rdfs:label AS ?p) ?name ?mailbox
                WHERE {
                    {
                        SELECT ?ror (SAMPLE(?n) AS ?n_sample) (SAMPLE(?n_fb) AS ?n_fb_sample) (SAMPLE(?n_other) AS ?n_other_sample)
                        WHERE {
                            ?ror rdfs:label ?label .

                            BIND(IF(LANG(?label) = @lang, ?label, UNDEF) AS ?n)
                            BIND(IF(LANG(?label) = @fallbackLang, ?label, UNDEF) AS ?n_fb)
                            BIND(IF(LANG(?label) NOT IN (@lang, @fallbackLang), ?label, UNDEF) AS ?n_other)
                        }
                        GROUP BY ?ror
                    }
                    BIND(COALESCE(?n_sample, ?n_fb_sample, ?n_other_sample) AS ?name)

                    OPTIONAL {
                        ?ror coscineresource:contactInformation ?info .
                        ?info foaf:mbox ?mailbox .
                    }
                    {
                        SELECT DISTINCT ?ror
                        WHERE {
                            ?ror ?p ?value .
                            FILTER( ?value IN ( @shibbolethUrl ))
                        }
                    }
                }
            "
        };

        query.Namespaces.AddNamespace("coscineresource", RdfUris.CoscineTermsResource);
        query.Namespaces.AddNamespace("rdfs", RdfUris.RdfsUrlPrefix);
        var language = AcceptedLanguage.en;
        query.SetLiteral("lang", language.GetEnumMemberValue() ?? language.ToString(), normalizeValue: false);
        var fallbackLang = language == AcceptedLanguage.en ? AcceptedLanguage.de : AcceptedLanguage.en;
        query.SetLiteral("fallbackLang", fallbackLang.GetEnumMemberValue() ?? fallbackLang.ToString(), normalizeValue: false);
        query.SetLiteral("shibbolethUrl", shibbolethUrl, normalizeValue: false);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        return resultSet.Select(r =>
        {
            return new Organization
            {
                Uri = new Uri(r.Value("ror").ToSafeString()),
                DisplayName = r.Value("name").AsValuedNode().AsString(),
                Email = r.Value("mailbox")?.AsValuedNode().AsString().Replace("mailto:", "")
            };
        });
    }

    /// <summary>
    /// Retrieves an organization by the specified ROR URI and its corresponding RWTH-IKZ.
    /// </summary>
    /// <param name="organizationRorUri">The ROR URI of the organization to retrieve.</param>
    /// <returns>An organization with the specified ROR URI and its corresponding RWTH-IKZ.</returns>
    public async Task<Organization> GetByIkzAsync(Uri organizationRorUri)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT (SUBSTR( ?ikz, 5 ) AS ?ikz)
                WHERE {
                    GRAPH @rwthRor {
                        @subject org:identifier ?ikz .
                        FILTER strStarts( ?ikz, ""ikz:"" )
                    }
                }
            "
        };

        query.Namespaces.AddNamespace("org", RdfUris.OrgPrefix);
        query.SetUri("rwthRor", RdfUris.RwthRorId);
        query.SetUri("subject", organizationRorUri);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        return new Organization
        {
            Uri = organizationRorUri,
            RwthIkz = resultSet.FirstOrDefault()?.Value("ikz")?.AsValuedNode().AsString()
        };
    }

    /// <summary>
    /// Retrieves an organization by a specified organization attribute scope.
    /// </summary>
    /// <param name="attributeScope">The organization attribute scope to search for.</param>
    /// <returns>The first instance of <see cref="Organization"/> if an organization matching the specified attribute scope is found; otherwise, <c>null</c>.</returns>
    /// <example>
    /// Example usage:
    /// <code>
    /// string attributeScope = "rwth-aachen.de";
    /// Organization? organization = await GetByAttributeScopeAsync(attributeScope);
    /// </code>
    /// </example>
    public async Task<Organization?> GetByAttributeScopeAsync(string attributeScope)
    {
        var rors = (await FindRorsByAttributeScopeAsync(attributeScope))
            .FirstOrDefault();

        return rors != null ? await GetByRorUrlAsync(rors) : null;
    }

    /// <summary>
    /// Retrieves a list of all organizations with their corresponding RWTH-IKZs.
    /// </summary>
    /// <returns>A list of all organizations with their corresponding RWTH-IKZs.</returns>
    public async Task<IEnumerable<Organization>> GetAllAsync()
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT ?ror (SUBSTR( ?ikz, 5 ) AS ?ikz)
                WHERE {
                    GRAPH @rwthRor {
                        ?ror org:identifier ?ikz .
                        FILTER strStarts( ?ikz, ""ikz:"" )
                    }
                }
            "
        };

        query.Namespaces.AddNamespace("org", RdfUris.OrgPrefix);
        query.SetUri("rwthRor", RdfUris.RwthRorId);
        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        var organization2IkzList = new List<Organization>();
        foreach (var r in resultSet)
        {
            organization2IkzList.Add(new Organization
            {
                Uri = new Uri(r.Value("ror").ToSafeString()),
                RwthIkz = r.Value("ikz")?.AsValuedNode().AsString()
            });
        }
        return organization2IkzList;
    }

    /// <summary>
    /// Retrieves the email address of an organization based on its ROR (Research Organization Registry) URL.
    /// </summary>
    /// <param name="organizationRorUri">The ROR URL of the organization.</param>
    /// <returns>The email address of the organization, or <c>null</c> if not found.</returns>
    public async Task<string?> GetOrganizationEmailByRorUrlAsync(Uri organizationRorUri)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT ?mailbox
                WHERE {
                    GRAPH @rorUrl {
                        @rorUrl coscineresource:contactInformation ?s .
                        ?s foaf:mbox ?mailbox .
                    }
                }
            "
        };
        query.Namespaces.AddNamespace("coscineresource", RdfUris.CoscineTermsResource);
        query.Namespaces.AddNamespace("foaf", RdfUris.FoafPrefix);
        query.SetUri("rorUrl", organizationRorUri);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        // Assuming that there is only one email per organization.
        // Results for ?mailbox have the format "mailto:servicedesk@rwth-aachen.de"
        return resultSet?.FirstOrDefault()?.Value("mailbox")?.AsValuedNode().AsString().Replace("mailto:", "");
    }

    /// <inheritdoc/>
    public async Task<PagedEnumerable<Organization>> GetPagedAsync(OrganizationParameters organizationParameters)
    {
        // TODO: Avoid string interpolation, look into a better way to do that!
        var publicationAdvisoryFilter = (organizationParameters.FilterByPublicationService == true) ?
            "FILTER (STRLEN(?pubAdvServName) != 0 && STRLEN(?pubAdvServEmail) != 0 )" : "";

        // Define the base value query
        var query = new SparqlParameterizedString
        {
            CommandText = @$"
                SELECT DISTINCT ?ror ?name ?mailbox ?pubAdvServName ?pubAdvServEmail ?pubAdvServDesc
                WHERE {{
                    {{
                        SELECT ?ror (SAMPLE(?n) AS ?n_sample) (SAMPLE(?n_fb) AS ?n_fb_sample) (SAMPLE(?n_other) AS ?n_other_sample)
                        WHERE {{
                            GRAPH @rorPrefix {{
                                ?ror rdfs:label ?label .

                                OPTIONAL {{ ?ror dbp:abbreviation ?abbreviation . }}
                                OPTIONAL {{ ?ror dbp:alternativeName ?altName . }}

                                BIND(IF(LANG(?label) = @lang, ?label, UNDEF) AS ?n)
                                BIND(IF(LANG(?label) = @fallbackLang, ?label, UNDEF) AS ?n_fb)
                                BIND(IF(LANG(?label) NOT IN (@lang, @fallbackLang), ?label, UNDEF) AS ?n_other)
                                
                                # Apply REGEX before assigning to labels by language
                                FILTER (
                                    REGEX(?ror, @searchTerm, ""i"") ||
                                    REGEX(?label, @searchTerm, ""i"") ||
                                    REGEX(?abbreviation, @searchTerm, ""i"") ||
                                    REGEX(?altName, @searchTerm, ""i"")
                                )
                            }}
                        }}
                        GROUP BY ?ror
                    }}
                    BIND(COALESCE(?n_sample, ?n_fb_sample, ?n_other_sample) AS ?name)
                    
                    OPTIONAL {{
                        ?ror coscineorganization:dataPublicationService ?service .

                        ?service foaf:name ?pubAdvServName . 
                        FILTER (LANG(?pubAdvServName) = @lang) .

                        ?service foaf:mbox ?pubAdvServEmail .
                        
                        ?service foaf:text ?pubAdvServDesc . 
                        FILTER (LANG(?pubAdvServDesc) = @lang) .
                    }}
                    OPTIONAL {{
                        ?ror coscineresource:contactInformation ?info . 
                        ?info foaf:mbox ?mailbox .
                    }}
                    {publicationAdvisoryFilter}
                }}
                GROUP BY ?ror ?mailbox ?pubAdvServName ?pubAdvServEmail ?pubAdvServDesc
            "
        };

        // Set namespaces, literals and URIs
        query.Namespaces.AddNamespace("rdfs", RdfUris.RdfsUrlPrefix);
        query.Namespaces.AddNamespace("foaf", RdfUris.FoafPrefix);
        query.Namespaces.AddNamespace("coscineresource", RdfUris.CoscineTermsResource);
        query.Namespaces.AddNamespace("coscineorganization", RdfUris.CoscineTermsOrganizations);
        query.Namespaces.AddNamespace("dbp", RdfUris.DbpPrefix);
        query.Namespaces.AddNamespace("skos", RdfUris.SkosPrefix);
        query.SetUri("rorPrefix", RdfUris.RorPrefix);
        query.SetLiteral("lang", organizationParameters.Language.GetEnumMemberValue() ?? organizationParameters.Language.ToString(), normalizeValue: false);
        var fallbackLang = organizationParameters.Language == AcceptedLanguage.en ? AcceptedLanguage.de : AcceptedLanguage.en;
        query.SetLiteral("fallbackLang", fallbackLang.GetEnumMemberValue() ?? fallbackLang.ToString(), normalizeValue: false);
        query.SetLiteral("searchTerm", organizationParameters.SearchTerm ?? string.Empty, normalizeValue: false);

        // Build and execute count query
        var count = await query.GetCountOrDefaultAsync(_rdfRepositoryBase);

        // Always sort before adding pagination, otherwise the query breaks!
        query.Sort<Organization>(organizationParameters.OrderBy,
            new()
            {
                {"ror", o => o.Uri },
                {"name", o => o.DisplayName },
                {"mailbox", o => o.Email }
            }
        );
        query.Paginate(organizationParameters.PageSize, organizationParameters.PageNumber);
        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        // Extract the objects
        var organizations = resultSet
            .Select(r =>
            {
                PublicationAdvisoryService? publicationAdvisoryService = null;
                var pubAdvServName = r.Value("pubAdvServName")?.AsValuedNode().AsString();
                var pubAdvServEmail = r.Value("pubAdvServEmail")?.AsValuedNode().AsString().Replace("mailto:", "");
                var pubAdvServDesc = r.Value("pubAdvServDesc")?.AsValuedNode().AsString();
                if (pubAdvServName != null && pubAdvServEmail != null)
                {
                    publicationAdvisoryService = new()
                    {
                        DisplayName = pubAdvServName,
                        Email = pubAdvServEmail,
                        Description = pubAdvServDesc
                    };
                }

                return new Organization
                {
                    Uri = new Uri(r.Value("ror").ToSafeString()),
                    DisplayName = r.Value("name").AsValuedNode().AsString(),
                    Email = r.Value("mailbox")?.AsValuedNode().AsString().Replace("mailto:", ""),
                    PublicationAdvisoryService = publicationAdvisoryService
                };
            });

        return new PagedEnumerable<Organization>(organizations, count, organizationParameters.PageNumber, organizationParameters.PageSize);
    }

    public async Task<Organization?> GetByRorUrlAsync(Uri organizationRorUrl)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT DISTINCT (@rorUrl AS ?ror) ?name ?mailbox ?pubAdvServName ?pubAdvServEmail ?pubAdvServDesc
                WHERE {
                    {
                        SELECT ?ror (SAMPLE(?n) AS ?n_sample) (SAMPLE(?n_fb) AS ?n_fb_sample) (SAMPLE(?n_other) AS ?n_other_sample)
                        WHERE {
                            @rorUrl rdfs:label ?label .

                            BIND(IF(LANG(?label) = @lang, ?label, UNDEF) AS ?n)
                            BIND(IF(LANG(?label) = @fallbackLang, ?label, UNDEF) AS ?n_fb)
                            BIND(IF(LANG(?label) NOT IN (@lang, @fallbackLang), ?label, UNDEF) AS ?n_other)
                            BIND(@rorUrl AS ?ror)
                        }
                        GROUP BY ?ror
                    }
                    BIND(COALESCE(?n_sample, ?n_fb_sample, ?n_other_sample) AS ?name)

                    OPTIONAL {
                        @rorUrl coscineorganization:dataPublicationService ?service .
                            ?service foaf:name ?pubAdvServName . FILTER (LANG(?pubAdvServName) = @lang) .
                            ?service foaf:mbox ?pubAdvServEmail .
                            ?service foaf:text ?pubAdvServDesc . FILTER (LANG(?pubAdvServDesc) = @lang) . 
                    }
                    OPTIONAL {
                        @rorUrl coscineresource:contactInformation ?info .
                        ?info foaf:mbox ?mailbox .
                    }
                }
            "
        };

        // Set namespaces, literals and URIs
        query.Namespaces.AddNamespace("rdfs", RdfUris.RdfsUrlPrefix);
        query.Namespaces.AddNamespace("foaf", RdfUris.FoafPrefix);
        query.Namespaces.AddNamespace("coscineresource", RdfUris.CoscineTermsResource);
        query.Namespaces.AddNamespace("coscineorganization", RdfUris.CoscineTermsOrganizations);
        var language = AcceptedLanguage.en;
        query.SetLiteral("lang", language.GetEnumMemberValue() ?? language.ToString(), normalizeValue: false);
        var fallbackLang = language == AcceptedLanguage.en ? AcceptedLanguage.de : AcceptedLanguage.en;
        query.SetLiteral("fallbackLang", fallbackLang.GetEnumMemberValue() ?? fallbackLang.ToString(), normalizeValue: false);
        query.SetUri("rorUrl", organizationRorUrl);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        // Extract the object
        var organization = resultSet
            .Select(r =>
            {
                PublicationAdvisoryService? publicationAdvisoryService = null;
                var pubAdvServName = r.Value("pubAdvServName")?.AsValuedNode().AsString();
                var pubAdvServEmail = r.Value("pubAdvServEmail")?.AsValuedNode().AsString().Replace("mailto:", "");
                var pubAdvServDesc = r.Value("pubAdvServDesc")?.AsValuedNode().AsString();
                if (pubAdvServName != null && pubAdvServEmail != null)
                {
                    publicationAdvisoryService = new()
                    {
                        DisplayName = pubAdvServName,
                        Email = pubAdvServEmail,
                        Description = pubAdvServDesc
                    };
                }

                return new Organization
                {
                    Uri = new Uri(r.Value("ror").ToSafeString()),
                    DisplayName = r.Value("name").AsValuedNode().AsString(),
                    Email = r.Value("mailbox")?.AsValuedNode().AsString().Replace("mailto:", ""),
                    PublicationAdvisoryService = publicationAdvisoryService
                };
            })
            .SingleOrDefault();

        return organization;
    }

    [Obsolete("This method is most likely obsolete, as institutes have been removed from the user entity.")]
    public async Task<IEnumerable<Organization>> GetInstitutesByUserExternalIdAsync(IEnumerable<ExternalId> userExternalIds)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT DISTINCT ?ror ?name ?mailbox
                WHERE {
                    ?rootOrg ?p ?rootValue .
                    FILTER( ?rootValue IN ( @shibbolethUrls ))
                    ?rootOrg org:hasUnit ?ror .
                    ?ror rdfs:label ?name .
                    OPTIONAL {
                        ?ror coscineresource:contactInformation ?info .
                        ?info foaf:mbox ?mailbox .
                    }
                    ?class org:organization ?ror ;
                        ?member ?memberUrl .
                    ?memberUrl ?openId ?value .
                    FILTER(?value IN ( @externalIds ))
                }
            "
        };

        // Convert the external IDs to a comma-separated string enclosed in double quotes
        var externalIds = string.Join(", ", userExternalIds.Select(ei => $"\"{ei.ExternalId1}\""));
        query.CommandText = query.CommandText.Replace("@externalIds", externalIds);

        // Convert the shibboleth IDs to a comma-separated string enclosed in double quotes
        var shibbolethUrls = string.Join(", ", userExternalIds.Select(ei => $"\"{ei.Organization}\""));
        query.CommandText = query.CommandText.Replace("@shibbolethUrls", shibbolethUrls);

        // Set namespaces, literals and URIs
        query.Namespaces.AddNamespace("rdfs", RdfUris.RdfsUrlPrefix);
        query.Namespaces.AddNamespace("foaf", RdfUris.FoafPrefix);
        query.Namespaces.AddNamespace("org", RdfUris.OrgPrefix);
        query.Namespaces.AddNamespace("coscineresource", RdfUris.CoscineTermsResource);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        // Extract the object
        var organizations = resultSet
            .Select(r => new Organization
            {
                Uri = new Uri(r.Value("ror").ToSafeString()),
                DisplayName = r.Value("name").AsValuedNode().AsString(),
                Email = r.Value("mailbox")?.AsValuedNode().AsString().Replace("mailto:", "")
            });

        return organizations;
    }

    /// <summary>
    /// Executes a SPARQL query to retrieve a list of ROR URIs that match the specified attribute scope.
    /// </summary>
    /// <param name="attributeScope">The attribute scope used to filter the ROR entities in the query. If null or empty, matches any scope.</param>
    /// <returns>A list of <see cref="Uri"/> objects representing the RORs that match the specified attribute scope.</returns>
    private async Task<List<Uri>> FindRorsByAttributeScopeAsync(string attributeScope)
    {
        var query = new SparqlParameterizedString
        {
            CommandText = @"
                SELECT DISTINCT ?ror ?attributescope
                WHERE {
                    GRAPH ?graph {
                        ?ror coscineidentity:attributescope ?attributescope .
                        FILTER(REGEX(?attributescope, @searchTerm, ""i""))
                    }
                }
            "
        };

        query.Namespaces.AddNamespace("coscineidentity", RdfUris.CoscineTermsIdentity);
        query.SetLiteral("searchTerm", attributeScope ?? string.Empty, normalizeValue: false);

        var resultSet = await _rdfRepositoryBase.RunQueryAsync(query);

        if (resultSet == null || resultSet.IsEmpty)
        {
            return [];
        }

        // Safely convert results to URIs, ignoring invalid URIs
        var rors = resultSet
            .Select(r =>
            {
                var uriString = r.Value("ror").ToSafeString();
                return Uri.TryCreate(uriString, UriKind.Absolute, out var uri) ? uri : null;
            })
            .Where(uri => uri != null) // Filter out null URIs
            .ToList() as List<Uri>;

        return rors;
    }
}