using Amazon.Runtime;
using Coscine.Api.Core.Entities.OtherModels;

namespace Coscine.Api.Core.Repository;

public class CustomHttpClientFactory(IHttpClientFactory httpClientFactory) : Amazon.Runtime.HttpClientFactory
{
    public override HttpClient CreateHttpClient(IClientConfig clientConfig)
    {
        return httpClientFactory.CreateClient(ClientName.RdsS3Client);
    }
}
