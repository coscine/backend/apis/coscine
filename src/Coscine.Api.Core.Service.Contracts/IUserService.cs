using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IUserService
{
    Task<PagedEnumerable<UserDto>> GetPagedUsersAsync(UserAdminParameters userParameters, bool trackChanges);

    Task<IEnumerable<PublicUserDto>> GetFirstTenPublicUsersAsync(ClaimsPrincipal principal, UserSearchParameters userParameters, bool trackChanges);

    Task<UserDto> GetUserAsync(ClaimsPrincipal principal, bool trackChanges);

    Task<UserDto> GetUserByIdAsync(Guid userId, bool trackChanges);

    Task<UserMergeDto> CreateMergeTokenForUserAsync(ClaimsPrincipal principal, UserMergeExternalAuthDto userMergeExternalAuthDto, HttpContext httpContext, bool trackChanges);

    Task ConfirmUserEmailAsync(UserEmailConfirmationDto contactEmailConfirmationDto, bool trackChanges);

    Task AcceptTermsOfServiceAsync(UserTermsOfServiceAcceptDto userTermsOfServiceAcceptDto, ClaimsPrincipal principal, bool trackChanges);

    Task UpdateUserAsync(ClaimsPrincipal principal, UserForUpdateDto userForUpdateDto, HttpContext httpContext, bool trackChanges);

    Task<Guid?> EnsureInternalUserAsync(ClaimsPrincipal principal, Guid externalAuthenticatorId, bool trackChanges);

    Task MergeUser(Guid fromUserId, Guid toUserId);
}