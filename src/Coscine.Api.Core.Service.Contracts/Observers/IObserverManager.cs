﻿namespace Coscine.Api.Core.Service.Contracts.Observers;

public interface IObserverManager
{
    IEnumerable<IAPCreationRequestObserver> APCreationRequestObservers { get; }
    IEnumerable<IAPCreationRequestServiceDeskObserver> APCreationRequestServiceDeskObservers { get; }
    IEnumerable<IPidContactConfirmationObserver> PidContactConfirmationObservers { get; }
    IEnumerable<IPidProjectContactObserver> PidProjectContactObservers { get; }
    IEnumerable<IPidResourceContactObserver> PidResourceContactObservers { get; }
    IEnumerable<IProjectCreationObserver> ProjectCreationObservers { get; }
    IEnumerable<IProjectResponsibleOrganizationChangeObserver> ProjectResponsibleOrganizationChangeObservers { get; }
    IEnumerable<IProjectDeletionObserver> ProjectDeletionObservers { get; }
    IEnumerable<IProjectInvitationCreationObserver> ProjectInvitationCreationObservers { get; }
    IEnumerable<IProjectRoleCreationObserver> ProjectRoleCreationObservers { get; }
    IEnumerable<IProjectRoleUpdateObserver> ProjectRoleUpdateObservers { get; }
    IEnumerable<IProjectRoleDeletionObserver> ProjectRoleDeletionObservers { get; }
    IEnumerable<IResourceCreationObserver> ResourceCreationObservers { get; }
    IEnumerable<IResourceMaintenanceModeEnabledObserver> ResourceMaintenanceModeEnabledObservers { get; }
    IEnumerable<IResourceMaintenanceModeDisabledObserver> ResourceMaintenanceModeDisabledObservers { get; }
    IEnumerable<IResourceDeletionObserver> ResourceDeletionObservers { get; }
    IEnumerable<IUserEmailChangedObserver> UserEmailChangedObservers { get; }
}