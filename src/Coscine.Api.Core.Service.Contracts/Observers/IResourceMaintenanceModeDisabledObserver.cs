using Coscine.Api.Core.Entities.EventArgs;

namespace Coscine.Api.Core.Service.Contracts.Observers;

public interface IResourceMaintenanceModeDisabledObserver : IObserver<ResourceMaintenanceModeDisabledEventArgs>
{
}