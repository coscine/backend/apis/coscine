﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.QueryParameters;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Security.Claims;
using VDS.RDF;

namespace Coscine.Api.Core.Service.Contracts;

public interface ITreeService
{
    Task<PagedEnumerable<FileTreeDto>> GetPagedFileTreeAsync(
        Guid projectId,
        Guid resourceId,
        ClaimsPrincipal principal,
        FileTreeParameters treeParameters
    );

    Task<PagedEnumerable<MetadataTreeDto>> GetPagedMetadataTreeAsync(
        Guid projectId,
        Guid resourceId,
        ClaimsPrincipal principal,
        MetadataTreeParameters treeParameters
    );

    Task<MetadataTreeDto> CreateMetadataTreeAsync(
        Guid projectId,
        Guid resourceId,
        ClaimsPrincipal principal,
        MetadataTreeForCreationDto metadataTreeForCreationDto
    );

    Task<MetadataTreeDto> CreateExtractedMetadataTreeAsync(
        Guid projectId,
        Guid resourceId,
        ClaimsPrincipal principal,
        ExtractedMetadataTreeForCreationDto metadataTreeForCreationDto
    );

    Task UpdateMetadataTreeAsync(
        Guid projectId,
        Guid resourceId,
        ClaimsPrincipal principal,
        MetadataTreeForUpdateDto metadataTreeForUpdateDto
    );

    Task UpdateExtractedMetadataTreeAsync(
        Guid projectId,
        Guid resourceId,
        ClaimsPrincipal principal,
        ExtractedMetadataTreeForUpdateDto metadataTreeForUpdateDto
    );

    Task CopyMetadataLocalAsync(Resource resourceEntity, IGraph graph);

    Task<MetadataTreeDto> GetMetadataTreeAsync(
        Guid projectId,
        Guid resourceId,
        ClaimsPrincipal principal,
        MetadataTreeQueryParameters metadataTreeQueryParameters
    );

    Task DeleteMetadataTreeAsync(
        Guid projectId,
        Guid resourceId,
        ClaimsPrincipal principal,
        MetadataTreeForDeletionDto metadataTreeForDeletionDto
    );
}
