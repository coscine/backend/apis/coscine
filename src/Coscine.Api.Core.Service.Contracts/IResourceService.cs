using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IResourceService
{
    Task<PagedEnumerable<ResourceAdminDto>> GetPagedResourcesAsync(ResourceAdminParameters resourceParameters, bool trackChanges);

    Task<PagedEnumerable<ResourceDto>> GetPagedResourcesForProjectAsync(Guid projectId, ResourceParameters resourceParameters, bool trackChanges);

    Task<ResourceDto> GetResourceByIdForProjectAsync(Guid projectId, Guid resourceId, bool trackChanges);

    Task UpdateResourceForProjectAsync(Guid projectId, Guid resourceId, ResourceForUpdateDto resourceForUpdateDto, bool trackChanges);

    Task DeleteResourceForProjectAsync(Guid projectId, Guid resourceId, bool trackChanges);

    Task<ResourceDto> CreateResourceForProjectAsync(Guid projectId, ResourceForCreationDto resourceForCreationDto);

    Task<ResourceDto> GetResourceByIdAsync(Guid resourceId, bool trackChanges);
}