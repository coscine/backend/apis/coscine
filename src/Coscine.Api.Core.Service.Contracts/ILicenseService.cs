﻿using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service.Contracts;

public interface ILicenseService
{
    Task<PagedEnumerable<LicenseDto>> GetPagedLicenses(LicenseParameters licenseParameters, bool trackChanges);

    Task<LicenseDto> GetLicenseById(Guid licenseId, bool trackChanges);
}