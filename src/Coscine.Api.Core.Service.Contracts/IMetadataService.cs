﻿using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service.Contracts;

public interface IMetadataService
{
    Task<RdfDefinitionDto> GetMetadataGraphAsync(Uri graphUri, MetadataGetAdminParameters metadataGetAdminParameters, bool trackChanges);

    Task<PagedEnumerable<DeployedGraphDto>> GetPagedDeployedGraphsAsync(DeployedGraphParameters deployedGraphParameters);

    Task UpdateMetadataGraphAsync(Uri graphUri, MetadataUpdateAdminParameters metadataUpdateAdminParameters, bool trackChanges);

    Task PatchMetadataGraphAsync(Uri graphUri, RdfPatchDocumentDto rdfPatchDoc);
}