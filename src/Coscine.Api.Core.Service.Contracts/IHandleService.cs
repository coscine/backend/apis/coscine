﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IHandleService
{
    Task<HandleDto?> CreateHandleAsync(ClaimsPrincipal principal, string prefix, string suffix, IEnumerable<HandleValue> handleValues);

    Task<HandleDto> GetAsync(ClaimsPrincipal principal, string prefix, string suffix);

    Task UpdateAsync(ClaimsPrincipal principal, string prefix, string suffix, HandleForUpdateDto handleForUpdateDto);
}