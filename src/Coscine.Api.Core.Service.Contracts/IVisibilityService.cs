using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service.Contracts;

public interface IVisibilityService
{
    Task<PagedEnumerable<VisibilityDto>> GetPagedVisibilities(VisibilityParameters visibilityParameters, bool trackChanges);

    Task<VisibilityDto> GetVisibilityById(Guid id, bool trackChanges);
}