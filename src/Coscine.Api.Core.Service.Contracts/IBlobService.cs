﻿using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IBlobService
{
    Task<Stream> GetBlobStreamAsync(Guid projectId, Guid resourceId, string key, ClaimsPrincipal principal, bool trackChanges);

    Task DeleteBlobAsync(Guid projectId, Guid resourceId, string key, ClaimsPrincipal principal, bool trackChanges);

    Task CreateBlobAsync(Guid projectId, Guid resourceId, string key, ClaimsPrincipal principal, Stream? stream, long length, bool trackChanges);

    Task UpdateBlobAsync(Guid projectId, Guid resourceId, string key, ClaimsPrincipal principal, Stream? stream, long length, bool trackChanges);
}