﻿using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service.Contracts;

[Obsolete($"This class is obsolete and will be removed in the future. Use {nameof(IMessageService)} instead.")]
public interface IMaintenanceService
{
    Task<PagedEnumerable<MaintenanceDto>> GetPagedCurrentMaintenances(bool trackChanges);
}