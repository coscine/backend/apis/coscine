using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service.Contracts;

public interface IMessageService
{
    Task<PagedEnumerable<MessageDto>> GetPagedNocMessages(MessageParameters messageParameters);
    Task<PagedEnumerable<MessageDto>> GetInternalMessages(MessageParameters messageParameters);
}