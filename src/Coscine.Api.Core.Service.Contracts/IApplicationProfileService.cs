﻿using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Security.Claims;
using VDS.RDF;

namespace Coscine.Api.Core.Service.Contracts;

public interface IApplicationProfileService
{
    Task<PagedEnumerable<ApplicationProfileDto>> GetPagedApplicationProfilesAsync(ApplicationProfileParameters applicationProfileParameters, ClaimsPrincipal principal, bool trackChanges);

    Task<ApplicationProfileDto> GetApplicationProfileByUriAsync(Uri applicationProfileUri, RdfFormat rdfFormat, AcceptedLanguage language, ClaimsPrincipal principal, bool trackChanges);

    Task<ApplicationProfileRequestDto> CreateApplicationProfileRequestAsync(ClaimsPrincipal principal, ApplicationProfileForCreationDto applicationProfileForCreationDto);
    
    Task<IGraph?> GetRawApplicationProfileByUriAsync(Uri applicationProfileUri, RdfFormat rdfFormat, AcceptedLanguage language);
}