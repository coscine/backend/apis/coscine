﻿using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IApiTokenService
{
    Task<PagedEnumerable<ApiTokenDto>> GetPagedApiTokensAsync(ClaimsPrincipal principal, ApiTokenParameters apiTokenParameters, bool trackChanges);

    Task<ApiTokenDto> GetApiTokenAsync(Guid apiTokenId, ClaimsPrincipal principal, bool trackChanges);

    Task DeleteApiTokenAsync(Guid apiTokenId, ClaimsPrincipal principal, bool trackChanges);

    Task<ApiTokenDto> CreateApiTokenAsync(ClaimsPrincipal principal, ApiTokenForCreationDto apiTokenForCreationDto);
}