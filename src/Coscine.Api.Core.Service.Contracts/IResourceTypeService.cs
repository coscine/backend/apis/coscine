﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IResourceTypeService
{
    Task<IEnumerable<ResourceType>> GetAvailableResourceTypesForProjectAndUserAsync(Guid projectId, Guid userId, bool trackChanges);

    Task<IEnumerable<ResourceTypeInformationDto>> GetAvailableResourceTypesInformationForProjectAsync(Guid projectId, ClaimsPrincipal principal, bool trackChanges);

    Task<IEnumerable<ResourceTypeInformationDto>> GetAllResourceTypesInformationAsync(bool trackChanges);

    Task<ResourceTypeInformationDto> GetResourceTypeInformationAsync(Guid resourceTypeId, ClaimsPrincipal principal, bool trackChanges);

    Task<ResourceTypeOptionsDto> GetResourceTypeOptionsDto(Resource resourceEntity, bool includeQuota = true);

    //void CreateResourceTypeConfigurationForResource(Resource resourceEntity, ResourceType resourceTypeEntity, ResourceTypeOptionsForCreationDto resourceTypeOptions, out ResourceTypeConfiguration resourceTypeConfiguration);

    //Task UpdateResourceTypeConfigurationForResourceAsync(Resource resourceEntity, ResourceTypeOptionsForUpdateDto? resourceTypeOptionsForManipulation);
}
