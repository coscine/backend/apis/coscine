﻿using Coscine.Api.Core.Entities.OtherModels;

namespace Coscine.Api.Core.Service.Contracts;

public interface IEmailTemplateManager
{
    IReadOnlyDictionary<string, EmailTemplate> APCreationRequestTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> APCreationRequestServiceDeskTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> PidContactConfirmationTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> PidContactProjectTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> PidContactResourceTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> ProjectCreatedTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> ProjectResponsibleOrganizationChangeTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> ProjectDeletedTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> PublicationRequestOwnersTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> PublicationRequestPublicationAdvisoryServiceTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> PublicationRequestResourcePartTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> InviteUserToProjectTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> ResourceCreatedTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> ResourceMaintenanceModeEnabledTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> ResourceMaintenanceModeDisabledTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> ResourceDeletedTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> UserRoleChangedTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> UserDeletedTemplates { get; }
    IReadOnlyDictionary<string, EmailTemplate> UserEmailChangedTemplates { get; }
}