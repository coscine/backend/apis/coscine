﻿using Coscine.Api.Core.Entities;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IQuotaService
{
    Task<PagedEnumerable<ProjectQuotaDto>> GetPagedQuotasForProject(Guid projectId, ProjectQuotaParameters projectQuotaParameters, bool trackChanges);

    Task<ProjectQuotaDto> GetQuotaForProject(Guid projectId, Guid resourceTypeId, bool trackChanges);

    Task UpdateProjectQuota(Guid projectId, Guid resourceTypeId, ProjectQuotaForUpdateDto projectQuotaForUpdate, bool trackChanges);

    Task ValidateResourceQuotaOnCreationAsync(Guid projectId, Guid resourceTypeId, QuotaForManipulationDto? desiredResourceQuota);

    Task ValidateResourceQuotaOnUpdateAsync(Guid projectId, Guid resourceId, QuotaForManipulationDto? desiredResourceQuota);

    Task<ResourceQuotaDto> GetQuotaForResourceForProjectAsync(Guid projectId, Guid resourceId, bool trackChanges);

    Task<IEnumerable<ResourceQuotaDto>> GetQuotaForAllResourcesByTypeForProjectAsync(Guid projectId, Guid resourceTypeId, bool trackChanges);

    QuotaForManipulationDto? ExtractDesiredResourceQuotaForResource(ResourceType resourceTypeEntity, ResourceTypeOptionsForCreationDto? resourceTypeOptions);

    QuotaForManipulationDto? ExtractDesiredResourceQuotaForResource(ResourceType resourceTypeEntity, ResourceTypeOptionsForUpdateDto? resourceTypeOptions);
}