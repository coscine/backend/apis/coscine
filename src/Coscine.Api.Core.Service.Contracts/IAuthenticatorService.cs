﻿using Coscine.Api.Core.Entities.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IAuthenticatorService
{
    JwtSecurityToken GenerateJwtSecurityToken(IReadOnlyDictionary<string, string> payloadContents, JsonWebKey jsonWebKey);

    JwtSecurityToken GenerateJwtSecurityToken(IReadOnlyDictionary<string, string> payloadContents, double expiresInMinutes, JsonWebKey jsonWebKey);

    JwtSecurityToken GenerateJwtSecurityToken(IReadOnlyDictionary<string, string> payloadContents, double? expiresInMinutes = null);

    string GetJwtStringFromToken(JwtSecurityToken token);

    Task<User?> GetUserAsync(ClaimsPrincipal principal, bool trackChanges, bool allowCached = true);
    Task<User?> GetUserAsync(bool trackChanges, bool allowCached = true);
}