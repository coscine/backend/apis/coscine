using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;

namespace Coscine.Api.Core.Service.Contracts;

public interface IResourceTypeGitlabService
{
    Task<IEnumerable<GitlabProjectDto>> GetAllGitlabProjectsAsync(GitlabCredentialsDto gitlabCredentials);

    Task<GitlabProjectDto> GetGitlabProjectAsync(int gitlabProjectId, GitlabCredentialsDto gitlabCredentials);

    Task<IEnumerable<GitlabBranchDto>> GetAllGitlabBranchesForProjectAsync(int gitlabProjectId, GitlabCredentialsDto gitlabCredentials);
}