﻿using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.Core.Service.Contracts;

public interface ITosService
{
    TermsOfServiceDto GetCurrentTosVersion();
}