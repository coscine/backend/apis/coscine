﻿using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service.Contracts;

public interface IVocabularyService
{
    Task<PagedEnumerable<VocabularyDto>> GetTopLevelVocabularies(VocabularyParameters vocabularyParameters);

    Task<PagedEnumerable<VocabularyInstanceDto>> GetVocabularyInstancesAsync(VocabularyInstancesParameters instanceParameters);

    Task<VocabularyInstanceDto> GetVocabularyInstance(Uri instanceUri, AcceptedLanguage lang, AcceptedLanguage fallbackLanguage);
}