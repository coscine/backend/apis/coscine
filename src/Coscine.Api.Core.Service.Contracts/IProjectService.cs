﻿using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.QueryParameters;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IProjectService
{
    Task<PagedEnumerable<ProjectAdminDto>> GetPagedProjectsAsync(ProjectAdminParameters projectParameters, bool trackChanges);

    Task<PagedEnumerable<ProjectDto>> GetPagedProjectsOfUserAsync(ProjectParameters projectParameters, bool trackChanges);

    Task<ProjectDto> GetProjectAsync(Guid projectId, bool trackChanges, ProjectGetQueryParameters projectGetQueryParameters);

    Task DeleteProjectAsync(Guid projectId, bool trackChanges);

    Task UpdateProjectAsync(Guid projectId, ProjectForUpdateDto projectForUpdateDto, bool trackChanges);

    Task<ProjectDto> CreateProjectForUserAsync(ProjectForCreationDto projectForCreationDto);
}