﻿using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service.Contracts;

public interface ILanguageService
{
    Task<IEnumerable<LanguageDto>> GetAllLanguages(LanguageParameters languageParameters, bool trackChanges);

    Task<LanguageDto> GetLanguageById(Guid languageId, bool trackChanges);
}