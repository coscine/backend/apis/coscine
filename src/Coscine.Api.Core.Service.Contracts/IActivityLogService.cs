﻿using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service.Contracts;

public interface IActivityLogService
{
    Task<PagedEnumerable<ActivityLogDto>> GetPagedActivityLogs(ActivityLogParameters activityLogParameters, bool trackChanges);
}