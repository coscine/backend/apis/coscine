﻿using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IPublicationRequestService
{
    Task<IEnumerable<ProjectPublicationRequestDto>> GetAllProjectPublicationRequestsAsync(Guid projectId, bool trackChanges);
    Task<PagedEnumerable<ProjectPublicationRequestDto>> GetPagedProjectPublicationRequestsAsync(Guid projectId, ProjectPublicationRequestAdminParameters projectPublicationRequestAdminParameters, bool trackChanges);
    Task<ProjectPublicationRequestDto> GetPublicationRequestAsync(Guid projectId, Guid publicationRequestId, bool trackChanges);
    Task<ProjectPublicationRequestDto> CreatePublicationRequestForUserAsync(Guid projectId, PublicationRequestForCreationDto publicationRequestDto);
}
