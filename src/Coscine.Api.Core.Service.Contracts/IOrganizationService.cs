using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service.Contracts;

public interface IOrganizationService
{
    Task<PagedEnumerable<OrganizationDto>> GetPagedOrganizations(OrganizationParameters organizationParameters, bool trackChanges);

    Task<OrganizationDto> GetOrganization(Uri organizationRorUrl, bool trackChanges);

    Task<IEnumerable<UserOrganizationDto>> GetOrganizationsForUserExternalIdsAsync(User userEntity);
}