﻿using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service.Contracts;

public interface IDisciplineService
{
    Task<PagedEnumerable<DisciplineDto>> GetPagedDisciplines(DisciplineParameters disciplineParameters, bool trackChanges);

    Task<DisciplineDto> GetDisciplineById(Guid id, bool trackChanges);
}