﻿using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.QueryParameters;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IProvenanceService
{
    public Task<ProvenanceDto> GetProvenanceAsync(Guid projectId, Guid resourceId, ClaimsPrincipal user, ProvenanceQueryParameters provenanceQueryParameters);
    public Task UpdateProvenanceAsync(Guid projectId, Guid resourceId, ClaimsPrincipal user, ProvenanceForUpdateDto provenanceForUpdateDto);
}
