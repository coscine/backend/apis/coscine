using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IProjectInvitationService
{
    Task<PagedEnumerable<ProjectInvitationDto>> GetPagedProjectInvitations(Guid projectId, ProjectInvitationParameters projectInvitationParameters, bool trackChanges);

    Task<ProjectInvitationDto> GetProjectInvitationById(Guid projectId, Guid projectInvitationId, bool trackChanges);

    Task DeleteProjectInvitationOfProject(Guid projectId, Guid projectInvitationId, ClaimsPrincipal principal, bool trackChanges);

    Task<ProjectInvitationDto> InviteUserToProject(Guid projectId, ClaimsPrincipal principal, ProjectInvitationForProjectManipulationDto projectInvitationForProjectManipulationDto);

    Task ResolveInvitation(ClaimsPrincipal principal, ProjectInvitationResolveDto projectInvitationResolveDto, bool trackChanges);
}