﻿using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service.Contracts;

public interface ITitleService
{
    Task<IEnumerable<TitleDto>> GetAllTitlesAsync(TitleParameters titleParameters, bool trackChanges);

    Task<TitleDto> GetTitleByIdAsync(Guid titleId, bool trackChanges);
}