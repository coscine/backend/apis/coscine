using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IProjectRoleService
{
    Task<PagedEnumerable<ProjectRoleDto>> GetPagedProjectRolesOfProjectById(Guid projectId, ProjectRoleParameters projectRoleParameters, bool trackChanges);

    Task<ProjectRoleDto> GetProjectRoleOfProjectById(Guid projectId, Guid id, bool trackChanges);

    Task DeleteProjectRoleOfProject(Guid projectRole, Guid projectId, ClaimsPrincipal principal, bool trackChanges);

    Task UpdateProjectRoleOfProject(Guid projectRole, Guid projectId, ClaimsPrincipal principal, ProjectRoleForProjectManipulationDto projectRoleForProjectManipulationDto, bool trackChanges);

    Task<ProjectRoleDto> CreateProjectRoleForProject(Guid projectId, ClaimsPrincipal principal, ProjectRoleForProjectCreationDto projectRoleForProjectCreationDto);
}