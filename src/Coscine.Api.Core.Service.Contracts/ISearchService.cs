﻿using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface ISearchService
{
    Task<PagedSearchResult<SearchResultDto>> SearchAsync(ClaimsPrincipal principal, SearchParameters searchParameters);
}