using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service.Contracts;

public interface IRoleService
{
    Task<PagedEnumerable<RoleDto>> GetPagedRoles(RoleParameters roleParameters, bool trackChanges);

    Task<RoleDto> GetRoleById(Guid id, bool trackChanges);
}