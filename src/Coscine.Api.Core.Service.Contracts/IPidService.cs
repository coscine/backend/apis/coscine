﻿using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Contracts;

public interface IPidService
{
    public Task SendEmailToOwnerAsync(string prefix, string suffix, PidRequestDto pidRequestDto, bool trackChanges);
    Task<PagedEnumerable<PidDto>> GetPagedAsync(ClaimsPrincipal principal, PidParameters pidParameters, bool trackChanges);
    Task<PidDto> GetAsync(string prefix, string suffix, ClaimsPrincipal principal, bool trackChanges);
}
