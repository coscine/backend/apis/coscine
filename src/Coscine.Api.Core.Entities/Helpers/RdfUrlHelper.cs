﻿using System.Web;

namespace Coscine.Api.Core.Entities.Helpers;

public class RdfUrlHelper
{
    public static string GetKey(Guid resourceId, Uri uri)
    {
        string uriString = uri.AbsoluteUri;

        int guidIndex = uriString.LastIndexOf(resourceId.ToString());
        if (guidIndex != -1)
        {
            int startIndex = uriString.IndexOf('/', guidIndex) + 1;
            int endIndex = uriString.IndexOf('@');
            if (startIndex >= 0 && endIndex >= 0)
            {
                return uriString[startIndex..endIndex];
            }
        }
        return ""; // Return empty string if no path found or provided GUID not found
    }

    public static int GetVersionNumber(Uri uri)
    {
        var queryStart = uri.AbsoluteUri.IndexOf('@');
        var query = uri.AbsoluteUri[(queryStart + 1)..];

        var queryParams = HttpUtility.ParseQueryString(query);
        if (int.TryParse(queryParams["version"], out int version))
        {
            return version;
        }

        return -1;
    }
}
