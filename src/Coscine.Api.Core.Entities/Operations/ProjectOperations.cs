﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Core.Entities.Operations;

public static class ProjectOperations
{
    public static OperationAuthorizationRequirement CreateSubProject { get; } = new() { Name = nameof(CreateSubProject) };
    public static OperationAuthorizationRequirement CreateInvitation { get; } = new() { Name = nameof(CreateInvitation) };
    public static OperationAuthorizationRequirement CreatePublicationRequest { get; } = new() { Name = nameof(CreatePublicationRequest) };
    public static OperationAuthorizationRequirement CreateResource { get; } = new() { Name = nameof(CreateResource) };
    public static OperationAuthorizationRequirement ReadAvailableResourceTypes { get; } = new() { Name = nameof(ReadAvailableResourceTypes) };
    public static OperationAuthorizationRequirement Read { get; } = new() { Name = nameof(Read) };

    public static OperationAuthorizationRequirement Update { get; } = new() { Name = nameof(Update) };
    public static OperationAuthorizationRequirement Delete { get; } = new() { Name = nameof(Delete) };
}