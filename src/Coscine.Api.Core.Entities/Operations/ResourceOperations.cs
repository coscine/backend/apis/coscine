﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Core.Entities.Operations;

public static class ResourceOperations
{
    public static OperationAuthorizationRequirement Create { get; } = new() { Name = nameof(Create) };
    public static OperationAuthorizationRequirement CreateBlob { get; } = new() { Name = nameof(CreateBlob) };
    public static OperationAuthorizationRequirement CreateExtractionTree { get; } = new() { Name = nameof(CreateExtractionTree) };
    public static OperationAuthorizationRequirement CreateTree { get; } = new() { Name = nameof(CreateTree) };

    public static OperationAuthorizationRequirement Read { get; } = new() { Name = nameof(Read) };
    public static OperationAuthorizationRequirement ReadBlob { get; } = new() { Name = nameof(ReadBlob) };
    public static OperationAuthorizationRequirement ReadProvenance { get; } = new() { Name = nameof(ReadProvenance) };
    public static OperationAuthorizationRequirement ReadTree { get; } = new() { Name = nameof(ReadTree) };
    public static OperationAuthorizationRequirement ReadQuota { get; } = new() { Name = nameof(ReadQuota) };
    public static OperationAuthorizationRequirement ReadOptions { get; } = new() { Name = nameof(ReadOptions) };

    public static OperationAuthorizationRequirement Update { get; } = new() { Name = nameof(Update) };
    public static OperationAuthorizationRequirement UpdateMaintenanceMode { get; } = new() { Name = nameof(UpdateMaintenanceMode) };
    public static OperationAuthorizationRequirement UpdateBlob { get; } = new() { Name = nameof(UpdateBlob) };
    public static OperationAuthorizationRequirement UpdateExtractionTree { get; } = new() { Name = nameof(UpdateExtractionTree) };
    public static OperationAuthorizationRequirement UpdateProvenance { get; } = new() { Name = nameof(UpdateProvenance) };
    public static OperationAuthorizationRequirement UpdateTree { get; } = new() { Name = nameof(UpdateTree) };

    public static OperationAuthorizationRequirement Delete { get; } = new() { Name = nameof(Delete) };
    public static OperationAuthorizationRequirement DeleteBlob { get; } = new() { Name = nameof(DeleteBlob) };
    public static OperationAuthorizationRequirement DeleteTree { get; } = new() { Name = nameof(DeleteTree) };
}