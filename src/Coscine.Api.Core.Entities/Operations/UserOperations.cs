﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Core.Entities.Operations;

public static class UserOperations
{
    public static OperationAuthorizationRequirement ReadAdmin { get; } = new() { Name = nameof(ReadAdmin) };
}