﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Core.Entities.Operations;

public static class ProjectQuotaOperations
{
    public static OperationAuthorizationRequirement Read { get; } = new() { Name = nameof(Read) };
    public static OperationAuthorizationRequirement Update { get; } = new() { Name = nameof(Update) };
    public static OperationAuthorizationRequirement UpdateMaximum { get; } = new() { Name = nameof(UpdateMaximum) };
}