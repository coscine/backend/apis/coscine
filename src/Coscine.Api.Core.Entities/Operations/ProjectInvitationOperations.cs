﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Core.Entities.Operations;

public static class ProjectInvitationOperations
{
    //public static OperationAuthorizationRequirement Create { get; } = new() { Name = nameof(Create) };

    public static OperationAuthorizationRequirement Read { get; } = new() { Name = nameof(Read) };
    public static OperationAuthorizationRequirement Update { get; } = new() { Name = nameof(Update) };
    public static OperationAuthorizationRequirement Delete { get; } = new() { Name = nameof(Delete) };
    public static OperationAuthorizationRequirement Resolve { get; } = new() { Name = nameof(Resolve) };
}