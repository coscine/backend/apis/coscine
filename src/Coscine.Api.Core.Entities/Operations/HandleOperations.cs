﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Core.Entities.Operations;

public static class HandleOperations
{
    public static OperationAuthorizationRequirement Create { get; } = new() { Name = nameof(Create) };
    public static OperationAuthorizationRequirement Read { get; } = new() { Name = nameof(Read) };
    public static OperationAuthorizationRequirement Update { get; } = new() { Name = nameof(Update) };
    public static OperationAuthorizationRequirement Delete { get; } = new() { Name = nameof(Delete) };
}