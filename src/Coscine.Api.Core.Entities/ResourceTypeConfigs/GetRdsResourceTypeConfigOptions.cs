﻿namespace Coscine.Api.Core.Entities.ResourceTypeConfigs;

public class GetRdsResourceTypeConfigOptions : GetResourceTypeConfigOptions
{
    public string? Bucketname { get; set; }
}