﻿namespace Coscine.Api.Core.Entities.ResourceTypeConfigs;

public class GetGitLabResourceTypeConfigOptions : GetResourceTypeConfigOptions
{
    public int ProjectId { get; set; }
    public string AccessToken { get; set; } = null!;
    public string RepoUrl { get; set; } = null!;
    public string Branch { get; set; } = null!;
    public bool TosAccepted { get; set; } = false;
}