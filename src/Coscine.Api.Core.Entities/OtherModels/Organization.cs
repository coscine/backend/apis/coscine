﻿namespace Coscine.Api.Core.Entities.OtherModels;

public class Organization
{
    public Uri Uri { get; set; } = null!;
    public string? DisplayName { get; set; }
    public string? RwthIkz { get; set; }
    public string? Email { get; set; }
    public PublicationAdvisoryService? PublicationAdvisoryService { get; set; }
}