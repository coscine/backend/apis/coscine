﻿namespace Coscine.Api.Core.Entities.OtherModels;

public static class ClientName
{
    public const string RdsS3Client = nameof(RdsS3Client);

    /// <summary>
    /// Client used to communicate with the ElasticSearch API.
    /// </summary>
    public const string ElasticSearchClient = nameof(ElasticSearchClient);

    /// <summary>
    /// Client used to communicate with the Handle-Server.
    /// </summary>
    public const string HandleClient = nameof(HandleClient);

    /// <summary>
    /// Client used to communicate with the NOC-Portal API.
    /// </summary>
    /// <remarks>
    /// Maintenance API Documentation available at https://noc-portal.rz.rwth-aachen.de/ticket/api-tokens, or under section <i>API</i> in the NOC-Portal.
    /// </remarks>
    public const string NocClient = nameof(NocClient);

    /// <summary>
    /// Client used to communicate with the Virtuoso API for read operations.
    /// </summary>
    public const string VirtuosoReadClient = nameof(VirtuosoReadClient);

    /// <summary>
    /// Client used to communicate with the Virtuoso API for write/update operations.
    /// </summary>
    public const string VirtuosoUpdateClient = nameof(VirtuosoUpdateClient);
}