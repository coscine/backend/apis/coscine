﻿using System.Security.Cryptography;

namespace Coscine.Api.Core.Entities.OtherModels;

public class HashParameters
{
    public required HashAlgorithmName AlgorithmName { get; init; }
    public required string Value { get; init; }
}
