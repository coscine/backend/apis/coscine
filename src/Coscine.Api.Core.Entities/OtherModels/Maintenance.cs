﻿namespace Coscine.Api.Core.Entities.OtherModels;

/// <summary>
/// This class represents a maintenance with its significant properties.
/// </summary>
[Obsolete($"This class is obsolete and will be removed in the future. Use {nameof(NocTicket)} instead.")]
public partial class Maintenance
{
    /// <summary>
    /// Maintenance title.
    /// </summary>
    public string DisplayName { get; set; } = null!;

    /// <summary>
    /// Maintenance url.
    /// </summary>
    public Uri Url { get; set; } = null!;

    /// <summary>
    /// Maintenance type.
    /// </summary>
    public string Type { get; set; } = null!;

    /// <summary>
    /// Maintenance description.
    /// </summary>
    public string Body { get; set; } = null!;

    /// <summary>
    /// Maintenance start.
    /// </summary>
    public DateTime StartsDate { get; set; }

    /// <summary>
    /// Maintenance end.
    /// </summary>
    public DateTime EndsDate { get; set; }
}