﻿using Scriban;

namespace Coscine.Api.Core.Entities.OtherModels;

public class EmailTemplate
{
    public string Language { get; set; } = null!;
    public string SubjectTemplate { get; set; } = null!;
    public string MessageTemplate { get; set; } = null!;
    public string Header { get; set; } = null!;
    public string Footer { get; set; } = null!;

    public string ParseMessage(object values)
    {
        var template = Template.Parse(MessageTemplate);

        return template.Render(new { Values = values, Header, Footer });
    }

    public string ParseSubject(object values)
    {
        var template = Template.Parse(SubjectTemplate);
        return template.Render(new { Values = values });
    }
}