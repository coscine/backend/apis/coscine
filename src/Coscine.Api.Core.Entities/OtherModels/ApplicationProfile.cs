﻿using Coscine.Api.Core.Shared.Enums;
using VDS.RDF;

namespace Coscine.Api.Core.Entities.OtherModels;

public class ApplicationProfile
{
    public Uri Uri { get; set; } = null!;
    public string? DisplayName { get; set; }
    public string? Description { get; set; }
    public RdfFormat? Format { get; set; }
    public string? Definition { get; set; }
    public IGraph? Graph {get; set; }
}