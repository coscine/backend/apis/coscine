using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Entities.OtherModels;

/// <summary>
/// Represents a follow-up entry for a ticket.
/// </summary>
public class NocTicketFollowup
{
    /// <summary>
    /// Gets or sets the ID of the follow-up.
    /// </summary>
    [JsonPropertyName("id")]
    public string Id { get; set; } = null!;

    /// <summary>
    /// Gets or sets the updated date and time of the follow-up.
    /// </summary>
    [JsonPropertyName("updated_at")]
    public DateTime UpdatedAt { get; set; }

    /// <summary>
    /// Gets or sets the message of the follow-up.
    /// </summary>
    [JsonPropertyName("message")]
    public string Message { get; set; } = null!;
}