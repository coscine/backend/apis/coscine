﻿namespace Coscine.Api.Core.Entities.OtherModels.ResourceTypes;

/// <summary>
/// Object containing the relevant information about the resource type for the metadata VUE component.
/// </summary>
public class MetadataView
{
    /// <summary>
    /// A dataUrl can be provided.
    /// </summary>
    public bool EditableDataUrl { get; set; }

    /// <summary>
    /// A key can be provided.
    /// </summary>
    public bool EditableKey { get; set; }
}