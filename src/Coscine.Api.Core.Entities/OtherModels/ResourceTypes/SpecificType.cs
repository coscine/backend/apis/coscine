﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Entities.OtherModels.ResourceTypes;

public class SpecificType
{
    public string? Type { get; set; }
    public string? SpecificTypeName { get; set; }
    public SpecificTypeConfig? Config { get; set; }
    public SpecificTypeI18nName? Name { get; set; }
    public ResourceTypeStatus? Status { get; set; }
    public List<string>? SupportedOrganizations { get; set; }

    public class SpecificTypeConfig
    {
        public string? RdsKey { get; set; }
        public string? RdsS3Key { get; set; }
        public string? UserKey { get; set; }
    }

    public class SpecificTypeI18nName
    {
        public string? De { get; set; }
        public string? En { get; set; }
    }
}