﻿namespace Coscine.Api.Core.Entities.OtherModels.ResourceTypes;

/// <summary>
/// Object containing the relevant information about the resource type for the resource create page.
/// </summary>
public class ResourceCreate
{
    /// <summary>
    /// List of Lists containing all the resource type specific components for the steps in the resource creation page.
    /// </summary>
    public List<List<string>> Components { get; set; } = null!;
}