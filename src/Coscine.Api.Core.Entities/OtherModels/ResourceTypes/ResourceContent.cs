﻿namespace Coscine.Api.Core.Entities.OtherModels.ResourceTypes;

/// <summary>
/// Object containing the relevant information about the resource type for the resource content page.
/// </summary>
public class ResourceContent
{
    /// <summary>
    /// Resource is read only.
    /// </summary>
    public bool ReadOnly { get; set; }

    /// <summary>
    /// Object containing the relevant information about the resource type for the metadata VUE component.
    /// </summary>
    public MetadataView MetadataView { get; set; } = null!;

    /// <summary>
    /// Object containing the relevant information about the resource type for the entries view VUE component.
    /// </summary>
    public EntriesView EntriesView { get; set; } = null!;
}