﻿namespace Coscine.Api.Core.Entities.OtherModels.ResourceTypes;

/// <summary>
/// Object containing the relevant information about the resource type for the entries view Vue component.
/// </summary>
public class EntriesView
{
    /// <summary>
    /// Object containing the relevant information about the resource type for the columns within entries view Vue component.
    /// </summary>
    public ColumnsObject Columns { get; set; } = new ColumnsObject();
}