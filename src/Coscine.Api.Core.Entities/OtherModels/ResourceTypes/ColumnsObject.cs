﻿namespace Coscine.Api.Core.Entities.OtherModels.ResourceTypes;

/// <summary>
/// Object containing the relevant information about the resource type for the columns within entries view VUE component.
/// </summary>
public class ColumnsObject
{
    /// <summary>
    /// List of columns that should always be displayed in the entries view.
    /// </summary>
    public List<string> Always { get; set; } = new List<string>() { "name", "size" };
}