﻿namespace Coscine.Api.Core.Entities.OtherModels.ResourceTypes;

public class ResourceTypeConfiguration
{
    public required SpecificType SpecificType { get; set; }
}