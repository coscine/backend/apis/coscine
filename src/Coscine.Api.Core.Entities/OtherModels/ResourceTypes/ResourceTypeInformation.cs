﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Entities.OtherModels.ResourceTypes;

/// <summary>
/// Represents all relevant information about the resource type intended for frontend use.
/// </summary>
public class ResourceTypeInformation
{
    /// <summary>
    /// Indicates whether the resource type is active.
    /// </summary>
    public bool IsEnabled => Status == ResourceTypeStatus.Active;

    /// <summary>
    /// Status of the resource type.
    /// </summary>
    public ResourceTypeStatus Status { get; set; }

    /// <summary>
    /// Indicates whether creation is supported.
    /// </summary>
    public bool CanCreate { get; set; }

    /// <summary>
    /// Indicates whether reading is supported.
    /// </summary>
    public bool CanRead { get; set; }

    /// <summary>
    /// Indicates whether setting a resource to read-only is supported.
    /// </summary>
    public bool CanSetResourceReadonly { get; set; }

    /// <summary>
    /// Indicates whether updating is supported.
    /// </summary>
    public bool CanUpdate { get; set; }

    /// <summary>
    /// Indicates whether updating a resource (that's not an object) is supported.
    /// </summary>
    public bool CanUpdateResource { get; set; }

    /// <summary>
    /// Indicates whether deletion is supported.
    /// </summary>
    public bool CanDelete { get; set; }

    /// <summary>
    /// Indicates whether deletion of a resource (that's not an object) is supported.
    /// </summary>
    public bool CanDeleteResource { get; set; }

    /// <summary>
    /// Indicates whether listing is supported.
    /// </summary>
    public bool CanList { get; set; }

    /// <summary>
    /// Indicates whether creating links is supported.
    /// </summary>
    public bool CanCreateLinks { get; set; }

    /// <summary>
    /// Indicates whether local backup of metadata is supported.
    /// </summary>
    public bool CanCopyLocalMetadata { get; set; }

    /// <summary>
    /// Indicates whether the resource type is archived.
    /// </summary>
    public bool IsArchived { get; set; }

    /// <summary>
    /// Indicates if the resource type has a quota.
    /// </summary>
    public bool IsQuotaAvailable { get; set; }

    /// <summary>
    /// Indicates if the resource type quota can be adjusted.
    /// </summary>
    public bool IsQuotaAdjustable { get; set; }

    /// <summary>
    /// General resource type.
    /// </summary>
    public string? GeneralType { get; set; }

    /// <summary>
    /// Specific resource type.
    /// </summary>
    public string? SpecificType { get; set; }

    /// <summary>
    /// Unique identifier of the resource type.
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Information relevant for the resource creation page.
    /// </summary>
    public ResourceCreate ResourceCreate { get; set; } = null!;

    /// <summary>
    /// Information relevant for the resource content page.
    /// </summary>
    public ResourceContent ResourceContent { get; set; } = null!;
}