﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Entities.OtherModels;

public class SearchResult
{
    public Uri? Uri { get; set; }

    public SearchCategoryType? Type { get; set; }

    public dynamic Source { get; set; } = null!;
}