﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Entities.OtherModels;

public class MetadataTree : Tree
{
    public Uri? Id { get; init; }
    public string Version { get; init; } = null!;
    public IEnumerable<string> AvailableVersions { get; init; } = new List<string>();
    public string? Definition { get; init; } = null!;
    public RdfFormat Format { get; init; }
    public bool SkipValidation { get; init; } = false;
    public bool ForceNewMetadataVersion { get; init; } = false;
    public MetadataTreeExtracted? Extracted { get; init; }
    public Provenance? Provenance { get; init; }
}
