﻿namespace Coscine.Api.Core.Entities.OtherModels;

public class Vocabulary
{
    /// <summary>
    /// The name of the graph, which contains the vocabulary.
    /// </summary>
    /// <remarks>
    /// See <c>@base</c> URI from vocabulary definitions.
    /// </remarks>
    public Uri GraphUri { get; set; } = null!;

    /// <summary>
    /// The top-level parent class in the vocabulary.
    /// </summary>
    /// <remarks>
    /// The definition has no <c>rdfs:subClassOf</c> property.
    /// </remarks>
    public Uri ClassUri { get; set; } = null!;

    public string? DisplayName { get; set; }

    public string? Description { get; set; }
}