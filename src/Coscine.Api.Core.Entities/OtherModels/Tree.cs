﻿namespace Coscine.Api.Core.Entities.OtherModels;

public class Tree
{
    /// <summary>
    /// URL Decoded Path
    /// </summary>
    public string Path { get; init; } = null!;
    public bool HasChildren { get; init; }
}