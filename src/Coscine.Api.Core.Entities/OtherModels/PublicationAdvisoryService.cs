﻿namespace Coscine.Api.Core.Entities.OtherModels;

public class PublicationAdvisoryService
{
    public string DisplayName { get; set; } = null!;

    public string Email { get; set; } = null!;

    public string? Description { get; set; } = null!;
}
