﻿namespace Coscine.Api.Core.Entities.OtherModels;

public class DataTree : Tree
{
    public Uri Id { get; init; } = null!;
    public string Version { get; init; } = null!;
    public IEnumerable<string> AvailableVersions { get; init; } = new List<string>();
}