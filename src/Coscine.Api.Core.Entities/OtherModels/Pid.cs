﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Entities.OtherModels;

/// <summary>
/// Represents a unique identifier (PID) consisting of a prefix and a suffix.
/// </summary>
public class Pid
{
    /// <summary>
    /// Gets or sets the prefix of the PID.
    /// </summary>
    /// <value>
    /// A required string representing the prefix of the PID.
    /// </value>
    public required string Prefix { get; set; }

    /// <summary>
    /// Gets or sets the suffix of the PID.
    /// </summary>
    /// <value>
    /// A required string representing the suffix of the PID.
    /// </value>
    public required string Suffix { get; set; }

    /// <summary>
    /// Gets the constructed PID by combining the prefix and suffix ("{Prefix}/{Suffix}").
    /// </summary>
    /// <value>
    /// The constructed PID string.
    /// </value>
    public string Identifier => $"{Prefix}/{Suffix}";

    /// <summary>
    /// Gets or sets the type of the PID.
    /// </summary>
    /// <value>
    /// An optional enumeration representing the type of the PID (e.g., PidType.Project, PidType.Resource).
    /// </value>
    public PidType? Type { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether the underlying entity (project or resource) associated with the PID exists and is valid.
    /// </summary>
    /// <value>
    /// This property reflects the existence and validity of the associated entity. If the entity is deleted or never existed, this property is set to <c>false</c>; otherwise <c>true</c>.
    /// </value>
    public bool IsEntityValid { get; init; }
}
