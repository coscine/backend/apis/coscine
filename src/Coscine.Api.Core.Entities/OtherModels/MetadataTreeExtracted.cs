﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Entities.OtherModels;

public class MetadataTreeExtracted : Tree
{
    public Uri MetadataId { get; init; } = null!;
    public Uri? RawDataId { get; init; } = null!;
    public string Definition { get; init; } = null!;
    public RdfFormat Format { get; init; }
}
