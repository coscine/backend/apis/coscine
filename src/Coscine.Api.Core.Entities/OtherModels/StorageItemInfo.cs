using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Entities.OtherModels;

public class StorageItemInfo
{
    public required string Path { get; set; }
    public long? ContentLength { get; set; }  // Null for trees
    public DateTime? CreatedDate { get; set; } // Might be missing
    public DateTime? LastModifiedDate { get; set; } // Might be missing
    public bool IsBlob { get; set; } // Indicates if the item is a blob or a tree
    public string? Version { get; set; } // Not all services will support a version
    public bool? Hidden { get; set; }
    public StorageLinks? Links { get; set; }

    // Potential properties:
    // ACL information
    // LastAccessedDate
    // StorageClass
    // ETag
    // Checksum or Hash
    // MIME Type
    // Retention Policy
    // Expiry Date
    // Version History (alredy coverd in metadata)
    // Custom Tags or simple Metadata
    // Content Encoding
    // ItemCount
    // Aggregate Size
    // Parent Path
    // Depth/Level
    // Replication Status
    // Encryption Information
}

public class StorageLinks
{
    public InteractionLink? Download { get; set; }
}

public class InteractionLink
{
    public required string Url { get; set; }  // The pre-signed URL
    public DateTime? Expiry { get; set; }    // Optional expiry timestamp
    public CoscineHttpMethod? Method { get; set; }     // HTTP method (e.g., GET, PUT, DELETE)
}