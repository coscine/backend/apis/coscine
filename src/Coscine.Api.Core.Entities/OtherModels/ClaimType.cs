namespace Coscine.Api.Core.Entities.OtherModels;

public static class ClaimType
{
    /// <summary>
    /// Used for the VopersonExternalAffiliations from the RegApp. 
    /// </summary>
    public const string Affiliation = nameof(Affiliation);
}