﻿namespace Coscine.Api.Core.Entities.OtherModels;

public class FileTree : Tree
{
    public string Directory { get; set; } = null!;
    public string Name { get; set; } = null!;
    public string? Extension { get; set; }
    public long Size { get; set; }
    public DateTime? CreationDate { get; set; }
    public DateTime? ChangeDate { get; set; }
    public bool Hidden { get; set; }
    public StorageLinks? Links {get; set;}
}