﻿namespace Coscine.Api.Core.Entities.OtherModels;

// Manually written for RDF and Virtuoso

public partial class DefaultProjectQuota
{
    public string ResourceType { get; set; } = null!;
    public int DefaultQuota { get; set; }
    public int DefaultMaxQuota { get; set; }
}