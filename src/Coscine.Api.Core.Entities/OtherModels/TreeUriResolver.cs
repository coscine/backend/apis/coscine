﻿using System.Text.RegularExpressions;
using System.Web;

namespace Coscine.Api.Core.Entities.OtherModels;

/// <summary>
/// Helper entity used for extracting the relevant components from a Coscine URI.
/// </summary>
/// <remarks>
/// See: <a href="https://git.rwth-aachen.de/groups/coscine/-/epics/38">Metadata@Coscine</a> EPIC on GitLab.
/// </remarks>
public partial class TreeUriResolver
{
    /// <summary>
    /// Identifier of the tree graph.
    /// </summary>
    /// <remarks>
    /// Does not contain a trailing slash '<c>/</c>'.
    /// </remarks>
    public Uri GraphId { get; set; }

    /// <summary>
    /// The <b>URL decoded</b> tree graph data path (file and folder).
    /// </summary>
    /// <remarks>
    /// Does not start nor end with a slash '<c>/</c>'.
    /// </remarks>
    public string Path { get; set; }

    /// <summary>
    /// The type of the tree graph, e.g., '<c>data</c>' or '<c>metadata</c>'.
    /// </summary>
    public string Type { get; set; }

    /// <summary>
    /// The version of the tree graph.
    /// </summary>
    public long Version { get; set; }

    /// <summary>
    /// The resource identifier containing the tree graph.
    /// </summary>
    public Guid ResourceId { get; set; }

    /// <summary>
    /// Determines if given path is for a folder or file.
    /// </summary>
    /// <remarks>If path ends with a slash '<c>/</c>', it is a folder.</remarks>
    public bool IsFolder => Path.EndsWith('/');

    [GeneratedRegex("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}")]
    private static partial Regex GuidRegex();

    [GeneratedRegex("type=([^&]*)")]
    private static partial Regex TypeRegex();

    [GeneratedRegex("version=(\\d+)")]
    private static partial Regex VersionRegex();

    public TreeUriResolver(Uri uri)
    {
        var absoluteUri = uri.AbsoluteUri;

        // Extract Resource Id as Guid
        var guidMatch = GuidRegex().Match(absoluteUri);
        ResourceId = new Guid(guidMatch.Value);

        // Extract Version as long
        var versionMatch = VersionRegex().Match(absoluteUri);
        Version = long.Parse(versionMatch.Groups[1].Value);

        // Extract Type
        var typeMatch = TypeRegex().Match(absoluteUri);
        Type = typeMatch.Groups[1].Value;

        GraphId = CreateGraphUri(uri);

        // Extract Path as string (everything after the ResourceId)
        var pathStartIndex =
            GraphId.AbsoluteUri.IndexOf(ResourceId.ToString(), StringComparison.Ordinal) + ResourceId.ToString().Length + 1; // + 1 is '/'
        Path = HttpUtility.UrlDecode(GraphId.AbsoluteUri[pathStartIndex..]); // Will never start or end with a '/'
    }

    /// <summary>
    /// Extracts the general graph name from the given URI. The general graph name is the URL without query parameters and <c>/@type=</c>.
    /// </summary>
    /// <remarks>There is no trailing slash '<c>/</c>' at the end of the returned URI.</remarks>
    public static Uri CreateGraphUri(Uri uri)
        => new(uri.AbsoluteUri[..uri.AbsoluteUri.LastIndexOf('/')]); // Without trailing '/'
}
