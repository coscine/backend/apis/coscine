﻿namespace Coscine.Api.Core.Entities.OtherModels;

public class DeployedGraph
{
    public Uri Uri { get; set; } = null!;
    public IEnumerable<string> FileHashes { get; set; } = [];
}
