﻿namespace Coscine.Api.Core.Entities.OtherModels;

public class VocabularyInstance
{
    /// <summary>
    /// The name of the graph, which contains the vocabulary.
    /// </summary>
    /// <remarks>
    /// See <c>@base</c> URI from vocabulary definitions.
    /// </remarks>
    public Uri GraphUri { get; set; } = null!;

    /// <summary>
    /// URI of the instance.
    /// </summary>
    public Uri InstanceUri { get; set; } = null!;

    /// <summary>
    /// Type of the instance.
    /// </summary>
    /// <remarks>
    /// NOTE: <c>a</c> is alias for <c>rdf:type</c>.
    /// </remarks>
    public Uri? TypeUri { get; set; }

    /// <summary>
    /// The direct parent class.
    /// </summary>
    /// <remarks>
    /// See the <c>rdfs:subClassOf</c> property.
    /// </remarks>
    public Uri? SubClassOfUri { get; set; }

    public string? DisplayName { get; set; }

    public string? Description { get; set; }
}