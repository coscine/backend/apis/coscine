﻿namespace Coscine.Api.Core.Entities.OtherModels;

public class Provenance
{
    public required Uri Id { get; init; }
    public DateTime? GeneratedAt { get; init; }
    public required IEnumerable<Uri> WasRevisionOf { get; init; }
    public required IEnumerable<Variant> Variants { get; init; }
    public Uri? WasInvalidatedBy { get; init; }
    public string? MetadataExtractorVersion { get; init; }
    public decimal? SimilarityToLastVersion { get; init; }
    public HashParameters? HashParameters { get; init; }

    /// <summary>
    /// Represents the variants of this specific metadata tree.
    /// </summary>
    /// <param name="GraphName">Name of the graph.</param>
    /// <param name="Similarity">Similarity value 0-1</param>
    public record Variant(Uri GraphName, double Similarity);
}
