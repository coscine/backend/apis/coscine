﻿namespace Coscine.Api.Core.Entities.OtherModels;

public class Handle
{
    public Pid? Pid { get; set; }

    public IEnumerable<HandleValue> Values { get; set; } = [];
}