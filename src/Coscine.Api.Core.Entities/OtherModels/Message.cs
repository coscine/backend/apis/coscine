using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Entities.OtherModels;

/// <summary>
/// This class represents a message with its significant properties.
/// </summary>
public partial class Message
{
    /// <summary>
    /// ID of the message.
    /// </summary>
    public string Id => GenerateConsistentId();

    /// <summary>
    /// Message body containing multiple languages as key-value pairs. 
    /// The key is the ISO 639-1 Alpha-2 two-letter language code, and the value is the message in that language.
    /// </summary>
    /// <example>
    /// {
    ///    "de": "Deutscher Text",
    ///    "en": "English text"
    ///    ...
    /// }
    /// </example>
    public Dictionary<string, string> Body { get; set; } = [];

    /// <summary>
    /// The message type or classification (e.g. information, warning, error).
    /// </summary>
    /// <remarks>Default is <see cref="MessageType.Information"/>.</remarks>
    public MessageType Type { get; set; } = MessageType.Information;

    /// <summary>
    /// Message title.
    /// </summary>
    /// <remarks>The title is optional.</remarks>
    public string? Title { get; set; }

    /// <summary>
    /// Message start.
    /// </summary>
    public DateTimeOffset? StartDate { get; set; }

    /// <summary>
    /// Message end.
    /// </summary>
    public DateTimeOffset? EndDate { get; set; }

    /// <summary>
    /// Generates a consistent ID based on the Message properties.
    /// </summary>
    /// <returns>A unique ID for the Message.</returns>
    private string GenerateConsistentId()
    {
        // Serialize relevant properties to a JSON string using the cached JsonSerializerOptions
        var serializedData = JsonSerializer.Serialize(new
        {
            Body,
            Type,
            Title,
            StartDate,
            EndDate
        }, _messsageJsonSerializerOptions);

        // Compute hash from the serialized string
        var hashBytes = SHA256.HashData(Encoding.UTF8.GetBytes(serializedData));
        return BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
    }

    // Cache and reuse a single instance of JsonSerializerOptions
    private static readonly JsonSerializerOptions _messsageJsonSerializerOptions = new()
    { 
        WriteIndented = false, 
        DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull 
    };
}