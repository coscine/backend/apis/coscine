﻿using System.Text.Json.Serialization;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Helpers;

namespace Coscine.Api.Core.Entities.OtherModels;

/// <summary>
/// Represents an NOC-Portal ticket with various details.
/// </summary>
public class NocTicket
{
    /// <summary>
    /// Gets or sets the ID of the ticket.
    /// </summary>
    [JsonPropertyName("id")]
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the title of the ticket.
    /// </summary>
    [JsonPropertyName("title")]
    public string Title { get; set; } = null!;

    /// <summary>
    /// Gets or sets the short message of the ticket.
    /// </summary>
    [JsonPropertyName("short_message")]
    public string ShortMessage { get; set; } = null!;

    /// <summary>
    /// Gets or sets the message of the ticket.
    /// </summary>
    [JsonPropertyName("message")]
    public string Message { get; set; } = null!;

    /// <summary>
    /// Gets or sets the technical message of the ticket.
    /// </summary>
    [JsonPropertyName("tec_message")]
    public string TecMessage { get; set; } = null!;

    /// <summary>
    /// Gets or sets the status of the ticket.
    /// </summary>
    [JsonPropertyName("status")]
    [JsonConverter(typeof(MessageTypeConverter))]
    public MessageType Type { get; set; }

    /// <summary>
    /// Gets or sets the start date and time of the ticket.
    /// </summary>
    [JsonPropertyName("start_at")]
    public DateTimeOffset StartAt { get; set; }

    /// <summary>
    /// Gets or sets the end date and time of the ticket.
    /// </summary>
    [JsonPropertyName("end_at")]
    public DateTimeOffset EndAt { get; set; }

    /// <summary>
    /// Gets or sets the direct href of the ticket.
    /// </summary>
    [JsonIgnore]
    public Uri Href { get; set; } = null!;

    /// <summary>
    /// Gets or sets the follow-up details for the ticket.
    /// </summary>
    [JsonPropertyName("ticket_followups")]
    public ICollection<NocTicketFollowup> TicketFollowups { get; set; } = [];
}
