﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Entities.OtherModels;
public class Quota
{
    public double Value { get; set; }

    public QuotaUnit Unit { get; set; }
}
