﻿using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Entities;

/// <summary>
/// Provides a contract for managing and manipulating Coscine resources and their resource types.
/// This includes operations like CRUD (Create, Read, Update, Delete), as well as some specialized functions.
/// </summary>
/// <remarks>
/// NOTE: Consider moving this interface to another namespace (e.g. <c>...Repository.Contracts</c>).
/// </remarks>
public interface IBaseResourceTypeDefinition
{
    /// <summary>
    /// Configuration specific to the resource type.
    /// </summary>
    public ResourceTypeConfiguration ResourceTypeConfiguration { get; set; }

    /// <summary>
    /// Retrieves the relevant information about the resource type for frontend use.
    /// </summary>
    /// <returns>A task representing the asynchronous operation which returns the <see cref="ResourceTypeInformation"/> object.</returns>
    public abstract Task<ResourceTypeInformation> GetResourceTypeInformation();

    public abstract Task<List<ResourceEntry>> ListEntries(string id, string prefix, Dictionary<string, string>? options = null);

    public abstract Task<ResourceEntry?> GetEntry(string id, string key, Dictionary<string, string>? options = null);

    public abstract Task StoreEntry(string id, string key, Stream body, Dictionary<string, string>? options = null);

    public abstract Task RenameEntry(string id, string keyOld, string keyNew, Dictionary<string, string>? options = null);

    public abstract Task DeleteEntry(string id, string key, Dictionary<string, string>? options = null);

    public abstract Task<Stream?> LoadEntry(string id, string key, Dictionary<string, string>? options = null);

    public abstract Task AddPrefix(string id, string prefix, Dictionary<string, string>? options = null);

    public abstract Task CreateResource(string id, long? quota = null, Dictionary<string, string>? options = null);

    public abstract Task UpdateResource(string id, Dictionary<string, string>? options = null);

    public abstract Task<bool> IsResourceCreated(string id, Dictionary<string, string>? options = null);

    public abstract Task DeleteResource(string id, Dictionary<string, string>? options = null);

    public abstract Task SetResourceQuota(string id, int quota, Dictionary<string, string>? options = null);

    public abstract Task<long> GetResourceQuotaReserved(string id, Dictionary<string, string>? options = null);

    public abstract Task<long> GetResourceQuotaUsed(string id, Dictionary<string, string>? options = null);

    public abstract Task<Uri> GetPreSignedUrl(string id, string key, CoscineHttpMethod httpVerb, Dictionary<string, string>? options = null);

    public abstract Task SetResourceReadOnly(string id, bool status, Dictionary<string, string>? options = null);
}