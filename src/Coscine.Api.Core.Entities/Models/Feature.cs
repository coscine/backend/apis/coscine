﻿namespace Coscine.Api.Core.Entities.Models;

public partial class Feature
{
    public Guid Id { get; set; }

    public string SharepointId { get; set; } = null!;

    public string DisplaynameEn { get; set; } = null!;

    public string DisplaynameDe { get; set; } = null!;

    public virtual ICollection<ActivatedFeature> ActivatedFeatures { get; } = new List<ActivatedFeature>();
}