﻿namespace Coscine.Api.Core.Entities.Models;

public partial class Role
{
    public Guid Id { get; set; }

    public string DisplayName { get; set; } = null!;

    public string Description { get; set; } = null!;

    public virtual ICollection<Invitation> Invitations { get; } = new List<Invitation>();

    public virtual ICollection<ProjectRole> ProjectRoles { get; } = new List<ProjectRole>();
}