﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Coscine.Api.Core.Entities.Models;

public partial class User
{
    public Guid Id { get; set; }

    public string? EmailAddress { get; set; }

    public string DisplayName { get; set; } = null!;

    public string? Givenname { get; set; }

    public string? Surname { get; set; }

    public string? Entitlement { get; set; }

    public string? Organization { get; set; }

    public Guid? TitleId { get; set; }

    public Guid? LanguageId { get; set; }

    public DateTime? LatestActivity { get; set; }

    public DateTime? DeletedAt { get; set; }
    
    [NotMapped]
    public bool IsDeleted => DeletedAt != null;

    public virtual ICollection<ActivityLog> ActivityLogs { get; } = [];
    
    public virtual ICollection<ApiToken> ApiTokens { get; } = [];

    public virtual ICollection<ContactChange> ContactChanges { get; } = [];

    public virtual ICollection<ExternalId> ExternalIds { get; } = [];

    public virtual ICollection<GroupMembership> GroupMemberships { get; } = [];

    public virtual ICollection<Invitation> Invitations { get; } = [];

    public virtual Language? Language { get; set; }

    public virtual ICollection<ProjectRole> ProjectRoles { get; } = [];

    public virtual ICollection<ProjectPublicationRequest> PublicationRequests { get; } = [];

    public virtual Title? Title { get; set; }

    public virtual ICollection<Tosaccepted> Tosaccepteds { get; } = [];

    public virtual ICollection<UserDiscipline> UserDisciplines { get; } = [];
}