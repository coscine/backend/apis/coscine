﻿namespace Coscine.Api.Core.Entities.Models;

public partial class Log
{
    public Guid Id { get; set; }

    public DateTime? ServerTimestamp { get; set; }

    public DateTime? ClientTimestamp { get; set; }

    public string? LogLevel { get; set; }

    public string? Message { get; set; }

    public string? Stacktrace { get; set; }

    public Guid? UserId { get; set; }

    public string? Uri { get; set; }

    public string? Server { get; set; }

    public Guid? CorrolationId { get; set; }

    public string? Status { get; set; }

    public string? Source { get; set; }
}