﻿namespace Coscine.Api.Core.Entities.Models;

public partial class ApiToken
{
    public Guid Id { get; set; }

    public string Name { get; set; } = null!;

    public Guid UserId { get; set; }

    public DateTime IssuedAt { get; set; }

    public DateTime Expiration { get; set; }

    public virtual User User { get; set; } = null!;
}