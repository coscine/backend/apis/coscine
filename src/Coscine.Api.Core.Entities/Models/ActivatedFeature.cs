﻿namespace Coscine.Api.Core.Entities.Models;

public partial class ActivatedFeature
{
    public Guid Id { get; set; }

    public Guid ProjectId { get; set; }

    public Guid FeatureId { get; set; }

    public virtual Feature Feature { get; set; } = null!;

    public virtual Project Project { get; set; } = null!;
}