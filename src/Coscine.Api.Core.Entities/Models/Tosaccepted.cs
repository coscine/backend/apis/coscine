﻿namespace Coscine.Api.Core.Entities.Models;

public partial class Tosaccepted
{
    public Guid RelationId { get; set; }

    public Guid UserId { get; set; }

    public string Version { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}