﻿namespace Coscine.Api.Core.Entities.Models;

public partial class Resource
{
    public Guid Id { get; set; }

    public Guid TypeId { get; set; }

    public string? ResourceName { get; set; }

    public string? DisplayName { get; set; }

    public Guid? VisibilityId { get; set; }

    public Guid? LicenseId { get; set; }

    public string? Keywords { get; set; }

    public string? UsageRights { get; set; }

    public Guid? ResourceTypeOptionId { get; set; }

    public string? Description { get; set; }

    public string? ApplicationProfile { get; set; }

    public string? FixedValues { get; set; }

    public Guid? Creator { get; set; }

    public string Archived { get; set; } = null!;

    public bool Deleted { get; set; }

    public DateTime? DateCreated { get; set; }

    public bool MetadataLocalCopy { get; set; }

    public bool MaintenanceMode { get; set; }

    public virtual License? License { get; set; }

    public virtual ICollection<MetadataExtraction> MetadataExtractions { get; } = [];

    public virtual ICollection<ProjectResource> ProjectResources { get; } = [];

    public virtual ICollection<ProjectPublicationRequest> PublicationRequests { get; } = [];

    public virtual ICollection<ResourceDiscipline> ResourceDisciplines { get; } = [];

    public virtual ResourceType Type { get; set; } = null!;

    public virtual Visibility? Visibility { get; set; }
}