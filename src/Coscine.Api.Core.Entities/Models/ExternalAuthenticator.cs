﻿namespace Coscine.Api.Core.Entities.Models;

public partial class ExternalAuthenticator
{
    public Guid Id { get; set; }

    public string DisplayName { get; set; } = null!;

    public virtual ICollection<ExternalId> ExternalIds { get; } = new List<ExternalId>();
}