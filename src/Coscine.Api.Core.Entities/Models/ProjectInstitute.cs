﻿namespace Coscine.Api.Core.Entities.Models;

public partial class ProjectInstitute
{
    public Guid RelationId { get; set; }

    public Guid ProjectId { get; set; }

    public string OrganizationUrl { get; set; } = null!;

    public bool Responsible { get; set; }

    public virtual Project Project { get; set; } = null!;
}