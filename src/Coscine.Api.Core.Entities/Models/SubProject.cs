﻿namespace Coscine.Api.Core.Entities.Models;

public partial class SubProject
{
    public Guid RelationId { get; set; }

    public Guid ProjectId { get; set; }

    public Guid SubProjectId { get; set; }

    public virtual Project Project { get; set; } = null!;

    public virtual Project SubProjectNavigation { get; set; } = null!;
}