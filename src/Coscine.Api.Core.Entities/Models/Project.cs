﻿namespace Coscine.Api.Core.Entities.Models;

public partial class Project
{
    public Guid Id { get; set; }

    public string ProjectName { get; set; } = null!;

    public string Description { get; set; } = null!;

    public DateTime StartDate { get; set; }

    public DateTime EndDate { get; set; }

    public string? Keywords { get; set; }

    public string? DisplayName { get; set; }

    public string? PrincipleInvestigators { get; set; }

    public string? GrantId { get; set; }

    public Guid? VisibilityId { get; set; }

    public bool Deleted { get; set; }

    public string Slug { get; set; } = null!;

    public Guid? Creator { get; set; }

    public DateTime? DateCreated { get; set; }

    public virtual ICollection<ActivatedFeature> ActivatedFeatures { get; } = [];

    public virtual ICollection<Invitation> Invitations { get; } = [];

    public virtual ICollection<ProjectDiscipline> ProjectDisciplines { get; } = [];

    public virtual ICollection<ProjectInstitute> ProjectInstitutes { get; } = [];

    public virtual ICollection<ProjectPublicationRequest> PublicationRequests { get; } = [];

    public virtual ICollection<ProjectQuota> ProjectQuota { get; } = [];

    public virtual ICollection<ProjectResource> ProjectResources { get; } = [];

    public virtual ICollection<ProjectRole> ProjectRoles { get; } = [];

    public virtual ICollection<SubProject> SubProjectProjects { get; } = [];

    public virtual ICollection<SubProject> SubProjectSubProjectNavigations { get; } = [];

    public virtual Visibility? Visibility { get; set; }
}