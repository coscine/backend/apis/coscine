﻿namespace Coscine.Api.Core.Entities.Models;

public partial class Discipline
{
    public Guid Id { get; set; }

    public string Url { get; set; } = null!;

    public string? DisplayNameDe { get; set; }

    public string? DisplayNameEn { get; set; }

    public virtual ICollection<ProjectDiscipline> ProjectDisciplines { get; } = new List<ProjectDiscipline>();

    public virtual ICollection<ResourceDiscipline> ResourceDisciplines { get; } = new List<ResourceDiscipline>();

    public virtual ICollection<UserDiscipline> UserDisciplines { get; } = new List<UserDiscipline>();
}