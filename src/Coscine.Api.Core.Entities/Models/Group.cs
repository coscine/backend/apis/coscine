﻿namespace Coscine.Api.Core.Entities.Models;

public partial class Group
{
    public Guid Id { get; set; }

    public string DisplayName { get; set; } = null!;

    public virtual ICollection<GroupMembership> GroupMemberships { get; } = new List<GroupMembership>();
}