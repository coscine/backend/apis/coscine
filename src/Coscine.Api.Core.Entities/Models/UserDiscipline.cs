﻿namespace Coscine.Api.Core.Entities.Models;

public partial class UserDiscipline
{
    public Guid RelationId { get; set; }

    public Guid DisciplineId { get; set; }

    public Guid UserId { get; set; }

    public virtual Discipline Discipline { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}