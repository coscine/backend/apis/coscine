﻿namespace Coscine.Api.Core.Entities.Models;

public partial class RdsResourceType
{
    public Guid Id { get; set; }

    public string BucketName { get; set; } = null!;

    public string AccessKey { get; set; } = null!;

    public string SecretKey { get; set; } = null!;

    public string Endpoint { get; set; } = null!;
}