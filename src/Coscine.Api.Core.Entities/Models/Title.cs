﻿namespace Coscine.Api.Core.Entities.Models;

public partial class Title
{
    public Guid Id { get; set; }

    public string DisplayName { get; set; } = null!;

    public virtual ICollection<User> Users { get; } = new List<User>();
}