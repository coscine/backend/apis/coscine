﻿namespace Coscine.Api.Core.Entities.Models;

public partial class License
{
    public Guid Id { get; set; }

    public string? DisplayName { get; set; }

    public Uri? Url { get; set; }

    public virtual ICollection<Resource> Resources { get; } = new List<Resource>();
}