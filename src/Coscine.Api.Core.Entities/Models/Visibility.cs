﻿namespace Coscine.Api.Core.Entities.Models;

public partial class Visibility
{
    public Guid Id { get; set; }

    public string DisplayName { get; set; } = null!;

    public virtual ICollection<Project> Projects { get; } = new List<Project>();

    public virtual ICollection<Resource> Resources { get; } = new List<Resource>();
}