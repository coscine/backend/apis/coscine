﻿namespace Coscine.Api.Core.Entities.Models;

public partial class ProjectResource
{
    public Guid RelationId { get; set; }

    public Guid ResourceId { get; set; }

    public Guid ProjectId { get; set; }

    public virtual Project Project { get; set; } = null!;

    public virtual Resource Resource { get; set; } = null!;
}