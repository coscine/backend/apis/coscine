﻿namespace Coscine.Api.Core.Entities.Models;

public partial class ActivityLog
{
    public Guid Id { get; set; }

    public string ApiPath { get; set; } = null!;

    public string HttpAction { get; set; } = null!;

    public string? ControllerName { get; set; }

    public string? ActionName { get; set; }

    public Guid UserId { get; set; }

    public DateTime ActivityTimestamp { get; set; }

    public virtual User User { get; set; } = null!;
}