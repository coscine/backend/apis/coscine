﻿namespace Coscine.Api.Core.Entities.Models;

public partial class ProjectDiscipline
{
    public Guid RelationId { get; set; }

    public Guid DisciplineId { get; set; }

    public Guid ProjectId { get; set; }

    public virtual Discipline Discipline { get; set; } = null!;

    public virtual Project Project { get; set; } = null!;
}