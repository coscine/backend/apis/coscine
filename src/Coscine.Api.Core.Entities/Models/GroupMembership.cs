﻿namespace Coscine.Api.Core.Entities.Models;

public partial class GroupMembership
{
    public Guid RelationId { get; set; }

    public Guid GroupId { get; set; }

    public Guid UserId { get; set; }

    public virtual Group Group { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}