﻿namespace Coscine.Api.Core.Entities.Models;

public partial class ResourceType
{
    public Guid Id { get; set; }

    public string DisplayName { get; set; } = null!;

    public string? Type { get; set; }

    public string? SpecificType { get; set; }

    public virtual ICollection<ProjectQuota> ProjectQuota { get; } = new List<ProjectQuota>();

    public virtual ICollection<Resource> Resources { get; } = new List<Resource>();
}