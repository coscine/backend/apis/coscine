﻿namespace Coscine.Api.Core.Entities.Models;

public partial class ExternalId
{
    public Guid RelationId { get; set; }

    public Guid UserId { get; set; }

    public Guid ExternalAuthenticatorId { get; set; }

    public string ExternalId1 { get; set; } = null!;

    public string? Organization { get; set; }

    public virtual ExternalAuthenticator ExternalAuthenticator { get; set; } = null!;

    public virtual User User { get; set; } = null!;
}