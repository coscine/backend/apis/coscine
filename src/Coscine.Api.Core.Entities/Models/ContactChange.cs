﻿namespace Coscine.Api.Core.Entities.Models;

public partial class ContactChange
{
    public Guid RelationId { get; set; }

    public Guid UserId { get; set; }

    public string NewEmail { get; set; } = null!;

    public DateTime? EditDate { get; set; }

    public Guid ConfirmationToken { get; set; }

    public virtual User User { get; set; } = null!;
}