﻿using Coscine.Api.Core.Entities.Helpers;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Entities.Models.QuadStore;

/// <summary>
/// Represents a metadata entry within the metadata catalog.
/// </summary>
[RdfClass("http://www.w3.org/ns/prov#Entity")]
public class MetadataEntity(Uri uri) : Semiodesk.Trinity.Resource(uri)
{
    /// <summary>
    /// Gets or sets the date and time when the metadata was generated.
    /// </summary>
    [RdfProperty("http://www.w3.org/ns/prov#generatedAtTime")]
    public DateTime? GeneratedAt { get; set; }

    /// <summary>
    /// Gets or sets the URI of the entity that invalidates this one.
    /// </summary>
    /// <remarks>Corresponds to the <see cref="MetadataEntity"/> class.</remarks>
    [RdfProperty("http://www.w3.org/ns/prov#wasInvalidatedBy")]
    public Uri? InvalidatedBy { get; set; }

    /// <summary>
    /// Gets or sets the URI of the entity that this one is a revision of.
    /// </summary>
    /// <remarks>Corresponds to the <see cref="MetadataEntity"/> class.</remarks>
    [RdfProperty("http://www.w3.org/ns/prov#wasRevisionOf")]
    public List<Uri> WasRevisionOf { get; set; } = [];

    /// <summary>
    /// Gets or sets the URI of the entity that this one is a revision of.
    /// </summary>
    /// <remarks>Corresponds to the <see cref="MetadataEntity"/> class.</remarks>
    [RdfProperty("https://purl.org/coscine/terms/metadatatracker#hasVariant")]
    public List<Uri> HasVariant { get; set; } = [];

    /// <summary>
    /// Gets or sets the similarity to the last version.
    /// String because of compatibility issues with Virtuoso.
    /// </summary>
    /// <remarks>Corresponds to the <see cref="MetadataEntity"/> class.</remarks>
    [RdfProperty("https://purl.org/coscine/terms/metadatatracker#similarityToLastVersion")]
    public string? SimilarityToLastVersion { get; set; }

    /// <summary>
    /// Gets or sets the URI of the extracted data related to this entity.
    /// </summary>
    /// <remarks>Corresponds to the <see cref="MetadataExtractedEntity"/> class.</remarks>
    [RdfProperty("https://purl.org/coscine/terms/metatadataextraction#extracted")]
    public Uri? ExtractedId { get; set; }

    /// <summary>
    /// Gets or sets the URI of the related data entity.
    /// </summary>
    /// <remarks>Corresponds to the <see cref="RawDataEntity"/> class.</remarks>
    [RdfProperty("http://purl.org/fdp/fdp-o#isMetadataOf")]
    public Uri? RawDataId { get; set; }

    /// <summary>
    /// Gets or sets the Version of this entity.
    /// </summary>
    [RdfProperty("http://www.loc.gov/premis/rdf/v3/version")]
    public long Version { get; set; }

    /// <summary>
    /// Gets the Computed Version of this entity.
    /// </summary>
    /// <remarks>Can't be used for mapping</remarks>
    public int ComputedVersion => RdfUrlHelper.GetVersionNumber(Uri);
}
