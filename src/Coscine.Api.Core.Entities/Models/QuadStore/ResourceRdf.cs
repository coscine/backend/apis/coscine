﻿// Ignore Spelling: Rdf

using Coscine.Api.Core.Entities.Ontologies;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Entities.Models.QuadStore;

[RdfClass(DCAT.Catalog)]
public class ResourceRdf(Uri uri) : Semiodesk.Trinity.Resource(uri)
{
    [RdfProperty(DCAT.service)]
    public Uri? TypeId { get; set; }

    [RdfProperty(DCTERMS.title)]
    public string? ResourceName { get; set; }

    [RdfProperty(DCTERMS.alternative)]
    public string? DisplayName { get; set; }

    [RdfProperty("https://purl.org/coscine/terms/resource#visibility")]
    public Uri? VisibilityId { get; set; }

    [RdfProperty(DCTERMS.license)]
    public Uri? LicenseId { get; set; }

    [RdfProperty(DCTERMS.subject)]
    public List<string> Keywords { get; set; } = [];

    [RdfProperty(DCTERMS.rights)]
    public string? UsageRights { get; set; }

    [RdfProperty(DCTERMS.description)]
    public string? Description { get; set; }

    [RdfProperty(DCTERMS.conformsTo)]
    public Uri? ApplicationProfile { get; set; }

    [RdfProperty("https://purl.org/coscine/terms/resource#fixedValues")]
    public string? FixedValues { get; set; }

    [RdfProperty(DCTERMS.creator)]
    public Uri? Creator { get; set; }

    [RdfProperty("https://purl.org/coscine/terms/resource#archived")]
    public bool? Archived { get; set; }

    [RdfProperty(DCAT.catalog)]
    public List<Uri> DataCatalogIds { get; set; } = [];
}
