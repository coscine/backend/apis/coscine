﻿using Semiodesk.Trinity;

namespace Coscine.Api.Core.Entities.Models.QuadStore;

/// <summary>
/// Represents a variant within the metadata catalog.
/// </summary>
[RdfClass("https://purl.org/coscine/terms/metadatatracker#Variant")]
public class Variant(Uri uri) : Semiodesk.Trinity.Resource(uri)
{
    /// <summary>
    /// Gets or sets the URI of the entity that it is a variant of.
    /// </summary>
    /// <remarks>Corresponds to the <see cref="Variant"/> class.</remarks>
    [RdfProperty("https://purl.org/coscine/terms/metadatatracker#isVariantOf")]
    public Uri? VariantOf { get; set; }

    /// <summary>
    /// Gets or sets the similarity value.
    /// String because of compatibility issues with Virtuoso.
    /// </summary>
    /// <remarks>Corresponds to the <see cref="Variant"/> class.</remarks>
    [RdfProperty("https://purl.org/coscine/terms/metadatatracker#similarity")]
    public string? Similarity { get; set; }
}
