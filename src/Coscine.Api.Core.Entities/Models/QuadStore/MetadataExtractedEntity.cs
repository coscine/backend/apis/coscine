﻿using Semiodesk.Trinity;

namespace Coscine.Api.Core.Entities.Models.QuadStore;

/// <summary>
/// Represents extracted metadata information.
/// </summary>
[RdfClass("https://purl.org/coscine/terms/metatadataextraction#Entity")]
public class MetadataExtractedEntity(Uri uri) : Semiodesk.Trinity.Resource(uri)
{
    /// <summary>
    /// Gets or sets the version of the extracted metadata.
    /// </summary>
    [RdfProperty("https://purl.org/coscine/terms/metatadataextraction#version")]
    public string? ExtractedVersion { get; set; }

    /// <summary>
    /// Gets or sets the date and time when the metadata was generated.
    /// </summary>
    [RdfProperty("http://www.w3.org/ns/prov#generatedAtTime")]
    public DateTime? GeneratedAt { get; set; }
}
