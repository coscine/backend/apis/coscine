﻿using Semiodesk.Trinity;

namespace Coscine.Api.Core.Entities.Models.QuadStore;

[RdfClass("https://purl.org/coscine/terms/metatadataextraction#Entity")]
public class RawDataExtractedEntity(Uri uri) : Semiodesk.Trinity.Resource(uri)
{
    [RdfProperty("https://purl.org/coscine/terms/metatadataextraction#version")]
    public string? ExtractedVersion { get; set; }

    [RdfProperty("http://www.w3.org/ns/prov#generatedAtTime")]
    public DateTime? GeneratedAt { get; set; }
}
