﻿using Semiodesk.Trinity;

namespace Coscine.Api.Core.Entities.Models.QuadStore;

[RdfClass("http://purl.obolibrary.org/obo/NCIT_C42883")]
public class FileEntity(Uri uri) : Semiodesk.Trinity.Resource(uri)
{
    /// <summary>
    /// Gets or sets the path of the file.
    /// </summary>
    [RdfProperty("https://w3id.org/idsa/core/path")]
    public string? Path { get; set; }
}
