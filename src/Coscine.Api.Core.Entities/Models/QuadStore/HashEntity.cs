﻿using Semiodesk.Trinity;

namespace Coscine.Api.Core.Entities.Models.QuadStore;

[RdfClass("http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#hashType")]
public class HashEntity(Uri uri) : Semiodesk.Trinity.Resource(uri)
{
    [RdfProperty("http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#hashFunction")]
    public string? HashFunction { get; set; }

    [RdfProperty("http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#hashValue")]
    public string? HashValue { get; set; }
}
