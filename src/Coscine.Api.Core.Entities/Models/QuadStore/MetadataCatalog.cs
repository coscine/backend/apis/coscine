﻿using Coscine.Api.Core.Entities.Ontologies;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Entities.Models.QuadStore;

/// <summary>
/// Represents a catalog of metadata entries within the data catalog.
/// </summary>
[RdfClass(DCAT.Catalog)]
public class MetadataCatalog(Uri uri) : Semiodesk.Trinity.Resource(uri)
{
    /// <summary>
    /// Gets or sets the list of entity URIs in the metadata catalog.
    /// </summary>
    /// <remarks>Corresponds to the <see cref="MetadataEntity"/> class.</remarks>
    [RdfProperty("http://purl.org/fdp/fdp-o#hasMetadata")]
    public List<Uri> EntityIds { get; set; } = [];
}
