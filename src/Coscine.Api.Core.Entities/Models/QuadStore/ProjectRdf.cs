﻿// Ignore Spelling: Rdf

using Coscine.Api.Core.Entities.Ontologies;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Entities.Models.QuadStore;

[RdfClass(DCAT.Catalog)]
[RdfClass(ORG.OrganizationalCollaboration)]
[RdfClass(VCARD.Group)]
[RdfClass(LDP.Container)]
public class ProjectRdf(Uri uri) : Semiodesk.Trinity.Resource(uri)
{
    [RdfProperty(DCTERMS.title)]
    public string? ProjectName { get; set; }

    [RdfProperty(DCTERMS.description)]
    public string? Description { get; set; }

    [RdfProperty(DCTERMS.created)]
    public DateTime? Created { get; set; }

    [RdfProperty("http://purl.org/dc/terms/startDate")]
    public DateTime? StartDate { get; set; }

    [RdfProperty("http://purl.org/dc/terms/endDate")]
    public DateTime? EndDate { get; set; }

    [RdfProperty(DCTERMS.subject)]
    public List<string> Keywords { get; set; } = [];

    [RdfProperty(DCTERMS.alternative)]
    public string? DisplayName { get; set; }

    [RdfProperty(DCTERMS.rightsHolder)]
    public string? PrincipleInvestigators { get; set; }

    [RdfProperty(FOAF.homepage)]
    public Uri? Homepage { get; set; }

    [RdfProperty("http://schema.org/funding")]
    public string? GrantId { get; set; }

    [RdfProperty("https://purl.org/coscine/terms/project#visibility")]
    public Uri? VisibilityId { get; set; }

    [RdfProperty("https://purl.org/coscine/terms/project#deleted")]
    public bool? Deleted { get; set; }

    [RdfProperty("https://purl.org/coscine/terms/project#slug")]
    public string? Slug { get; set; }

    [RdfProperty("http://www.w3.org/ns/org#organization")]
    public List<Uri> OrganizationIds { get; set; } = [];

    [RdfProperty(VCARD.hasMember)]
    public List<Uri> MemberIds { get; set; } = [];

    [RdfProperty(DCAT.catalog)]
    public List<Uri> ResourceIds { get; set; } = [];
}
