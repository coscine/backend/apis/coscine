﻿using Semiodesk.Trinity;

namespace Coscine.Api.Core.Entities.Models.QuadStore;

[RdfClass("http://purl.obolibrary.org/obo/NCIT_C62486")]
public class FolderEntity(Uri uri) : Semiodesk.Trinity.Resource(uri)
{
    /// <summary>
    /// Gets or sets the path of the folder.
    /// </summary>
    [RdfProperty("https://w3id.org/idsa/core/path")]
    public string? Path { get; set; }
}
