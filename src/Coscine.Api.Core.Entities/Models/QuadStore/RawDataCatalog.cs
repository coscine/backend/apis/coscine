﻿using Coscine.Api.Core.Entities.Ontologies;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Entities.Models.QuadStore;

[RdfClass(DCAT.Catalog)]
public class RawDataCatalog(Uri uri) : Semiodesk.Trinity.Resource(uri)
{
    [RdfProperty("http://www.w3.org/ns/ldp#describedBy")]
    public Uri? DescribedBy { get; set; }

    /// <summary>
    /// Gets or sets the list of entity URIs in the metadata catalog.
    /// </summary>
    /// <remarks>Corresponds to the <see cref="RawDataEntity"/> class.</remarks>
    [RdfProperty("http://www.w3.org/ns/dcat#dataset")]
    public List<Uri> EntityIds { get; set; } = [];

    /// <summary>
    /// Gets or sets the list of extracted URIs in the metadata catalog.
    /// </summary>
    /// <remarks>Corresponds to the <see cref="RawDataEntity"/> class.</remarks>
    [RdfProperty("http://www.w3.org/ns/dcat#distribution")]
    public List<Uri> ExtractedIds { get; set; } = [];
}
