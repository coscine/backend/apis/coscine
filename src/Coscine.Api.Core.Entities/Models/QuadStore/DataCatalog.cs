﻿using Coscine.Api.Core.Entities.Ontologies;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Entities.Models.QuadStore;

/// <summary>
/// Represents an entry in the data catalog.
/// </summary>
[RdfClass(DCAT.Catalog)]
[RdfClass(LDP.Container)]
[RdfClass(LDP.BasicContainer)]
[RdfClass(FDP_O.MetadataService)]
public class DataCatalog(Uri uri) : Semiodesk.Trinity.Resource(uri)
{
    /// <summary>
    /// Gets or sets the ID of the associated metadata catalog.
    /// </summary>
    /// <remarks>Corresponds to the <see cref="MetadataCatalog"/> property.</remarks>
    [RdfProperty("http://purl.org/fdp/fdp-o#hasMetadata")]
    public Uri? MetadataCatalogId { get; set; }

    [RdfProperty(DCAT.catalog)]
    protected List<Uri> Catalogs { get; set; } = [];

    /// <summary>
    /// Gets the ID of the associated raw data collection.
    /// </summary>
    /// <remarks>Corresponds to the <see cref="RawDataCatalog"/> property.</remarks>
    public Uri? RawDataCatalogId
    {
        get => Catalogs.Find(x => x != MetadataCatalogId);
    }
}
