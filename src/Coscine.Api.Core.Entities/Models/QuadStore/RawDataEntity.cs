﻿using Coscine.Api.Core.Entities.Helpers;
using Semiodesk.Trinity;

namespace Coscine.Api.Core.Entities.Models.QuadStore;

[RdfClass("http://www.w3.org/ns/prov#Entity")]
public class RawDataEntity(Uri uri) : Semiodesk.Trinity.Resource(uri)
{
    [RdfProperty("http://www.w3.org/ns/prov#generatedAtTime")]
    public DateTime? GeneratedAt { get; set; }

    [RdfProperty("http://www.w3.org/ns/prov#wasInvalidatedBy")]
    public Uri? InvalidatedBy { get; set; }

    [RdfProperty("http://purl.org/fdp/fdp-o#isMetadataOf")]
    public Uri? MetadataOf { get; set; }

    [RdfProperty("http://www.w3.org/ns/prov#wasRevisionOf")]
    public List<Uri> WasRevisionOf { get; set; } = [];

    [RdfProperty("https://purl.org/coscine/terms/metatadataextraction#extracted")]
    public Uri? ExtractedId { get; set; }

    [RdfProperty("http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#hash")]
    public Uri? Hash { get; set; }

    /// <summary>
    /// Gets or sets the Version of this entity.
    /// </summary>
    [RdfProperty("http://www.loc.gov/premis/rdf/v3/version")]
    public long Version { get; set; }

    /// <summary>
    /// Gets the Computed Version of this entity.
    /// </summary>
    /// <remarks>Can't be used for mapping</remarks>
    public int ComputedVersion => RdfUrlHelper.GetVersionNumber(Uri);
}
