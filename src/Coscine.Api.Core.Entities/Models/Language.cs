﻿namespace Coscine.Api.Core.Entities.Models;

public partial class Language
{
    public Guid Id { get; set; }

    public string DisplayName { get; set; } = null!;

    public string Abbreviation { get; set; } = null!;

    public virtual ICollection<User> Users { get; } = new List<User>();
}