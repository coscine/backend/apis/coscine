namespace Coscine.Api.Core.Entities.Models;

public partial class ProjectPublicationRequest
{
    public Guid Id { get; set; }

    public Guid ProjectId { get; set; }

    public Uri PublicationServiceRorId { get; set; } = null!;

    public Guid CreatorId { get; set; }

    public DateTime DateCreated { get; set; }

    public string? Message { get; set; }

    public virtual Project Project { get; set; } = null!;

    public virtual User Creator { get; set; } = null!;

    public virtual ICollection<Resource> Resources { get; set; } = [];
}