﻿namespace Coscine.Api.Core.Entities.Models;

public partial class GitlabResourceType
{
    public Guid Id { get; set; }

    public string Branch { get; set; } = null!;

    public int GitlabProjectId { get; set; }

    public string RepoUrl { get; set; } = null!;

    public string ProjectAccessToken { get; set; } = null!;

    public bool TosAccepted { get; set; }
}