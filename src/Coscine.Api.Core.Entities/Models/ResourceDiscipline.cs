﻿namespace Coscine.Api.Core.Entities.Models;

public partial class ResourceDiscipline
{
    public Guid RelationId { get; set; }

    public Guid DisciplineId { get; set; }

    public Guid ResourceId { get; set; }

    public virtual Discipline Discipline { get; set; } = null!;

    public virtual Resource Resource { get; set; } = null!;
}