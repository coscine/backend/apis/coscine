﻿namespace Coscine.Api.Core.Entities.Models;

public partial class Invitation
{
    public Guid Id { get; set; }

    public Guid Project { get; set; }

    public Guid Issuer { get; set; }

    public Guid Role { get; set; }

    public string InviteeEmail { get; set; } = null!;

    public DateTime Expiration { get; set; }

    public Guid Token { get; set; }

    public virtual User IssuerNavigation { get; set; } = null!;

    public virtual Project ProjectNavigation { get; set; } = null!;

    public virtual Role RoleNavigation { get; set; } = null!;
}