﻿namespace Coscine.Api.Core.Entities.Models;

public partial class RdsS3ResourceType
{
    public Guid Id { get; set; }

    public string BucketName { get; set; } = null!;

    public string AccessKey { get; set; } = null!;

    public string SecretKey { get; set; } = null!;

    public string AccessKeyRead { get; set; } = null!;

    public string SecretKeyRead { get; set; } = null!;

    public string AccessKeyWrite { get; set; } = null!;

    public string SecretKeyWrite { get; set; } = null!;

    public string Endpoint { get; set; } = null!;
}