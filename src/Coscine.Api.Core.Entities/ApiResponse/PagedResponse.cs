﻿using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Entities.ApiResponse;

public class PagedResponse<T> : IResponse<IEnumerable<T>>, IPagedResponse where T : class
{
    public IEnumerable<T>? Data { get; set; }
    public bool IsSuccess { get; }
    public int? StatusCode { get; set; }
    public string? TraceId { get; set; }
    public Pagination? Pagination { get; set; }
}