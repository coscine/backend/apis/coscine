﻿namespace Coscine.Api.Core.Entities.ApiResponse;

public class Response<T> : IResponse<T> where T : class
{
    public T? Data { get; set; }
    public bool IsSuccess { get; }
    public int? StatusCode { get; set; }
    public string? TraceId { get; set; }
}