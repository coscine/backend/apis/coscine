﻿using Coscine.Api.Core.Shared.RequestFeatures;
using System.Text.Json;

namespace Coscine.Api.Core.Entities.ApiResponse;

public class InternalResponse<T> : IResponse<T>, IPagedResponse, IPagedSearchResponse where T : class
{
    public T? Data { get; set; }
    public int? StatusCode { get; set; }
    public bool IsSuccess => StatusCode >= 200 && StatusCode <= 299;
    public string? TraceId { get; set; }
    public Pagination? Pagination { get; set; }
    public IEnumerable<SearchCategory>? Categories { get; set; }

    public InternalResponse()
    {
    }

    public InternalResponse(T? data, int? statusCode, string? traceId)
    {
        Data = data;
        StatusCode = statusCode;
        TraceId = traceId;
    }

    public override string ToString()
    {
        return JsonSerializer.Serialize(this, new JsonSerializerOptions
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
        });
    }
}