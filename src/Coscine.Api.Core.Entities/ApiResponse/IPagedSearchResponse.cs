﻿using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Entities.ApiResponse;

public interface IPagedSearchResponse
{
    IEnumerable<SearchCategory>? Categories { get; set; }
}