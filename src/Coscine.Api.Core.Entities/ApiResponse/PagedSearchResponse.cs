﻿using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Entities.ApiResponse;

public class PagedSearchResponse<T> : IResponse<IEnumerable<T>>, IPagedResponse, IPagedSearchResponse where T : class
{
    public IEnumerable<T>? Data { get; set; }
    public bool IsSuccess { get; }
    public int? StatusCode { get; set; }
    public string? TraceId { get; set; }
    public Pagination? Pagination { get; set; }
    public IEnumerable<SearchCategory>? Categories { get; set; }
}