﻿using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Entities.ApiResponse;

public interface IPagedResponse
{
    Pagination? Pagination { get; set; }
}