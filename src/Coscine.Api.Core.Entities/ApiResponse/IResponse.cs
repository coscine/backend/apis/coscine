﻿namespace Coscine.Api.Core.Entities.ApiResponse;

public interface IResponse<T> where T : class
{
    T? Data { get; set; }
    bool IsSuccess { get; }
    int? StatusCode { get; set; }
    string? TraceId { get; set; }
}