﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class ElasticSearchConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "ElasticSearchConfiguration";

    [Required]
    public required Uri Endpoint { get; set; }

    [Required]
    public required string DefaultIndex { get; set; }
}
