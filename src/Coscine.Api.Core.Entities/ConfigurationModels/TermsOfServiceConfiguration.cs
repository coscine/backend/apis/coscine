﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class TermsOfServiceConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "TosConfiguration";

    [Required]
    public required string Version { get; set; }

    [Required]
    public required string Url { get; set; }
}
