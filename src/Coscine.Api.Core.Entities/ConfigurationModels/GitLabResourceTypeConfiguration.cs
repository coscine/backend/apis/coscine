﻿using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class GitLabResourceTypeConfiguration : ResourceTypeConfiguration
{
    public int ProjectId { get; set; }

    public string? RepoUrl { get; set; }

    public string? AccessToken { get; set; }

    public string? Branch { get; set; }

    public bool? TosAccepted { get; set; }
}
