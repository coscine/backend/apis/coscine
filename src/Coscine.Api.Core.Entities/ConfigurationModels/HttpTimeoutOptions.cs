﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class HttpTimeoutOptions
{
    [Required]
    public required TimeSpan TimeoutTime { get; set; } = TimeSpan.FromSeconds(100);
}
