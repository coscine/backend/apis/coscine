﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

/// <summary>
/// Represents the configuration settings for the PID ePIC API used in the application.
/// </summary>
public class PidConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "PidConfiguration";

    [Required]
    public required string Prefix { get; set; }

    [Required]
    public required string Url { get; set; }

    [Required]
    public required string User { get; set; }

    [Required]
    public required string Password { get; set; }

    [Required]
    public required string DigitalObjectLocationUrl { get; set; }
}
