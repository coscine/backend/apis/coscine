using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class RetryPolicyOptions
{
    [Required]
    public int Count { get; set; } = 3;
    
    [Required]
    public int BackoffPower { get; set; } = 2;
}