using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class OpenIdConnectProviderConfiguration
{
    [Required]
    public string ExternalId { get; set; } = null!;

    [Required]
    public string Name { get; set; } = null!;

    [Required]
    [Url]
    public string Authority { get; set; } = null!;

    [Required]
    public string ClientId { get; set; } = null!;

    [Required]
    public string ClientSecret { get; set; } = null!;
}