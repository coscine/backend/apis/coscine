using System.Net;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class DefaultHttpClientHandler : HttpClientHandler
{
    public DefaultHttpClientHandler(HttpClientOptions options)
    {
        AutomaticDecompression = DecompressionMethods.Brotli | DecompressionMethods.Deflate | DecompressionMethods.GZip;
        Credentials = options.Credentials;        
        if (options.Insecure)
        {
            ServerCertificateCustomValidationCallback = (message, cert, chain, sslPolicyErrors) => true;
        }
    }

}
