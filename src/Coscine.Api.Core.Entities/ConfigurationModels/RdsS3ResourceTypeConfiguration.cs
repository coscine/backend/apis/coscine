﻿using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using Coscine.ECSManager;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class RdsS3ResourceTypeConfiguration : ResourceTypeConfiguration
{
    [Required]
    public required EcsManagerConfiguration RdsS3EcsManagerConfiguration { get; set; }

    [Required]
    public required EcsManagerConfiguration UserEcsManagerConfiguration { get; set; }

    public IHttpClientFactory HttpClientFactory { get; set; } = null!;

    public string? AccessKey { get; set; }

    public string? SecretKey { get; set; }

    public string? AccessKeyRead { get; set; }

    public string? SecretKeyRead { get; set; }

    public string? AccessKeyWrite { get; set; }

    public string? SecretKeyWrite { get; set; }

    [Required]
    public required string Endpoint { get; set; }
}
