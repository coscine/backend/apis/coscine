﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class RateLimiterConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "RateLimiterConfiguration";

    [Required]
    public required bool AutoReplenishment { get; set; } = true;

    [Required]
    [Range(0, int.MaxValue)]
    public required int PermitLimit { get; set; } = 400;

    [Required]
    [Range(0, int.MaxValue)]
    public required int QueueLimit { get; set; }

    [Required]
    [Range(0, int.MaxValue)]
    public required int WindowInMinutes { get; set; } = 1;
}
