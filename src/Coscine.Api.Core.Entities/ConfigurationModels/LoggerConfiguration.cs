﻿namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class LoggerConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "LoggerConfiguration";

    public string LogLevel { get; set; } = "Trace";

    public string LogHome { get; set; } = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs");
}
