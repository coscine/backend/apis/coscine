using System.Net;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class HttpClientOptions
{
    public Uri? BaseAddress { get; set; }

    public TimeSpan Timeout { get; set; } = TimeSpan.FromSeconds(30);

    public NetworkCredential? Credentials { get; set; }

    public bool Insecure { get; set; } = false;
}