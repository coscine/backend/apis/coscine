namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class MessageConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "MessageConfiguration";

    /// <summary>
    /// The message for the user in German ("de").
    /// </summary>
    public string? De { get; set; }
    
    /// <summary>
    /// The message for the user in English ("en").
    /// </summary>
    public string? En { get; set; }

    /// <summary>
    /// The message type or classification (e.g. information, warning, error).
    /// </summary>
    public string? Type { get; set; }

    /// <summary>
    /// The start date for the message (UTC).
    /// </summary>
    /// <remarks>
    /// Always provide a time zone information to avoid confusion, as Linux systems default to UTC, while Windows systems default to the local time zone of the machine!
    /// </remarks>
    public DateTime? StartDate { get; set; }

    /// <summary>
    /// The end date for the message (UTC).
    /// </summary>
    /// <remarks>
    /// Always provide a time zone information to avoid confusion, as Linux systems default to UTC, while Windows systems default to the local time zone of the machine!
    /// </remarks>
    public DateTime? EndDate { get; set; }
}