﻿using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using Coscine.ECSManager;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class RdsResourceTypeConfiguration : ResourceTypeConfiguration
{
    [Required]
    public required EcsManagerConfiguration EcsManagerConfiguration { get; set; }

    public IHttpClientFactory HttpClientFactory { get; set; } = null!;

    public string? AccessKey { get; set; }

    public string? SecretKey { get; set; }

    public string? Endpoint { get; set; }
}
