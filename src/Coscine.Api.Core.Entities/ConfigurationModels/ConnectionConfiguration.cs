﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class ConnectionConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "ConnectionConfiguration";

    [JsonPropertyName("ServiceUrl")]
    [Required]
    public required string ServiceUrl { get; set; }

    [JsonPropertyName("ApplicationProfiles")]
    [Required]
    public required ApplicationProfiles ApplicationProfiles { get; set; }

    [JsonPropertyName("ProjectInvitations")]
    [Required]
    public required ProjectInvitations ProjectInvitations { get; set; }

    [JsonPropertyName("ServiceDesk")]
    [Required]
    public required ServiceDesk ServiceDesk { get; set; }
}

public class GitLab
{
    [JsonPropertyName("HostUrl")]
    [Required]
    public required string HostUrl { get; set; }

    [JsonPropertyName("Token")]
    [Required]
    public required string Token { get; set; }

    [JsonPropertyName("ProjectId")]
    [Required]
    [Range(0, int.MaxValue)]
    public required int ProjectId { get; set; }
}

public class ProjectInvitations
{
    [JsonPropertyName("QueryParameter")]
    [Required]
    public required string QueryParameter { get; set; }
}

public class Requests
{
    [JsonPropertyName("GitLab")]
    [Required]
    public required GitLab GitLab { get; set; }
}

public class ServiceDesk
{
    [JsonPropertyName("Email")]
    [Required]
    public required string Email { get; set; }
}

public class ApplicationProfiles
{
    [JsonPropertyName("Requests")]
    [Required]
    public required Requests Requests { get; set; }
}
