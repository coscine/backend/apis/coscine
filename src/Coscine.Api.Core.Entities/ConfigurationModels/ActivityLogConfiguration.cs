﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

/// <summary>
/// Represents the configuration settings for activity logging used in the application.
/// </summary>
public class ActivityLogConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "ActivityLogConfiguration";

    /// <summary>
    /// A value indicating whether activity logging is enabled.
    /// </summary>
    /// <remarks>When set to <c>true</c>, activity logging is enabled; otherwise, it is disabled.</remarks>
    public bool IsEnabled { get; set; }

    /// <summary>
    /// The number of days to retain the activity logs.
    /// Has to be between and including 7 and 14.
    /// </summary>
    /// <remarks>Defines the period for which the activity logs should be kept before being purged.</remarks>
    [Range(7, 14)]
    public int RetentionDays { get; set; }
}
