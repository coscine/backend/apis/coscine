using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class AuthenticationConfiguration
{
    public static readonly string Section = "AuthenticationConfiguration";

    [Required]
    [Url]
    public string LoginCallbackUri { get; set; } = null!;

    public string? LoginCallbackPrefix { get; set; }

    [Required]
    [Url]
    public string RedirectUri { get; set; } = null!;

    [Required]
    public List<OpenIdConnectProviderConfiguration> OpenIdConnectProviders { get; set; } = null!;
}