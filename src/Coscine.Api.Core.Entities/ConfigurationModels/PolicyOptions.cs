using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class PolicyOptions
{
    [Required]
    public CircuitBreakerPolicyOptions HttpCircuitBreaker { get; set; } = null!;

    [Required]
    public RetryPolicyOptions HttpRetry { get; set; } = null!;
}
