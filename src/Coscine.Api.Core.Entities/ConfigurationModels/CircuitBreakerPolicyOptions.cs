using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class CircuitBreakerPolicyOptions
{
    [Required]
    public TimeSpan DurationOfBreak { get; set; } = TimeSpan.FromSeconds(30);
    
    [Required]
    public int ExceptionsAllowedBeforeBreaking { get; set; } = 12;
}
