﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class SparqlConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "SparqlConfiguration";

    [Required]
    public required string ReadSparqlEndpoint { get; set; }

    [Required]
    public required string UpdateSparqlEndpoint { get; set; }
}
