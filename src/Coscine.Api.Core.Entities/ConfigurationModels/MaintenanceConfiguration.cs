﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

[Obsolete($"This class is obsolete and will be removed in the future. Use {nameof(NocConfiguration)} instead.")]
public class MaintenanceConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "MaintenanceConfiguration";

    [Required]
    public required List<string> RelevanceList { get; set; }

    [Required]
    public required string RssUrl { get; set; }

    [Required]
    public required string UndefinedString { get; set; }
}
