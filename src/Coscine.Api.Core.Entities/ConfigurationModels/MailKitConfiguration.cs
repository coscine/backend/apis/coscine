﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class MailKitConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "MailKitConfiguration";

    [Required]
    public required string Server { get; set; }

    [Required]
    public required int Port { get; set; }

    [Required]
    public required string SenderName { get; set; }

    [Required]
    public required string SenderEmail { get; set; }

    public string? Account { get; set; }

    public string? Password { get; set; }

    [Required]
    public required bool Security { get; set; }
}
