﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

/// <remarks>
/// Maintenance API Documentation available at https://noc-portal.rz.rwth-aachen.de/ticket/api-tokens, or under section <i>API</i> in the NOC-Portal.
/// </remarks>
public class NocConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "NocConfiguration";

    /// <summary>
    /// API Endpoint URL of the NOC-Portal.
    /// </summary>
    [Required]
    public required Uri Endpoint { get; set; }

    /// <summary>
    /// ID of the Ticket-Queue, from which to read information.
    /// </summary>
    /// <remarks>The "<i>Coscine</i>" Ticket-Queue in the NOC-Portal.</remarks>
    [Required]
    public required string TicketQueueId { get; set; }

    [Required]
    public required string User { get; set; }

    [Required]
    public required string Password { get; set; }

    [Required]
    public required string BasicAuthToken { get; set; }
}
