﻿using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Entities.ConfigurationModels;

public class LinkedResourceTypeConfiguration : ResourceTypeConfiguration
{
    [Required]
    public required string SparqlEndpoint { get; set; }
}
