﻿namespace Coscine.Api.Core.Entities.Exceptions;

public interface ICoscineHttpError
{
    int StatusCode { get; }

    string Type { get; }

    string Title { get; }
}