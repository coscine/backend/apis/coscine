﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class TopLevelVocabularyForInstanceNotFoundException : NotFoundException
{
    public TopLevelVocabularyForInstanceNotFoundException(Uri instanceUri) : base($"The top-level vocabulary for the instance with id: {instanceUri} was not found.")
    { }

    protected TopLevelVocabularyForInstanceNotFoundException(string? message) : base($"")
    { }

    protected TopLevelVocabularyForInstanceNotFoundException() : base(null)
    { }

    protected TopLevelVocabularyForInstanceNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    { }
}