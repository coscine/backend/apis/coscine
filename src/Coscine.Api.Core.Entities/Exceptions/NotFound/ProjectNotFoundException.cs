﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class ProjectNotFoundException : NotFoundException
{
    public ProjectNotFoundException(Guid projectId) : base($"The project with id: {projectId} was not found.")
    {
    }

    public ProjectNotFoundException(string slug) : base($"The project with slug: {slug} was not found.")
    {
    }

    protected ProjectNotFoundException() : base("The project was not found.")
    {
    }

    protected ProjectNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}