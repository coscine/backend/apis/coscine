namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class EmailConfirmationTokenExpiredBadRequestException : NotFoundException
{
    public EmailConfirmationTokenExpiredBadRequestException(Guid userId, Guid confirmationTokenId) : base($"The email confirmation token for user with id: {userId} and token with id: {confirmationTokenId} has expired.")
    { }
}