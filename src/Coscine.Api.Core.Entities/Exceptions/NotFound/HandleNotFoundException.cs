﻿using Coscine.Api.Core.Entities.OtherModels;

namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class HandleNotFoundException : NotFoundException
{
    public HandleNotFoundException(Pid pid) : base($"The Handle with pid: {pid.Identifier} doesn't exist.")
    {
    }

    protected HandleNotFoundException(string? message) : base(message)
    {
    }

    protected HandleNotFoundException() : base(null)
    {
    }

    protected HandleNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}