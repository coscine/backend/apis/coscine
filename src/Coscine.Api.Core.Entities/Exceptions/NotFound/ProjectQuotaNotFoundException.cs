namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class ProjectQuotaNotFoundException : NotFoundException
{
    public ProjectQuotaNotFoundException(Guid projectId, Guid resourceTypeId) : base($"The projectQuota with projectId {projectId} resourceTypeId {resourceTypeId} and was not found.")
    {
    }

    protected ProjectQuotaNotFoundException(string? message) : base(message)
    {
    }

    protected ProjectQuotaNotFoundException() : base(null)
    {
    }

    protected ProjectQuotaNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}