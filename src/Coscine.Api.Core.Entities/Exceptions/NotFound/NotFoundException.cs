﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public abstract class NotFoundException : Exception, ICoscineHttpError
{
    public int StatusCode => 404;

    public string Type => "https://datatracker.ietf.org/doc/html/rfc7231#section-6.5.4";

    public string Title => "Not Found";

    protected NotFoundException(string? message) : base(message)
    {
    }

    protected NotFoundException() : base()
    {
    }

    protected NotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}