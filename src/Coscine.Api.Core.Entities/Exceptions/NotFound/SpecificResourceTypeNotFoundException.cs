namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class SpecificResourceTypeNotFoundException : NotFoundException
{
    public SpecificResourceTypeNotFoundException(string? specificResourceType, string resourceType) : base($"No specific resource type \"{specificResourceType}\" found for the given resource type \"{resourceType}\".")
    {
    }

    protected SpecificResourceTypeNotFoundException() : base()
    {
    }

    protected SpecificResourceTypeNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }

    protected SpecificResourceTypeNotFoundException(string? message) : base(message)
    {
    }
}