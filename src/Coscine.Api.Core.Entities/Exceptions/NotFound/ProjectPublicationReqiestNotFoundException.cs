namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class ProjectPublicationRequestNotFoundException : NotFoundException
{
    public ProjectPublicationRequestNotFoundException(Guid publicationRequestId) : base($"The project publication request with id: {publicationRequestId} was not found.")
    { }

    protected ProjectPublicationRequestNotFoundException() : base("The project publication request was not found.")
    { }
}