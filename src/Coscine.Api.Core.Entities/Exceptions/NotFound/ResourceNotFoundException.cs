namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class ResourceNotFoundException : NotFoundException
{
    public ResourceNotFoundException(Guid resourceId) : base($"The resource with id: {resourceId} was not found.")
    {
    }

    public ResourceNotFoundException(string? resource) : base($"The resource: {resource} doesn't exist in the database.")
    {
    }

    protected ResourceNotFoundException() : base()
    {
    }

    protected ResourceNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}