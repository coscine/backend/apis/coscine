namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class ProjectRoleNotFoundException : NotFoundException
{
    public ProjectRoleNotFoundException(Guid projectRoleId) : base($"The projectRole with id: {projectRoleId} was not found.")
    {
    }

    protected ProjectRoleNotFoundException(string? message) : base(message)
    {
    }

    protected ProjectRoleNotFoundException() : base(null)
    {
    }

    protected ProjectRoleNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}