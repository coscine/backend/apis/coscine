﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class GitLabProjectNotFoundException : NotFoundException
{
    protected GitLabProjectNotFoundException()
    {
    }

    public GitLabProjectNotFoundException(int gitlabProjectId) : base($"The GitLab project with id: {gitlabProjectId} was not found.")
    {
    }

    protected GitLabProjectNotFoundException(string? message) : base(message)
    {
    }

    protected GitLabProjectNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}