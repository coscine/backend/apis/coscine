namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class TitleNotFoundException : NotFoundException
{
    public TitleNotFoundException(Guid titleId) : base($"The title with id: {titleId} was not found.")
    {
    }

    public TitleNotFoundException(string? title) : base($"The title: {title} doesn't exist in the database.")
    {
    }

    protected TitleNotFoundException() : base(null)
    {
    }

    protected TitleNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}