﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class ProjectInstituteNotFoundException : NotFoundException
{
    public ProjectInstituteNotFoundException(string url) : base($"The ProjectInstitute with the url {url} was not found.")
    {
    }

    protected ProjectInstituteNotFoundException() : base(null)
    {
    }

    protected ProjectInstituteNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}