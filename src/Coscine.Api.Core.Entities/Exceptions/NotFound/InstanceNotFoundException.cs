﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class InstanceNotFoundException : NotFoundException
{
    public InstanceNotFoundException(Uri instanceUri) : base($"The instance with id: {instanceUri} was not found.")
    { }

    protected InstanceNotFoundException(string? message) : base($"")
    { }

    protected InstanceNotFoundException() : base(null)
    { }

    protected InstanceNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    { }
}