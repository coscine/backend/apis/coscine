namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class EmailConfirmationTokenNotFoundException : NotFoundException
{
    public EmailConfirmationTokenNotFoundException(Guid confirmationTokenId) : base($"There are no pending email confirmations for the token with id: {confirmationTokenId}.")
    { }
}