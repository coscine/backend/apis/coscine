﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class MetadataGraphNotFoundException : NotFoundException
{
    public MetadataGraphNotFoundException(Guid resourceId, string path)
        : base($"A metadata graph for the tree path: {path} for resource with id: {resourceId} does not exist.")
    {
    }

    protected MetadataGraphNotFoundException() : base(null)
    {
    }

    protected MetadataGraphNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}