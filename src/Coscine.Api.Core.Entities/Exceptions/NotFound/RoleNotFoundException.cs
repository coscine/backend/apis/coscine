namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class RoleNotFoundException : NotFoundException
{
    public RoleNotFoundException(Guid roleId) : base($"The role with id: {roleId} was not found.")
    {
    }

    public RoleNotFoundException(string? role) : base($"The role: {role} doesn't exist in the database.")
    {
    }

    protected RoleNotFoundException() : base(null)
    {
    }

    protected RoleNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}