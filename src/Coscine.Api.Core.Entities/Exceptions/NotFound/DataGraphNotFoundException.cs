﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class DataGraphNotFoundException : NotFoundException
{
    public DataGraphNotFoundException(Guid resourceId, string path)
        : base($"A data graph for the tree path: {path} for resource with id: {resourceId} does not exist.")
    {
    }

    protected DataGraphNotFoundException() : base(null)
    {
    }

    protected DataGraphNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}