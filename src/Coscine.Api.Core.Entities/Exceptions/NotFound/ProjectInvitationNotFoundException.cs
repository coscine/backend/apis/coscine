namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class ProjectInvitationNotFoundException : NotFoundException
{
    public ProjectInvitationNotFoundException(Guid invitation) : base($"The projectInvitation with id or token: {invitation} was not found.")
    {
    }

    public ProjectInvitationNotFoundException(string? projectInvitation) : base($"The projectInvitation: {projectInvitation} doesn't exist in the database.")
    {
    }

    protected ProjectInvitationNotFoundException() : base(null)
    {
    }

    protected ProjectInvitationNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}