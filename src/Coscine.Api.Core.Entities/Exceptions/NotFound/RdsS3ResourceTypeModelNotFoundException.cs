namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class RdsS3ResourceTypeNotFoundException : NotFoundException
{
    public RdsS3ResourceTypeNotFoundException(Guid rdsS3ResourceTypeId) : base($"The rdsS3ResourceType with id: {rdsS3ResourceTypeId} was not found.")
    {
    }

    public RdsS3ResourceTypeNotFoundException(string? rdsS3ResourceType) : base($"The rdsS3ResourceType: {rdsS3ResourceType} doesn't exist in the database.")
    {
    }

    protected RdsS3ResourceTypeNotFoundException() : base()
    {
    }

    protected RdsS3ResourceTypeNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}