namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class RdsResourceTypeNotFoundException : NotFoundException
{
    public RdsResourceTypeNotFoundException(Guid rdsResourceTypeId) : base($"The rdsResourceType with id: {rdsResourceTypeId} was not found.")
    {
    }

    public RdsResourceTypeNotFoundException(string? rdsResourceType) : base($"The rdsResourceType: {rdsResourceType} doesn't exist in the database.")
    {
    }

    protected RdsResourceTypeNotFoundException() : base()
    {
    }

    protected RdsResourceTypeNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}