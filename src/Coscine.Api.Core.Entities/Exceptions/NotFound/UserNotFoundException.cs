﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class UserNotFoundException : NotFoundException
{
    public UserNotFoundException() : base("The user was not found.")
    {
    }

    public UserNotFoundException(Guid id) : base($"The user with id: {id} was not found.")
    {
    }

    protected UserNotFoundException(string? message) : base(message)
    {
    }

    protected UserNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}