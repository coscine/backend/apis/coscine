namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class ResourceTypeOptionsNotFoundException : NotFoundException
{
    public ResourceTypeOptionsNotFoundException(Guid resourceId) : base($"The resource with id: {resourceId} has no type options provided or none are available.")
    {
    }

    public ResourceTypeOptionsNotFoundException(string? resourceType) : base($"The resource type: {resourceType} has no available type definition or type options.")
    {
    }

    protected ResourceTypeOptionsNotFoundException() : base()
    {
    }

    protected ResourceTypeOptionsNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}