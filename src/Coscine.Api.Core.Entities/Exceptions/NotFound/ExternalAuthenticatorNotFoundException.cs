﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class ExternalAuthenticatorNotFoundException : NotFoundException
{
    public ExternalAuthenticatorNotFoundException(Guid id) : base($"The External Authenticator with id: {id} doesn't exist in the database.")
    {
    }

    protected ExternalAuthenticatorNotFoundException(string? message) : base(message)
    {
    }

    protected ExternalAuthenticatorNotFoundException() : base(null)
    {
    }

    protected ExternalAuthenticatorNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}