namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class TopLevelVocabularyForClassNotFoundException : NotFoundException
{
    public TopLevelVocabularyForClassNotFoundException(Uri classUri) : base($"The top-level vocabulary for the class with id: {classUri} was not found.")
    { }

    protected TopLevelVocabularyForClassNotFoundException(string? message) : base($"")
    { }

    protected TopLevelVocabularyForClassNotFoundException() : base(null)
    { }

    protected TopLevelVocabularyForClassNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    { }
}