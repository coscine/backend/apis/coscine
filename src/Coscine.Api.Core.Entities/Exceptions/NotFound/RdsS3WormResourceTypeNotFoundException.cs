namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class RdsS3WormResourceTypeNotFoundException : NotFoundException
{
    public RdsS3WormResourceTypeNotFoundException(Guid rdsS3WormResourceTypeId) : base($"The rdsS3WormResourceType with id: {rdsS3WormResourceTypeId} was not found.")
    {
    }

    public RdsS3WormResourceTypeNotFoundException(string? rdsS3WormResourceType) : base($"The rdsS3WormResourceType: {rdsS3WormResourceType} doesn't exist in the database.")
    {
    }

    protected RdsS3WormResourceTypeNotFoundException() : base()
    {
    }

    protected RdsS3WormResourceTypeNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}