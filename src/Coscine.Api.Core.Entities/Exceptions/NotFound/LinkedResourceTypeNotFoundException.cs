namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class LinkedResourceTypeNotFoundException : NotFoundException
{
    public LinkedResourceTypeNotFoundException(Guid linkedResourceTypeId) : base($"The linkedResourceType with id: {linkedResourceTypeId} was not found.")
    {
    }

    public LinkedResourceTypeNotFoundException(string? linkedResourceType) : base($"The linkedResourceType: {linkedResourceType} doesn't exist in the database.")
    {
    }

    protected LinkedResourceTypeNotFoundException() : base()
    {
    }

    protected LinkedResourceTypeNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}