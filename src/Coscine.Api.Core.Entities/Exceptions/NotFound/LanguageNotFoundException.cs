namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class LanguageNotFoundException : NotFoundException
{
    public LanguageNotFoundException(Guid languageId) : base($"The language with id: {languageId} was not found.")
    {
    }

    public LanguageNotFoundException(string? language) : base($"The language: {language} doesn't exist in the database.")
    {
    }

    protected LanguageNotFoundException() : base(null)
    {
    }

    protected LanguageNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}