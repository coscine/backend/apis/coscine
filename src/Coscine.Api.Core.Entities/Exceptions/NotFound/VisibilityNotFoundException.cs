namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class VisibilityNotFoundException : NotFoundException
{
    public VisibilityNotFoundException(Guid visibilityId) : base($"The visibility with id: {visibilityId} was not found.")
    {
    }

    protected VisibilityNotFoundException(string? message) : base(message)
    {
    }

    protected VisibilityNotFoundException() : base(null)
    {
    }

    protected VisibilityNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}