﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class PidNotFoundException : NotFoundException
{
    public PidNotFoundException(string prefix, Guid id)
        : base($"The provided PID \"{prefix}/{id}\" was not found.") { }

    public PidNotFoundException(string prefix, string suffix)
        : base($"The provided PID \"{prefix}/{suffix}\" was not found.") { }

    protected PidNotFoundException()
        : base(null) { }

    protected PidNotFoundException(string? message, Exception? innerException)
        : base(message, innerException) { }
}
