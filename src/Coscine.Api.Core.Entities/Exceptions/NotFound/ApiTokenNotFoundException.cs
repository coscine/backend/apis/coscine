﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class ApiTokenNotFoundException : NotFoundException
{
    public ApiTokenNotFoundException(Guid id) : base($"The ApiToken with id: {id} doesn't exist in the database.")
    {
    }

    protected ApiTokenNotFoundException(string? message) : base(message)
    {
    }

    protected ApiTokenNotFoundException() : base(null)
    {
    }

    protected ApiTokenNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}