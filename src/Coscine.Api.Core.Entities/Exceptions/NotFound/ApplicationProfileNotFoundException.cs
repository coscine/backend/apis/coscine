﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class ApplicationProfileNotFoundException : NotFoundException
{
    public ApplicationProfileNotFoundException(Uri applicationProfileUri)
        : base($"The provided application profile URI: {applicationProfileUri} does not exist.")
    {
    }

    protected ApplicationProfileNotFoundException() : base(null)
    {
    }

    protected ApplicationProfileNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}