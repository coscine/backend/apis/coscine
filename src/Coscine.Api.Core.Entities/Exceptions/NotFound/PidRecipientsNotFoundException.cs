﻿using Coscine.Api.Core.Entities.OtherModels;

namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class PidRecipientsNotFoundException : NotFoundException
{
    public PidRecipientsNotFoundException(string prefix, Guid id)
        : base($@"No eligible recipients found for the provided PID ""{prefix}/{id}"".") { }

    public PidRecipientsNotFoundException(string prefix, string suffix)
        : base($@"No eligible recipients found for the provided PID ""{prefix}/{suffix}"".") { }

    public PidRecipientsNotFoundException(Pid pid)
        : base($@"No eligible recipients found for the provided PID ""{pid.Identifier}"".") { }

    protected PidRecipientsNotFoundException()
        : base(null) { }

    protected PidRecipientsNotFoundException(string? message, Exception? innerException)
        : base(message, innerException) { }

    protected PidRecipientsNotFoundException(string? message)
        : base(message) { }
}
