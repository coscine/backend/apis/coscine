﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class LicenseNotFoundException : NotFoundException
{
    public LicenseNotFoundException(Guid licenseId) : base($"The license with id: {licenseId} was not found.")
    {
    }

    protected LicenseNotFoundException(string? message) : base(message)
    {
    }

    protected LicenseNotFoundException() : base(null)
    {
    }

    protected LicenseNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}