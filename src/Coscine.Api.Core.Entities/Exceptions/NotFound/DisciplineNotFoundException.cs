﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class DisciplineNotFoundException : NotFoundException
{
    public DisciplineNotFoundException(Guid disciplineId) : base($"The discipline with id: {disciplineId} was not found.")
    {
    }

    protected DisciplineNotFoundException(string? message) : base(message)
    {
    }

    protected DisciplineNotFoundException() : base(null)
    {
    }

    protected DisciplineNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}