namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class ResourceTypeConfigNotFoundException : NotFoundException
{
    public ResourceTypeConfigNotFoundException(string? resourceType) : base($"The resourceType: {resourceType} has no configuration options provided.")
    {
    }

    protected ResourceTypeConfigNotFoundException() : base()
    {
    }

    protected ResourceTypeConfigNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}