﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class TreeNotFoundException : NotFoundException
{
    public TreeNotFoundException(Uri graphId) : base($"A tree with id: {graphId.AbsoluteUri} does not exist.")
    {
    }

    protected TreeNotFoundException() : base(null)
    {
    }

    protected TreeNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}