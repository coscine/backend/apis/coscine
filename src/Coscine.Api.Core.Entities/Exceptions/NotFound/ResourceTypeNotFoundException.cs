namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class ResourceTypeNotFoundException : NotFoundException
{
    public ResourceTypeNotFoundException(Guid resourceTypeId) : base($"The resourceType with id: {resourceTypeId} was not found.")
    {
    }

    protected ResourceTypeNotFoundException(string? resourceType) : base($"The resourceType: {resourceType} doesn't exist in the database.")
    {
    }

    protected ResourceTypeNotFoundException() : base()
    {
    }

    protected ResourceTypeNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}