﻿namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class BlobNotFoundException : NotFoundException
{
    public BlobNotFoundException(string? key) : base($"The blob with the key: {key} was not found.")
    {
    }

    protected BlobNotFoundException() : base(null)
    {
    }

    protected BlobNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}