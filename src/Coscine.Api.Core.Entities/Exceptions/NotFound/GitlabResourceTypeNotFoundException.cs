namespace Coscine.Api.Core.Entities.Exceptions.NotFound;

public class GitlabResourceTypeNotFoundException : NotFoundException
{
    public GitlabResourceTypeNotFoundException(Guid gitlabResourceTypeId) : base($"The gitlabResourceType with id: {gitlabResourceTypeId} was not found.")
    {
    }

    public GitlabResourceTypeNotFoundException(string? gitlabResourceType) : base($"The gitlabResourceType: {gitlabResourceType} doesn't exist in the database.")
    {
    }

    protected GitlabResourceTypeNotFoundException() : base()
    {
    }

    protected GitlabResourceTypeNotFoundException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}