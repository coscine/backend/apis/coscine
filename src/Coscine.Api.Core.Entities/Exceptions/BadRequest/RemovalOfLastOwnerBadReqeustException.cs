﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class RemovalOfLastOwnerBadRequestException : BadRequestException
{
    public RemovalOfLastOwnerBadRequestException(Guid userId, Guid projectId) : base($"The user with the id {userId} is the last owner of the project {projectId}. A project has to have at least one owner.")
    {
    }

    protected RemovalOfLastOwnerBadRequestException(string? message) : base(message)
    {
    }

    protected RemovalOfLastOwnerBadRequestException() : base(null)
    {
    }

    protected RemovalOfLastOwnerBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}