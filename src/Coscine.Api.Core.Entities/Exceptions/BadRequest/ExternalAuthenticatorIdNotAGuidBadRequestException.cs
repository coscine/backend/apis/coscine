﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class ExternalAuthenticatorIdNotAGuidBadRequestException : BadRequestException
{

    public ExternalAuthenticatorIdNotAGuidBadRequestException(string? id) : base($"The provided external authenticator id: '{id}', is not a GUID.")
    {
    }

    protected ExternalAuthenticatorIdNotAGuidBadRequestException() : base(null)
    {
    }

    protected ExternalAuthenticatorIdNotAGuidBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}