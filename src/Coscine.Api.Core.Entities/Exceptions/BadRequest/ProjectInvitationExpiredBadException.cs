﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class ProjectInvitationExpiredBadException : BadRequestException
{
    public ProjectInvitationExpiredBadException(DateTime expired) : base($"The project invitation has already expired at {expired}.")
    {
    }

    protected ProjectInvitationExpiredBadException(string? message) : base(message)
    {
    }

    protected ProjectInvitationExpiredBadException() : base(null)
    {
    }

    protected ProjectInvitationExpiredBadException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}