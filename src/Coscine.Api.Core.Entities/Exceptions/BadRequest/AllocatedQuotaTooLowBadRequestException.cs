﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class AllocatedQuotaTooLowBadRequestException : BadRequestException
{
    public AllocatedQuotaTooLowBadRequestException(int toAllocateGib, float currentlyReserved)
        : base($"Cannot set quota ({toAllocateGib} GB) below the total ({currentlyReserved} GB) that are currently reserved.")
    {
    }

    public AllocatedQuotaTooLowBadRequestException(long minimum)
        : base($"The requested quota must be greater than or equal to the minimum of {minimum} GB.")
    {
    }

    public AllocatedQuotaTooLowBadRequestException(string? message) : base(message)
    {
    }

    protected AllocatedQuotaTooLowBadRequestException() : base(null)
    {
    }

    protected AllocatedQuotaTooLowBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}