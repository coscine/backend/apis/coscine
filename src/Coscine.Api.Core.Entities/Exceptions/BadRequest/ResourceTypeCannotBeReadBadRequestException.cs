namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class ResourceTypeCannotBeReadBadRequestException : BadRequestException
{
    public ResourceTypeCannotBeReadBadRequestException(string displayName)
        : base($"Reading of entries inside the resource type: {displayName} is not possible.")
    {
    }

    protected ResourceTypeCannotBeReadBadRequestException() : base(null)
    {
    }

    protected ResourceTypeCannotBeReadBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}