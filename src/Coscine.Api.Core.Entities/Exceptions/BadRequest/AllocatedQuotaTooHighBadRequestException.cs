﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class AllocatedQuotaTooHighBadRequestException : BadRequestException
{
    public AllocatedQuotaTooHighBadRequestException(int toAllocateGib, int maxGib, string specificType)
        : base($"Cannot set quota to {toAllocateGib} GB. It would exceed the maximum of {maxGib} GB for {specificType} resources.")
    {
    }

    public AllocatedQuotaTooHighBadRequestException(long maximum)
        : base($"The requested quota must be less than or equal to the maximum of {maximum} GB.")
    {
    }

    public AllocatedQuotaTooHighBadRequestException(string? message) : base(message)
    {
    }

    protected AllocatedQuotaTooHighBadRequestException() : base(null)
    {
    }

    protected AllocatedQuotaTooHighBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}