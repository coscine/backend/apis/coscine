﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class TreeBlobResourceArchivedBadRequestException : BadRequestException
{
    public TreeBlobResourceArchivedBadRequestException(Guid resourceId) : base($"Unable to upload to or delete from the resource with ID \"{resourceId}\" due to its archived state.")
    {
    }

    protected TreeBlobResourceArchivedBadRequestException(string? message) : base(message)
    {
    }

    protected TreeBlobResourceArchivedBadRequestException() : base(null)
    {
    }

    protected TreeBlobResourceArchivedBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}