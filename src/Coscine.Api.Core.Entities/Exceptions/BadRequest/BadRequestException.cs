﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public abstract class BadRequestException : Exception, ICoscineHttpError
{
    public int StatusCode => 400;

    public string Type => "https://tools.ietf.org/html/rfc7231#section-6.5.1";

    public string Title => "Bad Request";

    protected BadRequestException(string? message) : base(message)
    {
    }

    protected BadRequestException() : base(null)
    {
    }

    protected BadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}