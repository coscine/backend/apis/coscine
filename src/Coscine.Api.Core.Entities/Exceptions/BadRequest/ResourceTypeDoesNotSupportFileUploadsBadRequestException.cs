﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class ResourceTypeDoesNotSupportFileUploadsBadRequestException : BadRequestException
{
    public ResourceTypeDoesNotSupportFileUploadsBadRequestException(string displayName)
        : base($"The resource type: {displayName} does not support file uploads.")
    {
    }

    protected ResourceTypeDoesNotSupportFileUploadsBadRequestException() : base(null)
    {
    }

    protected ResourceTypeDoesNotSupportFileUploadsBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}