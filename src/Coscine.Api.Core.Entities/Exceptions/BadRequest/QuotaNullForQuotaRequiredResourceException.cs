﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class QuotaNullForQuotaRequiredResourceException : BadRequestException
{
    public QuotaNullForQuotaRequiredResourceException(Guid resourceTypeId) : base($"A null quota has been assigned to the resource type with ID '{resourceTypeId}', which requires a quota value.")
    {
    }

    protected QuotaNullForQuotaRequiredResourceException(string? message) : base(message)
    {
    }

    protected QuotaNullForQuotaRequiredResourceException() : base(null)
    {
    }

    protected QuotaNullForQuotaRequiredResourceException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}