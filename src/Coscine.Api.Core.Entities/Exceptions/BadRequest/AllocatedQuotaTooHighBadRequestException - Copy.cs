﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

/// <summary>
/// Represents an exception thrown when the Content-Length request header is missing or invalid.
/// </summary>
public class ContentLengthRequestHeaderMissingBadRequestException : BadRequestException
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ContentLengthRequestHeaderMissingBadRequestException"/> class with a default message.
    /// </summary>
    public ContentLengthRequestHeaderMissingBadRequestException()
        : base("Content-Length request header is missing or invalid.")
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ContentLengthRequestHeaderMissingBadRequestException"/> class with a specified message.
    /// </summary>
    /// <param name="message">The error message that explains the reason for the exception.</param>
    public ContentLengthRequestHeaderMissingBadRequestException(string? message) : base(message)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="ContentLengthRequestHeaderMissingBadRequestException"/> class with a specified message and inner exception.
    /// </summary>
    /// <param name="message">The error message that explains the reason for the exception.</param>
    /// <param name="innerException">The exception that is the cause of the current exception, or a null reference if no inner exception is specified.</param>
    protected ContentLengthRequestHeaderMissingBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}
