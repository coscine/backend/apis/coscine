﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class ApplicationProfileValidationBadRequestException : BadRequestException
{
    public ApplicationProfileValidationBadRequestException(string? applicationProfile) : base($"Provided RDF graph does not conform to the specified SHACL application profile: {applicationProfile}.")
    {
    }

    protected ApplicationProfileValidationBadRequestException() : base()
    { }

    protected ApplicationProfileValidationBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}