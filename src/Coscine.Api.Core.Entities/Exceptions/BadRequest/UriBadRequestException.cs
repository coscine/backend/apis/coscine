﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class UriBadRequestException : BadRequestException
{
    public UriBadRequestException(string profile) : base($"The provided input: {profile} can not be parsed to a valid URI.")
    {
    }

    protected UriBadRequestException() : base(null)
    {
    }

    protected UriBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}