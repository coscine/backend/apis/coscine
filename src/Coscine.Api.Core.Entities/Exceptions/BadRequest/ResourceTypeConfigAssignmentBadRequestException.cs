﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class ResourceTypeConfigAssignmentBadRequestException : BadRequestException
{
    public ResourceTypeConfigAssignmentBadRequestException(string resourceType) : base($"The provided resource type configuration is not assignable to the resource type: {resourceType}.")
    {
    }

    protected ResourceTypeConfigAssignmentBadRequestException() : base(null)
    {
    }

    protected ResourceTypeConfigAssignmentBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}