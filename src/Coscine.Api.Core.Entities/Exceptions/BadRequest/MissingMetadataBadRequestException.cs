﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class MissingMetadataBadRequestException : BadRequestException
{
    public MissingMetadataBadRequestException(Guid resourceId, string key) : base($"The blob at \"{key}\" for resource with id: {resourceId} is missing metadata. Metadata (tree) must be submitted before posting any blob.")
    {
    }

    protected MissingMetadataBadRequestException(string? message) : base(message)
    {
    }

    protected MissingMetadataBadRequestException() : base(null)
    {
    }

    protected MissingMetadataBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}