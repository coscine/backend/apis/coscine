﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class ForbiddenCharInKeyBadRequest : BadRequestException
{
    public ForbiddenCharInKeyBadRequest(string? key) : base(@$"The provided key: ""{key}"" contains forbidden characters. The following characters cannot be used: \ : ? * < > |")
    {
    }

    protected ForbiddenCharInKeyBadRequest() : base(null)
    {
    }

    protected ForbiddenCharInKeyBadRequest(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}