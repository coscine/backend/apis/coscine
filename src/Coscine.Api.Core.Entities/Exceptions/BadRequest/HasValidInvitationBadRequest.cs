﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class HasValidInvitationBadRequest : BadRequestException
{
    public HasValidInvitationBadRequest(Guid projectId, string email)
        : base($"The project with the id {projectId} has already a valid invitation with the following email address {email}.")
    {
    }

    protected HasValidInvitationBadRequest(string? message) : base(message)
    {
    }

    protected HasValidInvitationBadRequest() : base(null)
    {
    }

    protected HasValidInvitationBadRequest(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}