﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class NoValidProviderBadRequest : BadRequestException
{
    public NoValidProviderBadRequest(Guid resourceId, string? provider) : base(@$"The resource ""{resourceId}"" has no valid provider: ""{provider}""")
    {
    }
    public NoValidProviderBadRequest(string? provider) : base(@$"No valid provider for resource type: ""{provider}""")
    {
    }

    protected NoValidProviderBadRequest() : base(null)
    {
    }

    protected NoValidProviderBadRequest(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}