namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class InvalidResponsibleOrganizationBadRequestException : BadRequestException
{
    public InvalidResponsibleOrganizationBadRequestException(Guid projectId) : base(@$"The project ""{projectId}"" has a invalid amount of responsible organizations.")
    {
    }

    protected InvalidResponsibleOrganizationBadRequestException() : base(null)
    {
    }

    protected InvalidResponsibleOrganizationBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}