﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class ResourceTypeCannotBeDeletedBadRequestException : BadRequestException
{
    public ResourceTypeCannotBeDeletedBadRequestException(string displayName)
        : base($"Deletion of entries inside the resource type: {displayName} is not possible.")
    {
    }

    protected ResourceTypeCannotBeDeletedBadRequestException() : base(null)
    {
    }

    protected ResourceTypeCannotBeDeletedBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}
