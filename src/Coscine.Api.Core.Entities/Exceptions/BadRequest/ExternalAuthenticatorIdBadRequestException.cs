﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class ExternalAuthenticatorIdBadRequestException : BadRequestException
{

    protected ExternalAuthenticatorIdBadRequestException(string? message) : base(message)
    {
    }

    public ExternalAuthenticatorIdBadRequestException() : base("The provided claims, do not contain an external authenticator id.")
    {
    }

    protected ExternalAuthenticatorIdBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}