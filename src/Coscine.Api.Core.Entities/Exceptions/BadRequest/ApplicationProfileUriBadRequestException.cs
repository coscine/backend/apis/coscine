﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class ApplicationProfileBaseUriBadRequestException : BadRequestException
{
    public ApplicationProfileBaseUriBadRequestException(Uri baseApplicationProfileUri) : base($"The provided URI has an invalid base. Valid application profiles have the base: {baseApplicationProfileUri}.")
    {
    }

    protected ApplicationProfileBaseUriBadRequestException(string? message) : base(message)
    {
    }

    public ApplicationProfileBaseUriBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}