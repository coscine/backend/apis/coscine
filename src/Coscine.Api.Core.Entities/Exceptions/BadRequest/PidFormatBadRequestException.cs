﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class PidFormatBadRequestException : BadRequestException
{
    public PidFormatBadRequestException(string pid, string regexPattern) : base($"PID \"{pid}\" has a bad format. Coscine PIDs have the following format: {regexPattern}.")
    {
    }

    protected PidFormatBadRequestException(string? message) : base(message)
    {
    }

    protected PidFormatBadRequestException() : base(null)
    {
    }

    protected PidFormatBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}