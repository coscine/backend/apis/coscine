﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class HandleCouldNotBeUpdatedBadRequestException : BadRequestException
{
    protected HandleCouldNotBeUpdatedBadRequestException(string? message) : base(message)
    {
    }

    public HandleCouldNotBeUpdatedBadRequestException() : base("The handle could not be updated. Error in communication with the handle service.")
    {
    }

    protected HandleCouldNotBeUpdatedBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}