﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class ResourceArchivedBadRequestException : BadRequestException
{
    public ResourceArchivedBadRequestException(Guid resourceId) : base($"Unable to update the archived resource with ID \"{resourceId}\" due to its archived state.")
    {
    }

    protected ResourceArchivedBadRequestException(string? message) : base(message)
    {
    }

    protected ResourceArchivedBadRequestException() : base(null)
    {
    }

    protected ResourceArchivedBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}