﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class MaximumQuotaTooLowBadRequestException : BadRequestException
{
    public MaximumQuotaTooLowBadRequestException(float minimum)
        : base($"The requested maximum quota must be greater than or equal to the minimum of {minimum} GB.")
    {
    }

    protected MaximumQuotaTooLowBadRequestException(string? message) : base(message)
    {
    }

    protected MaximumQuotaTooLowBadRequestException() : base(null)
    {
    }

    protected MaximumQuotaTooLowBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}