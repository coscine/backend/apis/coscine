﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class QuotaNotAdjustableBadRequestException : BadRequestException
{
    public QuotaNotAdjustableBadRequestException(Guid resourceTypeId) : base($"The quota of the resourceType {resourceTypeId} is not adjustable.")
    {
    }

    protected QuotaNotAdjustableBadRequestException(string? message) : base(message)
    {
    }

    protected QuotaNotAdjustableBadRequestException() : base(null)
    {
    }

    protected QuotaNotAdjustableBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}