﻿// Ignore Spelling: Multipart

namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class NotAMultipartBadRequestException : BadRequestException
{
    protected NotAMultipartBadRequestException(string? message) : base(message)
    {
    }

    public NotAMultipartBadRequestException() : base(@"The request is not of the type ""multipart/form-data.""")
    {
    }

    protected NotAMultipartBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}