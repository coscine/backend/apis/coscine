﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class InvalidResourceTypeOptionsBadRequestException : BadRequestException
{
    public InvalidResourceTypeOptionsBadRequestException(string? message) : base(message)
    {
    }

    protected InvalidResourceTypeOptionsBadRequestException() : base()
    {
    }

    protected InvalidResourceTypeOptionsBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}