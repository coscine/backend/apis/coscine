﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public sealed class MaxDateRangeBadRequestException : BadRequestException
{
    public MaxDateRangeBadRequestException() : base("Max date can't be less than min date.")
    {
    }

    public MaxDateRangeBadRequestException(string? message) : base(message)
    {
    }

    public MaxDateRangeBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}