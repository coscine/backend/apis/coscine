﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class PrefixNotSupportedBadRequestException : BadRequestException
{
    public PrefixNotSupportedBadRequestException(string prefix)
        : base(
            $"The prefix: {prefix} is not supported by our API. Please referrer to the API documentation or use a prefix returned by the API."
        )
    { }

    protected PrefixNotSupportedBadRequestException()
        : base(null) { }

    protected PrefixNotSupportedBadRequestException(string? message, Exception? innerException)
        : base(message, innerException) { }
}
