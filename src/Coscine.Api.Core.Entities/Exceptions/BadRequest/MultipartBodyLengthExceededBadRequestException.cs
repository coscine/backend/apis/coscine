﻿// Ignore Spelling: Multipart

namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class MultipartBodyLengthExceededBadRequestException : BadRequestException
{
    public MultipartBodyLengthExceededBadRequestException(int limit)
        : base($"Failed to read the request form. Multipart body length limit of {limit} bytes exceeded.")
    {
    }

    public MultipartBodyLengthExceededBadRequestException(string? message) : base(message)
    {
    }

    protected MultipartBodyLengthExceededBadRequestException() : base(null)
    {
    }

    protected MultipartBodyLengthExceededBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}