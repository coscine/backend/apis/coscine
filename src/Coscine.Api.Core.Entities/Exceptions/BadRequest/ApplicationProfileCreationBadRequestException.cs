﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class ApplicationProfileCreationBadRequestException : BadRequestException
{
    public ApplicationProfileCreationBadRequestException(string? message) : base(message)
    {
    }

    protected ApplicationProfileCreationBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}