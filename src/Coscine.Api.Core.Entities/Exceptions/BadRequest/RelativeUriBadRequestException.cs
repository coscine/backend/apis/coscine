﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class RelativeUriBadRequestException : BadRequestException
{
    public RelativeUriBadRequestException(Uri uri) : base($"The provided URI: {uri} must be absolute.")
    { }

    protected RelativeUriBadRequestException(string? message) : base(message)
    { }

    public RelativeUriBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    { }
}