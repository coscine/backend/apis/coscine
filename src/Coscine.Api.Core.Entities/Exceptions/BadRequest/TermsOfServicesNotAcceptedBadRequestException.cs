﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class TermsOfServicesNotAcceptedBadRequestException : BadRequestException
{
    public TermsOfServicesNotAcceptedBadRequestException(Guid userId, string currentVersion)
        : base($"The user {userId} has not accepted the current ToS version ({currentVersion}).") { }

    public TermsOfServicesNotAcceptedBadRequestException()
       : base($"The user does not exist and could not accept the accept the ToS.") { }

    protected TermsOfServicesNotAcceptedBadRequestException(string? message, Exception? innerException)
        : base(message, innerException) { }

    protected TermsOfServicesNotAcceptedBadRequestException(string? message)
        : base(message) { }
}
