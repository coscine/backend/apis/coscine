﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class QuotaLimitExceededBadRequestException : BadRequestException
{
    public QuotaLimitExceededBadRequestException() : base("The quota limit has been exceeded.")
    {
    }

    protected QuotaLimitExceededBadRequestException(string? message) : base(message)
    {
    }

    protected QuotaLimitExceededBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}