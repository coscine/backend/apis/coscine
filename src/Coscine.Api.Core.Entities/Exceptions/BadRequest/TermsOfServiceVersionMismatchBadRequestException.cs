﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class TermsOfServiceVersionMismatchBadRequestException : BadRequestException
{
    public TermsOfServiceVersionMismatchBadRequestException(Guid userId, string requestedToSVersion, string currentToSVersion)
        : base($"The user with the id {userId} has to accept the current Terms of Service version {currentToSVersion} but requested the version {requestedToSVersion}.")
    {
    }

    protected TermsOfServiceVersionMismatchBadRequestException(string? message) : base(message)
    {
    }

    protected TermsOfServiceVersionMismatchBadRequestException() : base(null)
    {
    }

    protected TermsOfServiceVersionMismatchBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}