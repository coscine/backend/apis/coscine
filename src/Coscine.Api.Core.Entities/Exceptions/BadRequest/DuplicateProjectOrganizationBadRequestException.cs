namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class DuplicateProjectOrganizationBadRequestException : BadRequestException
{
    public DuplicateProjectOrganizationBadRequestException(Guid projectId) : base(@$"The project ""{projectId}"" has duplicate organizations.")
    {
    }

    protected DuplicateProjectOrganizationBadRequestException() : base(null)
    {
    }

    protected DuplicateProjectOrganizationBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}