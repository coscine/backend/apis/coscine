﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class UserAndTokenBadRequestException : BadRequestException
{
    protected UserAndTokenBadRequestException(string? message) : base(message)
    {
    }

    public UserAndTokenBadRequestException() : base(null)
    {
    }

    public UserAndTokenBadRequestException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}