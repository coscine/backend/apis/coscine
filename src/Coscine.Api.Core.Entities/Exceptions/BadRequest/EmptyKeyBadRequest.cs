﻿namespace Coscine.Api.Core.Entities.Exceptions.BadRequest;

public class EmptyKeyBadRequest : BadRequestException
{
    public EmptyKeyBadRequest(string? key) : base(@$"The provided key: ""{key}"" is empty and can't be used.")
    {
    }

    protected EmptyKeyBadRequest() : base(null)
    {
    }

    protected EmptyKeyBadRequest(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}