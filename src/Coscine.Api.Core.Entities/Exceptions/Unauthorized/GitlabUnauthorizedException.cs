﻿namespace Coscine.Api.Core.Entities.Exceptions.Unauthorized;

public class GitlabUnauthorizedException : UnauthorizedException
{
    public GitlabUnauthorizedException(int gitlabProjectId) : base($"The provided GitLab access token is missing the correct authorization requirements for GitLab project with id: {gitlabProjectId}.")
    {
    }

    public GitlabUnauthorizedException() : base("The provided GitLab access token is missing the correct authorization requirements.")
    {
    }

    protected GitlabUnauthorizedException(string? message) : base(message)
    {
    }

    protected GitlabUnauthorizedException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}