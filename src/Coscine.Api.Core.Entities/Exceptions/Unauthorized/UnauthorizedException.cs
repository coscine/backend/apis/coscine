﻿namespace Coscine.Api.Core.Entities.Exceptions.Unauthorized;

public abstract class UnauthorizedException : Exception, ICoscineHttpError
{
    public int StatusCode => 401;

    public string Type => "https://datatracker.ietf.org/doc/html/rfc7235#section-3.1";

    public string Title => "Unauthorized";

    protected UnauthorizedException(string? message) : base(message)
    {
    }

    protected UnauthorizedException() : base(null)
    {
    }

    protected UnauthorizedException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}