namespace Coscine.Api.Core.Entities.Exceptions.Forbidden;

public class ResourceInMaintenanceModeForbiddenException : ForbiddenException
{
    public ResourceInMaintenanceModeForbiddenException(Guid resourceId) : base($"Unable to perform the requested operation on the resource with ID \"{resourceId}\". The resource is currently in maintenance mode and cannot be accessed or modified.")
    {
    }

    protected ResourceInMaintenanceModeForbiddenException(string? message) : base(message)
    {
    }

    protected ResourceInMaintenanceModeForbiddenException() : base(null)
    {
    }

    protected ResourceInMaintenanceModeForbiddenException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}