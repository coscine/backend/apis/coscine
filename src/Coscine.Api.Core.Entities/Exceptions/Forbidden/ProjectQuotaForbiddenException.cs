﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Core.Entities.Exceptions.Forbidden;

public class ProjectQuotaForbiddenException : ForbiddenException
{
    public ProjectQuotaForbiddenException(Guid projectId, OperationAuthorizationRequirement requirement) : base($"You are missing the correct project quota authorization requirement ({requirement.Name}) for the project {projectId}")
    {
    }

    protected ProjectQuotaForbiddenException(string? message) : base(message)
    {
    }

    protected ProjectQuotaForbiddenException() : base(null)
    {
    }

    protected ProjectQuotaForbiddenException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}