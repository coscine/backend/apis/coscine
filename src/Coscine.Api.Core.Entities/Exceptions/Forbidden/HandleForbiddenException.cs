﻿using Coscine.Api.Core.Entities.OtherModels;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Core.Entities.Exceptions.Forbidden;

public class HandleForbiddenException : ForbiddenException
{
    public HandleForbiddenException(Pid pid, OperationAuthorizationRequirement requirement) : base($"You are missing the correct handle authorization requirement ({requirement.Name}) for the handle with pid {pid.Identifier}.")
    {
    }

    public HandleForbiddenException(OperationAuthorizationRequirement requirement) : base($"You are missing the correct handle authorization requirement ({requirement.Name}).")
    {
    }

    protected HandleForbiddenException(string? message) : base(message)
    {
    }

    protected HandleForbiddenException() : base(null)
    {
    }

    protected HandleForbiddenException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}