﻿namespace Coscine.Api.Core.Entities.Exceptions.Forbidden;

public abstract class ForbiddenException : Exception, ICoscineHttpError
{
    public int StatusCode => 403;

    public string Type => "https://datatracker.ietf.org/doc/html/rfc7231#section-6.5.3";

    public string Title => "Forbidden";

    protected ForbiddenException(string? message) : base(message)
    {
    }

    protected ForbiddenException() : base(null)
    {
    }

    protected ForbiddenException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}