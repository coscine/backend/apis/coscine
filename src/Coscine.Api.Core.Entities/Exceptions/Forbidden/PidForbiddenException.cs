﻿using Coscine.Api.Core.Entities.OtherModels;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Core.Entities.Exceptions.Forbidden;

public class PidForbiddenException : ForbiddenException
{
    public PidForbiddenException(Pid pid, OperationAuthorizationRequirement requirement) : base($"You are missing the correct Pid authorization requirement ({requirement.Name}) for the pid {pid.Identifier}.")
    {
    }

    public PidForbiddenException(OperationAuthorizationRequirement requirement) : base($"You are missing the correct handle authorization requirement ({requirement.Name}).")
    {
    }

    protected PidForbiddenException(string? message) : base(message)
    {
    }

    protected PidForbiddenException() : base(null)
    {
    }

    protected PidForbiddenException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}