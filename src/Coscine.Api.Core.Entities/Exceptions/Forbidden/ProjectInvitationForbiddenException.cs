﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Core.Entities.Exceptions.Forbidden;

public class ProjectInvitationForbiddenException : ForbiddenException
{
    public ProjectInvitationForbiddenException(Guid projectId, OperationAuthorizationRequirement requirement) : base($"You are missing the correct project invitation authorization requirement ({requirement.Name}) for the project {projectId}")
    {
    }

    protected ProjectInvitationForbiddenException(string? message) : base(message)
    {
    }

    protected ProjectInvitationForbiddenException() : base(null)
    {
    }

    protected ProjectInvitationForbiddenException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}