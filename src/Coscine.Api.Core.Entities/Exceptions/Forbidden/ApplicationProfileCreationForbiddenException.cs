﻿namespace Coscine.Api.Core.Entities.Exceptions.Forbidden;

public sealed class ApplicationProfileCreationForbiddenException : ForbiddenException
{
    public ApplicationProfileCreationForbiddenException(string? message) : base(message)
    { }

    public ApplicationProfileCreationForbiddenException() : base("Users without a verified email address are not allowed to create application profile requests.")
    { }

    public ApplicationProfileCreationForbiddenException(string? message, Exception? innerException) : base(message, innerException)
    { }
}