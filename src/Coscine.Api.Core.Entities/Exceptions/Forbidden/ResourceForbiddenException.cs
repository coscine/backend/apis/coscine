﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Core.Entities.Exceptions.Forbidden;

public sealed class ResourceForbiddenException : ForbiddenException
{
    public ResourceForbiddenException(Guid resourceId, OperationAuthorizationRequirement requirement) : base($"You are missing the correct resource authorization requirement ({requirement.Name}) for the resource {resourceId}")
    {
    }

    public ResourceForbiddenException(string? message) : base(message)
    {
    }

    public ResourceForbiddenException() : base(null)
    {
    }

    public ResourceForbiddenException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}