﻿using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Core.Entities.Exceptions.Forbidden;

public class ProjectForbiddenException : ForbiddenException
{
    public ProjectForbiddenException(Guid projectId, OperationAuthorizationRequirement requirement) : base($"You are missing the correct project authorization requirement ({requirement.Name}) for the project {projectId}.")
    {
    }

    protected ProjectForbiddenException(string? message) : base(message)
    {
    }

    protected ProjectForbiddenException() : base(null)
    {
    }

    protected ProjectForbiddenException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}