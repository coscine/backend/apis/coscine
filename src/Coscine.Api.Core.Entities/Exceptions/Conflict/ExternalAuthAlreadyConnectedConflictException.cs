﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Entities.Exceptions.Conflict;

public class ExternalAuthAlreadyConnectedConflictException : ConflictException
{
    public ExternalAuthAlreadyConnectedConflictException(Guid userId, IdentityProviders externalAuthenticator)
        : base($"The external authentication provider: {externalAuthenticator} is already connected to the user with id: {userId}.")
    { }
}