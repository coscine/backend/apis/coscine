﻿namespace Coscine.Api.Core.Entities.Exceptions.Conflict;

public class TreeAlreadyExistsConflictException : ConflictException
{
    public TreeAlreadyExistsConflictException(Uri graphId) : base($"A tree with id: {graphId.AbsoluteUri} already exists. Instead of creating a new tree, you should perform an update.")
    {
    }

    protected TreeAlreadyExistsConflictException(string? message) : base(message)
    {
    }

    protected TreeAlreadyExistsConflictException() : base(null)
    {
    }

    protected TreeAlreadyExistsConflictException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}