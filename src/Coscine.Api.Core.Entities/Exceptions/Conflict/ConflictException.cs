﻿namespace Coscine.Api.Core.Entities.Exceptions.Conflict;

public abstract class ConflictException : Exception, ICoscineHttpError
{
    public int StatusCode => 409;

    public string Type => "https://datatracker.ietf.org/doc/html/rfc7231#section-6.5.8";

    public string Title => "Conflict";

    protected ConflictException(string? message) : base(message)
    {
    }

    protected ConflictException() : base(null)
    {
    }

    protected ConflictException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}