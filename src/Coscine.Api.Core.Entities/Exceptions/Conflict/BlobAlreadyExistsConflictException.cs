﻿namespace Coscine.Api.Core.Entities.Exceptions.Conflict;

public class BlobAlreadyExistsConflictException : ConflictException
{
    public BlobAlreadyExistsConflictException(string? key) : base(@$"The blob with the key: ""{key}"" already exists and can't be created. Use update instead.")
    {
    }

    protected BlobAlreadyExistsConflictException() : base(null)
    {
    }

    protected BlobAlreadyExistsConflictException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}