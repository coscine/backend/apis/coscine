﻿namespace Coscine.Api.Core.Entities.Exceptions.Conflict;

public class UserAlreadyMemberOfProjectConflictException : ConflictException
{
    public UserAlreadyMemberOfProjectConflictException(Guid userId, Guid projectId) : base($"The user with the id: {userId} is already part of the project: {projectId}. Use the update method instead.")
    {
    }

    protected UserAlreadyMemberOfProjectConflictException(string? message) : base(message)
    {
    }

    protected UserAlreadyMemberOfProjectConflictException() : base(null)
    {
    }

    protected UserAlreadyMemberOfProjectConflictException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}