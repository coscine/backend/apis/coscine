﻿namespace Coscine.Api.Core.Entities.Exceptions.InternalServerError;

public class VirtuosoConfigurationInternalServerErrorException : InternalServerErrorException
{
    public VirtuosoConfigurationInternalServerErrorException()
        : base($"Could not load the Virtuoso configuration.")
    { }
}