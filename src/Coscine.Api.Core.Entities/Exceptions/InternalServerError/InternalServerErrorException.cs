﻿namespace Coscine.Api.Core.Entities.Exceptions.InternalServerError;

public abstract class InternalServerErrorException : Exception, ICoscineHttpError
{
    public int StatusCode => 500;

    public string Type => "https://datatracker.ietf.org/doc/html/rfc7231#section-6.6.1";

    public string Title => "Internal Server Error";

    protected InternalServerErrorException(string? message) : base(message)
    {
    }

    protected InternalServerErrorException() : base(null)
    {
    }

    protected InternalServerErrorException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}