﻿namespace Coscine.Api.Core.Entities.Exceptions.InternalServerError;

public class NegativeQuotaInternalServerErrorException : InternalServerErrorException
{
    public NegativeQuotaInternalServerErrorException(Guid projectId, Guid resourceTypeId) : base($"The free quota for resource type '{resourceTypeId}' in project '{projectId}' cannot be negative.")
    {
    }

    public NegativeQuotaInternalServerErrorException(string? message) : base(message)
    {
    }

    protected NegativeQuotaInternalServerErrorException() : base(null)
    {
    }

    protected NegativeQuotaInternalServerErrorException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}