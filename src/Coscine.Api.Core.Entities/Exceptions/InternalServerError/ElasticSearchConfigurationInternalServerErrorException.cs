﻿namespace Coscine.Api.Core.Entities.Exceptions.InternalServerError;

public class ElasticSearchConfigurationInternalServerErrorException : InternalServerErrorException
{
    public ElasticSearchConfigurationInternalServerErrorException()
        : base($"Could not load the ElasticSearch configuration.")
    { }
}