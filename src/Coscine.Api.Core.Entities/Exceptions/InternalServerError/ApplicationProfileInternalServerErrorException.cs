﻿namespace Coscine.Api.Core.Entities.Exceptions.InternalServerError;

public class ApplicationProfileInternalServerErrorException : InternalServerErrorException
{
    public ApplicationProfileInternalServerErrorException(Guid resourceId) : base($"The resource with id: {resourceId} has no application profile specified!")
    {
    }

    protected ApplicationProfileInternalServerErrorException() : base()
    { }

    protected ApplicationProfileInternalServerErrorException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}