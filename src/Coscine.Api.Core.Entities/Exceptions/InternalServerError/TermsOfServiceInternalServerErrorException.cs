﻿namespace Coscine.Api.Core.Entities.Exceptions.InternalServerError;

public class TermsOfServiceInternalServerErrorException : InternalServerErrorException
{
    public TermsOfServiceInternalServerErrorException()
        : base($"Could not find the current Terms of Service version or URL.")
    {
    }

    public TermsOfServiceInternalServerErrorException(string? message) : base(message)
    {
    }

    protected TermsOfServiceInternalServerErrorException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}