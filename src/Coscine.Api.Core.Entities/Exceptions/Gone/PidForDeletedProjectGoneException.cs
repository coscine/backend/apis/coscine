﻿namespace Coscine.Api.Core.Entities.Exceptions.Gone;

public class PidForDeletedProjectGoneException : GoneException
{
    public PidForDeletedProjectGoneException(string prefix, Guid id) : base($"The provided PID \"{prefix}/{id}\" belongs to a project that has been deleted by its owner.")
    {
    }

    protected PidForDeletedProjectGoneException() : base(null)
    {
    }

    protected PidForDeletedProjectGoneException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}