﻿namespace Coscine.Api.Core.Entities.Exceptions.Gone;

public class PidForDeletedResourceGoneException : GoneException
{
    public PidForDeletedResourceGoneException(string prefix, Guid id) : base($"The provided PID \"{prefix}/{id}\" belongs to a resource that has been deleted by its owner.")
    {
    }

    protected PidForDeletedResourceGoneException() : base(null)
    {
    }

    protected PidForDeletedResourceGoneException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}