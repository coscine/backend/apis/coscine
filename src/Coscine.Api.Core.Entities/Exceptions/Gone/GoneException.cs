﻿namespace Coscine.Api.Core.Entities.Exceptions.Gone;

public abstract class GoneException : Exception, ICoscineHttpError
{
    public int StatusCode => 410;

    public string Type => "https://datatracker.ietf.org/doc/html/rfc7231#section-6.5.9";

    public string Title => "Gone";

    protected GoneException(string? message) : base(message)
    {
    }

    protected GoneException() : base(null)
    {
    }

    protected GoneException(string? message, Exception? innerException) : base(message, innerException)
    {
    }
}