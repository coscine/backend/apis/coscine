﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Entities.EventArgs;

public class PidContactEventArgs
{
    public string Pid { get; set; } = null!;
    public string EnquirerName { get; set; } = null!;
    public string EnquirerEmail { get; set; } = null!;
    public string EnquirerMessage { get; set; } = null!;
    public string? ResourceName { get; set; }
    public string ProjectName { get; set; } = null!;
    public IEnumerable<User> Recipients { get; set; } = null!;
}