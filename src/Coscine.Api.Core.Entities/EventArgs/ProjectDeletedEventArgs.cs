﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Entities.EventArgs;

public class ProjectDeletedEventArgs
{
    public Project Project { get; set; } = null!;
    public IEnumerable<User> Recipients { get; set; } = null!;
}