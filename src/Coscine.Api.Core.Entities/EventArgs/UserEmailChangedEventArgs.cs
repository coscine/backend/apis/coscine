﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Entities.EventArgs;

public class UserEmailChangedEventArgs
{
    /// <summary>
    /// Object containing the information about the newest change.
    /// </summary>
    public required ContactChange ContactChange { get; set; }

    /// <summary>
    /// The base url or domain of the application to use in the confirmation link generation.
    /// </summary>
    public required string BaseUrl { get; set; }
}