using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;

namespace Coscine.Api.Core.Entities.EventArgs;

public class ProjectResponsibleOrganizationChangedEventArgs {
    public Project Project { get; set; } = null!;
    public IEnumerable<User> Recipients { get; set; } = null!;
    public Organization? OldOrganization { get; set; } = null!;
    public Organization? NewOrganization { get; set; } = null!;
}