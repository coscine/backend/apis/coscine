﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Entities.EventArgs;

public class ResourceCreatedEventArgs
{
    public Resource Resource { get; set; } = null!;
    public User Creator { get; set; } = null!;
}