﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Entities.EventArgs;

public class ResourceDeletedEventArgs
{
    public Resource Resource { get; set; } = null!;
    public IEnumerable<User> Recipients { get; set; } = null!;
}