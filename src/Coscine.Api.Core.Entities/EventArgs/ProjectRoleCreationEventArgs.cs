﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Entities.EventArgs;

public class ProjectRoleCreationEventArgs
{
    public Project Project { get; set; } = null!;
    public User User { get; set; } = null!;
}