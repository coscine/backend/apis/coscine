﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Entities.EventArgs;

public class ProjectInvitationCreationEventArgs
{
    public required Project Project { get; set; } = null!;
    public required User User { get; set; } = null!;
    public required Invitation Invitation { get; set; } = null!;
}