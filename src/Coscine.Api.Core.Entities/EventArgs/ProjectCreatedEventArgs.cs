﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Entities.EventArgs;

public class ProjectCreatedEventArgs
{
    public Project Project { get; set; } = null!;
    public User Creator { get; set; } = null!;
}