﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Entities.EventArgs;

public class ProjectRoleUpdateEventArgs
{
    public ProjectRole ProjectRole { get; set; } = null!;
}