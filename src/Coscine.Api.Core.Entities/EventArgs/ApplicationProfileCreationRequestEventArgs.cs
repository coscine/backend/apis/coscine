﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Entities.EventArgs;

public class APCreationRequestedEventArgs
{
    public required User Creator { get; set; }
    public required string ApplicationProfileName { get; set; }
    public required Uri MergeRequestURL { get; set; }
}