﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Entities.EventArgs;

public class ProjectRoleDeletionEventArgs
{
    public ProjectRole ProjectRole { get; set; } = null!;
}