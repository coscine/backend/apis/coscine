﻿// Ignore Spelling: mvc

using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.Options;
using VDS.RDF;

namespace Coscine.Api.ObjectResultExecutors;

internal class ResponseEnvelopeResultExecutor(OutputFormatterSelector formatterSelector, IHttpResponseStreamWriterFactory writerFactory, ILoggerFactory loggerFactory, IOptions<MvcOptions> mvcOptions) : ObjectResultExecutor(formatterSelector, writerFactory, loggerFactory, mvcOptions)
{

    public override Task ExecuteAsync(ActionContext context, ObjectResult result)
    {
        if(result.Value is IGraph)
        {
            return base.ExecuteAsync(context, result);
        }

        if (result.Value is object value)
        {
            var httpContext = context.HttpContext;

            // Convert the result to a standard response
            var internalResponse = new InternalResponse<object>()
            {
                Data = value,
                StatusCode = result.StatusCode,
                TraceId = httpContext.TraceIdentifier,
            };

            // Handle paged results
            if (value is IPagedEnumerable pagedEnumerable && pagedEnumerable.Pagination is not null)
            {
                // Add paging Information
                internalResponse.Pagination = pagedEnumerable.Pagination;

                httpContext.Response.Headers["X-Pagination"] = pagedEnumerable?.Pagination.ToString();

                if (pagedEnumerable is not null)
                {
                    var pagination = pagedEnumerable.Pagination;
                    httpContext.Response.Headers["X-Pagination-CurrentPage"] = pagination.CurrentPage.ToString();
                    httpContext.Response.Headers["X-Pagination-TotalPages"] = pagination.TotalPages.ToString();
                    httpContext.Response.Headers["X-Pagination-PageSize"] = pagination.PageSize.ToString();
                    httpContext.Response.Headers["X-Pagination-TotalCount"] = pagination.TotalCount.ToString();
                    httpContext.Response.Headers["X-Pagination-HasPrevious"] = pagination.HasPrevious.ToString();
                    httpContext.Response.Headers["X-Pagination-HasNext"] = pagination.HasNext.ToString();
                }
            }

            if (value is ISearchCategories searchCategories && searchCategories.Categories is not null)
            {
                internalResponse.Categories = searchCategories.Categories;

                foreach (var category in searchCategories.Categories)
                {
                    httpContext.Response.Headers["X-Category-Count-" + category.Name] = category.Count.ToString();
                }
            }

            // Update the result to the internal response
            result.Value = internalResponse;
        }

        return base.ExecuteAsync(context, result);
    }
}