using Coscine.Api.Core.Repository.Contracts;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using VDS.RDF.Query;

namespace Coscine.Api.Health;

/// <summary>
/// Performs a health check on a Virtuoso database using SPARQL queries.
/// </summary>
public class VirtuosoHealthCheck(IRdfRepositoryBase rdfRepositoryBase) : IHealthCheck
{
    /// <summary>
    /// Checks the health of the Virtuoso database by executing a SPARQL query.
    /// </summary>
    /// <param name="context">The health check context.</param>
    /// <param name="cancellationToken">A cancellation token to cancel the operation.</param>
    /// <returns>A task that represents the asynchronous operation. The task result contains a <see cref="HealthCheckResult"/> indicating the health status.</returns>
    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        var isHealthy = true;

        try
        {
            var query = new SparqlParameterizedString
            {
                // Count triples of the default graph
                CommandText = @"SELECT (COUNT(?s) AS ?count) WHERE { ?s ?p ?o }"
            };

            await rdfRepositoryBase.RunQueryAsync(query);
        }
        catch (Exception)
        {
            isHealthy = false;
        }


        if (isHealthy)
        {
            return HealthCheckResult.Healthy("The Virtuoso database is reachable.");
        }

        return new HealthCheckResult(context.Registration.FailureStatus, "The Virtuoso database is not reachable.");
    }
}