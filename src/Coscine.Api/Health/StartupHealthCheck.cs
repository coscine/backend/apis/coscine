using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Coscine.Api.Health;

/// <summary>
/// Provides a health check that monitors whether a startup task has completed.
/// </summary>
public class StartupHealthCheck : IHealthCheck
{
    /// <summary>
    /// A flag indicating whether the startup task has completed.
    /// </summary>
    private volatile bool _isReady;

    /// <summary>
    /// Gets or sets a value indicating whether the startup task has been completed.
    /// </summary>
    public bool StartupCompleted
    {
        get => _isReady;
        set => _isReady = value;
    }

    /// <summary>
    /// Checks the health of the application by determining whether the startup task has completed.
    /// </summary>
    /// <param name="context">The context for the health check.</param>
    /// <param name="cancellationToken">A token that can be used to cancel the health check operation.</param>
    /// <returns>A <see cref="HealthCheckResult"/> indicating whether the application is healthy.</returns>
    public Task<HealthCheckResult> CheckHealthAsync(
        HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        if (StartupCompleted)
        {
            return Task.FromResult(HealthCheckResult.Healthy("The startup task has completed."));
        }

        return Task.FromResult(HealthCheckResult.Unhealthy("The startup task is still running."));
    }
}