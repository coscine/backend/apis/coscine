﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Coscine.Api.ActionFilters;

/// <summary>
/// Specifies that the action method accepts multipart/form-data requests.
/// If the request does not have a form content type or does not start with "multipart/form-data",
/// returns a 415 Unsupported Media Type status code.
/// </summary>
[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
public class MultipartFormDataAttribute : ActionFilterAttribute
{
    /// <summary>
    /// Called before the action method is executed.
    /// Checks if the request has a form content type and starts with "multipart/form-data".
    /// If not, returns a 415 Unsupported Media Type status code.
    /// </summary>
    /// <param name="context">The action executing context.</param>
    public override void OnActionExecuting(ActionExecutingContext context)
    {
        var request = context.HttpContext.Request;
        if (request.HasFormContentType
            && request.ContentType?.StartsWith("multipart/form-data", StringComparison.OrdinalIgnoreCase) == true)
        {
            return;
        }
        context.Result = new StatusCodeResult(StatusCodes.Status415UnsupportedMediaType);
    }
}
