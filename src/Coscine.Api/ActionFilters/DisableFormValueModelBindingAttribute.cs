﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Coscine.Api.ActionFilters;

/// <summary>
/// Specifies that form value model binding should be disabled for a controller or action method.
/// </summary>
[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class DisableFormValueModelBindingAttribute : Attribute, IResourceFilter
{
    /// <summary>
    /// Called at the beginning of the resource execution.
    /// Removes form value provider factories to disable form value model binding.
    /// </summary>
    /// <param name="context">The resource executing context.</param>
    public void OnResourceExecuting(ResourceExecutingContext context)
    {
        // Remove form value provider factories
        var factories = context.ValueProviderFactories;
        factories.RemoveType<FormValueProviderFactory>();
        factories.RemoveType<FormFileValueProviderFactory>();
        factories.RemoveType<JQueryFormValueProviderFactory>();
    }

    /// <summary>
    /// Called at the end of the resource execution.
    /// </summary>
    /// <param name="context">The resource executed context.</param>
    public void OnResourceExecuted(ResourceExecutedContext context)
    {
        // No implementation needed for this method
    }
}