﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Coscine.Api.ActionFilters;

/// <summary>
/// An action filter for validating request DTOs to ensure they are not null before an API call is processed.
/// This filter checks if any action argument is a DTO and verifies it is not null, additionally ensuring the model state is valid.
/// </summary>
public class ValidationFilter : IActionFilter
{
    /// <inheritdoc/>
    public void OnActionExecuted(ActionExecutedContext context)
    { }

    /// <inheritdoc/>
    public void OnActionExecuting(ActionExecutingContext context)
    {
        var action = context.RouteData.Values["action"];
        var controller = context.RouteData.Values["controller"];

        var parameters = context.ActionArguments;

        foreach (var param in parameters)
        {
            if (param.Value?.ToString()?.Contains("dto", StringComparison.OrdinalIgnoreCase) == true)
            {
                // DTO parameters can't be null
                if (param.Value is null)
                {
                    context.Result = new BadRequestObjectResult($"Object is null. Controller: {controller}, action: {action}");

                    return;
                }
            }
        }

        // Model has to be valid
        if (!context.ModelState.IsValid)
        {
            context.Result = new UnprocessableEntityObjectResult(context.ModelState);
        }
    }
}