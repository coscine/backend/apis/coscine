﻿namespace Coscine.Api.ActionFilters;

/// <summary>
/// Specifies that the action method allows file uploads and should be displayed with an upload button in Swagger UI.
/// </summary>
[AttributeUsage(AttributeTargets.Method)]
public class SwaggerFileUploadAttribute : Attribute;