﻿namespace Coscine.Api.ActionFilters;

using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Microsoft.AspNetCore.Mvc.Filters;

/// <summary>
/// Specifies that the action method requires a valid Content-Length header in the request.
/// Throws a <see cref="ContentLengthRequestHeaderMissingBadRequestException"/> if the Content-Length header is missing or negative.
/// </summary>
public class RequireContentLengthAttribute : ActionFilterAttribute
{
    /// <summary>
    /// Called before the action method is executed.
    /// Checks if the Content-Length header in the request is missing or negative.
    /// Throws a <see cref="ContentLengthRequestHeaderMissingBadRequestException"/> if the condition is met.
    /// </summary>
    /// <param name="context">The action executing context.</param>
    public override void OnActionExecuting(ActionExecutingContext context)
    {
        if (context.HttpContext.Request.ContentLength < 0)
        {
            throw new ContentLengthRequestHeaderMissingBadRequestException();
        }
    }
}