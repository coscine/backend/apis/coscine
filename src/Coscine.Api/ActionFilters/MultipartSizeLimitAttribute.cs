﻿// Ignore Spelling: Multipart

namespace Coscine.Api.ActionFilters;

using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Microsoft.AspNetCore.Mvc.Filters;

/// <summary>Check the upload size limit.</summary>
public class MultipartSizeLimitAttribute : IActionFilter
{
    private readonly int _maxSizeInBytes = int.MaxValue;

    /// <summary>Called before the action executes, after model binding is complete.</summary>
    /// <param name="context">The <see cref="T:Microsoft.AspNetCore.Mvc.Filters.ActionExecutingContext">ActionExecutingContext</see>.</param>
    public void OnActionExecuting(ActionExecutingContext context)
    {
        // Check if of type multipart/form-data.
        if (!context.HttpContext.Request.HasFormContentType)
        {
            throw new NotAMultipartBadRequestException();
        }

        // Check, if the model state is valid.
        // Check, if any error in the model state comes from exceeding the limit.
        // A bit convoluted, but found no other way.
        if (!context.ModelState.IsValid
            && context.ModelState.Any(x => x.Value?.Errors.Any(e => e.ErrorMessage.StartsWith("Failed to read the request form. Multipart body length limit")) == true))
        {
            throw new MultipartBodyLengthExceededBadRequestException(_maxSizeInBytes);
        }
    }

    /// <summary>Called after the action executes, before the action result.</summary>
    /// <param name="context">The <see cref="T:Microsoft.AspNetCore.Mvc.Filters.ActionExecutedContext">ActionExecutedContext</see>.</param>
    public void OnActionExecuted(ActionExecutedContext context)
    {
        // No action needed after the execution.
    }
}