﻿using Coscine.Api.Core.Repository.Contracts;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.Net.Http.Headers;
using System.Text;
using VDS.RDF;

namespace Coscine.Api.OutputFormatters;

/// <summary>
/// Custom output formatter for handling JSON-LD format responses.
/// </summary>
public class JsonLdOutputFormatter : TextOutputFormatter
{
    /// <summary>
    /// Initializes a new instance of the <see cref="JsonLdOutputFormatter"/> class.
    /// Configures supported media types and encodings for JSON-LD.
    /// </summary>
    public JsonLdOutputFormatter()
    {
        // Supported media types
        SupportedMediaTypes.Add(MediaTypeHeaderValue.Parse("application/ld+json"));

        // Supported encodings
        SupportedEncodings.Add(Encoding.UTF8);
        SupportedEncodings.Add(Encoding.Unicode);
    }

    /// <summary>
    /// Determines if the given <paramref name="type"/> can be written by this formatter.
    /// </summary>
    /// <param name="type">The type of object being serialized.</param>
    /// <returns>
    ///   <c>true</c> if the type is assignable to <see cref="IGraph"/>; otherwise, <c>false</c>.
    /// </returns>
    protected override bool CanWriteType(Type? type)
    {
        // Check if the type is a IGraph
        return typeof(IGraph).IsAssignableFrom(type);
    }

    /// <summary>
    /// Asynchronously writes the response body in JSON-LD format.
    /// </summary>
    /// <param name="context">The context in which the output is being formatted.</param>
    /// <param name="selectedEncoding">The encoding selected for the response.</param>
    /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
    /// <remarks>
    /// If the object is of type <see cref="IGraph"/>, it will be serialized into JSON-LD format
    /// using the <see cref="IRdfRepositoryBase"/> service. Otherwise, an empty response is written.
    /// </remarks>
    public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context, Encoding selectedEncoding)
    {
        var httpContext = context.HttpContext;
        var serviceProvider = httpContext.RequestServices;
        var rdfRepositoryBase = serviceProvider.GetRequiredService<IRdfRepositoryBase>();

        if (context.Object is IGraph graph)
        {
            var graphString = rdfRepositoryBase.WriteGraph(graph, Core.Shared.Enums.RdfFormat.JsonLd);

            await httpContext.Response.WriteAsync(graphString, selectedEncoding);
        }
        else
        {
            await httpContext.Response.WriteAsync(string.Empty, selectedEncoding);
        }
    }
}
