using AutoMapper;

namespace Coscine.Api.MappingProfiles.Converters;

/// <summary>
/// Custom AutoMapper converter to convert <see cref="DateTime"/> with <see cref="DateTimeKind.Unspecified"/> to <see cref="DateTimeOffset"/> using the German time zone.
/// </summary>
/// <remarks>
/// All other <see cref="DateTime"/> values are internally will probably have the <see cref="DateTimeKind.Unspecified"/> kind, 
/// but will be understood as Local. This makes the conversion to <see cref="DateTimeOffset"/> ambiguous, therefore we need to explicitly set the time zone.
/// </remarks>
public class DateTimeToDateTimeOffsetConverter : ITypeConverter<DateTime, DateTimeOffset>
{
    private readonly TimeZoneInfo _germanTimeZone;

    /// <summary>
    /// Initializes a new instance of the <see cref="DateTimeToDateTimeOffsetConverter"/> class.
    /// </summary>
    public DateTimeToDateTimeOffsetConverter()
    {
        // Explicitly set the German time zone
        _germanTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time");
    }

    /// <inheritdoc />
    public DateTimeOffset Convert(DateTime source, DateTimeOffset destination, ResolutionContext context)
    {
        if (source.Kind == DateTimeKind.Unspecified)
        {
            // Convert assuming the German time zone
            return new DateTimeOffset(source, _germanTimeZone.GetUtcOffset(source));
        }

        // If already specified as Local or Utc, use the existing conversion methods
        return new DateTimeOffset(source);
    }
}
