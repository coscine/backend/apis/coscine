using AutoMapper;

namespace Coscine.Api.MappingProfiles.Converters;

/// <summary>
/// Custom AutoMapper converter to convert nullable <see cref="DateTime"/> with <see cref="DateTimeKind.Unspecified"/> to nullable <see cref="DateTimeOffset"/> using the German time zone.
/// </summary>
/// <remarks>
/// All other <see cref="DateTime"/> values are internally will probably have the <see cref="DateTimeKind.Unspecified"/> kind, 
/// but will be understood as Local. This makes the conversion to <see cref="DateTimeOffset"/> ambiguous, therefore we need to explicitly set the time zone.
/// </remarks>
public class NullableDateTimeToDateTimeOffsetConverter : ITypeConverter<DateTime?, DateTimeOffset?>
{
    private readonly TimeZoneInfo _germanTimeZone;

    /// <summary>
    /// Initializes a new instance of the <see cref="NullableDateTimeToDateTimeOffsetConverter"/> class.
    /// </summary>
    public NullableDateTimeToDateTimeOffsetConverter()
    {
        // Explicitly set the German time zone
        _germanTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time");
    }

    /// <inheritdoc />
    public DateTimeOffset? Convert(DateTime? source, DateTimeOffset? destination, ResolutionContext context)
    {
        if (!source.HasValue)
        {
            return null;
        }

        if (source.Value.Kind == DateTimeKind.Unspecified)
        {
            // Convert assuming the German time zone
            return new DateTimeOffset(source.Value, _germanTimeZone.GetUtcOffset(source.Value));
        }

        // If already specified as Local or Utc, use the existing conversion methods
        return new DateTimeOffset(source.Value);
    }
}