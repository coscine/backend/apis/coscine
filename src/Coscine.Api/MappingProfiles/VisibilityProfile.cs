﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="Visibility" /> class.</summary>
public class VisibilityProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="VisibilityProfile" /> class.</summary>
    public VisibilityProfile()
    {
        CreateMap<Visibility, VisibilityDto>();
        CreateMap<VisibilityForProjectManipulationDto, Visibility>();
        CreateMap<VisibilityForResourceManipulationDto, Visibility>();
    }
}