using AutoMapper;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ResourceTypeOptions;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType.ResourceTypeOptions;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="DataStorageInfo" /> class.</summary>
public class DatastorageProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="DatastorageProfile" /> class.</summary>
    public DatastorageProfile()
    {
        CreateMap<DataStorageInfo, ResourceTypeOptionsDto>()
            .ForMember(x => x.FileSystemStorage, opt => opt.MapFrom(entity => entity.FileSystemStorageInfo))
            .ForMember(x => x.GitLab, opt => opt.MapFrom(entity => entity.GitlabStorageInfo))
            .ForMember(x => x.RdsS3, opt => opt.MapFrom(entity => entity.RdsS3StorageInfo))
            .ForMember(x => x.Rds, opt => opt.MapFrom(entity => entity.RdsStorageInfo))
            .ForMember(x => x.RdsS3Worm, opt => opt.MapFrom(entity => entity.RdsS3WormStorageInfo));
        
        CreateMap<FileSystemStorageInfo, FileSystemStorageOptionsDto>();
        
        CreateMap<GitlabStorageInfo, GitLabOptionsDto>();
        CreateMap<RdsS3StorageInfo, RdsS3OptionsDto>();
        CreateMap<RdsS3WormStorageInfo, RdsS3WormOptionsDto>();
        CreateMap<RdsStorageInfo, RdsOptionsDto>();
        
        CreateMap<GitlabResourceTypeOptionsForCreationDto, CreateGitLabDataStorageOptions>()
            .ForMember(x => x.ProjectAccessToken, opt => opt.MapFrom(entity => entity.AccessToken))
            .ForMember(x => x.GitlabProjectId, opt => opt.MapFrom(entity => entity.ProjectId));
        CreateMap<ResourceTypeOptionsForCreationDto, CreateDataStorageParameters>()
            .ForMember(x => x.CreateGitLabDataStorageOptions, opt => opt.MapFrom(entity => entity.GitlabResourceTypeOptions));
    }
}