﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="Invitation" /> class.</summary>
public class InvitationProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="InvitationProfile" /> class.</summary>
    public InvitationProfile()
    {
        CreateMap<ProjectInvitationForProjectManipulationDto, Invitation>()
            .ForMember(i => i.InviteeEmail, opt => opt.MapFrom(dto => dto.Email))
            .ForMember(i => i.Role, opt => opt.MapFrom(dto => dto.RoleId));

        CreateMap<Invitation, ProjectInvitationDto>()
            .ForMember(dto => dto.ExpirationDate, opt => opt.MapFrom(i => i.Expiration))
            .ForMember(dto => dto.Email, opt => opt.MapFrom(i => i.InviteeEmail))
            .ForMember(dto => dto.Project, opt => opt.MapFrom(i => new ProjectMinimalDto { Id = i.Project }))
            .ForMember(dto => dto.Role, opt => opt.MapFrom(i => new RoleMinimalDto { Id = i.Role }))
            .ForMember(dto => dto.Issuer, opt => opt.Ignore());
    }
}