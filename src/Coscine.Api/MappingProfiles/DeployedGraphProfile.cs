﻿using AutoMapper;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="DeployedGraph" /> class.</summary>
public class DeployedGraphProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="DeployedGraphProfile" /> class.</summary>
    public DeployedGraphProfile()
    {
        CreateMap<DeployedGraph, DeployedGraphDto>();
    }
}