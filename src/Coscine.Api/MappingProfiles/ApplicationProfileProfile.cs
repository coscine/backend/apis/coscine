﻿using AutoMapper;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums.Utility;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="ApplicationProfile" /> class.</summary>
public class ApplicationProfileProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="ApplicationProfileProfile" /> class.</summary>
    public ApplicationProfileProfile()
    {
        CreateMap<ApplicationProfile, ApplicationProfileDto>()
            .ForMember(dto => dto.Definition, opt => opt.MapFrom(ap => new RdfDefinitionDto { Content = ap.Definition, Type = ap.Format.HasValue ? ap.Format.Value.GetEnumMemberValue() : null }))
            .ForMember(dto => dto.Definition, opt => opt.Condition(ap => ap.Definition != null && ap.Format != null));
    }
}
