﻿using AutoMapper;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="Maintenance" /> class.</summary>
public class MaintenanceProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="MaintenanceProfile" /> class.</summary>
    public MaintenanceProfile()
    {
        CreateMap<Maintenance, MaintenanceDto>()
            .ForMember(dto => dto.Href, opt => opt.MapFrom(m => m.Url));
    }
}