﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="User" /> class.</summary>
public class UserProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="UserProfile" /> class.</summary>
    public UserProfile()
    {
        CreateMap<UserForUpdateDto, User>()
            .ForMember(u => u.Givenname, opt => opt.MapFrom(dto => dto.GivenName))
            .ForMember(u => u.Surname, opt => opt.MapFrom(dto => dto.FamilyName))
            .ForMember(u => u.EmailAddress, opt => opt.MapFrom(dto => dto.Email))
            .ForMember(u => u.Title, opt => opt.Ignore())
            .ForMember(u => u.Language, opt => opt.Ignore())
            .ForMember(u => u.UserDisciplines, opt => opt.Ignore());

        CreateMap<User, UserDto>()
            .ForMember(dto => dto.GivenName, opt => opt.MapFrom(u => u.Givenname))
            .ForMember(dto => dto.FamilyName, opt => opt.MapFrom(u => u.Surname))
            .ForMember(dto => dto.LatestActivity, opt => opt.MapFrom(u => u.LatestActivity))
            .ForMember(dto => dto.Emails, opt => opt.MapFrom(u => new List<UserEmailDto>() {
                new()
                {
                    // NOTE: Adjust this mapping, once multi-email support is implemented
                    Email = u.EmailAddress,
                    IsConfirmed = !string.IsNullOrWhiteSpace(u.EmailAddress) && !u.ContactChanges.Any(cc => cc.NewEmail == u.EmailAddress),
                    IsPrimary = true
                }
            }))
            .ForMember(dto => dto.DisplayName, opt => opt.MapFrom(u => u.Title != null
                ? string.Join(" ", u.Title.DisplayName, u.Givenname, u.Surname).Trim()
                : string.Join(" ", u.Givenname, u.Surname).Trim())
            )
            .ForMember(dto => dto.Disciplines, opt => opt.MapFrom(u => u.UserDisciplines.Select(ud => ud.Discipline)))
            .ForMember(dto => dto.Identities, opt => opt.MapFrom(u => u.ExternalIds.Select(eid => eid.ExternalAuthenticator)));

        CreateMap<User, UserOrganizationDto>()
            .ForMember(dto => dto.DisplayName, opt => opt.MapFrom(e => e.Organization))
            .ForMember(dto => dto.ReadOnly, opt => opt.MapFrom(_ => false)); // Value comes from SQL DB => ORCiD => Editable

        CreateMap<User, PublicUserDto>()
            .ForMember(dto => dto.GivenName, opt => opt.MapFrom(u => u.Givenname))
            .ForMember(dto => dto.FamilyName, opt => opt.MapFrom(u => u.Surname))
            .ForMember(dto => dto.Email, opt => opt.MapFrom(u => u.EmailAddress))
            .ForMember(dto => dto.DisplayName, opt => opt.MapFrom(u => u.Title != null
                ? string.Join(" ", u.Title.DisplayName, u.Givenname, u.Surname).Trim()
                : string.Join(" ", u.Givenname, u.Surname).Trim())
            );

        CreateMap<UserForUpdateDto, ContactChange>()
            .ForMember(cc => cc.NewEmail, opt => opt.MapFrom(dto => dto.Email))
            .ForMember(cc => cc.EditDate, opt => opt.MapFrom(_ => DateTime.Now)) // Consider using UTC
            .ForMember(cc => cc.ConfirmationToken, opt => opt.MapFrom(_ => Guid.NewGuid()));

        CreateMap<UserForUpdateDto, Tosaccepted>();
        CreateMap<ExternalAuthenticator, IdentityProviderDto>();
    }
}