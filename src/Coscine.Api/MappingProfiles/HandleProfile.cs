﻿using AutoMapper;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="HandleValue" /> and <see cref="Handle" /> classes.</summary>
public class HandleProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="HandleProfile" /> class.</summary>
    public HandleProfile()
    {
        CreateMap<HandleValue, HandleValueDto>();
        CreateMap<Handle, HandleDto>();
        CreateMap<HandleForUpdateDto, Handle>();
        CreateMap<HandleValueForUpdateDto, HandleValue>();
    }
}