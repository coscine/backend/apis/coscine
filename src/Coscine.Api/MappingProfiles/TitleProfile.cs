﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="Title" /> class.</summary>
public class TitleProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="TitleProfile" /> class.</summary>
    public TitleProfile()
    {
        CreateMap<Title, TitleDto>();
        CreateMap<TitleForUserManipulationDto, Title>();
    }
}