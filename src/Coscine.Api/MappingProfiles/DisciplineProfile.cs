﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="Discipline" /> class.</summary>
public class DisciplineProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="DisciplineProfile" /> class.</summary>
    public DisciplineProfile()
    {
        CreateMap<Discipline, DisciplineDto>()
            .ForMember(dto => dto.Uri, opt => opt.MapFrom(d => d.Url));
    }
}