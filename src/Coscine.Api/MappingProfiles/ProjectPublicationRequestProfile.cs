﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>
/// The mapping profiles for the <see cref="ProjectPublicationRequest" /> and <see cref="PublicationAdvisoryService"/> classes.
/// </summary>
public class ProjectPublicationRequestProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="ProjectPublicationRequestProfile" /> class.</summary>
    public ProjectPublicationRequestProfile()
    {
        CreateMap<ProjectPublicationRequest, ProjectPublicationRequestDto>()
            .ForMember(dto => dto.Project, opt => opt.MapFrom(ppr => new ProjectMinimalDto { Id = ppr.ProjectId }))
            .ForMember(dto => dto.Creator, opt => opt.MapFrom(ppr => new UserMinimalDto { Id = ppr.CreatorId }))
            .ForMember(dto => dto.Resources, opt => opt.MapFrom(ppr => ppr.Resources.Select(r => new ResourceMinimalDto { Id = r.Id })));
        
        CreateMap<PublicationRequestForCreationDto, ProjectPublicationRequest>()
            .ForMember(ppr => ppr.PublicationServiceRorId, opt => opt.MapFrom(dto => new Uri(dto.PublicationServiceRorId, UriKind.Absolute)))
            .ForMember(ppr => ppr.Resources, opt => opt.Ignore()); // Will map manually

        CreateMap<PublicationAdvisoryService, PublicationAdvisoryServiceDto>();
    }
}
