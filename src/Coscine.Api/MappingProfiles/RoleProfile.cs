﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="Role" /> class.</summary>
public class RoleProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="RoleProfile" /> class.</summary>
    public RoleProfile()
    {
        CreateMap<Role, RoleDto>();
    }
}