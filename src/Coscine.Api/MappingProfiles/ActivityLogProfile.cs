﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="ActivityLog" /> class.</summary>
public class ActivityLogProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="ActivityLogProfile" /> class.</summary>
    public ActivityLogProfile()
    {
        CreateMap<ActivityLog, ActivityLogDto>();
    }
}