﻿using AutoMapper;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="SearchResult" /> class.</summary>
public class SearchResultProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="SearchResultProfile" /> class.</summary>
    public SearchResultProfile()
    {
        CreateMap<SearchResult, SearchResultDto>();
    }
}