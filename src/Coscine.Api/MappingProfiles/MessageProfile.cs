using AutoMapper;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="MessageProfile" /> class.</summary>
public class MessageProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="MessageProfile" /> class.</summary>
    public MessageProfile()
    {
        CreateMap<Message, MessageDto>()
            .ForMember(dto => dto.Id, opt => opt.MapFrom(m => $"int-{m.Id}"))
            .ForMember(dto => dto.StartDate, opt => opt.MapFrom(m => m.StartDate))
            .ForMember(dto => dto.EndDate, opt => opt.MapFrom(m => m.EndDate));

        CreateMap<NocTicket, MessageDto>()
            .ForMember(dto => dto.Id, opt => opt.MapFrom(t => $"noc-{t.Id}"))
            .ForMember(dto => dto.Title, opt => opt.MapFrom(t => t.Title))
            .ForMember(dto => dto.Type, opt => opt.MapFrom(t => t.Type))
            .ForMember(dto => dto.Href, opt => opt.MapFrom(t => t.Href))
            .ForMember(dto => dto.StartDate, opt => opt.MapFrom(t => t.StartAt))
            .ForMember(dto => dto.EndDate, opt => opt.MapFrom(t => t.EndAt))
            .ForMember(dto => dto.Body, opt => opt.Ignore());
    }
}