﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ResourceTypeOptions;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType.ResourceTypeOptions;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="ResourceType" /> class.</summary>
public class ResourceTypeProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="ResourceTypeProfile" /> class.</summary>
    public ResourceTypeProfile()
    {
        CreateMap<ResourceType, ResourceTypeDto>()
            .ForMember(dto => dto.GeneralType, opt => opt.MapFrom(rt => rt.Type))
            .ForMember(dto => dto.Options, opt => opt.Ignore()); // ResourceTypeOptionsDto is manually set in a separate step

        CreateMap<ColumnsObject, ResourceContentPageColumnsDto>();
        CreateMap<EntriesView, ResourceContentPageEntriesViewDto>();
        CreateMap<MetadataView, ResourceContentPageMetadataViewDto>();
        CreateMap<ResourceContent, ResourceContentPageDto>();
        CreateMap<ResourceTypeInformation, ResourceTypeInformationDto>()
            .ForMember(dto => dto.ResourceCreation, opt => opt.Ignore());

        CreateMap<GitlabResourceType, GitLabOptionsDto>()
            .ForMember(dto => dto.RepoUrl, opt => opt.MapFrom(rt => rt.RepoUrl))
            .ForMember(dto => dto.ProjectId, opt => opt.MapFrom(rt => rt.GitlabProjectId))
            .ForMember(dto => dto.Branch, opt => opt.MapFrom(rt => rt.Branch))
            .ForMember(dto => dto.AccessToken, opt => opt.MapFrom(rt => rt.ProjectAccessToken));
        CreateMap<LinkedResourceType, LinkedDataOptionsDto>();
        CreateMap<RdsResourceType, RdsOptionsDto>()
            .ForMember(dto => dto.BucketName, opt => opt.MapFrom(rt => rt.BucketName))
            .ForMember(dto => dto.Size, opt => opt.Ignore());
        CreateMap<RdsS3ResourceType, RdsS3OptionsDto>()
            .ForMember(dto => dto.BucketName, opt => opt.MapFrom(rt => rt.BucketName))
            .ForMember(dto => dto.AccessKeyRead, opt => opt.MapFrom(rt => rt.AccessKeyRead))
            .ForMember(dto => dto.AccessKeyWrite, opt => opt.MapFrom(rt => rt.AccessKeyWrite))
            .ForMember(dto => dto.SecretKeyRead, opt => opt.MapFrom(rt => rt.SecretKeyRead))
            .ForMember(dto => dto.SecretKeyWrite, opt => opt.MapFrom(rt => rt.SecretKeyWrite))
            .ForMember(dto => dto.Endpoint, opt => opt.MapFrom(rt => rt.Endpoint))
            .ForMember(dto => dto.Size, opt => opt.Ignore());
        CreateMap<RdsS3WormResourceType, RdsS3WormOptionsDto>()
            .ForMember(dto => dto.BucketName, opt => opt.MapFrom(rt => rt.BucketName))
            .ForMember(dto => dto.AccessKeyRead, opt => opt.MapFrom(rt => rt.AccessKeyRead))
            .ForMember(dto => dto.AccessKeyWrite, opt => opt.MapFrom(rt => rt.AccessKeyWrite))
            .ForMember(dto => dto.SecretKeyRead, opt => opt.MapFrom(rt => rt.SecretKeyRead))
            .ForMember(dto => dto.SecretKeyWrite, opt => opt.MapFrom(rt => rt.SecretKeyWrite))
            .ForMember(dto => dto.Size, opt => opt.Ignore());

        CreateMap<GitlabResourceTypeOptionsForManipulationDto, GitlabResourceType>()
            .ForMember(grt => grt.ProjectAccessToken, opt => opt.MapFrom(dto => dto.AccessToken));

        CreateMap<GitLabApiClient.Models.Projects.Responses.Project, GitlabProjectDto>();
        CreateMap<GitLabApiClient.Models.Branches.Responses.Branch, GitlabBranchDto>();
    }
}