﻿using AutoMapper;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using System.Security.Cryptography;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="Provenance" /> classes.</summary>
public class ProvenanceProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="ProvenanceProfile" /> class.</summary>
    public ProvenanceProfile()
    {
        CreateMap<ProvenanceParametersDto, Provenance>()
            .ForMember(mt => mt.MetadataExtractorVersion, opt => opt.MapFrom(dto => dto.MetadataExtractorVersion))
            .ForMember(mt => mt.HashParameters, opt => opt.MapFrom(dto => dto.HashParameters != null ? new HashParameters()
            {
                AlgorithmName = new HashAlgorithmName(dto.HashParameters.AlgorithmName),
                Value = dto.HashParameters.Value,
            } : null))
            .ForMember(mt => mt.Variants, opt => opt.MapFrom(dto => dto.Variants.Select((v) => new Provenance.Variant(v.GraphName, v.Similarity))))
            .ForMember(mt => mt.WasInvalidatedBy, opt => opt.MapFrom(dto => dto.WasInvalidatedBy))
            .ForMember(mt => mt.WasRevisionOf, opt => opt.MapFrom(dto => dto.WasRevisionOf));

        CreateMap<Provenance, ProvenanceDto>()
            .ForMember(mt => mt.Id, opt => opt.MapFrom(dto => dto.Id))
            .ForMember(mt => mt.HashParameters, opt => opt.MapFrom(dto => dto.HashParameters != null ? new HashParametersDto()
            {
                AlgorithmName = dto.HashParameters.AlgorithmName.Name ?? "NaN",
                Value = dto.HashParameters.Value,
            } : null))
            .ForMember(mt => mt.Variants, opt => opt.MapFrom(dto => dto.Variants.Select((v) => new VariantDto(v.GraphName, v.Similarity))))
            .ForMember(mt => mt.WasInvalidatedBy, opt => opt.MapFrom(dto => dto.WasInvalidatedBy))
            .ForMember(mt => mt.WasRevisionOf, opt => opt.MapFrom(dto => dto.WasRevisionOf));
    }
}
