﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="Language" /> class.</summary>
public class LanguageProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="LanguageProfile" /> class.</summary>
    public LanguageProfile()
    {
        CreateMap<Language, LanguageDto>();
        CreateMap<LanguageForUserManipulationDto, Language>();
    }
}