﻿using AutoMapper;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="Organization" /> class.</summary>
public class OrganizationProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="OrganizationProfile" /> class.</summary>
    public OrganizationProfile()
    {
        CreateMap<Organization, OrganizationDto>();
        CreateMap<Organization, UserOrganizationDto>()
            .ForMember(dto => dto.ReadOnly, opt => opt.MapFrom(_ => true)); // Value comes from Graph => Shibboleth => Read Only
        CreateMap<Organization, ProjectOrganizationDto>();
    }
}