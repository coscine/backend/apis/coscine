﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="ApiToken" /> class.</summary>
public class ApiTokenProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="ApiTokenProfile" /> class.</summary>
    public ApiTokenProfile()
    {
        CreateMap<ApiToken, ApiTokenDto>()
            .ForMember(x => x.ExpiryDate, opt => opt.MapFrom(entity => entity.Expiration))
            .ForMember(x => x.CreationDate, opt => opt.MapFrom(entity => entity.IssuedAt))
            .ForMember(x => x.Owner, opt => opt.MapFrom(entity => new UserMinimalDto { Id = entity.UserId }))
            .ForMember(x => x.Token, opt => opt.Ignore());
    }
}