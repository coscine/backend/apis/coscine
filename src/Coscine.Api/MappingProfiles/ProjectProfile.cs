﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="Project" /> class.</summary>
public class ProjectProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="ProjectProfile" /> class.</summary>
    public ProjectProfile()
    {
        CreateMap<Project, ProjectDto>()
            .ForMember(dto => dto.Name, opt => opt.MapFrom(p => p.ProjectName))
            .ForMember(dto => dto.Creator, opt => opt.MapFrom(p => p.Creator.HasValue ? new UserMinimalDto { Id = p.Creator.Value } : null))
            .ForMember(dto => dto.CreationDate, opt => opt.MapFrom(p => p.DateCreated))
            .ForMember(dto => dto.Disciplines, opt => opt.MapFrom(p => p.ProjectDisciplines.Select(pd => pd.Discipline)))
            .ForMember(dto => dto.Keywords, opt => opt.MapFrom(p => p.Keywords == null ? null : p.Keywords.Split(';', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries)))
            .ForMember(dto => dto.Parent, opt => opt.MapFrom(p => p.SubProjectSubProjectNavigations.Count > 0 ? new ProjectMinimalDto { Id = p.SubProjectSubProjectNavigations.First().ProjectId } : null))
            .ForMember(dto => dto.Organizations, opt => opt.Ignore()); // Ignoring, because the organizations need to be fetched properly in an extra step. SQL DB does not serve organization names.

        CreateMap<Project, ProjectAdminDto>()
            .ForMember(dto => dto.Name, opt => opt.MapFrom(p => p.ProjectName))
            .ForMember(dto => dto.Creator, opt => opt.MapFrom(p => p.Creator.HasValue ? new UserMinimalDto { Id = p.Creator.Value } : null))
            .ForMember(dto => dto.CreationDate, opt => opt.MapFrom(p => p.DateCreated))
            .ForMember(dto => dto.Disciplines, opt => opt.MapFrom(p => p.ProjectDisciplines.Select(pd => pd.Discipline)))
            .ForMember(dto => dto.Keywords, opt => opt.MapFrom(p => p.Keywords == null ? null : p.Keywords.Split(';', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries)))
            .ForMember(dto => dto.Parent, opt => opt.MapFrom(p => p.SubProjectSubProjectNavigations.Count > 0 ? new ProjectMinimalDto { Id = p.SubProjectSubProjectNavigations.First().ProjectId } : null))
            .ForMember(dto => dto.Organizations, opt => opt.Ignore()) // Ignoring, because the organizations need to be fetched properly in an extra step. SQL DB does not serve organization names.
            .ForMember(dto => dto.ProjectResources, opt => opt.MapFrom(p => p.ProjectResources.Select((pr) => new ProjectResourceMinimalDto { ProjectId = pr.ProjectId, ResourceId = pr.ResourceId })))
            .ForMember(dto => dto.ProjectRoles, opt => opt.MapFrom(p => p.ProjectRoles.Select((pr) => new ProjectRoleMinimalDto { ProjectId = pr.ProjectId, RoleId = pr.RoleId, UserId = pr.UserId })))
            .ForMember(dto => dto.ProjectQuota, opt => opt.Ignore()) // Ignoring, because the quota needs to be fetched properly in an extra step.
            .ForMember(dto => dto.PublicationRequests, opt => opt.Ignore()); // Ignoring, because the publication requests need to be fetched properly in an extra step

        CreateMap<ProjectForUpdateDto, Project>()
            .ForMember(p => p.ProjectName, opt => opt.MapFrom(dto => dto.Name))
            .ForMember(p => p.ProjectDisciplines, opt => opt.Ignore())
            .ForMember(p => p.ProjectInstitutes, opt => opt.Ignore())
            .ForMember(p => p.Keywords, opt => opt.MapFrom(dto => dto.Keywords == null ? null : string.Join(';', dto.Keywords)))
            .ForMember(p => p.Visibility, opt => opt.Ignore());

        CreateMap<ProjectForCreationDto, Project>()
            .ForMember(p => p.ProjectName, opt => opt.MapFrom(dto => dto.Name))
            .ForMember(p => p.ProjectDisciplines, opt => opt.Ignore())
            .ForMember(p => p.ProjectInstitutes, opt => opt.Ignore())
            .ForMember(p => p.Keywords, opt => opt.MapFrom(dto => dto.Keywords == null ? null : string.Join(';', dto.Keywords)))
            .ForMember(p => p.Visibility, opt => opt.Ignore());

        CreateMap<ProjectResource, ProjectMinimalDto>()
            .ForMember(dto => dto.Id, opt => opt.MapFrom(pr => pr.ProjectId));
    }
}