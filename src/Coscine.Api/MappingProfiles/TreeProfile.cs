﻿using AutoMapper;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Enums.Utility;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="MetadataTree" /> and <see cref="FileTree"/> classes.</summary>
public class TreeProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="TreeProfile" /> class.</summary>
    public TreeProfile()
    {
        CreateMap<MetadataTree, MetadataTreeDto>()
            .ForMember(
                dto => dto.Definition,
                opt => opt.MapFrom(mt => new RdfDefinitionDto { Content = mt.Definition, Type = mt.Format.GetEnumMemberValue() })
            )
            .ForMember(dto => dto.Type, opt => opt.MapFrom(mt => mt.HasChildren ? TreeDataType.Tree : TreeDataType.Leaf));

        CreateMap<MetadataTreeExtracted, MetadataTreeExtractedDto>()
            .ForMember(
                dto => dto.Definition,
                opt => opt.MapFrom(mt => new RdfDefinitionDto { Content = mt.Definition, Type = mt.Format.GetEnumMemberValue() })
            )
            .ForMember(dto => dto.Type, opt => opt.MapFrom(mt => mt.HasChildren ? TreeDataType.Tree : TreeDataType.Leaf));

        CreateMap<MetadataTreeForCreationDto, MetadataTree>()
            .ForMember(mt => mt.Definition, opt => opt.MapFrom(dto => dto.Definition.Content))
            .ForMember(mt => mt.Format, opt => opt.MapFrom(dto => dto.Definition.Type));

        CreateMap<ExtractedMetadataTreeForCreationDto, MetadataTree>()
            .ForMember(mt => mt.Id, opt => opt.MapFrom(dto => dto.Id))
            .ForMember(mt => mt.Definition, opt => opt.Ignore())
            .ForMember(mt => mt.Extracted, opt => opt.MapFrom(dto => new MetadataTreeExtracted()
            {
                Definition = dto.Definition.Content,
                MetadataId = dto.Id,
                RawDataId = new Uri(dto.Id.AbsoluteUri.Replace("type=metadata", "type=data")),
                Format = dto.Definition.Type,
            }))
            .ForMember(mt => mt.SkipValidation, opt => opt.MapFrom(_ => true));

        CreateMap<MetadataTreeForUpdateDto, MetadataTree>()
            .ForMember(mt => mt.Definition, opt => opt.MapFrom(dto => dto.Definition.Content))
            .ForMember(mt => mt.Format, opt => opt.MapFrom(dto => dto.Definition.Type));

        CreateMap<ExtractedMetadataTreeForUpdateDto, MetadataTree>()
            .ForMember(mt => mt.Id, opt => opt.MapFrom(dto => dto.Id))
            .ForMember(mt => mt.Definition, opt => opt.Ignore())
            .ForMember(mt => mt.Extracted, opt => opt.MapFrom(dto => new MetadataTreeExtracted()
            {
                Definition = dto.Definition.Content,
                MetadataId = dto.Id,
                RawDataId = new Uri(dto.Id.AbsoluteUri.Replace("type=metadata", "type=data")),
                Format = dto.Definition.Type,
            }))
            .ForMember(mt => mt.SkipValidation, opt => opt.MapFrom(_ => true))
            .ForMember(mt => mt.ForceNewMetadataVersion, opt => opt.MapFrom(dto => dto.ForceNewMetadataVersion));


        CreateMap<StorageLinks, FileActionsDto>()
            .ForMember(dto => dto.Download, opt => opt.MapFrom(x => x.Download));

        CreateMap<InteractionLink, FileActionDto>()
            .ForMember(dto => dto.Url, opt => opt.MapFrom(x => x.Url))
            .ForMember(dto => dto.Method, opt => opt.MapFrom(x => x.Method));

        CreateMap<CoscineHttpMethod, FileActionHttpMethod>()
        .ConvertUsing((source, dest) => source switch
        {
            CoscineHttpMethod.GET => FileActionHttpMethod.GET,
            CoscineHttpMethod.POST => FileActionHttpMethod.POST,
            CoscineHttpMethod.PUT => FileActionHttpMethod.PUT,
            CoscineHttpMethod.DELETE => FileActionHttpMethod.DELETE,
            _ => throw new ArgumentOutOfRangeException(nameof(source), source, null)
        });

        CreateMap<FileTree, FileTreeDto>()
                .ForMember(dto => dto.Type, opt => opt.MapFrom(ft => ft.HasChildren ? TreeDataType.Tree : TreeDataType.Leaf))
                .ForMember(dto => dto.Actions, opt => opt.MapFrom(x => x.Links))
                ;
    }
}
