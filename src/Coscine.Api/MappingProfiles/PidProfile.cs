﻿using AutoMapper;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="Pid" /> class.</summary>
public class PidProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="DisciplineProfile" /> class.</summary>
    public PidProfile()
    {
        CreateMap<Pid, PidDto>();
    }
}