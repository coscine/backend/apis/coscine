﻿using AutoMapper;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="Vocabulary" /> class.</summary>
public class VocabularyProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="VocabularyProfile" /> class.</summary>
    public VocabularyProfile()
    {
        CreateMap<VocabularyInstance, VocabularyInstanceDto>();
        CreateMap<Vocabulary, VocabularyDto>();
    }
}