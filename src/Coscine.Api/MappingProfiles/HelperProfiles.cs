using AutoMapper;

namespace Coscine.Api.MappingProfiles;

/// <summary>
/// Additional helper mapping profiles for the application.
/// </summary>
class HelperProfiles : Profile
{
    /// <summary>Initializes a new instance of the <see cref="HelperProfiles" /> class.</summary>
    public HelperProfiles()
    {   
        // Use custom converters for DateTime to DateTimeOffset conversions using German time zone
        // CreateMap<DateTime, DateTimeOffset>().ConvertUsing<DateTimeToDateTimeOffsetConverter>();
        // CreateMap<DateTime?, DateTimeOffset?>().ConvertUsing<NullableDateTimeToDateTimeOffsetConverter>();
    }
}
