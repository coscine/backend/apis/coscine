﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="License" /> class.</summary>
public class LicenseProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="LicenseProfile" /> class.</summary>
    public LicenseProfile()
    {
        CreateMap<License, LicenseDto>();
        CreateMap<LicenseForResourceManipulationDto, License>();
    }
}