﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Extensions;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="Resource" /> class.</summary>
public class ResourceProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="ResourceProfile" /> class.</summary>
    public ResourceProfile()
    {
        CreateMap<Resource, ResourceDto>()
            .ForMember(dto => dto.Name, opt => opt.MapFrom(r => r.ResourceName))
            .ForMember(dto => dto.Creator, opt => opt.MapFrom(r => r.Creator.HasValue ? new UserMinimalDto { Id = r.Creator.Value } : null))
            .ForMember(dto => dto.Disciplines, opt => opt.MapFrom(r => r.ResourceDisciplines.Select(rd => rd.Discipline)))
            .ForMember(dto => dto.Keywords, opt => opt.MapFrom(r => r.Keywords == null ? null : r.Keywords.Split(';', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries)))
            .ForMember(dto => dto.FixedValues, opt => opt.MapFrom(r => r.FixedValues == null ? null : r.FixedValues.DeserializeFromCamelCase<Dictionary<string, Dictionary<string, List<FixedValueForResourceManipulationDto>>>>()))
            .ForMember(dto => dto.ApplicationProfile, opt => opt.MapFrom(r => new ApplicationProfileMinimalDto { Uri = new Uri(r.ApplicationProfile) }))
            .ForMember(dto => dto.Archived, opt => opt.MapFrom(r => r.Archived == "1"))
            .ForMember(dto => dto.MetadataExtraction, opt => opt.MapFrom(r => r.MetadataExtractions.Any(x => x.Activated)));

        CreateMap<Resource, ResourceAdminDto>()
            .ForMember(dto => dto.Name, opt => opt.MapFrom(r => r.ResourceName))
            .ForMember(dto => dto.Creator, opt => opt.MapFrom(r => r.Creator.HasValue ? new UserMinimalDto { Id = r.Creator.Value } : null))
            .ForMember(dto => dto.Disciplines, opt => opt.MapFrom(r => r.ResourceDisciplines.Select(rd => rd.Discipline)))
            .ForMember(dto => dto.Keywords, opt => opt.MapFrom(r => r.Keywords == null ? null : r.Keywords.Split(';', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries)))
            .ForMember(dto => dto.FixedValues, opt => opt.MapFrom(r => r.FixedValues == null ? null : r.FixedValues.DeserializeFromCamelCase<Dictionary<string, Dictionary<string, List<FixedValueForResourceManipulationDto>>>>()))
            .ForMember(dto => dto.ApplicationProfile, opt => opt.MapFrom(r => new ApplicationProfileMinimalDto { Uri = new Uri(r.ApplicationProfile) }))
            .ForMember(dto => dto.Archived, opt => opt.MapFrom(r => r.Archived == "1"))
            .ForMember(dto => dto.ProjectResources, opt => opt.MapFrom(p => p.ProjectResources.Select((pr) => new ProjectResourceMinimalDto { ProjectId = pr.ProjectId, ResourceId = pr.ResourceId })))
            .ForMember(dto => dto.MetadataExtraction, opt => opt.MapFrom(r => r.MetadataExtractions.Any(x => x.Activated)));

        CreateMap<ResourceForUpdateDto, Resource>()
            .ForMember(r => r.ResourceName, opt => opt.MapFrom(dto => dto.Name))
            .ForMember(r => r.DisplayName, opt => opt.MapFrom(dto => string.IsNullOrWhiteSpace(dto.DisplayName) ? string.Concat(dto.Name.Take(25)) : dto.DisplayName))
            .ForMember(r => r.ResourceDisciplines, opt => opt.Ignore())
            .ForMember(r => r.License, opt => opt.Ignore())
            .ForMember(r => r.Visibility, opt => opt.Ignore())
            .ForMember(r => r.UsageRights, opt => opt.MapFrom(dto => dto.UsageRights))
            .ForMember(r => r.Keywords, opt => opt.MapFrom(dto => dto.Keywords == null ? null : string.Join(';', dto.Keywords)))
            .ForMember(r => r.FixedValues, opt => opt.MapFrom(dto => dto.FixedValues.SerializeWithCamelCase()))
            .ForMember(r => r.Archived, opt => opt.MapFrom(dto => dto.Archived ? "1" : "0")); // TODO: Fix database column data type

        CreateMap<ResourceForCreationDto, Resource>()
            .ForMember(r => r.ResourceName, opt => opt.MapFrom(dto => dto.Name))
            .ForMember(r => r.DisplayName, opt => opt.MapFrom(dto => string.IsNullOrWhiteSpace(dto.DisplayName) ? string.Concat(dto.Name.Take(25)) : dto.DisplayName))
            .ForMember(r => r.ResourceDisciplines, opt => opt.Ignore())
            .ForMember(r => r.License, opt => opt.Ignore())
            .ForMember(r => r.Visibility, opt => opt.Ignore())
            .ForMember(r => r.UsageRights, opt => opt.MapFrom(dto => dto.UsageRights))
            .ForMember(r => r.Keywords, opt => opt.MapFrom(dto => dto.Keywords == null ? null : string.Join(';', dto.Keywords)))
            .ForMember(r => r.FixedValues, opt => opt.MapFrom(src => src.FixedValues.SerializeWithCamelCase()))
            .ForMember(r => r.ApplicationProfile, opt => opt.MapFrom(dto => dto.ApplicationProfile.Uri));
    }
}