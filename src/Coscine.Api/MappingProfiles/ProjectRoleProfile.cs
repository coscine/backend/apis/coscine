﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="ProjectRole" /> class.</summary>
public class ProjectRoleProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="ProjectRoleProfile" /> class.</summary>
    public ProjectRoleProfile()
    {
        CreateMap<ProjectRole, ProjectRoleDto>()
            .ForMember(dto => dto.Id, opt => opt.MapFrom(pr => pr.RelationId))
            .ForMember(dto => dto.User, opt => opt.MapFrom(pr => pr.User))
            .ForMember(dto => dto.Project, opt => opt.MapFrom(pr => new ProjectMinimalDto { Id = pr.ProjectId }));
    }
}