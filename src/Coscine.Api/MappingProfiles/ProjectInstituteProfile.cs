﻿using AutoMapper;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.MappingProfiles;

/// <summary>The mapping profiles for the <see cref="ProjectInstitute" /> class.</summary>
public class ProjectInstituteProfile : Profile
{
    /// <summary>Initializes a new instance of the <see cref="ProjectInstituteProfile" /> class.</summary>
    public ProjectInstituteProfile()
    {
        CreateMap<ProjectInstitute, OrganizationDto>()
            .ForMember(dto => dto.Uri, opt => opt.MapFrom(pi => pi.OrganizationUrl));
        
        CreateMap<ProjectInstitute, ProjectOrganizationDto>();
    }
}