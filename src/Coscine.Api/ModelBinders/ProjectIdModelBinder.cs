﻿using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Coscine.Api.ModelBinders;

/// <summary>A custom model binder, to resolve a project slug to a GUID/Id of the project.</summary>
public class ProjectIdModelBinder : IModelBinder
{
    private readonly IProjectRepository _projectRepository;

    /// <summary>Initializes a new instance of the <see cref="ProjectIdModelBinder" /> class.</summary>
    /// <param name="projectRepository">The project repository.</param>
    public ProjectIdModelBinder(IProjectRepository projectRepository)
    {
        _projectRepository = projectRepository;
    }

    /// <summary>Attempts to bind a slug to a project entity.</summary>
    /// <param name="bindingContext">The <see cref="T:Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingContext">ModelBindingContext</see>.</param>
    /// <exception cref="System.ArgumentNullException">bindingContext</exception>
    /// <exception cref="Coscine.Api.Core.Entities.Exceptions.NotFound.ProjectNotFoundException">If the project was not found.</exception>
    public async Task BindModelAsync(ModelBindingContext bindingContext)
    {
        if (bindingContext == null)
        {
            throw new ArgumentNullException(nameof(bindingContext));
        }

        var modelName = bindingContext.ModelName;

        ValueProviderResult valueProviderResult = bindingContext.ValueProvider.GetValue(modelName);
        if (valueProviderResult != ValueProviderResult.None)
        {
            bindingContext.ModelState.SetModelValue(modelName, valueProviderResult);

            var value = valueProviderResult.FirstValue;

            if (!string.IsNullOrWhiteSpace(value))
            {
                if (Guid.TryParse(value, out var guidId))
                {
                    bindingContext.Result = ModelBindingResult.Success(guidId);
                }
                else
                {
                    var projectEntity = await _projectRepository.GetAsync(value, trackChanges: false)
                        ?? throw new ProjectNotFoundException(value);

                    bindingContext.Result = ModelBindingResult.Success(projectEntity.Id);
                }
            }
        }

        return;
    }
}