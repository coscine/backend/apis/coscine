﻿using Coscine.Api.Core.Shared.Enums.Utility;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Coscine.Api.ModelBinders;

/// <summary>
/// A custom model binder for binding query string parameters to enum members.
/// </summary>
/// <typeparam name="TEnum">The type of the enum this binder handles. Must be an enum type.</typeparam>
/// <remarks>
/// This model binder allows values like, e.g. <c>application/ld+json</c>, to be correctly bound
/// to an <c>RdfFormat</c> enum member. This might not be necessary if the
/// <see cref="EnumMemberValueConverterFactory"/> handles the <c>Read()</c> actions
/// appropriately, but as of the current implementation, it does not seem to work as expected (ToDo).
/// </remarks>
public class QueryEnumModelBinder<TEnum> : IModelBinder where TEnum : struct, Enum
{
    /// <inheritdoc/>
    public Task BindModelAsync(ModelBindingContext bindingContext)
    {
        if (bindingContext == null)
        {
            throw new ArgumentNullException(nameof(bindingContext));
        }

        var modelName = bindingContext.ModelName;

        ValueProviderResult valueProviderResult = bindingContext.ValueProvider.GetValue(modelName);
        if (valueProviderResult != ValueProviderResult.None)
        {
            bindingContext.ModelState.SetModelValue(modelName, valueProviderResult);

            var value = valueProviderResult.FirstValue;

            if (!string.IsNullOrWhiteSpace(value))
            {
                var rdfFormat = Enum.GetValues<TEnum>()
                    .FirstOrDefault(f =>
                        string.Equals(f.GetEnumMemberValue(), value, StringComparison.OrdinalIgnoreCase)
                        || string.Equals(f.ToString(), value, StringComparison.OrdinalIgnoreCase)
                    );

                bindingContext.Result = ModelBindingResult.Success(rdfFormat);
            }
        }

        return Task.CompletedTask;
    }
}