﻿using Coscine.Api.Core.Shared.Enums.Utility;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Coscine.Api.ModelBinders;

/// <summary>A custom model binder for enums in header.<br />It is used, to extract a enum form a HTTP header.</summary>
/// <typeparam name="TEnum">The type of the enum.</typeparam>
public class HeaderEnumModelBinder<TEnum> : IModelBinder where TEnum : struct, Enum
{
    /// <summary>Attempts to bind a model.</summary>
    /// <param name="bindingContext">The <see cref="T:Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingContext">ModelBindingContext</see>.</param>
    /// <returns>
    ///   <para>
    /// A <see cref="T:System.Threading.Tasks.Task">Task</see> which will complete when the model binding process completes.
    /// </para>
    ///   <para>
    /// If model binding was successful, the <see cref="P:Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingContext.Result">Result</see> should have
    /// <see cref="P:Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingResult.IsModelSet">IsModelSet</see> set to <c>true</c>.
    /// </para>
    ///   <para>
    /// A model binder that completes successfully should set <see cref="P:Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingContext.Result">Result</see> to
    /// a value returned from <see cref="M:Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingResult.Success(System.Object)">Success(System.Object)</see>.
    /// </para>
    /// </returns>
    /// <exception cref="System.ArgumentNullException">bindingContext</exception>
    public Task BindModelAsync(ModelBindingContext bindingContext)
    {
        if (bindingContext == null)
        {
            throw new ArgumentNullException(nameof(bindingContext));
        }

        var headerName = bindingContext.ModelName;

        // Try to fetch the value of the argument from Headers
        var headerValue = bindingContext.HttpContext.Request.Headers[headerName].FirstOrDefault();

        if (!string.IsNullOrWhiteSpace(headerValue))
        {
            var rdfFormat = Array.Find(Enum.GetValues<TEnum>(), f =>
                    string.Equals(f.GetEnumMemberValue(), headerValue, StringComparison.OrdinalIgnoreCase)
                    || string.Equals(f.ToString(), headerValue, StringComparison.OrdinalIgnoreCase)
);

            bindingContext.Result = ModelBindingResult.Success(rdfFormat);
        }

        return Task.CompletedTask;
    }
}