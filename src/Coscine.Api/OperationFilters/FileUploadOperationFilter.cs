﻿namespace Coscine.Api.OperationFilters;

using Coscine.Api.ActionFilters;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.Reflection;

/// <summary>
/// Implements an operation filter to modify Swagger operation requests for actions marked with the <see cref="SwaggerFileUploadAttribute"/>.
/// </summary>
public class FileUploadOperationFilter : IOperationFilter
{
    /// <summary>
    /// Applies the operation filter to modify Swagger operation requests.
    /// </summary>
    /// <param name="operation">The Swagger operation.</param>
    /// <param name="context">The operation filter context.</param>
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        // Check if the action method is marked with the SwaggerFileUploadAttribute
        var fileUploadAttribute = context.MethodInfo.GetCustomAttribute<SwaggerFileUploadAttribute>();
        if (fileUploadAttribute != null)
        {
            // If the attribute is present, configure the request body for multipart/form-data
            operation.RequestBody = new OpenApiRequestBody
            {
                Content = new Dictionary<string, OpenApiMediaType>
                {
                    ["multipart/form-data"] = new OpenApiMediaType
                    {
                        Schema = new OpenApiSchema
                        {
                            Type = "object",
                            Properties = new Dictionary<string, OpenApiSchema>
                            {
                                ["file"] = new OpenApiSchema
                                {
                                    Type = "string",
                                    Format = "binary"
                                }
                            }
                        }
                    }
                },
                Required = true
            };
        }
    }
}