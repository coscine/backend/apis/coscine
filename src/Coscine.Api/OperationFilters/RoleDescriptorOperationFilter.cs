﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Coscine.Api.OperationFilters;

/// <summary>
/// Operation filter to add roles to the swagger documentation.
/// </summary>
public class RoleDescriptorOperationFilter : IOperationFilter
{
    /// <summary>
    /// Adds role information to the operation description.
    /// </summary>
    /// <param name="operation">The Swagger operation.</param>
    /// <param name="context">The operation filter context.</param>
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        // Extract roles from method-level [Authorize] attributes
        var roles = context.MethodInfo.
             GetCustomAttributes(true)
             .OfType<AuthorizeAttribute>()
             .Select(x => x.Roles)
             .Distinct()
             .Where(x => x != null)
             .ToArray();

        // If no roles at the method level, check the controller level
        if (roles.Length == 0)
        {
            roles = context.MethodInfo.DeclaringType?
                 .GetCustomAttributes(true)
                 .OfType<AuthorizeAttribute>()
                 .Select(attr => attr.Roles)
                 .Distinct()
                 .Where(x => x != null)
                 .ToArray();
        }

        // Append roles to the operation description if any roles are found
        if (roles?.Length > 0)
        {
            var rolesString = string.Join(", ", roles.Select(r => $"<code>{r}</code>"));
            // Append required roles information to the operation's description
            // NOTE: This is the same comment that is added using <remarks> in the XML documentation
            var roleDescription = $"<p><strong>Required JWT roles for access:</strong> {rolesString}.</p>";
            operation.Description += roleDescription;
        }
    }
}
