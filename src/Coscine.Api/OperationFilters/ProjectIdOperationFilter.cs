﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Coscine.Api.OperationFilters;

/// <summary>A operation filter, to make the Swagger UI compliant with our <see cref="ModelBinders.ProjectIdModelBinder"/> class.</summary>
public class ProjectIdOperationFilter : IOperationFilter
{
    /// <summary>Applies the specified operation.</summary>
    /// <param name="operation">The operation.</param>
    /// <param name="context">The context.</param>
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        if (context.ApiDescription.ParameterDescriptions != null)
        {
            foreach (var parameterDescription in context.ApiDescription.ParameterDescriptions)
            {
                if (parameterDescription.Name == "projectId" && parameterDescription.ModelMetadata?.ModelType == typeof(Guid))
                {
                    operation.Parameters[0].Schema = new OpenApiSchema
                    {
                        Type = "string",
                    };
                }
            }
        }
    }
}