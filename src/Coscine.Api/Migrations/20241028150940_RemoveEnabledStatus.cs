﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Coscine.Api.Migrations
{
    /// <inheritdoc />
    public partial class RemoveEnabledStatus : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Enabled",
                table: "ResourceTypes");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                table: "ResourceTypes",
                type: "bit",
                nullable: false,
                defaultValueSql: "((1))");

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("198c9ba4-e6bb-4b76-8c37-16aad43e61e8"),
                column: "Enabled",
                value: true);

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("23580290-2d3e-4c12-94e7-7ce1e50ae094"),
                column: "Enabled",
                value: true);

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("2629a6f9-2129-432a-9304-123dbbfa7685"),
                column: "Enabled",
                value: true);

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("2d9100e3-f4c2-4307-b679-e234e37ffde7"),
                column: "Enabled",
                value: true);

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("649a5035-c798-4136-9b93-974f79b8d548"),
                column: "Enabled",
                value: true);

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("78e6aef3-8450-494d-ab86-d033bb9b015b"),
                column: "Enabled",
                value: true);

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("8dbc1e2d-432d-4320-b98c-bac41c8af630"),
                column: "Enabled",
                value: true);

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("a3539377-f7a4-4751-9f7a-08b9de4f0c8d"),
                column: "Enabled",
                value: true);

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("a9c0ea37-83c8-4c0b-b159-fccdafed75f0"),
                column: "Enabled",
                value: true);

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("bc9dec17-a893-4670-bd2c-ae8d736fd4de"),
                column: "Enabled",
                value: true);

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("bcb34a02-9b01-40b7-bce1-14cb9eb543f5"),
                column: "Enabled",
                value: true);

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("c1f3b320-fc0f-42d1-b7a3-aa357cc7a5f4"),
                column: "Enabled",
                value: false);

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("c52d5419-9556-42f5-97b2-860ab359b03f"),
                column: "Enabled",
                value: true);

            migrationBuilder.UpdateData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("d0ce723a-edf8-49f8-b2ac-34d7251cf104"),
                column: "Enabled",
                value: true);
        }
    }
}
