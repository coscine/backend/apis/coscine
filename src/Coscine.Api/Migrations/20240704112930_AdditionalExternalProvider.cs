﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Coscine.Api.Migrations
{
    /// <inheritdoc />
    public partial class AdditionalExternalProvider : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "ExternalAuthenticators",
                keyColumn: "Id",
                keyValue: new Guid("64509960-17a6-4ed6-a54e-0833ad5453d1"),
                column: "DisplayName",
                value: "DFN-AAI");

            migrationBuilder.InsertData(
                table: "ExternalAuthenticators",
                columns: new[] { "Id", "DisplayName" },
                values: new object[,]
                {
                    { new Guid("3a1e7185-0328-47ed-afd7-a4ffd7f03a79"), "NFDI4Ing AAI" },
                    { new Guid("4043104a-52bd-4dfd-9e6b-d818e2ea0094"), "GitLab RWTH" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ExternalAuthenticators",
                keyColumn: "Id",
                keyValue: new Guid("3a1e7185-0328-47ed-afd7-a4ffd7f03a79"));

            migrationBuilder.DeleteData(
                table: "ExternalAuthenticators",
                keyColumn: "Id",
                keyValue: new Guid("4043104a-52bd-4dfd-9e6b-d818e2ea0094"));

            migrationBuilder.UpdateData(
                table: "ExternalAuthenticators",
                keyColumn: "Id",
                keyValue: new Guid("64509960-17a6-4ed6-a54e-0833ad5453d1"),
                column: "DisplayName",
                value: "Shibboleth");
        }
    }
}
