﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Coscine.Api.Migrations
{
    /// <inheritdoc />
    public partial class ActivityLogControllerDescriptor : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "MetadataLocalCopy",
                table: "Resources",
                type: "bit",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "bit");

            migrationBuilder.AddColumn<string>(
                name: "ActionName",
                table: "ActivityLogs",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                defaultValue: "NA");

            migrationBuilder.AddColumn<string>(
                name: "ControllerName",
                table: "ActivityLogs",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: true,
                defaultValue: "NA");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ActionName",
                table: "ActivityLogs");

            migrationBuilder.DropColumn(
                name: "ControllerName",
                table: "ActivityLogs");

            migrationBuilder.AlterColumn<bool>(
                name: "MetadataLocalCopy",
                table: "Resources",
                type: "bit",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "bit",
                oldDefaultValue: false);
        }
    }
}
