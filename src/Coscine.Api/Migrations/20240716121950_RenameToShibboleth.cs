﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Coscine.Api.Migrations
{
    /// <inheritdoc />
    public partial class RenameToShibboleth : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "ExternalAuthenticators",
                keyColumn: "Id",
                keyValue: new Guid("64509960-17a6-4ed6-a54e-0833ad5453d1"),
                column: "DisplayName",
                value: "Shibboleth");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "ExternalAuthenticators",
                keyColumn: "Id",
                keyValue: new Guid("64509960-17a6-4ed6-a54e-0833ad5453d1"),
                column: "DisplayName",
                value: "DFN-AAI");
        }
    }
}
