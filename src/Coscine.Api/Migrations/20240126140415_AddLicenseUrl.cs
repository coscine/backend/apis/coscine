﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Coscine.Api.Migrations
{
    /// <inheritdoc />
    public partial class AddLicenseUrl : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "Licenses",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("0935a00d-0546-4297-b64f-5277bdc944cc"),
                column: "Url",
                value: "http://spdx.org/licenses/EPL-1.0");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("0ee2948b-ed07-4ece-9f32-75c06d8c112c"),
                column: "Url",
                value: "http://spdx.org/licenses/GPL-2.0-only");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("1882c0f2-70b0-4a5a-8114-f440e333de9a"),
                column: "Url",
                value: "http://spdx.org/licenses/Unlicense");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("1cbbe05e-4dd8-4933-9b32-03dba59caa5d"),
                column: "Url",
                value: "http://spdx.org/licenses/MPL-2.0");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("323da6cc-b9dc-4d2a-b512-f0689262b840"),
                column: "Url",
                value: "http://spdx.org/licenses/LGPL-3.0-only");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("48593a5b-1dce-40e2-a269-0588c59eeb36"),
                column: "Url",
                value: "http://spdx.org/licenses/CC-BY-NC-4.0");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("4e7af4f3-fb52-4914-8457-970721fcc09f"),
                column: "Url",
                value: "http://spdx.org/licenses/CC-BY-SA-4.0");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("569d79fe-7ac4-4cb0-a968-4196e3ed11be"),
                column: "Url",
                value: "http://spdx.org/licenses/CC-BY-NC-SA-4.0");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("5b1ac517-fb6f-416e-9c1a-5cf75fba9f1f"),
                column: "Url",
                value: "http://spdx.org/licenses/CC-BY-NC-ND-4.0");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("66ebfa84-6874-49ab-8517-3b92814ef28c"),
                column: "Url",
                value: "http://spdx.org/licenses/CC-BY-4.0");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("74d9911b-398b-4241-90b9-f213ebdee2a9"),
                column: "Url",
                value: "http://spdx.org/licenses/AGPL-3.0-only");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("7f2d774d-1eb3-4cec-ab3d-4d7bd299785a"),
                column: "Url",
                value: "http://spdx.org/licenses/MIT");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("9a98a097-b1b1-4e15-8871-acc9e74ebeb2"),
                column: "Url",
                value: "http://spdx.org/licenses/BSD-2-Clause");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("a439949f-96d2-4dcf-9275-48d78a5ac028"),
                column: "Url",
                value: "http://spdx.org/licenses/GPL-3.0-only");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("b414c963-c5b4-44e7-96d8-f6df55ee2507"),
                column: "Url",
                value: "http://spdx.org/licenses/CC-BY-ND-4.0");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("b974004b-df00-4872-92dc-f93f1448ab98"),
                column: "Url",
                value: "http://spdx.org/licenses/LGPL-2.1-only");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("f6110b01-835e-41fd-942e-d099783adad7"),
                column: "Url",
                value: "http://spdx.org/licenses/Apache-2.0");

            migrationBuilder.UpdateData(
                table: "Licenses",
                keyColumn: "Id",
                keyValue: new Guid("f9eb4f7e-6ba8-4ccd-a08b-a3c8d9c07835"),
                column: "Url",
                value: "http://spdx.org/licenses/BSD-3-Clause");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Url",
                table: "Licenses");
        }
    }
}
