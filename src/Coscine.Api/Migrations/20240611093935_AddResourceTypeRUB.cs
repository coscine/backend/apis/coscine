﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Coscine.Api.Migrations
{
    /// <inheritdoc />
    public partial class AddResourceTypeRUB : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "ResourceTypes",
                columns: new[] { "Id", "DisplayName", "Enabled", "SpecificType", "Type" },
                values: new object[,]
                {
                    { new Guid("198c9ba4-e6bb-4b76-8c37-16aad43e61e8"), "rdsrub", true, "rdsrub", "rds" },
                    { new Guid("c52d5419-9556-42f5-97b2-860ab359b03f"), "rdss3rub", true, "rdss3rub", "rdss3" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("198c9ba4-e6bb-4b76-8c37-16aad43e61e8"));

            migrationBuilder.DeleteData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("c52d5419-9556-42f5-97b2-860ab359b03f"));
        }
    }
}
