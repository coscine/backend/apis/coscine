﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Coscine.Api.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Disciplines",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    Url = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    DisplayNameDe = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    DisplayNameEn = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Disciplines", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ExternalAuthenticators",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    DisplayName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExternalAuthenticators", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Features",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    SharepointId = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    DisplaynameEn = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    DisplaynameDe = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Features", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GitlabResourceType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    Branch = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    GitlabProjectId = table.Column<int>(type: "int", nullable: false),
                    RepoUrl = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    ProjectAccessToken = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    TosAccepted = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GitlabResourceType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    DisplayName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    DisplayName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Abbreviation = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Licenses",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    DisplayName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Licenses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LinkedResourceType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LinkedResourceType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Log",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    ServerTimestamp = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    ClientTimestamp = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    LogLevel = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Stacktrace = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    URI = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    Server = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    CorrolationId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Status = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Source = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Log", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RDSResourceType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    BucketName = table.Column<string>(type: "nvarchar(63)", maxLength: 63, nullable: false),
                    AccessKey = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    SecretKey = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Endpoint = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RDSResourceType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RdsS3ResourceType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    BucketName = table.Column<string>(type: "nvarchar(63)", maxLength: 63, nullable: false),
                    AccessKey = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    SecretKey = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    AccessKeyRead = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    SecretKeyRead = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    AccessKeyWrite = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    SecretKeyWrite = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Endpoint = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RdsS3ResourceType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "RdsS3WormResourceType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    BucketName = table.Column<string>(type: "nvarchar(63)", maxLength: 63, nullable: false),
                    AccessKey = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    SecretKey = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    AccessKeyRead = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    SecretKeyRead = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    AccessKeyWrite = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    SecretKeyWrite = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Endpoint = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RdsS3WormResourceType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ResourceTypes",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    DisplayName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Enabled = table.Column<bool>(type: "bit", nullable: false, defaultValueSql: "((1))"),
                    Type = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    SpecificType = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResourceTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    DisplayName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "S3ResourceType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    BucketName = table.Column<string>(type: "nvarchar(63)", maxLength: 63, nullable: false),
                    AccessKey = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    SecretKey = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    ResourceUrl = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_S3ResourceType", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Titles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    DisplayName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Titles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VersionInfo",
                columns: table => new
                {
                    Version = table.Column<long>(type: "bigint", nullable: false),
                    AppliedOn = table.Column<DateTime>(type: "datetime", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(1024)", maxLength: 1024, nullable: true)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "Visibilities",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    DisplayName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Visibilities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    EmailAddress = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    DisplayName = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Givenname = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Surname = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Entitlement = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Organization = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    TitleId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    LanguageId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Institute = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_LanguageId_Languages_Id",
                        column: x => x.LanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Users_TitleId_Titles_Id",
                        column: x => x.TitleId,
                        principalTable: "Titles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    ProjectName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getdate())"),
                    EndDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Keywords = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    DisplayName = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: true),
                    PrincipleInvestigators = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    GrantId = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    VisibilityId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    Slug = table.Column<string>(type: "nvarchar(63)", maxLength: 63, nullable: false),
                    Creator = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getutcdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_VisibilityId_Visibilities_Id",
                        column: x => x.VisibilityId,
                        principalTable: "Visibilities",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Resources",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    TypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ResourceName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    DisplayName = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: true),
                    VisibilityId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    LicenseId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Keywords = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    UsageRights = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    ResourceTypeOptionId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ApplicationProfile = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    FixedValues = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Creator = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    Archived = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false, defaultValueSql: "(N'0')"),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getutcdate())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resources", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Resources_LicenseId_Licenses_Id",
                        column: x => x.LicenseId,
                        principalTable: "Licenses",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Resources_TypeId_ResourceTypes_Id",
                        column: x => x.TypeId,
                        principalTable: "ResourceTypes",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Resources_VisibilityId_Visibilities_Id",
                        column: x => x.VisibilityId,
                        principalTable: "Visibilities",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ApiTokens",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    Name = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    IssuedAt = table.Column<DateTime>(type: "datetime", nullable: false),
                    Expiration = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApiTokens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApiTokens_UserId_Users_Id",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ContactChange",
                columns: table => new
                {
                    RelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NewEmail = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    EditDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    ConfirmationToken = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactChange", x => x.RelationId);
                    table.ForeignKey(
                        name: "FK_ContactChange_UserId_Users_Id",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ExternalIds",
                columns: table => new
                {
                    RelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ExternalAuthenticatorId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ExternalId = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false),
                    Organization = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExternalIds", x => x.RelationId);
                    table.ForeignKey(
                        name: "FK_ExternalIds_ResourceTypeId_ExternalAuthenticators_Id",
                        column: x => x.ExternalAuthenticatorId,
                        principalTable: "ExternalAuthenticators",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ExternalIds_UserId_Users_Id",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "GroupMemberships",
                columns: table => new
                {
                    RelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    GroupId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupMemberships", x => x.RelationId);
                    table.ForeignKey(
                        name: "FK_GroupMemberships_GroupId_Groups_Id",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_GroupMemberships_UserId_Users_Id",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "TOSAccepted",
                columns: table => new
                {
                    RelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Version = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TOSAccepted", x => x.RelationId);
                    table.ForeignKey(
                        name: "FK_TOSAccepted_UserId_Users_Id",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "UserDisciplines",
                columns: table => new
                {
                    RelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    DisciplineId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDisciplines", x => x.RelationId);
                    table.ForeignKey(
                        name: "FK_UserDisciplines_DisciplineId_Disciplines_Id",
                        column: x => x.DisciplineId,
                        principalTable: "Disciplines",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_UserDisciplines_UserId_Users_Id",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ActivatedFeatures",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    ProjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    FeatureId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivatedFeatures", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActivatedFeatures_FeatureId_Features_Id",
                        column: x => x.FeatureId,
                        principalTable: "Features",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ActivatedFeatures_ProjectId_Projects_Id",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "Invitations",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    Project = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Issuer = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Role = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InviteeEmail = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Expiration = table.Column<DateTime>(type: "datetime", nullable: false),
                    Token = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invitations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Invitations_Issuer_Users_Id",
                        column: x => x.Issuer,
                        principalTable: "Users",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Invitations_Project_Projects_Id",
                        column: x => x.Project,
                        principalTable: "Projects",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Invitations_Role_Roles_Id",
                        column: x => x.Role,
                        principalTable: "Roles",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProjectDiscipline",
                columns: table => new
                {
                    RelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    DisciplineId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectDiscipline", x => x.RelationId);
                    table.ForeignKey(
                        name: "FK_ProjectDiscipline_DisciplineId_Disciplines_Id",
                        column: x => x.DisciplineId,
                        principalTable: "Disciplines",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProjectDiscipline_ProjectId_Projects_Id",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProjectInstitute",
                columns: table => new
                {
                    RelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    ProjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    OrganizationUrl = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: false, defaultValueSql: "(N'https://www.rwth-aachen.de/22000')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectInstitute", x => x.RelationId);
                    table.ForeignKey(
                        name: "FK_ProjectInstitute_ProjectId_Projects_Id",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProjectQuotas",
                columns: table => new
                {
                    RelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    ProjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ResourceTypeId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Quota = table.Column<int>(type: "int", nullable: false),
                    MaxQuota = table.Column<int>(type: "int", nullable: false, defaultValueSql: "(N'0')")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectQuotas", x => x.RelationId);
                    table.ForeignKey(
                        name: "FK_ProjectQuotas_ProjectId_Projects_Id",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProjectQuotas_ResourceTypeId_ResourceTypes_Id",
                        column: x => x.ResourceTypeId,
                        principalTable: "ResourceTypes",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProjectRoles",
                columns: table => new
                {
                    RelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    ProjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RoleId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectRoles", x => x.RelationId);
                    table.ForeignKey(
                        name: "FK_ProjectRoles_ProjectId_Projects_Id",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProjectRoles_RoleId_Roles_Id",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProjectRoles_UserId_Users_Id",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "SubProjects",
                columns: table => new
                {
                    RelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    ProjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubProjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubProjects", x => x.RelationId);
                    table.ForeignKey(
                        name: "FK_SubProjects_ProjectId_Projects_Id",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_SubProjects_SubProjectId_Projects_Id",
                        column: x => x.SubProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "MetadataExtraction",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    ResourceId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Activated = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MetadataExtraction", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MetadataExtraction_ResourceId_Resources_Id",
                        column: x => x.ResourceId,
                        principalTable: "Resources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectResource",
                columns: table => new
                {
                    RelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    ResourceId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectResource", x => x.RelationId);
                    table.ForeignKey(
                        name: "FK_ProjectResource_ProjectId_Projects_Id",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProjectResource_ResourceId_Resources_Id",
                        column: x => x.ResourceId,
                        principalTable: "Resources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ResourceDiscipline",
                columns: table => new
                {
                    RelationId = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    DisciplineId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ResourceId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResourceDiscipline", x => x.RelationId);
                    table.ForeignKey(
                        name: "FK_ResourceDiscipline_DisciplineId_Disciplines_Id",
                        column: x => x.DisciplineId,
                        principalTable: "Disciplines",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ResourceDiscipline_ResourceId_Resources_Id",
                        column: x => x.ResourceId,
                        principalTable: "Resources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Disciplines",
                columns: new[] { "Id", "DisplayNameDe", "DisplayNameEn", "Url" },
                values: new object[,]
                {
                { new Guid("01192bca-1804-4750-9696-464b94fd20e3"), "Alte Kulturen 101", "Ancient Cultures 101", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=101" },
                { new Guid("11c3fcb3-8004-4168-aaa5-d0c06f10faac"), "Erziehungswissenschaft und Bildungsforschung 109", "Educational Research 109", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=109" },
                { new Guid("187fd242-9347-46ad-a43b-1d383d700849"), "Bauwesen und Architektur 410", "Construction Engineering and Architecture 410", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=410" },
                { new Guid("18cfd5a8-7b5e-4a77-9417-bb1b31fd0b60"), "Elektrotechnik und Informationstechnik 408", "Electrical Engineering and Information Technology 408", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=408" },
                { new Guid("1a2bcb3b-969b-402e-aba1-ce83e8bc5035"), "Sozialwissenschaften 111", "Social Sciences 111", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=111" },
                { new Guid("20dbc09f-9304-44eb-9f9f-7636536d5fca"), "Chemische Festkörper- und Oberflächenforschung 302", "Chemical Solid State and Surface Research 302", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=302" },
                { new Guid("2b09f6ce-8483-4c53-bd73-860317c11ab5"), "Geschichtswissenschaften 102", "History 102", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=102" },
                { new Guid("338c270b-6613-42e0-be9f-e71c45bfd83d"), "Analytik, Methodenentwicklung (Chemie) 304", "Analytical Chemistry, Method Development (Chemistry) 304", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=304" },
                { new Guid("34e51e82-bc4a-466e-8649-3c27e45c94d6"), "Polymerforschung 306", "Polymer Research 306", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=306" },
                { new Guid("45ed6c72-166e-4358-bddf-95ec9feb792e"), "Biologische Chemie und Lebensmittelchemie 305", "Biological Chemistry and Food Chemistry 305", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=305" },
                { new Guid("4d03241b-587a-4cdf-b6ac-58ed14477a0a"), "Pflanzenwissenschaften 202", "Plant Sciences 202", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=202" },
                { new Guid("4ff1833e-d2f3-41f2-bffa-db1f0ab661fc"), "Philosophie 108", "Philosophy 108", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=108" },
                { new Guid("60119210-390d-4a36-9d09-c821c3612a4e"), "Molekülchemie 301", "Molecular Chemistry 301", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=301" },
                { new Guid("64eb5f3c-533d-4b77-b7e0-3afeaa72d315"), "Systemtechnik 407", "Systems Engineering 407", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=407" },
                { new Guid("6809a857-494c-4202-ab58-c649c43d10bd"), "Geophysik und Geodäsie 315", "Geophysics and Geodesy 315", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=315" },
                { new Guid("69d0d1df-6d4a-4ac9-89ec-b82cf95c9295"), "Neurowissenschaft 206", "Neurosciences 206", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=206" },
                { new Guid("73483467-afc7-4af8-8e94-5401a96ce5df"), "Geographie 317", "Geography 317", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=317" },
                { new Guid("77f8990d-4053-4c35-bc48-f7402e7324b8"), "Kunst-, Musik-, Theater- und Medienwissenschaften 103", "Fine Arts, Music, Theatre and Media Studies 103", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=103" },
                { new Guid("801564c8-ac8e-42e9-8ff5-6ca919cc9786"), "Grundlagen der Biologie und Medizin 201", "Basic Research in Biology and Medicine 201", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=201" },
                { new Guid("83c9306c-f985-4f13-84e3-460cf69901ef"), "Sozial- und Kulturanthropologie, Außereuropäische Kulturen, Judaistik und Religionswissenschaft 106", "Social and Cultural Anthropology, Non-European Cultures, Jewish Studies and Religious Studies 106", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=106" },
                { new Guid("86304353-4b24-4746-beaf-2f6e7bdfab18"), "Wärmeenergietechnik, Thermische Maschinen, Strömungsmechanik 404", "Heat Energy Technology, Thermal Machines, Fluid Mechanics 404", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=404" },
                { new Guid("86454a0c-40ab-4a41-9e5e-3694a0ce0580"), "Medizin 205", "Medicine 205", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=205" },
                { new Guid("8afa849a-1cb2-48c6-91fd-397c481ff624"), "Sprachwissenschaften 104", "Linguistics 104", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=104" },
                { new Guid("8bc6dd9e-046f-4d5c-96b5-105740ed32a0"), "Geochemie, Mineralogie und Kristallographie 316", "Geochemistry, Mineralogy and Crystallography 316", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=316" },
                { new Guid("9739635d-8103-4729-889f-98954f841053"), "Produktionstechnik 401", "Production Technology 401", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=401" },
                { new Guid("9a6f509f-439b-4664-89ec-92a5e09845f9"), "Wasserforschung 318", "Water Research 318", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=318" },
                { new Guid("9cbb9163-9f09-4aac-95b2-776374c856d0"), "Zoologie 203", "Zoology 203", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=203" },
                { new Guid("a61f2ac4-3443-4a76-8df6-9f3ff95ea18c"), "Mikrobiologie, Virologie und Immunologie 204", "Microbiology, Virology and Immunology 204", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=204" },
                { new Guid("acb1c755-232f-4143-815f-d78fcea202a9"), "Literaturwissenschaft 105", "Literary Studies 105", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=105" },
                { new Guid("b4250311-9995-4743-be14-3b1b34ba5392"), "Astrophysik und Astronomie 311", "Astrophysics and Astronomy 311", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=311" },
                { new Guid("b5d7cdf2-b757-4be2-973f-9c922d399b89"), "Agrar-, Forstwissenschaften und Tiermedizin 207", "Agriculture, Forestry and Veterinary Medicine 207", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=207" },
                { new Guid("ba3bd5af-a490-4102-b925-1fa94708263f"), "Physikalische und Theoretische Chemie 303", "Physical and Theoretical Chemistry 303", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=303" },
                { new Guid("bb6be7bc-1dc4-4d88-95cf-11bcd3ae57b7"), "Optik, Quantenoptik und Physik der Atome, Moleküle und Plasmen 308", "Optics, Quantum Optics and Physics of Atoms, Molecules and Plasmas 308", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=308" },
                { new Guid("bbcaadec-501b-491b-90e5-6bd814151b31"), "Statistische Physik, Weiche Materie, Biologische Physik, Nichtlineare Dynamik 310", "Statistical Physics, Soft Matter, Biological Physics, Nonlinear Dynamics 310", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=310" },
                { new Guid("cdf74163-3895-4307-b6bd-800d94fdfdac"), "Atmosphären-, Meeres- und Klimaforschung 313", "Atmospheric Science, Oceanography and Climate Research 313", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=313" },
                { new Guid("cfd6b656-f4ba-48c6-b5d8-2f98a7b60d1f"), "Informatik 409", "Computer Science 409", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=409" },
                { new Guid("d3f3e972-515d-4a99-a633-d31c984e959c"), "Physik der kondensierten Materie 307", "Condensed Matter Physics 307", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=307" },
                { new Guid("d5eabb9e-e3ab-41ad-80f9-2cf2d25fd4c1"), "Werkstofftechnik 405", "Materials Engineering 405", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=405" },
                { new Guid("d9db2d55-bef5-4c2d-839f-45627ed88747"), "Mechanik und Konstruktiver Maschinenbau 402", "Mechanics and Constructive Mechanical Engineering 402", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=402" },
                { new Guid("dbae6f72-0596-40f3-9246-e4f1e1f9d79d"), "Geologie und Paläontologie 314", "Geology and Palaeontology 314", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=314" },
                { new Guid("dc4194d3-9e4b-45a2-8484-93314069339a"), "Teilchen, Kerne und Felder 309", "Particles, Nuclei and Fields 309", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=309" },
                { new Guid("e2cde3d3-0966-4832-a482-7f5a9a3e15a1"), "Verfahrenstechnik, Technische Chemie 403", "Process Engineering, Technical Chemistry 403", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=403" },
                { new Guid("e61c76c1-5ee4-43a3-b740-19a673c4ce64"), "Materialwissenschaft 406", "Materials Science 406", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=406" },
                { new Guid("ebb0d09a-0b2d-4737-ba2f-85f9dd45d4dd"), "Theologie 107", "Theology 107", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=107" },
                { new Guid("ed1e8854-8cda-4357-b9fd-65144a6208e1"), "Mathematik 312", "Mathematics 312", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=312" },
                { new Guid("fb170c05-9b63-472a-8851-4d22ef80622c"), "Psychologie 110", "Psychology 110", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=110" },
                { new Guid("ff07eeae-e133-414c-aeaa-a1e8c9188f05"), "Wirtschaftswissenschaften 112", "Economics 112", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=112" },
                { new Guid("ffcf2e27-28fa-48fd-981d-095a63db39e8"), "Rechtswissenschaften 113", "Jurisprudence 113", "http://www.dfg.de/dfg_profil/gremien/fachkollegien/liste/index.jsp?id=113" }
                });

            migrationBuilder.InsertData(
                table: "ExternalAuthenticators",
                columns: new[] { "Id", "DisplayName" },
                values: new object[,]
                {
                { new Guid("64509960-17a6-4ed6-a54e-0833ad5453d1"), "Shibboleth" },
                { new Guid("f383f83c-26cb-47eb-80c8-639ed05bd0de"), "ORCiD" }
                });

            migrationBuilder.InsertData(
                table: "Features",
                columns: new[] { "Id", "DisplaynameDe", "DisplaynameEn", "SharepointId" },
                values: new object[,]
                {
                { new Guid("1e237667-34ba-4889-bf12-d56eea266341"), "Announcement Board", "Announcement Board", "MSOZoneCell_WebPartWPQ4" },
                { new Guid("6589db7b-fe26-4739-9a92-3e3ad86c3a69"), "Dokumente", "Documents", "MSOZoneCell_WebPartWPQ3" },
                { new Guid("d43e457f-a35f-43a3-8bdb-543048e6f87c"), "Discussion Board", "Discussion Board", "MSOZoneCell_WebPartWPQ2" }
                });

            migrationBuilder.InsertData(
                table: "Languages",
                columns: new[] { "Id", "Abbreviation", "DisplayName" },
                values: new object[,]
                {
                { new Guid("781823ea-01df-4566-aa58-fb44b5cc1d4d"), "en", "English" },
                { new Guid("922c0fef-d9ab-4f36-bd77-eed0384d19b9"), "de", "Deutsch" }
                });

            migrationBuilder.InsertData(
                table: "Licenses",
                columns: new[] { "Id", "DisplayName" },
                values: new object[,]
                {
                { new Guid("0935a00d-0546-4297-b64f-5277bdc944cc"), "Eclipse Public License 1.0" },
                { new Guid("0ee2948b-ed07-4ece-9f32-75c06d8c112c"), "GNU General Public License v2.0" },
                { new Guid("1882c0f2-70b0-4a5a-8114-f440e333de9a"), "The Unlicense" },
                { new Guid("1cbbe05e-4dd8-4933-9b32-03dba59caa5d"), "Mozilla Public License 2.0" },
                { new Guid("323da6cc-b9dc-4d2a-b512-f0689262b840"), "GNU Lesser General Public License v3.0" },
                { new Guid("48593a5b-1dce-40e2-a269-0588c59eeb36"), "CC BY NC 4.0 (Attribution-NonCommercial)" },
                { new Guid("4e7af4f3-fb52-4914-8457-970721fcc09f"), "CC BY SA 4.0 (Attribution-ShareAlike)" },
                { new Guid("569d79fe-7ac4-4cb0-a968-4196e3ed11be"), "CC BY NC SA 4.0 (Attribution-NonCommercial-ShareAlike)" },
                { new Guid("5b1ac517-fb6f-416e-9c1a-5cf75fba9f1f"), "CC BY NC ND 4.0 (Attribution-NonCommercial-NoDerivatives)" },
                { new Guid("66ebfa84-6874-49ab-8517-3b92814ef28c"), "CC BY 4.0 (Attribution)" },
                { new Guid("74d9911b-398b-4241-90b9-f213ebdee2a9"), "GNU Affero General Public License v3.0" },
                { new Guid("7f2d774d-1eb3-4cec-ab3d-4d7bd299785a"), "MIT License" },
                { new Guid("9a98a097-b1b1-4e15-8871-acc9e74ebeb2"), "BSD 2-clause 'Simplified' License" },
                { new Guid("a439949f-96d2-4dcf-9275-48d78a5ac028"), "GNU General Public License v3.0" },
                { new Guid("b414c963-c5b4-44e7-96d8-f6df55ee2507"), "CC BY ND 4.0 (Attribution-NoDerivatives)" },
                { new Guid("b974004b-df00-4872-92dc-f93f1448ab98"), "GNU Lesser General Public License v2.1" },
                { new Guid("f6110b01-835e-41fd-942e-d099783adad7"), "Apache License 2.0" },
                { new Guid("f9eb4f7e-6ba8-4ccd-a08b-a3c8d9c07835"), "BSD 3-clause 'New' or 'Revised' License" }
                });

            migrationBuilder.InsertData(
                table: "ResourceTypes",
                columns: new[] { "Id", "DisplayName", "Enabled", "SpecificType", "Type" },
                values: new object[,]
                {
                { new Guid("23580290-2d3e-4c12-94e7-7ce1e50ae094"), "rdstudo", true, "rdstudo", "rds" },
                { new Guid("2629a6f9-2129-432a-9304-123dbbfa7685"), "linked", true, "linked", "linked" },
                { new Guid("2d9100e3-f4c2-4307-b679-e234e37ffde7"), "rdss3tudo", true, "rdss3tudo", "rdss3" },
                { new Guid("649a5035-c798-4136-9b93-974f79b8d548"), "gitlab", true, "gitlab", "gitlab" },
                { new Guid("78e6aef3-8450-494d-ab86-d033bb9b015b"), "rdsude", true, "rdsude", "rds" },
                { new Guid("8dbc1e2d-432d-4320-b98c-bac41c8af630"), "rdss3ude", true, "rdss3ude", "rdss3" },
                { new Guid("a3539377-f7a4-4751-9f7a-08b9de4f0c8d"), "rdss3", true, "rdss3rwth", "rdss3" },
                { new Guid("a9c0ea37-83c8-4c0b-b159-fccdafed75f0"), "rdsnrw", true, "rdsnrw", "rds" },
                { new Guid("bc9dec17-a893-4670-bd2c-ae8d736fd4de"), "rdss3nrw", true, "rdss3nrw", "rdss3" },
                { new Guid("bcb34a02-9b01-40b7-bce1-14cb9eb543f5"), "rdss3worm", true, "rdss3wormrwth", "rdss3worm" },
                { new Guid("c1f3b320-fc0f-42d1-b7a3-aa357cc7a5f4"), "s3", false, "s3", "s3" },
                { new Guid("d0ce723a-edf8-49f8-b2ac-34d7251cf104"), "rds", true, "rdsrwth", "rds" }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Description", "DisplayName" },
                values: new object[,]
                {
                { new Guid("508b6d4e-c6ac-4aa5-8a8d-caa31dd39527"), "Member of the project.", "Member" },
                { new Guid("9184a442-4419-4e30-9fe6-0cfe32c9a81f"), "Guest of the project.", "Guest" },
                { new Guid("be294c5e-4e42-49b3-bec4-4b15f49df9a5"), "Owner of the project.", "Owner" }
                });

            migrationBuilder.InsertData(
                table: "Titles",
                columns: new[] { "Id", "DisplayName" },
                values: new object[,]
                {
                { new Guid("98d96229-4a1b-4844-9ec2-925b6a33a696"), "Dr." },
                { new Guid("f47098e0-3cc6-4f53-bbcd-d9ab48eb8899"), "Prof." }
                });

            migrationBuilder.InsertData(
                table: "Visibilities",
                columns: new[] { "Id", "DisplayName" },
                values: new object[,]
                {
                { new Guid("451788b0-aaa1-4419-999d-77a7fa25a080"), "Public" },
                { new Guid("8ab9c883-eb0d-4402-aaad-2e4007badce6"), "Project Members" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivatedFeatures_FeatureId",
                table: "ActivatedFeatures",
                column: "FeatureId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivatedFeatures_ProjectId",
                table: "ActivatedFeatures",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ApiTokens_UserId",
                table: "ApiTokens",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactChange_UserId",
                table: "ContactChange",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ExternalIds_ExternalAuthenticatorId",
                table: "ExternalIds",
                column: "ExternalAuthenticatorId");

            migrationBuilder.CreateIndex(
                name: "IX_ExternalIds_UserId",
                table: "ExternalIds",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupMemberships_GroupId",
                table: "GroupMemberships",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_GroupMemberships_UserId",
                table: "GroupMemberships",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Invitations_Issuer",
                table: "Invitations",
                column: "Issuer");

            migrationBuilder.CreateIndex(
                name: "IX_Invitations_Project",
                table: "Invitations",
                column: "Project");

            migrationBuilder.CreateIndex(
                name: "IX_Invitations_Role",
                table: "Invitations",
                column: "Role");

            migrationBuilder.CreateIndex(
                name: "log_idx_userid_loglevel_servertime",
                table: "Log",
                columns: new[] { "UserId", "LogLevel", "ServerTimestamp" });

            migrationBuilder.CreateIndex(
                name: "IX_MetadataExtraction_ResourceId",
                table: "MetadataExtraction",
                column: "ResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectDiscipline_DisciplineId",
                table: "ProjectDiscipline",
                column: "DisciplineId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectDiscipline_ProjectId",
                table: "ProjectDiscipline",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectInstitute_ProjectId",
                table: "ProjectInstitute",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectQuotas_ResourceTypeId",
                table: "ProjectQuotas",
                column: "ResourceTypeId");

            migrationBuilder.CreateIndex(
                name: "ProjectIdResourceTypeId",
                table: "ProjectQuotas",
                columns: new[] { "ProjectId", "ResourceTypeId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProjectResource_ProjectId",
                table: "ProjectResource",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectResource_ResourceId",
                table: "ProjectResource",
                column: "ResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectRoles_ProjectId",
                table: "ProjectRoles",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectRoles_RoleId",
                table: "ProjectRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectRoles_UserId",
                table: "ProjectRoles",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_VisibilityId",
                table: "Projects",
                column: "VisibilityId");

            migrationBuilder.CreateIndex(
                name: "IX_ResourceDiscipline_DisciplineId",
                table: "ResourceDiscipline",
                column: "DisciplineId");

            migrationBuilder.CreateIndex(
                name: "IX_ResourceDiscipline_ResourceId",
                table: "ResourceDiscipline",
                column: "ResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Resources_LicenseId",
                table: "Resources",
                column: "LicenseId");

            migrationBuilder.CreateIndex(
                name: "IX_Resources_TypeId",
                table: "Resources",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Resources_VisibilityId",
                table: "Resources",
                column: "VisibilityId");

            migrationBuilder.CreateIndex(
                name: "IX_SubProjects_ProjectId",
                table: "SubProjects",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SubProjects_SubProjectId",
                table: "SubProjects",
                column: "SubProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_TOSAccepted_UserId",
                table: "TOSAccepted",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserDisciplines_DisciplineId",
                table: "UserDisciplines",
                column: "DisciplineId");

            migrationBuilder.CreateIndex(
                name: "IX_UserDisciplines_UserId",
                table: "UserDisciplines",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_LanguageId",
                table: "Users",
                column: "LanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TitleId",
                table: "Users",
                column: "TitleId");

            migrationBuilder.CreateIndex(
                name: "UC_Version",
                table: "VersionInfo",
                column: "Version",
                unique: true)
                .Annotation("SqlServer:Clustered", true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivatedFeatures");

            migrationBuilder.DropTable(
                name: "ApiTokens");

            migrationBuilder.DropTable(
                name: "ContactChange");

            migrationBuilder.DropTable(
                name: "ExternalIds");

            migrationBuilder.DropTable(
                name: "GitlabResourceType");

            migrationBuilder.DropTable(
                name: "GroupMemberships");

            migrationBuilder.DropTable(
                name: "Invitations");

            migrationBuilder.DropTable(
                name: "LinkedResourceType");

            migrationBuilder.DropTable(
                name: "Log");

            migrationBuilder.DropTable(
                name: "MetadataExtraction");

            migrationBuilder.DropTable(
                name: "ProjectDiscipline");

            migrationBuilder.DropTable(
                name: "ProjectInstitute");

            migrationBuilder.DropTable(
                name: "ProjectQuotas");

            migrationBuilder.DropTable(
                name: "ProjectResource");

            migrationBuilder.DropTable(
                name: "ProjectRoles");

            migrationBuilder.DropTable(
                name: "RDSResourceType");

            migrationBuilder.DropTable(
                name: "RdsS3ResourceType");

            migrationBuilder.DropTable(
                name: "RdsS3WormResourceType");

            migrationBuilder.DropTable(
                name: "ResourceDiscipline");

            migrationBuilder.DropTable(
                name: "S3ResourceType");

            migrationBuilder.DropTable(
                name: "SubProjects");

            migrationBuilder.DropTable(
                name: "TOSAccepted");

            migrationBuilder.DropTable(
                name: "UserDisciplines");

            migrationBuilder.DropTable(
                name: "VersionInfo");

            migrationBuilder.DropTable(
                name: "Features");

            migrationBuilder.DropTable(
                name: "ExternalAuthenticators");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Resources");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Disciplines");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Licenses");

            migrationBuilder.DropTable(
                name: "ResourceTypes");

            migrationBuilder.DropTable(
                name: "Visibilities");

            migrationBuilder.DropTable(
                name: "Languages");

            migrationBuilder.DropTable(
                name: "Titles");
        }
    }
}