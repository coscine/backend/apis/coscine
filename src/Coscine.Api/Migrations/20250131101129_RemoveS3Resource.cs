﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Coscine.Api.Migrations
{
    /// <inheritdoc />
    public partial class RemoveS3Resource : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "S3ResourceType");

            migrationBuilder.DeleteData(
                table: "ResourceTypes",
                keyColumn: "Id",
                keyValue: new Guid("c1f3b320-fc0f-42d1-b7a3-aa357cc7a5f4"));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "S3ResourceType",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false, defaultValueSql: "(newid())"),
                    AccessKey = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    BucketName = table.Column<string>(type: "nvarchar(63)", maxLength: 63, nullable: false),
                    ResourceUrl = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    SecretKey = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_S3ResourceType", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "ResourceTypes",
                columns: new[] { "Id", "DisplayName", "SpecificType", "Type" },
                values: new object[] { new Guid("c1f3b320-fc0f-42d1-b7a3-aa357cc7a5f4"), "s3", "s3", "s3" });
        }
    }
}
