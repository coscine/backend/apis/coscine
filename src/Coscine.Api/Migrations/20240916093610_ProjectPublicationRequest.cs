﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Coscine.Api.Migrations
{
    /// <inheritdoc />
    public partial class ProjectPublicationRequest : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProjectPublicationRequests",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ProjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    PublicationServiceRorId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CreatorId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "(getutcdate())"),
                    Message = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectPublicationRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProjectPublicationRequests_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ProjectPublicationRequests_Users_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateTable(
                name: "ProjectPublicationRequestResource",
                columns: table => new
                {
                    PublicationRequestsId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ResourcesId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectPublicationRequestResource", x => new { x.PublicationRequestsId, x.ResourcesId });
                    table.ForeignKey(
                        name: "FK_ProjectPublicationRequestResource_ProjectPublicationRequests_PublicationRequestsId",
                        column: x => x.PublicationRequestsId,
                        principalTable: "ProjectPublicationRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProjectPublicationRequestResource_Resources_ResourcesId",
                        column: x => x.ResourcesId,
                        principalTable: "Resources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProjectPublicationRequestResource_ResourcesId",
                table: "ProjectPublicationRequestResource",
                column: "ResourcesId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectPublicationRequests_CreatorId",
                table: "ProjectPublicationRequests",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectPublicationRequests_ProjectId",
                table: "ProjectPublicationRequests",
                column: "ProjectId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProjectPublicationRequestResource");

            migrationBuilder.DropTable(
                name: "ProjectPublicationRequests");
        }
    }
}
