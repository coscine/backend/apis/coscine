﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Coscine.Api.Migrations
{
    /// <inheritdoc />
    public partial class ResponsibleOrganization : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Responsible",
                table: "ProjectInstitute",
                type: "bit",
                nullable: false,
                defaultValue: false);

            //Assumptions:
            //    - We only have valid entries in the database
            //      (no duplicates per project)
            //    - After creating the new colum, all entries in Responsible are false

            // 1: if institute is rwth -> responsible = true
            migrationBuilder.Sql(@"
                UPDATE ProjectInstitute SET Responsible = 1 WHERE OrganizationUrl = 'https://ror.org/04xfq0f34'
            ");

            // 2: if no institute in project is rwth, first institute -> responsible = true
            // (we select the first institute from each project that does not contain the rwth)
            migrationBuilder.Sql(@"
                UPDATE p1
                SET p1.Responsible = 1
                FROM ProjectInstitute AS p1
                WHERE p1.RelationId IN
                (
                    SELECT TOP 1 RelationId AS p2 FROM ProjectInstitute WHERE ProjectId = p1.ProjectId AND NOT EXISTS
                    (
                        SELECT * FROM ProjectInstitute WHERE ProjectId = p1.ProjectId AND OrganizationUrl = 'https://ror.org/04xfq0f34'
                    )
                )
            ");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Responsible",
                table: "ProjectInstitute");
        }
    }
}
