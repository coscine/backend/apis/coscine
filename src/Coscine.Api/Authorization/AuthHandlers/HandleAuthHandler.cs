﻿using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Authorization.AuthHandlers;

/// <summary>
/// Authorization handler for operations related to handles.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="HandleAuthHandler"/> class.
/// </remarks>
public class HandleAuthHandler : AuthorizationHandler<OperationAuthorizationRequirement, Handle>
{
    /// <summary>
    /// Makes a decision if authorization is allowed based on a specific requirement and resource.
    /// </summary>
    /// <param name="context">The authorization context.</param>
    /// <param name="requirement">The requirement to evaluate.</param>
    /// <param name="resource">The resource to evaluate.</param>
    protected override Task HandleRequirementAsync(
        AuthorizationHandlerContext context,
        OperationAuthorizationRequirement requirement,
        Handle resource
    )
    {
        // Allow read operations publicly (without authentication)
        if (requirement.Name == HandleOperations.Read.Name)
        {
            context.Succeed(requirement);
        }

        // Check if the user has the role of Administrator
        if (context.User is not null && context.User.IsInRole(ApiRoles.Administrator))
        {
            // Succeed if the user is an Administrator
            context.Succeed(requirement);
        }

        // Consider adding furhter resource-based authorization checks here (e.g. checking if the user can access the project, resource, etc.)

        // Task.CompletedTask is returned, indicating that the authorization process is complete.
        return Task.CompletedTask;
    }
}