﻿using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Authorization.AuthHandlers;

/// <summary>
/// Auth handler for the resources.
/// </summary>
public class ResourceAuthHandler : AuthorizationHandler<OperationAuthorizationRequirement, Resource>
{
    private readonly ILogger<ResourceAuthHandler> _logger;
    private readonly IRoleRepository _roleRepository;
    private readonly IProjectRoleRepository _projectRoleRepository;
    private readonly IAuthenticatorService _authenticatorService;
    private readonly IReadOnlyDictionary<string, HashSet<Guid>> _allowedRoles;

    /// <summary>
    /// Initializes a new instance of the <see cref="ResourceAuthHandler" /> class.
    /// </summary>
    /// <param name="logger">The logger.</param>
    /// <param name="roleRepository">The role repository.</param>
    /// <param name="projectRoleRepository">The project role repository.</param>
    /// <param name="authenticatorService">The authenticator service.</param>
    /// <exception cref="RoleNotFoundException">
    /// Owner
    /// or
    /// Member
    /// or
    /// Guest</exception>
    /// <exception cref="RoleNotFoundException">Owner
    /// or
    /// Member
    /// or
    /// Guest</exception>
    public ResourceAuthHandler(
        ILogger<ResourceAuthHandler> logger,
        IRoleRepository roleRepository,
        IProjectRoleRepository projectRoleRepository,
        IAuthenticatorService authenticatorService)
    {
        _logger = logger;
        _roleRepository = roleRepository;
        _projectRoleRepository = projectRoleRepository;
        _authenticatorService = authenticatorService;

        var owner = RoleConfiguration.Owner;
        var member = RoleConfiguration.Member;
        var guest = RoleConfiguration.Guest;

        _allowedRoles = new Dictionary<string, HashSet<Guid>>
        {
            { nameof(ResourceOperations.CreateBlob), new HashSet<Guid> { owner.Id, member.Id } },
            { nameof(ResourceOperations.CreateExtractionTree), new HashSet<Guid> { } },
            { nameof(ResourceOperations.CreateTree), new HashSet<Guid> { owner.Id, member.Id } },

            { nameof(ResourceOperations.Read), new HashSet<Guid> { owner.Id, member.Id, guest.Id } },
            { nameof(ResourceOperations.ReadBlob), new HashSet<Guid> { owner.Id, member.Id, guest.Id } },
            { nameof(ResourceOperations.ReadOptions), new HashSet<Guid> { owner.Id, member.Id} },
            { nameof(ResourceOperations.ReadProvenance), new HashSet<Guid> { owner.Id, member.Id, guest.Id } },
            { nameof(ResourceOperations.ReadQuota), new HashSet<Guid> { owner.Id, member.Id, guest.Id } },
            { nameof(ResourceOperations.ReadTree), new HashSet<Guid> { owner.Id, member.Id, guest.Id } },

            { nameof(ResourceOperations.Update), new HashSet<Guid> { owner.Id } },
            { nameof(ResourceOperations.UpdateBlob), new HashSet<Guid> { owner.Id, member.Id } },
            { nameof(ResourceOperations.UpdateExtractionTree), new HashSet<Guid> { } },
            { nameof(ResourceOperations.UpdateMaintenanceMode), new HashSet<Guid> { } },
            { nameof(ResourceOperations.UpdateProvenance), new HashSet<Guid> { } },
            { nameof(ResourceOperations.UpdateTree), new HashSet<Guid> { owner.Id, member.Id } },

            { nameof(ResourceOperations.Delete), new HashSet<Guid> { owner.Id } },
            { nameof(ResourceOperations.DeleteBlob), new HashSet<Guid> { owner.Id, member.Id } },
            { nameof(ResourceOperations.DeleteTree), new HashSet<Guid> { } }
        };
    }

    /// <summary>Makes a decision if authorization is allowed based on a specific requirement and resource.</summary>
    /// <param name="context">The authorization context.</param>
    /// <param name="requirement">The requirement to evaluate.</param>
    /// <param name="resource">The resource to evaluate.</param>
    protected override async Task HandleRequirementAsync(
        AuthorizationHandlerContext context,
        OperationAuthorizationRequirement requirement,
        Resource resource
    )
    {
        _logger.LogDebug(@"Entering ""{class}.{method}"" with requirement ""{requirementName}"" and project ""{resourceId}"".", nameof(ResourceAuthHandler), nameof(HandleRequirementAsync), requirement.Name, resource.Id);

        if (context.User.IsInRole(ApiRoles.Administrator))
        {
            // Allow all supported operations for administrators
            context.Succeed(requirement);
            return;
        }

        var user = await _authenticatorService.GetUserAsync(context.User, trackChanges: false);

        if (user is null)
        {
            _logger.LogDebug("User is null.");
            return;
        }

        _logger.LogDebug(@"User: ""{userId}"".", user.Id);

        // Check if user has any role in the project.
        var projectRoles = await _projectRoleRepository.GetAllByResourceAsync(resource.Id, user.Id, trackChanges: false);

        foreach (var role in projectRoles)
        {
            _logger.LogDebug(@"User ""{userId}"" with Role: ""{roleId}""", user.Id, role.RoleId);
        }

        // Check the allowed roles
        foreach (var projectRole in projectRoles)
        {
            if (_allowedRoles.TryGetValue(requirement.Name, out var roles) && roles.Contains(projectRole.RoleId))
            {
                context.Succeed(requirement);
                _logger.LogInformation(@"Authorization succeeded for user ""{userId}"" with role ""{roleId}"" on resource ""{resourceId}"" for requirement ""{requirementName}"".", user.Id, projectRole.RoleId, resource.Id, requirement.Name);
            }
        }

        // If the user is a quota admin, they can read the resource quota
        if (
            !context.HasSucceeded
            && (requirement.Name == nameof(ResourceOperations.ReadQuota))
            && await _roleRepository.IsUserQuotaAdmin(user.Id)
        )
        {
            context.Succeed(requirement);
            _logger.LogInformation(@"Authorization succeeded for user ""{userId}"" with role ""quotaAdmin"" on resource ""{resourceId}"" for requirement ""{requirementName}"".", user.Id, resource.Id, requirement.Name);
        }

        _logger.LogDebug(@"Leaving ""{class}.{method}"".", nameof(ProjectAuthHandler), nameof(HandleRequirementAsync));
    }
}