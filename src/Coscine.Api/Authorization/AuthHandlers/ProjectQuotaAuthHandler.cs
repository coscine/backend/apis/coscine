﻿using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Authorization.AuthHandlers;

/// <summary>
/// Auth handler for the project quota.
/// </summary>
public class ProjectQuotaAuthHandler : AuthorizationHandler<OperationAuthorizationRequirement, ProjectQuota>
{
    private readonly ILogger<ProjectQuotaAuthHandler> _logger;
    private readonly IRoleRepository _roleRepository;
    private readonly IProjectRoleRepository _projectRoleRepository;
    private readonly IAuthenticatorService _authenticatorService;
    private readonly IReadOnlyDictionary<string, HashSet<Guid>> _allowedRoles;

    /// <summary>
    /// Initializes a new instance of the <see cref="ProjectQuotaAuthHandler" /> class.
    /// </summary>
    /// <param name="logger">The logger.</param>
    /// <param name="roleRepository">The role repository.</param>
    /// <param name="projectRoleRepository">The project role repository.</param>
    /// <param name="authenticatorService">The authenticator service.</param>
    /// <exception cref="RoleNotFoundException">Owner</exception>
    /// <exception cref="RoleNotFoundException">Owner or Member or Guest</exception>
    public ProjectQuotaAuthHandler(
        ILogger<ProjectQuotaAuthHandler> logger,
        IRoleRepository roleRepository,
        IProjectRoleRepository projectRoleRepository,
        IAuthenticatorService authenticatorService)
    {
        _logger = logger;
        _roleRepository = roleRepository;
        _projectRoleRepository = projectRoleRepository;
        _authenticatorService = authenticatorService;

        var owner = RoleConfiguration.Owner;
        var member = RoleConfiguration.Member;
        //var guest = RoleConfiguration.Guest;

        _allowedRoles = new Dictionary<string, HashSet<Guid>>
        {
            { nameof(ProjectQuotaOperations.Read), new HashSet<Guid> { member.Id, owner.Id } },

            { nameof(ProjectQuotaOperations.Update), new HashSet<Guid> { owner.Id } },
        };
    }

    /// <summary>
    /// Makes a decision if authorization is allowed based on a specific requirement and resource.
    /// </summary>
    /// <param name="context">The authorization context.</param>
    /// <param name="requirement">The requirement to evaluate.</param>
    /// <param name="resource">The resource to evaluate.</param>
    protected override async Task HandleRequirementAsync(
        AuthorizationHandlerContext context,
        OperationAuthorizationRequirement requirement,
        ProjectQuota resource
    )
    {
        _logger.LogDebug(@"Entering ""{class}.{method}"" with requirement ""{requirementName}"" and project quota ""{relationId}"".", nameof(ProjectQuotaAuthHandler), nameof(HandleRequirementAsync), requirement.Name, resource.RelationId);

        if (context.User.IsInRole(ApiRoles.Administrator))
        {
            context.Succeed(requirement);
            return;
        }

        var user = await _authenticatorService.GetUserAsync(context.User, trackChanges: false);

        if (user is null)
        {
            _logger.LogDebug("User is null.");
            return;
        }

        _logger.LogDebug(@"User: ""{userId}"".", user.Id);

        var projectRoles = await _projectRoleRepository.GetAllByProjectAsync(resource.ProjectId, user.Id, trackChanges: false);

        foreach (var role in projectRoles)
        {
            _logger.LogDebug(@"User ""{userId}"" with Role: ""{roleId}""", user.Id, role.RoleId);
        }

        if (projectRoles.Count() == 1)
        {
            var projectRole = projectRoles.First();

            if (_allowedRoles.TryGetValue(requirement.Name, out var roles) && roles.Contains(projectRole.RoleId))
            {
                context.Succeed(requirement);
                _logger.LogInformation(@"Authorization succeeded for user ""{userId}"" with role ""{roleId}"" on project quota ""{relationId}"" for requirement ""{requirementName}"".", user.Id, projectRole.RoleId, resource.RelationId, requirement.Name);
            }
        }

        // If the user is a quota admin, they can read and update the quota
        if (
            !context.HasSucceeded
            && (requirement.Name == nameof(ProjectQuotaOperations.Read) || requirement.Name == nameof(ProjectQuotaOperations.Update) || requirement.Name == nameof(ProjectQuotaOperations.UpdateMaximum))
            && await _roleRepository.IsUserQuotaAdmin(user.Id)
        )
        {
            context.Succeed(requirement);
            _logger.LogInformation(@"Authorization succeeded for user ""{userId}"" with role ""quotaAdmin"" on project quota ""{relationId}"" for requirement ""{requirementName}"".", user.Id, resource.RelationId, requirement.Name);
        }

        _logger.LogDebug(@"Leaving ""{class}.{method}"".", nameof(ProjectQuotaAuthHandler), nameof(HandleRequirementAsync));
    }
}