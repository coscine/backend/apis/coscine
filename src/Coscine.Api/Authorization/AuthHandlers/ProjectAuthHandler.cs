using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Authorization.AuthHandlers;

/// <summary>
/// Auth handler for access to the <see cref="Project"/> class.
/// </summary>
public class ProjectAuthHandler : AuthorizationHandler<OperationAuthorizationRequirement, Project>
{
    private readonly ILogger<ProjectAuthHandler> _logger;
    private readonly IRoleRepository _roleRepository;
    private readonly IProjectRoleRepository _projectRoleRepository;
    private readonly IAuthenticatorService _authenticatorService;
    private readonly IReadOnlyDictionary<string, HashSet<Guid>> _allowedRoles;

    /// <summary>Initializes a new instance of the <see cref="ProjectAuthHandler" /> class.</summary>
    /// <param name="logger">The logger.</param>
    /// <param name="roleRepository">The role repository.</param>
    /// <param name="projectRoleRepository">The project role repository.</param>
    /// <param name="authenticatorService">The authenticator service.</param>
    /// <exception cref="RoleNotFoundException">Owner
    /// or
    /// Member
    /// or
    /// Guest</exception>
    /// <exception cref="RoleNotFoundException">Owner
    /// or
    /// Member
    /// or
    /// Guest</exception>
    public ProjectAuthHandler(
        ILogger<ProjectAuthHandler> logger,
        IRoleRepository roleRepository,
        IProjectRoleRepository projectRoleRepository,
        IAuthenticatorService authenticatorService)
    {
        _logger = logger;
        _roleRepository = roleRepository;
        _projectRoleRepository = projectRoleRepository;
        _authenticatorService = authenticatorService;

        var owner = RoleConfiguration.Owner;
        var member = RoleConfiguration.Member;
        var guest = RoleConfiguration.Guest;

        _allowedRoles = new Dictionary<string, HashSet<Guid>>
        {
            { nameof(ProjectOperations.CreateInvitation), new HashSet<Guid> { owner.Id } },
            { nameof(ProjectOperations.CreatePublicationRequest), new HashSet<Guid> { owner.Id } },
            { nameof(ProjectOperations.CreateResource), new HashSet<Guid> { owner.Id } },
            { nameof(ProjectOperations.CreateSubProject), new HashSet<Guid> { owner.Id } },

            { nameof(ProjectOperations.Read), new HashSet<Guid> { owner.Id, member.Id, guest.Id } },
            { nameof(ProjectOperations.ReadAvailableResourceTypes), new HashSet<Guid> { owner.Id, member.Id, guest.Id } },

            { nameof(ProjectOperations.Update), new HashSet<Guid> { owner.Id, member.Id } },

            { nameof(ProjectOperations.Delete), new HashSet<Guid> { owner.Id } },
        };
    }

    /// <summary>
    /// Makes a decision if authorization is allowed based on a specific requirement and resource.
    /// </summary>
    /// <param name="context">The authorization context.</param>
    /// <param name="requirement">The requirement to evaluate.</param>
    /// <param name="resource">The resource to evaluate.</param>
    protected override async Task HandleRequirementAsync(
        AuthorizationHandlerContext context,
        OperationAuthorizationRequirement requirement,
        Project resource
    )
    {
        _logger.LogDebug(@"Entering ""{class}.{method}"" with requirement ""{requirementName}"" and project ""{resourceId}"".", nameof(ProjectAuthHandler), nameof(HandleRequirementAsync), requirement.Name, resource.Id);

        if (context.User.IsInRole(ApiRoles.Administrator))
        {
            context.Succeed(requirement);
            return;
        }

        var user = await _authenticatorService.GetUserAsync(context.User, trackChanges: false);

        if (user is null)
        {
            _logger.LogDebug("User is null.");
            return;
        }

        _logger.LogDebug(@"User: ""{userId}"".", user.Id);

        var projectRoles = await _projectRoleRepository.GetAllByProjectAsync(resource.Id, user.Id, trackChanges: false);

        foreach (var role in projectRoles)
        {
            _logger.LogDebug(@"User ""{userId}"" with Role: ""{roleId}""", user.Id, role.RoleId);
        }

        if (projectRoles.Count() == 1)
        {
            var projectRole = projectRoles.First();

            if (_allowedRoles.TryGetValue(requirement.Name, out var roles) && roles.Contains(projectRole.RoleId))
            {
                context.Succeed(requirement);
                _logger.LogInformation(@"Authorization succeeded for user ""{userId}"" with role ""{roleId}"" on project ""{slug}"" for requirement ""{requirementName}"".", user.Id, projectRole.RoleId, resource.Slug, requirement.Name);
            }
        }

        foreach (var role in projectRoles)
        {
            _logger.LogDebug(@"User ""{userId}"" with Role: ""{roleId}""", user.Id, role.RoleId);
        }

        // If the user is a quota administrator, they can read the project and its available resource types
        if (
            !context.HasSucceeded
            && (requirement.Name == nameof(ProjectOperations.Read) || requirement.Name == nameof(ProjectOperations.ReadAvailableResourceTypes))
            && await _roleRepository.IsUserQuotaAdmin(user.Id)
        )
        {
            context.Succeed(requirement);
            _logger.LogInformation(@"Authorization succeeded for user ""{userId}"" with role ""quotaAdmin"" on project ""{slug}"" for requirement ""{requirementName}"".", user.Id, resource.Slug, requirement.Name);
        }

        _logger.LogDebug(@"Leaving ""{class}.{method}"".", nameof(ProjectAuthHandler), nameof(HandleRequirementAsync));
    }
}