﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Authorization.AuthHandlers;

/// <summary>
/// Authorization handler for operations related to PIDs (Persistent Identifiers).
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="PidAuthHandler"/> class.
/// </remarks>
public class PidAuthHandler : AuthorizationHandler<OperationAuthorizationRequirement, Pid>
{
    /// <summary>
    /// Makes a decision if authorization is allowed based on a specific requirement and resource.
    /// </summary>
    /// <param name="context">The authorization context.</param>
    /// <param name="requirement">The requirement to evaluate.</param>
    /// <param name="resource">The resource to evaluate.</param>
    protected override Task HandleRequirementAsync(
        AuthorizationHandlerContext context,
        OperationAuthorizationRequirement requirement,
        Pid resource
    )
    {
        // Check if the user has the role of Administrator
        if (context.User.IsInRole(ApiRoles.Administrator))
        {
            // Succeed if the user is an Administrator
            context.Succeed(requirement);
        }

        // Task.CompletedTask is returned, indicating that the authorization process is complete.
        return Task.CompletedTask;
    }
}