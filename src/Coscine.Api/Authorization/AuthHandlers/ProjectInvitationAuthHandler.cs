﻿using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace Coscine.Api.Authorization.AuthHandlers;

/// <summary>Auth handler for the project invitations.</summary>
public class ProjectInvitationAuthHandler : AuthorizationHandler<OperationAuthorizationRequirement, Invitation>
{
    private readonly IRoleRepository _roleRepository;
    private readonly IProjectRoleRepository _projectRoleRepository;
    private readonly IAuthenticatorService _authenticatorService;
    private readonly IReadOnlyDictionary<string, HashSet<Guid>> _allowedRoles;

    /// <summary>Initializes a new instance of the <see cref="ProjectInvitationAuthHandler" /> class.</summary>
    /// <param name="roleRepository">The role repository.</param>
    /// <param name="projectRoleRepository">The project role repository.</param>
    /// <param name="authenticatorService">The authenticator service.</param>
    /// <exception cref="RoleNotFoundException">Owner
    /// or
    /// Member
    /// or
    /// Guest</exception>
    public ProjectInvitationAuthHandler(
        IRoleRepository roleRepository,
        IProjectRoleRepository projectRoleRepository,
        IAuthenticatorService authenticatorService)
    {
        _roleRepository = roleRepository;
        _projectRoleRepository = projectRoleRepository;
        _authenticatorService = authenticatorService;

        var owner = RoleConfiguration.Owner;
        var member = RoleConfiguration.Member;
        var guest = RoleConfiguration.Guest;

        _allowedRoles = new Dictionary<string, HashSet<Guid>>
        {
            { nameof(ProjectInvitationOperations.Read), new HashSet<Guid> { owner.Id, member.Id, guest.Id } },
            //{ nameof(ProjectInvitationOperations.Create), new HashSet<Guid> { owner.Id, member.Id } },
            { nameof(ProjectInvitationOperations.Update), new HashSet<Guid> { owner.Id, member.Id } },
            { nameof(ProjectInvitationOperations.Delete), new HashSet<Guid> { owner.Id } },
            { nameof(ProjectInvitationOperations.Resolve), new HashSet<Guid> { owner.Id } }
        };
    }

    /// <summary>Makes a decision if authorization is allowed based on a specific requirement and resource.</summary>
    /// <param name="context">The authorization context.</param>
    /// <param name="requirement">The requirement to evaluate.</param>
    /// <param name="resource">The resource to evaluate.</param>
    protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context,
                                                   OperationAuthorizationRequirement requirement,
                                                   Invitation resource)
    {
        // Works as follows:
        // Should a resolve be required, we use the rights of the inviter, to ensure, that he can still invite the user.
        // In any other case, we use the rights of the current user, as those are the rights regarding update and delete.

        if (context.User.IsInRole(ApiRoles.Administrator))
        {
            context.Succeed(requirement);
            return;
        }

        var user = await _authenticatorService.GetUserAsync(context.User, trackChanges: false);

        if (user is null)
        {
            return;
        }

        IEnumerable<ProjectRole> projectRoles;

        if (requirement.Name == nameof(ProjectInvitationOperations.Resolve))
        {
            projectRoles = await _projectRoleRepository.GetAllByProjectAsync(resource.Project, resource.Issuer, trackChanges: false);
        }
        else
        {
            projectRoles = await _projectRoleRepository.GetAllByProjectAsync(resource.Project, user.Id, trackChanges: false);
        }

        if (projectRoles.Count() == 1)
        {
            var projectRole = projectRoles.First();

            if (_allowedRoles.TryGetValue(requirement.Name, out var roles) && roles.Contains(projectRole.RoleId))
            {
                context.Succeed(requirement);
            }
        }
    }
}