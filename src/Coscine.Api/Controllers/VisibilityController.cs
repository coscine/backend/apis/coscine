﻿using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>Endpoints for the visibilities.</summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/visibilities")]
[ApiController]
[Authorize]
public class VisibilityController : BaseController
{
    private readonly IVisibilityService _visibilityService;

    /// <summary>
    /// Initializes a new instance of the <see cref="VisibilityController" /> class.
    /// </summary>
    /// <param name="visibilityService">The visibility service.</param>
    public VisibilityController(IVisibilityService visibilityService)
    {
        _visibilityService = visibilityService;
    }

    /// <summary>
    /// Retrieves a visibility by ID.
    /// </summary>
    /// <param name="visibilityId">The ID of the visibility.</param>
    /// <returns>The retrieved visibility.</returns>
    [HttpGet("{visibilityId:guid}", Name = "GetVisibility")]
    [OutputCache(VaryByRouteValueNames = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the visibility.", typeof(Response<VisibilityDto>))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Visibility does not exist.")]
    [AllowAnonymous]
    public async Task<IActionResult> GetVisibility([FromRoute] Guid visibilityId)
    {
        var visibility = await _visibilityService.GetVisibilityById(visibilityId, trackChanges: false);
        return Ok(visibility);
    }

    /// <summary>
    /// Retrieves all visibilities.
    /// </summary>
    /// <param name="visibilityParameters">The parameters for visibility filtering and pagination.</param>
    /// <returns>The retrieved visibilities.</returns>
    [HttpGet(Name = "GetVisibilities")]
    [OutputCache(VaryByQueryKeys = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the visibilities.", typeof(PagedResponse<VisibilityDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetVisibilities([FromQuery] VisibilityParameters visibilityParameters)
    {
        var visibilities = await _visibilityService.GetPagedVisibilities(visibilityParameters, trackChanges: false);
        return Ok(visibilities);
    }
}