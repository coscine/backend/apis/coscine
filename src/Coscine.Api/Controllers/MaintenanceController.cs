﻿using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the maintenances.
/// </summary>
[Obsolete("This controller is deprecated and will be removed in the future. Use the new MessageController instead.")]
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/maintenances")]
[ApiController]
[Authorize]
public class MaintenanceController : BaseController
{
    private readonly IMaintenanceService _maintenanceService;

    /// <summary>
    /// Initializes a new instance of the <see cref="MaintenanceController" /> class.
    /// </summary>
    /// <param name="maintenanceService">The maintenance service.</param>
    public MaintenanceController(IMaintenanceService maintenanceService)
    {
        _maintenanceService = maintenanceService;
    }

    /// <summary>
    /// Retrieves the current maintenance messages.
    /// </summary>
    /// <returns>The retrieved maintenances.</returns>
    [HttpGet(Name = "GetCurrentMaintenances")]
    // Maintenances might change more frequently, therefore only 30 second caching
    [OutputCache(VaryByQueryKeys = ["*"], VaryByHeaderNames = ["accept"], Duration = 30)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the maintenances.", typeof(PagedResponse<MaintenanceDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetMaintenances()
    {
        var currentMaintenances = await _maintenanceService.GetPagedCurrentMaintenances(trackChanges: false);
        return Ok(currentMaintenances);
    }
}