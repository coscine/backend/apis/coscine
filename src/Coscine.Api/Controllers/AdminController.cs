﻿// Ignore Spelling: Admin

using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.RateLimiting;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for admin functionality.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="ProjectController" /> class.
/// </remarks>
/// <param name="activityLogService">The activity log service.</param>
/// <param name="metadataService">The metadata service.</param>
/// <param name="projectService">The project service.</param>
/// <param name="resourceService">The resource service.</param>
/// <param name="userService">The user service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/admin")]
[ApiController]
[Authorize(Roles = ApiRoles.Administrator)]
[DisableRateLimiting]
public class AdminController(
    IActivityLogService activityLogService,
    IMetadataService metadataService,
    IProjectService projectService,
    IResourceService resourceService,
    IUserService userService
    ) : BaseController
{
    private readonly IActivityLogService _activityLogService = activityLogService;
    private readonly IMetadataService _metadataService = metadataService;
    private readonly IProjectService _projectService = projectService;
    private readonly IResourceService _resourceService = resourceService;
    private readonly IUserService _userService = userService;

    /// <summary>
    /// Retrieves all activity logs.
    /// </summary>
    /// <param name="activityLogParameters">The parameters for activity log filtering and pagination.</param>
    /// <returns>The retrieved activity logs.</returns>
    [HttpGet("activity-logs", Name = "GetAllActivityLogs")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the activity logs.", typeof(PagedResponse<ActivityLogDto>))]
    [ServiceFilter(typeof(ValidationFilter))]
    public async Task<IActionResult> GetAllActivityLogs([FromQuery] ActivityLogParameters activityLogParameters)
    {
        var activityLogs = await _activityLogService.GetPagedActivityLogs(activityLogParameters, trackChanges: false);
        return Ok(activityLogs);
    }

    /// <summary>
    /// Retrieves all projects.
    /// </summary>
    /// <param name="projectParameters">Additional query parameters for retrieving the projects.</param>
    /// <returns>The retrieved projects.</returns>
    [HttpGet("projects", Name = "GetAllProjects")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the projects.", typeof(PagedResponse<ProjectAdminDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements. This endpoint is only available to Coscine Admin users.")]
    public async Task<IActionResult> GetAllProjects([FromQuery] ProjectAdminParameters projectParameters)
    {
        var projects = await _projectService.GetPagedProjectsAsync(projectParameters, trackChanges: false);
        return Ok(projects);
    }

    /// <summary>
    /// Retrieves all resources.
    /// </summary>
    /// <param name="resourceParameters">The parameters for resource filtering and pagination.</param>
    /// <returns>The retrieved resources.</returns>
    [HttpGet("resources", Name = "GetAllResources")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the resources.", typeof(PagedResponse<ResourceAdminDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements. This endpoint is only available to Coscine Admin users.")]
    public async Task<IActionResult> GetAllResources([FromQuery] ResourceAdminParameters resourceParameters)
    {
        var resources = await _resourceService.GetPagedResourcesAsync(resourceParameters, trackChanges: false);
        return Ok(resources);
    }

    /// <summary>
    /// Retrieves all users.
    /// </summary>
    /// <param name="userParameters">The parameters for user filtering and pagination.</param>
    /// <returns>The retrieved users.</returns>
    [HttpGet("users", Name = "GetAllUsers")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the users.", typeof(PagedResponse<UserDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements. This endpoint is only available to Coscine Admin users.")]
    public async Task<IActionResult> GetAllUsers([FromQuery] UserAdminParameters userParameters)
    {
        var users = await _userService.GetPagedUsersAsync(userParameters, trackChanges: false);
        return Ok(users);
    }

    /// <summary>
    /// Gets a metadata graph.
    /// </summary>
    /// <param name="graph">The absolute URI of the graph to get.</param>
    /// <param name="metadataGetAdminParameters">The metadata get admin parameters.</param>
    [HttpGet("metadata/{graph}", Name = "GetMetadataGraph")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the metadata.", typeof(Response<RdfDefinitionDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements. This endpoint is only available to Coscine Admin users.")]
    public async Task<IActionResult> GetMetadataGraph([FromRoute] string graph, [FromQuery] MetadataGetAdminParameters metadataGetAdminParameters)
    {
        if (Uri.TryCreate(Uri.UnescapeDataString(graph), UriKind.Absolute, out var graphUri))
        {
            var metadata = await _metadataService.GetMetadataGraphAsync(graphUri, metadataGetAdminParameters, trackChanges: false);
            return Ok(metadata);
        }
        throw new UriBadRequestException(graph);
    }

    /// <summary>
    /// Updates a metadata graph.
    /// </summary>
    /// <param name="graph">The absolute URI of the graph to update.</param>
    /// <param name="metadataUpdateAdminParameters">The metadata update admin parameters.</param>
    [HttpPut("metadata/{graph}", Name = "UpdateMetadataGraph"), DisableRequestSizeLimit, RequestFormLimits(ValueLengthLimit = 104857600 /* 100 MiB */)]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Metadata graph updated")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements. This endpoint is only available to Coscine Admin users.")]
    public async Task<IActionResult> UpdateMetadataGraph([FromRoute] string graph, [FromBody] MetadataUpdateAdminParameters metadataUpdateAdminParameters)
    {
        if (Uri.TryCreate(Uri.UnescapeDataString(graph), UriKind.Absolute, out var graphUri))
        {
            await _metadataService.UpdateMetadataGraphAsync(graphUri, metadataUpdateAdminParameters, trackChanges: false);
            return NoContent();
        }
        throw new UriBadRequestException(graph);
    }

    /// <summary>
    /// Patches a metadata graph.
    /// </summary>
    /// <param name="graph">The absolute URI of the graph to patch.</param>
    /// <param name="rdfPatchDoc">The well-formed RDF patch document containing the changes as operations to be applied to the graph.</param>
    /// <exception cref="UriBadRequestException">Thrown if the provided graph URI is not a valid absolute URI.</exception>
    [HttpPatch("metadata/{graph}", Name = "PatchMetadata")]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Metadata graph patched")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements. This endpoint is only available to Coscine Admin users.")]
    public async Task<IActionResult> PatchMetadata([FromRoute] string graph, [FromBody] RdfPatchDocumentDto rdfPatchDoc)
    {
        if (Uri.TryCreate(Uri.UnescapeDataString(graph), UriKind.Absolute, out var graphUri))
        {
            if (rdfPatchDoc is null)
            {
                return BadRequest($"{nameof(rdfPatchDoc)} object sent from client is null.");
            }

            await _metadataService.PatchMetadataGraphAsync(graphUri, rdfPatchDoc);
            return NoContent();
        }
        throw new UriBadRequestException(graph);
    }

    /// <summary>
    /// Gets all deployed graphs.
    /// </summary>
    /// <param name="deployedGraphParameters">Additional query parameters for retrieving the deployed graphs.</param>
    [HttpGet("graphs", Name = "GetDeployedGraphs")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the metadata.", typeof(PagedResponse<DeployedGraphDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements. This endpoint is only available to Coscine Admin users.")]
    public async Task<IActionResult> GetDeployedGraphs([FromQuery] DeployedGraphParameters deployedGraphParameters)
    {
        var graphs = await _metadataService.GetPagedDeployedGraphsAsync(deployedGraphParameters);
        return Ok(graphs);
    }
}