﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.QueryParameters;
using Coscine.Api.Core.Shared.RequestFeatures;
using Coscine.Api.ModelBinders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the projects.
/// </summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/projects")]
[ApiController]
[Authorize]
public class ProjectController : BaseController
{
    private readonly IProjectService _projectService;

    /// <summary>
    /// Initializes a new instance of the <see cref="ProjectController" /> class.
    /// </summary>
    /// <param name="projectService">The project service.</param>
    public ProjectController(IProjectService projectService)
    {
        _projectService = projectService;
    }

    /// <summary>
    /// Creates a new project.
    /// </summary>
    /// <param name="projectForCreationDto">The project data for creation.</param>
    /// <returns>The location of the created project.</returns>
    [HttpPost(Name = "CreateProject")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.Created, "Project created.", typeof(Response<ProjectDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> CreateProject([FromBody] ProjectForCreationDto projectForCreationDto)
    {
        var project = await _projectService.CreateProjectForUserAsync(projectForCreationDto);
        return CreatedAtRoute("GetProject", new { projectId = project.Id }, project);
    }

    /// <summary>
    /// Retrieves a project.
    /// </summary>
    /// <param name="projectId">The ID of the project.</param>
    /// <param name="projectGetQueryParameters">Additional query parameters for retrieving the project.</param>
    /// <returns>The retrieved project.</returns>
    [HttpGet("{projectId}", Name = "GetProject")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the project.", typeof(Response<ProjectDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Project does not exist or has been deleted.")]
    public async Task<IActionResult> GetProject([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromQuery] ProjectGetQueryParameters projectGetQueryParameters)
    {
        var project = await _projectService.GetProjectAsync(projectId, trackChanges: true, projectGetQueryParameters);
        return Ok(project);
    }

    /// <summary>
    /// Retrieves all projects.
    /// </summary>
    /// <param name="projectParameters">Additional query parameters for retrieving the project.</param>
    /// <returns>The retrieved projects.</returns>
    [HttpGet(Name = "GetProjects")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the projects.", typeof(PagedResponse<ProjectDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    public async Task<IActionResult> GetProjects([FromQuery] ProjectParameters projectParameters)
    {
        var projects = await _projectService.GetPagedProjectsOfUserAsync(projectParameters, trackChanges: false);
        return Ok(projects);
    }

    /// <summary>
    /// Updates a project.
    /// </summary>
    /// <param name="projectId">The ID of the project.</param>
    /// <param name="project">The updated project data.</param>
    /// <returns>No content.</returns>
    [HttpPut("{projectId}", Name = "UpdateProject")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Project updated.")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format or the resource is write-protected due to its archived status.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> UpdateProject([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromBody] ProjectForUpdateDto project)
    {
        await _projectService.UpdateProjectAsync(projectId, project, trackChanges: true);
        return NoContent();
    }

    /// <summary>
    /// Deletes a project.
    /// </summary>
    /// <param name="projectId">The ID of the project.</param>
    /// <returns>No content.</returns>
    [HttpDelete("{projectId}", Name = "DeleteProject")]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Project deleted.")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format or the resource is write-protected due to its archived status.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> DeleteProject([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId)
    {
        await _projectService.DeleteProjectAsync(projectId, trackChanges: true);
        return NoContent();
    }
}