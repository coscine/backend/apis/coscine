﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the users.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="UserController" /> class.
/// </remarks>
/// <param name="userService">The user service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/users")]
[ApiController]
[Authorize]
public class UserController(IUserService userService) : BaseController
{
    private readonly IUserService _userService = userService;


    /// <summary>
    /// Retrieves all users.
    /// </summary>
    /// <param name="userParameters">Additional query parameters for retrieving the user.</param>
    /// <returns>The retrieved users.</returns>
    [HttpGet(Name = "GetUsers")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the users.", typeof(Response<IEnumerable<PublicUserDto>>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    public async Task<IActionResult> GetUsers([FromQuery] UserSearchParameters userParameters)
    {
        var users = await _userService.GetFirstTenPublicUsersAsync(User, userParameters, trackChanges: false);
        return Ok(users);
    }
}