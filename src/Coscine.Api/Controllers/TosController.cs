﻿using Asp.Versioning;
using Coscine.Api.Attributes;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the terms of services (tos).
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="TosController" /> class.
/// </remarks>
/// <param name="tosService">The tos service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/tos")]
[ApiController]
[Authorize]
public class TosController(ITosService tosService) : BaseController
{
    private readonly ITosService _tosService = tosService;

    /// <summary>
    /// Retrieves the current Terms of Service version.
    /// </summary>
    /// <returns>The retrieved tos.</returns>
    [HttpGet(Name = "GetTos")]
    [OutputCache(VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse(
        (int)HttpStatusCode.OK,
        "Returns the current Terms of Service version.",
        typeof(Response<TermsOfServiceDto>)
    )]
    [AllowAnonymous]
    [SkipTermsOfServiceCheck]
    public IActionResult GetTos()
    {
        var tos = _tosService.GetCurrentTosVersion();
        return Ok(tos);
    }
}
