﻿using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the resource types.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="ResourceTypeController" /> class.
/// </remarks>
/// <param name="resourceTypeService">The resource type service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/resource-types")]
[ApiController]
[Authorize]
public class ResourceTypeController(IResourceTypeService resourceTypeService) : BaseController
{
    private readonly IResourceTypeService _resourceTypeService = resourceTypeService;

    /// <summary>
    /// Retrieves the entire global resource types information.
    /// </summary>
    /// <returns>The entire global resource types information.</returns>
    [HttpGet("types", Name = "GetAllResourceTypesInformation")]
    [OutputCache(VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the entire global resource types information.", typeof(Response<IEnumerable<ResourceTypeInformationDto>>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetAllResourceTypesInformation()
    {
        var resourceTypesInformation = await _resourceTypeService.GetAllResourceTypesInformationAsync(trackChanges: false);
        return Ok(resourceTypesInformation);
    }

    /// <summary>
    /// Retrieves the resource type information for a specific resource type.
    /// </summary>
    /// <param name="resourceTypeId">The ID of the resource type to retrieve.</param>
    /// <returns>The retrieved resource.</returns>
    [HttpGet("types/{resourceTypeId:guid}", Name = "GetResourceTypeInformation")]
    [OutputCache(VaryByRouteValueNames = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the resource type information.", typeof(Response<ResourceTypeInformationDto>))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Resource type does not exist.")]
    [AllowAnonymous]
    public async Task<IActionResult> GetResourceTypeInformation([FromRoute] Guid resourceTypeId)
    {
        var resourceTypeInformation = await _resourceTypeService.GetResourceTypeInformationAsync(resourceTypeId, User, trackChanges: false);
        return Ok(resourceTypeInformation);
    }
}