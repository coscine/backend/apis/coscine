﻿using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the disciplines.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="DisciplineController" /> class.
/// </remarks>
/// <param name="disciplineService">The discipline service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/disciplines")]
[ApiController]
[Authorize]
public class DisciplineController(IDisciplineService disciplineService) : BaseController
{
    private readonly IDisciplineService _disciplineService = disciplineService;

    /// <summary>
    /// Retrieves a discipline with the specified ID.
    /// </summary>
    /// <param name="disciplineId">The ID of the discipline.</param>
    /// <returns>The retrieved discipline.</returns>
    [HttpGet("{disciplineId:guid}", Name = "GetDiscipline")]
    [OutputCache(VaryByRouteValueNames = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the discipline.", typeof(Response<DisciplineDto>))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Discipline does not exist.")]
    [AllowAnonymous]
    public async Task<IActionResult> GetDiscipline([FromRoute] Guid disciplineId)
    {
        var discipline = await _disciplineService.GetDisciplineById(disciplineId, trackChanges: false);
        return Ok(discipline);
    }

    /// <summary>
    /// Retrieves all disciplines.
    /// </summary>
    /// <param name="disciplineParameters">The parameters for discipline filtering and pagination.</param>
    /// <returns>The retrieved disciplines.</returns>
    [HttpGet(Name = "GetDisciplines")]
    [OutputCache(VaryByQueryKeys = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the disciplines.", typeof(PagedResponse<DisciplineDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetDisciplines([FromQuery] DisciplineParameters disciplineParameters)
    {
        var disciplines = await _disciplineService.GetPagedDisciplines(disciplineParameters, trackChanges: false);
        return Ok(disciplines);
    }
}
