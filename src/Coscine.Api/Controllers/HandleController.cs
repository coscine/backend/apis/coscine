﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the handles.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="HandleController" /> class.
/// </remarks>
/// <param name="handleService">The handle service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/handles")]
[ApiController]
[Authorize]
public class HandleController(IHandleService handleService) : BaseController
{
    private readonly IHandleService _handleService = handleService;

    /// <summary>
    /// Retrieves all values of a handle by its PID.
    /// </summary>
    /// <param name="prefix">The prefix of the PID</param>
    /// <param name="suffix">The suffix of the PID</param>
    /// <returns>The retrieved handle values.</returns>
    [HttpGet("{prefix}/{suffix}", Name = "GetHandle")]
    [OutputCache(VaryByRouteValueNames = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the handles.", typeof(Response<HandleDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements. This endpoint is only available to Coscine Admin users.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    [AllowAnonymous]
    public async Task<IActionResult> GetHandle([FromRoute] string prefix, [FromRoute] string suffix)
    {
        var handles = await _handleService.GetAsync(User, prefix, suffix);
        return Ok(handles);
    }

    /// <summary>
    /// Updates a handle.
    /// </summary>
    /// <param name="prefix">The prefix of the PID</param>
    /// <param name="suffix">The suffix of the PID</param>
    /// <param name="handleForUpdateDto">The handle for updating.</param>
    /// <returns>No content.</returns>
    [HttpPut("{prefix}/{suffix}", Name = "UpdateHandle")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Handle updated.")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements. This endpoint is only available to Coscine Admin users.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    [Authorize(Roles = ApiRoles.Administrator)]
    public async Task<IActionResult> UpdateHandle([FromRoute] string prefix, [FromRoute] string suffix, [FromBody] HandleForUpdateDto handleForUpdateDto)
    {
        await _handleService.UpdateAsync(User, prefix, suffix, handleForUpdateDto);
        return NoContent();
    }
}