﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.QueryParameters;
using Coscine.Api.ModelBinders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the provenance information.
/// </summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/projects/{projectId}/resources/{resourceId}/provenance")]
[ApiController]
[Authorize]
public class ProvenanceController : BaseController
{
    private readonly IProvenanceService _provenanceService;

    /// <summary>
    /// Initializes a new instance of the <see cref="ProvenanceController" /> class.
    /// </summary>
    /// <param name="provenanceService">The provenance service.</param>
    public ProvenanceController(IProvenanceService provenanceService)
    {
        _provenanceService = provenanceService;
    }

    /// <summary>
    /// Retrieves the specific provenance information associated with a resource.
    /// </summary>
    /// <param name="projectId">The unique identifier or slug of the project.</param>
    /// <param name="resourceId">The unique identifier of the resource.</param>
    /// <param name="provenanceQueryParameters">Additional query parameters for retrieving the provenance information.</param>
    /// <returns>The metadata tree associated with the resource.</returns>
    [HttpGet("specific", Name = "GetSpecificProvenance")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse(
        (int)HttpStatusCode.OK,
        "Returns the specific metadata tree of a resource.",
        typeof(Response<ProvenanceDto>)
    )]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Project does not exist or has been deleted.")]
    public async Task<IActionResult> GetSpecificProvenance(
        [FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId,
        [FromRoute] Guid resourceId,
        [FromQuery] ProvenanceQueryParameters provenanceQueryParameters
    )
    {
        var provenance = await _provenanceService.GetProvenanceAsync(projectId, resourceId, User, provenanceQueryParameters);
        return Ok(provenance);
    }

    /// <summary>
    /// Updates existing specific provenance information of a resource.
    /// </summary>
    /// <param name="projectId">The unique identifier or slug of the project.</param>
    /// <param name="resourceId">The unique identifier of the resource.</param>
    /// <param name="provenanceForUpdateDto">The updated provenance information.</param>
    /// <returns>No content.</returns>
    [HttpPut("specific", Name = "UpdateSpecificProvenance")]
    [Authorize(Roles = ApiRoles.Administrator)]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Provenance information updated.")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse(
        (int)HttpStatusCode.BadRequest,
        "Provided input has a bad format or the resource is write-protected due to its archived status."
    )]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> UpdateSpecificProvenance(
        [FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId,
        [FromRoute] Guid resourceId,
        [FromBody] ProvenanceForUpdateDto provenanceForUpdateDto
    )
    {
        await _provenanceService.UpdateProvenanceAsync(projectId, resourceId, User, provenanceForUpdateDto);
        return NoContent();
    }
}
