﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.ModelBinders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the projects.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="ProjectPublicationRequestController" /> class.
/// </remarks>
/// <param name="publicationRequestService">The publicationRequest service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/projects/{projectId}/publications/requests")]
[ApiController]
[Authorize]
public class ProjectPublicationRequestController(IPublicationRequestService publicationRequestService) : BaseController
{
    private readonly IPublicationRequestService _publicationRequestService = publicationRequestService;

    /// <summary>
    /// Creates a new publication request.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="publicationRequestDto">The publication request data for creation.</param>
    /// <returns>The location of the created project.</returns>
    [HttpPost(Name = "CreatePublicationRequest")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.Created, "Publication request created.", typeof(Response<ProjectPublicationRequestDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> CreatePublicationRequest([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromBody] PublicationRequestForCreationDto publicationRequestDto)
    {
        if (!Uri.TryCreate(Uri.UnescapeDataString(publicationRequestDto.PublicationServiceRorId), UriKind.Absolute, out var _))
        {
            throw new UriBadRequestException(publicationRequestDto.PublicationServiceRorId);
        }
        var publicationRequest = await _publicationRequestService.CreatePublicationRequestForUserAsync(projectId, publicationRequestDto);
        return CreatedAtRoute("GetPublicationRequest", new { projectId, publicationRequestId = publicationRequest.Id }, publicationRequest);
    }


    /// <summary>
    /// Retrieves a publication request.
    /// </summary>
    /// <param name="projectId">The ID of the project.</param>
    /// <param name="publicationRequestId">The ID of the publication request.</param> 
    /// <returns>The retrieved publication request.</returns>
    [HttpGet("{publicationRequestId}", Name = "GetPublicationRequest")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the publication request.", typeof(Response<ProjectPublicationRequestDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Publication request does not exist or has been deleted.")]
    public async Task<IActionResult> GetPublicationRequest([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromRoute] Guid publicationRequestId)
    {
        var publicationRequest = await _publicationRequestService.GetPublicationRequestAsync(projectId, publicationRequestId, trackChanges: false);
        return Ok(publicationRequest);
    }
}