﻿using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for search.
/// </summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/search")]
[ApiController]
[Authorize]
public class SearchController : BaseController
{
    private readonly ISearchService _searchService;

    /// <summary>
    /// Initializes a new instance of the <see cref="SearchController" /> class.
    /// </summary>
    /// <param name="searchService">The search service.</param>
    public SearchController(ISearchService searchService)
    {
        _searchService = searchService;
    }

    /// <summary>
    /// Retrieves a search result by given search parameters.
    /// </summary>
    /// <param name="searchParameters">The parameters and filters for the search.</param>
    /// <returns>The search results.</returns>
    [HttpGet(Name = "GetSearchResults")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the search result.", typeof(PagedSearchResponse<SearchResultDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    [AllowAnonymous]
    public async Task<IActionResult> GetSearchResults([FromQuery] SearchParameters searchParameters)
    {
        var searchResultDtos = await _searchService.SearchAsync(User, searchParameters);
        return Ok(searchResultDtos);
    }
}