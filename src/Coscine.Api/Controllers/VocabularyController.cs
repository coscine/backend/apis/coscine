﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;
using Coscine.Api.ModelBinders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>Endpoints for the Endpoints for the vocabularies.</summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/vocabularies")]
[ApiController]
[Authorize]
public class VocabularyController : BaseController
{
    private readonly IVocabularyService _vocabularyService;

    /// <summary>Initializes a new instance of the <see cref="VocabularyController" /> class.</summary>
    /// <param name="vocabularyService">The vocabulary service.</param>
    public VocabularyController(IVocabularyService vocabularyService)
    {
        _vocabularyService = vocabularyService;
    }

    /// <summary>
    /// Retrieves top-level instances from vocabularies.
    /// </summary>
    /// <param name="vocabularyParameters">The parameters for vocabulary filtering and pagination.</param>
    /// <returns>The retrieved top-level instances from vocabularies.</returns>
    [HttpGet(Name = "GetVocabularies")]
    [OutputCache(VaryByQueryKeys = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the top-level vocabularies.", typeof(PagedResponse<VocabularyDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetVocabularies([FromQuery] VocabularyParameters vocabularyParameters)
    {
        var vocabularies = await _vocabularyService.GetTopLevelVocabularies(vocabularyParameters);
        return Ok(vocabularies);
    }

    /// <summary>
    /// Retrieves vocabulary instances.
    /// </summary>
    /// <param name="vocabularyInstancesParameters">The parameters for vocabulary instance filtering and pagination.</param>
    /// <returns>The retrieved vocabulary instances.</returns>
    [HttpGet("instances", Name = "GetVocabularyInstances")]
    [OutputCache(VaryByQueryKeys = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the vocabulary instances.", typeof(PagedResponse<VocabularyInstanceDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetVocabularyInstances([FromQuery] VocabularyInstancesParameters vocabularyInstancesParameters)
    {
        var vocabularyInstances = await _vocabularyService.GetVocabularyInstancesAsync(vocabularyInstancesParameters);
        return Ok(vocabularyInstances);
    }

    /// <summary>
    /// Retrieves a single instance from a vocabulary.
    /// </summary>
    /// <param name="instance">The URI of the vocabulary instance to retrieve.</param>
    /// <param name="lang">The preferred language for the instance data.</param>
    /// <returns>The retrieved instance.</returns>
    /// <exception cref="UriBadRequestException">Thrown when the provided instance URI is not a valid URI.</exception>
    /// <remarks>
    /// Could be a top-level instance, or an intermediate-level instance from a vocabulary.
    /// </remarks>
    [HttpGet("instances/{instance}", Name = "GetVocabularyInstance")]
    [OutputCache(VaryByRouteValueNames = ["*"], VaryByHeaderNames = ["Accept-Language, accept"], Duration = 30 * 60)]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the vocabulary instance.", typeof(Response<VocabularyInstanceDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetInstance(
        [FromRoute] string instance,
        [FromHeader(Name = "Accept-Language")][ModelBinder(typeof(HeaderEnumModelBinder<AcceptedLanguage>))] AcceptedLanguage lang
    )
    {
        if (Uri.TryCreate(Uri.UnescapeDataString(instance), UriKind.Absolute, out var instanceUri))
        {
            var fallbackLanguage = AcceptedLanguage.en; // Hardcoding for the time being, as we do not ask the user to provide it
            var vocabularyInstance = await _vocabularyService.GetVocabularyInstance(instanceUri, lang, fallbackLanguage);
            return Ok(vocabularyInstance);
        }
        throw new UriBadRequestException(instance);
    }
}