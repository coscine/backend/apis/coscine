using Asp.Versioning;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.ErrorHandlers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Web;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the current current session.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="SelfSessionController" /> class.
/// Uses a specialized error handler, to redirect back to the UI in error cases.
/// </remarks>
/// <param name="authenticatorService"></param>
/// <param name="connectionConfiguration"></param>
/// <param name="authenticationConfiguration"></param>
/// <param name="externalAuthenticatorsRepository"></param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/self/session/")]
[ApiController]
[Authorize]
[CoscineErrorHandler(typeof(HttpRedirectErrorHandler))]
public class SelfSessionController(IAuthenticatorService authenticatorService, IOptionsMonitor<ConnectionConfiguration> connectionConfiguration, IOptionsMonitor<AuthenticationConfiguration> authenticationConfiguration, IExternalAuthenticatorsRepository externalAuthenticatorsRepository) : BaseController
{
    private readonly ConnectionConfiguration _connectionConfiguration = connectionConfiguration.CurrentValue;

    private readonly IExternalAuthenticatorsRepository _externalAuthenticatorsRepository = externalAuthenticatorsRepository;

    private readonly AuthenticationConfiguration _authenticationConfiguration = authenticationConfiguration.CurrentValue;

    /// <summary>
    /// Initiate the login workflow with the default (NFDI4Ing AAI) provider.
    /// </summary>
    /// <returns>Challenge for the API.</returns>
    [AllowAnonymous]
    [HttpGet(Name = "Login")]
    public IActionResult Login()
    {
        // Redirect to the OpenID Connect provider for authentication
        // This is not the callback URL for the API, but the URL for when the workflow is finished.
        var properties = new AuthenticationProperties { RedirectUri = _authenticationConfiguration.RedirectUri };

        // Set the NFDI4Ing AAI as provider
        properties.Items.Add("externalAuthenticatorId", ExternalAuthenticatorConfiguration.NFDI4Ing_AAI.Id.ToString());

        return Challenge(properties, authenticationConfiguration.CurrentValue.OpenIdConnectProviders.Where(x => x.ExternalId.Equals(ExternalAuthenticatorConfiguration.NFDI4Ing_AAI.Id.ToString(), StringComparison.CurrentCultureIgnoreCase)).First().Name);
    }

    /// <summary>
    /// Initiate the login workflow with specific provider.
    /// </summary>
    /// <returns>Challenge for the API.</returns>
    [AllowAnonymous]
    [HttpGet("{externalAuthenticatorId:guid}", Name = "LoginWithProvider")]
    public async Task<IActionResult> LoginWithProvider(Guid externalAuthenticatorId)
    {
        // Redirect to the OpenID Connect provider for authentication
        // This is not the callback URL for the API, but the URL for when the workflow is finished.
        var properties = new AuthenticationProperties { RedirectUri = _authenticationConfiguration.RedirectUri };

        _ = await _externalAuthenticatorsRepository.GetAsync(externalAuthenticatorId, trackChanges: false) ?? throw new ExternalAuthenticatorNotFoundException(externalAuthenticatorId);

        // Set the NFDI4Ing AAI as provider
        properties.Items.Add("externalAuthenticatorId", externalAuthenticatorId.ToString());

        return Challenge(properties, authenticationConfiguration.CurrentValue.OpenIdConnectProviders.Where(x => x.ExternalId.Equals(externalAuthenticatorId.ToString(), StringComparison.CurrentCultureIgnoreCase)).First().Name);
    }

    /// <summary>
    /// Initiate the merge workflow with specific provider.
    /// </summary>
    /// <returns>Challenge for the API.</returns>
    [HttpGet("merge/{externalAuthenticatorId:guid}", Name = "Merge")]
    public async Task<IActionResult> Merge(Guid externalAuthenticatorId)
    {
        var uriBuilder = new UriBuilder(_authenticationConfiguration.RedirectUri);
        var query = HttpUtility.ParseQueryString(uriBuilder.Query);
        query["merged_with"] = externalAuthenticatorId.ToString();
        uriBuilder.Query = query.ToString();

        var properties = new AuthenticationProperties { RedirectUri = uriBuilder.ToString() };

        _ = await _externalAuthenticatorsRepository.GetAsync(externalAuthenticatorId, trackChanges: false) ?? throw new ExternalAuthenticatorNotFoundException(externalAuthenticatorId);

        var user = await authenticatorService.GetUserAsync(User, trackChanges: false) ?? throw new UserNotFoundException();

        properties.Items.Add("externalAuthenticatorId", externalAuthenticatorId.ToString());
        properties.Items.Add("mergeTarget", user.Id.ToString());

        return Challenge(properties, authenticationConfiguration.CurrentValue.OpenIdConnectProviders.Where(x => x.ExternalId.Equals(externalAuthenticatorId.ToString(), StringComparison.CurrentCultureIgnoreCase)).First().Name);
    }

    /// <summary>
    /// Initiate the Log out workflow.
    /// </summary>
    /// <returns>Challenge for the API.</returns>
    [AllowAnonymous]
    [HttpPost("logout")]
    public async Task<IActionResult> Logout()
    {
        // Sign out the user
        await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);


        // Clear the additional cookie
        var expirationCookieOptions = new CookieOptions
        {
            HttpOnly = false,
            Secure = true,
            Expires = DateTime.UtcNow.AddDays(-1) // Set the expiration date to the past to remove the cookie
        };

        Response.Cookies.Append("CoscineLoginExpiration", string.Empty, expirationCookieOptions);


        // Redirect to home page or any other page
        return Redirect(new Uri(new Uri(_connectionConfiguration.ServiceUrl), "./login/?logout=true").AbsoluteUri.ToString());
    }
}