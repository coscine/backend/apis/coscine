﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the api tokens of a user.
/// </summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/self/api-tokens")]
[ApiController]
[Authorize]
public class SelfApiTokenController : BaseController
{
    private readonly IApiTokenService _apiTokenService;

    /// <summary>
    /// Initializes a new instance of the <see cref="SelfApiTokenController" /> class.
    /// </summary>
    /// <param name="apiTokenService">The API token service.</param>
    public SelfApiTokenController(IApiTokenService apiTokenService)
    {
        _apiTokenService = apiTokenService;
    }

    /// <summary>
    /// Retrieves an API token for the current authenticated user.
    /// </summary>
    /// <param name="apiTokenId">The ID of the token.</param>
    /// <returns>The retrieved API token.</returns>
    [HttpGet("{apiTokenId:guid}", Name = "GetApiToken")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the API token.", typeof(Response<ApiTokenDto>))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "API token does not exist.")]
    public async Task<IActionResult> GetApiToken(Guid apiTokenId)
    {
        var apiToken = await _apiTokenService.GetApiTokenAsync(apiTokenId, User, trackChanges: false);
        return Ok(apiToken);
    }

    /// <summary>
    /// Retrieves all API tokens for the current authenticated user.
    /// </summary>
    /// <param name="apiTokenParameters">The parameters for API token filtering and pagination.</param>
    /// <returns>The retrieved API tokens.</returns>
    [HttpGet(Name = "GetAllApiTokens")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the API tokens of the current user.", typeof(PagedResponse<ApiTokenDto>))]
    public async Task<IActionResult> GetAllApiTokens([FromQuery] ApiTokenParameters apiTokenParameters)
    {
        var apiTokens = await _apiTokenService.GetPagedApiTokensAsync(User, apiTokenParameters, trackChanges: false);
        return Ok(apiTokens);
    }

    /// <summary>
    /// Revokes an API token for the current authenticated user.
    /// </summary>
    /// <param name="apiTokenId">The ID of the token.</param>
    /// <returns>No content.</returns>
    [HttpDelete("{apiTokenId:guid}", Name = "RevokeToken")]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "API token revoked/deleted.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> RevokeToken(Guid apiTokenId)
    {
        await _apiTokenService.DeleteApiTokenAsync(apiTokenId, User, trackChanges: true);
        return NoContent();
    }

    /// <summary>
    /// Creates an API token for the current authenticated user.
    /// </summary>
    /// <param name="apiTokenForCreationDto">The API token data for creation.</param>
    /// <returns>The created API token.</returns>
    [HttpPost(Name = "CreateApiToken")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.Created, "API token created.", typeof(Response<ApiTokenDto>))]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> CreateApiToken([FromBody] ApiTokenForCreationDto apiTokenForCreationDto)
    {
        var apiToken = await _apiTokenService.CreateApiTokenAsync(User, apiTokenForCreationDto);
        return CreatedAtRoute("GetApiToken", new { apiTokenId = apiToken.Id }, apiToken);
    }
}