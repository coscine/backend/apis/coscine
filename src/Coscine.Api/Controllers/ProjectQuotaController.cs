﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Coscine.Api.ModelBinders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the project quotas.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="ProjectQuotaController" /> class.
/// </remarks>
/// <param name="quotaService">The project quota service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/projects/{projectId}/quotas")]
[ApiController]
[Authorize]
public class ProjectQuotaController(IQuotaService quotaService) : BaseController
{
    private readonly IQuotaService _quotaService = quotaService;

    /// <summary>
    /// Retrieves a project quota for a specified project and resource type.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="resourceTypeId">The ID of the resource type.</param>
    /// <returns>The retrieved project quota.</returns>
    [HttpGet("{resourceTypeId:guid}", Name = "GetProjectQuota")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the project quota.", typeof(Response<ProjectQuotaDto>))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Project quota does not exist.")]
    public async Task<IActionResult> GetProjectQuota([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, Guid resourceTypeId)
    {
        var projectQuota = await _quotaService.GetQuotaForProject(projectId, resourceTypeId, trackChanges: false);
        return Ok(projectQuota);
    }

    /// <summary>
    /// Retrieves all project quotas for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="projectQuotaParameters">The parameters for project quota filtering and pagination.</param>
    /// <returns>The retrieved project quotas.</returns>
    [HttpGet(Name = "GetProjectQuotas")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the project quotas.", typeof(PagedResponse<ProjectQuotaDto>))]
    public async Task<IActionResult> GetProjectQuotas([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromQuery] ProjectQuotaParameters projectQuotaParameters)
    {
        var projectQuotas = await _quotaService.GetPagedQuotasForProject(projectId, projectQuotaParameters, trackChanges: false);
        return Ok(projectQuotas);
    }

    /// <summary>
    /// Updates a project quota for a specified project and resource type.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="resourceTypeId">The ID of the resource type.</param>
    /// <param name="projectQuota">The updated project quota data.</param>
    /// <returns>No content.</returns>
    [HttpPut("{resourceTypeId:guid}", Name = "UpdateProjectQuota")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Project quota updated.")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> UpdateProjectQuota([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, Guid resourceTypeId, [FromBody] ProjectQuotaForUpdateDto projectQuota)
    {
        await _quotaService.UpdateProjectQuota(projectId, resourceTypeId, projectQuota, trackChanges: true);
        return NoContent();
    }
}