using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the gitlab resource types.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="ResourceTypeController" /> class.
/// </remarks>
/// <param name="resourceTypeGitlabService">Service for GitLab interactions</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/resource-types/gitlab")]
[ApiController]
[Authorize]
public class ResourceTypeGitLabController(IResourceTypeGitlabService resourceTypeGitlabService) : BaseController
{
    /// <summary>
    /// Retrieves all GitLab projects, that the user is a member of, based on the provided credentials.
    /// </summary>
    /// <param name="gitlabCredentials">The credentials required for accessing a GitLab provider.</param>
    /// <returns>The GitLab projects, that the user is a member of.</returns>
    [HttpGet("projects", Name = "GetAllGitlabProjects")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the GitLab projects.", typeof(Response<IEnumerable<GitlabProjectDto>>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "GitLab access token is invalid.")]
    public async Task<IActionResult> GetAllGitlabProjects([FromQuery] GitlabCredentialsDto gitlabCredentials)
    {
        var gitlabProjects = await resourceTypeGitlabService.GetAllGitlabProjectsAsync(gitlabCredentials);
        return Ok(gitlabProjects);
    }

    /// <summary>
    /// Retrieves a single GitLab project, that the user is a member of, based on the provided credentials.
    /// </summary>
    /// <param name="gitlabProjectId" description="TEST">The ID of the GitLab project.</param>
    /// <param name="gitlabCredentials">The credentials required for accessing a GitLab provider.</param>
    /// <returns>The GitLab projects, that the user is a member of.</returns>
    [HttpGet("projects/{gitlabProjectId:int}", Name = "GetGitlabProject")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the GitLab project.", typeof(Response<GitlabProjectDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "GitLab access token is invalid.")]
    public async Task<IActionResult> GetGitlabProject(int gitlabProjectId, [FromQuery] GitlabCredentialsDto gitlabCredentials)
    {
        var gitlabProject = await resourceTypeGitlabService.GetGitlabProjectAsync(gitlabProjectId, gitlabCredentials);
        return Ok(gitlabProject);
    }

    /// <summary>
    /// Retrieves all branches of a GitLab project, that the user is a member of, based on the provided credentials.
    /// </summary>
    /// <param name="gitlabProjectId">The ID of the GitLab project.</param>
    /// <param name="gitlabCredentials">The credentials required for accessing a GitLab provider.</param>
    /// <returns>The GitLab projects, that the user is a member of.</returns>
    [HttpGet("projects/{gitlabProjectId:int}/branches", Name = "GetAllGitlabBranchesForProject")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the GitLab project.", typeof(Response<IEnumerable<GitlabBranchDto>>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "GitLab access token is invalid.")]
    public async Task<IActionResult> GetAllGitlabBranchesForProject(int gitlabProjectId, [FromQuery] GitlabCredentialsDto gitlabCredentials)
    {
        var gitlabBranches = await resourceTypeGitlabService.GetAllGitlabBranchesForProjectAsync(gitlabProjectId, gitlabCredentials);
        return Ok(gitlabBranches);
    }
}