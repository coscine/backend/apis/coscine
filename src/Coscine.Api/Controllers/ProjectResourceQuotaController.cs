﻿using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.ModelBinders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the resource quotas.
/// </summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/projects/{projectId}/resources/{resourceId:guid}/quota")]
[ApiController]
[Authorize]
public class ProjectResourceQuotaController : BaseController
{
    private readonly IQuotaService _quotaService;

    /// <summary>
    /// Initializes a new instance of the <see cref="ProjectResourceQuotaController" /> class.
    /// </summary>
    /// <param name="quotaService">The resource quota service.</param>
    public ProjectResourceQuotaController(IQuotaService quotaService)
    {
        _quotaService = quotaService;
    }

    /// <summary>
    /// Retrieves the resource quota for a specific resource in a project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="resourceId">The ID of the resource.</param>
    /// <returns>The resource quota.</returns>
    [HttpGet(Name = "GetQuotaForResourceForProject")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the resource quota.", typeof(Response<ResourceQuotaDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    public async Task<IActionResult> GetQuotaForResourceForProject([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, Guid resourceId)
    {
        var resourceQuota = await _quotaService.GetQuotaForResourceForProjectAsync(projectId, resourceId, trackChanges: false);
        return Ok(resourceQuota);
    }
}