﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Coscine.Api.ModelBinders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the project resources.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="ProjectResourceController" /> class.
/// </remarks>
/// <param name="resourceService">The resource service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/projects/{projectId}/resources")]
[ApiController]
[Authorize]
public class ProjectResourceController(IResourceService resourceService) : BaseController
{
    private readonly IResourceService _resourceService = resourceService;

    /// <summary>
    /// Creates a new resource for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="resourceForCreationDto">The resource data for creation.</param>
    /// <returns>The location of the created resource.</returns>
    [HttpPost(Name = "CreateResourceForProject")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.Created, "Resource created.", typeof(Response<ResourceDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> CreateResourceForProject([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromBody] ResourceForCreationDto resourceForCreationDto)
    {
        var resource = await _resourceService.CreateResourceForProjectAsync(projectId, resourceForCreationDto);
        return CreatedAtRoute("GetResourceForProject", new { projectId, resourceId = resource.Id }, resource);
    }

    /// <summary>
    /// Retrieves a resource for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="resourceId">The ID of the resource to retrieve.</param>
    /// <returns>The retrieved resource.</returns>
    [HttpGet("{resourceId:guid}", Name = "GetResourceForProject")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the resource.", typeof(Response<ResourceDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Resource does not exist or has been deleted.")]
    public async Task<IActionResult> GetResourceForProject([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, Guid resourceId)
    {
        var resource = await _resourceService.GetResourceByIdForProjectAsync(projectId, resourceId, trackChanges: false);
        return Ok(resource);
    }

    /// <summary>
    /// Retrieves all resources for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="resourceParameters">The parameters for resource filtering and pagination.</param>
    /// <returns>The retrieved resources.</returns>
    [HttpGet(Name = "GetResourcesForProject")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the resources.", typeof(PagedResponse<ResourceDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    public async Task<IActionResult> GetResourcesForProject([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromQuery] ResourceParameters resourceParameters)
    {
        var resources = await _resourceService.GetPagedResourcesForProjectAsync(projectId, resourceParameters, trackChanges: false);
        return Ok(resources);
    }

    /// <summary>
    /// Updates a resource for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="resourceId">The ID of the resource to update.</param>
    /// <param name="resource">The updated resource data.</param>
    /// <returns>No content.</returns>
    [HttpPut("{resourceId:guid}", Name = "UpdateResourceForProject")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Resource updated.")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format or the resource is write-protected due to its archived status.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> UpdateResourceForProject([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, Guid resourceId, [FromBody] ResourceForUpdateDto resource)
    {
        await _resourceService.UpdateResourceForProjectAsync(projectId, resourceId, resource, trackChanges: true);
        return NoContent();
    }

    /// <summary>
    /// Deletes a resource for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="resourceId">The ID of the resource to delete.</param>
    /// <returns>No content.</returns>
    [HttpDelete("{resourceId:guid}", Name = "DeleteResourceForProject")]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Resource deleted.")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format or the resource is write-protected due to its archived status.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> DeleteResourceForProject([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, Guid resourceId)
    {
        await _resourceService.DeleteResourceForProjectAsync(projectId, resourceId, trackChanges: true);
        return NoContent();
    }
}