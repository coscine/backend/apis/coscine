﻿using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the resources.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="ResourceController" /> class.
/// </remarks>
/// <param name="resourceService">The service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/resources")]
[ApiController]
[Authorize]
public class ResourceController(IResourceService resourceService) : BaseController
{
    private readonly IResourceService _resourceService = resourceService;

    /// <summary>
    /// Retrieves a resource by its ID.
    /// </summary>
    /// <param name="resourceId">The ID of the resource to retrieve.</param>
    /// <returns>The retrieved resource.</returns>
    [HttpGet("{resourceId:guid}", Name = "GetResource")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the resource.", typeof(Response<ResourceDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Resource does not exist or has been deleted.")]
    public async Task<IActionResult> GetResource(Guid resourceId)
    {
        var resource = await _resourceService.GetResourceByIdAsync(resourceId, trackChanges: false);
        return Ok(resource);
    }
}