﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Enums.Utility;
using Coscine.Api.Core.Shared.RequestFeatures;
using Coscine.Api.ModelBinders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the applications profiles.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="ApplicationProfileController" /> class.
/// </remarks>
/// <param name="applicationProfileService">The application profile service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/application-profiles")]
[ApiController]
[Authorize]
public class ApplicationProfileController(IApplicationProfileService applicationProfileService) : BaseController
{
    private readonly IApplicationProfileService _applicationProfileService = applicationProfileService;


    /// <summary>
    /// Retrieves all application profiles.
    /// </summary>
    /// <param name="applicationProfileParameters">The parameters for application profile filtering and pagination.</param>
    /// <returns>The retrieved application profiles.</returns>
    [HttpGet("profiles", Name = "GetApplicationProfiles")]
    [OutputCache(VaryByQueryKeys = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the application profiles.", typeof(PagedResponse<ApplicationProfileDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetApplicationProfiles([FromQuery] ApplicationProfileParameters applicationProfileParameters)
    {
        var applicationProfiles = await _applicationProfileService.GetPagedApplicationProfilesAsync(applicationProfileParameters, User, trackChanges: false);
        return Ok(applicationProfiles);
    }

    /// <summary>
    /// Retrieves an application profile by its URI.
    /// </summary>
    /// <param name="profile">The URI of the application profile to retrieve.</param>
    /// <param name="format">The desired data format for the returned application profile.</param>
    /// <param name="lang">The preferred language for the application profile data.</param>
    /// <returns>The retrieved application profile.</returns>
    /// <exception cref="UriBadRequestException">Thrown when the provided profile URI is not a valid URI.</exception>
    [HttpGet("profiles/{profile}", Name = "GetApplicationProfile")]
    [OutputCache(VaryByQueryKeys = ["*"], VaryByRouteValueNames = ["*"], VaryByHeaderNames = ["Accept-Language, accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the application profile.", typeof(Response<ApplicationProfileDto>))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Application profile does not exist.")]
    [AllowAnonymous]
    public async Task<IActionResult> GetApplicationProfile(
        [FromRoute] string profile,
        [FromQuery][ModelBinder(typeof(QueryEnumModelBinder<RdfFormat>))] RdfFormat format,
        [FromHeader(Name = "Accept-Language")][ModelBinder(typeof(HeaderEnumModelBinder<AcceptedLanguage>))] AcceptedLanguage lang
    )
    {
        if (Uri.TryCreate(Uri.UnescapeDataString(profile), UriKind.Absolute, out var applicationProfileUri))
        {
            var applicationProfile = await _applicationProfileService.GetApplicationProfileByUriAsync(applicationProfileUri, format, lang, User, trackChanges: false);
            return Ok(applicationProfile);
        }
        throw new UriBadRequestException(profile);
    }

    /// <summary>
    /// Retrieves the <c>raw</c> application profile definition by its URI.
    /// </summary>
    /// <param name="profile">The URI of the application profile.</param>
    /// <returns>The retrieved <c>raw</c> application profile definition.</returns>
    /// <exception cref="UriBadRequestException">Thrown when the provided profile URI is not a valid URI.</exception>
    [HttpGet("profiles/{profile}/raw", Name = "GetRawApplicationProfile")]
    [OutputCache(VaryByRouteValueNames = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the raw application profile.", typeof(string))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Application profile does not exist.")]
    [ServiceFilter(typeof(ValidateMediaTypeAttribute))]
    [Produces("text/turtle", "application/ld+json")]
    [Consumes("text/json", "application/json")]
    [AllowAnonymous]
    public async Task<IActionResult> GetRawApplicationProfile([FromRoute] string profile)
    {
        var mediaType = Request.Headers.Accept.FirstOrDefault();

        if (mediaType is null || !mediaType.TryParseEnumMember<RdfFormat>(out var format))
        {
            return BadRequest("Unable to parse the provided 'Accept' header to an accepted RDF Format.");
        }

        if (!Uri.TryCreate(Uri.UnescapeDataString(profile), UriKind.Absolute, out var applicationProfileUri))
        {
            throw new UriBadRequestException(profile);
        }

        var graph = await _applicationProfileService.GetRawApplicationProfileByUriAsync(applicationProfileUri, format, AcceptedLanguage.en);

        return Ok(graph);
    }

    /// <summary>
    /// Submits a request to create a new application profile.
    /// </summary>
    /// <param name="applicationProfileForCreationDto">The details required to create a new application profile.</param>
    /// <returns>A response indicating the result of the creation request, including the details of the created application profile, if successful.</returns>
    [HttpPost("requests", Name = "CreateApplicationProfileRequest")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.Created, "Application profile request created.", typeof(Response<ApplicationProfileForCreationDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> CreateApplicationProfileRequest([FromBody] ApplicationProfileForCreationDto applicationProfileForCreationDto)
    {
        var request = await _applicationProfileService.CreateApplicationProfileRequestAsync(User, applicationProfileForCreationDto);
        return Created(request.MergeRequestUrl, request);
    }
}