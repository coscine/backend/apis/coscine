﻿using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the resource types.
/// </summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/projects/{projectId:guid}/resource-types")]
[ApiController]
[Authorize]
public class ProjectResourceTypeController : BaseController
{
    private readonly IResourceTypeService _resourceTypeService;

    /// <summary>
    /// Initializes a new instance of the <see cref="ProjectResourceTypeController" /> class.
    /// </summary>
    /// <param name="resourceTypeService">The resource type service.</param>
    public ProjectResourceTypeController(IResourceTypeService resourceTypeService)
    {
        _resourceTypeService = resourceTypeService;
    }

    /// <summary>
    /// Retrieves the available resource types information for a specific project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <returns>The available resource types information.</returns>
    [HttpGet(Name = "GetAvailableResourceTypesInformationForProject")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the available resourceTypeInformation types information.", typeof(Response<IEnumerable<ResourceTypeInformationDto>>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> GetAvailableResourceTypesInformationForProject(Guid projectId)
    {
        var resourceTypeInformation = await _resourceTypeService.GetAvailableResourceTypesInformationForProjectAsync(projectId, User, trackChanges: false);
        return Ok(resourceTypeInformation);
    }
}