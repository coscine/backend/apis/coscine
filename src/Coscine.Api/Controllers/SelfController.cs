﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Attributes;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the current authenticated user.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="SelfController" /> class.
/// </remarks>
/// <param name="userService">The user service.</param>
/// <param name="projectInvitationService">The project invitation service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/self")]
[ApiController]
[Authorize]
public class SelfController(IUserService userService, IProjectInvitationService projectInvitationService) : BaseController
{
    private readonly IUserService _userService = userService;
    private readonly IProjectInvitationService _projectInvitationService = projectInvitationService;

    /// <summary>
    /// Retrieves the current authenticated user.
    /// </summary>
    /// <returns>The retrieved user.</returns>
    [HttpGet(Name = "GetCurrentUser")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the current authenticated user.", typeof(Response<UserDto>))]
    [SkipTermsOfServiceCheck]
    public async Task<IActionResult> GetCurrentUser()
    {
        var user = await _userService.GetUserAsync(User, trackChanges: true);
        return Ok(user);
    }

    /// <summary>
    /// Updates the current authenticated user.
    /// </summary>
    /// <param name="userForUpdateDto">The updated user data.</param>
    /// <returns>No content.</returns>
    [HttpPut(Name = "UpdateCurrentUser")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "User updated.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> UpdateCurrentUser([FromBody] UserForUpdateDto userForUpdateDto)
    {
        await _userService.UpdateUserAsync(User, userForUpdateDto, HttpContext, trackChanges: true);
        return NoContent();
    }

    /// <summary>
    /// Accepts the current Terms Of Service for the current authenticated user.
    /// </summary>
    /// <returns>No content.</returns>
    [HttpPost("tos", Name = "AcceptCurrentTos")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Terms of Service accepted.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    [SkipTermsOfServiceCheck]
    public async Task<IActionResult> AcceptCurrentTos([FromBody] UserTermsOfServiceAcceptDto userTermsOfServiceAcceptDto)
    {
        await _userService.AcceptTermsOfServiceAsync(userTermsOfServiceAcceptDto, User, trackChanges: true);
        return NoContent();
    }

    /// <summary>
    /// Confirms the email of a user.
    /// </summary>
    /// <param name="contactEmailConfirmationDto">The user email confirmation data.</param>
    /// <returns>No content.</returns>
    [AllowAnonymous]
    [HttpPost("emails", Name = "ConfirmUserEmail")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Contact email confirmed.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    public async Task<IActionResult> ConfirmUserEmail([FromQuery] UserEmailConfirmationDto contactEmailConfirmationDto)
    {
        await _userService.ConfirmUserEmailAsync(contactEmailConfirmationDto, trackChanges: true);
        return NoContent();
    }

    /// <summary>
    /// Initiates user merging for the current user.
    /// </summary>
    /// <returns>The retrieved user.</returns>
    [HttpPost("identities", Name = "InitiateUserMerge")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the user merge dto.", typeof(Response<UserMergeDto>))]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    public async Task<IActionResult> InitiateUserMerge([FromQuery] UserMergeExternalAuthDto userMergeExternalAuthDto)
    {
        var userMerge = await _userService.CreateMergeTokenForUserAsync(
            User,
            userMergeExternalAuthDto,
            HttpContext,
            trackChanges: true
        );
        return Ok(userMerge);
    }

    /// <summary>
    /// Resolves a project invitation for the authenticated user.
    /// </summary>
    /// <param name="projectInvitationResolveDto">The project invitation resolve data.</param>
    /// <returns>No content.</returns>
    [HttpPost("project-invitations", Name = "ResolveProjectInvitation")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> ResolveProjectInvitation([FromBody] ProjectInvitationResolveDto projectInvitationResolveDto)
    {
        await _projectInvitationService.ResolveInvitation(User, projectInvitationResolveDto, trackChanges: true);
        return NoContent();
    }
}
