using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for system status messages.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="SystemStatusController" /> class.
/// </remarks>
/// <param name="messageService">The message service.</param>a
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/system/status")]
[ApiController]
[Authorize]
public class SystemStatusController(IMessageService messageService) : BaseController
{
    private readonly IMessageService _messageService = messageService;

    /// <summary>
    /// Retrieves the internal messages.
    /// </summary>
    /// <param name="messageParameters">The parameters for message filtering and pagination.</param>
    /// <returns>The retrieved internal messages.</returns>
    [HttpGet("internal", Name = "GetInternalMessages")]
    [OutputCache(VaryByQueryKeys = ["*"], VaryByHeaderNames = ["accept"], Duration = 5 * 60 /* 5 minutes */)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the internal message.", typeof(PagedResponse<MessageDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetInternalMessages([FromQuery] MessageParameters messageParameters)
    {
        var messages = await _messageService.GetInternalMessages(messageParameters);
        return Ok(messages);
    }

    /// <summary>
    /// Retrieves the NOC messages.
    /// </summary>
    /// <param name="messageParameters">The parameters for message filtering and pagination.</param>
    /// <returns>The retrieved NOC messages.</returns>
    [HttpGet("noc", Name = "GetNocMessages")]
    [OutputCache(VaryByQueryKeys = ["*"], VaryByHeaderNames = ["accept"], Duration = 10 * 60 /* 10 minutes */)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the NOC messages.", typeof(PagedResponse<MessageDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetNocMessages([FromQuery] MessageParameters messageParameters)
    {
        var messages = await _messageService.GetPagedNocMessages(messageParameters);
        return Ok(messages);
    }
}