﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Coscine.Api.ModelBinders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the project members.
/// </summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/projects/{projectId}/members")]
[ApiController]
[Authorize]
public class ProjectMemberController : BaseController
{
    private readonly IProjectRoleService _projectRoleService;

    /// <summary>
    /// Initializes a new instance of the <see cref="ProjectMemberController" /> class.
    /// </summary>
    /// <param name="projectRoleService">The project role service.</param>
    public ProjectMemberController(IProjectRoleService projectRoleService)
    {
        _projectRoleService = projectRoleService;
    }

    /// <summary>
    /// Retrieves a project membership for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="membershipId">The ID of the project membership to retrieve.</param>
    /// <returns>The retrieved project membership.</returns>
    [HttpGet("{membershipId:guid}", Name = "GetMembership")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the project membership.", typeof(Response<ProjectRoleDto>))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Project membership does not exist.")]
    public async Task<IActionResult> GetMembership([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, Guid membershipId)
    {
        var projectRole = await _projectRoleService.GetProjectRoleOfProjectById(projectId, membershipId, trackChanges: false);

        return Ok(projectRole);
    }

    /// <summary>
    /// Retrieves all project memberships for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="projectRoleParameters">The parameters for project membership filtering and pagination.</param>
    /// <returns>The retrieved project memberships.</returns>
    [HttpGet(Name = "GetMemberships")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the project memberships.", typeof(PagedResponse<ProjectRoleDto>))]
    public async Task<IActionResult> GetMemberships([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromQuery] ProjectRoleParameters projectRoleParameters)
    {
        var projectRoles = await _projectRoleService.GetPagedProjectRolesOfProjectById(projectId, projectRoleParameters, trackChanges: false);

        return Ok(projectRoles);
    }

    /// <summary>
    /// Updates a project membership for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="membershipId">The ID of the project membership to update.</param>
    /// <param name="projectRoleForProjectManipulation">The updated project membership data.</param>
    /// <returns>No content.</returns>
    [HttpPut("{membershipId:guid}", Name = "UpdateMembership")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Project membership updated.")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> UpdateProjectRole([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, Guid membershipId, [FromBody] ProjectRoleForProjectManipulationDto projectRoleForProjectManipulation)
    {
        await _projectRoleService.UpdateProjectRoleOfProject(membershipId, projectId, User, projectRoleForProjectManipulation, trackChanges: true);

        return NoContent();
    }

    /// <summary>
    /// Deletes a project membership for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="membershipId">The ID of the project membership to delete.</param>
    /// <returns>No content.</returns>
    [HttpDelete("{membershipId:guid}", Name = "DeleteMembership")]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Project membership deleted.")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> DeleteMembership([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, Guid membershipId)
    {
        await _projectRoleService.DeleteProjectRoleOfProject(membershipId, projectId, User, trackChanges: true);

        return NoContent();
    }

    /// <summary>
    /// Creates a project membership for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="projectRoleForProjectCreationDto">The project membership data for creation.</param>
    /// <returns>The created project membership.</returns>
    [HttpPost(Name = "AddMembership")]
    [SwaggerResponse((int)HttpStatusCode.Created, "Project membership created.", typeof(Response<ProjectRoleDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> AddMembership([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromBody] ProjectRoleForProjectCreationDto projectRoleForProjectCreationDto)
    {
        var projectMembership = await _projectRoleService.CreateProjectRoleForProject(projectId, User, projectRoleForProjectCreationDto);

        return CreatedAtRoute("GetMembership", new { projectId, membershipId = projectMembership.Id }, projectMembership);
    }
}