﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Coscine.Api.ModelBinders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the project invitations.
/// </summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/projects/{projectId}/invitations")]
[ApiController]
[Authorize]
public class ProjectInvitationController : BaseController
{
    private readonly IProjectInvitationService _projectInvitationService;

    /// <summary>
    /// Initializes a new instance of the <see cref="ProjectInvitationController" /> class.
    /// </summary>
    /// <param name="projectInvitationService">The project invitation service.</param>
    public ProjectInvitationController(IProjectInvitationService projectInvitationService)
    {
        _projectInvitationService = projectInvitationService;
    }

    /// <summary>
    /// Retrieves a project invitation for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="projectInvitationId">The ID of the project invitation to retrieve.</param>
    /// <returns>The retrieved project invitation.</returns>
    [HttpGet("{projectInvitationId:guid}", Name = "GetProjectInvitation")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the project invitation.", typeof(Response<ProjectInvitationDto>))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Project invitation does not exist.")]
    public async Task<IActionResult> GetProjectInvitation([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, Guid projectInvitationId)
    {
        var projectInvitation = await _projectInvitationService.GetProjectInvitationById(projectId, projectInvitationId, trackChanges: false);
        return Ok(projectInvitation);
    }

    /// <summary>
    /// Retrieves all project invitations for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="projectInvitationParameters">The parameters for project invitation filtering and pagination.</param>
    /// <returns>The retrieved project invitations.</returns>
    [HttpGet(Name = "GetProjectInvitations")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the project invitations.", typeof(PagedResponse<ProjectInvitationDto>))]
    public async Task<IActionResult> GetProjectInvitations([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromQuery] ProjectInvitationParameters projectInvitationParameters)
    {
        var projectInvitations = await _projectInvitationService.GetPagedProjectInvitations(projectId, projectInvitationParameters, trackChanges: false);
        return Ok(projectInvitations);
    }

    /// <summary>
    /// Creates a project invitation for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="projectInvitationForProjectManipulationDto">The project invitation data for creation.</param>
    /// <returns>The created project invitation.</returns>
    [HttpPost(Name = "CreateProjectInvitation")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.Created, "Project invitation created.", typeof(Response<ProjectInvitationDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> CreateProjectInvitation([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromBody] ProjectInvitationForProjectManipulationDto projectInvitationForProjectManipulationDto)
    {
        var projectInvitation = await _projectInvitationService.InviteUserToProject(projectId, User, projectInvitationForProjectManipulationDto);
        return CreatedAtRoute("GetProjectInvitation", new { projectId, projectInvitationId = projectInvitation.Id }, projectInvitation);
    }

    /// <summary>
    /// Deletes a project invitation for a specified project.
    /// </summary>
    /// <param name="projectId">The Id or slug of the project.</param>
    /// <param name="projectInvitationId">The ID of the project invitation to delete.</param>
    /// <returns>No content.</returns>
    [HttpDelete("{projectInvitationId:guid}", Name = "DeleteProjectInvitation")]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Project invitation deleted.")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> DeleteProjectInvitation([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, Guid projectInvitationId)
    {
        await _projectInvitationService.DeleteProjectInvitationOfProject(projectId, projectInvitationId, User, trackChanges: true);
        return NoContent();
    }
}