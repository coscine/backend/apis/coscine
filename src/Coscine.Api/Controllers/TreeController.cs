﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.QueryParameters;
using Coscine.Api.Core.Shared.RequestFeatures;
using Coscine.Api.ModelBinders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the file and metadata trees.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="TreeController" /> class.
/// </remarks>
/// <param name="treeService">The tree service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/projects/{projectId}/resources/{resourceId}/trees")]
[ApiController]
[Authorize]
public class TreeController(ITreeService treeService) : BaseController
{
    private readonly ITreeService _treeService = treeService;

    /// <summary>
    /// Retrieves the file tree associated with a resource.
    /// </summary>
    /// <param name="projectId">The unique identifier or slug of the project.</param>
    /// <param name="resourceId">The unique identifier of the resource.</param>
    /// <param name="treeParameters">Additional query parameters for retrieving the tree.</param>
    /// <returns>The file tree associated with the resource.</returns>
    /// <remarks>The <c>OrderBy</c> query is currently not supported.</remarks>
    [HttpGet("files", Name = "GetFileTree")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the file tree of a resource.", typeof(PagedResponse<FileTreeDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Project does not exist or has been deleted.")]
    public async Task<IActionResult> GetFileTree(
        [FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId,
        [FromRoute] Guid resourceId,
        [FromQuery] FileTreeParameters treeParameters
    )
    {
        var tree = await _treeService.GetPagedFileTreeAsync(projectId, resourceId, User, treeParameters);
        return Ok(tree);
    }

    /// <summary>
    /// Retrieves the metadata tree associated with a resource.
    /// </summary>
    /// <param name="projectId">The unique identifier or slug of the project.</param>
    /// <param name="resourceId">The unique identifier of the resource.</param>
    /// <param name="treeParameters">Additional query parameters for retrieving the tree.</param>
    /// <returns>The metadata tree associated with the resource.</returns>
    [HttpGet("metadata", Name = "GetMetadataTree")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the metadata tree of a resource.", typeof(PagedResponse<MetadataTreeDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Project does not exist or has been deleted.")]
    public async Task<IActionResult> GetMetadataTree(
        [FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId,
        [FromRoute] Guid resourceId,
        [FromQuery] MetadataTreeParameters treeParameters
    )
    {
        var tree = await _treeService.GetPagedMetadataTreeAsync(projectId, resourceId, User, treeParameters);
        return Ok(tree);
    }

    /// <summary>
    /// Retrieves the specific metadata tree associated with a resource.
    /// </summary>
    /// <param name="projectId">The unique identifier or slug of the project.</param>
    /// <param name="resourceId">The unique identifier of the resource.</param>
    /// <param name="metadataTreeQueryParameters">Additional query parameters for retrieving the tree.</param>
    /// <returns>The metadata tree associated with the resource.</returns>
    [HttpGet("metadata/specific", Name = "GetSpecificMetadataTree")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse(
        (int)HttpStatusCode.OK,
        "Returns the specific metadata tree of a resource.",
        typeof(Response<MetadataTreeDto>)
    )]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Project does not exist or has been deleted.")]
    public async Task<IActionResult> GetSpecificMetadataTree(
        [FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId,
        [FromRoute] Guid resourceId,
        [FromQuery] MetadataTreeQueryParameters metadataTreeQueryParameters
    )
    {
        var tree = await _treeService.GetMetadataTreeAsync(projectId, resourceId, User, metadataTreeQueryParameters);
        return Ok(tree);
    }

    /// <summary>
    /// Creates a new metadata tree for a resource.
    /// </summary>
    /// <param name="projectId">The unique identifier or slug of the project.</param>
    /// <param name="resourceId">The unique identifier of the resource.</param>
    /// <param name="metadataTreeForCreationDto">The metadata tree data for creation.</param>
    /// <returns>The location of the created metadata tree.</returns>
    [HttpPost("metadata", Name = "CreateMetadataTree")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.Created, "Metadata tree created.", typeof(Response<MetadataTreeDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> CreateMetadataTree(
        [FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId,
        [FromRoute] Guid resourceId,
        [FromBody] MetadataTreeForCreationDto metadataTreeForCreationDto
    )
    {
        var metadata = await _treeService.CreateMetadataTreeAsync(projectId, resourceId, User, metadataTreeForCreationDto);

        // Anonymous type for GET expects '<VARIABLE_NAME>=<VALUE>'.
        // Thus parse to variable "path" (as <VARIABLE_NAME>) and set value (as <VALUE>) to use in the query.
        var path = metadata.Path;
        return CreatedAtRoute(
            "GetMetadataTree",
            new
            {
                projectId,
                resourceId,
                path
            },
            null
        );
    }

    /// <summary>
    /// Creates a new extracted metadata tree for a resource.
    /// </summary>
    /// <param name="projectId">The unique identifier or slug of the project.</param>
    /// <param name="resourceId">The unique identifier of the resource.</param>
    /// <param name="metadataTreeForCreationDto">The metadata tree data for creation.</param>
    /// <returns>The location of the created metadata tree.</returns>
    [HttpPost("metadata/extracted", Name = "CreateExtractedMetadataTree")]
    [Authorize(Roles = ApiRoles.Administrator)]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.Created, "Extracted Metadata tree created.", typeof(Response<MetadataTreeDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Provided input has a bad format.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> CreateExtractedMetadataTree(
        [FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId,
        [FromRoute] Guid resourceId,
        [FromBody] ExtractedMetadataTreeForCreationDto metadataTreeForCreationDto
    )
    {
        var metadata = await _treeService.CreateExtractedMetadataTreeAsync(projectId, resourceId, User, metadataTreeForCreationDto);

        // Anonymous type for GET expects '<VARIABLE_NAME>=<VALUE>'.
        // Thus parse to variable "path" (as <VARIABLE_NAME>) and set value (as <VALUE>) to use in the query.
        var path = metadata.Path;
        return CreatedAtRoute(
            "CreateExtractedMetadataTree",
            new
            {
                projectId,
                resourceId,
                path
            },
            null
        );
    }

    /// <summary>
    /// Updates an existing metadata tree of a resource.
    /// </summary>
    /// <param name="projectId">The unique identifier or slug of the project.</param>
    /// <param name="resourceId">The unique identifier of the resource.</param>
    /// <param name="metadataTreeForUpdateDto">The updated metadata tree data.</param>
    /// <returns>No content.</returns>
    [HttpPut("metadata", Name = "UpdateMetadataTree")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Metadata tree updated.")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse(
        (int)HttpStatusCode.BadRequest,
        "Provided input has a bad format or the resource is write-protected due to its archived status."
    )]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> UpdateMetadataTree(
        [FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId,
        [FromRoute] Guid resourceId,
        [FromBody] MetadataTreeForUpdateDto metadataTreeForUpdateDto
    )
    {
        await _treeService.UpdateMetadataTreeAsync(projectId, resourceId, User, metadataTreeForUpdateDto);
        return NoContent();
    }

    /// <summary>
    /// Updates an existing metadata tree of a resource.
    /// </summary>
    /// <param name="projectId">The unique identifier or slug of the project.</param>
    /// <param name="resourceId">The unique identifier of the resource.</param>
    /// <param name="metadataTreeForUpdateDto">The updated metadata tree data.</param>
    /// <returns>No content.</returns>
    [HttpPut("metadata/extracted", Name = "UpdateExtractedMetadataTree")]
    [Authorize(Roles = ApiRoles.Administrator)]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Metadata tree updated.")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse(
        (int)HttpStatusCode.BadRequest,
        "Provided input has a bad format or the resource is write-protected due to its archived status."
    )]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> UpdateExtractedMetadataTree(
        [FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId,
        [FromRoute] Guid resourceId,
        [FromBody] ExtractedMetadataTreeForUpdateDto metadataTreeForUpdateDto
    )
    {
        await _treeService.UpdateExtractedMetadataTreeAsync(projectId, resourceId, User, metadataTreeForUpdateDto);
        return NoContent();
    }

    /// <summary>Deletes (invalidates) a metadata tree associated with a resource.</summary>
    /// <param name="projectId">The unique identifier or slug of the project.</param>
    /// <param name="resourceId">The unique identifier of the resource.</param>
    /// <param name="metadataTreeForDeletionDto">The dto for the deletion.</param>
    /// <returns>No content.</returns>
    [HttpDelete("metadata", Name = "DeleteMetadataTree")]
    [Authorize(Roles = ApiRoles.Administrator)]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Metadata tree deleted.")]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [SwaggerResponse(
        (int)HttpStatusCode.BadRequest,
        "Provided input has a bad format or the resource is write-protected due to its archived status."
    )]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Provided input refers to entries that do not exist or have been deleted.")]
    public async Task<IActionResult> DeleteMetadataTree(
        [FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId,
        [FromRoute] Guid resourceId,
        [FromBody] MetadataTreeForDeletionDto metadataTreeForDeletionDto
    )
    {
        await _treeService.DeleteMetadataTreeAsync(projectId, resourceId, User, metadataTreeForDeletionDto);
        return NoContent();
    }
}
