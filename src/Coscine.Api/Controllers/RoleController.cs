﻿using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the roles.
/// </summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/roles")]
[ApiController]
[Authorize]
public class RoleController : BaseController
{
    private readonly IRoleService _roleService;

    /// <summary>
    /// Initializes a new instance of the <see cref="RoleController" /> class.
    /// </summary>
    /// <param name="roleService">The role service.</param>
    public RoleController(IRoleService roleService)
    {
        _roleService = roleService;
    }

    /// <summary>
    /// Retrieves a role by ID.
    /// </summary>
    /// <param name="roleId">The ID of the role.</param>
    /// <returns>The retrieved role.</returns>
    [HttpGet("{roleId:guid}", Name = "GetRole")]
    [OutputCache(VaryByRouteValueNames = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the role.", typeof(Response<RoleDto>))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Role does not exist.")]
    [AllowAnonymous]
    public async Task<IActionResult> GetRole([FromRoute] Guid roleId)
    {
        var role = await _roleService.GetRoleById(roleId, trackChanges: false);

        return Ok(role);
    }

    /// <summary>
    /// Retrieves all roles.
    /// </summary>
    /// <param name="roleParameters">The parameters for role filtering and pagination.</param>
    /// <returns>The retrieved roles.</returns>
    [HttpGet(Name = "GetRoles")]
    [OutputCache(VaryByQueryKeys = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the roles.", typeof(PagedResponse<RoleDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetRoles([FromQuery] RoleParameters roleParameters)
    {
        var roles = await _roleService.GetPagedRoles(roleParameters, trackChanges: false);

        return Ok(roles);
    }
}