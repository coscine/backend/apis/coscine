﻿using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the languages.
/// </summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/languages")]
[ApiController]
[Authorize]
public class LanguageController : BaseController
{
    private readonly ILanguageService _languageService;

    /// <summary>Initializes a new instance of the <see cref="LanguageController" /> class.</summary>
    /// <param name="languageService">The language service.</param>
    public LanguageController(ILanguageService languageService)
    {
        _languageService = languageService;
    }

    /// <summary>
    /// Retrieves a language by ID.
    /// </summary>
    /// <param name="languageId">The ID of the language.</param>
    /// <returns>The retrieved language.</returns>
    [HttpGet("{languageId:guid}", Name = "GetLanguage")]
    [OutputCache(VaryByRouteValueNames = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the language.", typeof(Response<LanguageDto>))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Language does not exist.")]
    [AllowAnonymous]
    public async Task<IActionResult> GetLanguage([FromRoute] Guid languageId)
    {
        var language = await _languageService.GetLanguageById(languageId, trackChanges: false);
        return Ok(language);
    }

    /// <summary>
    /// Retrieves all languages.
    /// </summary>
    /// <param name="languageParameters">The parameters for language filtering and pagination.</param>
    /// <returns>The retrieved languages.</returns>
    [HttpGet(Name = "GetLanguages")]
    [OutputCache(VaryByQueryKeys = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the languages.", typeof(Response<IEnumerable<LanguageDto>>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetLanguages([FromQuery] LanguageParameters languageParameters)
    {
        var languages = await _languageService.GetAllLanguages(languageParameters, trackChanges: false);
        return Ok(languages);
    }
}