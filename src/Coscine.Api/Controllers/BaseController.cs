﻿using Coscine.Api.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Coscine.Api.Controllers;

/// <summary>
/// Base controller for all controllers.
/// </summary>
public abstract class BaseController : ControllerBase
{
    /// <summary>
    /// Responds with the HTTP methods allowed for the endpoint.
    /// </summary>
    /// <returns>Response with the 'Access-Control-Allow-Methods' header populated with the allowed HTTP methods for the endpoint.</returns>
    [HttpOptions]
    [AllowAnonymous]
    public IActionResult GetEndpointOptions()
    {
        var methods = this.GetUsedHttpMethods();
        Response.Headers.Append("Access-Control-Allow-Methods", methods);
        return Ok();
    }
}
