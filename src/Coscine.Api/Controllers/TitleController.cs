﻿using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the titles.
/// </summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/titles")]
[ApiController]
[Authorize]
public class TitleController : BaseController
{
    private readonly ITitleService _titleService;

    /// <summary>
    /// Initializes a new instance of the <see cref="TitleController" /> class.
    /// </summary>
    /// <param name="titleService">The title service.</param>
    public TitleController(ITitleService titleService)
    {
        _titleService = titleService;
    }

    /// <summary>
    /// Retrieves a title by ID.
    /// </summary>
    /// <param name="titleId">The ID of the title.</param>
    /// <returns>The retrieved title.</returns>
    [HttpGet("{titleId:guid}", Name = "GetTitle")]
    [OutputCache(VaryByRouteValueNames = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the title.", typeof(Response<TitleDto>))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Title does not exist.")]
    [AllowAnonymous]
    public async Task<IActionResult> GetTitle([FromRoute] Guid titleId)
    {
        var title = await _titleService.GetTitleByIdAsync(titleId, trackChanges: false);
        return Ok(title);
    }

    /// <summary>
    /// Retrieves all titles.
    /// </summary>
    /// <param name="titleParameters">The parameters for title filtering and pagination.</param>
    /// <returns>The retrieved titles.</returns>
    [HttpGet(Name = "GetTitles")]
    [OutputCache(VaryByQueryKeys = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the titles.", typeof(Response<IEnumerable<TitleDto>>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetTitles([FromQuery] TitleParameters titleParameters)
    {
        var titles = await _titleService.GetAllTitlesAsync(titleParameters, trackChanges: false);
        return Ok(titles);
    }
}