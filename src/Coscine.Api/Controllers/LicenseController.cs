﻿using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the licenses.
/// </summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/licenses")]
[ApiController]
[Authorize]
public class LicenseController : BaseController
{
    private readonly ILicenseService _licenseService;

    /// <summary>
    /// Initializes a new instance of the <see cref="LicenseController" /> class.
    /// </summary>
    /// <param name="licenseService">The license service.</param>
    public LicenseController(ILicenseService licenseService)
    {
        _licenseService = licenseService;
    }

    /// <summary>
    /// Retrieves a license with the specified ID.
    /// </summary>
    /// <param name="licenseId">The ID of the license.</param>
    /// <returns>The retrieved license.</returns>
    [HttpGet("{licenseId:guid}", Name = "GetLicense")]
    [OutputCache(VaryByRouteValueNames = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the license.", typeof(Response<LicenseDto>))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "License does not exist.")]
    [AllowAnonymous]
    public async Task<IActionResult> GetLicense([FromRoute] Guid licenseId)
    {
        var license = await _licenseService.GetLicenseById(licenseId, trackChanges: false);
        return Ok(license);
    }

    /// <summary>
    /// Retrieves all licenses.
    /// </summary>
    /// <param name="licenseParameters">The parameters for license filtering and pagination.</param>
    /// <returns>The retrieved licenses.</returns>
    [HttpGet(Name = "GetLicenses")]
    [OutputCache(VaryByQueryKeys = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60)]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the licenses.", typeof(PagedResponse<LicenseDto>))]
    [AllowAnonymous]
    public async Task<IActionResult> GetLicenses([FromQuery] LicenseParameters licenseParameters)
    {
        var licenses = await _licenseService.GetPagedLicenses(licenseParameters, trackChanges: false);
        return Ok(licenses);
    }
}