﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.ModelBinders;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.AspNetCore.WebUtilities;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the Binary Large Objects (BLOBs).
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="BlobController" /> class.
/// </remarks>
/// <param name="blobService">The BLOB service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/projects/{projectId}/resources/{resourceId}/blobs")]
[ApiController]
[Authorize]
public class BlobController(IBlobService blobService) : BaseController
{
    private readonly FileExtensionContentTypeProvider _fileExtensionContentTypeProvider = new();
    private readonly IBlobService _blobService = blobService;

    /// <summary>
    /// Creates a new blob for a resource.
    /// </summary>
    /// <param name="projectId">The identifier or slug of the project.</param>
    /// <param name="resourceId">The identifier of the resource.</param>
    /// <param name="key">The key for the new blob.</param>
    /// <returns>The location of the created blob.</returns>
    [HttpPost("{key}", Name = "CreateBlob"), DisableRequestSizeLimit, RequestFormLimits(MultipartBodyLengthLimit = long.MaxValue)]
    [MultipartFormData]
    [DisableFormValueModelBinding]
    [RequireContentLength]
    [SwaggerResponse((int)HttpStatusCode.Created, "Blob created.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Blob already exists.")]
    [SwaggerResponse((int)HttpStatusCode.UnsupportedMediaType, @"Blob is not of the type ""multipart/form-data""")]
    [SwaggerFileUpload]
    public async Task<IActionResult> CreateBlob([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromRoute] Guid resourceId, string key)
    {
        // Trim leading forward slash for path consistency
        var unescapedKey = Uri.UnescapeDataString(key).TrimStart('/');

        // Parse the multipart upload and retrieve the respective stream
        var boundary = HttpContext.Request.GetMultipartBoundary();
        if (boundary is null)
        {
            return BadRequest();
        }
        var reader = new MultipartReader(boundary, HttpContext.Request.Body);
        var section = await reader.ReadNextSectionAsync();
        if (section is null)
        {
            return BadRequest();
        }

        await _blobService.CreateBlobAsync(projectId, resourceId, unescapedKey, User, section.Body, HttpContext.Request.ContentLength ?? 0, trackChanges: false);

        // Anonymous type for GET expects '<VARIABLE_NAME>=<VALUE>'.
        // Endpoint expects a value "key", must overwrite it with the unescaped/decoded value.
        // Otherwise it will get double-encoded ("%2F" -> "%252F" in "Location" response header).
        key = unescapedKey;
        return CreatedAtRoute("GetBlob", new { projectId, resourceId, key }, null);
    }

    /// <summary>
    /// Download a blob from a resource.
    /// </summary>
    /// <param name="projectId">The identifier or slug of the project.</param>
    /// <param name="resourceId">The identifier of the resource.</param>
    /// <param name="key">The key associated with the blob.</param>
    /// <returns>The raw data of the blob as a file stream.</returns>
    [HttpGet("{key}", Name = "GetBlob")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the blob.", typeof(FileStreamResult), "application/octet-stream")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Blob does not exist.")]
    public async Task<IActionResult> GetBlob([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromRoute] Guid resourceId, string key)
    {
        // Trim leading forward slash for path consistency
        var unescapedKey = Uri.UnescapeDataString(key).TrimStart('/');

        var data = await _blobService.GetBlobStreamAsync(projectId, resourceId, unescapedKey, User, trackChanges: false);
        _fileExtensionContentTypeProvider.TryGetContentType(unescapedKey, out var contentType);

        return File(data, contentType ?? "application/octet-stream", Path.GetFileName(unescapedKey));
    }

    /// <summary>
    /// Updates an existing blob of a resource.
    /// </summary>
    /// <param name="projectId">The identifier or slug of the project.</param>
    /// <param name="resourceId">The identifier of the resource.</param>
    /// <param name="key">The key associated with the blob to update.</param>
    /// <returns>No content.</returns>
    [HttpPut("{key}", Name = "UpdateBlob"), DisableRequestSizeLimit, RequestFormLimits(MultipartBodyLengthLimit = long.MaxValue)]
    [MultipartFormData]
    [DisableFormValueModelBinding]
    [RequireContentLength]
    [SwaggerResponse((int)HttpStatusCode.Created, "Blob updated.")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "Blob already exists.")]
    [SwaggerResponse((int)HttpStatusCode.UnsupportedMediaType, @"Blob is not of the type ""multipart/form-data""")]
    [SwaggerFileUpload]
    public async Task<IActionResult> UpdateBlob([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, [FromRoute] Guid resourceId, string key)
    {
        // Trim leading forward slash for path consistency
        var unescapedKey = Uri.UnescapeDataString(key).TrimStart('/');

        // Parse the multipart upload and retrieve the respective stream
        var boundary = HttpContext.Request.GetMultipartBoundary();
        if (boundary is null)
        {
            return BadRequest();
        }
        var reader = new MultipartReader(boundary, HttpContext.Request.Body);
        var section = await reader.ReadNextSectionAsync();
        if (section is null)
        {
            return BadRequest();
        }

        await _blobService.UpdateBlobAsync(projectId, resourceId, unescapedKey, User, section.Body, HttpContext.Request.ContentLength ?? 0, trackChanges: false);
        return NoContent();
    }

    /// <summary>
    /// Deletes a blob from a resource.
    /// </summary>
    /// <param name="projectId">The identifier or slug of the project.</param>
    /// <param name="resourceId">The identifier of the resource.</param>
    /// <param name="key">The key associated with the blob to delete.</param>
    /// <returns>No content.</returns>
    [HttpDelete("{key}", Name = "DeleteBlob")]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Blob deleted.")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "Blob does not exist.")]
    public async Task<IActionResult> DeleteBlob([FromRoute][ModelBinder(typeof(ProjectIdModelBinder))] Guid projectId, Guid resourceId, string key)
    {
        // Trim leading forward slash for path consistency
        var unescapedKey = Uri.UnescapeDataString(key).TrimStart('/');

        await _blobService.DeleteBlobAsync(projectId, resourceId, unescapedKey, User, trackChanges: false);
        return NoContent();
    }
}
