﻿using Asp.Versioning;
using Coscine.Api.ActionFilters;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the pids.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="PidController" /> class.
/// </remarks>
/// <param name="pidService">The pid service.</param>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/pids")]
[ApiController]
[Authorize]
public class PidController(IPidService pidService) : BaseController
{
    private readonly IPidService _pidService = pidService;

    /// <summary>
    /// Retrieves the Persistent Identifier (PID) for a given prefix and suffix.
    /// </summary>
    /// <param name="prefix">The PID prefix. Limited to the values provided by the API.</param>
    /// <param name="suffix">The PID suffix of a project or a resource, represented as a GUID.</param>
    /// <returns>
    /// An HTTP response containing the PID wrapped in a <see cref="Response{T}"/> with the
    /// specified type <see cref="PidDto"/>.
    /// </returns>
    [HttpGet("{prefix}/{suffix:guid}", Name = "GetPid")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Successful response with the PID", typeof(Response<PidDto>))]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "The specified PID does not exist")]
    [SwaggerResponse((int)HttpStatusCode.Gone, "The PID is no longer valid (e.g., resource deleted)")]
    [AllowAnonymous]
    public async Task<IActionResult> GetPid(string prefix, Guid suffix)
    {
        var pidResult = await _pidService.GetAsync(prefix, suffix.ToString(), User, trackChanges: false);
        return Ok(pidResult);
    }

    /// <summary>
    /// Sends an inquiry to the owner of the given PID.
    /// </summary>
    /// <param name="prefix">The PID prefix of a project or a resource to validate</param>
    /// <param name="suffix">The PID body of a project or a resource to validate</param>
    /// <param name="pidRequestDto">The data transfer object containing the inquiry details.</param>
    /// <returns>No content.</returns>
    [HttpPost("{prefix}/{suffix:guid}/requests", Name = "SendRequestToOwner")]
    [ServiceFilter(typeof(ValidationFilter))]
    [SwaggerResponse((int)HttpStatusCode.NoContent, "Email sent successfully")]
    [SwaggerResponse((int)HttpStatusCode.BadRequest, "PID has a bad format")]
    [SwaggerResponse((int)HttpStatusCode.NotFound, "PID does not exist or is invalid")]
    [AllowAnonymous]
    public async Task<IActionResult> SendRequestToOwner(string prefix, Guid suffix, [FromBody] PidRequestDto pidRequestDto)
    {
        await _pidService.SendEmailToOwnerAsync(prefix, suffix.ToString(), pidRequestDto, trackChanges: false);
        return NoContent();
    }

    /// <summary>
    /// Retrieves all PIDs.
    /// </summary>
    /// <param name="pidParameters">Additional query parameters for retrieving the PID.</param>
    /// <returns>The retrieved PIDs.</returns>
    [HttpGet(Name = "GetPids")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the PIDs.", typeof(PagedResponse<PidDto>))]
    [SwaggerResponse((int)HttpStatusCode.Forbidden, "User is missing authorization requirements.")]
    [Authorize(Roles = ApiRoles.Administrator)]
    public async Task<IActionResult> GetPids([FromQuery] PidParameters pidParameters)
    {
        var pids = await _pidService.GetPagedAsync(User, pidParameters, trackChanges: false);
        return Ok(pids);
    }
}