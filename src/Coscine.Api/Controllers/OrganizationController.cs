﻿using Asp.Versioning;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OutputCaching;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace Coscine.Api.Controllers;

/// <summary>
/// Endpoints for the organizations.
/// </summary>
[ApiVersion("2.0")]
[Route("api/v{version:apiVersion}/organizations")]
[ApiController]
[Authorize]
public class OrganizationController : BaseController
{
    private readonly IOrganizationService _organizationService;

    /// <summary>
    /// Initializes a new instance of the <see cref="OrganizationController" /> class.
    /// </summary>
    /// <param name="organizationService">The organization service.</param>
    public OrganizationController(IOrganizationService organizationService)
    {
        _organizationService = organizationService;
    }

    /// <summary>
    /// Retrieves an organization.
    /// </summary>
    /// <param name="organizationRorUri">The parameters for organization filtering and pagination.</param>
    /// <returns>The retrieved organizations.</returns>
    [HttpGet("{organizationRorUri}", Name = "GetOrganization")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the organizations.", typeof(Response<OrganizationDto>))]
    public async Task<IActionResult> GetOrganization([FromRoute] string organizationRorUri)
    {
        if (Uri.TryCreate(Uri.UnescapeDataString(organizationRorUri), UriKind.Absolute, out var parsedUri))
        {
            var organizations = await _organizationService.GetOrganization(parsedUri, trackChanges: false);
            return Ok(organizations);
        }
        else
        {
            return BadRequest($"The provided input: {organizationRorUri} can not be parsed to a valid URI.");
        }
    }

    /// <summary>
    /// Retrieves all organizations.
    /// </summary>
    /// <param name="organizationParameters">The parameters for organization filtering and pagination.</param>
    /// <returns>The retrieved organizations.</returns>
    [HttpGet(Name = "GetOrganizations")]
    [OutputCache(VaryByQueryKeys = ["*"], VaryByHeaderNames = ["accept"], Duration = 30 * 60, PolicyName = "OutputCacheWithAuthPolicy")]
    [SwaggerResponse((int)HttpStatusCode.OK, "Returns the organizations.", typeof(PagedResponse<OrganizationDto>))]
    public async Task<IActionResult> GetOrganizations([FromQuery] OrganizationParameters organizationParameters)
    {
        var organizations = await _organizationService.GetPagedOrganizations(organizationParameters, trackChanges: false);
        return Ok(organizations);
    }
}
