using System.Text.Json.Serialization;

namespace Coscine.Api.Extensions;

/// <summary>
/// Represents a user obtained from the OpenID Connect UserInfo endpoint.
/// </summary>
public class OIDCUser
{
    /// <summary>
    /// Gets the list of entitlements associated with the user.
    /// </summary>
    [JsonPropertyName("entitlements")]
    public List<string> Entitlements { get; } = [];

    /// <summary>
    /// Gets or sets the subject identifier for the user.
    /// </summary>
    [JsonPropertyName("sub")]
    public string? Sub { get; set; }

    /// <summary>
    /// Gets the list of external affiliations associated with the user.
    /// </summary>
    [JsonObjectCreationHandling(JsonObjectCreationHandling.Populate)]
    [JsonPropertyName("voperson_external_affiliation")]
    public List<string> VopersonExternalAffiliation { get; } = [];

    /// <summary>
    /// Gets or sets the ePPN (eduPersonPrincipalName) for the user.
    /// </summary>
    [JsonPropertyName("eppn")]
    public string? Eppn { get; set; }

    /// <summary>
    /// Gets or sets the given name of the user.
    /// </summary>
    [JsonPropertyName("given_name")]
    public string? GivenName { get; set; }

    /// <summary>
    /// Gets the list of entitlements specific to educational institutions associated with the user.
    /// </summary>
    [JsonPropertyName("eduperson_entitlement")]
    public List<string> EdupersonEntitlement { get; } = [];

    /// <summary>
    /// Gets or sets the VoPerson identifier for the user.
    /// </summary>
    [JsonPropertyName("voperson_id")]
    public string? VopersonId { get; set; }

    /// <summary>
    /// Gets or sets the full name of the user.
    /// </summary>
    [JsonPropertyName("name")]
    public string? Name { get; set; }

    /// <summary>
    /// Gets the list of scoped affiliations associated with the user.
    /// </summary>
    [JsonPropertyName("eduperson_scoped_affiliation")]
    public List<string> EdupersonScopedAffiliation { get; } = [];

    /// <summary>
    /// Gets the list of assurances associated with the user.
    /// </summary>
    [JsonPropertyName("eduperson_assurance")]
    public List<string> EdupersonAssurance { get; } = [];

    /// <summary>
    /// Gets or sets the family name (surname) of the user.
    /// </summary>
    [JsonPropertyName("family_name")]
    public string? FamilyName { get; set; }

    /// <summary>
    /// Gets or sets the email address of the user.
    /// </summary>
    [JsonPropertyName("email")]
    public string? Email { get; set; }
}
