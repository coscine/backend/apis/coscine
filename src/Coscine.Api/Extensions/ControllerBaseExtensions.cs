﻿namespace Coscine.Api.Extensions;

using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

/// <summary>Extensions for the <see cref="ControllerBase" /> class.</summary>
public static class ControllerBaseExtensions
{
    /// <summary>Gets the used HTTP methods.</summary>
    /// <param name="controller">The controller.</param>
    /// <returns>The used HTTP method as a string.</returns>
    public static string GetUsedHttpMethods(this ControllerBase controller)
    {
        var methods = new List<string>();
        var controllerType = controller.GetType();
        var controllerMethods = controllerType.GetMethods()
            .Where(m => m.IsPublic && !m.IsDefined(typeof(NonActionAttribute)))
            .Select(m => new
            {
                Name = m.Name.ToLowerInvariant(),
                HasHttpGetAttribute = m.IsDefined(typeof(HttpGetAttribute)),
                HasHttpPostAttribute = m.IsDefined(typeof(HttpPostAttribute)),
                HasHttpPutAttribute = m.IsDefined(typeof(HttpPutAttribute)),
                HasHttpDeleteAttribute = m.IsDefined(typeof(HttpDeleteAttribute)),
                HasHttpPatchAttribute = m.IsDefined(typeof(HttpPatchAttribute)),
                HasHttpOptionsAttribute = m.IsDefined(typeof(HttpOptionsAttribute)),
                HasHttpHeadAttribute = m.IsDefined(typeof(HttpHeadAttribute))
            });
        var controllerMethodsWithHttpMethods = controllerMethods.Where(m =>
            m.HasHttpGetAttribute ||
            m.HasHttpPostAttribute ||
            m.HasHttpPutAttribute ||
            m.HasHttpDeleteAttribute ||
            m.HasHttpPatchAttribute ||
            m.HasHttpOptionsAttribute ||
            m.HasHttpHeadAttribute
        );
        if (controllerMethodsWithHttpMethods.Any(m => m.HasHttpGetAttribute))
        {
            methods.Add("GET");
        }

        if (controllerMethodsWithHttpMethods.Any(m => m.HasHttpPostAttribute))
        {
            methods.Add("POST");
        }

        if (controllerMethodsWithHttpMethods.Any(m => m.HasHttpPutAttribute))
        {
            methods.Add("PUT");
        }

        if (controllerMethodsWithHttpMethods.Any(m => m.HasHttpDeleteAttribute))
        {
            methods.Add("DELETE");
        }

        if (controllerMethodsWithHttpMethods.Any(m => m.HasHttpPatchAttribute))
        {
            methods.Add("PATCH");
        }

        if (controllerMethodsWithHttpMethods.Any(m => m.HasHttpOptionsAttribute))
        {
            methods.Add("OPTIONS");
        }

        if (controllerMethodsWithHttpMethods.Any(m => m.HasHttpHeadAttribute))
        {
            methods.Add("HEAD");
        }

        return string.Join(",", methods);
    }
}