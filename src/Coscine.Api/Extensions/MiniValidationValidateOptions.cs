﻿using Microsoft.Extensions.Options;
using MiniValidation;

namespace Coscine.Api.Extensions;

/// <summary>
/// Provides validation for options using MiniValidation.
/// </summary>
/// <typeparam name="TOptions">The type of options to validate.</typeparam>
/// <remarks>
/// Initializes a new instance of the <see cref="MiniValidationValidateOptions{TOptions}"/> class.
/// </remarks>
/// <param name="name">The name of the options. Can be null.</param>
public class MiniValidationValidateOptions<TOptions>(string? name) : IValidateOptions<TOptions>
    where TOptions : class
{
    /// <summary>
    /// Gets the name of the options being validated.
    /// </summary>
    public string? Name { get; } = name;

    /// <summary>
    /// Validates the specified options.
    /// </summary>
    /// <param name="name">The name of the options to validate. Can be null to configure all named options.</param>
    /// <param name="options">The options to validate.</param>
    /// <returns>A <see cref="ValidateOptionsResult"/> indicating whether validation succeeded or failed.</returns>
    public ValidateOptionsResult Validate(string? name, TOptions options)
    {
        // Null name is used to configure ALL named options, so always applies.
        if (Name != null && Name != name)
        {
            // Ignored if not validating this instance.
            return ValidateOptionsResult.Skip;
        }

        // Ensure options are provided to validate against
        ArgumentNullException.ThrowIfNull(options);

        // MiniValidation validation
        if (MiniValidator.TryValidate(options, out var validationErrors))
        {
            return ValidateOptionsResult.Success;
        }

        string typeName = options.GetType().Name;
        var errors = new List<string>();
        foreach (var (member, memberErrors) in validationErrors)
        {
            errors.Add(
                $"DataAnnotation validation failed for '{typeName}' member: '{member}' with errors: '{string.Join("', '", memberErrors)}'."
            );
        }

        return ValidateOptionsResult.Fail(errors);
    }
}
