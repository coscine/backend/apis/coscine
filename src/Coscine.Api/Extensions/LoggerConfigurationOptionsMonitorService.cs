﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Microsoft.Extensions.Options;
using NLog;

namespace Coscine.Api.Extensions;

/// <summary>Monitors for changes in the logger configuration.</summary>
public class LoggerConfigurationOptionsMonitorService : IHostedService
{
    private readonly IOptionsMonitor<LoggerConfiguration> _optionsMonitor;
    private IDisposable? _optionsChangeToken;

    /// <summary>Initializes a new instance of the <see cref="LoggerConfigurationOptionsMonitorService" /> class.</summary>
    /// <param name="optionsMonitor">The options monitor.</param>
    public LoggerConfigurationOptionsMonitorService(IOptionsMonitor<LoggerConfiguration> optionsMonitor)
    {
        _optionsMonitor = optionsMonitor;
    }

    /// <summary>Triggered when the application host is ready to start the service.</summary>
    /// <param name="cancellationToken">Indicates that the start process has been aborted.</param>
    public Task StartAsync(CancellationToken cancellationToken)
    {
        // Register a callback to be executed when the options change
        _optionsChangeToken = _optionsMonitor.OnChange(HandleOptionsChange);

        return Task.CompletedTask;
    }

    /// <summary>Triggered when the application host is performing a graceful shutdown.</summary>
    /// <param name="cancellationToken">Indicates that the shutdown process should no longer be graceful.</param>
    public Task StopAsync(CancellationToken cancellationToken)
    {
        // Dispose of the options change token when stopping the service
        _optionsChangeToken?.Dispose();

        return Task.CompletedTask;
    }

    /// <summary>Handles the options change.</summary>
    /// <param name="options">The options.</param>
    private static void HandleOptionsChange(LoggerConfiguration options)
    {
        // Set the default LogLevel
        LogManager.Configuration.Variables["logLevel"] = options.LogLevel ?? "Trace";

        // Set the log location
        LogManager.Configuration.Variables["logHome"] = options.LogHome ?? Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs");

        LogManager.ReconfigExistingLoggers();
    }
}