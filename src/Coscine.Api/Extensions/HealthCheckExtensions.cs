using Microsoft.Extensions.Diagnostics.HealthChecks;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text;

namespace Coscine.Api.Extensions;

/// <summary>
/// Provides extension methods for health check-related operations.
/// </summary>
public static class HealthCheckExtensions
{
    private static readonly JsonSerializerOptions jsonSerializerOptions = new()
    {
        WriteIndented = false,
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
    };

    /// <summary>
    /// Writes the health check report as a JSON response to the HTTP context.
    /// </summary>
    /// <param name="context">The HTTP context to which the response will be written.</param>
    /// <param name="report">The health report containing the status and details of the health checks.</param>
    /// <returns>A task that represents the asynchronous operation of writing the response.</returns>
    public static Task WriteResponse(HttpContext context, HealthReport report)
    {
        // Code taken mostly from the MS example page

        context.Response.ContentType = "application/json; charset=utf-8";

        var options = new JsonWriterOptions { Indented = true };

        using var memoryStream = new MemoryStream();
        using (var jsonWriter = new Utf8JsonWriter(memoryStream, options))
        {
            jsonWriter.WriteStartObject();
            jsonWriter.WriteString("status", report.Status.ToString());
            jsonWriter.WriteString("totalDuration", report.TotalDuration.ToString());
            jsonWriter.WriteStartObject("results");

            foreach (var healthReportEntry in report.Entries)
            {
                jsonWriter.WriteStartObject(healthReportEntry.Key);
                jsonWriter.WriteString("status", healthReportEntry.Value.Status.ToString());
                jsonWriter.WriteString("description", healthReportEntry.Value.Description);
                jsonWriter.WriteString("duration", healthReportEntry.Value.Duration.ToString());
                jsonWriter.WriteStartObject("data");

                foreach (var item in healthReportEntry.Value.Data)
                {
                    jsonWriter.WritePropertyName(item.Key);

                    JsonSerializer.Serialize(jsonWriter, item.Value, item.Value?.GetType() ?? typeof(object));
                }

                jsonWriter.WriteEndObject();
                jsonWriter.WriteEndObject();
            }

            jsonWriter.WriteEndObject();
            jsonWriter.WriteEndObject();
        }

        return context.Response.WriteAsync(
            Encoding.UTF8.GetString(memoryStream.ToArray()));
    }
}