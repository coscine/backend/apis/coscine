﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.Exceptions;
using Coscine.Api.ErrorHandlers;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using NLog;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace Coscine.Api.Extensions;

/// <summary>
/// Class to provide a type for the ILogger.
/// </summary>
public class ExceptionHandlerMiddleware
{ }

/// <summary>
/// Extensions for the <see cref="WebApplication" /> class.
/// Provides methods for setting up and configuring exception handling middleware.
/// </summary>
public static class ExceptionMiddlewareExtensions
{
    /// <summary>
    /// Configures the exception handler middleware for the application.
    /// </summary>
    /// <param name="app">The instance of <see cref="WebApplication"/> representing the application.</param>
    /// <param name="logger">The logger instance for logging exceptions.</param>

    public static void ConfigureExceptionHandler(this WebApplication app, ILogger<ExceptionHandlerMiddleware> logger)
    {
        // This method sets up middleware to handle exceptions in the application.
        // It configures the handling of errors and logs relevant information.
        app.UseExceptionHandler(appError =>
        {
            appError.Run(async context =>
            {
                // It sets the HTTP status code and content type for the response in case of an error.
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                context.Response.ContentType = "application/json";

                // Retrieves the feature that handles exceptions from the context.
                var contextFeature = context.Features.Get<IExceptionHandlerFeature>();

                // Checks if there is an exception to handle.
                if (contextFeature is not null)
                {
                    // Retrieves error information that implements ICoscineHttpError.
                    var httpError = contextFeature.Error as ICoscineHttpError;

                    // Determines the HTTP status code for the response based on the error.
                    context.Response.StatusCode = httpError?.StatusCode ?? StatusCodes.Status500InternalServerError;

                    // Builds a ProblemDetails object containing information about the error.
                    // Some details might include type, title, status, details, and instance of the error.

                    var relativeUrl = $"{context.Request.Path}{context.Request.QueryString}";
                    var instance = string.IsNullOrWhiteSpace(relativeUrl) ? null : relativeUrl;

                    var problemDetails = new ProblemDetails
                    {
                        Type = httpError?.Type,
                        Title = httpError?.Title,
                        Status = context.Response.StatusCode,
                        // Avoid leaking stack trace, if exception is unexpected
                        Detail = app.Environment.IsProduction() ? null : contextFeature.Error.Message,
                        Instance = instance
                    };

                    problemDetails.Extensions.Add("httpMethod", context.Request.Method);
                    problemDetails.Extensions.Add("errorId", httpError?.GetType().Name);

                    // Determines the property key based on available user claims.
                    var propertyKey = context.User.FindFirst("userId") is not null ? "userId" : context.User.FindFirst("tokenId") is not null ? "tokenId" : null;

                    if (propertyKey != null)
                    {
                        // Associates the context with the identified property in the logging scope.
                        // As any scope was lost due to the exception, add relevant information back to the scope.
                        using (ScopeContext.PushProperty(propertyKey, context.User.FindFirst(propertyKey)))
                        {
                            // Logs the error details along with the specified log level and message.
                            var logLevel = httpError is null ? LogLevel.Error : LogLevel.Warning;
                            var logMessage = httpError is null ? "Unexpected exception" : "Expected exception";

                            logger.Log(logLevel, contextFeature.Error, "{logMessage}: ProblemDetails: {problemDetails}", logMessage, problemDetails.SerializeWithCamelCase());
                        }
                    }

                    // Special filter for custom behaviour
                    var attribute = contextFeature.Endpoint?.Metadata.GetMetadata<CoscineErrorHandlerAttribute>();
                    if (attribute is not null)
                    {
                        try
                        {
                            await attribute.HandleError(context, problemDetails);
                            return;
                        }
                        catch (Exception e)
                        {
                            logger.Log(LogLevel.Warning, e, "{logMessage}", "Error while handling special error case.");
                        }
                    }

                    // Sends an error response to the client with the formatted problem details.
                    await context.Response.WriteAsync(new InternalResponse<object>()
                    {
                        Data = problemDetails,
                        StatusCode = context.Response.StatusCode,
                        TraceId = context.TraceIdentifier,
                    }.ToString());
                }
            });
        });
    }
}