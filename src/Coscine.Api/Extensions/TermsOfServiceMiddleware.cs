﻿using Coscine.Api.Attributes;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.InternalServerError;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

namespace Coscine.Api.Extensions;

/// <summary>
/// Middleware for enforcing Terms of Service acceptance.
/// <param name="next">The request delegate.</param>
/// </summary>
public class TermsOfServiceMiddleware(RequestDelegate next)
{
    private readonly RequestDelegate _next = next;

    /// <summary>
    /// Invokes the middleware to check if the user has accepted the Terms of Service.
    /// </summary>
    /// <param name="context">The HTTP context.</param>
    /// <param name="authenticatorService">The authenticator service.</param>
    /// <param name="termsOfServiceConfiguration">The current Terms of Service configuration.</param>
    /// <param name="termsOfServiceRepository">The repository for Terms of Service data.</param>
    /// <returns>A task representing the asynchronous operation.</returns>
    public async Task Invoke(
        HttpContext context,
        IAuthenticatorService authenticatorService,
        IOptionsMonitor<TermsOfServiceConfiguration> termsOfServiceConfiguration,
        ITermsOfServiceRepository termsOfServiceRepository
    )
    {
        var endpoint = context.GetEndpoint();

        // Skip this middleware if:
        // - user is not authenticated
        // - endpoint is marked to allow anonymous access
        // - endpoint uses the SkipTermsOfServiceCheckAttribute
        // - user is an administrator (also covers all endpoints requiring admin role)
        if (
            context.User.Identity?.IsAuthenticated == false
            || endpoint?.Metadata.GetMetadata<AllowAnonymousAttribute>() is not null
            || endpoint?.Metadata.GetMetadata<SkipTermsOfServiceCheckAttribute>() is not null
            || context.User.IsInRole(ApiRoles.Administrator)
        )
        {
            await _next(context);
            return;
        }

        // Retrieve the authenticated user
        var user = await authenticatorService.GetUserAsync(context.User, trackChanges: true)
            ?? throw new TermsOfServicesNotAcceptedBadRequestException();

        // Retrieve the current version of Terms of Service
        var currentToSVersion =
            termsOfServiceConfiguration.CurrentValue.Version ?? throw new TermsOfServiceInternalServerErrorException();

        var isAccepted = await termsOfServiceRepository.HasAccepted(user.Id, currentToSVersion, trackChanges: false);

        // If the user has not accepted the current ToS version, throw an exception
        if (!isAccepted)
        {
            throw new TermsOfServicesNotAcceptedBadRequestException(user.Id, currentToSVersion);
        }

        // Proceed to the next middleware in the pipeline
        await _next(context);
    }
}
