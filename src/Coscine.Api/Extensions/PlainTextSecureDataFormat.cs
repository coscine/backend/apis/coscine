using Microsoft.AspNetCore.Authentication;
using Newtonsoft.Json;

namespace Coscine.Api.Extensions;

/// <summary>
/// Provides a plain text implementation of <see cref="ISecureDataFormat{TData}"/> 
/// for serializing and deserializing <see cref="AuthenticationProperties"/> objects.
/// This implementation uses JSON serialization to convert 
/// <see cref="AuthenticationProperties"/> to a string and back.
/// 
/// Note: This implementation is not secure for production use as it does not 
/// provide any encryption or signing of the data.
/// </summary>
public class PlainTextSecureDataFormat : ISecureDataFormat<AuthenticationProperties>
{
    /// <summary>
    /// Serializes the <see cref="AuthenticationProperties"/> object to a JSON string.
    /// </summary>
    /// <param name="data">The <see cref="AuthenticationProperties"/> to protect.</param>
    /// <returns>A JSON string representation of the <see cref="AuthenticationProperties"/>.</returns>
    /// <exception cref="ArgumentNullException">Thrown when the data is null.</exception>
    public string Protect(AuthenticationProperties data)
    {
        return data == null ? throw new ArgumentNullException(nameof(data)) : JsonConvert.SerializeObject(data);
    }

    /// <summary>
    /// This method is not implemented.
    /// </summary>
    /// <param name="data">The <see cref="AuthenticationProperties"/> to protect.</param>
    /// <param name="purpose">The purpose for which the data is being protected.</param>
    /// <returns>Throws <see cref="NotImplementedException"/>.</returns>
    public string Protect(AuthenticationProperties data, string? purpose)
    {
        throw new NotImplementedException();
    }

    /// <summary>
    /// Deserializes the JSON string back to an <see cref="AuthenticationProperties"/> object.
    /// </summary>
    /// <param name="protectedText">The JSON string to unprotect.</param>
    /// <returns>The deserialized <see cref="AuthenticationProperties"/>.</returns>
    public AuthenticationProperties Unprotect(string? protectedText)
    {
        if (string.IsNullOrEmpty(protectedText))
        {
            return new AuthenticationProperties();
        }

        return JsonConvert.DeserializeObject<AuthenticationProperties>(protectedText) ?? new AuthenticationProperties();
    }

    /// <summary>
    /// This method is not implemented.
    /// </summary>
    /// <param name="protectedText">The JSON string to unprotect.</param>
    /// <param name="purpose">The purpose for which the data was protected.</param>
    /// <returns>Throws <see cref="NotImplementedException"/>.</returns>
    public AuthenticationProperties? Unprotect(string? protectedText, string? purpose)
    {
        throw new NotImplementedException();
    }
}