﻿// Ignore Spelling: Deserialize Serializer

using System.Text.Json;

namespace Coscine.Api.Extensions;

/// <summary>Extensions for the JSON serializer.</summary>
public static class JsonSerializerExtensions
{
    private static readonly JsonSerializerOptions jsonSerializerOptions = new()
    {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        PropertyNameCaseInsensitive = true,
    };

    private static readonly JsonSerializerOptions jsonDeserializerOptions = new()
    {
        PropertyNamingPolicy = JsonNamingPolicy.CamelCase
    };

    /// <summary>Serializes the with camel case.</summary>
    /// <typeparam name="T">Any type of data.</typeparam>
    /// <param name="data">The data.</param>
    /// <returns>A JSON formatted string.</returns>
    public static string SerializeWithCamelCase<T>(this T data)
    {
        return JsonSerializer.Serialize(data, jsonDeserializerOptions);
    }

    /// <summary>Deserializes from camel case.</summary>
    /// <typeparam name="T">Any type of data.</typeparam>
    /// <param name="json">The JSON string.</param>
    /// <returns>The data or null, if it can't be deserialized.</returns>
    public static T? DeserializeFromCamelCase<T>(this string json)
    {
        try
        {
            return JsonSerializer.Deserialize<T>(json, jsonSerializerOptions);
        }
        catch (JsonException)
        {
            // Default, as T could not be nullable
            return default;
        }
    }
}