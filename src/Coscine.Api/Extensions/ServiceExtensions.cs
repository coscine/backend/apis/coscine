using Asp.Versioning;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Coscine.Api.Core.Service.Observers;
using Coscine.Api.ObjectResultExecutors;
using Coscine.Api.OperationFilters;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;
using NETCore.MailKit.Extensions;
using NETCore.MailKit.Infrastructure.Internal;
using Polly;
using Polly.Extensions.Http;
using Semiodesk.Trinity;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Reflection;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.RateLimiting;

namespace Coscine.Api.Extensions;

/// <summary>
/// Extensions for the IServiceCollection
/// </summary>
public static class ServiceExtensions
{
    /// <summary>
    /// Configures the cors for API access (any origin, method and header) as an extension Method.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The config</param>
    public static void ConfigureCors(this IServiceCollection services, IConfiguration configuration)
    {
        var connectionConfiguration = configuration.GetSection(ConnectionConfiguration.Section).Get<ConnectionConfiguration>()
            ?? throw new InvalidOperationException("ConnectionConfiguration has not been set.");

        var domain = new Uri(connectionConfiguration.ServiceUrl).Host;

        services.AddCors(options =>
            options.AddPolicy(name: "DefaultCorsPolicy", builder =>
            {
                builder.WithOrigins($"https://{domain}")
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();

                // Allow any subdomain under *.web.vulcanus.otc.coscine.dev for HTTP and HTTPS
                builder.SetIsOriginAllowed(origin =>
                    Uri.TryCreate(origin, UriKind.Absolute, out var uri) &&
                    uri.Host.EndsWith(".web.vulcanus.otc.coscine.dev", StringComparison.OrdinalIgnoreCase));
            })
        );
    }

    /// <summary>
    /// Configures the form options, to accept large files and values.
    /// </summary>
    /// <param name="services">The services collection.</param>
    public static void ConfigureFormOptions(this IServiceCollection services)
    {
        services.Configure<FormOptions>(x =>
        {
            x.ValueLengthLimit = int.MaxValue;
            x.MultipartBodyLengthLimit = int.MaxValue;
            x.MemoryBufferThreshold = int.MaxValue;
        });
    }

    /// <summary>
    /// Configures the repositories.
    /// </summary>
    /// <param name="services">The services collection.</param>
    public static void ConfigureRepositories(this IServiceCollection services)
    {
        services.AddScoped<IProjectRepository, ProjectRepository>();
        services.AddScoped<IResourceRepository, ResourceRepository>();
        services.AddScoped<IProjectRoleRepository, ProjectRoleRepository>();
        services.AddScoped<IApiTokenRepository, ApiTokenRepository>();
        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IExternalIdRepository, ExternalIdRepository>();
        services.AddScoped<IExternalAuthenticatorsRepository, ExternalAuthenticatorsRepository>();
        services.AddScoped<IPidRepository, PidRepository>();
        services.AddScoped<IDisciplineRepository, DisciplineRepository>();
        services.AddScoped<IMaintenanceRepository, MaintenanceRepository>();
        services.AddScoped<IMessageRepository, MessageRepository>();
        services.AddScoped<INocRepository, NocRepository>();
        services.AddScoped<IProjectDisciplineRepository, ProjectDisciplineRepository>();
        services.AddScoped<IResourceDisciplineRepository, ResourceDisciplineRepository>();
        services.AddScoped<IUserDisciplineRepository, UserDisciplineRepository>();
        services.AddScoped<IProjectInstituteRepository, ProjectInstituteRepository>();
        services.AddScoped<IDefaultProjectQuotaRepository, DefaultProjectQuotaRepository>();
        services.AddScoped<IResourceTypeRepository, ResourceTypeRepository>();
        services.AddScoped<IVisibilityRepository, VisibilityRepository>();
        services.AddScoped<IRoleRepository, RoleRepository>();
        services.AddScoped<ILicenseRepository, LicenseRepository>();
        services.AddScoped<IProjectInvitationRepository, ProjectInvitationRepository>();
        services.AddScoped<IProjectQuotaRepository, ProjectQuotaRepository>();
        services.AddScoped<IGitlabResourceTypeRepository, GitlabResourceTypeRepository>();
        services.AddScoped<ILinkedResourceTypeRepository, LinkedResourceTypeRepository>();
        services.AddScoped<IRdsResourceTypeRepository, RdsResourceTypeRepository>();
        services.AddScoped<IRdsS3ResourceTypeRepository, RdsS3ResourceTypeRepository>();
        services.AddScoped<IRdsS3WormResourceTypeRepository, RdsS3WormResourceTypeRepository>();
        services.AddScoped<IOrganizationRepository, OrganizationRepository>();
        services.AddScoped<IContactChangeRepository, ContactChangeRepository>();
        services.AddScoped<IEmailTemplateRepository, EmailTemplateRepository>();
        services.AddScoped<ITermsOfServiceRepository, TermsOfServiceRepository>();
        services.AddScoped<ITitleRepository, TitleRepository>();
        services.AddScoped<ILanguageRepository, LanguageRepository>();
        services.AddScoped<IApplicationProfileRepository, ApplicationProfileRepository>();
        services.AddScoped<IVocabularyRepository, VocabularyRepository>();
        services.AddScoped<ITreeRepository, TreeRepository>();
        services.AddScoped<ISearchRepository, SearchRepository>();
        services.AddScoped<IMetadataRepository, MetadataRepository>();
        services.AddScoped<IRepositoryContextLoader, RepositoryContextLoader>();
        services.AddScoped<IRdfRepositoryBase, RdfRepositoryBase>();
        services.AddScoped<IActivityLogRepository, ActivityLogRepository>();
        services.AddScoped<IProjectPublicationRequestRepository, ProjectPublicationRequestRepository>();
        services.AddScoped<IPublicationAdvisoryServiceRepository, PublicationAdvisoryServiceRepository>();
        services.AddScoped<IHandleRepository, HandleRepository>();
        services.AddScoped<IDataCatalogRepository, DataCatalogRepository>();
        services.AddScoped<IMetadataCatalogRepository, MetadataCatalogRepository>();
        services.AddScoped<IMetadataEntityRepository, MetadataEntityRepository>();
        services.AddScoped<IProjectRdfRepository, ProjectRdfRepository>();
        services.AddScoped<IRawDataCatalogRepository, RawDataCatalogRepository>();
        services.AddScoped<IRawDataEntityRepository, RawDataEntityRepository>();
        services.AddScoped<IResourceRdfRepository, ResourceRdfRepository>();
        services.AddScoped<IProvenanceRepository, ProvenanceRepository>();
        services.AddTransient<IS3ClientFactory, S3ClientFactory>();
        services.AddTransient<IEcsManagerFactory, EcsManagerFactory>();
        // Internal
        services.AddScoped<IDataStorageRepository, FileSystemStorageRepository>();
        // Linked
        services.AddScoped<IDataStorageRepository, LinkedDataStorageRepository>();
        // GitLab
        services.AddScoped<IDataStorageRepository, GitlabDataStorageRepository>();
        // RdsS3
        services.AddScoped<IDataStorageRepository, RdsS3RwthDataStorageRepository>();
        services.AddScoped<IDataStorageRepository, RdsS3NrwDataStorageRepository>();
        services.AddScoped<IDataStorageRepository, RdsS3UdeDataStorageRepository>();
        services.AddScoped<IDataStorageRepository, RdsS3TudoDataStorageRepository>();
        services.AddScoped<IDataStorageRepository, RdsS3RubDataStorageRepository>();
        // Rds
        services.AddScoped<IDataStorageRepository, RdsRwthDataStorageRepository>();
        services.AddScoped<IDataStorageRepository, RdsNrwDataStorageRepository>();
        services.AddScoped<IDataStorageRepository, RdsUdeDataStorageRepository>();
        services.AddScoped<IDataStorageRepository, RdsTudoDataStorageRepository>();
        services.AddScoped<IDataStorageRepository, RdsRubDataStorageRepository>();
        // Worm
        services.AddScoped<IDataStorageRepository, RdsS3WormRwthDataStorageRepository>();
    }

    /// <summary>
    /// Configures the services.
    /// </summary>
    /// <param name="services">The services collection.</param>
    public static void ConfigureServices(this IServiceCollection services)
    {
        services.AddScoped<IApplicationProfileService, ApplicationProfileService>();
        services.AddScoped<IActivityLogService, ActivityLogService>();
        services.AddScoped<IVocabularyService, VocabularyService>();
        services.AddScoped<IApiTokenService, ApiTokenService>();
        services.AddScoped<IProjectService, ProjectService>();
        services.AddScoped<IResourceService, ResourceService>();
        services.AddScoped<IAuthenticatorService, AuthenticatorService>();
        services.AddScoped<IDisciplineService, DisciplineService>();
        services.AddScoped<IMaintenanceService, MaintenanceService>();
        services.AddScoped<IMessageService, MessageService>();
        services.AddScoped<IMetadataService, MetadataService>();
        services.AddScoped<ILicenseService, LicenseService>();
        services.AddScoped<IPidService, PidService>();
        services.AddScoped<IRoleService, RoleService>();
        services.AddScoped<IVisibilityService, VisibilityService>();
        services.AddScoped<IProjectRoleService, ProjectRoleService>();
        services.AddScoped<IProjectInvitationService, ProjectInvitationService>();
        services.AddScoped<IQuotaService, QuotaService>();
        services.AddScoped<IResourceTypeService, ResourceTypeService>();
        services.AddScoped<IOrganizationService, OrganizationService>();
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<ITitleService, TitleService>();
        services.AddScoped<ILanguageService, LanguageService>();
        services.AddScoped<ITosService, TosService>();
        services.AddScoped<IEmailTemplateManager, EmailTemplateManager>();
        services.AddScoped<IBlobService, BlobService>();
        services.AddScoped<ITreeService, TreeService>();
        services.AddScoped<ISearchService, SearchService>();
        services.AddScoped<IObserverManager, ObserverManager>();
        services.AddScoped<IPublicationRequestService, PublicationRequestService>();
        services.AddScoped<IHandleService, HandleService>();
        services.AddScoped<IProvenanceService, ProvenanceService>();
        services.AddScoped<IResourceTypeGitlabService, ResourceTypeGitlabService>();

        services.AddTransient<IDataStorageRepositoryFactory, DataStorageRepositoryFactory>();
    }

    /// <summary>
    /// Configures the SQL context.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<RepositoryContext>(options =>
            options
                .UseLazyLoadingProxies()
                .UseSqlServer(
                    configuration.GetConnectionString("sqlConnection"),
                    builder =>
                    {
                        builder.MigrationsAssembly("Coscine.Api");
                        builder.EnableRetryOnFailure();
                    }
                )
        );
    }

    /// <summary>
    /// Configures the Trinity store.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void ConfigureTrinity(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped(_ =>
        {
            configuration.GetSection(SparqlConfiguration.Section);
            return StoreFactory.CreateStore(configuration.GetConnectionString("sparqlConnection"));
        });
    }

    /// <summary>
    /// Configures the versioning.
    /// </summary>
    /// <param name="services">The services collection.</param>
    public static void ConfigureVersioning(this IServiceCollection services)
    {
        services
            .AddApiVersioning(opt =>
            {
                opt.DefaultApiVersion = new ApiVersion(2, 0);
                opt.AssumeDefaultVersionWhenUnspecified = true;
                opt.ReportApiVersions = true;
                opt.ApiVersionReader = ApiVersionReader.Combine(new UrlSegmentApiVersionReader());
            })
            .AddApiExplorer(options =>
            {
                options.GroupNameFormat = "'v'VVV";
                options.SubstituteApiVersionInUrl = true;
            });
    }

    /// <summary>
    /// Adds the rate limiter configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void AddRateLimiterConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<RateLimiterConfiguration>()
            .Bind(configuration.GetSection(RateLimiterConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>
    /// Configures the activity logging options.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void ConfigureActivityLogging(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<ActivityLogConfiguration>()
            .Bind(configuration.GetSection(ActivityLogConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>
    /// Configures the rate limiting options.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void ConfigureRateLimitingOptions(this IServiceCollection services, IConfiguration configuration)
    {
        // We can add specific rate limiting policies for specific endpoints or disable them on endpoints
        services.AddRateLimiter(opt =>
        {
            opt.GlobalLimiter = PartitionedRateLimiter.Create<HttpContext, string>(httpContext =>
            {
                // Respects the forward headers of the Proxy
                IPAddress? remoteIpAddress = httpContext.Connection.RemoteIpAddress;

                // If we are the loop back, apply no limit
                if (remoteIpAddress is not null && IPAddress.IsLoopback(remoteIpAddress))
                {
                    return RateLimitPartition.GetNoLimiter(IPAddress.Loopback.ToString());
                }

                var userId = httpContext
                    .User.Claims.FirstOrDefault(x => string.Equals(x.Type, "userId", StringComparison.OrdinalIgnoreCase))
                    ?.Value ?? httpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;;
                var tokenId = httpContext
                    .User.Claims.FirstOrDefault(x => string.Equals(x.Type, "tokenId", StringComparison.OrdinalIgnoreCase))
                    ?.Value;

                // Only one can be set
                if (userId is not null && tokenId is not null)
                {
                    throw new UserAndTokenBadRequestException();
                }

                // Checks for the first non null value or sets it to GlobalLimiter
                var partitionKey = userId ?? tokenId ?? (remoteIpAddress != null ? remoteIpAddress.ToString() : "GlobalLimiter");

                var rateLimiterConfiguration =
                    configuration.GetSection(RateLimiterConfiguration.Section).Get<RateLimiterConfiguration>()
                    ?? throw new InvalidOperationException("RateLimiterConfiguration has not been set.");

                return RateLimitPartition.GetFixedWindowLimiter(
                    partitionKey: partitionKey,
                    factory: _ => new FixedWindowRateLimiterOptions
                    {
                        AutoReplenishment = rateLimiterConfiguration.AutoReplenishment,
                        PermitLimit = rateLimiterConfiguration.PermitLimit,
                        QueueLimit = rateLimiterConfiguration.QueueLimit,
                        QueueProcessingOrder = QueueProcessingOrder.OldestFirst,
                        Window = TimeSpan.FromMinutes(rateLimiterConfiguration.WindowInMinutes),
                    }
                );
            });

            opt.OnRejected = async (context, token) =>
            {
                context.HttpContext.Response.StatusCode = 429;
                if (context.Lease.TryGetMetadata(MetadataName.RetryAfter, out var retryAfter))
                {
                    context.HttpContext.Response.Headers.RetryAfter = ((int)retryAfter.TotalSeconds).ToString(
                        NumberFormatInfo.InvariantInfo
                    );
                    await context.HttpContext.Response.WriteAsync(
                        $"Too many requests. Please try again after {retryAfter.TotalSeconds} second(s).",
                        token
                    );
                }
                else
                {
                    await context.HttpContext.Response.WriteAsync("Too many requests. Please try again later.", token);
                }
            };
        });
    }

    static SymmetricSecurityKey ExtendKeyLengthIfNeeded(SymmetricSecurityKey key, int minLenInBytes)
    {
        ArgumentNullException.ThrowIfNull(key);

        if (key.KeySize < (minLenInBytes * 8))
        {
            var newKey = new byte[minLenInBytes]; // zeros by default
            key.Key.CopyTo(newKey, 0);
            return new SymmetricSecurityKey(newKey);
        }

        return key;
    }

    /// <summary>
    /// Configures the JWT.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void ConfigureAuthentication(this IServiceCollection services, IConfiguration configuration)
    {
        var jwtConfiguration =
            configuration.GetSection(JwtConfiguration.Section).Get<JwtConfiguration>()
            ?? throw new InvalidOperationException("JwtConfiguration has not been set.");

        var authenticationConfiguration = configuration.GetSection(AuthenticationConfiguration.Section).Get<AuthenticationConfiguration>()
            ?? throw new InvalidOperationException("AuthenticationConfiguration has not been set.");

        var connectionConfiguration = configuration.GetSection(ConnectionConfiguration.Section).Get<ConnectionConfiguration>()
            ?? throw new InvalidOperationException("ConnectionConfiguration has not been set.");

        var issuerSigningKeys = new List<SecurityKey>();

        foreach (var jsonWebKey in jwtConfiguration.JsonWebKeys)
        {
            if (string.IsNullOrEmpty(jsonWebKey.Alg))
            {
                throw new InvalidOperationException("Alg must be set in JsonWebKey.");
            }

            if (string.IsNullOrEmpty(jsonWebKey.K))
            {
                throw new InvalidOperationException("K must be set in JsonWebKey.");
            }

            if (string.IsNullOrEmpty(jsonWebKey.Kty))
            {
                throw new InvalidOperationException("Kty must be set in JsonWebKey.");
            }

            if (jsonWebKey.KeySize <= 0)
            {
                throw new InvalidOperationException("KeySize must be a positive value in JsonWebKey.");
            }

            issuerSigningKeys.Add(new SymmetricSecurityKey(Base64UrlEncoder.DecodeBytes(jsonWebKey.K)));
        }

        const string DEFAULT_SCHEME = "JWT_OR_COOKIE";

        services
            .AddAuthentication(opt =>
            {
                opt.DefaultAuthenticateScheme = DEFAULT_SCHEME;
                opt.DefaultChallengeScheme = DEFAULT_SCHEME;
            })
            .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
            {
                // Custom code to accept Bearer token from the query string
                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                            {
                                // Check if the endpoint path matches a specific endpoint
                                var requestPath = context.HttpContext.Request.Path;

                                if (requestPath.ToString().StartsWith("/api/v2/self/session/merge/"))
                                {
                                    var token = context.Request.Query["access_token"];
                                    if (!string.IsNullOrEmpty(token))
                                    {
                                        context.Token = token;
                                    }

                                }

                                return Task.CompletedTask;
                            }
                };

                options.TokenValidationParameters = new TokenValidationParameters
                {

                    IssuerSigningKeyResolver = (tokenString, securityToken, identifier, parameters) =>
                    {
                        string? alg = null;

                        if (securityToken is JwtSecurityToken jwtSecurityToken)
                        {
                            alg = jwtSecurityToken.SignatureAlgorithm;
                        }

                        if (securityToken is Microsoft.IdentityModel.JsonWebTokens.JsonWebToken jsonWebToken)
                        {
                            alg = jsonWebToken.Alg;
                        }

                        var keys = new List<SecurityKey>();

                        foreach (var key in parameters.IssuerSigningKeys)
                        {
                            if (key is SymmetricSecurityKey symIssKey && alg != null)
                            {
                                // workaround for breaking change in "System.IdentityModel.Tokens.Jwt 6.30.1+ 
                                switch (alg?.ToLowerInvariant())
                                {
                                    case "hs256":
                                        keys.Add(ExtendKeyLengthIfNeeded(symIssKey, 32));
                                        break;
                                    case "hs512":
                                        keys.Add(ExtendKeyLengthIfNeeded(symIssKey, 64));
                                        break;
                                }
                            }
                        }

                        return keys;
                    },
                    ValidateIssuer = jwtConfiguration.ValidateIssuer,
                    ValidateAudience = jwtConfiguration.ValidateAudience,
                    ValidateLifetime = jwtConfiguration.ValidateLifetime,
                    ValidateIssuerSigningKey = jwtConfiguration.ValidateIssuerSigningKey,
                    ValidIssuers = jwtConfiguration.ValidIssuers,
                    ValidAudiences = jwtConfiguration.ValidAudiences,
                    IssuerSigningKeys = issuerSigningKeys,
                    TryAllIssuerSigningKeys = true,
                };
            })
            .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
            {
                options.Cookie.Name = "CoscineSession";
                options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                options.Cookie.SameSite = SameSiteMode.Strict; // Allow cross-site cookies
                options.Cookie.HttpOnly = true; // So that it can't be read by ts
                options.Cookie.Path = "/"; // No subpath, defaults to /coscine

                // This makes the cookie persist for 1 day even if the browser is closed
                options.Cookie.MaxAge = TimeSpan.FromHours(24); // Set the client-side lifetime of the cookie

                // This determines how long the session is valid on the server
                options.ExpireTimeSpan = TimeSpan.FromHours(24); // Set server-side authentication expiration

                options.SlidingExpiration = true; // Extend the expiration time on each request
                options.Cookie.IsEssential = true; // Cookie is required for core functionality (login session), making it exempt from consent banners under GDPR

                options.Events = new CookieAuthenticationEvents
                {
                    // On redirect to login, return 401 instead of redirecting to login page (e.g. when data protection key changed)
                    OnRedirectToLogin = context =>
                    {
                        context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                        return Task.CompletedTask;
                    },
                    // When the cookie is validated, we want to store the expiration date in a separate cookie
                    OnValidatePrincipal = context =>
                    {
                        var expiresUtc = context.Properties.ExpiresUtc;
                        context.HttpContext.Response.Cookies.Append("CoscineLoginExpiration", expiresUtc?.UtcDateTime.ToString("o") ?? string.Empty, new CookieOptions()
                        {
                            Expires = expiresUtc,
                        });
                        return Task.CompletedTask;
                    },
                    // When the user signs in, we want to store the expiration date in a separate cookie
                    OnSignedIn = context =>
                    {
                        var expiresUtc = context.Properties.ExpiresUtc;
                        context.HttpContext.Response.Cookies.Append("CoscineLoginExpiration", expiresUtc?.UtcDateTime.ToString("o") ?? string.Empty, new CookieOptions()
                        {
                            Expires = expiresUtc,
                        });
                        return Task.CompletedTask;
                    }
                };
            })
            .AddPolicyScheme(DEFAULT_SCHEME, DEFAULT_SCHEME, options =>
            {
                // runs on each request
                options.ForwardDefaultSelector = context =>
                {
                    // filter by auth type
                    var authorization = context.Request.Headers.Authorization.ToString();
                    if (!string.IsNullOrEmpty(authorization) && authorization.StartsWith("Bearer ") || context.Request.Query.ContainsKey("access_token"))
                    {
                        return "Bearer";
                    }

                    // Check if the session cookie is set
                    if (context.Request.Cookies.ContainsKey("CoscineSession"))
                    {
                        // Return "Cookies" if session cookie is present
                        return "Cookies";
                    }

                    // Check if we come from the signin
                    if (context.Request.Path.ToString().Contains("/signin-oidc-", StringComparison.OrdinalIgnoreCase))
                    {
                        // Return "Cookies" if session cookie is present
                        return "Cookies";
                    }

                    // To keep current behaviour
                    return "Bearer";
                };
            }
        );

        foreach (var provider in authenticationConfiguration.OpenIdConnectProviders)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = DEFAULT_SCHEME;
                options.DefaultChallengeScheme = DEFAULT_SCHEME;
            })
            .AddOpenIdConnect(provider.Name, options =>
            {
                options.SignInScheme = DEFAULT_SCHEME;

                options.Authority = provider.Authority;
                options.ClientId = provider.ClientId;
                options.ClientSecret = provider.ClientSecret;
                options.ResponseType = OpenIdConnectResponseType.Code;
                options.ResponseMode = OpenIdConnectResponseMode.Query;

                // Traefik has a redirect rule using RegEx.
                // For this, it searches the URL for the state, as we are only allowed to modify this value for handling this case.
                // However, MS encrypts/encodes the state in default and this can't be read by Treafik.
                // As such, I wrote a second class, that just does not encode/encrypt.
                // Now the URL again contains the value in a get parameter and can be read.
                // We only need this trick in dev, because we have only registered one address in NFDI4Ing AAI and can't register all dev machines there.
                if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development"
                    && string.Equals(provider.ExternalId, ExternalAuthenticatorConfiguration.NFDI4Ing_AAI.Id.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    options.StateDataFormat = new PlainTextSecureDataFormat();
                }

                options.GetClaimsFromUserInfoEndpoint = true;
                options.CallbackPath = new PathString($"/signin-oidc-{provider.Name}");
                options.Scope.Add("openid");
                options.Scope.Add("profile");
                options.Scope.Add("email");

                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = true,
                    ValidateIssuer = true,
                    ValidIssuer = options.Authority,
                    ValidAudience = options.ClientId
                };

                options.Events = new OpenIdConnectEvents
                {
                    OnUserInformationReceived = async context =>
                    {
                        // Add the provider Id to the claim
                        if (context.Properties?.Items.TryGetValue("externalAuthenticatorId", out var externalAuthenticatorId) == true)
                        {
                            if (!string.IsNullOrEmpty(externalAuthenticatorId))
                            {
                                var userService = context.HttpContext.RequestServices.GetRequiredService<IUserService>();

                                // Make sure, that the it is a GUID
                                if (!Guid.TryParse(externalAuthenticatorId, out var externalAuthenticatorGuid))
                                {
                                    throw new ExternalAuthenticatorIdNotAGuidBadRequestException(externalAuthenticatorId);
                                }

                                if (context.Principal is not null)
                                {
                                    // Currently not all values are set
                                    var oidcUser = JsonSerializer.Deserialize<OIDCUser>(context.User.RootElement);

                                    if (oidcUser is not null)
                                    {
                                        var identity = context.Principal.Identity as ClaimsIdentity;

                                        identity?.AddClaim(new Claim("external_authenticator_id", externalAuthenticatorId));

                                        identity?.AddClaimIfNotExists(ClaimTypes.GivenName, oidcUser.GivenName ?? "");
                                        identity?.AddClaimIfNotExists(ClaimTypes.Name, oidcUser.Name ?? "");
                                        identity?.AddClaimIfNotExists(ClaimTypes.Surname, oidcUser.FamilyName ?? "");

                                        foreach (var affiliation in oidcUser.VopersonExternalAffiliation)
                                        {
                                            identity?.AddClaim(new Claim(ClaimType.Affiliation, affiliation));
                                        }

                                        // Create or update the coscine user
                                        var userId = await userService.EnsureInternalUserAsync(context.Principal, externalAuthenticatorGuid, trackChanges: true);

                                        // Check if this login workflow contains a merge target
                                        // If so, initiate a merge of the previous user (merge target) and the current user (principal)
                                        if (context.Properties?.Items.TryGetValue("mergeTarget", out var mergeTarget) == true && userId is not null)
                                        {
                                            if (!string.IsNullOrEmpty(mergeTarget) && context.Principal is not null)
                                            {
                                                var mergeTargetId = Guid.Parse(mergeTarget);

                                                await userService.MergeUser(userId.Value, mergeTargetId);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    OnRedirectToIdentityProvider = context =>
                    {
                        context.ProtocolMessage.State = authenticationConfiguration.LoginCallbackPrefix;
                        context.ProtocolMessage.RedirectUri = $"{authenticationConfiguration.LoginCallbackUri.TrimEnd('/')}/signin-oidc-{provider.Name}";

                        return Task.CompletedTask;
                    },
                    OnRemoteFailure = context =>
                    {
                        context.HandleResponse();

                        // Redirect the user to the custom error page
                        var baseUri = new Uri(new Uri(connectionConfiguration.ServiceUrl), "./error").AbsoluteUri;
                        context.Response.Redirect(QueryHelpers.AddQueryString(baseUri, "message", context.Failure?.Message ?? ""));
                        return Task.CompletedTask;
                    }
                };
            });
        }
    }

    /// <summary>
    /// Adds the Sparql configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void AddSparqlConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<SparqlConfiguration>()
            .Bind(configuration.GetSection(SparqlConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>
    /// Adds the logger configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void AddLoggerConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<LoggerConfiguration>()
            .Bind(configuration.GetSection(LoggerConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();

        // Register the LoggerConfigurationOptionsMonitorService as a hosted service
        services.AddHostedService<LoggerConfigurationOptionsMonitorService>();
    }

    /// <summary>
    /// Adds the JWT configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void AddJwtConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<JwtConfiguration>()
            .Bind(configuration.GetSection(JwtConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>
    /// Adds the Authentication configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void AddAuthenticationConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<AuthenticationConfiguration>()
            .Bind(configuration.GetSection(AuthenticationConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>Adds the response wrapper.</summary>
    /// <param name="services">The services collection.</param>
    public static void AddResponseWrapper(this IServiceCollection services)
    {
        services.AddSingleton<IActionResultExecutor<ObjectResult>, ResponseEnvelopeResultExecutor>();
    }

    /// <summary>
    /// Configures the swagger page.
    /// </summary>
    /// <param name="services">The services collection.</param>
    public static void ConfigureSwagger(this IServiceCollection services)
    {
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen(options =>
        {
            // Add the custom operation filter
            options.OperationFilter<ProjectIdOperationFilter>();
            options.OperationFilter<RoleDescriptorOperationFilter>();
            options.OperationFilter<FileUploadOperationFilter>();

            // Enable support for Swagger annotations
            options.EnableAnnotations();

            // Enable support for nullables
            options.SupportNonNullableReferenceTypes();

            options.IncludeXmlComments(
                Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"),
                true
            );
            options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Coscine.Api.Core.Shared.xml"), true);
        });
        services.ConfigureOptions<ConfigureSwaggerOptions>();
    }

    /// <summary>
    /// Adds the mail kit configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void AddMailKitConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<MailKitConfiguration>()
            .Bind(configuration.GetSection(MailKitConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>
    /// Configures the mail service.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void ConfigureMailService(this IServiceCollection services, IConfiguration configuration)
    {
        var mailKitConfiguration =
            configuration.GetSection(MailKitConfiguration.Section).Get<MailKitConfiguration>()
            ?? throw new InvalidOperationException("MailKitConfiguration has not been set.");

        services.AddMailKit(optionBuilder =>
        {
            optionBuilder.UseMailKit(
                new MailKitOptions()
                {
                    //get options from sercets.json
                    Server = mailKitConfiguration.Server,
                    Port = mailKitConfiguration.Port,
                    SenderName = mailKitConfiguration.SenderName,
                    SenderEmail = mailKitConfiguration.SenderEmail,

                    // can be optional with no authentication
                    Account = mailKitConfiguration.Account,
                    Password = mailKitConfiguration.Password,

                    // enable SSL or TLS
                    Security = mailKitConfiguration.Security
                }
            );
        });
    }

    /// <summary>
    /// Adds the ElasticSearch configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void AddElasticSearchConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<ElasticSearchConfiguration>()
            .Bind(configuration.GetSection(ElasticSearchConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>
    /// Adds the resource type configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void AddResourceTypeConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        // Load linked configurations
        services
            .AddOptions<LinkedResourceTypeConfiguration>("linked")
            .Bind(configuration.GetSection("ResourceTypes:linked:linked"))
            .ValidateMiniValidation()
            .ValidateOnStart();

        // Load GitLab configurations
        services
            .AddOptions<GitLabResourceTypeConfiguration>("gitlab")
            .Bind(configuration.GetSection("ResourceTypes:gitlab:gitlab"))
            .ValidateMiniValidation()
            .ValidateOnStart();

        // Load rds configurations
        services
            .AddOptions<RdsResourceTypeConfiguration>("rdsrwth")
            .Bind(configuration.GetSection("ResourceTypes:rds:rwth"))
            .ValidateMiniValidation()
            .ValidateOnStart();

        services
            .AddOptions<RdsResourceTypeConfiguration>("rdsnrw")
            .Bind(configuration.GetSection("ResourceTypes:rds:nrw"))
            .ValidateMiniValidation()
            .ValidateOnStart();

        services
            .AddOptions<RdsResourceTypeConfiguration>("rdsude")
            .Bind(configuration.GetSection("ResourceTypes:rds:ude"))
            .ValidateMiniValidation()
            .ValidateOnStart();

        services
            .AddOptions<RdsResourceTypeConfiguration>("rdstudo")
            .Bind(configuration.GetSection("ResourceTypes:rds:tudo"))
            .ValidateMiniValidation()
            .ValidateOnStart();

        services
            .AddOptions<RdsResourceTypeConfiguration>("rdsrub")
            .Bind(configuration.GetSection("ResourceTypes:rds:rub"))
            .ValidateMiniValidation()
            .ValidateOnStart();

        // Load rds S3 configurations
        services
            .AddOptions<RdsS3ResourceTypeConfiguration>("rdss3rwth")
            .Bind(configuration.GetSection("ResourceTypes:rdss3:rwth"))
            .ValidateMiniValidation()
            .ValidateOnStart();

        services
            .AddOptions<RdsS3ResourceTypeConfiguration>("rdss3nrw")
            .Bind(configuration.GetSection("ResourceTypes:rdss3:nrw"))
            .ValidateMiniValidation()
            .ValidateOnStart();

        services
            .AddOptions<RdsS3ResourceTypeConfiguration>("rdss3ude")
            .Bind(configuration.GetSection("ResourceTypes:rdss3:ude"))
            .ValidateMiniValidation()
            .ValidateOnStart();

        services
            .AddOptions<RdsS3ResourceTypeConfiguration>("rdss3tudo")
            .Bind(configuration.GetSection("ResourceTypes:rdss3:tudo"))
            .ValidateMiniValidation()
            .ValidateOnStart();

        services
            .AddOptions<RdsS3ResourceTypeConfiguration>("rdss3rub")
            .Bind(configuration.GetSection("ResourceTypes:rdss3:rub"))
            .ValidateMiniValidation()
            .ValidateOnStart();

        // Load rds S3 WORM configurations
        services
            .AddOptions<RdsS3WormResourceTypeConfiguration>("rdss3wormrwth")
            .Bind(configuration.GetSection("ResourceTypes:rdss3worm:rwth"))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>
    /// Adds the message configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void AddMessageConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<MessageConfiguration>()
            .Bind(configuration.GetSection(MessageConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>
    /// Adds the NOC configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void AddNocConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<NocConfiguration>()
            .Bind(configuration.GetSection(NocConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>
    /// Adds the maintenance configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    [Obsolete($"This method is deprecated and will be removed in the future. Use {nameof(AddNocConfiguration)} instead.")]
    public static void AddMaintenanceConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<MaintenanceConfiguration>()
            .Bind(configuration.GetSection(MaintenanceConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>
    /// Adds the pid configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void AddPidConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<PidConfiguration>()
            .Bind(configuration.GetSection(PidConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>
    /// Adds the terms of service configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    public static void AddTermsOfServiceConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<TermsOfServiceConfiguration>()
            .Bind(configuration.GetSection(TermsOfServiceConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>
    /// Adds the connection configuration.
    /// </summary>
    /// <param name="services">The services collection.</param>
    /// <param name="configuration">The configuration.</param>
    /// <exception cref="System.NullReferenceException"></exception>
    public static void AddConnectionConfiguration(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddOptions<ConnectionConfiguration>()
            .Bind(configuration.GetSection(ConnectionConfiguration.Section))
            .ValidateMiniValidation()
            .ValidateOnStart();
    }

    /// <summary>Adds the policies.</summary>
    /// <param name="services">The services.</param>
    /// <param name="configuration">The configuration.</param>
    public static IServiceCollection AddPolicies(this IServiceCollection services, IConfiguration configuration)
    {
        services.Configure<PolicyOptions>(configuration);
        var policyOptions = configuration.GetSection("Policies").Get<PolicyOptions>()
            ?? throw new InvalidOperationException("Policies configuration has not been set.");

        var policyRegistry = services.AddPolicyRegistry();

        policyRegistry.Add(
            PolicyName.HttpRetry,
            HttpPolicyExtensions
                .HandleTransientHttpError()
                .WaitAndRetryAsync(
                    policyOptions.HttpRetry.Count,
                    retryAttempt => TimeSpan.FromSeconds(Math.Pow(policyOptions.HttpRetry.BackoffPower, retryAttempt))));

        policyRegistry.Add(
            PolicyName.HttpCircuitBreaker,
            HttpPolicyExtensions
                .HandleTransientHttpError()
                .CircuitBreakerAsync(
                    handledEventsAllowedBeforeBreaking: policyOptions.HttpCircuitBreaker.ExceptionsAllowedBeforeBreaking,
                    durationOfBreak: policyOptions.HttpCircuitBreaker.DurationOfBreak));

        return services;
    }

    public static IServiceCollection AddCustomHttpClient(this IServiceCollection services, IConfiguration configuration, string configurationSectionName)
    {
        return services
            .Configure<HttpClientOptions>(configuration.GetSection(configurationSectionName))
            .AddHttpClient(configurationSectionName)
            .ConfigureHttpClient(
                (serviceProvider, httpClient) =>
                {

                    var httpClientOptions = configuration.GetSection(configurationSectionName).Get<HttpClientOptions>()
                        ?? throw new InvalidOperationException($"{configurationSectionName} configuration has not been set.");

                    httpClient.BaseAddress = httpClientOptions.BaseAddress;
                    httpClient.Timeout = httpClientOptions.Timeout;
                })
            .ConfigurePrimaryHttpMessageHandler(x => new DefaultHttpClientHandler(configuration.GetSection(configurationSectionName).Get<HttpClientOptions>()
                        ?? throw new InvalidOperationException($"{configurationSectionName} configuration has not been set.")))
            .AddPolicyHandlerFromRegistry(PolicyName.HttpRetry)
            .AddPolicyHandlerFromRegistry(PolicyName.HttpCircuitBreaker)
            .Services;
    }


    private static void AddClaimIfNotExists(this ClaimsIdentity identity, string claimType, string claimValue)
    {
        // Check if the claim already exists
        var existingClaim = identity.FindFirst(claimType);

        // Add the claim if it does not exist
        if (existingClaim == null)
        {
            identity.AddClaim(new Claim(claimType, claimValue));
        }
    }
}