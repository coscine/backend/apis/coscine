﻿using Asp.Versioning.ApiExplorer;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Coscine.Api.Extensions;

/// <summary>
/// Configures Swagger generation options to include API versioning.
/// </summary>
public class ConfigureSwaggerOptions : IConfigureNamedOptions<SwaggerGenOptions>
{
    private readonly IApiVersionDescriptionProvider _provider;

    /// <summary>
    /// Initializes a new instance of the <see cref="ConfigureSwaggerOptions"/> class.
    /// </summary>
    /// <param name="provider">The API version description provider used to configure Swagger options based on API versions.</param>
    public ConfigureSwaggerOptions(IApiVersionDescriptionProvider provider)
    {
        _provider = provider;
    }

    /// <summary>
    /// Configures the Swagger documentation for each discovered API version, setting up public and internal documents.
    /// </summary>
    /// <param name="options">The Swagger generation options to configure.</param>
    public void Configure(SwaggerGenOptions options)
    {
        // add swagger document for every API version discovered
        foreach (var description in _provider.ApiVersionDescriptions)
        {
            options.SwaggerDoc(
                description.GroupName,
                CreateVersionInfo(description));
        }

        options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
        {
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey,
            Scheme = "Bearer",
            BearerFormat = "JWT",
            In = ParameterLocation.Header,
            Description = "JWT Authorization header using the Bearer scheme."
        });

        options.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                        Array.Empty<string>()
                }
            });
    }

    /// <summary>
    /// Configures Swagger options for a named option instance. This overload is required by the <c>IConfigureNamedOptions</c> interface.
    /// </summary>
    /// <param name="name">The name of the Swagger document to configure, or <c>null</c> to configure the default.</param>
    /// <param name="options">The Swagger generation options to configure.</param>
    public void Configure(string? name, SwaggerGenOptions options)
    {
        Configure(options);
    }

    /// <summary>
    /// Creates version information for the Swagger document based on the provided API version description.
    /// </summary>
    /// <param name="description">The API version description to create document information for.</param>
    /// <returns>An <see cref="OpenApiInfo"/> object containing API version information.</returns>
    private static OpenApiInfo CreateVersionInfo(ApiVersionDescription description)
    {
        var info = new OpenApiInfo()
        {
            Title = "Coscine Web API",
            Version = description.ApiVersion.ToString(),
            Description = "Coscine (short for <b>CO</b>llaborative <b>SC</b>ientific <b>IN</b>tegration <b>E</b>nvironment) is the research data management platform for your research project.",
            TermsOfService = new Uri("https://git.rwth-aachen.de/coscine/docs/public/terms/-/blob/master/TermsOfUse.md"),
            Contact = new OpenApiContact { Name = "Coscine Team", Email = "servicedesk@itc.rwth-aachen.de" },
            //License = new OpenApiLicense { Name = "Coscine License", Url = new Uri("https://example.com/license"), }
        };

        if (description.IsDeprecated)
        {
            info.Description += " This API version has been deprecated. Please use one of the new APIs available from the explorer.";
        }

        return info;
    }
}