﻿using Microsoft.Extensions.Options;

namespace Coscine.Api.Extensions;

/// <summary>
/// Provides extension methods for enabling MiniValidation for options.
/// </summary>
public static class MiniValidationExtensions
{
    /// <summary>
    /// Adds MiniValidation support for the specified options.
    /// </summary>
    /// <typeparam name="TOptions">The type of options to validate.</typeparam>
    /// <param name="optionsBuilder">The <see cref="OptionsBuilder{TOptions}"/> to which MiniValidation will be added.</param>
    /// <returns>The same <see cref="OptionsBuilder{TOptions}"/> instance for chaining.</returns>
    /// <remarks>
    /// MiniValidation performs simple validation for the specified options type.
    /// </remarks>
    public static OptionsBuilder<TOptions> ValidateMiniValidation<TOptions>(this OptionsBuilder<TOptions> optionsBuilder)
        where TOptions : class
    {
        // Register the validator against the options
        optionsBuilder.Services.AddSingleton<IValidateOptions<TOptions>>(
            new MiniValidationValidateOptions<TOptions>(optionsBuilder.Name)
        );

        return optionsBuilder;
    }
}
