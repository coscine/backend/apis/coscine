﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Options;

namespace Coscine.Api.Extensions;

/// <summary>
/// Middleware that logs user activities and updates their last activity timestamp.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="ActivityLoggingMiddleware" /> class.
/// </remarks>
/// <param name="next">The delegate representing the remaining middleware in the request pipeline.</param>
public class ActivityLoggingMiddleware(RequestDelegate next)
{
    private readonly RequestDelegate _next = next;

    /// <summary>
    /// Invokes the specified context.
    /// </summary>
    /// <param name="context">The <see cref="HttpContext"/> for the current request.</param>
    /// <param name="logger">The </param>
    /// <param name="activityLogRepository">The activity log repository.</param>
    /// <param name="repositoryContextLoader">The repository context loader.</param>
    /// <param name="authenticatorService">The authenticator service.</param>
    /// <param name="activityLogConfiguration">The activity log configuration.</param>
    /// <returns>The <see cref="Task"/> that represents the completion of request processing.</returns>
    public async Task Invoke(
        HttpContext context,
        ILogger<ActivityLoggingMiddleware> logger,
        IActivityLogRepository activityLogRepository,
        IRepositoryContextLoader repositoryContextLoader,
        IAuthenticatorService authenticatorService,
        IOptionsMonitor<ActivityLogConfiguration> activityLogConfiguration
    )
    {
        // Call the next delegate/middleware in the pipeline to skip the "Request" phase
        await _next(context);

        // If activity logging is enabled, log the activity and update the user's last activity timestamp
        if (activityLogConfiguration.CurrentValue.IsEnabled
            && context.User.Identity?.IsAuthenticated == true
            && !context.User.IsInRole(ApiRoles.Administrator))
        {
            // Log the activity
            var user = await authenticatorService.GetUserAsync(context.User, trackChanges: true);

            // Can only log a existing user, but should never be null here
            if (user is not null)
            {
                var controllerDescriptor = context.GetEndpoint()?.Metadata.GetMetadata<ControllerActionDescriptor>();
                activityLogRepository.Create(
                    new()
                    {
                        Id = Guid.NewGuid(),
                        ApiPath = context.Request.Path,
                        HttpAction = context.Request.Method,
                        ControllerName = controllerDescriptor?.ControllerName,
                        ActionName = controllerDescriptor?.ActionName,
                        UserId = user.Id,
                        ActivityTimestamp = DateTime.UtcNow,
                    }
                );

                // Clean up old activity logs
                var toDelteAfter = DateTime.UtcNow.AddDays(-activityLogConfiguration.CurrentValue.RetentionDays);
                activityLogRepository.DeleteAllBeforeAsync(toDelteAfter);

                // Update user's last activity in the database
                user.LatestActivity = DateTime.UtcNow;

                // Save changes to the database here instead of in the methods above for better performance
                await repositoryContextLoader.SaveAsync();
            }
        }
    }
}
