﻿using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using NLog;

namespace Coscine.Api.Extensions;

/// <summary>
/// Middleware for adding JWT claims (specifically user and token IDs) to the NLog context.
/// </summary>
public class NLogClaimsMiddleware
{
    private readonly RequestDelegate _next;

    /// <summary>
    /// Initializes a new instance of the <see cref="NLogClaimsMiddleware" /> class.
    /// </summary>
    /// <param name="next">The delegate representing the remaining middleware in the request pipeline.</param>
    public NLogClaimsMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    /// <summary>
    /// Invokes the middleware with the provided context.
    /// This method checks for specific claims ("userId" or "tokenId") in the user's context
    /// and associates the context with the corresponding property in the logging scope before calling the next middleware.
    /// </summary>
    /// <param name="context">The HttpContext for the current request.</param>
    /// <returns>The <see cref="Task"/> that represents the completion of request processing.</returns>
    public async Task Invoke(HttpContext context)
    {
        // Determines the property key based on the presence of "userId" or "tokenId" in the claims.
        var propertyKey = context.User.FindFirst("userId") is not null ? "userId" : context.User.FindFirst("tokenId") is not null ? "tokenId" : null;

        if (propertyKey != null)
        {
            // Associates the context with the identified property in the logging scope.
            // An exception will leave the scope and you will loose the information!
            // As such it has to be reset by the exception handler.
            using (ScopeContext.PushProperty(propertyKey, context.User.FindFirstValue(propertyKey)))
            {
                // Calls the next middleware in the pipeline.
                await _next(context);
            }
        }
        else
        {
            // If neither "userId" nor "tokenId" is found, proceeds without modifying the logging scope.
            await _next(context);
        }
    }
}