using Microsoft.AspNetCore.Mvc;

namespace Coscine.Api.ErrorHandlers;

/// <summary>
/// Interface for handling errors in a custom manner.
/// </summary>
public interface ICoscineErrorHandler
{
    /// <summary>
    /// Handles the error by performing a custom action.
    /// </summary>
    /// <param name="context">The HTTP context.</param>
    /// <param name="problemDetails">Details of the problem encountered.</param>
    /// <returns>A task that represents the asynchronous operation.</returns>
    Task HandleError(HttpContext context, ProblemDetails problemDetails);
}