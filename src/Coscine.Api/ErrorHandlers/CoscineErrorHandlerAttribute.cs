using Microsoft.AspNetCore.Mvc;

namespace Coscine.Api.ErrorHandlers;

/// <summary>
/// Attribute for handling errors in a specific manner defined by a type implementing ICoscineErrorHandler.
/// </summary>
/// <typeparam name="T">Type that implements ICoscineErrorHandler.</typeparam>
public class CoscineErrorHandlerAttribute<T> : CoscineErrorHandlerAttribute where T : ICoscineErrorHandler
{
    /// <summary>
    /// Initializes a new instance of the <see cref="CoscineErrorHandlerAttribute{T}"/> class.
    /// </summary>
    public CoscineErrorHandlerAttribute() : base(typeof(T))
    {
    }
}

/// <summary>
/// Attribute for handling errors using a specified type that implements ICoscineErrorHandler.
/// </summary>
[AttributeUsage(AttributeTargets.Class)]
public class CoscineErrorHandlerAttribute : Attribute
{
    private Type Type { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="CoscineErrorHandlerAttribute"/> class.
    /// </summary>
    /// <param name="type">The type that implements ICoscineErrorHandler.</param>
    /// <exception cref="ArgumentNullException">Thrown when the type is null.</exception>
    /// <exception cref="ArgumentException">Thrown when the type does not implement ICoscineErrorHandler.</exception>
    public CoscineErrorHandlerAttribute(Type type)
    {
        Type = type ?? throw new ArgumentNullException(nameof(type));

        if (!type.GetInterfaces().Contains(typeof(ICoscineErrorHandler)))
        {
            throw new ArgumentException($"Type given in {nameof(type)} should implement {nameof(ICoscineErrorHandler)}.");
        }
    }

    /// <summary>
    /// Handles the error by invoking the appropriate error handler.
    /// </summary>
    /// <param name="context">The HTTP context.</param>
    /// <param name="problemDetails">Details of the problem encountered.</param>
    /// <returns>A task that represents the asynchronous operation.</returns>
    public async Task HandleError(HttpContext context, ProblemDetails problemDetails)
    {
        var factory = ActivatorUtilities.CreateFactory(Type, Type.EmptyTypes);
        var handler = factory(context.RequestServices, []);
        if (handler is ICoscineErrorHandler coscineHandler)
        {
            await coscineHandler.HandleError(context, problemDetails);
        }
    }
}