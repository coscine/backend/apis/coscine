using Coscine.Api.Core.Entities.ConfigurationModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Options;

namespace Coscine.Api.ErrorHandlers;

/// <summary>
/// Error handler that redirects to a specified URL on error.
/// </summary>
/// <remarks>
/// Initializes a new instance of the <see cref="HttpRedirectErrorHandler"/> class.
/// </remarks>
/// <param name="connectionConfiguration">The connection configuration.</param>
public class HttpRedirectErrorHandler(IOptionsMonitor<ConnectionConfiguration> connectionConfiguration) : ICoscineErrorHandler
{
    ConnectionConfiguration _connectionConfiguration { get; set; } = connectionConfiguration.CurrentValue;


    /// <summary>
    /// Handles the error by redirecting to a specified URL with error details.
    /// </summary>
    /// <param name="context">The HTTP context.</param>
    /// <param name="problemDetails">Details of the problem encountered.</param>
    /// <returns>A task that represents the asynchronous operation.</returns>
    public Task HandleError(HttpContext context, ProblemDetails problemDetails)
    {
        var errorDetails = new Dictionary<string, string?>()
        {
            { "error_type", problemDetails.Type },
            { "error_title", problemDetails.Title },
            { "error_detail", problemDetails.Detail },
            { "status_code", problemDetails.Status.ToString() },
            { "trace_id", context.TraceIdentifier },
        }.Where(kvp => kvp.Value != null);

        // Set redirect
        context.Response.StatusCode = StatusCodes.Status302Found;
        var baseUri = new Uri(new Uri(_connectionConfiguration.ServiceUrl), "./error").AbsoluteUri;
        context.Response.Redirect(QueryHelpers.AddQueryString(baseUri, errorDetails));

        return Task.CompletedTask;
    }
}
