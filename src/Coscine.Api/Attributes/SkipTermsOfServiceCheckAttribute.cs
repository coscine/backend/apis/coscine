﻿namespace Coscine.Api.Attributes;

/// <summary>
/// Used to skip the middleware ToS check.
/// </summary>
[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
public class SkipTermsOfServiceCheckAttribute : Attribute;
