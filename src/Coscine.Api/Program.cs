using Asp.Versioning.ApiExplorer;
using Coscine.Api.ActionFilters;
using Coscine.Api.Authorization.AuthHandlers;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Service;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.Enums.Utility;
using Coscine.Api.Extensions;
using Coscine.Api.OutputFormatters;
using Coscine.Api.Health;
using Coscine.Api.Policies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.OpenApi.Models;
using NLog;
using NLog.Web;
using OpenTelemetry.Metrics;
using OpenTelemetry.Trace;
using Semiodesk.Trinity;
using Semiodesk.Trinity.Store.Virtuoso;
using System.Text.Json;
using System.Text.Json.Serialization;
using Winton.Extensions.Configuration.Consul;
using Microsoft.AspNetCore.DataProtection;
using Coscine.Api.Core.Entities.OtherModels;

var builder = WebApplication.CreateBuilder(args);

// Initialize the logger
LogManager
    .Setup()
    .LoadConfigurationFromFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "nlog.config"))
    .GetCurrentClassLogger();

// Remove all loggers and add NLog
// The NLog targets are used to filter the log level.
// By setting the level to trace for the internal, we get anything from MS.
// The LogLevel of NLog will determine, what is displayed, allowing us to combine the MS logs with our custom logs.
builder.Logging.ClearProviders();
builder.Logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
builder.Host.UseNLog();

// Add consul
var consulUrl = Environment.GetEnvironmentVariable("CONSUL_URL") ?? "http://localhost:8500";

var isTestingEnv = Environment.GetEnvironmentVariable("IS_TESTING_ENV") == "true";
var isCi = Environment.GetEnvironmentVariable("CI") == "true";

// Remove the default sources
builder.Configuration.Sources.Clear();

// Add appsettings, environment appsettings, consul appsettings, consul appsettings appsettings and environment variables
// Consul is not used in the test cases, if run locally
// The order matters
builder
    .Configuration.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
    .AddJsonFile($"appsettings.{builder.Environment.EnvironmentName}.json", optional: true, reloadOnChange: true);

if (isCi || !isTestingEnv)
{
    builder
        .Configuration.AddConsul(
        "coscine/Coscine.Api/appsettings",
        options =>
        {
            options.ConsulConfigurationOptions = cco => cco.Address = new Uri(consulUrl);
            options.Optional = true;
            options.ReloadOnChange = true;
            options.PollWaitTime = TimeSpan.FromSeconds(5);
            options.OnLoadException = exceptionContext => exceptionContext.Ignore = true;
        }
    )
    .AddConsul(
        $"coscine/Coscine.Api/appsettings.{builder.Environment.EnvironmentName}",
        options =>
        {
            options.ConsulConfigurationOptions = cco => cco.Address = new Uri(consulUrl);
            options.Optional = true;
            options.ReloadOnChange = true;
            options.PollWaitTime = TimeSpan.FromSeconds(5);
            options.OnLoadException = exceptionContext => exceptionContext.Ignore = true;
        }
    );
}

builder
    .Configuration.AddEnvironmentVariables();

// Configure the logger, after the configuration was loaded
var loggerConfiguration = new LoggerConfiguration();
builder.Configuration.Bind(LoggerConfiguration.Section, loggerConfiguration);

// Set the default LogLevel
LogManager.Configuration.Variables["logLevel"] = loggerConfiguration.LogLevel;

// Set the log location
LogManager.Configuration.Variables["logHome"] = loggerConfiguration.LogHome;

LogManager.ReconfigExistingLoggers();

// Configure Trinity RDF
var entityAssembly = AppDomain
    .CurrentDomain.GetAssemblies()
    .First(x => x.FullName?.Contains(nameof(Coscine.Api.Core.Entities)) == true);

OntologyDiscovery.AddAssembly(entityAssembly);
MappingDiscovery.RegisterCallingAssembly();

var virtuosoProvider = new VirtuosoStoreProvider();
StoreFactory.LoadProvider(virtuosoProvider);

builder.Services.ConfigureTrinity(builder.Configuration);

// Custom extension methods
builder.Services.ConfigureCors(builder.Configuration);
builder.Services.ConfigureFormOptions();
builder.Services.ConfigureSqlContext(builder.Configuration);

// Store the keys in the database
builder.Services.AddDataProtection()
    .PersistKeysToDbContext<RepositoryContext>()
    .SetApplicationName("CoscineApi");

builder.Services.AddSingleton<StartupHealthCheck>();
builder.Services.AddHealthChecks()
    .AddDbContextCheck<RepositoryContext>("SQL Database", tags: ["health", "external"])
    .AddCheck<VirtuosoHealthCheck>("Virtuoso", tags: ["health", "external"])
    .AddCheck<StartupHealthCheck>("Startup", tags: ["ready"]);
builder.Services.ConfigureRepositories();
builder.Services.ConfigureServices();
builder.Services.ConfigureSwagger();

builder.Services.AddOpenTelemetry()
    .WithMetrics(builder =>
    {
        builder.AddPrometheusExporter();

        builder.AddMeter("Microsoft.AspNetCore.Hosting");
        builder.AddMeter("Microsoft.AspNetCore.Server.Kestrel");
        builder.AddMeter("Microsoft.AspNetCore.Http.Connections");
        builder.AddMeter("Microsoft.AspNetCore.Routing");
        builder.AddMeter("Microsoft.AspNetCore.Diagnostics");
        builder.AddMeter("Microsoft.AspNetCore.RateLimiting");

        builder.AddView("http.server.request.duration",
            new ExplicitBucketHistogramConfiguration
            {
                Boundaries = [ 0, 0.005, 0.01, 0.025, 0.05,
                       0.075, 0.1, 0.25, 0.5, 0.75, 1, 2.5, 5, 7.5, 10 ]
            });
    });

// Add default HttpClient
builder.Services.AddCustomHttpClient(builder.Configuration, ClientName.RdsS3Client);
builder.Services.AddCustomHttpClient(builder.Configuration, ClientName.ElasticSearchClient);
builder.Services.AddCustomHttpClient(builder.Configuration, ClientName.HandleClient);
builder.Services.AddCustomHttpClient(builder.Configuration, ClientName.NocClient);
builder.Services.AddCustomHttpClient(builder.Configuration, ClientName.VirtuosoReadClient);
builder.Services.AddCustomHttpClient(builder.Configuration, ClientName.VirtuosoUpdateClient);

// Add policies
builder.Services.AddPolicies(builder.Configuration);

// Add memory caching
builder.Services.AddMemoryCache();

// Add output caching
if (builder.Configuration.GetValue<bool?>("OutputCaching:Enabled") ?? true)
{
    builder.Services.AddOutputCache(options =>
    {
        // Commenting out, as the output caching breaks normal functionality
        //options.AddBasePolicy(OutputCacheWithAuthPolicy.Instance);
        options.AddPolicy("OutputCacheWithAuthPolicy", OutputCacheWithAuthPolicy.Instance);
    });
}

// Configure Sparql
builder.Services.AddSparqlConfiguration(builder.Configuration);

// Configure Logger
builder.Services.AddLoggerConfiguration(builder.Configuration);

//builder.Services.ConfigureIdentity();
builder.Services.AddJwtConfiguration(builder.Configuration);
builder.Services.AddAuthenticationConfiguration(builder.Configuration);
builder.Services.ConfigureAuthentication(builder.Configuration);

// Configure PID
builder.Services.AddPidConfiguration(builder.Configuration);

// Configure Maintenance
builder.Services.AddNocConfiguration(builder.Configuration);
builder.Services.AddMessageConfiguration(builder.Configuration);
builder.Services.AddMaintenanceConfiguration(builder.Configuration);

// Cofigure Terms Of Service
builder.Services.AddTermsOfServiceConfiguration(builder.Configuration);

// Add MailKit as a service
builder.Services.AddMailKitConfiguration(builder.Configuration);
builder.Services.ConfigureMailService(builder.Configuration);

// Add ElasticSearch as a service
builder.Services.AddElasticSearchConfiguration(builder.Configuration);

// Add resource type configuration
builder.Services.AddResourceTypeConfiguration(builder.Configuration);

// Add connection configuration
builder.Services.AddConnectionConfiguration(builder.Configuration);

// Add activity logging
builder.Services.ConfigureActivityLogging(builder.Configuration);

// Custom authenticator service
builder.Services.AddScoped<IAuthenticatorService, AuthenticatorService>();

// Custom authentication handler for resource based authentication
builder.Services.AddScoped<IAuthorizationHandler, ProjectAuthHandler>();
builder.Services.AddScoped<IAuthorizationHandler, ResourceAuthHandler>();
builder.Services.AddScoped<IAuthorizationHandler, ProjectInvitationAuthHandler>();
builder.Services.AddScoped<IAuthorizationHandler, ProjectQuotaAuthHandler>();
builder.Services.AddScoped<IAuthorizationHandler, HandleAuthHandler>();

// Add AutoMapper (AutoMapper.Extensions.Microsoft.DependencyInjection), loaded/provided from the Service project
// MappingProfile contains the specific mappings
builder.Services.AddAutoMapper(typeof(Program));

// Suppress any default model checking of the [ApiController] attribute
builder.Services.Configure<ApiBehaviorOptions>(options => options.SuppressModelStateInvalidFilter = true);

// Add custom validation
builder.Services.AddScoped<ValidationFilter>();
builder.Services.AddScoped<ValidateMediaTypeAttribute>();
builder.Services.AddScoped<MultipartSizeLimitAttribute>();

// Add data shaping
/*
 * Deactivated for now
builder.Services.AddScoped<IDataShaper<ProjectDto>, DataShaper<ProjectDto>>();
builder.Services.AddScoped<IDataShaper<DisciplineDto>, DataShaper<DisciplineDto>>();
builder.Services.AddScoped<IDataShaper<LicenseDto>, DataShaper<LicenseDto>>();
builder.Services.AddScoped<IDataShaper<RoleDto>, DataShaper<RoleDto>>();
builder.Services.AddScoped<IDataShaper<VisibilityDto>, DataShaper<VisibilityDto>>();
builder.Services.AddScoped<IDataShaper<ProjectRoleDto>, DataShaper<ProjectRoleDto>>();
*/

// Added for rate limiting
builder.Services.AddRateLimiterConfiguration(builder.Configuration);
builder.Services.ConfigureRateLimitingOptions(builder.Configuration);

// API versions
builder.Services.ConfigureVersioning();

builder
    .Services.AddControllers(config =>
    {
        config.RespectBrowserAcceptHeader = true;
        config.ReturnHttpNotAcceptable = true;
        config.OutputFormatters.RemoveType<StringOutputFormatter>();
        config.OutputFormatters.Add(new TextTurtleOutputFormatter());
        // Ensure, that the specialized formatter for application/ld+json is on top, so it is selected first.
        // This due to the default json formatter looking for application/*+json, denying the specialized formatter access.
        config.OutputFormatters.Insert(0, new JsonLdOutputFormatter());
    })
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
        options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
        options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
        // The order of adding is important
        options.JsonSerializerOptions.Converters.Add(new EnumMemberValueConverterFactory());
        options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
    });

builder.Services.AddResponseWrapper();
builder.Services.AddHttpContextAccessor();

var app = builder.Build();

// Used to find a list with all unique api versions
var apiVersionDescriptionProvider = app.Services.GetRequiredService<IApiVersionDescriptionProvider>();

var basePath = "coscine";

// No leading slash!
var baseSwaggerPath = $"{basePath}/api/swagger";

app.UseSwagger(c =>
{
    // Add a server list to the UI
    // Append the base path to the request URL
    // Otherwise the used API url will be wrong
    // Check if the scheme was rewritten by the reverse proxy
    c.PreSerializeFilters.Add(
        (swagger, httpReq) =>
        {
            string? forwardedProto = null;

            if (httpReq.Headers.TryGetValue("X-Forwarded-Proto", out var values))
            {
                forwardedProto = values.ToString();
            }

            swagger.Servers = new List<OpenApiServer>
            {
                new() { Url = $"{forwardedProto ?? httpReq.Scheme}://{httpReq.Host.Value}/{basePath}" }
            };
        }
    );

    c.RouteTemplate = $"{baseSwaggerPath}/{{documentname}}/swagger.json";
});

app.UseSwaggerUI(o =>
{
    // Add a swagger.json endpoint for all versions (automated discovery)
    foreach (var description in apiVersionDescriptionProvider.ApiVersionDescriptions.Reverse())
    {
        o.SwaggerEndpoint($"{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
    }

    // Change the swagger UI url
    o.RoutePrefix = baseSwaggerPath;

    // Keep your authentication token on reload!
    if (app.Environment.IsDevelopment())
    {
        o.ConfigObject.AdditionalItems.Add("persistAuthorization", "true");
    }
});

app.MapPrometheusScrapingEndpoint()
.AllowAnonymous();

#if DEBUG
// Quickly generate an admin token
app.MapWhen(context => context.Request.Path.StartsWithSegments("/coscine/generate-admin-token") && app.Environment.IsDevelopment(), appBuilder =>
    {
        appBuilder.Run(async context =>
        {
            using var scope = appBuilder.ApplicationServices.CreateScope();
            var authenticatorService = scope.ServiceProvider.GetRequiredService<IAuthenticatorService>();

            var jwtSecurityToken = authenticatorService.GenerateJwtSecurityToken(new Dictionary<string, string>
            {
                [System.Security.Claims.ClaimTypes.Role] = "administrator"
            }, 60 * 24 * 365 * 2);
            context.Response.ContentType = "application/json";

            var token = authenticatorService.GetJwtStringFromToken(jwtSecurityToken);

            await context.Response.WriteAsync($"{{ \"token\": \"{token}\" }}");
        });
    });

// Generate a token with a provided userId
app.MapWhen(context => context.Request.Path.StartsWithSegments("/coscine/generate-token-for-user") && app.Environment.IsDevelopment(), appBuilder =>
{
    appBuilder.Run(async context =>
    {
        using var scope = appBuilder.ApplicationServices.CreateScope();

        // Extract userId from query parameter
        if (!context.Request.Query.ContainsKey("userId"))
        {
            context.Response.StatusCode = 400; // Bad Request
            await context.Response.WriteAsync("userId parameter is required.");
            return;
        }

        var userId = context.Request.Query["userId"].ToString();

        if (string.IsNullOrWhiteSpace(userId))
        {
            context.Response.StatusCode = 400; // Bad Request
            await context.Response.WriteAsync("userId parameter is empty.");
            return;
        }

        var authenticatorService = scope.ServiceProvider.GetRequiredService<IAuthenticatorService>();
        var jwtSecurityToken = authenticatorService.GenerateJwtSecurityToken(new Dictionary<string, string>
        {
            ["userId"] = userId
        }, 60 * 24 * 365 * 2);

        context.Response.ContentType = "application/json";

        var token = authenticatorService.GetJwtStringFromToken(jwtSecurityToken);

        await context.Response.WriteAsync($"{{ \"token\": \"{token}\" }}");
    });
});
#endif

// Used for the reverse proxy
app.UsePathBase($"/{basePath}");

// Use extended global error handling
// Get the logger
var logger = app.Services.GetRequiredService<ILogger<ExceptionHandlerMiddleware>>();

// Add logger to exception handler
app.ConfigureExceptionHandler(logger);

app.UseHttpsRedirection();

// Forward proxy headers to the application
app.UseForwardedHeaders(
    new ForwardedHeadersOptions { ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto }
);

// Policy defined in extension method
app.UseCors("DefaultCorsPolicy");

// Enable authorization and authentication
app.UseAuthentication();

app.UseMiddleware<NLogClaimsMiddleware>();
app.UseMiddleware<ActivityLoggingMiddleware>();
app.UseMiddleware<TermsOfServiceMiddleware>();

app.UseAuthorization();

// Enable rate limiting based on IP or UserId
app.UseRateLimiter();

// Use output cache
if (builder.Configuration.GetValue<bool?>("OutputCaching:Enabled") ?? true)
{
    app.UseOutputCache();
}

app.MapControllers();

// The extra z at the end of "/healthz" is intentional, to indicate,
// that this endpoint is for machine usage and not for human users.

// Checks the overall health and the health of the connected systems
app.MapHealthChecks("/healthz", new HealthCheckOptions()
{
    Predicate = healthCheck => healthCheck.Tags.Contains("health"),
    ResponseWriter = HealthCheckExtensions.WriteResponse,
});

// Checks if any background task finished and API ready for usage
app.MapHealthChecks("/healthz/ready", new HealthCheckOptions
{
    Predicate = healthCheck => healthCheck.Tags.Contains("ready"),
    ResponseWriter = HealthCheckExtensions.WriteResponse,
});

// Simple probe, if the API is available.
// Makes no statement to health, just if it is reachable
app.MapHealthChecks("/healthz/live", new HealthCheckOptions
{
    ResponseWriter = HealthCheckExtensions.WriteResponse,
    Predicate = _ => false,
});

app.MigrateDatabase();

// Set the Startup to healthy.
// Can later be used for lengthy tasks (Deploying graphs, downloading files, etc.).
// A backgroud service would handle this and set the value using th DI.
using (var scope = app.Services.CreateScope())
{

    scope.ServiceProvider.GetRequiredService<StartupHealthCheck>().StartupCompleted = true;
}

app.Run();

/// <summary>Used for testing, as we need a reference to the Program.cs.</summary>
public partial class Program { }
