﻿namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// Represents the possible HTTP methods associated with file actions.
/// </summary>
public enum FileActionHttpMethod
{
    /// <summary>
    /// Represents the HTTP GET method.
    /// </summary>
    GET,

    /// <summary>
    /// Represents the HTTP POST method.
    /// </summary>
    POST,

    /// <summary>
    /// Represents the HTTP PUT method.
    /// </summary>
    PUT,

    /// <summary>
    /// Represents the HTTP DELETE method.
    /// </summary>
    DELETE,
}