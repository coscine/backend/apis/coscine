﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// Specifies the type of PID.
/// </summary>
public enum PidType
{
    /// <summary>
    /// The PID belongs to a Coscine project.
    /// </summary>
    [Description("The PID belongs to a Coscine project.")]
    [EnumMember(Value = "project")]
    Project,

    /// <summary>
    /// The PID belongs to a Coscine resource.
    /// </summary>
    [Description("The PID belongs to a Coscine resource.")]
    [EnumMember(Value = "resource")]
    Resource
}