﻿using Coscine.Api.Core.Shared.Enums.Utility;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// Specifies the status of a resource type.
/// </summary>
[JsonConverter(typeof(EnumMemberValueConverterFactory))]
public enum ResourceTypeStatus
{
    /// <summary>
    /// The resource type is hidden.
    /// </summary>
    [Description("The resource type is hidden.")]
    [EnumMember(Value = "hidden")]
    Hidden,

    /// <summary>
    /// The resource type is active.
    /// </summary>
    [Description("The resource type is active.")]
    [EnumMember(Value = "active")]
    Active
}