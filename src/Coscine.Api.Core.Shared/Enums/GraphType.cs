using System.Runtime.Serialization;

namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// Defines the graph types that can be used in the provenance model.
/// </summary>
public enum GraphType
{
    /// <summary>
    /// Represents a metadata graph.
    /// </summary>
    [EnumMember(Value = "metadata")]
    Metadata,

    /// <summary>
    /// Represents a data graph.
    /// </summary>
    [EnumMember(Value = "data")]
    Data
}