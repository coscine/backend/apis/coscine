﻿using Coscine.Api.Core.Shared.Enums.Utility;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// Specifies the supported external authentication services.
/// </summary>
/// <remarks>
/// Legacy name: <c>ExternalAuthenticators</c>.
/// </remarks>
[JsonConverter(typeof(EnumMemberValueConverterFactory))]
public enum IdentityProviders
{
    /// <summary>
    /// Shibboleth is an open-source software system that enables single sign-on (SSO) authentication and authorization across multiple organizations, allowing users to access multiple web resources using a single set of credentials.
    /// </summary>
    [Description("Shibboleth is an open-source software system that enables single sign-on (SSO) authentication and authorization across multiple organizations, allowing users to access multiple web resources using a single set of credentials.")]
    [EnumMember(Value = "Shibboleth")]
    Shibboleth,

    /// <summary>
    /// ORCID (Open Researcher and Contributor ID) is a persistent digital identifier that uniquely distinguishes researchers and provides a reliable way to connect their research activities and outputs.
    /// </summary>
    [Description("ORCID (Open Researcher and Contributor ID) is a persistent digital identifier that uniquely distinguishes researchers and provides a reliable way to connect their research activities and outputs.")]
    [EnumMember(Value = "ORCiD")]
    ORCiD,
}