﻿using Coscine.Api.Core.Shared.Enums.Utility;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// Represents the types of operations that can be performed in an RDF Patch document.
/// </summary>
[JsonConverter(typeof(EnumMemberValueConverterFactory))]
public enum RdfPatchOperationType
{
    ///// <summary>
    ///// Header operation, usually containing metadata about the patch.
    ///// </summary>
    //[EnumMember(Value = "H")]
    //Header, // H

    ///// <summary>
    ///// Marks the start of a transaction in the patch.
    ///// </summary>
    //[EnumMember(Value = "TX")]
    //TransactionStart, // TX

    ///// <summary>
    ///// Marks the commit point of a transaction in the patch.
    ///// </summary>
    //[EnumMember(Value = "TC")]
    //TransactionCommit, // TC

    ///// <summary>
    ///// Aborts the current transaction in the patch.
    ///// </summary>
    //[EnumMember(Value = "TA")]
    //TransactionAbort, // TA

    ///// <summary>
    ///// Adds a new prefix binding to the patch.
    ///// </summary>
    //[EnumMember(Value = "PA")]
    //PrefixAdd, // PA

    ///// <summary>
    ///// Removes an existing prefix binding from the patch.
    ///// </summary>
    //[EnumMember(Value = "PD")]
    //PrefixDelete, // PD

    /// <summary>
    /// Adds (asserts) a triple or quad to the patch.
    /// </summary>
    [EnumMember(Value = "A")]
    Add, // A

    /// <summary>
    /// Deletes (retracts) a triple or quad from the patch.
    /// </summary>
    [EnumMember(Value = "D")]
    Delete // D
}