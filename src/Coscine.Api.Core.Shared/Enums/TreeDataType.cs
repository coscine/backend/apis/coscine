﻿namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// Represents the types of nodes within a hierarchical tree structure.
/// </summary>
public enum TreeDataType
{
    /// <summary>
    /// Represents a node in the structure that has children nodes.
    /// </summary>
    Tree,

    /// <summary>
    /// Represents a node in the structure that does not have any further elements below it.
    /// </summary>
    Leaf,
}