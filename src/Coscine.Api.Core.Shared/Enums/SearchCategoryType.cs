﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// Specifies the search item type.
/// </summary>
[JsonConverter(typeof(JsonStringEnumConverter))]
public enum SearchCategoryType
{
    /// <summary>
    /// The search item type not defined.
    /// </summary>
    [Description("The search item type not defined.")]
    [EnumMember(Value = "None")]
    None,

    /// <summary>
    /// The search item type is metadata.
    /// </summary>
    [Description("The search item type is Metadata.")]
    [EnumMember(Value = "https://purl.org/coscine/terms/structure#Metadata")]
    Metadata,

    /// <summary>
    /// The search item type is Project.
    /// </summary>
    [Description("The search item type is Project.")]
    [EnumMember(Value = "https://purl.org/coscine/terms/structure#Project")]
    Project,

    /// <summary>
    /// The search item type is Resource.
    /// </summary>
    [Description("The search item type is Resource.")]
    [EnumMember(Value = "https://purl.org/coscine/terms/structure#Resource")]
    Resource,
}