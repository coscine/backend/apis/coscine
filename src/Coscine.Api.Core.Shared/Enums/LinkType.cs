﻿namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// An enum representing all Linked Data Types (Container, Resource) and Authorization
/// </summary>
public enum LDPLinkType
{
    /// <summary>
    /// LDP Container.
    /// </summary>
    Container,

    /// <summary>
    /// LDP Resource.
    /// </summary>
    Resource,

    /// <summary>
    /// WAC Authorization strategy.
    /// </summary>
    Authorization
}