﻿using Coscine.Api.Core.Shared.Enums.Utility;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// Specifies the RDF <i>(Resource Description Framework)</i> format.
/// </summary>
[JsonConverter(typeof(EnumMemberValueConverterFactory))]
public enum RdfFormat
{
    /// <summary>
    /// Represents the Turtle <i>(Terse RDF Triple Language)</i> RDF format.
    /// </summary>
    /// <remarks>The media type for Turtle data is <c>text/turtle</c>.</remarks>
    [Description("Represents the Turtle (Terse RDF Triple Language) RDF format.")]
    [EnumMember(Value = "text/turtle")]
    Turtle,

    /// <summary>
    /// Represents the JSON-LD <i>(JavaScript Object Notation for Linked Data)</i> RDF format.
    /// </summary>
    /// <remarks>The media type for JSON-LD data is <c>application/json+ld</c>.</remarks>
    [Description("Represents the JSON-LD (JavaScript Object Notation for Linked Data) RDF format.")]
    [EnumMember(Value = "application/ld+json")]
    JsonLd,

    /// <summary>
    /// Represents the TriG RDF format.
    /// </summary>
    /// <remarks>The media type for TriG data is <c>application/x-trig</c>.</remarks>
    [Description("Represents the TriG RDF format.")]
    [EnumMember(Value = "application/x-trig")]
    TriG,
}