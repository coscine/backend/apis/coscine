﻿namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// An enumeration representing the supported HTTP verbs.
/// </summary>
public enum CoscineHttpMethod
{
    /// <summary>
    /// Represents the GET HTTP verb, used to retrieve data.
    /// </summary>
    GET,

    /// <summary>
    /// Represents the HEAD HTTP verb, used to retrieve headers without the response body.
    /// </summary>
    HEAD,

    /// <summary>
    /// Represents the POST HTTP verb, used to update a current resource with new data.
    /// </summary>
    POST,

    /// <summary>
    /// Represents the PUT HTTP verb, used to update a current resource with new data.
    /// </summary>
    PUT,

    /// <summary>
    /// Represents the DELETE HTTP verb, used to delete a resource.
    /// </summary>
    DELETE
}