﻿using System.ComponentModel;
using System.Reflection;
using System.Runtime.Serialization;

namespace Coscine.Api.Core.Shared.Enums.Utility;

/// <summary>
/// Extension methods for working with enumerations and their associated attributes.
/// </summary>
public static class EnumExtensions
{
    /// <summary>
    /// Gets the <c>Value</c> of the <see cref="EnumMemberAttribute"/> for the specified enum value.
    /// <code>
    /// [EnumMember(Value = "VALUE_TO_RETURN")]
    /// </code>
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="value">The enum value.</param>
    /// <returns>The value of the <see cref="EnumMemberAttribute"/>, or <c>null</c> if not found.</returns>
    public static string? GetEnumMemberValue<TEnum>(this TEnum value) where TEnum : Enum
    {
        return value.GetType()
            .GetMember(value.ToString())
            .First()
            .GetCustomAttribute<EnumMemberAttribute>()
            ?.Value;
    }

    /// <summary>
    /// Gets the <see cref="DescriptionAttribute"/> value for the specified enum value.
    /// <code>
    /// [Description("VALUE_TO_RETURN")]
    /// </code>
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="enum">The enum value.</param>
    /// <returns>The <see cref="DescriptionAttribute"/> value, or <c>null</c> if not found.</returns>
    public static string? GetDescription<TEnum>(this TEnum @enum) where TEnum : Enum
    {
        if (@enum is not null)
        {
            var fieldName = @enum.ToString();
            if (fieldName is not null)
            {
                var field = @enum.GetType().GetField(fieldName);
                if (field is not null)
                {
                    return field
                            .GetCustomAttributes(typeof(DescriptionAttribute), false)
                            .Cast<DescriptionAttribute>()
                            .FirstOrDefault()?.Description ?? null;
                }
            }
        }
        return null;
    }

    /// <summary>
    /// Tries to parse a string representation of an enumeration member into its corresponding enumeration value.
    /// <code>
    /// [EnumMember(Value = "VALUE_TO_PARSE")]
    /// </code>
    /// </summary>
    /// <typeparam name="TEnum">The enum type.</typeparam>
    /// <param name="value">The string representation of the enumeration member.</param>
    /// <param name="result">When this method returns, contains the enumeration value corresponding to the string representation,
    /// if the conversion succeeded, or the default value of <typeparamref name="TEnum"/> if the conversion failed.</param>
    /// <returns><see langword="true"/> if the string representation was successfully parsed; otherwise, <see langword="false"/>.</returns>
    /// <remarks>
    /// This extension method can be used to parse a string representation of an enumeration member with the <see cref="EnumMemberAttribute"/>
    /// into its corresponding enumeration value. If the parsing succeeds, the resulting value will be stored in the <paramref name="result"/> parameter.
    /// If the parsing fails, the <paramref name="result"/> parameter will be set to the default value of the enumeration <typeparamref name="TEnum"/>.
    /// </remarks>
    public static bool TryParseEnumMember<TEnum>(this string value, out TEnum result) where TEnum : struct, Enum
    {
        foreach (var item in Enum.GetValues(typeof(TEnum)).Cast<TEnum>())
        {
            var itemName = item.ToString();
            if (itemName is null) continue;

            var fi = typeof(TEnum).GetField(itemName);
            if (fi is null) continue;

            var attributes = (EnumMemberAttribute[])fi.GetCustomAttributes(typeof(EnumMemberAttribute), false);

            if (attributes.Length > 0 && attributes[0].Value == value)
            {
                result = item;
                return true;
            }
        }

        result = default;
        return false;
    }
}