﻿using System.Reflection;
using System.Runtime.Serialization;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Shared.Enums.Utility;

/// <summary>
/// Provides a JSON converter factory for converting enum values based on the associated EnumMemberAttribute.
/// </summary>
public class EnumMemberValueConverterFactory : JsonConverterFactory
{
    /// <summary>
    /// Determines if the converter can convert the specified type to its enum representation.
    /// </summary>
    /// <param name="typeToConvert">The type to be converted.</param>
    /// <returns><c>true</c> if the type can be converted; otherwise, <c>false</c>.</returns>
    public override bool CanConvert(Type typeToConvert)
    {
        // Checks if the type is an enum and all of its members have the EnumMemberAttribute.
        return typeToConvert.IsEnum &&
               typeToConvert.GetFields(BindingFlags.Public | BindingFlags.Static)
                            .All(field => Attribute.IsDefined(field, typeof(EnumMemberAttribute)));
    }

    /// <summary>
    /// Creates a JSON converter for the specified type.
    /// </summary>
    /// <param name="typeToConvert">The type to be converted.</param>
    /// <param name="options">The serializer options.</param>
    /// <returns>A JSON converter for the specified type.</returns>
    public override JsonConverter? CreateConverter(Type typeToConvert, JsonSerializerOptions options)
    {
        var converter = (JsonConverter?)Activator.CreateInstance(
            typeof(EnumMemberValueConverter<>)
                .MakeGenericType(typeToConvert),
            BindingFlags.Instance | BindingFlags.Public,
            binder: null,
            args: Array.Empty<object>(),
            culture: null);

        return converter!;
    }

    /// <summary>
    /// Provides a JSON converter for converting enum values based on associated EnumMemberAttribute.
    /// </summary>
    /// <typeparam name="T">The enum type.</typeparam>
    public class EnumMemberValueConverter<T> : JsonConverter<T> where T : Enum
    {
        /// <summary>
        /// Reads and converts JSON to the enum value.
        /// </summary>
        public override T Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var enumValueAsString = reader.GetString();
            foreach (var value in Enum.GetValues(typeToConvert).Cast<T>())
            {
                var enumMemberValue = value.GetEnumMemberValue();
                if (enumMemberValue == enumValueAsString || (enumValueAsString is not null && string.Equals(value.ToString(), enumValueAsString.Trim(), StringComparison.OrdinalIgnoreCase)))
                {
                    return value;
                }
            }

            throw new JsonException($"Unable to convert \"{enumValueAsString}\" to {typeToConvert.Name}.");
        }

        /// <summary>
        /// Writes the enum value to JSON.
        /// </summary>
        public override void Write(Utf8JsonWriter writer, T value, JsonSerializerOptions options)
        {
            var enumMemberValue = value.GetEnumMemberValue() ?? value.ToString();
            writer.WriteStringValue(enumMemberValue);
        }
    }
}