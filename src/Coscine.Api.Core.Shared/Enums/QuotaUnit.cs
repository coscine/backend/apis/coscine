﻿using Coscine.Api.Core.Shared.Enums.Utility;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// Specifies the unit of quota.
/// </summary>
[JsonConverter(typeof(EnumMemberValueConverterFactory))]
public enum QuotaUnit
{
    /// <summary>
    /// The byte is a unit of digital information in computing and telecommunications that most commonly consists of eight bits.
    /// </summary>
    [Description("The byte is a unit of digital information in computing and telecommunications that most commonly consists of eight bits.")]
    [EnumMember(Value = "https://qudt.org/vocab/unit/BYTE")]
    BYTE,

    /// <summary>
    /// The kibibyte is a multiple of the unit byte for digital information equivalent to 1024 bytes.
    /// </summary>
    [Description("The kibibyte is a multiple of the unit byte for digital information equivalent to 1024 bytes.")]
    [EnumMember(Value = "https://qudt.org/vocab/unit/KibiBYTE")]
    KibiBYTE,

    /// <summary>
    /// The mebibyte is a multiple of the unit byte for digital information equivalent to 1024^2 or 2^20 bytes.
    /// </summary>
    [Description("The mebibyte is a multiple of the unit byte for digital information equivalent to 1024^2 or 2^20 bytes.")]
    [EnumMember(Value = "https://qudt.org/vocab/unit/MebiBYTE")]
    MebiBYTE,

    /// <summary>
    /// The gibibyte is a multiple of the unit byte for digital information storage. The prefix gibi means 1024^3.
    /// </summary>
    [Description("The gibibyte is a multiple of the unit byte for digital information storage. The prefix gibi means 1024^3.")]
    [EnumMember(Value = "https://qudt.org/vocab/unit/GibiBYTE")]
    GibiBYTE,

    /// <summary>
    /// The tebibyte is a multiple of the unit byte for digital information. The prefix tebi means 1024^4.
    /// </summary>
    [Description("The tebibyte is a multiple of the unit byte for digital information. The prefix tebi means 1024^4.")]
    [EnumMember(Value = "https://qudt.org/vocab/unit/TebiBYTE")]
    TebiBYTE,

    /// <summary>
    /// The pebibyte is a multiple of the unit byte for digital information. The prefix pebi means 1024^5.
    /// </summary>
    [Description("The pebibyte is a multiple of the unit byte for digital information. The prefix pebi means 1024^5.")]
    [EnumMember(Value = "https://qudt.org/vocab/unit/PebiBYTE")]
    PebiBYTE
}