﻿using Coscine.Api.Core.Shared.Enums.Utility;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// Accepted languages by the API.
/// </summary>
[JsonConverter(typeof(EnumMemberValueConverterFactory))]
public enum AcceptedLanguage
{
    /// <summary>
    /// German language.
    /// </summary>
    [EnumMember(Value = "de")]
    de,

    /// <summary>
    /// English language.
    /// </summary>
    [EnumMember(Value = "en")]
    en,
}