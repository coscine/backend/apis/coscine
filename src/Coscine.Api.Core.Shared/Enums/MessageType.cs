using System.ComponentModel;

namespace Coscine.Api.Core.Shared.Enums;

/// <summary>
/// Defines the various types of messages.
/// </summary>
public enum MessageType
{
    /// <summary>
    /// Represents a disturbance message type.
    /// </summary>
    [Description("Disturbance|Störung")]
    Disturbance,

    /// <summary>
    /// Represents a partial disturbance message type.
    /// </summary>
    [Description("Partial Disturbance|Teilstörung")]
    PartialDisturbance,

    /// <summary>
    /// Represents a maintenance message type.
    /// </summary>
    [Description("Maintenance|Wartung")]
    Maintenance,

    /// <summary>
    /// Represents a partial maintenance message type.
    /// </summary>
    [Description("Partial Maintenance|Teilwartung")]
    PartialMaintenance,

    /// <summary>
    /// Represents a limited operation message type.
    /// </summary>
    [Description("Limited Operation|eingeschränkt betriebsfähig")]
    LimitedOperation,

    /// <summary>
    /// Represents an interruption message type.
    /// </summary>
    [Description("Interruption|Unterbrechung")]
    Interruption,

    /// <summary>
    /// Represents a change message type.
    /// </summary>
    [Description("Change|Änderung")]
    Change,

    /// <summary>
    /// Represents a hint message type.
    /// </summary>
    [Description("Hint|Hinweis")]
    Hint,

    /// <summary>
    /// Represents an error message type.
    /// </summary>
    [Description("Warning|Warnung|Warn")]
    Warning,

    /// <summary>
    /// Represents an informational message type.
    /// </summary>
    [Description("Information|Information|Info")]
    Information
}
