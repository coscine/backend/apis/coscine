﻿namespace Coscine.Api.Core.Shared;

/// <summary>
/// Defines the roles used within the API.
/// </summary>
/// <remarks>
/// This class uses constants instead of an enum to define roles due to the requirements of ASP.NET Core Authorization.
/// The roles defined here are essential for controlling access and permissions across different parts of the API, incuding its autorization strategies.
/// </remarks>
public static class ApiRoles
{
    /// <summary>
    /// The 'Administrator' role represents a high-level access role within the API.
    /// </summary>
    /// <remarks><b>Use with caution!</b> This role grants extensive privileges, including full read and write access.</remarks>
    public const string Administrator = "administrator";

    /// <summary>
    /// The 'User' role represents a standard access level within the API.
    /// </summary>
    /// <remarks> 
    /// This role is designed for regular API consumers and is typically assigned for day-to-day operations.
    /// It operates under standard authorization protocols, like token-based authentication and is subject to additional role-based access controls
    /// based on the specific privileges associated with each user account.
    /// </remarks>
    public const string User = "user";
}