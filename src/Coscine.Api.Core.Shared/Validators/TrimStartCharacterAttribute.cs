﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.Validators;

/// <summary>
/// Validation attribute that trims the specified character from the start of a string property value.
/// </summary>
[AttributeUsage(AttributeTargets.Property)]
public class TrimStartCharacterAttribute : ValidationAttribute
{
    private readonly char _characterToTrim;

    /// <summary>
    /// Initializes a new instance of the <see cref="TrimStartCharacterAttribute"/> class.
    /// </summary>
    /// <param name="characterToTrim">The character to trim from the start of the string.</param>
    public TrimStartCharacterAttribute(char characterToTrim)
    {
        _characterToTrim = characterToTrim;
    }

    /// <summary>
    /// Trims the specified character from the start of the string property value.
    /// </summary>
    /// <param name="value">The value to validate.</param>
    /// <param name="validationContext">The validation context.</param>
    /// <returns>A <see cref="ValidationResult"/> indicating success or an error.</returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (validationContext.MemberName != null)
        {
            var property = validationContext.ObjectType.GetProperty(validationContext.MemberName);
            if (property != null && value is string stringValue)
            {
                property.SetValue(validationContext.ObjectInstance, stringValue.TrimStart(_characterToTrim));
            }
        }

        return ValidationResult.Success;
    }
}