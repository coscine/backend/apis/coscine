using System.ComponentModel.DataAnnotations;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

namespace Coscine.Api.Core.Shared.Validators;

/// <summary>
/// Validation attribute that checks if there is a responsible organiozation.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
public class ValidOrganizationsAttribute : ValidationAttribute
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ValidOrganizationsAttribute"/> class.
    /// </summary>
    public ValidOrganizationsAttribute()
    {
    }

    /// <summary>
    /// Validates whether the field contains the required number of responsible organizations and no duplicates.
    /// </summary>
    /// <param name="value">The value to validate.</param>
    /// <param name="validationContext">The validation context.</param>
    /// <returns>A <see cref="ValidationResult"/> indicating success or an error.</returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value is null)
        {
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }

        if (value is not IEnumerable<OrganizationForProjectManipulationDto> list)
        {
            return new ValidationResult($"{validationContext.DisplayName} is not in the expected list format.");
        }

        if ( !list.Any() )
        {
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }
        
        if ( list.Count(o => o.Responsible == true) != 1){
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }

        if ( list.Count() != list.DistinctBy(p => p.Uri).Count()){
            return new ValidationResult($"{validationContext.DisplayName} does not allow duplicate entries.");
        }

        return ValidationResult.Success;
    }

    /// <summary>
    /// Formats the error message for the validation attribute.
    /// </summary>
    /// <param name="member">The name of the member to validate.</param>
    /// <returns>The formatted error message.</returns>
    public override string FormatErrorMessage(string member)
    {
        if (!string.IsNullOrWhiteSpace(ErrorMessage))
        {
            return string.Format(ErrorMessageString, member);
        }

        return $"The {member} field must contain a responsible organization.";
    }
}