﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.Validators;

/// <summary>
/// Validation attribute that ensures a provided value is an absolute URI.
/// </summary>
[AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
public class AbsoluteUriAttribute : ValidationAttribute
{
    /// <summary>
    /// Validates if the provided object is an absolute URI.
    /// </summary>
    /// <param name="value">The value to validate.</param>
    /// <param name="validationContext">The validation context.</param>
    /// <returns>A <see cref="ValidationResult"/> indicating whether the value is an absolute URI.</returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value is Uri uri)
        {
            if (!uri.IsAbsoluteUri)
            {
                var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(errorMessage);
            }
        }

        return ValidationResult.Success;
    }

    /// <summary>
    /// Formats an error message if one is not explicitly provided.
    /// </summary>
    /// <param name="member">The member being validated.</param>
    /// <returns>The formatted error message.</returns>
    public override string FormatErrorMessage(string member)
    {
        if (!string.IsNullOrWhiteSpace(ErrorMessage))
        {
            return string.Format(ErrorMessageString, member);
        }

        return $"The {member} field must be an absolute URI.";
    }
}