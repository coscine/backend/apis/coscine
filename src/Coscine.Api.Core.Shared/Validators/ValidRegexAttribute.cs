﻿using Coscine.Api.Core.Shared.Helpers;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.Validators;

/// <summary>
/// Validation attribute that ensures a provided value is a valid regular expression.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
public class ValidRegexAttribute : ValidationAttribute
{
    /// <summary>
    /// Validates if the provided object is a string that represents a valid Regex expression.
    /// </summary>
    /// <param name="value">The value to validate.</param>
    /// <param name="validationContext">The validation context.</param>
    /// <returns>A <see cref="ValidationResult"/> indicating whether the value is a valid Regex expression.</returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value is string stringValue)
        {
            if (RegexExtensions.TryParse(stringValue, out _))
            {
                return ValidationResult.Success;
            }
            else
            {
                var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(errorMessage);
            }
        }

        // If the value is null, we consider it valid since the attribute does not enforce not null,
        // leaving the responsibility for null checking to other mechanisms or attributes (like [Required]) if desired
        return ValidationResult.Success;
    }

    /// <summary>
    /// Formats an error message if one is not explicitly provided.
    /// </summary>
    /// <param name="member">The member being validated.</param>
    /// <returns>The formatted error message.</returns>
    public override string FormatErrorMessage(string member)
    {
        if (!string.IsNullOrWhiteSpace(ErrorMessage))
        {
            return string.Format(ErrorMessageString, member);
        }

        return $"The {member} field must contain a valid Regex expression.";
    }
}
