﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.Validators;

/// <summary>
/// Validation attribute that checks the maximum total length of multiple strings.
/// </summary>
[AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
public class MaxTotalLengthAttribute : ValidationAttribute
{
    private readonly int _maxLength;

    /// <summary>
    /// Initializes a new instance of the <see cref="MaxTotalLengthAttribute"/> class with the specified maximum length.
    /// </summary>
    /// <param name="maxLength">The maximum total length allowed.</param>
    public MaxTotalLengthAttribute(int maxLength)
    {
        _maxLength = maxLength;
    }

    /// <summary>
    /// Validates whether the total length of multiple strings does not exceed the specified limit.
    /// </summary>
    /// <param name="value">The collection of strings to validate.</param>
    /// <param name="validationContext">The validation context.</param>
    /// <returns>A <see cref="ValidationResult"/> indicating success or an error.</returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value is IEnumerable<string> enumerable)
        {
            var totalLength = string.Join(';', enumerable).Length;

            if (totalLength > _maxLength)
            {
                var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                return new ValidationResult(errorMessage);
            }
        }

        return ValidationResult.Success;
    }

    /// <summary>
    /// Formats the error message for the validation attribute.
    /// </summary>
    /// <param name="name">The name of the member to validate.</param>
    /// <returns>The formatted error message.</returns>
    public override string FormatErrorMessage(string name)
    {
        return string.Format(ErrorMessageString, name, _maxLength);
    }
}