﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.Validators;

/// <summary>
/// Validation attribute that checks the minimum length of a string after trimming.
/// </summary>
[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
public class MinLengthAfterTrimAttribute : ValidationAttribute
{
    private readonly int _minLength;

    /// <summary>
    /// Initializes a new instance of the <see cref="MinLengthAfterTrimAttribute"/> class with the specified minimum length.
    /// </summary>
    /// <param name="minLength">The minimum length required after trimming.</param>
    public MinLengthAfterTrimAttribute(int minLength)
    {
        _minLength = minLength;
    }

    /// <summary>
    /// Validates whether the string length after trimming meets the specified minimum length.
    /// </summary>
    /// <param name="value">The value to validate.</param>
    /// <param name="validationContext">The validation context.</param>
    /// <returns>A <see cref="ValidationResult"/> indicating success or an error.</returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value is null)
        {
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }

        if (value is not string str)
        {
            return new ValidationResult($"{validationContext.DisplayName} is not of type string.");
        }

        str = str.Trim();
        if (str.Length < _minLength)
        {
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }

        return ValidationResult.Success;
    }

    /// <summary>
    /// Formats the error message for the validation attribute.
    /// </summary>
    /// <param name="member">The name of the member to validate.</param>
    /// <returns>The formatted error message.</returns>
    public override string FormatErrorMessage(string member)
    {
        if (!string.IsNullOrWhiteSpace(ErrorMessage))
        {
            return string.Format(ErrorMessageString, member, _minLength);
        }

        return $"The {member} field must have a minimum length of {_minLength} characters after trimming.";
    }
}