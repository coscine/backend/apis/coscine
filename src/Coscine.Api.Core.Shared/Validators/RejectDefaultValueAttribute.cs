﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.Validators;

/// <summary>
/// Validation attribute that rejects default values for value types and null values for reference types.
/// </summary>
[AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
public class RejectDefaultValueAttribute : ValidationAttribute
{
    /// <summary>
    /// Validates whether the provided value is the default value for value types or null for reference types.
    /// </summary>
    /// <param name="value">The value to validate.</param>
    /// <param name="validationContext">The validation context.</param>
    /// <returns>A <see cref="ValidationResult"/> indicating success or an error.</returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        if (value is null)
        {
            return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
        }

        var type = value.GetType();
        if (type.IsValueType)
        {
            var defaultValue = Activator.CreateInstance(type);
            if (value.Equals(defaultValue)) // Important: Use .Equals() instead of ==
            {
                return new ValidationResult(FormatErrorMessage(validationContext.DisplayName));
            }
        }

        return ValidationResult.Success;
    }

    /// <summary>
    /// Formats the error message for the validation attribute.
    /// </summary>
    /// <param name="member">The name of the member to validate.</param>
    /// <returns>The formatted error message.</returns>
    public override string FormatErrorMessage(string member)
    {
        if (!string.IsNullOrWhiteSpace(ErrorMessage))
        {
            return string.Format(ErrorMessageString, member);
        }

        return $"The {member} field cannot have its default value or be null.";
    }
}