﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.Validators;

/// <summary>
/// Validation attribute that ensures only one of the specified properties is set to true.
/// </summary>
[AttributeUsage(AttributeTargets.Property)]
public class AllowOnlyOneAttribute : ValidationAttribute
{
    private readonly string[] _otherPropertyNames;

    /// <summary>
    /// Initializes a new instance of the <see cref="AllowOnlyOneAttribute"/> class.
    /// </summary>
    /// <param name="otherPropertyNames">Names of other properties to compare.</param>
    public AllowOnlyOneAttribute(params string[] otherPropertyNames)
    {
        _otherPropertyNames = otherPropertyNames;
    }

    /// <summary>
    /// Validates whether only one of the specified properties is set to true.
    /// </summary>
    /// <param name="value">The value of the current property.</param>
    /// <param name="validationContext">The validation context.</param>
    /// <returns>A <see cref="ValidationResult"/> indicating whether only one property is set to true.</returns>
    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        var otherProperties = _otherPropertyNames
            .Select(name => validationContext.ObjectType.GetProperty(name))
            .ToList();

        var otherPropertyValues = otherProperties
            .ConvertAll(property => (bool?)property?.GetValue(validationContext.ObjectInstance));

        if ((bool?)value == true && otherPropertyValues.Any(v => v == true))
        {
            var conflictingProperties = otherProperties
                .Where(property => (bool?)property?.GetValue(validationContext.ObjectInstance) == true)
                .Select(property => property?.Name);

            var errorMessage = $"Only one of '{validationContext.MemberName}' or {string.Join("', '", conflictingProperties)} can be set.";
            return new ValidationResult(errorMessage);
        }

        return ValidationResult.Success;
    }
}