﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.Validators;

/// <summary>
/// Validation attribute that ensures the maximum length of a URI string.
/// </summary>
[AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
public class MaxLengthForUriAttribute : ValidationAttribute
{
    private readonly int _maxLength;

    /// <summary>
    /// Initializes a new instance of the <see cref="MaxLengthForUriAttribute"/> class with the specified maximum length.
    /// </summary>
    /// <param name="maxLength">The maximum length of the URI string.</param>
    public MaxLengthForUriAttribute(int maxLength)
    {
        _maxLength = maxLength;
    }

    /// <summary>
    /// Validates whether the provided URI string length is within the specified limit.
    /// </summary>
    /// <param name="value">The URI value to validate.</param>
    /// <returns><c>true</c> if the URI string length is within the specified limit; otherwise, <c>false</c>.</returns>
    public override bool IsValid(object? value)
    {
        if (value is Uri uri)
        {
            return uri.ToString().Length <= _maxLength;
        }
        return false;
    }

    /// <summary>
    /// Formats the error message for the validation attribute.
    /// </summary>
    /// <param name="member">The name of the member to validate.</param>
    /// <returns>The formatted error message.</returns>
    public override string FormatErrorMessage(string member)
    {
        if (!string.IsNullOrWhiteSpace(ErrorMessage))
        {
            return string.Format(ErrorMessageString, member, _maxLength);
        }

        return $"The {member} field can only have a maximum length of {_maxLength} characters after converting to a URI string.";
    }
}