﻿namespace Coscine.Api.Core.Shared;

/// <summary>
/// Provides a collection of constant handles relevant to the Coscine PID Record.
/// These handles represent entries in the Data Type Registry (DTR) and are used to identify various
/// types of information within the PID system.
/// </summary>
public static class PidHandles
{
    /// <summary>
    /// The general handle for the Kernel Information Profile type.
    /// </summary>
    public static readonly string KernelInformationProfileHandle = "21.T11148/076759916209e5d62bd5";

    /// <summary>
    /// The handle for the Coscine-specific Kernel Information Profile type.
    /// </summary>
    public static readonly string CoscineKernelInformationProfileHandle = "21.T11148/8882327b7c25331e3cdd";

    /// <summary>
    /// The handle representing the date the PID record was created.
    /// </summary>
    public static readonly string DateCreatedHandle = "21.T11148/aafd5fb4c7222e2d950a";

    /// <summary>
    /// The handle for identifying the digital object's location.
    /// </summary>
    public static readonly string DigitalObjectLocationHandle = "21.T11148/b8457812905b83046284";

    /// <summary>
    /// The handle for the digital object type.
    /// </summary>
    public static readonly string DigitalObjectTypeHandle = "21.T11148/1c699a5d1b4ad3ba4956";

    /// <summary>
    /// The handle for the digital object value corresponding to a resource.
    /// </summary>
    public static readonly string DigitalObjectTypeResourceHandle = "21.T11148/12aad485b74d04f584c1";

    /// <summary>
    /// The handle for the digital object value corresponding to a project.
    /// </summary>
    public static readonly string DigitalObjectTypeProjectHandle = "21.T11148/0f13b0a83bd926fe269f";

    /// <summary>
    /// The handle representing the type of license associated with the digital object.
    /// </summary>
    public static readonly string LicenseHandle = "21.T11148/2f314c8fe5fb6a0063a8";

    /// <summary>
    /// The handle for the contact type, representing contact information within the PID record.
    /// </summary>
    public static readonly string ContactHandle = "21.T11148/1a73af9e7ae00182733b";

    /// <summary>
    /// The handle for the topic type, used to categorize or tag the digital object with specific topics.
    /// </summary>
    public static readonly string TopicHandle = "21.T11148/b415e16fbe4ca40f2270";
}