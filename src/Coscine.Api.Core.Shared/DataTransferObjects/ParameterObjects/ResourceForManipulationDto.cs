﻿using Coscine.Api.Core.Shared.Validators;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object (DTO) used for manipulating a resource.
/// </summary>
public abstract record ResourceForManipulationDto
{
    /// <summary>
    /// The name of the resource.
    /// </summary>
    /// <remarks>
    /// This field contains the name of the resource and is required.
    /// Maximum length for the name is 200 characters.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [MaxLength(200, ErrorMessage = "Maximum length for {0} is {1} characters.")]
    public required string Name { get; init; }

    /// <summary>
    /// The display name of the resource.
    /// </summary>
    /// <remarks>
    /// This field contains the display name of the resource, with a maximum length of 25 characters.
    /// </remarks>
    [MaxLength(25, ErrorMessage = "Maximum length for {0} is {1} characters.")]
    public string? DisplayName { get; init; }

    /// <summary>
    /// The description of the resource.
    /// </summary>
    /// <remarks>
    /// This field contains the description of the resource and is required.
    /// Maximum length for the description is 10000 characters.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [MaxLength(10000, ErrorMessage = "Maximum length for {0} is {1} characters.")]
    public required string Description { get; init; }

    /// <summary>
    /// The keywords associated with the resource.
    /// </summary>
    /// <remarks>
    /// This field holds keywords associated with the resource, allowing a total length of 1000 characters.
    /// </remarks>
    [MaxTotalLength(1000, ErrorMessage = "Maximum total length for {0} is {1} characters.")]
    public IEnumerable<string>? Keywords { get; init; }

    // TODO: Drop that from the SQL DB - move to RDF
    /// <summary>
    /// Fixed values associated with the resource.
    /// </summary>
    /// <remarks>
    /// This field contains fixed values associated with the resource.
    /// It is recommended to migrate this data from SQL to RDF.
    /// </remarks>
    public Dictionary<string, Dictionary<string, List<FixedValueForResourceManipulationDto>>>? FixedValues { get; init; }

    /// <summary>
    /// The license information related to the resource.
    /// </summary>
    public LicenseForResourceManipulationDto? License { get; init; }

    /// <summary>
    /// The usage rights description of the resource.
    /// </summary>
    /// <remarks>
    /// This field contains the usage rights description of the resource, limited to 200 characters.
    /// </remarks>
    [MaxLength(200, ErrorMessage = "Maximum length for {0} is {1} characters.")]
    public string? UsageRights { get; init; }

    /// <summary>
    /// If a local copy for the metadata should be created.
    /// </summary>
    /// <remarks>
    /// This field contains the setting, if a local metadata copy should be created in the resource. Only certain resources support this feature.
    /// </remarks>
    public bool? MetadataLocalCopy { get; init; }

    /// <summary>
    /// The visibility settings for the resource.
    /// </summary>
    /// <remarks>
    /// This field defines the visibility settings for the resource and is required.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required VisibilityForResourceManipulationDto Visibility { get; init; }

    /// <summary>
    /// The disciplines associated with the resource.
    /// </summary>
    /// <remarks>
    /// This field holds the disciplines related to the resource and is required.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required IEnumerable<DisciplineForResourceManipulationDto> Disciplines { get; init; }
}