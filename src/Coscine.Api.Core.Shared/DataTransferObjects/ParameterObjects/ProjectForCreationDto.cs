﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the creation of a project.
/// Inherits from the base class <see cref="ProjectForManipulationDto"/>.
/// </summary>
public record ProjectForCreationDto : ProjectForManipulationDto
{
    /// <summary>
    /// Gets or initializes the identifier of the parent project.
    /// </summary>
    /// <remarks>
    /// The ParentId field represents the identifier of the parent project (if any) associated with the new project creation.
    /// </remarks>
    public Guid? ParentId { get; init; }

    /// <summary>
    /// Gets or initializes if the owners of the parent project should be copied to the sub project.
    /// </summary>
    /// <remarks>
    /// This property determines whether the owners of the parent project should be copied to the sub project
    /// when the parent project ID is set (`ParentId` is not null). If `CopyOwnersFromParent` is set to true,
    /// the owners of the parent project will be assigned as owners of the sub project. This behavior is only
    /// applicable when the `parentId` property has been explicitly defined.
    /// </remarks>
    public bool? CopyOwnersFromParent { get; init; } = true;
}