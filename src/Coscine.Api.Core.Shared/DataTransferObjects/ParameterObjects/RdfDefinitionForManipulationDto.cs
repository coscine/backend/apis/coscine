﻿using Coscine.Api.Core.Shared.Enums;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object (DTO) used for manipulating RDF definitions.
/// </summary>
public record RdfDefinitionForManipulationDto
{
    /// <summary>
    /// The content of the RDF definition.
    /// </summary>
    /// <remarks>
    /// This field contains the content of the RDF definition.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string Content { get; init; }

    /// <summary>
    /// The format of the RDF definition, based on <see cref="RdfFormat"/>.
    /// </summary>
    /// <remarks>
    /// This field denotes the format of the RDF definition using the <see cref="RdfFormat"/> enum.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required RdfFormat Type { get; init; }
}