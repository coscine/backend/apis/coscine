using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ResourceTypeOptions;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object (DTO) for creating options related to any resource type.
/// </summary>
public record ResourceTypeOptionsForCreationDto
{
    /// <summary>
    /// Options for Linked Data resource types.
    /// </summary>
    public LinkedDataResourceTypeOptionsForManipulationDto? LinkedResourceTypeOptions { get; init; }

    /// <summary>
    /// Options for GitLab resource types.
    /// </summary>
    public GitlabResourceTypeOptionsForCreationDto? GitlabResourceTypeOptions { get; init; }

    /// <summary>
    /// Options for RDS resource types.
    /// </summary>
    public RdsResourceTypeOptionsForManipulationDto? RdsResourceTypeOptions { get; init; }

    /// <summary>
    /// Options for RDS S3 resource types.
    /// </summary>
    public RdsS3ResourceTypeOptionsForManipulationDto? RdsS3ResourceTypeOptions { get; init; }

    /// <summary>
    /// Options for RDS S3 Worm resource types.
    /// </summary>
    public RdsS3WormResourceTypeOptionsForManipulationDto? RdsS3WormResourceTypeOptions { get; init; }
}