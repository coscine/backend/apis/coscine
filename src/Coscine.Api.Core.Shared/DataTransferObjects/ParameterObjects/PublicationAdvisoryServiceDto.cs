﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the publication advisory service of an organization.
/// </summary>
public record PublicationAdvisoryServiceDto
{
    /// <summary>
    /// The data publication service's display name.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string DisplayName { get; init; }

    /// <summary>
    /// The data publication service's email address.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string Email { get; init; }

    /// <summary>
    /// The data publication service's description.
    /// </summary>
    public string? Description { get; init; }
}
