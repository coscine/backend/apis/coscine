﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents an RDF Patch document containing a series of operations.
/// </summary>
public record RdfPatchDocumentDto
{
    /// <summary>
    /// The list of operations in the RDF Patch document.
    /// </summary>
    public List<RdfPatchOperationDto> Operations { get; set; } = new List<RdfPatchOperationDto>();
}