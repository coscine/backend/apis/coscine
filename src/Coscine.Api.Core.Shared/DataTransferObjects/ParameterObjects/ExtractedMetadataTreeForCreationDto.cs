﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the creation of a metadata tree.
/// Extends the base class <see cref="MetadataTreeForManipulationDto"/>.
/// </summary>
public record ExtractedMetadataTreeForCreationDto : MetadataTreeForManipulationDto
{
    /// <summary>
    /// Gets or sets the id of the new metadata tree.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Uri Id { get; init; }

    /// <summary>
    /// Gets or initializes the RDF definition associated with the metadata tree creation.
    /// </summary>
    /// <remarks>
    /// The Definition field is required for creating the metadata tree.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required RdfDefinitionForManipulationDto Definition { get; init; }

    /// <summary>
    /// Gets or initializes the provenance information associated with the metadata tree update.
    /// </summary>
    /// <remarks>
    /// The Provenance field is required for updating the metadata tree.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required ProvenanceParametersDto Provenance { get; init; }
}
