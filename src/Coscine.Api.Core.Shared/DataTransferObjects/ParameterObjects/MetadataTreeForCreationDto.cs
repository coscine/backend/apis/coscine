﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the creation of a metadata tree.
/// Extends the base class <see cref="MetadataTreeForManipulationDto"/>.
/// </summary>
public record MetadataTreeForCreationDto : MetadataTreeForManipulationDto
{
    /// <summary>
    /// Gets or initializes the RDF definition associated with the metadata tree creation.
    /// </summary>
    /// <remarks>
    /// The Definition field is required for creating the metadata tree.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required RdfDefinitionForManipulationDto Definition { get; init; }
}