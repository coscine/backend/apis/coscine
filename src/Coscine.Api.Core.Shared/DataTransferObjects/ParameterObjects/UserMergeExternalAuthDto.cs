﻿using Coscine.Api.Core.Shared.Enums;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object (DTO) for merging user data from an external authentication provider.
/// </summary>
public record UserMergeExternalAuthDto
{
    /// <summary>
    /// The identity provider from which user data is to be merged.
    /// </summary>
    /// <remarks>
    /// This field specifies the identity provider and is a required field for merging user data.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required IdentityProviders IdentityProvider { get; init; }
}