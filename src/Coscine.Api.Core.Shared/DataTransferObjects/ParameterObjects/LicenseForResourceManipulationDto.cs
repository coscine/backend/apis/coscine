﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing a license for resource manipulation.
/// </summary>
public record LicenseForResourceManipulationDto
{
    /// <summary>
    /// Gets or initializes the identifier of the license.
    /// </summary>
    /// <remarks>
    /// The Id field is required for manipulation of licenses associated with resources.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Guid Id { get; init; }
}