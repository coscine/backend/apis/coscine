﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the creation of an API token.
/// </summary>
public record ApiTokenForCreationDto
{
    /// <summary>
    /// Gets or sets the name of the API token.
    /// </summary>
    /// <remarks>
    /// The Name field is required for the API token creation.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public string Name { get; set; } = null!;

    /// <summary>
    /// Gets or sets the expiration duration of the token in days.
    /// </summary>
    /// <remarks>
    /// ExpiresInDays represents the duration (in days) for which the API token remains valid.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [Range(7, 370, ErrorMessage = "{0} must be between {1} and {2} days.")]
    public uint ExpiresInDays { get; set; } = 7;
}