﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing a language for user manipulation.
/// </summary>
public record LanguageForUserManipulationDto
{
    /// <summary>
    /// Gets or initializes the identifier of the language.
    /// </summary>
    /// <remarks>
    /// The Id field is required for manipulation of languages associated with users.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Guid Id { get; init; }
}