﻿using Coscine.Api.Core.Shared.Enums;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents a data transfer object (DTO) used for manipulating quota values.
/// </summary>
public record QuotaForManipulationDto
{
    /// <summary>
    /// The numerical value of the quota. Must be a positive <see cref="long"/>.
    /// </summary>
    /// <remarks>
    /// This field holds the numerical value of the quota. Only positive numbers are allowed.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [Range(0, long.MaxValue, ErrorMessage = "Only a positive number is allowed")]
    public required long Value { get; init; }

    /// <summary>
    /// The unit of measurement for the quota, part of the accepted set of <see cref="QuotaUnit"/>s.
    /// </summary>
    /// <remarks>
    /// This field denotes the unit of measurement used for the quota value.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required QuotaUnit Unit { get; init; }
}