﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents a single operation in an RDF Patch document.
/// </summary>
public record RdfPatchOperationDto
{
    /// <summary>
    /// The type of operation.
    /// </summary>
    public RdfPatchOperationType OperationType { get; set; }

    /// <summary>
    /// The changes to be applied to the RDF data.
    /// </summary>
    public RdfDefinitionForManipulationDto Changes { get; set; } = null!;
}