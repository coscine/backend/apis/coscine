﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing an organization for user manipulation.
/// </summary>
public record OrganizationForUserManipulationDto
{
    /// <summary>
    /// Gets or initializes the URI of the organization.
    /// </summary>
    /// <remarks>
    /// The Uri field represents the URI of the organization and is a required field for user manipulation.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Uri Uri { get; init; }
}