﻿using Coscine.Api.Core.Shared.Validators;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) for manipulating metadata trees.
/// </summary>
public record MetadataTreeForManipulationDto
{
    /// <summary>
    /// Gets or initializes the path of the metadata tree.
    /// </summary>
    /// <remarks>
    /// The Path field represents the path of the metadata tree. The '/' character is trimmed from the start.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [TrimStartCharacter('/')]
    public required string Path { get; init; } = "";
}