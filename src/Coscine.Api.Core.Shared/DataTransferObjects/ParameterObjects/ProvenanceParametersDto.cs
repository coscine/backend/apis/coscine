﻿using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing Provenance Parameters in a request.
/// </summary>
public class ProvenanceParametersDto
{
    /// <summary>
    /// Gets or sets the adapted versions from the specific metadata tree.
    /// </summary>
    public IEnumerable<Uri> WasRevisionOf { get; init; } = [];

    /// <summary>
    /// Gets or sets the variants of the specific metadata tree.
    /// </summary>
    public IEnumerable<VariantDto> Variants { get; init; } = [];

    /// <summary>
    /// Information if the specific metadata tree was invalidated by something.
    /// </summary>
    public Uri? WasInvalidatedBy { get; init; }

    /// <summary>
    /// The similarity to the last version.
    /// </summary>
    public decimal? SimilarityToLastVersion { get; init; }

    /// <summary>
    /// Gets or initializes the version of the metadata extractor.
    /// </summary>
    public string? MetadataExtractorVersion { get; init; }

    /// <summary>
    /// Gets or initializes the hash parameters of the extracted metadata.
    /// </summary>
    public HashParametersDto? HashParameters { get; init; }
}