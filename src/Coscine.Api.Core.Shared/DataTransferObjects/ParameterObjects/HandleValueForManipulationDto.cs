﻿using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the manipulation of a handle.
/// </summary>
public abstract record HandleValueForManipulationDto
{
    /// <summary>
    /// The data type of the handle.
    /// </summary>
    [JsonPropertyName("type")]
    public required string Type { get; init; }

    /// <summary>
    /// The parsed data of the handle.
    /// </summary>
    [JsonPropertyName("parsed_data")]
    public required object ParsedData { get; init; }
}