﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the creation of a project role within a project.
/// Inherits from the base class <see cref="ProjectRoleForProjectManipulationDto"/>.
/// </summary>
public record ProjectRoleForProjectCreationDto : ProjectRoleForProjectManipulationDto
{
    /// <summary>
    /// Gets or initializes the identifier of the user associated with the project role creation.
    /// </summary>
    /// <remarks>
    /// The UserId field is required for defining the user linked to the project role within the project.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Guid UserId { get; init; }
}