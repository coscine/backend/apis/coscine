﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object (DTO) for accepting the terms of service by a user.
/// </summary>
public record UserTermsOfServiceAcceptDto
{
    /// <summary>
    /// The version of the terms of service being accepted.
    /// </summary>
    /// <remarks>
    /// This field holds the version of the terms of service that the user is accepting.
    /// It is a required field to proceed with the terms of service acceptance.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string Version { get; init; }
}