﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object (DTO) used for creating a resource.
/// Inherits properties from <see cref="ResourceForManipulationDto"/>.
/// </summary>
public record ResourceForCreationDto : ResourceForManipulationDto
{
    /// <summary>
    /// The identifier of the resource type associated with the resource.
    /// </summary>
    /// <remarks>
    /// This field contains the identifier of the resource type linked to the resource being created.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Guid ResourceTypeId { get; init; }

    /// <summary>
    /// The application profile for the resource creation.
    /// </summary>
    /// <remarks>
    /// This field holds the application profile used for creating the resource.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required ApplicationProfileForResourceCreationDto ApplicationProfile { get; init; }

    /// <summary>
    /// The resource type options for the creation of the resource.
    /// </summary>
    /// <remarks>
    /// This field contains the resource type options used during the creation of the resource.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required ResourceTypeOptionsForCreationDto ResourceTypeOptions { get; init; }
}