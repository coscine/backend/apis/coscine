﻿using Coscine.Api.Core.Shared.Validators;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Abstract base class representing a data transfer object (DTO) for project manipulation.
/// </summary>
public abstract record ProjectForManipulationDto
{
    /// <summary>
    /// Gets or initializes the name of the project.
    /// </summary>
    /// <remarks>
    /// The Name field is required and has a maximum length of 200 characters for the project.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [MaxLength(200, ErrorMessage = "Maximum length for {0} is {1} characters.")]
    public required string Name { get; init; }

    /// <summary>
    /// Gets or initializes the description of the project.
    /// </summary>
    /// <remarks>
    /// The Description field is required and has a maximum length of 10000 characters for the project.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [MaxLength(10000, ErrorMessage = "Maximum length for {0} is {1} characters.")]
    public required string Description { get; init; }

    /// <summary>
    /// Gets or initializes the start date of the project.
    /// </summary>
    public DateTime StartDate { get; init; }

    /// <summary>
    /// Gets or initializes the end date of the project.
    /// </summary>
    public DateTime EndDate { get; init; }

    /// <summary>
    /// Gets or initializes the keywords associated with the project.
    /// </summary>
    [MaxTotalLength(1000, ErrorMessage = "Maximum total length for {0} is {1} characters.")]
    public IEnumerable<string>? Keywords { get; init; }

    /// <summary>
    /// Gets or initializes the display name of the project.
    /// </summary>
    [MaxLength(25, ErrorMessage = "Maximum length for {0} is {1} characters.")]
    public string? DisplayName { get; init; }

    /// <summary>
    /// Gets or initializes the principal investigators associated with the project.
    /// </summary>
    [MaxLength(500, ErrorMessage = "Maximum length for {0} is {1} characters.")]
    public string? PrincipleInvestigators { get; init; }

    /// <summary>
    /// Gets or initializes the grant ID associated with the project.
    /// </summary>
    [MaxLength(500, ErrorMessage = "Maximum length for {0} is {1} characters.")]
    public string? GrantId { get; init; }

    /// <summary>
    /// Gets or initializes the visibility settings for the project.
    /// </summary>
    /// <remarks>
    /// The Visibility field is required for defining the visibility settings of the project.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required VisibilityForProjectManipulationDto Visibility { get; init; }

    /// <summary>
    /// Gets or initializes the disciplines associated with the project.
    /// </summary>
    /// <remarks>
    /// The Disciplines field is required for defining the disciplines related to the project.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required IEnumerable<DisciplineForProjectManipulationDto> Disciplines { get; init; }

    /// <summary>
    /// Gets or initializes the organizations associated with the project.
    /// </summary>
    /// <remarks>
    /// The Organizations field is required for defining the organizations related to the project.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [ValidOrganizations]
    public required IEnumerable<OrganizationForProjectManipulationDto> Organizations { get; init; }
}