﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object (DTO) for manipulating the visibility of a resource.
/// </summary>
public record VisibilityForResourceManipulationDto
{
    /// <summary>
    /// The unique identifier of the visibility setting.
    /// </summary>
    /// <remarks>
    /// This field holds the unique identifier representing the visibility setting for a resource.
    /// It is a required field for manipulating the resource's visibility.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Guid Id { get; init; }
}