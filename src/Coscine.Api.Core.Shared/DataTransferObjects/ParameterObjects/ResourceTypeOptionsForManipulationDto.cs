﻿using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ResourceTypeOptions;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object (DTO) for manipulating options related to any resource type.
/// </summary>
public record ResourceTypeOptionsForManipulationDto
{
    /// <summary>
    /// Options for manipulating Linked Data resource types.
    /// </summary>
    public LinkedDataResourceTypeOptionsForManipulationDto? LinkedResourceTypeOptions { get; init; }

    /// <summary>
    /// Options for manipulating GitLab resource types.
    /// </summary>
    public GitlabResourceTypeOptionsForManipulationDto? GitlabResourceTypeOptions { get; init; }

    /// <summary>
    /// Options for manipulating RDS resource types.
    /// </summary>
    public RdsResourceTypeOptionsForManipulationDto? RdsResourceTypeOptions { get; init; }

    /// <summary>
    /// Options for manipulating RDS S3 resource types.
    /// </summary>
    public RdsS3ResourceTypeOptionsForManipulationDto? RdsS3ResourceTypeOptions { get; init; }

    /// <summary>
    /// Options for manipulating RDS S3 Worm resource types.
    /// </summary>
    public RdsS3WormResourceTypeOptionsForManipulationDto? RdsS3WormResourceTypeOptions { get; init; }
}