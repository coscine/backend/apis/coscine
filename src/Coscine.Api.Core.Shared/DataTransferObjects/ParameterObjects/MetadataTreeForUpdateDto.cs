﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the update of a metadata tree.
/// Inherits from the base class <see cref="MetadataTreeForManipulationDto"/>.
/// </summary>
public record MetadataTreeForUpdateDto : MetadataTreeForManipulationDto
{
    /// <summary>
    /// Gets or initializes the RDF definition associated with the metadata tree update.
    /// </summary>
    /// <remarks>
    /// The Definition field is required for updating the metadata tree.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required RdfDefinitionForManipulationDto Definition { get; init; }
}