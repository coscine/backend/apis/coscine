﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ResourceTypeOptions;

/// <summary>
/// Represents the data transfer object (DTO) for manipulating GitLab resource type options.
/// </summary>
public abstract record GitlabResourceTypeOptionsForManipulationDto
{
    /// <summary>
    /// The branch associated with the GitLab resource.
    /// </summary>
    /// <remarks>
    /// This field holds the branch name for the GitLab resource and is required for manipulating GitLab resource type options.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string Branch { get; init; }

    /// <summary>
    /// The access token for authentication with GitLab.
    /// </summary>
    /// <remarks>
    /// This field contains the access token required for authentication with GitLab and is required for manipulating GitLab resource type options.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string AccessToken { get; init; }
}