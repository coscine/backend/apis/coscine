﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ResourceTypeOptions;

/// <summary>
/// Represents the data transfer object (DTO) for manipulating RDS S3 resource type options.
/// </summary>
public record RdsS3ResourceTypeOptionsForManipulationDto
{
    /// <summary>
    /// The quota information associated with the RDS S3 resource type.
    /// </summary>
    /// <remarks>
    /// This field holds the quota details for the RDS S3 resource type and is required for manipulating RDS S3 resource type options.
    /// </remarks>
    public required QuotaForManipulationDto Quota { get; init; }
}