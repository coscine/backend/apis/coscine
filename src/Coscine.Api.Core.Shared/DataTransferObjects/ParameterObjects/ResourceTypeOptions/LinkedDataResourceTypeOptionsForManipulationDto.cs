﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ResourceTypeOptions;

/// <summary>
/// Represents the data transfer object (DTO) for manipulating linked data resource type options.
/// </summary>
public record LinkedDataResourceTypeOptionsForManipulationDto
{ }