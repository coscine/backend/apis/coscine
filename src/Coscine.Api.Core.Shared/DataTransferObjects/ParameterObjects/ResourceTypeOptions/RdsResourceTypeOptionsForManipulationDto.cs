﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ResourceTypeOptions;

/// <summary>
/// Represents the data transfer object (DTO) for manipulating RDS Web resource type options.
/// </summary>
public record RdsResourceTypeOptionsForManipulationDto
{
    /// <summary>
    /// The quota information associated with the RDS Web resource type.
    /// </summary>
    /// <remarks>
    /// This field holds the quota details for the RDS Web resource type and is required for manipulating RDS Web resource type options.
    /// </remarks>
    public required QuotaForManipulationDto Quota { get; init; }
}