﻿using Coscine.Api.Core.Shared.Validators;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ResourceTypeOptions;

/// <summary>
/// Represents the data transfer object (DTO) for creating GitLab resource type options.
/// </summary>
public record GitlabResourceTypeOptionsForCreationDto : GitlabResourceTypeOptionsForManipulationDto
{
    /// <summary>
    /// The repository URL for the GitLab resource.
    /// </summary>
    /// <remarks>
    /// This field holds the absolute URL of the GitLab repository and is required for creating the GitLab resource type options.
    /// It must contain the entire web address ('https://' or 'http://').
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [AbsoluteUri(ErrorMessage = "Provided GitLab {0} is not absolute. Make sure the URL contains the entire web address of the page ('https://' or 'http://').")]
    public required Uri RepoUrl { get; init; }

    /// <summary>
    /// The project ID associated with the GitLab resource.
    /// </summary>
    /// <remarks>
    /// This field represents the project ID for the GitLab resource and is required for creating the GitLab resource type options.
    /// It must be a positive whole number.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [Range(1, int.MaxValue, ErrorMessage = "The {0} must be a positive whole number.")]
    public required int ProjectId { get; init; }

    /// <summary>
    /// Indicates whether the terms of service for the GitLab resource are accepted.
    /// </summary>
    /// <remarks>
    /// This field specifies whether the terms of service for the GitLab resource are accepted and is required for creating the GitLab resource type options.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required bool TosAccepted { get; init; }
}