﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects.ResourceTypeOptions;

/// <summary>
/// Represents the data transfer object (DTO) for updating GitLab resource type options.
/// </summary>
public record GitlabResourceTypeOptionsForUpdateDto : GitlabResourceTypeOptionsForManipulationDto
{ }