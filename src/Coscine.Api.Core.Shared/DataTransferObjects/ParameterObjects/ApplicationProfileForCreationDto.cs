﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the creation of an application profile.
/// </summary>
public record ApplicationProfileForCreationDto
{
    /// <summary>
    /// Gets or initializes the name of the application profile.
    /// </summary>
    /// <remarks>
    /// The Name field is required for the application profile creation.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string Name { get; init; }

    /// <summary>
    /// Gets or initializes the URI of the application profile.
    /// </summary>
    /// <remarks>
    /// The Uri field is required for the application profile creation.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Uri Uri { get; init; }

    /// <summary>
    /// Gets or initializes the RDF definition associated with the application profile.
    /// </summary>
    /// <remarks>
    /// The Definition field is required for the application profile creation.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required RdfDefinitionForManipulationDto Definition { get; init; }
}