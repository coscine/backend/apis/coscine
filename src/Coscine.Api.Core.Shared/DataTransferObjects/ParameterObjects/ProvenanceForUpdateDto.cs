﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the update of provenance
/// Inherits from the base class <see cref="ProvenanceParametersDto"/>.
/// </summary>
public class ProvenanceForUpdateDto : ProvenanceParametersDto
{
    /// <summary>
    /// Gets or sets the id of the specific metadata tree.
    /// </summary>
    public required Uri Id { get; init; }
}
