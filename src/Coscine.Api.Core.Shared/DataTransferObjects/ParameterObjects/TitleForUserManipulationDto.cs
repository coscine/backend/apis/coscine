﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object (DTO) for manipulating a user's title.
/// </summary>
public record TitleForUserManipulationDto
{
    /// <summary>
    /// The unique identifier of the title.
    /// </summary>
    /// <remarks>
    /// This field holds the unique identifier of the title and is a required field for user manipulation.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Guid Id { get; init; }
}