﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing Hash Parameters in a request.
/// </summary>
public class HashParametersDto
{
    /// <summary>
    /// Gets or initializes the hash algorithm name.
    /// </summary>
    /// <remarks>
    /// The AlgorithmName field is required for the hash parameters request.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string AlgorithmName { get; init; }

    /// <summary>
    /// Gets or initializes the hash value.
    /// </summary>
    /// <remarks>
    /// The Value field is required for the hash parameters request.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string Value { get; init; }
}
