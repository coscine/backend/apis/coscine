﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object (DTO) used for updating a resource.
/// Inherits properties from <see cref="ResourceForManipulationDto"/>.
/// </summary>
public record ResourceForUpdateDto : ResourceForManipulationDto
{
    /// <summary>
    /// Indicates whether the resource is archived.
    /// </summary>
    /// <remarks>
    /// This field determines whether the resource is archived (true) or not (false).
    /// It is a required field for updating the resource.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required bool Archived { get; init; }

    /// <summary>
    /// Updated resource type options for the resource.
    /// </summary>
    /// <remarks>
    /// This field contains the updated resource type options, if provided, for the resource update.
    /// </remarks>
    public ResourceTypeOptionsForUpdateDto? ResourceTypeOptions { get; init; }

    /// <summary>
    /// Indicates whether the resource is in maintenance mode.
    /// </summary>
    /// <remarks>Setting can by changed only by admin users. Corresponding checks are done in the service layer.</remarks>
    public bool? MaintenanceMode { get; init; }
}