﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

public record HandleForUpdateDto : HandleForManipulationDto
{
    public required IEnumerable<HandleValueForUpdateDto> Values { get; init; }
}