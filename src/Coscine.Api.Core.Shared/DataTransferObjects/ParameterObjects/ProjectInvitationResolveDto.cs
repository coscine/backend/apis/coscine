﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) for resolving a project invitation.
/// </summary>
public record ProjectInvitationResolveDto
{
    /// <summary>
    /// Gets or initializes the token associated with resolving the project invitation.
    /// </summary>
    /// <remarks>
    /// The Token field is required for resolving the project invitation.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Guid Token { get; init; }
}