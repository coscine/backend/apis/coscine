﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing a PID request.
/// </summary>
public record PidRequestDto
{
    /// <summary>
    /// Gets or initializes the name associated with the PID request.
    /// </summary>
    /// <remarks>
    /// The Name field is required for the PID request.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string Name { get; init; }

    /// <summary>
    /// Gets or initializes the email associated with the PID request.
    /// </summary>
    /// <remarks>
    /// The Email field is required and must follow a valid email format for the PID request.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [EmailAddress(ErrorMessage = "Invalid email format.")]
    public required string Email { get; init; }

    /// <summary>
    /// Gets or initializes the message for the PID request.
    /// </summary>
    /// <remarks>
    /// The Message field is required and has a maximum length of 5000 characters for the PID request.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [MaxLength(5000, ErrorMessage = "Maximum length for {0} is {1} characters.")]
    public required string Message { get; init; }

    /// <summary>
    /// Gets or initializes whether to send a confirmation email for the PID request.
    /// </summary>
    public bool SendConfirmationEmail { get; init; }
}