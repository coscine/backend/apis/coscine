﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object (DTO) used for updating a resource quota.
/// </summary>
public record ResourceQuotaForUpdateDto
{
    /// <summary>
    /// The new reserved quota value to set for the selected resource in GiB.
    /// </summary>
    /// <remarks>
    /// This field holds the new reserved quota value in GiB for the selected resource.
    /// </remarks>
    public required QuotaForManipulationDto Reserved { get; set; }
}