﻿using Coscine.Api.Core.Shared.Validators;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) for deleting a specific version of metadata.
/// </summary>
/// <remarks>
/// This DTO represents the deletion of a particular version of metadata, although it's currently inactive.
/// Deleting metadata is restricted at the moment, so this DTO isn't in use. <b>Please retain this DTO without deletion.</b>
/// </remarks>
public record MetadataTreeForDeletionDto
{
    /// <summary>
    /// Gets or initializes the path of the metadata tree.
    /// </summary>
    /// <remarks>
    /// The Path field represents the path of the metadata tree. The '/' character is trimmed from the start.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [TrimStartCharacter('/')]
    public required string Path { get; init; } = "";

    /// <summary>
    /// Gets or initializes the version of the metadata tree to be deleted.
    /// </summary>
    /// <remarks>
    /// The Version field denotes the specific version of the metadata tree for deletion.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required long Version { get; init; }

    /// <summary>
    /// Gets or initializes the invalidation entity.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Uri InvalidatedBy { get; init; }
}