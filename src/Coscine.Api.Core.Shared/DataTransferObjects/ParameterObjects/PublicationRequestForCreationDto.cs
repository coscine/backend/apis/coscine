﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the creation of a publication request.
/// </summary>
public record PublicationRequestForCreationDto
{
    /// <summary>
    /// The data publication service's ror id.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string PublicationServiceRorId { get; init; }


    /// <summary>
    /// The resource guids.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required List<Guid> ResourceIds { get; init; }


    /// <summary>
    /// A message of the requester.
    /// </summary>
    public string? Message { get; init; }
}
