﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing a discipline for resource manipulation.
/// </summary>
public record DisciplineForResourceManipulationDto
{
    /// <summary>
    /// Gets or initializes the identifier of the discipline.
    /// </summary>
    /// <remarks>
    /// The Id field is required for manipulation of the discipline associated with a resource.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Guid Id { get; init; }
}