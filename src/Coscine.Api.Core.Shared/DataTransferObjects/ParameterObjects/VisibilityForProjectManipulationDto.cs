﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object (DTO) for manipulating the visibility of a project.
/// </summary>
public record VisibilityForProjectManipulationDto
{
    /// <summary>
    /// The unique identifier of the visibility setting.
    /// </summary>
    /// <remarks>
    /// This field holds the unique identifier representing the visibility setting for a project.
    /// It is a required field for manipulating the project's visibility.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Guid Id { get; init; }
}