﻿using Coscine.Api.Core.Shared.Validators;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the creation of an application profile for a resource.
/// </summary>
public record ApplicationProfileForResourceCreationDto
{
    /// <summary>
    /// Gets or initializes the URI of the resource for the application profile.
    /// </summary>
    /// <remarks>
    /// The Uri field is required for creating the application profile and has a maximum length of 500 characters.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [MaxLengthForUri(500, ErrorMessage = "Maximum length for {0} is {1} characters.")]
    public required Uri Uri { get; init; }
}