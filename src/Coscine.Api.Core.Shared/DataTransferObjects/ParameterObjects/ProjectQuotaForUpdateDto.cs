﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the update of project quotas.
/// </summary>
public record ProjectQuotaForUpdateDto
{
    /// <summary>
    /// Gets or initializes the allocated quota for the project.
    /// </summary>
    /// <remarks>
    /// The Allocated field represents the allocated quota for the project and is a required field for quota updates.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required QuotaForManipulationDto Allocated { get; init; }

    /// <summary>
    /// Gets or initializes the maximum quota for the project.
    /// </summary>
    /// <remarks>
    /// The Maximum field represents the maximum quota for the project.
    /// The user must be an authorized <b>Quota Admin</b> to update the maximum quota.
    /// Set this value to <c>null</c> if you lack the required permission.
    /// </remarks>
    public QuotaForManipulationDto? Maximum { get; init; }
}