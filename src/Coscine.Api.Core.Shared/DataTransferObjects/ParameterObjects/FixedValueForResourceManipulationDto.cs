﻿using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the manipulation of a fixed value associated with a resource.
/// </summary>
public record FixedValueForResourceManipulationDto
{
    /// <summary>
    /// Gets or initializes the value of the fixed resource value.
    /// </summary>
    [JsonPropertyName("value")]
    public string? Value { get; init; }

    /// <summary>
    /// Gets or initializes the type of the fixed resource value.
    /// </summary>
    [JsonPropertyName("type")]
    public string? Type { get; init; }

    /// <summary>
    /// Gets or initializes the data type URI of the fixed resource value.
    /// </summary>
    [JsonPropertyName("datatype")]
    public Uri? DataType { get; init; }

    /// <summary>
    /// The target class of the provided value (e.g., "https://purl.org/coscine/ap/base/")
    /// </summary>
    [JsonPropertyName("targetClass")]
    public string? TargetClass { get; init; }
}