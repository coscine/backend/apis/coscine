﻿using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing updating a handle.
/// </summary>
public record HandleValueForUpdateDto : HandleValueForManipulationDto
{
    /// <summary>
    /// The id of the handle.
    /// </summary>
    [JsonPropertyName("idx")]
    public int Idx { get; init; }
}