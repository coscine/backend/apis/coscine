﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing an invitation for project manipulation.
/// </summary>
public record ProjectInvitationForProjectManipulationDto
{
    /// <summary>
    /// Gets or initializes the identifier of the role associated with the invitation.
    /// </summary>
    /// <remarks>
    /// The RoleId field is required for defining the role associated with the project invitation.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Guid RoleId { get; init; }

    /// <summary>
    /// Gets or initializes the email address of the invited user.
    /// </summary>
    /// <remarks>
    /// The Email field is required and must follow a valid email format for the project invitation.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [DataType(DataType.EmailAddress)]
    [EmailAddress(ErrorMessage = "Invalid email format.")]
    public required string Email { get; init; }
}