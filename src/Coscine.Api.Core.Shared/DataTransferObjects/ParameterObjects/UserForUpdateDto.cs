﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object (DTO) for updating user details.
/// </summary>
public record UserForUpdateDto
{
    /// <summary>
    /// The user's given name.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string GivenName { get; init; }

    /// <summary>
    /// The user's family name.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string FamilyName { get; init; }

    /// <summary>
    /// The user's email address.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    [EmailAddress]
    public required string Email { get; init; }

    /// <summary>
    /// The user's title information for manipulation.
    /// </summary>
    /// <remarks>
    /// This field represents the user's title information and can be null if not provided.
    /// </remarks>
    public TitleForUserManipulationDto? Title { get; init; }

    /// <summary>
    /// The user's preferred language for manipulation.
    /// </summary>
    /// <remarks>
    /// This field specifies the user's preferred language and is a required field for the update.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required LanguageForUserManipulationDto Language { get; init; }

    /// <summary>
    /// The user's organization.
    /// </summary>
    /// <remarks>
    /// This field holds the user's organization details and can be null if not provided.
    /// </remarks>
    public string? Organization { get; init; }

    /// <summary>
    /// The disciplines associated with the user for manipulation.
    /// </summary>
    /// <remarks>
    /// This field contains the disciplines associated with the user and is a required field for the update.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required IEnumerable<DisciplineForUserManipulationDto> Disciplines { get; init; }
}