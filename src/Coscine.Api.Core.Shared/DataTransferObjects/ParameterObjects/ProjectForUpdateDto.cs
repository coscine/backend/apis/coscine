﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing the update of a project.
/// Inherits from the base class <see cref="ProjectForManipulationDto"/>.
/// </summary>
public record ProjectForUpdateDto : ProjectForManipulationDto
{
    /// <summary>
    /// Gets or initializes the slug associated with the project for identification purposes.
    /// </summary>
    /// <remarks>
    /// The Slug field is required and has a maximum length of 63 characters for the project update.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    [MaxLength(63, ErrorMessage = "Maximum length for {0} is {1} characters.")]
    public required string Slug { get; init; }
}