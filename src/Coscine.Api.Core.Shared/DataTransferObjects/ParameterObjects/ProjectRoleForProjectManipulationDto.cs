﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing a project role for manipulation within a project.
/// </summary>
public record ProjectRoleForProjectManipulationDto
{
    /// <summary>
    /// Gets or initializes the identifier of the role associated with the project manipulation.
    /// </summary>
    /// <remarks>
    /// The RoleId field is required for defining the role linked to the project manipulation within the project.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Guid RoleId { get; init; }
}