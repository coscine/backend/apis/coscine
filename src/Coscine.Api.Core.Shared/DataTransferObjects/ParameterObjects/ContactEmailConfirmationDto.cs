﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing user email confirmation.
/// </summary>
public record UserEmailConfirmationDto
{
    /// <summary>
    /// Gets or initializes the confirmation token for user email confirmation.
    /// </summary>
    /// <remarks>
    /// The ConfirmationToken field is required for confirming user email.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Guid ConfirmationToken { get; init; }
}