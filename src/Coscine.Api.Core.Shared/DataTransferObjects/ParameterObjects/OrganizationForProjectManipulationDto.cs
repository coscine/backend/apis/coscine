﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing an organization for project manipulation.
/// </summary>
public record OrganizationForProjectManipulationDto
{
    /// <summary>
    /// Gets or initializes the URI of the organization.
    /// </summary>
    /// <remarks>
    /// The Uri field represents the URI of the organization and is a required field for project manipulation.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Uri Uri { get; init; } = null!;

    /// <summary>
    /// Gets or initializes if the organization is responsible.
    /// </summary>
    /// <remarks>
    /// The Uri field represents the URI of the organization and is a required field for project manipulation.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required bool Responsible { get; init; }
}