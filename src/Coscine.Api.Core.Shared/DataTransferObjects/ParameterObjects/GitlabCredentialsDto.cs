﻿using Coscine.Api.Core.Shared.Validators;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Represents the data transfer object containing the credentials required for accessing a GitLab provider.
/// </summary>
public record GitlabCredentialsDto
{
    /// <summary>
    /// Domain/Host of the GitLab Provider.
    /// </summary>
    /// <remarks>
    /// The Domain field represents the URL of the GitLab instance. It should be an absolute URI.
    /// </remarks>
    /// <example>https://git.rwth-aachen.de/</example>
    [Required(ErrorMessage = "{0} is a required field.")]
    [AbsoluteUri(ErrorMessage = "Provided GitLab {0} is not absolute. Make sure the URL contains the entire web address of the page ('https://' or 'http://').")]
    public required Uri Domain { get; init; }

    /// <summary>
    /// GitLab Project or Group Access Token.
    /// </summary>
    /// <remarks>
    /// The AccessToken field represents the access token required for accessing GitLab projects or groups.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string AccessToken { get; init; }
}