﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

/// <summary>
/// Data transfer object (DTO) representing a discipline for user manipulation.
/// </summary>
public record DisciplineForUserManipulationDto
{
    /// <summary>
    /// Gets or initializes the identifier of the discipline.
    /// </summary>
    /// <remarks>
    /// The Id field is required for manipulation of the discipline related to a user.
    /// </remarks>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required Guid Id { get; init; }
}