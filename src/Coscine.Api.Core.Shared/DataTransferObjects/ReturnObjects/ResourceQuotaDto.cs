﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) containing quota information for a resource.
/// </summary>
public record ResourceQuotaDto
{
    /// <summary>
    /// The minimal information about the resource.
    /// </summary>
    public required ResourceMinimalDto Resource { get; init; }

    /// <summary>
    /// The percentage of quota used by the resource.
    /// </summary>
    public float? UsedPercentage { get; set; }

    /// <summary>
    /// The amount of quota used by the resource.
    /// </summary>
    public QuotaDto? Used { get; set; }

    /// <summary>
    /// The reserved quota for the resource.
    /// </summary>
    public QuotaDto? Reserved { get; set; }
}