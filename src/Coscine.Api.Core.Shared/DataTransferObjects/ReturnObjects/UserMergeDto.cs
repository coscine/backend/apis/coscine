﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for merging user accounts.
/// </summary>
public record UserMergeDto
{
    /// <summary>
    /// The token required for merging user accounts.
    /// </summary>
    public required string Token { get; init; }
}