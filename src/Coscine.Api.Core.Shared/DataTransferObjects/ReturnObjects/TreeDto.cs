﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for tree-related information.
/// </summary>
public record TreeDto
{
    /// <summary>
    /// The path of the tree item.
    /// </summary>
    public string Path { get; init; } = null!;

    /// <summary>
    /// The type of data in the tree.
    /// </summary>
    public TreeDataType Type { get; init; }
}