namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for a role.
/// </summary>
public record RoleDto
{
    /// <summary>
    /// The unique identifier of the role.
    /// </summary>
    public required Guid Id { get; init; }

    /// <summary>
    /// The display name of the role.
    /// </summary>
    public required string DisplayName { get; init; }

    /// <summary>
    /// The description of the role.
    /// </summary>
    public required string Description { get; init; }
}