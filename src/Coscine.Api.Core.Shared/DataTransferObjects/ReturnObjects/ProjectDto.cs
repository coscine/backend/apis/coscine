﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a data transfer object (DTO) for project information.
/// </summary>
public record ProjectDto
{
    /// <summary>
    /// Unique identifier for the project.
    /// </summary>
    public required Guid Id { get; init; }

    /// <summary>
    /// Persistent identifier for the project.
    /// </summary>
    public required string Pid { get; set; }

    /// <summary>
    /// Name of the project.
    /// </summary>
    public required string Name { get; init; } = null!;

    /// <summary>
    /// Description of the project.
    /// </summary>
    public required string Description { get; init; } = null!;

    /// <summary>
    /// Start date of the project.
    /// </summary>
    public required DateTime StartDate { get; init; }

    /// <summary>
    /// End date of the project.
    /// </summary>
    public required DateTime EndDate { get; init; }

    /// <summary>
    /// Collection of keywords associated with the project.
    /// </summary>
    public IEnumerable<string>? Keywords { get; init; }

    /// <summary>
    /// Display name of the project.
    /// </summary>
    public string? DisplayName { get; init; }

    /// <summary>
    /// Principal investigators involved in the project.
    /// </summary>
    public string? PrincipleInvestigators { get; init; }

    /// <summary>
    /// Grant ID associated with the project.
    /// </summary>
    public string? GrantId { get; init; }

    /// <summary>
    /// Visibility settings for the project.
    /// </summary>
    public required VisibilityDto Visibility { get; init; } = null!;

    /// <summary>
    /// Disciplines related to the project.
    /// </summary>
    public required IEnumerable<DisciplineDto> Disciplines { get; init; } = null!;

    /// <summary>
    /// Organizations associated with the project.
    /// </summary>
    public required IEnumerable<ProjectOrganizationDto> Organizations { get; set; } = null!;

    /// <summary>
    /// Slug for the project.
    /// </summary>
    public required string Slug { get; init; } = null!;

    /// <summary>
    /// Creator of the project.
    /// </summary>
    public UserMinimalDto? Creator { get; init; }

    /// <summary>
    /// Date of creation of the project.
    /// </summary>
    public DateTime? CreationDate { get; init; }

    /// <summary>
    /// Collection of sub-projects associated with this project.
    /// </summary>
    public IEnumerable<ProjectDto>? SubProjects { get; set; }

    /// <summary>
    /// Parent project details if this project is a sub-project.
    /// </summary>
    public ProjectMinimalDto? Parent { get; init; }
}