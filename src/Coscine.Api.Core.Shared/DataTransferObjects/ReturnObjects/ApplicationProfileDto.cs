﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents an application profile data transfer object.
/// </summary>
public record ApplicationProfileDto
{
    /// <summary>
    /// The URI associated with the application profile.
    /// </summary>
    public required Uri Uri { get; init; }

    /// <summary>
    /// The display name for the application profile.
    /// </summary>
    public string? DisplayName { get; init; }

    /// <summary>
    /// The description of the application profile.
    /// </summary>
    public string? Description { get; init; }

    /// <summary>
    /// The RDF definition associated with the application profile.
    /// </summary>
    public RdfDefinitionDto? Definition { get; init; }
}