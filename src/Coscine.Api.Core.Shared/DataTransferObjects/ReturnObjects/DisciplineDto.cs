﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents the data transfer object for a discipline.
/// </summary>
public record DisciplineDto
{
    /// <summary>
    /// The unique identifier for the discipline.
    /// </summary>
    public Guid Id { get; init; }

    /// <summary>
    /// The URI associated with the discipline.
    /// </summary>
    public string Uri { get; init; } = null!;

    /// <summary>
    /// The display name of the discipline in German.
    /// </summary>
    public string DisplayNameDe { get; init; } = null!;

    /// <summary>
    /// The display name of the discipline in English.
    /// </summary>
    public string DisplayNameEn { get; init; } = null!;
}