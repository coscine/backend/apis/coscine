﻿using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a data transfer object (DTO) for provenance information.
/// </summary>
public partial class ProvenanceDto
{
    /// <summary>
    /// Gets or sets the id of the current metadata graph.
    /// </summary>
    public required Uri Id { get; init; }

    /// <summary>
    /// Gets or sets the date and time when the metadata was generated.
    /// </summary>
    public DateTime? GeneratedAt { get; set; }

    /// <summary>
    /// Gets or sets the adapted versions from the specific metadata tree.
    /// </summary>
    public required IEnumerable<Uri> WasRevisionOf { get; init; }

    /// <summary>
    /// Gets or sets the variants of the specific metadata tree.
    /// </summary>
    public required IEnumerable<VariantDto> Variants { get; init; }

    /// <summary>
    /// The similarity to the last version.
    /// </summary>
    public decimal? SimilarityToLastVersion { get; init; }

    /// <summary>
    /// Information if the specific metadata tree was invalidated by something.
    /// </summary>
    public Uri? WasInvalidatedBy { get; init; }

    /// <summary>
    /// Gets or initializes the hash parameters of the metadata.
    /// </summary>
    public HashParametersDto? HashParameters { get; init; }
}
