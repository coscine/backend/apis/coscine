namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a data transfer object (DTO) for an organization in the context of a project.
/// </summary>
public record ProjectOrganizationDto : OrganizationDto
{
    /// <summary>
    /// Determines if the organization is Responsible for a given project.
    /// </summary>
    public required bool Responsible { get; set; }
}