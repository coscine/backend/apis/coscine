﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for administrative purposes with additional resource details.
/// </summary>
public record ResourceAdminDto : ResourceDto
{
    /// <summary>
    /// Indicates whether the resource is deleted.
    /// </summary>
    public required bool Deleted { get; init; }

    /// <summary>
    /// Collection of minimal project resource details associated with this resource.
    /// </summary>
    public required IEnumerable<ProjectResourceMinimalDto> ProjectResources { get; init; }

    /// <summary>
    /// Collection of resource quota details associated with this resource.
    /// </summary>
    public ResourceQuotaDto ResourceQuota { get; set; } = null!;
}