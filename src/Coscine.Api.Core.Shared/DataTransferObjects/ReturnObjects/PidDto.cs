﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a data transfer object (DTO) for a PID (Persistent Identifier).
/// </summary>
public record PidDto
{
    /// <summary>
    /// Gets or sets the prefix of the PID.
    /// </summary>
    /// <value>
    /// A required string representing the prefix of the PID.
    /// </value>
    public required string Prefix { get; init; }

    /// <summary>
    /// Gets or sets the suffix of the PID.
    /// </summary>
    /// <value>
    /// A required string representing the suffix of the PID.
    /// </value>
    public required string Suffix { get; init; }

    /// <summary>
    /// Gets the constructed PID by combining the prefix and suffix ("{Prefix}/{Suffix}").
    /// </summary>
    /// <value>
    /// The constructed PID string.
    /// </value>
    public string Identifier => $"{Prefix}/{Suffix}";

    /// <summary>
    /// Gets or sets the type of the PID.
    /// </summary>
    /// <value>
    /// An optional enumeration representing the type of the PID (e.g., PidType.Project, PidType.Resource).
    /// </value>
    public PidType? Type { get; init; }

    /// <summary>
    /// Gets or sets a value indicating whether the linked entity is considered valid.
    /// </summary>
    /// <value>
    /// <c>true</c> if the linked entity is valid; otherwise, <c>false</c>.
    /// </value>
    public bool IsEntityValid { get; init; }
}
