﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a data transfer object (DTO) containing quota information for a project.
/// </summary>
public record ProjectQuotaDto
{
    /// <summary>
    /// Identifier of the project.
    /// </summary>
    public Guid ProjectId { get; set; }

    /// <summary>
    /// Total space used by all resources [GiB].
    /// </summary>
    public QuotaDto TotalUsed { get; set; } = null!;

    /// <summary>
    /// Total space reserved by all resources [GiB].
    /// </summary>
    public QuotaDto TotalReserved { get; set; } = null!;

    /// <summary>
    /// Allocated space available to be taken by resources [GiB].
    /// </summary>
    public QuotaDto Allocated { get; set; } = null!;

    /// <summary>
    /// Maximum space that can be taken by resources [GiB].
    /// </summary>
    public QuotaDto Maximum { get; set; } = null!;

    /// <summary>
    /// The type of resource.
    /// </summary>
    public ResourceTypeMinimalDto ResourceType { get; set; } = null!;

    /// <summary>
    /// Quota information for individual resources of the selected project's resource type.
    /// </summary>
    public IEnumerable<ResourceQuotaDto>? ResourceQuotas { get; set; }
}