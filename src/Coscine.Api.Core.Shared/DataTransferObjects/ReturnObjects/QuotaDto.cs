﻿using Coscine.Api.Core.Shared.Enums;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for quota values.
/// </summary>
public record QuotaDto
{
    /// <summary>
    /// The value of the quota.
    /// </summary>
    [Required] // Annotation needed for api-client. Otherwise property can be undefined.
    public required float Value { get; set; }

    /// <summary>
    /// The unit of the quota value.
    /// </summary>
    [Required] // Annotation needed for api-client. Otherwise property can be undefined.
    public required QuotaUnit Unit { get; set; }
}