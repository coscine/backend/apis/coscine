﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
public record HandleDto
{
    public PidDto Pid { get; init; }

    public IEnumerable<HandleValueDto> Values { get; init; }
}