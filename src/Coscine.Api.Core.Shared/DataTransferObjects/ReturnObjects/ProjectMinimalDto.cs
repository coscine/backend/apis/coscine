﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a minimal data transfer object (DTO) for a project.
/// </summary>
public record ProjectMinimalDto
{
    /// <summary>
    /// Unique identifier for the project.
    /// </summary>
    public required Guid Id { get; init; }
}