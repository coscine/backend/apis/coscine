﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for a file within a tree structure, extending the base TreeDto.
/// </summary>
public record FileTreeDto : TreeDto
{
    /// <summary>
    /// Gets or sets the directory of the file.
    /// </summary>
    /// <value>The directory of the file.</value>
    public string Directory { get; init; } = null!;

    /// <summary>
    /// Gets or sets the name of the file.
    /// </summary>
    /// <value>The name of the file.</value>
    public string Name { get; init; } = null!;

    /// <summary>
    /// Gets or sets the extension of the file.
    /// </summary>
    /// <value>The extension of the file.</value>
    public string Extension { get; init; } = null!;

    /// <summary>
    /// Gets or sets the size of the file in bytes.
    /// </summary>
    /// <value>The size of the file in bytes.</value>
    public long Size { get; init; }

    /// <summary>
    /// Gets or sets the creation date of the file.
    /// </summary>
    /// <value>The creation date of the file, or <c>null</c> if not available.</value>
    public DateTime? CreationDate { get; init; }

    /// <summary>
    /// Gets or sets the last change date of the file.
    /// </summary>
    /// <value>The last change date of the file, or <c>null</c> if not available.</value>
    public DateTime? ChangeDate { get; init; }

    /// <summary>
    /// Gets or sets the details for file actions associated with this file.
    /// </summary>
    /// <value>The file actions details, or <c>null</c> if no actions are specified.</value>
    public FileActionsDto? Actions { get; set; }

    /// <summary>
    /// Determines if the specific tree entry is hidden.
    /// </summary>
    /// <value>Hidden or not.</value>
    public bool Hidden { get; set; }
}