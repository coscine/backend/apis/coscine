﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for deployed graph details.
/// </summary>
public record DeployedGraphDto
{
    /// <summary>
    /// Graph URI and identifier.
    /// </summary>
    public Uri Uri { get; set; } = null!;

    /// <summary>
    /// Collection of file hashes associated with the graph.
    /// </summary>
    public IEnumerable<string> FileHashes { get; set; } = [];
}