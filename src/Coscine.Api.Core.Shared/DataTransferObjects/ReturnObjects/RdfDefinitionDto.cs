﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for RDF (Resource Description Framework) definition details.
/// </summary>
public record RdfDefinitionDto
{
    /// <summary>
    /// The content of the RDF definition.
    /// </summary>
    public string? Content { get; init; }

    /// <summary>
    /// The type of the RDF definition.
    /// </summary>
    public string? Type { get; init; }
}