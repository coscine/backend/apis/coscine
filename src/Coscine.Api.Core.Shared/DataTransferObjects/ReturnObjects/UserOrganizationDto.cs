﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for user-related organization information, inheriting from OrganizationDto.
/// </summary>
public record UserOrganizationDto : OrganizationDto
{
    /// <summary>
    /// Determines if the organization's details can be modified.
    /// </summary>
    /// <value><c>true</c> if the organization is read-only; otherwise, <c>false</c>.</value>
    /// <remarks>
    /// This property defaults to <c>true</c> and can be manually set to <c>false</c>.
    /// For entries set by Shibboleth, this property is automatically set to <c>true</c>.
    /// </remarks>
    public required bool ReadOnly { get; init; }
}