﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a minimal Data Transfer Object (DTO) for user information.
/// </summary>
public record UserMinimalDto
{
    /// <summary>
    /// The unique identifier for the user.
    /// </summary>
    public required Guid Id { get; init; }
}