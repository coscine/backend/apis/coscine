namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a data transfer object (DTO) for project invitations.
/// </summary>
public record ProjectInvitationDto
{
    /// <summary>
    /// Unique identifier for the invitation.
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Expiration date of the invitation.
    /// </summary>
    public DateTime ExpirationDate { get; set; }

    /// <summary>
    /// Email associated with the invitation.
    /// </summary>
    public string Email { get; set; } = null!;

    /// <summary>
    /// Issuer of the invitation.
    /// </summary>
    public PublicUserDto Issuer { get; set; } = null!;

    /// <summary>
    /// Project related to the invitation.
    /// </summary>
    public ProjectMinimalDto Project { get; set; } = null!;

    /// <summary>
    /// Role assigned for the invitation within the project.
    /// </summary>
    public RoleMinimalDto Role { get; set; } = null!;
}