﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for visibility settings.
/// </summary>
public record VisibilityDto
{
    /// <summary>
    /// The identifier for the visibility setting.
    /// </summary>
    public Guid Id { get; init; }

    /// <summary>
    /// The display name for the visibility setting.
    /// </summary>
    public string DisplayName { get; init; } = null!;
}