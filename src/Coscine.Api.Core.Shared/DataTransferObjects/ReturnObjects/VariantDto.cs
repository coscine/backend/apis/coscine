﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents the variants of this specific metadata tree.
/// </summary>
/// <param name="GraphName">Name of the graph.</param>
/// <param name="Similarity">Similarity value 0-1</param>
public record VariantDto(Uri GraphName, double Similarity);
