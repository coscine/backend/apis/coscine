﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents the Data Transfer Object (DTO) for title information.
/// </summary>
public record TitleDto
{
    /// <summary>
    /// The identifier for the title.
    /// </summary>
    public required Guid Id { get; init; }

    /// <summary>
    /// The displayed name of the title.
    /// </summary>
    public required string DisplayName { get; init; }
}