﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a minimal Data Transfer Object (DTO) for a resource type.
/// </summary>
public record ResourceTypeMinimalDto
{
    /// <summary>
    /// The unique identifier of the resource type.
    /// </summary>
    public required Guid Id { get; init; }

    /// <summary>
    /// The specific type of the resource.
    /// </summary>
    public required string SpecificType { get; init; }
}