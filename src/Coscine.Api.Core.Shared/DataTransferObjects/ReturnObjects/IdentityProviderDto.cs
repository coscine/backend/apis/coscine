﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for an identity provider.
/// </summary>
public record IdentityProviderDto
{
    /// <summary>
    /// Gets or sets the unique identifier for the identity provider.
    /// </summary>
    public required Guid Id { get; init; }

    /// <summary>
    /// Gets or sets the display name of the identity provider.
    /// </summary>
    public required string DisplayName { get; init; }
}