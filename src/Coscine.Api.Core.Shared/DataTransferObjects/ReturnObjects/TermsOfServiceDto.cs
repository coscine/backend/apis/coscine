﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents the Data Transfer Object (DTO) for terms of service information.
/// </summary>
public record TermsOfServiceDto
{
    /// <summary>
    /// The version of the terms of service.
    /// </summary>
    public required string Version { get; init; }

    ///<summary>
    /// The URI point to the content of ToS' version
    /// </summary>
    public required Uri Href { get; init; }

    /// <summary>
    /// Indicates whether these terms of service are current or not.
    /// </summary>
    public required bool IsCurrent { get; init; }
}