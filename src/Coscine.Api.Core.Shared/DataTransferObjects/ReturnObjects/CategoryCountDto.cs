﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents the data transfer object containing the count for a specific category.
/// </summary>
public record CategoryCountDto
{
    /// <summary>
    /// The name of the category.
    /// </summary>
    public required string Name { get; init; }

    /// <summary>
    /// The count associated with the category.
    /// </summary>
    public required long Count { get; init; }
}