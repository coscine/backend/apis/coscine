﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// This class represents a maintenance with its significant properties, which is returned from the API.
/// </summary>
[Obsolete($"This class is obsolete and will be removed in the future. Use {nameof(MessageDto)} instead.")]
public record MaintenanceDto
{
    /// <summary>
    /// Gets or sets the maintenance title.
    /// </summary>
    public string DisplayName { get; init; } = null!;

    /// <summary>
    /// Gets or sets the URL related to the maintenance.
    /// </summary>
    public Uri Href { get; init; } = null!;

    /// <summary>
    /// Gets or sets the type of maintenance.
    /// </summary>
    public string Type { get; init; } = null!;

    /// <summary>
    /// Gets or sets the description of the maintenance.
    /// </summary>
    public string Body { get; init; } = null!;

    /// <summary>
    /// Gets or sets the start date of the maintenance, if available.
    /// </summary>
    public DateTime? StartsDate { get; init; }

    /// <summary>
    /// Gets or sets the end date of the maintenance, if available.
    /// </summary>
    public DateTime? EndsDate { get; init; }
}