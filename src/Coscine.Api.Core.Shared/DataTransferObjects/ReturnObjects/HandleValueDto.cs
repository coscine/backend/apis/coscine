﻿using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

public record HandleValueDto
{
    [JsonPropertyName("idx")]
    public int Idx { get; init; }

    [JsonPropertyName("type")]
    public string Type { get; init; } = null!;

    [JsonPropertyName("parsed_data")]
    public object ParsedData { get; init; } = null!;

    [JsonPropertyName("data")]
    public string Data { get; init; } = null!;

    [JsonPropertyName("timestamp")]
    public DateTime Timestamp { get; init; }

    [JsonPropertyName("ttl_type")]
    public int TtlType { get; init; }

    [JsonPropertyName("ttl")]
    public int Ttl { get; init; }

    [JsonPropertyName("refs")]
    public List<object> Refs { get; init; } = [];

    [JsonPropertyName("privs")]
    public string Privs { get; init; } = null!;
}