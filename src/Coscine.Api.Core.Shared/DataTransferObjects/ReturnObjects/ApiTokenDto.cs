﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents an API token used for authentication.
/// </summary>
public record ApiTokenDto
{
    /// <summary>
    /// The unique identifier of the API token.
    /// </summary>
    public Guid Id { get; init; }

    /// <summary>
    /// The name associated with the API token.
    /// </summary>
    public string Name { get; init; } = null!;

    /// <summary>
    /// The date when the API token was created.
    /// </summary>
    public DateTime CreationDate { get; init; }

    /// <summary>
    /// The expiry date of the API token.
    /// </summary>
    public DateTime ExpiryDate { get; init; }

    /// <summary>
    /// The actual token used for authentication.
    /// </summary>
    public string? Token { get; set; }

    /// <summary>
    /// Information about the owner of the API token.
    /// </summary>
    public UserMinimalDto Owner { get; init; } = null!;
}