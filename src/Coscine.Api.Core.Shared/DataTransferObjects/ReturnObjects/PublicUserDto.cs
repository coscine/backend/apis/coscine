﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a public user data transfer object (DTO).
/// </summary>
public record PublicUserDto
{
    /// <summary>
    /// Unique identifier of the user.
    /// </summary>
    public required Guid Id { get; init; }

    /// <summary>
    /// Display name of the user.
    /// </summary>
    public required string DisplayName { get; init; }

    /// <summary>
    /// Given name of the user.
    /// </summary>
    public required string GivenName { get; init; }

    /// <summary>
    /// Family name of the user.
    /// </summary>
    public required string FamilyName { get; init; }

    /// <summary>
    /// Email address of the user.
    /// </summary>
    public required string Email { get; init; }

    /// <summary>
    /// Title of the user (if available).
    /// </summary>
    public TitleDto? Title { get; init; }
}