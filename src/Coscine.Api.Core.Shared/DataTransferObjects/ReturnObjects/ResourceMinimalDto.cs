﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a minimal Data Transfer Object (DTO) for a resource, containing essential identifiers.
/// </summary>
public record ResourceMinimalDto
{
    /// <summary>
    /// Unique identifier for the resource.
    /// </summary>
    public required Guid Id { get; init; }
}