using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for resource details.
/// </summary>
public record ResourceDto
{
    /// <summary>
    /// Unique identifier for the resource.
    /// </summary>
    public Guid Id { get; init; }

    /// <summary>
    /// Persistent identifier for the resource.
    /// </summary>
    public required string Pid { get; set; }

    /// <summary>
    /// Type of the resource.
    /// </summary>
    public required ResourceTypeDto Type { get; init; }

    /// <summary>
    /// Name of the resource.
    /// </summary>
    public required string Name { get; init; }

    /// <summary>
    /// Display name of the resource.
    /// </summary>
    public required string DisplayName { get; init; }

    /// <summary>
    /// Description of the resource.
    /// </summary>
    public required string Description { get; init; }

    /// <summary>
    /// Keywords associated with the resource.
    /// </summary>
    public IEnumerable<string>? Keywords { get; init; }

    /// <summary>
    /// License information for the resource.
    /// </summary>
    public LicenseDto? License { get; init; }

    /// <summary>
    /// Usage rights associated with the resource.
    /// </summary>
    public string? UsageRights { get; init; }

    /// <summary>
    /// Indicates whether a local copy of the metadata is available for the resource.
    /// </summary>
    public bool MetadataLocalCopy { get; init; }

    /// <summary>
    /// Indicates whether metadata extraction is enabled for the resource.
    /// </summary>
    public bool MetadataExtraction { get; init; }

    /// <summary>
    /// Application profile details for the resource.
    /// </summary>
    public required ApplicationProfileMinimalDto ApplicationProfile { get; init; }

    /// <summary>
    /// Fixed values associated with resource manipulation.
    /// This dictionary may contain multiple levels and is structured as follows:
    /// Dictionary (Key: string) -> Dictionary (Key: string) -> List of FixedValueForResourceManipulationDto.
    /// </summary>
    // TODO: Ensure to test out completely. Could contain multiple levels of the dictionary object (here: 2).
    public required Dictionary<string, Dictionary<string, List<FixedValueForResourceManipulationDto>>> FixedValues { get; init; }

    /// <summary>
    /// Disciplines associated with the resource.
    /// </summary>
    public required IEnumerable<DisciplineDto> Disciplines { get; init; }

    /// <summary>
    /// Visibility settings for the resource.
    /// </summary>
    public required VisibilityDto Visibility { get; init; }

    /// <summary>
    /// Date when the resource was created.
    /// </summary>
    public DateTime? DateCreated { get; init; }

    /// <summary>
    /// Identifier of the creator of the resource.
    /// </summary>
    public UserMinimalDto? Creator { get; init; }

    /// <summary>
    /// Indicates whether the resource is archived.
    /// </summary>
    public bool Archived { get; init; }

    /// <summary>
    /// Indicates whether the resource is in maintenance mode.
    /// </summary>
    public bool MaintenanceMode { get; set; }

    /// <summary>
    /// The projects associated with the resource.
    /// </summary>
    /// <remarks>Optional, as the property's presence is controlled by a query parameter.</remarks>
    public List<ProjectMinimalDto>? Projects { get; set; }
}