namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a data transfer object (DTO) for a project role.
/// </summary>
public record ProjectRoleDto
{
    /// <summary>
    /// Identifier of the project role.
    /// </summary>
    public Guid Id { get; init; }

    /// <summary>
    /// The project associated with this role.
    /// </summary>
    public ProjectMinimalDto Project { get; init; } = null!;

    /// <summary>
    /// The role information.
    /// </summary>
    public RoleDto Role { get; init; } = null!;

    /// <summary>
    /// The user assigned to this project role.
    /// </summary>
    public PublicUserDto User { get; init; } = null!;
}