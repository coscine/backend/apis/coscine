﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a minimal data transfer object (DTO) for a project resource.
/// </summary>
public record ProjectResourceMinimalDto
{
    /// <summary>
    /// Identifier of the resource.
    /// </summary>
    public Guid ResourceId { get; set; }

    /// <summary>
    /// Identifier of the project associated with the resource.
    /// </summary>
    public Guid ProjectId { get; set; }
}