﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for language details.
/// </summary>
public record LanguageDto
{
    /// <summary>
    /// Gets or sets the unique identifier for the language.
    /// </summary>
    public required Guid Id { get; init; }

    /// <summary>
    /// Gets or sets the display name of the language.
    /// </summary>
    public required string DisplayName { get; init; }

    /// <summary>
    /// Gets or sets the abbreviation for the language.
    /// </summary>
    public required string Abbreviation { get; init; }
}