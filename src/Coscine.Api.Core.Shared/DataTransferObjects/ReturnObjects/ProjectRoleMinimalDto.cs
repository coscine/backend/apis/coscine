﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a minimal data transfer object (DTO) for a project role.
/// </summary>
public record ProjectRoleMinimalDto
{
    /// <summary>
    /// Identifier of the project associated with the role.
    /// </summary>
    public Guid ProjectId { get; set; }

    /// <summary>
    /// Identifier of the user associated with the role.
    /// </summary>
    public Guid UserId { get; set; }

    /// <summary>
    /// Identifier of the role.
    /// </summary>
    public Guid RoleId { get; set; }
}