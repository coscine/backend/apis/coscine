﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a minimalistic application profile data transfer object.
/// </summary>
public record ApplicationProfileMinimalDto
{
    /// <summary>
    /// The URI associated with the application profile.
    /// </summary>
    public required Uri Uri { get; init; }
}