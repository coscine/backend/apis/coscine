﻿using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a data transfer object (DTO) for an organization, including its ROR (Research Organization Registry) ID, display name, and email address.
/// </summary>
public record OrganizationDto
{
    /// <summary>
    /// The ROR (Research Organization Registry) ID of the organization.
    /// </summary>
    public required Uri Uri { get; init; }

    /// <summary>
    /// The display name of the organization.
    /// </summary>
    public required string DisplayName { get; init; }

    /// <summary>
    /// The email address of the organization.
    /// </summary>
    public string? Email { get; set; }

    /// <summary>
    /// The publication advisory service of the organization.
    /// </summary>
    public PublicationAdvisoryServiceDto? PublicationAdvisoryService { get; set; }
}