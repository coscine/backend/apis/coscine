﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for vocabulary details.
/// </summary>
public record VocabularyDto
{
    /// <summary>
    /// The URI of the graph containing the vocabulary.
    /// </summary>
    /// <remarks>
    /// Refers to the '@base' URI from vocabulary definitions.
    /// </remarks>
    public required Uri GraphUri { get; init; }

    /// <summary>
    /// The URI of the top-level parent class in the vocabulary.
    /// </summary>
    /// <remarks>
    /// The definition may lack the 'rdfs:subClassOf' property.
    /// </remarks>
    public required Uri ClassUri { get; init; }

    /// <summary>
    /// The display name of the vocabulary.
    /// </summary>
    public string? DisplayName { get; init; }

    /// <summary>
    /// The description of the vocabulary.
    /// </summary>
    public string? Description { get; init; }
}