﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for search results.
/// </summary>
public record SearchResultDto
{
    /// <summary>
    /// The URI associated with the search result.
    /// </summary>
    public required Uri Uri { get; init; }

    /// <summary>
    /// The type of search category.
    /// </summary>
    public required SearchCategoryType Type { get; init; }

    /// <summary>
    /// The dynamic source data for the search result.
    /// </summary>
    public required dynamic Source { get; init; }
}