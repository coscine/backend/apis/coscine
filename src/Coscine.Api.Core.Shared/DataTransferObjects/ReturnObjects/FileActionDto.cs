﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for file actions, including the URL and HTTP method.
/// </summary>
public record FileActionDto
{
    /// <summary>
    /// Gets or sets the presigned URL associated with the file action.
    /// </summary>
    /// <value>The URL for the file action.</value>
    public Uri Url { get; set; } = null!;

    /// <summary>
    /// Gets or sets the HTTP method associated with the file action.
    /// </summary>
    /// <value>The HTTP method for the file action.</value>
    public FileActionHttpMethod Method { get; set; }
}