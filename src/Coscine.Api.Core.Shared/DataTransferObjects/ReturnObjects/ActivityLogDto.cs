﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a data transfer object (DTO) for an activity log.
/// </summary>
public record ActivityLogDto
{
    public Guid Id { get; set; }

    public string ApiPath { get; set; } = null!;

    public string HttpAction { get; set; } = null!;

    public string? ControllerName { get; set; }

    public string? ActionName { get; set; }

    public Guid UserId { get; set; }

    public DateTime ActivityTimestamp { get; set; }
}