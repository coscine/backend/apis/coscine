﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for vocabulary instance details.
/// </summary>
public record VocabularyInstanceDto
{
    /// <summary>
    /// The URI of the graph containing the vocabulary.
    /// </summary>
    /// <remarks>
    /// Refers to the '@base' URI from vocabulary definitions.
    /// </remarks>
    public required Uri GraphUri { get; init; }

    /// <summary>
    /// The URI of the instance.
    /// </summary>
    public Uri InstanceUri { get; set; } = null!;

    /// <summary>
    /// The URI representing the type of the instance.
    /// </summary>
    /// <remarks>
    /// NOTE: 'a' is an alias for 'rdf:type'.
    /// </remarks>
    public Uri? TypeUri { get; init; }

    /// <summary>
    /// The URI of the direct parent class.
    /// </summary>
    /// <remarks>
    /// Refers to the 'rdfs:subClassOf' property.
    /// </remarks>
    public Uri? SubClassOfUri { get; init; }

    /// <summary>
    /// The display name of the vocabulary instance.
    /// </summary>
    public string? DisplayName { get; init; }

    /// <summary>
    /// The description of the vocabulary instance.
    /// </summary>
    public string? Description { get; init; }
}