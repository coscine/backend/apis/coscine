namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for user-related information.
/// </summary>
public record UserDto
{
    /// <summary>
    /// The unique identifier of the user.
    /// </summary>
    public required Guid Id { get; init; }

    /// <summary>
    /// The display name of the user.
    /// </summary>
    public required string DisplayName { get; init; }

    /// <summary>
    /// The given name of the user.
    /// </summary>
    public required string GivenName { get; init; }

    /// <summary>
    /// The family name of the user.
    /// </summary>
    public required string FamilyName { get; init; }

    /// <summary>
    /// The email addresses associated with the user.
    /// </summary>
    public required IEnumerable<UserEmailDto> Emails { get; init; }

    /// <summary>
    /// The title of the user.
    /// </summary>
    public TitleDto? Title { get; init; }

    /// <summary>
    /// The language of the user.
    /// </summary>
    public required LanguageDto Language { get; init; }

    /// <summary>
    /// Indicates if the terms of service are accepted by the user.
    /// </summary>
    public required bool AreToSAccepted { get; set; }

    /// <summary>
    /// The date and time of the latest activity of the user.
    /// </summary>
    public DateTime? LatestActivity { get; init; }

    /// <summary>
    /// The disciplines associated with the user.
    /// </summary>
    public required IEnumerable<DisciplineDto> Disciplines { get; init; }

    /// <summary>
    /// The organizations associated with the user.
    /// </summary>
    public required IEnumerable<UserOrganizationDto> Organizations { get; set; }

    /// <summary>
    /// The identity providers associated with the user.
    /// </summary>
    public required IEnumerable<IdentityProviderDto> Identities { get; init; }
}