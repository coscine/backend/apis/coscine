﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a data transfer object (DTO) for metadata within a tree structure, extending the base TreeDto.
/// </summary>
public record MetadataTreeDto : TreeDto
{
    /// <summary>
    /// Gets or sets the id of the specific metadata tree.
    /// </summary>
    public Uri Id { get; init; } = null!;

    /// <summary>
    /// Gets or sets the version associated with the metadata.
    /// </summary>
    public string Version { get; init; } = null!;

    /// <summary>
    /// Gets or sets the collection of available versions related to the metadata.
    /// </summary>
    public IEnumerable<string> AvailableVersions { get; init; } = new List<string>();

    /// <summary>
    /// Gets or sets the RDF definition associated with the metadata.
    /// </summary>
    public RdfDefinitionDto Definition { get; init; } = null!;

    /// <summary>
    /// Gets or sets the extracted metadata.
    /// </summary>
    /// <remarks>
    /// Will only be retrieved, if the extracted flag was set to true.
    /// </remarks>
    public MetadataTreeExtractedDto? Extracted { get; init; }

    /// <summary>
    /// Gets or sets the provenance information.
    /// </summary>
    public ProvenanceDto? Provenance { get; init; }
}
