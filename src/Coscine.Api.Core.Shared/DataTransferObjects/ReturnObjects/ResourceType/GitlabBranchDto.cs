namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;

/// <summary>
/// Represents a GitLab branch data transfer object (DTO).
/// </summary>
public record GitlabBranchDto
{
    /// <summary>
    /// The name of the GitLab branch.
    /// </summary>
    public required string Name { get; init; }

    /// <summary>
    /// Indicates if the branch is merged.
    /// </summary>
    public required bool Merged { get; init; }

    /// <summary>
    /// Indicates if the branch is protected.
    /// </summary>
    public required bool Protected { get; init; }

    /// <summary>
    /// Indicates if the branch is set as default.
    /// </summary>
    public required bool Default { get; init; }

    /// <summary>
    /// Indicates if developers can push changes to the branch.
    /// </summary>
    public required bool DevelopersCanPush { get; init; }

    /// <summary>
    /// Indicates if developers can merge changes into the branch.
    /// </summary>
    public required bool DevelopersCanMerge { get; init; }

    /// <summary>
    /// Indicates if the user can push changes to the branch.
    /// </summary>
    public required bool CanPush { get; init; }
}