namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;

/// <summary>
/// Represents the structure of resource type-specific components for steps in the resource creation page.
/// </summary>
[Obsolete("Should not use that anymore. Remove when possible!")]
public record ResourceCreationPageDto
{
    /// <summary>
    /// List of Lists containing all the resource type specific components for the steps in the resource creation page.
    /// </summary>
    public List<List<string>> Components { get; init; } = null!;
}