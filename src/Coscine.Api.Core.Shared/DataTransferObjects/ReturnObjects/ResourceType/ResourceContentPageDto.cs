namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;

/// <summary>
/// Represents the content page details for a resource.
/// </summary>
public record ResourceContentPageDto
{
    /// <summary>
    /// Indicates whether the resource is read-only.
    /// </summary>
    public bool ReadOnly { get; init; }

    /// <summary>
    /// Relevant information about the resource type for the metadata Vue component.
    /// </summary>
    public ResourceContentPageMetadataViewDto MetadataView { get; init; } = null!;

    /// <summary>
    /// Relevant information about the resource type for the entries view Vue component.
    /// </summary>
    public ResourceContentPageEntriesViewDto EntriesView { get; init; } = null!;
}