﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;
/// <summary>
/// Represents information about a resource type.
/// </summary>
public record ResourceTypeInformationDto
{
    /// <summary>
    /// The unique identifier of the resource type.
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// The general type of the resource.
    /// </summary>
    public string? GeneralType { get; init; }

    /// <summary>
    /// The specific type of the resource.
    /// </summary>
    public string? SpecificType { get; init; }

    /// <summary>
    /// The status of the resource type.
    /// </summary>
    public ResourceTypeStatus Status { get; init; }

    /// <summary>
    /// Indicates if the resource type supports creation.
    /// </summary>
    public bool CanCreate { get; init; }

    /// <summary>
    /// Indicates if the resource type supports reading.
    /// </summary>
    public bool CanRead { get; init; }

    /// <summary>
    /// Indicates if the resource type supports read-only.
    /// </summary>
    public bool CanSetResourceReadonly { get; init; }

    /// <summary>
    /// Indicates if the resource type supports updating.
    /// </summary>
    public bool CanUpdate { get; init; }

    /// <summary>
    /// Indicates if the resource type supports updating (not an Object).
    /// </summary>
    public bool CanUpdateResource { get; init; }

    /// <summary>
    /// Indicates if the resource type supports deletion.
    /// </summary>
    public bool CanDelete { get; init; }

    /// <summary>
    /// Indicates if the resource type supports deletion (not an Object).
    /// </summary>
    public bool CanDeleteResource { get; init; }

    /// <summary>
    /// Indicates if the resource type supports listing.
    /// </summary>
    public bool CanList { get; init; }

    /// <summary>
    /// Indicates if the resource type supports linking.
    /// </summary>
    public bool CanCreateLinks { get; init; }

    /// <summary>
    /// Indicates whether local backup of metadata is supported.
    /// </summary>
    public bool CanCopyLocalMetadata { get; set; }

    /// <summary>
    /// Indicates if the resource type is archived.
    /// </summary>
    public bool IsArchived { get; init; }

    /// <summary>
    /// Indicates if the resource type supports quota.
    /// </summary>
    public bool IsQuotaAvailable { get; init; }

    /// <summary>
    /// Indicates if the resource type quota can be changed.
    /// </summary>
    public bool IsQuotaAdjustable { get; init; }

    /// <summary>
    /// Indicates if the resource type is enabled.
    /// </summary>
    public bool IsEnabled => Status == ResourceTypeStatus.Active;

    /// <summary>
    /// Deprecated property for the resource creation page information.
    /// </summary>
    [Obsolete("This property is deprecated. Use IsQuotaAvailable instead.")]
    public ResourceCreationPageDto ResourceCreation { get; init; } = null!;

    /// <summary>
    /// Relevant information about the resource type for the resource content page.
    /// </summary>
    public ResourceContentPageDto ResourceContent { get; init; } = null!;
}