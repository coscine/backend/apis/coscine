namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;

/// <summary>
/// Represents a GitLab project data transfer object (DTO).
/// </summary>
public record GitlabProjectDto
{
    /// <summary>
    /// The unique identifier for the GitLab project.
    /// </summary>
    public required int Id { get; init; }

    /// <summary>
    /// Indicates if the GitLab project is archived.
    /// </summary>
    public required bool Archived { get; init; }

    /// <summary>
    /// The name of the GitLab project.
    /// </summary>
    public required string Name { get; init; }

    /// <summary>
    /// The full name of the GitLab project including namespace.
    /// </summary>
    public required string NameWithNamespace { get; init; }

    /// <summary>
    /// A brief description of the GitLab project.
    /// </summary>
    public required string Description { get; init; }

    /// <summary>
    /// The default branch of the GitLab project.
    /// </summary>
    public required string DefaultBranch { get; init; }

    /// <summary>
    /// The path of the GitLab project.
    /// </summary>
    public required string Path { get; init; }

    /// <summary>
    /// The path of the GitLab project including namespace.
    /// </summary>
    public required string PathWithNamespace { get; init; }

    /// <summary>
    /// The timestamp of the last activity related to the GitLab project.
    /// </summary>
    public required string LastActivityAt { get; init; }

    /// <summary>
    /// The creation timestamp of the GitLab project.
    /// </summary>
    public required string CreatedAt { get; init; }

    /// <summary>
    /// The URL to view the GitLab project in a web browser.
    /// </summary>
    public required string WebUrl { get; init; }

    /// <summary>
    /// The HTTP URL to access the GitLab project repository.
    /// </summary>
    public required string HttpUrlToRepo { get; init; }

    /// <summary>
    /// The SSH URL to access the GitLab project repository.
    /// </summary>
    public required string SshUrlToRepo { get; init; }
}