namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;

/// <summary>
/// Represents information about the resource type for the columns within the entries view Vue component.
/// </summary>
public record ResourceContentPageEntriesViewDto
{
    /// <summary>
    /// Relevant information about the resource type for the columns within entries view Vue component.
    /// </summary>
    public ResourceContentPageColumnsDto Columns { get; init; } = null!;
}