namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;

/// <summary>
/// Represents the metadata view for a resource's content page.
/// </summary>
public record ResourceContentPageMetadataViewDto
{
    /// <summary>
    /// Indicates if a data URL can be provided and is editable.
    /// </summary>
    public bool EditableDataUrl { get; set; }

    /// <summary>
    /// Indicates if a key can be provided and is editable.
    /// </summary>
    public bool EditableKey { get; set; }
}