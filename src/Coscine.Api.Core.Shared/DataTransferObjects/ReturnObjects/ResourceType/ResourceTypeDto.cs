namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;

/// <summary>
/// Represents a resource type.
/// </summary>
public record ResourceTypeDto
{
    /// <summary>
    /// The unique identifier of the resource type.
    /// </summary>
    public Guid Id { get; init; }

    /// <summary>
    /// The general type of the resource.
    /// </summary>
    public required string GeneralType { get; init; }

    /// <summary>
    /// The specific type of the resource.
    /// </summary>
    public required string SpecificType { get; init; }

    /// <summary>
    /// Additional options related to the resource type.
    /// </summary>
    public ResourceTypeOptionsDto? Options { get; set; }
}