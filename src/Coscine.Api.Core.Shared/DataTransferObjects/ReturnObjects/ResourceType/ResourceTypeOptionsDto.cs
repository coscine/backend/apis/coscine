﻿using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType.ResourceTypeOptions;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;

/// <summary>
/// Represents the options available for different resource types.
/// </summary>
public record ResourceTypeOptionsDto
{
    /// <summary>
    /// Options specific to linked data resources.
    /// </summary>
    public LinkedDataOptionsDto? LinkedData { get; init; }

    /// <summary>
    /// Options specific to GitLab resources.
    /// </summary>
    public GitLabOptionsDto? GitLab { get; init; }

    /// <summary>
    /// Options specific to RDS resources.
    /// </summary>
    public RdsOptionsDto? Rds { get; init; }

    /// <summary>
    /// Options specific to RDS S3 resources.
    /// </summary>
    public RdsS3OptionsDto? RdsS3 { get; init; }

    /// <summary>
    /// Options specific to RDS S3 WORM resources.
    /// </summary>
    public RdsS3WormOptionsDto? RdsS3Worm { get; init; }

    /// <summary>
    /// Options specific to the local File System Storage resources.
    /// </summary>
    public FileSystemStorageOptionsDto? FileSystemStorage { get; init; }
}