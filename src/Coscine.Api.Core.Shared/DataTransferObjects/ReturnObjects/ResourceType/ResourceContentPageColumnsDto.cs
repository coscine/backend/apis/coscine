namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;

/// <summary>
/// Represents a set of columns to be displayed in the content view.
/// </summary>
public record ResourceContentPageColumnsDto
{
    /// <summary>
    /// Set of columns that should always be displayed in the content view.
    /// </summary>
    public HashSet<string> Always { get; init; } = null!;
}