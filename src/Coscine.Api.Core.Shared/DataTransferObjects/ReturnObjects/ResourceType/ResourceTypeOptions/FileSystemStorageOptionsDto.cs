namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType.ResourceTypeOptions;

/// <summary>
/// Represents the data transfer object (DTO) for FileSystemStorage options.
/// </summary>
public record FileSystemStorageOptionsDto
{
    /// <summary>
    /// The directory where the files are stored.
    /// </summary>
    public required string Directory { get; init; }
}