﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType.ResourceTypeOptions;

/// <summary>
/// Represents the data transfer object (DTO) for RDS options.
/// </summary>
public record RdsOptionsDto
{
    /// <summary>
    /// The name of the bucket associated with RDS.
    /// </summary>
    public required string BucketName { get; init; }

    /// <summary>
    /// The size quota details for RDS.
    /// </summary>
    public QuotaDto? Size { get; set; } = null!;
}
