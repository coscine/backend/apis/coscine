﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType.ResourceTypeOptions;

/// <summary>
/// Represents the data transfer object (DTO) for RDS S3 options.
/// </summary>
public record RdsS3OptionsDto
{
    /// <summary>
    /// The name of the bucket associated with RDS S3.
    /// </summary>
    public required string BucketName { get; init; }

    /// <summary>
    /// The access key for reading from the RDS S3 bucket.
    /// </summary>
    public required string AccessKeyRead { get; init; }

    /// <summary>
    /// The secret key for reading from the RDS S3 bucket.
    /// </summary>
    public required string SecretKeyRead { get; init; }

    /// <summary>
    /// The access key for writing to the RDS S3 bucket.
    /// </summary>
    public required string AccessKeyWrite { get; init; }

    /// <summary>
    /// The secret key for writing to the RDS S3 bucket.
    /// </summary>
    public required string SecretKeyWrite { get; init; }

    /// <summary>
    /// The endpoint for the RDS S3 bucket.
    /// </summary>
    public required string Endpoint { get; init; }

    /// <summary>
    /// The size quota details for RDS S3.
    /// </summary>
    public QuotaDto? Size { get; set; } = null!;
}
