﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType.ResourceTypeOptions;

/// <summary>
/// Represents the data transfer object (DTO) for RDS S3 WORM options.
/// </summary>
public record RdsS3WormOptionsDto
{
    /// <summary>
    /// The name of the bucket associated with RDS S3 WORM.
    /// </summary>
    public required string BucketName { get; init; }

    /// <summary>
    /// The access key for reading from the RDS S3 WORM bucket.
    /// </summary>
    public required string AccessKeyRead { get; init; }

    /// <summary>
    /// The secret key for reading from the RDS S3 WORM bucket.
    /// </summary>
    public required string SecretKeyRead { get; init; }

    /// <summary>
    /// The access key for writing to the RDS S3 WORM bucket.
    /// </summary>
    public required string AccessKeyWrite { get; init; }

    /// <summary>
    /// The secret key for writing to the RDS S3 WORM bucket.
    /// </summary>
    public required string SecretKeyWrite { get; init; }

    /// <summary>
    /// The endpoint for the RDS S3 WORM bucket.
    /// </summary>
    public required string Endpoint { get; init; }

    /// <summary>
    /// The size quota details for RDS S3 WORM.
    /// </summary>
    public QuotaDto? Size { get; set; } = null!;
}
