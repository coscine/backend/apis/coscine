﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType.ResourceTypeOptions;

/// <summary>
/// Represents the data transfer object (DTO) for GitLab options.
/// </summary>
public record GitLabOptionsDto
{
    /// <summary>
    /// The project ID associated with GitLab.
    /// </summary>
    public int ProjectId { get; init; }

    /// <summary>
    /// The repository URL for GitLab.
    /// </summary>
    public string? RepoUrl { get; init; }

    /// <summary>
    /// The access token for GitLab.
    /// </summary>
    public string? AccessToken { get; init; }

    /// <summary>
    /// The branch for GitLab.
    /// </summary>
    public string? Branch { get; init; }
}