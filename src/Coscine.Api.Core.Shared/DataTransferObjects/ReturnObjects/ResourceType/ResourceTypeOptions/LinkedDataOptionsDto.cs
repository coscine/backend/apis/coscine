﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType.ResourceTypeOptions;

/// <summary>
/// Represents the data transfer object (DTO) for Linked Data options.
/// </summary>
public record LinkedDataOptionsDto
{ }