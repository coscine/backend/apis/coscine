﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for license details.
/// </summary>
public record LicenseDto
{
    /// <summary>
    /// Gets or sets the unique identifier for the license.
    /// </summary>
    public Guid Id { get; init; }

    /// <summary>
    /// Gets or sets the display name of the license.
    /// </summary>
    public string DisplayName { get; init; } = null!;

    /// <summary>
    /// Gets or sets the Uri of the license.
    /// </summary>
    public Uri Url { get; init; } = null!;
}