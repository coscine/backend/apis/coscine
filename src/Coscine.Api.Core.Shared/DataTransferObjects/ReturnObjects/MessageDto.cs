using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

public record MessageDto {
    /// <summary>
    /// ID of the message.
    /// </summary>
    public required string Id { get; set; }

    /// <summary>
    /// Message body in multiple languages as key-value pairs. 
    /// The key is the ISO 639-1 Alpha-2 two-letter language code, and the value is the message in that language.
    /// </summary>
    /// <remarks>
    /// Represents the relevant payload of the message to be communicated to the user. 
    /// For NOC tickets, the body will be left empty, as the user will be redirected to the NOC ticketing system to view the message there.
    /// </remarks>
    public Dictionary<string, string>? Body { get; init; }

    /// <summary>
    /// Message type (e.g. information, warning, error).
    /// </summary>
    public MessageType Type { get; init; }
    
    /// <summary>
    /// Title of the message.
    /// </summary>
    public string? Title { get; init; }

    /// <summary>
    /// URL related to the message.
    /// </summary>
    public Uri? Href { get; init; } = null!;

    /// <summary>
    /// Start date of the message (UTC).
    /// </summary>
    public DateTimeOffset? StartDate { get; init; }

    /// <summary>
    /// End date of the message (UTC).
    /// </summary>
    public DateTimeOffset? EndDate { get; init; }
}