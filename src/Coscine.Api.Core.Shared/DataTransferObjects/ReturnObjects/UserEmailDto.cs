﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for user email information.
/// </summary>
public record UserEmailDto
{
    /// <summary>
    /// The email address of the user.
    /// </summary>
    public string? Email { get; init; }

    /// <summary>
    /// Indicates whether the email address is confirmed.
    /// </summary>
    public bool IsConfirmed { get; init; }

    /// <summary>
    /// Indicates whether the email address is the primary one for the user.
    /// </summary>
    public bool IsPrimary { get; init; }
}