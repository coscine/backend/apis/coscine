﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) for a collection of file actions, including download action details.
/// </summary>
public record FileActionsDto
{
    /// <summary>
    /// Gets or sets the details for the download file action.
    /// </summary>
    /// <value>The download action details, or <c>null</c> if no download action is specified.</value>
    public FileActionDto? Download { get; set; }
}