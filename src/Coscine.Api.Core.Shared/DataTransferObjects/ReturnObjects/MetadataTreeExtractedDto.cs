﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a data transfer object (DTO) for extracted metadata within a tree structure, extending the base TreeDto.
/// </summary>
public record MetadataTreeExtractedDto : TreeDto
{
    /// <summary>
    /// Gets the identifier of the metadata extraction graph.
    /// </summary>
    public Uri MetadataId { get; init; } = null!;

    /// <summary>
    /// Gets the identifier of the raw data extraction graph.
    /// </summary>
    public Uri? RawDataId { get; init; } = null!;

    /// <summary>
    /// Gets the RDF definition associated with the metadata.
    /// </summary>
    public RdfDefinitionDto Definition { get; init; } = null!;
}
