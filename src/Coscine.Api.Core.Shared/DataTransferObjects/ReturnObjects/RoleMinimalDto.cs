﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a Data Transfer Object (DTO) containing minimal information about a role.
/// </summary>
public record RoleMinimalDto
{
    /// <summary>
    /// The unique identifier of the role.
    /// </summary>
    public required Guid Id { get; init; }
}