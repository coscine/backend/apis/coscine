﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents the data transfer object for requesting an application profile.
/// </summary>
public record ApplicationProfileRequestDto
{
    /// <summary>
    /// The name of the application profile.
    /// </summary>
    public required string ApplicationProfileName { get; init; }

    /// <summary>
    /// The URL of the merge request associated with the application profile.
    /// </summary>
    public required Uri MergeRequestUrl { get; init; }
}