namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a data transfer object (DTO) for a project publication request.
/// </summary>
public record ProjectPublicationRequestDto
{
    /// <summary>
    /// Unique identifier for the project publication request.
    /// </summary>
    public Guid Id { get; init; }

    /// <summary>
    /// The associated project.
    /// </summary>
    public ProjectMinimalDto Project { get; init; } = null!;

    /// <summary>
    /// Identifier for the publication service used for this request.
    /// </summary>
    public Uri PublicationServiceRorId { get; init; } = null!;

    /// <summary>
    /// The user who created the request.
    /// </summary>
    public UserMinimalDto Creator { get; init; } = null!;

    /// <summary>
    /// The date and time when the request was created.
    /// </summary>
    public DateTime DateCreated { get; init; }

    /// <summary>
    /// Optional message associated with the publication request.
    /// </summary>
    public string? Message { get; init; }

    /// <summary>
    /// Collection of the resources involved in the publication request.
    /// </summary>
    public IEnumerable<ResourceMinimalDto> Resources { get; init; } = [];
}