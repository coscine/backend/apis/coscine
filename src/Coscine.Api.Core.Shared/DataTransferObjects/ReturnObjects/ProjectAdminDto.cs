﻿namespace Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;

/// <summary>
/// Represents a data transfer object (DTO) for an administrative view of a project.
/// Extends the base information in <see cref="ProjectDto"/>.
/// </summary>
public record ProjectAdminDto : ProjectDto
{
    /// <summary>
    /// Indicates whether the project is marked as deleted.
    /// </summary>
    public bool Deleted { get; init; }

    /// <summary>
    /// Collection of minimal project resource details associated with the project.
    /// </summary>
    public IEnumerable<ProjectResourceMinimalDto> ProjectResources { get; init; } = [];

    /// <summary>
    /// Collection of minimal project role details associated with the project.
    /// </summary>
    public IEnumerable<ProjectRoleMinimalDto> ProjectRoles { get; init; } = [];

    /// <summary>
    /// Collection of project quotas associated with the project.
    /// </summary>
    public IEnumerable<ProjectQuotaDto> ProjectQuota { get; set; } = [];

    /// <summary>
    /// Collection of project publication requests associated with the project.
    /// </summary>
    public IEnumerable<ProjectPublicationRequestDto> PublicationRequests { get; set; } = [];
}
