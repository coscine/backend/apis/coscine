﻿using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.QueryParameters;

public class ProvenanceQueryParameters
{
    /// <summary>
    /// Gets or sets the path of the metadata tree.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string Path { get; set; }

    /// <summary>
    /// Gets or sets the desired version.
    /// If the version is null, the newest will be returned.
    /// </summary>
    public int? Version { get; set; }
}
