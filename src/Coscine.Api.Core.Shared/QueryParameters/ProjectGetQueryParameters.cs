﻿namespace Coscine.Api.Core.Shared.QueryParameters;

/// <summary>
/// Represents query parameters for retrieving project-related information.
/// </summary>
public class ProjectGetQueryParameters
{
    /// <summary>
    /// Gets or sets a value indicating whether to include sub-projects in the retrieval.
    /// </summary>
    public bool? IncludeSubProjects { get; set; }
}