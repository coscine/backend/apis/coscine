﻿using Coscine.Api.Core.Shared.Enums;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.QueryParameters;

public class MetadataTreeQueryParameters
{
    /// <summary>
    /// Gets or sets the path of the metadata tree.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    public required string Path { get; set; }

    /// <summary>
    /// Gets or sets the format of the RDF data.
    /// </summary>
    public RdfFormat Format { get; set; }

    /// <summary>
    /// Gets or sets if extracted metadata should be included.
    /// </summary>
    public bool IncludeExtractedMetadata { get; set; }

    /// <summary>
    /// Gets or sets if provenance should be included.
    /// </summary>
    public bool IncludeProvenance { get; set; }

    /// <summary>
    /// Gets or sets the desired version.
    /// If the version is null, the newest will be returned.
    /// </summary>
    public int? Version { get; set; }
}
