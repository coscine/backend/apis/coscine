namespace Coscine.Api.Core.Shared.Helpers;

public static class UriExtensions
{
    /// <summary>
    /// Appends one or more relative paths to the base <see cref="Uri"/>.
    /// </summary>
    /// <param name="baseUri">The base <see cref="Uri"/> to which the relative paths will be appended.</param>
    /// <param name="paths">An array of relative path segments to append to the base URI.</param>
    /// <returns>
    /// A new <see cref="Uri"/> instance with the appended path segments. 
    /// If the URI uses the default port for its scheme (80 for HTTP or 443 for HTTPS), the port will be excluded from the resulting URI.
    /// </returns>
    public static Uri AppendToPath(this Uri baseUri, params string[] paths)
    {
        if (baseUri == null)
            throw new ArgumentNullException(nameof(baseUri));

        if (paths == null)
            throw new ArgumentNullException(nameof(paths));

        // Use UriBuilder to construct the Uri
        var uriBuilder = new UriBuilder(baseUri);

        // Start with the existing path of the base Uri, trimming the end slash
        string combinedPath = uriBuilder.Path.TrimEnd('/');

        // Append each path segment, ensuring they are properly separated by slashes
        foreach (var path in paths)
        {
            if (!string.IsNullOrWhiteSpace(path))
            {
                // Combine the paths and maintain proper slash positioning
                combinedPath = $"{combinedPath.TrimEnd('/')}/{path.TrimStart('/')}";
            }
        }

        // Assign the combined path back to the UriBuilder
        uriBuilder.Path = combinedPath;

        // Remove the port if it's the default for the scheme
        if ((uriBuilder.Scheme == Uri.UriSchemeHttp && uriBuilder.Port == 80) ||
            (uriBuilder.Scheme == Uri.UriSchemeHttps && uriBuilder.Port == 443))
        {
            uriBuilder.Port = -1;  // This will exclude the port from the Uri when converted to a string
        }

        // Return the constructed Uri
        return uriBuilder.Uri;
    }
}