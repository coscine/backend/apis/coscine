﻿using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Shared.Helpers;

/// <summary>
/// Provides helper methods to work with paginated data.
/// </summary>
public static class PaginationHelper
{
    /// <summary>
    /// Asynchronously retrieves all items of type <typeparamref name="T"/> across all pages.
    /// </summary>
    /// <remarks>
    /// This method repeatedly calls the provided <paramref name="PageRequest"/> function to fetch items from multiple pages.
    /// It lazily generates an asynchronous sequence of items, yielding each item as it becomes available.
    /// </remarks>
    /// <typeparam name="T">The type of the items in the paginated collection.</typeparam>
    /// <param name="PageRequest">
    /// A function that takes an integer (representing the page number) and returns
    /// a <see cref="Task"/> that results in a <see cref="PagedEnumerable{T}"/>.
    /// The function should handle the logic to retrieve items for the specified page number.
    /// </param>
    /// <returns>
    /// An asynchronous enumerable <see cref="IAsyncEnumerable{T}"/> that represents the sequence of items.
    /// The sequence is lazily generated, yielding items as they are retrieved from each page.
    /// </returns>
    /// <example>
    /// <para>
    /// Usage example:
    /// </para>
    /// <code>
    /// var allItems = PaginationHelper.GetAllAsync(pageNumber => FetchItemsPageAsync(pageNumber));
    /// </code>
    /// <para>
    /// This asynchronously retrieves all items across all pages using the provided function <c>FetchItemsPageAsync</c>.
    /// </para>
    /// </example>
    public static async IAsyncEnumerable<T> GetAllAsync<T>(Func<int, Task<PagedEnumerable<T>>> PageRequest) where T : class
    {
        var currentPage = 1;
        bool hasNext;

        do
        {
            var iteration = await PageRequest(currentPage);

            if (iteration == null || iteration.Pagination == null)
            {
                // Handle the case where the PageRequest did not return expected data
                yield break;
            }

            foreach (var element in iteration)
            {
                yield return element;
            }

            currentPage++;
            hasNext = iteration.Pagination.HasNext;
        }
        while (hasNext);
    }
}