using System.Text.Json;
using System.Text.Json.Serialization;
using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.Helpers;

public class MessageTypeConverter : JsonConverter<MessageType>
{
    public override MessageType Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var stringValue = reader.GetString();
        if (!string.IsNullOrWhiteSpace(stringValue))
        {
            return MessageTypeExtensions.FromString(stringValue);
        }
        throw new JsonException($"Unable to convert \"{stringValue}\" to {nameof(MessageType)}");
    }

    public override void Write(Utf8JsonWriter writer, MessageType value, JsonSerializerOptions options)
    {
        var descriptionString = value.ToDescriptionString();
        writer.WriteStringValue(descriptionString);
    }
}
