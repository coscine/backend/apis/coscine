﻿using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace Coscine.Api.Core.Shared.Helpers;

/// <summary>
/// Provides extension methods for working with regular expressions.
/// </summary>
public static class RegexExtensions
{
    /// <summary>
    /// Tries to parse a string into a <see cref="Regex"/> object, validating the string as a regular expression.
    /// </summary>
    /// <param name="possibleRegex">The string to parse as a regular expression.</param>
    /// <param name="regex">When this method returns, contains the <see cref="Regex"/> object created from the <paramref name="possibleRegex"/> string if the parse succeeded, or null if the parse failed. This parameter is passed uninitialized.</param>
    /// <returns><c>true</c> if <paramref name="possibleRegex"/> was successfully parsed as a <see cref="Regex"/>; otherwise, <c>false</c>.</returns>
    public static bool TryParse([NotNullWhen(true)] string? possibleRegex, [NotNullWhen(true)] out Regex? regex)
    {
        regex = null;
        if (possibleRegex is null)
        {
            return false;
        }

        try
        {
            regex = new Regex(possibleRegex);
            return true;
        }
        catch (ArgumentException)
        {
            return false;
        }
    }
}
