using System.ComponentModel;
using System.Reflection;
using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.Helpers;

public static class MessageTypeExtensions
{
    public static MessageType FromString(string stringValue)
    {
        foreach (var field in typeof(MessageType).GetFields())
        {
            var attribute = field.GetCustomAttribute<DescriptionAttribute>();
            if (attribute != null)
            {
                var descriptions = attribute.Description.Split('|');
                if (descriptions.Contains(stringValue, StringComparer.OrdinalIgnoreCase))
                {
                    return (MessageType)field.GetValue(null)!;
                }
            }
        }
        throw new ArgumentException($"Unable to convert \"{stringValue}\" to {nameof(MessageType)}");
    }

    public static string ToDescriptionString(this MessageType value)
    {
        var field = value.GetType().GetField(value.ToString());
        var attribute = field?.GetCustomAttribute<DescriptionAttribute>();
        if (attribute != null)
        {
            var descriptions = attribute.Description.Split('|');
            return descriptions[0]; // Default to English
        }
        return value.ToString();
    }
}