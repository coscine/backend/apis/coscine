﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for user administration, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class UserAdminParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="UserAdminParameters"/> class with the default <c>OrderBy</c> value set to "firstName".
    /// </summary>
    public UserAdminParameters() => OrderBy = "firstName";

    /// <summary>
    /// Gets or sets a value indicating whether the terms of service have been accepted by the user.
    /// </summary>
    public bool? TosAccepted { get; set; }

    /// <summary>
    /// Override the max page size for this derived class to <c>250</c>.
    /// </summary>
    protected override int MaxPageSize => 250;

    /// <summary>
    /// Number of items per page. The maximum number of items per page is <c>250</c>.
    /// </summary>
    public override int PageSize
    {
        get => base.PageSize;
        set => base.PageSize = value;
    }
}