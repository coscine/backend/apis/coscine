﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for discipline requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class DisciplineParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DisciplineParameters"/> class with the default <c>OrderBy</c> value set to "id".
    /// </summary>
    /// <remarks>
    /// The default sorting order applied when retrieving disciplines is by the "id".
    /// </remarks>
    public DisciplineParameters() => OrderBy = "id";
}