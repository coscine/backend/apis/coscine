namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for project invitations, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class ProjectInvitationParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ProjectInvitationParameters"/> class with the default <c>OrderBy</c> value set to "id".
    /// </summary>
    public ProjectInvitationParameters() => OrderBy = "id";
}