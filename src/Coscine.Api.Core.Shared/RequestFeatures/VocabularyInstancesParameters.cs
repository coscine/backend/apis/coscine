﻿using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Validators;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for managing vocabulary instances, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class VocabularyInstancesParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="VocabularyInstancesParameters"/> class with the default <c>OrderBy</c> value set to "displayName".
    /// </summary>
    public VocabularyInstancesParameters() => OrderBy = "displayName";

    /// <summary>
    /// Gets or sets the URI class, which is a required field.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    [AbsoluteUri]
    public Uri Class { get; set; } = null!;

    /// <summary>
    /// Gets or sets the search term used to filter vocabulary instances.
    /// </summary>
    public string? SearchTerm { get; set; }

    /// <summary>
    /// Gets or sets the language for which the vocabulary instances are requested.
    /// </summary>
    public AcceptedLanguage Language { get; set; } = AcceptedLanguage.en;

    /// <summary>
    /// Gets the fallback language used when the requested language is not available.
    /// </summary>
    public readonly AcceptedLanguage FallbackLanguage = AcceptedLanguage.en; // Hardcoding for the time being, as we do not ask the user to provide it

    /// <summary>
    /// Override the max page size for this derived class to <c>150</c>.
    /// </summary>
    protected override int MaxPageSize => 150;

    /// <summary>
    /// Number of items per page. The maximum number of items per page is <c>150</c>.
    /// </summary>
    public override int PageSize
    {
        get => base.PageSize;
        set => base.PageSize = value;
    }
}