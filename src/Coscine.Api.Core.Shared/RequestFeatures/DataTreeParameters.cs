﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for data tree requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class DataTreeParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="DataTreeParameters"/> class with the default <c>OrderBy</c> value set to "id".
    /// </summary>
    /// <remarks>
    /// The default sorting order applied when retrieving data trees is by the "id".
    /// </remarks>
    public DataTreeParameters() => OrderBy = "id";

    /// <summary>
    /// Gets or sets the path used to filter data trees.
    /// </summary>
    public string? Path { get; set; }

    // TODO: Consider implementing this
    // public bool IncludeInvalidated { get; set; } = false;
}