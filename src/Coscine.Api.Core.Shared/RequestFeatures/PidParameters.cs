﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for pid requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class PidParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PidParameters"/> class with the default <c>OrderBy</c> value set to "suffix".
    /// </summary>
    /// <remarks>
    /// The default sorting order applied when retrieving pids is by the "id" property.
    /// </remarks>
    public PidParameters() => OrderBy = "id";

    /// <summary>
    /// Gets or sets a value indicating whether to include projects when retrieving pid information.
    /// </summary>
    /// <value>
    /// <c>true</c> to include projects; otherwise, <c>false</c>. The default value is <c>true</c>.
    /// </value>
    public bool IncludeProjects { get; set; } = true;

    /// <summary>
    /// Gets or sets a value indicating whether to include resources when retrieving pid information.
    /// </summary>
    /// <value>
    /// <c>true</c> to include resources; otherwise, <c>false</c>. The default value is <c>true</c>.
    /// </value>
    public bool IncludeResources { get; set; } = true;

    /// <summary>
    /// Gets or sets a value indicating whether to include deleted pid information.
    /// </summary>
    /// <value>
    /// <c>true</c> to include deleted project and deleted resource information; otherwise, <c>false</c>. The default value is <c>false</c>.
    /// </value>
    public bool IncludeDeleted { get; set; } = false;
}