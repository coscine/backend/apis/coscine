﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for API token requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class ApiTokenParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ApiTokenParameters"/> class with the default <c>OrderBy</c> value set to "IssuedAt".
    /// </summary>
    /// <remarks>
    /// The default sorting order applied when retrieving API tokens is by the "IssuedAt" date.
    /// </remarks>
    public ApiTokenParameters() => OrderBy = "IssuedAt";
}