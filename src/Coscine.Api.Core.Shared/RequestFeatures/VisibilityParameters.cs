namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for visibility settings, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class VisibilityParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="VisibilityParameters"/> class with the default <c>OrderBy</c> value set to "id".
    /// </summary>
    public VisibilityParameters() => OrderBy = "id";
}