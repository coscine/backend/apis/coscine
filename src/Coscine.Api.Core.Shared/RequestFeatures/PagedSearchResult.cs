﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents a paginated search result collection of items of type <typeparamref name="T"/>.
/// Inherits from <see cref="PagedEnumerable{T}"/> and implements <see cref="ISearchCategories"/>.
/// </summary>
/// <typeparam name="T">The type of items in the collection.</typeparam>
public class PagedSearchResult<T> : PagedEnumerable<T>, ISearchCategories where T : class
{
    /// <summary>
    /// Gets or sets the categories associated with the search result.
    /// </summary>
    public IEnumerable<SearchCategory>? Categories { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="PagedSearchResult{T}"/> class.
    /// </summary>
    /// <param name="items">The collection of items in the search result.</param>
    /// <param name="count">The total count of items in the entire collection.</param>
    /// <param name="pageNumber">The current page number.</param>
    /// <param name="pageSize">The size of each page.</param>
    /// <param name="categories">The categories associated with the search result.</param>
    public PagedSearchResult(IEnumerable<T> items, long count, int pageNumber, int pageSize, IEnumerable<SearchCategory>? categories)
        : base(items, count, pageNumber, pageSize)
    {
        Categories = categories;
    }
}