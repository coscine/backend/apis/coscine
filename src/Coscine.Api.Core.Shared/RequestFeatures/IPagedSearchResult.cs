﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents an interface for search categories.
/// </summary>
public interface ISearchCategories
{
    /// <summary>
    /// Gets or sets the collection of search categories.
    /// </summary>
    IEnumerable<SearchCategory>? Categories { get; set; }
}