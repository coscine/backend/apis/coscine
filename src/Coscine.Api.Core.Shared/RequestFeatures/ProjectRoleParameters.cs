namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for managing project roles, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class ProjectRoleParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ProjectRoleParameters"/> class with the default <c>OrderBy</c> value set to "id".
    /// </summary>
    public ProjectRoleParameters() => OrderBy = "id";
}