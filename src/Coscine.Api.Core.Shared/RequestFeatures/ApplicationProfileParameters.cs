﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for application profile requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class ApplicationProfileParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ApplicationProfileParameters"/> class with the default <c>OrderBy</c> value set to "displayName".
    /// </summary>
    /// <remarks>
    /// The default sorting order applied when retrieving application profiles is by the "displayName".
    /// </remarks>
    public ApplicationProfileParameters() => OrderBy = "displayName";

    /// <summary>
    /// Gets or sets the search term used to filter application profiles.
    /// </summary>
    public string? SearchTerm { get; set; }

    /// <summary>
    /// Gets or sets the language for which the application profiles are requested.
    /// </summary>
    public AcceptedLanguage Language { get; set; } = AcceptedLanguage.en;

    /// <summary>
    /// Gets or sets a value indicating whether to include modules in the application profiles.
    /// </summary>
    public bool? Modules { get; set; }
}