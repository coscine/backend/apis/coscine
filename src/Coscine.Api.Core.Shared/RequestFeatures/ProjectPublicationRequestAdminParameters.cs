namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for administrative operations related to projects publication requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class ProjectPublicationRequestAdminParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ProjectPublicationRequestAdminParameters"/> class with the default <c>OrderBy</c> value set to "name".
    /// </summary>
    public ProjectPublicationRequestAdminParameters() => OrderBy = "dateCreated";

    /// <summary>
    /// Override the max page size for this derived class to <c>250</c>.
    /// </summary>
    protected override int MaxPageSize => 250;
}
