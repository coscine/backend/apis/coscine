﻿using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for updating metadata in an administrative context.
/// </summary>
public class MetadataUpdateAdminParameters
{
    /// <summary>
    /// The definition for manipulating the RDF data.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    public RdfDefinitionForManipulationDto Definition { get; init; } = null!;
}