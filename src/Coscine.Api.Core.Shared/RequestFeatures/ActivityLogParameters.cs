﻿using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Validators;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for API token requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class ActivityLogParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ActivityLogParameters"/> class with the default <c>OrderBy</c> value set to "created".
    /// </summary>
    /// <remarks>
    /// The default sorting order applied when retrieving activity logs is the activity timestamp in a descending order.
    /// </remarks>
    public ActivityLogParameters() => OrderBy = "activityTimestamp_desc";

    /// <summary>
    /// The activity timestamp date before which activity logs should be retrieved.
    /// </summary>
    public DateTime? ActivityTimestampBefore { get; set; }

    /// <summary>
    /// The activity timestamp date after which activity logs should be retrieved.
    /// </summary>
    public DateTime? ActivityTimestampAfter { get; set; }

    /// <summary>
    /// The GUID for which activity logs should be retrieved. Can be a project, resource or other ID.
    /// </summary>
    public Guid? Guid { get; set; }

    /// <summary>
    /// The user ID for which activity logs should be retrieved.
    /// </summary>`
    public Guid? UserId { get; set; }

    /// <summary>
    /// The regular expression to filter activity logs' API path by. The regex must be a valid, already escaped string.
    /// </summary>
    /// <remarks>The regex must be a valid, already escaped string.</remarks>
    [ValidRegex]
    public string? RegularExpression { get; set; }

    /// <summary>
    /// The http method to filter activity logs by.
    /// </summary>
    public CoscineHttpMethod? HttpMethod { get; set; }
}