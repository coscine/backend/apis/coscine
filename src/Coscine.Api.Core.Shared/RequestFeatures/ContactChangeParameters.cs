﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for contact change requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class ContactChangeParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ContactChangeParameters"/> class with the default <c>OrderBy</c> value set to "editDate".
    /// </summary>
    /// <remarks>
    /// The default sorting order applied when retrieving contact changes is by the "editDate".
    /// </remarks>
    public ContactChangeParameters() => OrderBy = "editDate";

    /// <summary>
    /// Gets or sets the date before which the contact change was submitted.
    /// </summary>
    /// <remarks>
    /// This should be <c>ExpiredBefore</c>, but we are not saving this value.
    /// Using this to keep the business logic only in the services.
    /// </remarks>
    public DateTime EditDateBefore { get; set; } = DateTime.Now;
}