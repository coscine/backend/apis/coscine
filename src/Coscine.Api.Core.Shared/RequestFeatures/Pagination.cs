﻿using System.Text.Json;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents pagination information for a collection of items.
/// </summary>
public class Pagination
{
    /// <summary>
    /// Gets or sets the current page number.
    /// </summary>
    public int CurrentPage { get; set; }

    /// <summary>
    /// Gets or sets the total number of pages.
    /// </summary>
    public int TotalPages { get; set; }

    /// <summary>
    /// Gets or sets the number of items per page.
    /// </summary>
    public int PageSize { get; set; }

    /// <summary>
    /// Gets or sets the total count of items across all pages.
    /// </summary>
    public long TotalCount { get; set; }

    /// <summary>
    /// Gets a value indicating whether there is a previous page.
    /// </summary>
    public bool HasPrevious => CurrentPage > 1;

    /// <summary>
    /// Gets a value indicating whether there is a next page.
    /// </summary>
    public bool HasNext => CurrentPage < TotalPages;

    /// <summary>
    /// Overrides the default ToString method to return a JSON string representation of the Pagination object.
    /// </summary>
    /// <returns>A JSON string representing the Pagination object.</returns>
    public override string ToString()
    {
        return JsonSerializer.Serialize(this);
    }
}