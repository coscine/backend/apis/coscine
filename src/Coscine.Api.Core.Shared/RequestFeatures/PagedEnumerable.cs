﻿using System.Collections;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents a paginated collection of items of type <typeparamref name="T"/>.
/// Implements <see cref="IPagedEnumerable"/> and <see cref="IEnumerable{T}"/>.
/// </summary>
/// <typeparam name="T">The type of items in the collection.</typeparam>
public class PagedEnumerable<T> : IPagedEnumerable, IEnumerable<T> where T : class
{
    private readonly IEnumerable<T> _items;

    /// <summary>
    /// Gets the pagination information associated with the collection.
    /// </summary>
    public Pagination Pagination { get; }

    /// <summary>
    /// Initializes a new instance of the <see cref="PagedEnumerable{T}"/> class.
    /// </summary>
    /// <param name="items">The collection of items to paginate.</param>
    /// <param name="count">The total count of items in the entire collection.</param>
    /// <param name="pageNumber">The current page number.</param>
    /// <param name="pageSize">The size of each page.</param>
    public PagedEnumerable(IEnumerable<T> items, long count, int pageNumber, int pageSize)
    {
        Pagination = new Pagination
        {
            TotalCount = count,
            PageSize = pageSize,
            CurrentPage = pageNumber,
            TotalPages = (int)Math.Ceiling(count / (double)pageSize)
        };

        _items = items;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PagedEnumerable{T}"/> class using existing pagination information.
    /// </summary>
    /// <param name="items">The collection of items to paginate.</param>
    /// <param name="pagination">The pagination information.</param>
    public PagedEnumerable(IEnumerable<T> items, Pagination pagination)
    {
        Pagination = pagination;
        _items = items;
    }

    /// <summary>
    /// Creates a paginated collection of items from a source enumerable based on page number and page size.
    /// </summary>
    /// <param name="source">The source enumerable to paginate.</param>
    /// <param name="pageNumber">The current page number.</param>
    /// <param name="pageSize">The size of each page.</param>
    /// <returns>A paginated collection of items.</returns>
    public static PagedEnumerable<T> ToPagedEnumerable(IEnumerable<T> source, int pageNumber, int pageSize)
    {
        var count = source.Count();
        var items = source
            .Skip((pageNumber - 1) * pageSize)
            .Take(pageSize)
            .ToList();

        return new PagedEnumerable<T>(items, count, pageNumber, pageSize);
    }

    /// <inheritdoc/>
    public IEnumerator<T> GetEnumerator()
    {
        return _items.GetEnumerator();
    }

    /// <inheritdoc/>
    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable)_items).GetEnumerator();
    }
}