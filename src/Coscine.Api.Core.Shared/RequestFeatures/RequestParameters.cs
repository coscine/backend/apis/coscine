﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents a base class for request parameters used for pagination and ordering.
/// </summary>
public abstract class RequestParameters
{
    private const int minPageSize = 1;
    private const int minPageNumber = 1;

    private int _pageNumber = 1;  // Default page number
    private int _pageSize = 10;   // Default page size

    // Provide default max page size but allow derived classes to override.
    protected virtual int MaxPageSize => 50;

    /// <summary>
    /// The desired page number. Should be greater than or equal to 1. Default is 1.
    /// </summary>
    public int PageNumber
    {
        get => _pageNumber;
        set => _pageNumber = Math.Max(minPageNumber, value);
    }

    /// <summary>
    /// The desired page size. Should be between 1 and the maximum allowed page size (50). Default is 10.
    /// </summary>
    public virtual int PageSize // Set to virtual so that it can be overridden
    {
        get => _pageSize;
        set => _pageSize = Math.Min(MaxPageSize, Math.Max(minPageSize, value));
    }

    /// <summary>
    /// Gets or sets the field name used for ordering the results.
    /// The order is constructed by an order string.
    /// Use the property followed by "asc" or "desc" and separate properties by commas. Default is asc.
    /// </summary>
    /// <example>"propertyA asc, propertyB desc"</example>
    public string? OrderBy { get; set; } = "uri_asc";
}