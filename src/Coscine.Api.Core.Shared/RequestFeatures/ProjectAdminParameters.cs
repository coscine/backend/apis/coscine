﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for administrative operations related to projects, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class ProjectAdminParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ProjectAdminParameters"/> class with the default <c>OrderBy</c> value set to "name".
    /// </summary>
    public ProjectAdminParameters() => OrderBy = "name";

    /// <summary>
    /// Gets or sets a value indicating whether to filter top-level projects.
    /// </summary>
    public bool? TopLevel { get; set; } = false;

    /// <summary>
    /// Gets or sets a value indicating whether to include deleted projects in the results.
    /// </summary>
    public bool? IncludeDeleted { get; set; } = false;

    /// <summary>
    /// Gets or sets a value indicating whether to include quotas in the results.
    /// </summary>
    public bool? IncludeQuotas { get; set; } = false;

    /// <summary>
    /// Gets or sets a value indicating whether to include project publication requests in the results.
    /// </summary>
    public bool? IncludePublicationRequests { get; set; } = false;

    /// <summary>
    /// Override the max page size for this derived class to <c>250</c>.
    /// </summary>
    protected override int MaxPageSize => 250;

    /// <summary>
    /// Number of items per page. The maximum number of items per page is <c>250</c>.
    /// </summary>
    public override int PageSize
    {
        get => base.PageSize;
        set => base.PageSize = value;
    }
}
