﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents a search category with a name and count of occurrences.
/// </summary>
public class SearchCategory
{
    /// <summary>
    /// The name of the search category.
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// The count of occurrences for the search category.
    /// </summary>
    public long Count { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="SearchCategory"/> class with a specified name and count.
    /// </summary>
    /// <param name="name">The name of the search category.</param>
    /// <param name="count">The count of occurrences for the search category.</param>
    public SearchCategory(string name, long count)
    {
        Name = name;
        Count = count;
    }
}