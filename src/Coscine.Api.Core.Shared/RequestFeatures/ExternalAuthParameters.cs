﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for external authentication requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class ExternalAuthParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ExternalAuthParameters"/> class with the default <c>OrderBy</c> value set to "displayName".
    /// </summary>
    /// <remarks>
    /// The default sorting order applied when retrieving external authentication is by the "displayName".
    /// </remarks>
    public ExternalAuthParameters() => OrderBy = "displayName";
}