﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for terms of service requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class TermsOfServiceParameters : RequestParameters
{
    /// <summary>
    /// Gets or sets the version of the terms of service.
    /// </summary>
    public string? Version { get; set; }

    /// <summary>
    /// Initializes a new instance of the <see cref="TermsOfServiceParameters"/> class with the default <c>OrderBy</c> value set to "version".
    /// </summary>
    public TermsOfServiceParameters() => OrderBy = "version";
}