﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for project requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class ProjectParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ProjectParameters"/> class with the default <c>OrderBy</c> value set to "name".
    /// </summary>
    /// <remarks>
    /// The default sorting order applied when retrieving projects is by the "name".
    /// </remarks>
    public ProjectParameters() => OrderBy = "name";

    /// <summary>
    /// Gets or sets a value indicating whether to retrieve the organizations. 
    /// </summary>
    public bool? IncludeOrganizations { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether to retrieve only top-level projects.
    /// </summary>
    public bool? TopLevel { get; set; } = false;
}