﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for license requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class LicenseParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="LicenseParameters"/> class with the default <c>OrderBy</c> value set to "DisplayName".
    /// </summary>
    public LicenseParameters() => OrderBy = "DisplayName";
}