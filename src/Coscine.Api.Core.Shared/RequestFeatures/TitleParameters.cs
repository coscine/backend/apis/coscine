﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for title requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class TitleParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="TitleParameters"/> class with the default <c>OrderBy</c> value set to "displayName".
    /// </summary>
    public TitleParameters() => OrderBy = "displayName";
}