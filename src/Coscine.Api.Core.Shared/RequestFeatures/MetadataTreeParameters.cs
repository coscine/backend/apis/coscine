﻿using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Validators;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for metadata tree requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class MetadataTreeParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="MetadataTreeParameters"/> class with the default <c>OrderBy</c> value set to "id".
    /// </summary>
    public MetadataTreeParameters() => OrderBy = "id";

    /// <summary>
    /// Gets or sets the path of the metadata tree.
    /// </summary>
    [TrimStartCharacter('/')]
    [DisplayFormat(ConvertEmptyStringToNull = false)]
    // Do not set as required, so default will be empty string
    public string Path { get; set; } = string.Empty;

    /// <summary>
    /// Gets or sets the format of the RDF data.
    /// </summary>
    public RdfFormat Format { get; set; }

    /// <summary>
    /// Gets or sets if extracted metadata should be included.
    /// </summary>
    public bool IncludeExtractedMetadata { get; set; }

    /// <summary>
    /// Gets or sets if provenance metadata should be included.
    /// </summary>
    public bool IncludeProvenance { get; set; }
}
