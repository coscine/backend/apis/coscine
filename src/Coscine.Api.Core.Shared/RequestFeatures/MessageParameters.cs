using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for license requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class MessageParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="LicenseParameters"/> class with the default <c>OrderBy</c> value set to "DisplayName".
    /// </summary>
    public MessageParameters() => OrderBy = "StartDate";

    /// <summary>
    /// Will only return messages that have their start date after this date (UTC).
    /// </summary>
    public DateTimeOffset? StartDateAfter { get; set; }

    /// <summary>
    /// Will only return messages that have their start date before this date (UTC).
    /// </summary>
    public DateTimeOffset? StartDateBefore { get; set; }

    /// <summary>
    /// Will only return messages that have their end date after this date (UTC).
    /// </summary>
    public DateTimeOffset? EndDateAfter { get; set; }

    /// <summary>
    /// Will only return messages that have their end date before this date (UTC).
    /// </summary>
    public DateTimeOffset? EndDateBefore { get; set; }

    /// <summary>
    /// Filter messages by their type  (e.g. information, warning, error).
    /// </summary>
    public MessageType? Type { get; set; }

    /// <summary>
    /// Filter messages by text contained in the messages or title.
    /// </summary>
    public string? SearchTerm { get; set; }
}