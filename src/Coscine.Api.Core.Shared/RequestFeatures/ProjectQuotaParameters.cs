namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for managing project quotas, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class ProjectQuotaParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ProjectQuotaParameters"/> class with the default <c>OrderBy</c> value set to "id".
    /// </summary>
    public ProjectQuotaParameters() => OrderBy = "id";
}