namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for resources, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class ResourceParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ResourceParameters"/> class with the default <c>OrderBy</c> value set to "id".
    /// </summary>
    public ResourceParameters() => OrderBy = "id";
}