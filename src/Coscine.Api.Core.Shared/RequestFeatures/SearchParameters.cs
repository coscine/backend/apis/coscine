﻿using Coscine.Api.Core.Shared.Enums;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Parameters and filters for the search
/// </summary>
public class SearchParameters : RequestParameters
{
    /// <summary>
    /// The search query
    /// </summary>
    public string Query { get; set; } = "*";

    /// <summary>
    /// Set true for advanced Elasticsearch search syntax
    /// </summary>
    public bool UseAdvancedSyntax { get; set; }

    /// <summary>
    /// Set the used languages
    /// </summary>
    public IEnumerable<string>? Languages { get; set; }

    /// <summary>
    /// Set the category filter
    /// </summary>
    [EnumDataType(typeof(SearchCategoryType))]
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public SearchCategoryType Category { get; set; } = SearchCategoryType.None;

    /// <summary>
    /// Set the order filter
    /// </summary>
    public SearchParameters() => OrderBy = "score";
}