﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters used for retrieving admin metadata.
/// </summary>
public class MetadataGetAdminParameters
{
    /// <summary>
    /// Gets or sets the RDF format.
    /// </summary>
    public RdfFormat? Format { get; set; }
}