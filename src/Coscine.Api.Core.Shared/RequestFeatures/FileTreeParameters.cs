﻿using Coscine.Api.Core.Shared.Validators;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for file tree requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class FileTreeParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="FileTreeParameters"/> class with the default <c>OrderBy</c> value set to "id".
    /// </summary>
    /// <remarks>
    /// The default sorting order applied when retrieving file tree is by the "id".
    /// </remarks>
    public FileTreeParameters() => OrderBy = "id";

    /// <summary>
    /// Gets or sets the path of the file tree.
    /// </summary>
    [TrimStartCharacter('/')]
    [DisplayFormat(ConvertEmptyStringToNull = false)]
    public string Path { get; set; } = string.Empty; // Do not set as required, so default will be empty string

    // TODO: Consider implementing this
    // /// <summary>
    // /// Value indicating whether to list folder contents recursively, including subfolders.
    // /// </summary>
    // public bool Recursive { get; set; } = false;
}