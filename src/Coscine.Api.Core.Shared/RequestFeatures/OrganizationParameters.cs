using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for organization-related requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class OrganizationParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="OrganizationParameters"/> class.
    /// </summary>
    public OrganizationParameters()
    { }

    /// <summary>
    /// Gets or sets the search term used to filter organizations.
    /// </summary>
    public string? SearchTerm { get; set; }

    /// <summary>
    /// Language of name or text (dataPublicationService)
    /// </summary>
    public AcceptedLanguage Language { get; set; } //= AcceptedLanguage.de;

    /// <summary>
    /// Gets or sets the option to find only organizations having the publication service set.
    /// </summary>
    public bool FilterByPublicationService { get; set; } = false;
}