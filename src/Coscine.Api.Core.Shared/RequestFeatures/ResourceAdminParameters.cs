﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for managing resources, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class ResourceAdminParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ResourceAdminParameters"/> class with the default <c>OrderBy</c> value set to "id".
    /// </summary>
    public ResourceAdminParameters() => OrderBy = "id";

    /// <summary>
    /// Value indicating whether to include deleted resources.
    /// </summary>
    public bool? IncludeDeleted { get; set; } = false;

    /// <summary>
    /// Value indicating whether to include individual resource quotas.
    /// </summary>
    public bool? IncludeQuotas { get; set; } = false;

    /// <summary>
    /// Override the max page size for this derived class to <c>250</c>.
    /// </summary>
    protected override int MaxPageSize => 250;

    /// <summary>
    /// Number of items per page. The maximum number of items per page is <c>250</c>.
    /// </summary>
    public override int PageSize
    {
        get => base.PageSize;
        set => base.PageSize = value;
    }
}