﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters used for deployed graph requests.
/// </summary>
public class DeployedGraphParameters : RequestParameters
{ }