﻿using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for managing vocabularies, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class VocabularyParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="VocabularyParameters"/> class with the default <c>OrderBy</c> value set to "displayName".
    /// </summary>
    public VocabularyParameters() => OrderBy = "displayName";

    /// <summary>
    /// Gets or sets the search term used to filter vocabularies.
    /// </summary>
    public string? SearchTerm { get; set; }

    /// <summary>
    /// Gets or sets the language for which the vocabularies are requested.
    /// </summary>
    public AcceptedLanguage Language { get; set; } = AcceptedLanguage.en;

    /// <summary>
    /// Gets the fallback language if the requested language is not available.
    /// </summary>
    public readonly AcceptedLanguage FallbackLanguage = AcceptedLanguage.de;
}