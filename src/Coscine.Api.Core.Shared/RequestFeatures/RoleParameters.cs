namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for roles, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class RoleParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="RoleParameters"/> class with the default <c>OrderBy</c> value set to "id".
    /// </summary>
    public RoleParameters() => OrderBy = "id";
}