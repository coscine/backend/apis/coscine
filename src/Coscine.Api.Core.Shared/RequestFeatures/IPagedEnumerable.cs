﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents an interface for paged enumerables.
/// </summary>
public interface IPagedEnumerable
{
    /// <summary>
    /// Gets the pagination information for the enumerable.
    /// </summary>
    Pagination Pagination { get; }
}