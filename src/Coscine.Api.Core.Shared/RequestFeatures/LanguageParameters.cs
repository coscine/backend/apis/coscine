﻿namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for language requests, inheriting from <see cref="RequestParameters"/>.
/// </summary>
public class LanguageParameters : RequestParameters
{
    /// <summary>
    /// Initializes a new instance of the <see cref="LanguageParameters"/> class with the default <c>OrderBy</c> value set to "displayName".
    /// </summary>
    public LanguageParameters() => OrderBy = "displayName";
}