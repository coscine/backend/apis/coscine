﻿using Coscine.Api.Core.Shared.Validators;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.Core.Shared.RequestFeatures;

/// <summary>
/// Represents parameters for user search.
/// </summary>
public class UserSearchParameters
{
    /// <summary>
    /// Gets or sets the field by which the search results are ordered. Defaults to "firstName".
    /// </summary>
    public string OrderBy { get; set; } = "firstName";

    /// <summary>
    /// Gets or sets the search term.
    /// </summary>
    [Required(ErrorMessage = "{0} is a required field.")]
    [MinLengthAfterTrim(3, ErrorMessage = "The trimmed {0} must be at least {1} characters long.")]
    public string SearchTerm { get; set; } = null!;
}