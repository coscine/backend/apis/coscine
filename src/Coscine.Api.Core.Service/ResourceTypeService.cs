﻿using AutoMapper;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.Forbidden;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Extensions;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;
using Coscine.Api.Core.Shared.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System.Security.Claims;

namespace Coscine.Api.Core.Service;

public class ResourceTypeService(
    IProjectRepository projectRepository,
    IProjectQuotaRepository projectQuotaRepository,
    IResourceTypeRepository resourceTypeRepository,
    IExternalIdRepository externalIdRepository,
    IOrganizationRepository organizationRepository,
    IAuthenticatorService authenticatorService,
    ILogger<ResourceTypeService> logger,
    IMapper mapper,
    IAuthorizationService authorizationService,
    IEnumerable<IDataStorageRepository> dataStorageRepositories,
    IDataStorageRepositoryFactory dataStorageRepositoryFactory
    ) : IResourceTypeService
{
    private readonly IProjectRepository _projectRepository = projectRepository;
    private readonly IProjectQuotaRepository _projectQuotaRepository = projectQuotaRepository;
    private readonly IResourceTypeRepository _resourceTypeRepository = resourceTypeRepository;
    private readonly IExternalIdRepository _externalIdRepository = externalIdRepository;
    private readonly IOrganizationRepository _organizationRepository = organizationRepository;
    private readonly IAuthenticatorService _authenticatorService = authenticatorService;
    private readonly ILogger<ResourceTypeService> _logger = logger;
    private readonly IMapper _mapper = mapper;
    private readonly IAuthorizationService _authorizationService = authorizationService;
    private readonly IEnumerable<IDataStorageRepository> _dataStorageRepositories = dataStorageRepositories;
    private readonly IDataStorageRepositoryFactory _dataStorageRepositoryFactory = dataStorageRepositoryFactory;



    /// <summary>
    /// Retrieves a list of available resource types for a given project and user based on their affiliated organizations and project quotas.
    /// </summary>
    /// <param name="projectId">The ID of the project.</param>
    /// <param name="userId">The ID of the user.</param>
    /// <returns>An enumerable of available resource types for the specified project and user.</returns>
    public async Task<IEnumerable<ResourceType>> GetAvailableResourceTypesForProjectAndUserAsync(Guid projectId, Guid userId, bool trackChanges)
    {
        // Fetch affiliated organizations for the specified user
        var externalIds = await _externalIdRepository.GetAllAsync(userId, trackChanges);

        var orgsOfUser = new List<Organization>();

        foreach (var externalId in externalIds)
        {
            if (externalId.Organization is not null)
            {
                orgsOfUser.AddRange(await _organizationRepository.GetByShibbolethUrlAsync(externalId.Organization));
            }
        }

        if (externalIds.Any())
        {
            orgsOfUser.AddRange(await _organizationRepository.GetInstitutesByUserExternalIdAsync(externalIds));
        }

        var affiliatedOrganizations = orgsOfUser
            .Select(o => o.Uri.ToString())
            .Distinct();

        // Should nothing be affiliate, provide at least the any resource types by adding a star
        if (!affiliatedOrganizations.Any())
        {
            affiliatedOrganizations = ["*"];
        }

        // Set of rt available to the user over the orgs
        HashSet<string> listOfAvailableResourceTypes = [];

        // The loop is likel to be run at most 3 times
        foreach (var org in affiliatedOrganizations)
        {
            // Get all available rts for this org
            IEnumerable<string> rtsForOrg = _dataStorageRepositories
                .SelectMany(ds => ds.GetSpecificResourceTypes(org, ResourceTypeStatus.Active))
                .Where(x => x.SpecificTypeName is not null)
                .Select(x => x.SpecificTypeName!); // Asserting not null

            // Add to the set, ignoring duplicates
            listOfAvailableResourceTypes.UnionWith(rtsForOrg);
        }

        var projectQuotas = await _projectQuotaRepository.GetAllAsync(projectId, trackChanges: true);

        // Figure out, which project has quota or maxquota assigned
        IEnumerable<string> resourceTypesByQuota = projectQuotas
            .Where(x => x.Quota > 0 || x.MaxQuota > 0)
            .Where(x => x is not null)
            .Select(x => x.ResourceType.SpecificType!); // Asserting not null

        listOfAvailableResourceTypes.UnionWith(resourceTypesByQuota);

        IEnumerable<ResourceType> resourceTypes = (await _resourceTypeRepository.GetAllAsync(trackChanges: true))
            .Where(x => x.SpecificType is not null); // filter null for later

        // Return any rt, that is supported
        return resourceTypes.Where((resourceType) => listOfAvailableResourceTypes.Contains(resourceType.SpecificType!)); // Asserting not null
    }

    /// <summary>
    /// Retrieves available resource types information for a specific project and user.
    /// </summary>
    /// <param name="projectId">The ID of the project.</param>
    /// <param name="principal">The ClaimsPrincipal of the current user.</param>
    /// <param name="trackChanges">A flag indicating whether to track changes in retrieved entities.</param>
    /// <returns>An IEnumerable of <see cref="ResourceTypeInformationDto"/> for each available resource type for the project.</returns>
    /// <exception cref="ProjectNotFoundException">Thrown when the project with the specified ID is not found.</exception>
    /// <exception cref="ProjectForbiddenException">Thrown when the user is not authorized to retrieve the available resource types for the specified project.</exception>
    public async Task<IEnumerable<ResourceTypeInformationDto>> GetAvailableResourceTypesInformationForProjectAsync(Guid projectId, ClaimsPrincipal principal, bool trackChanges)
    {
        var user = await _authenticatorService.GetUserAsync(principal, trackChanges) ?? throw new UserNotFoundException(); ;

        var projectEntity = await _projectRepository.GetAsync(projectId, user.Id, trackChanges) ?? throw new ProjectNotFoundException(projectId);

        // Check role authorization
        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, projectEntity, ProjectOperations.ReadAvailableResourceTypes);

        var availableResourceTypesInformationDto = new List<ResourceTypeInformationDto>();
        var availableResourceTypes = await GetAvailableResourceTypesForProjectAndUserAsync(projectId, user.Id, false);
        foreach (var resourceType in availableResourceTypes)
        {
            availableResourceTypesInformationDto.Add(await GetResourceTypeInfoDto(resourceType));
        }

        return availableResourceTypesInformationDto;
    }

    /// <summary>
    /// Retrieves all global resource types information.
    /// </summary>
    /// <param name="trackChanges">A flag indicating whether to track changes in retrieved entities.</param>
    /// <returns>An IEnumerable of <see cref="ResourceTypeInformationDto"/> for each global resource type.</returns>
    public async Task<IEnumerable<ResourceTypeInformationDto>> GetAllResourceTypesInformationAsync(bool trackChanges)
    {
        var resourceTypes = await _resourceTypeRepository.GetAllAsync(trackChanges);

        var resourceTypesInformationDto = new List<ResourceTypeInformationDto>();

        foreach (var resourceType in resourceTypes)
        {
            try
            {
                resourceTypesInformationDto.Add(await GetResourceTypeInfoDto(resourceType));
            }
            catch (NoValidProviderBadRequest) // Only handle this type of exception
            {
            }
        }

        return resourceTypesInformationDto;
    }

    /// <summary>
    /// Retrieves information for a specific resource type.
    /// </summary>
    /// <param name="resourceTypeId">The ID of the resource type.</param>
    /// <param name="principal">The ClaimsPrincipal of the current user.</param>
    /// <param name="trackChanges">A flag indicating whether to track changes in retrieved entities.</param>
    /// <returns>A <see cref="ResourceTypeInformationDto"/> for the requested resource type.</returns>
    public async Task<ResourceTypeInformationDto> GetResourceTypeInformationAsync(Guid resourceTypeId, ClaimsPrincipal principal, bool trackChanges)
    {
        var resourceType = await _resourceTypeRepository.GetAsync(resourceTypeId, trackChanges) ?? throw new ResourceTypeNotFoundException(resourceTypeId);
        return await GetResourceTypeInfoDto(resourceType);
    }

    /// <summary>
    /// Helper method that builds a <see cref="ResourceTypeInformationDto"/> for a given <see cref="ResourceType"/>.
    /// </summary>
    /// <param name="resourceType">The resource type for which to retrieve information.</param>
    /// <returns>A <see cref="ResourceTypeInformationDto"/> for the provided resource type.</returns>
    private async Task<ResourceTypeInformationDto> GetResourceTypeInfoDto(ResourceType resourceType)
    {
        var dataStorage = _dataStorageRepositoryFactory.Create(resourceType.Id)
            ?? throw new NoValidProviderBadRequest(resourceType.DisplayName);

        var rtinfo = await dataStorage.GetResourceTypeInformationAsync();

        // Load the resource type information defined in the corresponding information JSON file for each resource type
        var resourceTypeInformationDto = _mapper.Map<ResourceTypeInformationDto>(rtinfo);

        resourceTypeInformationDto.Id = resourceType.Id;

        return resourceTypeInformationDto;
    }

    public async Task<ResourceTypeOptionsDto> GetResourceTypeOptionsDto(Resource resourceEntity, bool includeQuota = true)
    {
        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        return _mapper.Map<ResourceTypeOptionsDto>(
            await dataStorage.GetStorageInfoAsync(
                new StorageInfoParameters
                {
                    Resource = resourceEntity,
                    FetchUsedSize = includeQuota,
                    FetchAllocatedSize = includeQuota
                }));
    }
}