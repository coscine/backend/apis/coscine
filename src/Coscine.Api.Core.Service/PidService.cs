﻿using AutoMapper;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Coscine.Api.Core.Service.Extensions.Utility;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.Extensions.Options;
using System.Reactive.Subjects;
using System.Security.Claims;

namespace Coscine.Api.Core.Service;

public partial class PidService : IPidService
{
    private readonly IPidRepository _pidRepository;
    private readonly IProjectRepository _projectRepository;
    private readonly IResourceRepository _resourceRepository;
    private readonly IRoleRepository _roleRepository;
    private readonly IMapper _mapper;
    private readonly PidConfiguration _pidConfiguration;
    private readonly Subject<PidContactEventArgs> _pidContactConfirmation = new();
    private readonly Subject<PidContactEventArgs> _pidProjectContact = new();
    private readonly Subject<PidContactEventArgs> _pidResourceContact = new();

    public PidService(
        IPidRepository pidRepository,
        IProjectRepository projectRepository,
        IResourceRepository resourceRepository,
        IRoleRepository roleRepository,
        IObserverManager observerManager,
        IMapper mapper,
        IOptionsMonitor<PidConfiguration> pidConfiguration
    )
    {
        _pidContactConfirmation.SubscribeAll(observerManager.PidContactConfirmationObservers);
        _pidProjectContact.SubscribeAll(observerManager.PidProjectContactObservers);
        _pidResourceContact.SubscribeAll(observerManager.PidResourceContactObservers);

        _pidRepository = pidRepository;
        _projectRepository = projectRepository;
        _resourceRepository = resourceRepository;
        _roleRepository = roleRepository;
        _mapper = mapper;
        _pidConfiguration = pidConfiguration.CurrentValue;
    }

    public async Task<PagedEnumerable<PidDto>> GetPagedAsync(ClaimsPrincipal principal, PidParameters pidParameters, bool trackChanges)
    {
        var pids = await _pidRepository.GetPagedAsync(_pidConfiguration.Prefix, pidParameters, trackChanges: trackChanges);

        var pidDtos = _mapper.Map<IEnumerable<PidDto>>(pids);

        return new PagedEnumerable<PidDto>(pidDtos, pids.Pagination);
    }

    public async Task<PidDto> GetAsync(string prefix, string suffix, ClaimsPrincipal principal, bool trackChanges)
    {
        // Enforce so usage of the correct prefix
        if (!string.Equals(prefix, _pidConfiguration.Prefix, StringComparison.OrdinalIgnoreCase))
        {
            throw new PrefixNotSupportedBadRequestException(prefix);
        }

        var pid = await _pidRepository.GetAsync(prefix, suffix, verifyHandle: true, trackChanges)
            ?? throw new PidNotFoundException(prefix, suffix);

        return _mapper.Map<PidDto>(pid);
    }

    /// <summary>
    /// Sends an email to the owner of the project or resource specified by the provided PID.
    /// </summary>
    /// <param name="prefix">PID prefix representing the project or resource.</param>
    /// <param name="suffix">The PID suffix representing the project or resource identifier.</param>
    /// <param name="pidRequestDto">Data transfer object containing the details of the enquirer and the inquiry.</param>
    /// <returns>A Task that represents the asynchronous operation.</returns>
    /// <exception cref="RoleNotFoundException">Thrown when the appropriate role is not found.</exception>
    /// <exception cref="PidRecipientsNotFoundException">Thrown when the recipients for the project or resource PID inquiry are not found.</exception>
    /// <exception cref="PidNotFoundException">Thrown when the provided PID does not correspond to a project or resource.</exception>
    public async Task SendEmailToOwnerAsync(string prefix, string suffix, PidRequestDto pidRequestDto, bool trackChanges)
    {
        // Enforce so usage of the correct prefix
        if (!string.Equals(prefix, _pidConfiguration.Prefix, StringComparison.OrdinalIgnoreCase))
        {
            throw new PrefixNotSupportedBadRequestException(prefix);
        }

        var pid = await _pidRepository.GetAsync(prefix, suffix, verifyHandle: true, trackChanges)
            ?? throw new PidNotFoundException(prefix, suffix);

        if (!Guid.TryParse(suffix, out var suffixGuid))
        {
            throw new PidNotFoundException(prefix, suffix);
        }

        if (pid.Type is null)
        {
            throw new PidRecipientsNotFoundException(pid);
        }

        var ownerRole = _roleRepository.GetOwnerRole();

        var pidContactEventArgs = new PidContactEventArgs
        {
            Pid = $"{prefix}/{suffix}",
            EnquirerName = pidRequestDto.Name,
            EnquirerEmail = pidRequestDto.Email,
            EnquirerMessage = pidRequestDto.Message,
        };

        if (pid.Type == PidType.Project)
        {
            var projectEntity = await _projectRepository.GetIncludingDeletedAsync(suffixGuid, trackChanges: true)
                ?? throw new ProjectNotFoundException(suffixGuid);

            pidContactEventArgs.Recipients = GetRecipientsForProject(projectEntity, ownerRole)
                ?? throw new PidRecipientsNotFoundException(pid);
            pidContactEventArgs.ProjectName = projectEntity.ProjectName;

            // Contact the users owning the project entity
            _pidProjectContact.OnNext(pidContactEventArgs);
        }
        else if (pid.Type == PidType.Resource)
        {
            var resourceEntity = await _resourceRepository.GetIncludingDeletedAsync(suffixGuid, trackChanges: true)
                ?? throw new ResourceNotFoundException(suffixGuid);

            pidContactEventArgs.Recipients = GetRecipientsForResource(resourceEntity, ownerRole)
                ?? throw new PidRecipientsNotFoundException(pid);
            pidContactEventArgs.ProjectName = resourceEntity.ProjectResources.First().Project.ProjectName;
            pidContactEventArgs.ResourceName = resourceEntity?.ResourceName;

            // Contact the users owning the resource entity
            _pidResourceContact.OnNext(pidContactEventArgs);
        }

        // Handle requester email sent confirmation
        if (pidRequestDto.SendConfirmationEmail)
        {
            _pidContactConfirmation.OnNext(pidContactEventArgs);
        }
    }

    /// <summary>
    /// Retrieves a list of users who are recipients for a particular project.
    /// </summary>
    /// <param name="projectEntity">The specific project entity.</param>
    /// <param name="ownerRole">The user project role to search for.</param>
    /// <returns>A collection of users who are the recipients for the specified project.</returns>
    private static IEnumerable<User> GetRecipientsForProject(Project projectEntity, Role ownerRole)
    {
        return projectEntity.ProjectRoles
            .Where(pr => pr.RoleId.Equals(ownerRole.Id))
            .Select(pr => pr.User)
            .Distinct();
    }

    /// <summary>
    /// Retrieves a list of users who are recipients for a particular resource.
    /// </summary>
    /// <param name="resourceEntity">The specific resource entity.</param>
    /// <param name="ownerRole">The user project role to search for.</param>
    /// <returns>A collection of users who are the recipients for the specified resource.</returns>
    private static IEnumerable<User> GetRecipientsForResource(Resource resourceEntity, Role ownerRole)
    {
        return resourceEntity.ProjectResources
            .Select(projectResources => projectResources.Project)
            .SelectMany(project => GetRecipientsForProject(project, ownerRole))
            .Distinct();
    }
}
