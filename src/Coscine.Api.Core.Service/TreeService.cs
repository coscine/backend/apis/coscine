﻿using AutoMapper;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.Forbidden;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Repository.Contracts.Helpers;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Extensions;
using Coscine.Api.Core.Service.Helpers;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.QueryParameters;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using VDS.RDF;

namespace Coscine.Api.Core.Service;

public class TreeService(
    IProjectRepository projectRepository,
    IResourceRepository resourceRepository,
    IProvenanceRepository provenanceRepository,
    ITreeRepository treeRepository,
    IAuthenticatorService authenticatorService,
    IMapper mapper,
    IAuthorizationService authorizationService,
    IRdfRepositoryBase rdfRepositoryBase,
    IDataStorageRepositoryFactory dataStorageRepositoryFactory
) : ITreeService
{
    private readonly IProjectRepository _projectRepository = projectRepository;

    private readonly IResourceRepository _resourceRepository = resourceRepository;
    private readonly IProvenanceRepository _provenanceRepository = provenanceRepository;
    private readonly ITreeRepository _treeRepository = treeRepository;
    private readonly IAuthenticatorService _authenticatorService = authenticatorService;
    private readonly IMapper _mapper = mapper;
    private readonly IAuthorizationService _authorizationService = authorizationService;
    private readonly IRdfRepositoryBase _rdfRepositoryBase = rdfRepositoryBase;
    private readonly IDataStorageRepositoryFactory _dataStorageRepositoryFactory = dataStorageRepositoryFactory;

    public async Task<MetadataTreeDto> GetMetadataTreeAsync(Guid projectId, Guid resourceId, ClaimsPrincipal principal, MetadataTreeQueryParameters metadataTreeQueryParameters)
    {
        _ = await _projectRepository.GetAsync(projectId, trackChanges: true)
            ?? throw new ProjectNotFoundException(projectId);

        var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges: false)
            ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.ReadTree);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // Metadata of a resource cannot be read while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        var metadataTree = await _treeRepository.GetMetadataAsync(resourceId, metadataTreeQueryParameters);

        return _mapper.Map<MetadataTreeDto>(metadataTree);
    }

    public async Task<PagedEnumerable<MetadataTreeDto>> GetPagedMetadataTreeAsync(Guid projectId, Guid resourceId, ClaimsPrincipal principal, MetadataTreeParameters treeParameters)
    {
        _ = await _projectRepository.GetAsync(projectId, trackChanges: true)
            ?? throw new ProjectNotFoundException(projectId);

        var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges: false)
            ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.ReadTree);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // Metadata of a resource cannot be read while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        var metadataTree = await _treeRepository.GetNewestPagedMetadataAsync(resourceId, treeParameters);

        var metadataDtos = _mapper.Map<IEnumerable<MetadataTreeDto>>(metadataTree);

        return new PagedEnumerable<MetadataTreeDto>(metadataDtos, metadataTree.Pagination);
    }

    public async Task<PagedEnumerable<FileTreeDto>> GetPagedFileTreeAsync(
        Guid projectId,
        Guid resourceId,
        ClaimsPrincipal principal,
        FileTreeParameters treeParameters
    )
    {
        _ = await _projectRepository.GetAsync(projectId, trackChanges: true)
            ?? throw new ProjectNotFoundException(projectId);

        var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges: false)
            ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.ReadTree);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // Metadata of a resource cannot be read while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        // TODO: Move most of the following code to the TreeRepo, once RTs are repo ready.
        var entries = await dataStorage.ListAsync(new ListTreeParameters
        {
            Resource = resourceEntity,
            TreePath = treeParameters.Path,
            GeneratePresignedLinks = dataStorage.Capabilities.Contains(Capability.SupportsLinks)
        });

        var count = entries.Count();

        var fileTrees = entries
            .Skip((treeParameters.PageNumber - 1) * treeParameters.PageSize)
            .Take(treeParameters.PageSize)
            .Select(re =>
            {
                // Calculate the correct path, extension and parent directory.
                // The code assumes "" as the root prefix.
                // It also takes the name of a directory or blob/file into account.
                // The Path class from Microsoft uses the \ of Windows directories sometimes. As such it is mostly avoided.

                var pathSegments = re
                    .Path.Split('/', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries)
                    .ToList();

                var name = pathSegments.LastOrDefault() ?? "";

                // A folder hast to end with a / and a fileSize of 0, which is equivalent to no Body.
                var isFolder = re.Path.EndsWith('/') && !re.IsBlob;

                var extension = isFolder ? Path.GetExtension(re.Path).TrimStart('.') : null;

                var parentDirectory = string.Join('/', pathSegments.SkipLast(1));

                // Ensure that "" is the root.
                parentDirectory = parentDirectory.Length > 0 ? $"{parentDirectory}/" : "";

                return new FileTree
                {
                    Name = name,
                    Path = re.Path,
                    Directory = parentDirectory,
                    Extension = extension,
                    Size = re.ContentLength ?? 0,
                    HasChildren = isFolder,
                    ChangeDate = re.LastModifiedDate,
                    CreationDate = re.CreatedDate,
                    Hidden = re.Hidden ?? false || re.Path.StartsWith(".coscine/"),
                    Links = re.Links
                };
            })

            // TODO: Move it to the repository
            // We can't use the extension function, but I have prepared it in FileTreeExtensions.
            // RTs are as of now a service...
            //.SortFileTrees(treeParameters.OrderBy)
            // Numeric comparer takes number into account
            .OrderByDescending(x => x.HasChildren)
            .ThenBy(x => x.Path, new NumericStringComparer())
            .ToList();

        var fileDtos = _mapper.Map<IEnumerable<FileTreeDto>>(fileTrees);

        return new PagedEnumerable<FileTreeDto>(fileDtos, count, treeParameters.PageNumber, treeParameters.PageSize);
    }

    public async Task<MetadataTreeDto> CreateMetadataTreeAsync(
        Guid projectId,
        Guid resourceId,
        ClaimsPrincipal principal,
        MetadataTreeForCreationDto metadataTreeForCreationDto
    )
    {
        ForbiddenChars.CheckKey(metadataTreeForCreationDto.Path);

        // Null if administrator, since no real user object exists
        var user = !principal.IsInRole(ApiRoles.Administrator) ?
            await _authenticatorService.GetUserAsync(principal, trackChanges: false) : null;

        var resourceEntity =
            await _resourceRepository.GetAsync(projectId, resourceId, trackChanges: false)
            ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.CreateTree);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // New metadata of a resource cannot be created while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        var metadataTree = _mapper.Map<MetadataTree>(metadataTreeForCreationDto);

        var graphs = await _treeRepository.CreateMetadataGraphAsync(resourceEntity, user, metadataTree);

        if (resourceEntity.MetadataLocalCopy)
        {
            foreach (
                var graph in graphs.Where(x => !x.BaseUri.AbsolutePath.Contains("trellis", StringComparison.CurrentCultureIgnoreCase))
            )
            {
                await CopyMetadataLocalAsync(resourceEntity, graph);
            }
        }

        return _mapper.Map<MetadataTreeDto>(metadataTree);
    }

    public async Task<MetadataTreeDto> CreateExtractedMetadataTreeAsync(
        Guid projectId,
        Guid resourceId,
        ClaimsPrincipal principal,
        ExtractedMetadataTreeForCreationDto extractedMetadataTreeForCreationDto
    )
    {
        ForbiddenChars.CheckKey(extractedMetadataTreeForCreationDto.Path);

        // Null if administrator, since no real user object exists
        var user = !principal.IsInRole(ApiRoles.Administrator) ?
            await _authenticatorService.GetUserAsync(principal, trackChanges: false) : null;

        var resourceEntity =
            await _resourceRepository.GetAsync(projectId, resourceId, trackChanges: false)
            ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.CreateExtractionTree);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // New metadata of a resource cannot be created while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        var metadataTree = _mapper.Map<MetadataTree>(extractedMetadataTreeForCreationDto);

        var graphs = await _treeRepository.CreateMetadataGraphAsync(resourceEntity, user, metadataTree);


        if (resourceEntity.MetadataLocalCopy)
        {
            foreach (
                var graph in graphs.Where(x => !x.BaseUri.AbsolutePath.Contains("trellis", StringComparison.CurrentCultureIgnoreCase))
            )
            {
                await CopyMetadataLocalAsync(resourceEntity, graph);
            }
        }

        return _mapper.Map<MetadataTreeDto>(metadataTree);
    }

    public async Task UpdateMetadataTreeAsync(
        Guid projectId,
        Guid resourceId,
        ClaimsPrincipal principal,
        MetadataTreeForUpdateDto metadataTreeForUpdateDto
    )
    {
        ForbiddenChars.CheckKey(metadataTreeForUpdateDto.Path);

        // Null if administrator, since no real user object exists
        var user = !principal.IsInRole(ApiRoles.Administrator) ?
            await _authenticatorService.GetUserAsync(principal, trackChanges: false) : null;

        var resourceEntity =
            await _resourceRepository.GetAsync(projectId, resourceId, trackChanges: false)
            ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.UpdateTree);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // Metadata of a resource cannot be updated while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        var metadataTree = _mapper.Map<MetadataTree>(metadataTreeForUpdateDto);

        var graphs = await _treeRepository.UpdateMetadataGraphAsync(resourceEntity, user, metadataTree);

        if (resourceEntity.MetadataLocalCopy)
        {
            foreach (
                var graph in graphs.Where(x => !x.BaseUri.AbsolutePath.Contains("trellis", StringComparison.CurrentCultureIgnoreCase))
            )
            {
                await CopyMetadataLocalAsync(resourceEntity, graph);
            }
        }
    }

    public async Task UpdateExtractedMetadataTreeAsync(
        Guid projectId,
        Guid resourceId,
        ClaimsPrincipal principal,
        ExtractedMetadataTreeForUpdateDto extractedMetadataTreeForUpdateDto
    )
    {
        ForbiddenChars.CheckKey(extractedMetadataTreeForUpdateDto.Path);

        // Null if administrator, since no real user object exists
        var user = !principal.IsInRole(ApiRoles.Administrator) ?
            await _authenticatorService.GetUserAsync(principal, trackChanges: false) : null;

        var resourceEntity =
            await _resourceRepository.GetAsync(projectId, resourceId, trackChanges: false)
            ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.UpdateExtractionTree);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // Metadata of a resource cannot be updated while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        var metadataTree = _mapper.Map<MetadataTree>(extractedMetadataTreeForUpdateDto);

        var graphs = await _treeRepository.UpdateMetadataGraphAsync(resourceEntity, user, metadataTree);

        if (dataStorage.Capabilities.Contains(Capability.SupportsLocalMetadataCopy) && resourceEntity.MetadataLocalCopy)
        {
            foreach (var graph in graphs.Where(x => !x.BaseUri.AbsolutePath.Contains("trellis", StringComparison.CurrentCultureIgnoreCase)))
            {
                // Will check if it can copy on its own
                await CopyMetadataLocalAsync(resourceEntity, graph);
            }
        }
    }

    public async Task CopyMetadataLocalAsync(Resource resourceEntity, IGraph graph)
    {
        // Check if we are using a patch graph
        if (graph is IPatchGraph pGraph)
        {
            // Get whole graph
            var wholeGraph = await _rdfRepositoryBase.GetGraph(graph.BaseUri);

            // Assert patch triples
            foreach (var t in pGraph.AssertList)
            {
                wholeGraph.Assert(t);
            }

            // Retract patch triples
            foreach (var t in pGraph.RetractList)
            {
                wholeGraph.Retract(t);
            }

            // Use the whole graph as ref
            graph = wholeGraph;
        }

        // Generate the key by using the base URI of the graph
        var sourceString = graph.BaseUri.AbsoluteUri;
        var removeString = RdfUris.CoscineResources.ToString();

        var index = sourceString.IndexOf(removeString);

        // Remove the first occurrence of the string
        var cleanPath = (index < 0) ? sourceString : sourceString.Remove(index, removeString.Length);

        var key = $".coscine/metadata/resource/{cleanPath}.json";

        // Get the jsonLd of the graph
        var jsonLd = _rdfRepositoryBase.WriteGraph(graph, RdfFormat.JsonLd);

        // Create a stream for upload
        await using var stream = new MemoryStream();
        var writer = new StreamWriter(stream);

        writer.Write(jsonLd);
        writer.Flush();
        stream.Position = 0;

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // Metadata of a resource cannot be copied locally while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        // Upload the local copy
        await dataStorage.CreateAsync(new CreateBlobParameters { Resource = resourceEntity, Blob = stream, Path = key });
    }


    public async Task DeleteMetadataTreeAsync(Guid projectId, Guid resourceId, ClaimsPrincipal principal, MetadataTreeForDeletionDto metadataTreeForDeletionDto)
    {
        var resourceEntity =
            await _resourceRepository.GetAsync(projectId, resourceId, trackChanges: false)
            ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.DeleteTree);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // Metadata of a resource cannot be deleted while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        var provenanceGraph = await _provenanceRepository.CreateInvalidation(
            resourceId,
            metadataTreeForDeletionDto.Path,
            _treeRepository.GetOrCreateMetadataId(resourceId, metadataTreeForDeletionDto.Path, GraphType.Metadata, metadataTreeForDeletionDto.Version),
            type: "metadata",
            metadataTreeForDeletionDto.InvalidatedBy
        );

        if (resourceEntity.MetadataLocalCopy)
        {
            await CopyMetadataLocalAsync(resourceEntity, provenanceGraph);
        }
    }
}
