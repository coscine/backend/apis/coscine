using AutoMapper;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service;

public class MessageService(IMessageRepository messageRepository, INocRepository nocRepository, IMapper mapper) : IMessageService
{
    private readonly INocRepository _nocRepository = nocRepository;
    private readonly IMessageRepository _messageRepository = messageRepository;
    private readonly IMapper _mapper = mapper;

    public async Task<PagedEnumerable<MessageDto>> GetPagedNocMessages(MessageParameters messageParameters)
    {
        var messagesFromNoc = await _nocRepository.GetPagedFromNocAsync(messageParameters);

        var messagesDto = _mapper.Map<IEnumerable<MessageDto>>(messagesFromNoc);

        return new PagedEnumerable<MessageDto>(messagesDto, messagesFromNoc.Pagination);
    }
    
    public async Task<PagedEnumerable<MessageDto>> GetInternalMessages(MessageParameters messageParameters)
    {
        var messagesFromConsul = await _messageRepository.GetPagedFromConsulAsync(messageParameters);

        var messagesDto = _mapper.Map<IEnumerable<MessageDto>>(messagesFromConsul);

        return new PagedEnumerable<MessageDto>(messagesDto, messagesFromConsul.Pagination);
    }
}