using AutoMapper;
using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Coscine.Api.Core.Service.Extensions;
using Coscine.Api.Core.Service.Extensions.Utility;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using System.Reactive.Subjects;
using System.Security.Claims;

namespace Coscine.Api.Core.Service;

public class ProjectInvitationService : IProjectInvitationService
{
    private readonly IRepositoryContextLoader _repositoryContextLoader;
    private readonly IProjectRepository _projectRepository;
    private readonly IProjectInvitationRepository _projectInvitationRepository;
    private readonly IRoleRepository _roleRepository;
    private readonly IAuthenticatorService _authenticatorService;
    private readonly IProjectRoleService _projectRoleService;
    private readonly IMapper _mapper;
    private readonly IAuthorizationService _authorizationService;
    private readonly Subject<ProjectInvitationCreationEventArgs> _projectInvitationCreated = new();

    public ProjectInvitationService(
        IRepositoryContextLoader repositoryContextLoader,
        IProjectRepository projectRepository,
        IProjectInvitationRepository projectInvitationRepository,
        IRoleRepository roleRepository,
        IAuthenticatorService authenticatorService,
        IProjectRoleService projectRoleService,
        IMapper mapper,
        IAuthorizationService authorizationService,
        IObserverManager observerManager
    )
    {
        _repositoryContextLoader = repositoryContextLoader;
        _projectRepository = projectRepository;
        _projectInvitationRepository = projectInvitationRepository;
        _roleRepository = roleRepository;
        _authenticatorService = authenticatorService;
        _projectRoleService = projectRoleService;
        _mapper = mapper;
        _authorizationService = authorizationService;

        _projectInvitationCreated.SubscribeAll(observerManager.ProjectInvitationCreationObservers);
    }

    public async Task<ProjectInvitationDto> InviteUserToProject(Guid projectId, ClaimsPrincipal principal, ProjectInvitationForProjectManipulationDto projectInvitationForProjectManipulationDto)
    {
        var user = await _authenticatorService.GetUserAsync(principal, trackChanges: false) ?? throw new UserNotFoundException();

        var projectEntity = await _projectRepository.GetAsync(projectId, user.Id, trackChanges: false) ?? throw new ProjectNotFoundException(projectId);
        _ = await _roleRepository.GetAsync(projectInvitationForProjectManipulationDto.RoleId, trackChanges: false) ?? throw new RoleNotFoundException(projectInvitationForProjectManipulationDto.RoleId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, projectEntity, ProjectOperations.CreateInvitation);

        if (await _projectInvitationRepository.HasValidInvitationAsync(projectId, projectInvitationForProjectManipulationDto.Email, trackChanges: false))
        {
            throw new HasValidInvitationBadRequest(projectId, projectInvitationForProjectManipulationDto.Email);
        }

        foreach (var invitation in await _projectInvitationRepository.GetAllExpiredInvitationsAsync(projectId, trackChanges: false))
        {
            _projectInvitationRepository.Delete(invitation);
        }

        var token = Guid.NewGuid();
        var projectInvitationEntity = _mapper.Map<Invitation>(projectInvitationForProjectManipulationDto);

        projectInvitationEntity.Issuer = user.Id;
        projectInvitationEntity.Expiration = DateTime.UtcNow.AddDays(7);
        projectInvitationEntity.Token = token;
        projectInvitationEntity.Project = projectId;

        _projectInvitationRepository.Create(projectInvitationEntity);

        await _repositoryContextLoader.SaveAsync();

        _projectInvitationCreated.OnNext(new() { Project = projectEntity, User = user, Invitation = projectInvitationEntity });

        var projectInvitationDto = _mapper.Map<ProjectInvitationDto>(projectInvitationEntity);
        projectInvitationDto.Issuer = _mapper.Map<PublicUserDto>(user);

        return projectInvitationDto;
    }

    public async Task DeleteProjectInvitationOfProject(Guid projectId, Guid projectInvitationId, ClaimsPrincipal principal, bool trackChanges)
    {
        var projectInvitationEntity = await _projectInvitationRepository.GetAsync(projectId, projectInvitationId, trackChanges) ?? throw new ProjectInvitationNotFoundException(projectInvitationId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, projectInvitationEntity, ProjectInvitationOperations.Delete);

        _projectInvitationRepository.Delete(projectInvitationEntity);
        await _repositoryContextLoader.SaveAsync();
    }

    public async Task<PagedEnumerable<ProjectInvitationDto>> GetPagedProjectInvitations(Guid projectId, ProjectInvitationParameters projectInvitationParameters, bool trackChanges)
    {
        var projectInvitationsDb = await _projectInvitationRepository.GetPagedAsync(projectId, projectInvitationParameters, trackChanges);

        var projectInvitationsDto = _mapper.Map<IEnumerable<ProjectInvitationDto>>(projectInvitationsDb);

        return new PagedEnumerable<ProjectInvitationDto>(projectInvitationsDto, projectInvitationsDb.Pagination);
    }

    public async Task<ProjectInvitationDto> GetProjectInvitationById(Guid projectId, Guid projectInvitationId, bool trackChanges)
    {
        var projectInvitationEntity = await _projectInvitationRepository.GetAsync(projectId, projectInvitationId, trackChanges)
            ?? throw new ProjectInvitationNotFoundException(projectInvitationId);

        return _mapper.Map<ProjectInvitationDto>(projectInvitationEntity);
    }

    public async Task ResolveInvitation(ClaimsPrincipal principal, ProjectInvitationResolveDto projectInvitationResolveDto, bool trackChanges)
    {
        var projectInvitationEntity = await _projectInvitationRepository.GetAsync(projectInvitationResolveDto.Token, trackChanges)
            ?? throw new ProjectInvitationNotFoundException(projectInvitationResolveDto.Token);

        var userToAdd = await _authenticatorService.GetUserAsync(principal, trackChanges: false) ?? throw new UserNotFoundException();

        // Check if the invitation has already expired
        if (projectInvitationEntity.Expiration < DateTime.UtcNow)
        {
            throw new ProjectInvitationExpiredBadException(projectInvitationEntity.Expiration);
        }

        var projectEntity = await _projectRepository.GetAsync(projectInvitationEntity.Project, trackChanges)
            ?? throw new ProjectNotFoundException(projectInvitationEntity.Project);

        // Check if the inviter can do the invite
        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, projectInvitationEntity, ProjectInvitationOperations.Resolve);

        // Let the ProjectRoleService do the heavy lifting
        await _projectRoleService.CreateProjectRoleForProject(projectEntity.Id, principal, new() { RoleId = projectInvitationEntity.Role, UserId = userToAdd.Id });

        // Delete Invitation
        _projectInvitationRepository.Delete(projectInvitationEntity);

        await _repositoryContextLoader.SaveAsync();
    }
}