﻿using AutoMapper;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.Forbidden;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Extensions;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.QueryParameters;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace Coscine.Api.Core.Service;

public class ProvenanceService(
    IResourceRepository resourceRepository,
    IProvenanceRepository provenanceRepository,
    ITreeRepository treeRepository,
    ITreeService treeService,
    IMapper mapper,
    IAuthorizationService authorizationService,
    IDataStorageRepositoryFactory dataStorageRepositoryFactory
) : IProvenanceService
{
    private readonly IResourceRepository _resourceRepository = resourceRepository;
    private readonly IProvenanceRepository _provenanceRepository = provenanceRepository;
    private readonly ITreeRepository _treeRepository = treeRepository;
    private readonly ITreeService _treeService = treeService;
    private readonly IMapper _mapper = mapper;
    private readonly IAuthorizationService _authorizationService = authorizationService;
    private readonly IDataStorageRepositoryFactory _dataStorageRepositoryFactory = dataStorageRepositoryFactory;

    public async Task<ProvenanceDto> GetProvenanceAsync(Guid projectId, Guid resourceId, ClaimsPrincipal principal, ProvenanceQueryParameters provenanceQueryParameters)
    {
        var resourceEntity =
            await _resourceRepository.GetAsync(projectId, resourceId, trackChanges: false)
        ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.ReadProvenance);

        var metadataTree = await _treeRepository.GetMetadataAsync(resourceId, new MetadataTreeQueryParameters()
        {
            Path = provenanceQueryParameters.Path,
            Format = Shared.Enums.RdfFormat.Turtle,
            IncludeExtractedMetadata = false,
            Version = provenanceQueryParameters.Version,
            IncludeProvenance = true
        });

        return _mapper.Map<ProvenanceDto>(metadataTree?.Provenance);
    }

    public async Task UpdateProvenanceAsync(Guid projectId, Guid resourceId, ClaimsPrincipal principal, ProvenanceForUpdateDto provenanceForUpdateDto)
    {
        var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges: false)
            ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.UpdateProvenance);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // Metadata of a resource cannot be updated while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        var provenance = _mapper.Map<Provenance>(provenanceForUpdateDto);

        var provenanceGraphs = await _provenanceRepository.AddProvenanceInformation(
            provenanceForUpdateDto.Id,
            createNewDataGraph: false,
            createNewMetadataGraph: false,
            provenance: provenance
        );

        foreach (
            var graph in provenanceGraphs.Where(x => !x.BaseUri.AbsolutePath.Contains("trellis", StringComparison.CurrentCultureIgnoreCase))
        )
        {
            // Will check if it can copy on its own
            await _treeService.CopyMetadataLocalAsync(resourceEntity, graph);
        }
    }
}
