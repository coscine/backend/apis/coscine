﻿using AutoMapper;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service;

public class DisciplineService(IDisciplineRepository disciplineRepository, IMapper mapper) : IDisciplineService
{
    private readonly IDisciplineRepository _disciplineRepository = disciplineRepository;
    private readonly IMapper _mapper = mapper;

    public async Task<PagedEnumerable<DisciplineDto>> GetPagedDisciplines(DisciplineParameters disciplineParameters, bool trackChanges)
    {
        var disciplinesDb = await _disciplineRepository.GetPagedAsync(disciplineParameters, trackChanges);
        var disciplinesDto = _mapper.Map<IEnumerable<DisciplineDto>>(disciplinesDb);

        return new PagedEnumerable<DisciplineDto>(disciplinesDto, disciplinesDb.Pagination);
    }

    public async Task<DisciplineDto> GetDisciplineById(Guid id, bool trackChanges)
    {
        var disciplineEntity = await _disciplineRepository.GetAsync(id, trackChanges) ?? throw new DisciplineNotFoundException(id);

        return _mapper.Map<DisciplineDto>(disciplineEntity);
    }
}