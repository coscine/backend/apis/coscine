﻿using AutoMapper;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.Forbidden;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Coscine.Api.Core.Service.Extensions;
using Coscine.Api.Core.Service.Extensions.Utility;
using Coscine.Api.Core.Service.Helpers;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Reactive.Subjects;

namespace Coscine.Api.Core.Service;

public sealed partial class ResourceService : IResourceService
{
    private readonly IRepositoryContextLoader _repositoryContextLoader;
    private readonly IProjectRepository _projectRepository;
    private readonly IResourceRepository _resourceRepository;
    private readonly IResourceDisciplineRepository _resourceDisciplineRepository;
    private readonly IResourceTypeRepository _resourceTypeRepository;
    private readonly IHandleRepository _handleRepository;
    private readonly IVisibilityRepository _visibilityRepository;
    private readonly IDisciplineRepository _disciplineRepository;
    private readonly ILicenseRepository _licenseRepository;
    private readonly IRoleRepository _roleRepository;
    private readonly IAuthenticatorService _authenticatorService;
    private readonly IResourceTypeService _resourceTypeService;
    private readonly IQuotaService _quotaService;
    private readonly IMapper _mapper;
    private readonly ILogger<ResourceService> _logger;
    private readonly IAuthorizationService _authorizationService;
    private readonly IHandleService _handleService;
    private readonly IDataStorageRepositoryFactory _dataStorageRepositoryFactory;
    private readonly IHttpContextAccessor _context;

    private readonly PidConfiguration _pidConfiguration;

    private readonly Subject<ResourceCreatedEventArgs> _resourceCreated = new();
    private readonly Subject<ResourceMaintenanceModeEnabledEventArgs> _resourceMaintenanceModeEnabled = new();
    private readonly Subject<ResourceMaintenanceModeDisabledEventArgs> _resourceMaintenanceModeDisabled = new();
    private readonly Subject<ResourceDeletedEventArgs> _resourceDeleted = new();

    // The service can retrieve other services using dependency injection.
    public ResourceService(
        IRepositoryContextLoader repositoryContextLoader,
        IProjectRepository projectRepository,
        IResourceRepository resourceRepository,
        IResourceDisciplineRepository resourceDisciplineRepository,
        IResourceTypeRepository resourceTypeRepository,
        IHandleRepository handleRepository,
        IVisibilityRepository visibilityRepository,
        IDisciplineRepository disciplineRepository,
        ILicenseRepository licenseRepository,
        IRoleRepository roleRepository,
        IAuthenticatorService authenticatorService,
        IResourceTypeService resourceTypeService,
        IQuotaService quotaService,
        IMapper mapper,
        ILogger<ResourceService> logger,
        IAuthorizationService authorizationService,
        IObserverManager observerManager,
        IOptionsMonitor<PidConfiguration> pidConfiguration,
        IHandleService handleService,
        IDataStorageRepositoryFactory dataStorageRepositoryFactory,
        IHttpContextAccessor context
    )
    {
        _repositoryContextLoader = repositoryContextLoader;
        _projectRepository = projectRepository;
        _resourceRepository = resourceRepository;
        _resourceDisciplineRepository = resourceDisciplineRepository;
        _resourceTypeRepository = resourceTypeRepository;
        _handleRepository = handleRepository;
        _visibilityRepository = visibilityRepository;
        _disciplineRepository = disciplineRepository;
        _licenseRepository = licenseRepository;
        _roleRepository = roleRepository;
        _authenticatorService = authenticatorService;
        _resourceTypeService = resourceTypeService;
        _quotaService = quotaService;
        _mapper = mapper;
        _logger = logger;
        _authorizationService = authorizationService;
        _handleService = handleService;
        _dataStorageRepositoryFactory = dataStorageRepositoryFactory;
        _context = context;

        _pidConfiguration = pidConfiguration.CurrentValue;

        _resourceCreated.SubscribeAll(observerManager.ResourceCreationObservers);
        _resourceMaintenanceModeEnabled.SubscribeAll(observerManager.ResourceMaintenanceModeEnabledObservers);
        _resourceMaintenanceModeDisabled.SubscribeAll(observerManager.ResourceMaintenanceModeDisabledObservers);
        _resourceDeleted.SubscribeAll(observerManager.ResourceDeletionObservers);
    }

    public async Task<ResourceDto> CreateResourceForProjectAsync(Guid projectId, ResourceForCreationDto resourceForCreationDto)
    {
        var user = await _authenticatorService.GetUserAsync(_context.HttpContext.User, trackChanges: false) ?? throw new UserNotFoundException();

        var projectEntity = await _projectRepository.GetAsync(projectId, trackChanges: true)
            ?? throw new ProjectNotFoundException(projectId);

        // Check role authorization
        await _authorizationService.AuthorizeAndThrowIfUnauthorized(_context.HttpContext.User, projectEntity, ProjectOperations.CreateResource);

        // Apply Email restrictions
        if (string.IsNullOrWhiteSpace(user.EmailAddress))
        {
            throw new ResourceForbiddenException("Users without a verified email address are not allowed to create resources.");
        }

        // Apply organization specific resource type restrictions
        var resourceTypeEntity = await _resourceTypeRepository.GetAsync(resourceForCreationDto.ResourceTypeId, trackChanges: true)
            ?? throw new ResourceTypeNotFoundException(resourceForCreationDto.ResourceTypeId);

        var availableResourceTypes = await _resourceTypeService.GetAvailableResourceTypesForProjectAndUserAsync(projectId, user.Id, trackChanges: false);
        if (!availableResourceTypes.Any(rt => rt.Id.Equals(resourceTypeEntity.Id)))
        {
            throw new ResourceForbiddenException("The user is not authorized to add a new resource of this type to the selected project!");
        }

        // Apply GitLab specific restrictions
        if (resourceTypeEntity.Type?.Equals("gitlab") == true)
        {
            var gitlabOptions = (resourceForCreationDto.ResourceTypeOptions?.GitlabResourceTypeOptions)
                ?? throw new InvalidResourceTypeOptionsBadRequestException("GitLab resource type options are missing.");
            if (!gitlabOptions.TosAccepted)
            {
                throw new InvalidResourceTypeOptionsBadRequestException("GitLab's Terms of Service must be accepted.");
            }
        }

        var resourceEntity = _mapper.Map<Resource>(resourceForCreationDto);
        resourceEntity.Id = Guid.NewGuid(); // Set the new resource id manually for interaction with external systems, before it's written to the database
        resourceEntity.Archived = "0"; // Set for in memory database

        // Attach the resource type
        resourceEntity.Type = resourceTypeEntity;

        // Attach the resource to its project
        resourceEntity.ProjectResources.Add(new ProjectResource { Project = projectEntity, Resource = resourceEntity, });

        // Create database entry for resource
        _resourceRepository.Create(resourceEntity, user.Id);

        // Add visibility
        resourceEntity.Visibility = await _visibilityRepository.GetAsync(resourceForCreationDto.Visibility.Id, trackChanges: true)
            ?? throw new VisibilityNotFoundException(resourceForCreationDto.Visibility.Id);

        // Add license if needed
        if (resourceForCreationDto.License is not null)
        {
            resourceEntity.License = await _licenseRepository.GetAsync(resourceForCreationDto.License.Id, trackChanges: true)
                ?? throw new LicenseNotFoundException(resourceForCreationDto.License.Id);
        }

        // Add disciplines
        foreach (var discipline in resourceForCreationDto.Disciplines)
        {
            var disciplineEntity = await _disciplineRepository.GetAsync(discipline.Id, trackChanges: true)
                ?? throw new DisciplineNotFoundException(discipline.Id);
            resourceEntity.ResourceDisciplines.Add(
                new ResourceDiscipline { Discipline = disciplineEntity, Resource = resourceEntity }
            );
        }

        // Execute the external creation interaction        
        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        var createDataStorageParameters = _mapper.Map<CreateDataStorageParameters>(resourceForCreationDto.ResourceTypeOptions);

        if (dataStorage.Capabilities.Contains(Capability.UsesQuota))
        {
            // Validate the desired resource reserved quota
            var desiredResourceReservedQuotaDto = _quotaService.ExtractDesiredResourceQuotaForResource(resourceTypeEntity, resourceForCreationDto.ResourceTypeOptions);

            await _quotaService.ValidateResourceQuotaOnCreationAsync(projectId, resourceTypeEntity.Id, desiredResourceReservedQuotaDto);

            long? desiredResourceReservedQuotaInGiB = desiredResourceReservedQuotaDto is not null
                ? QuotaHelper.ConvertCapacityUnits(desiredResourceReservedQuotaDto, QuotaUnit.GibiBYTE)
                : null;

            createDataStorageParameters.Quota = desiredResourceReservedQuotaInGiB;
        }

        // Create PID for resource
        var handleValues = _handleRepository.GenerateResourceHandleValues(resourceEntity);
        var handle = new Handle { Values = handleValues };
        await _repositoryContextLoader.CreateExecutionStrategy().ExecuteAsync(async () =>
        {
            using var transaction = await _repositoryContextLoader.BeginTransactionAsync();
            try
            {
                // Create db Entires and create the external dependency
                await dataStorage.CreateAsync(resourceEntity, createDataStorageParameters);

                // Finalize and save everything to the database
                await _repositoryContextLoader.SaveAsync();

                // Calling update will trigger the creation of the PID in the handle service
                await _handleRepository.UpdateAsync(_pidConfiguration.Prefix, resourceEntity.Id.ToString(), handle);

                // Fetch the handle from the handle service to get the PID and ensure it was created
                handle = await _handleRepository.GetAsync(_pidConfiguration.Prefix, resourceEntity.Id.ToString());

                if (handle is null)
                {
                    _logger.LogWarning("PID for resource with id: {resourceEntityId} could not be created!", resourceEntity.Id);
                }

                await _resourceRepository.CreateGraphsAsync(resourceEntity);

                await transaction.CommitAsync();
            }
            catch (Exception)
            {
                // Ef is rolledback automatically

                // Rollback graph changes
                await _resourceRepository.DeleteGraphsAsync(resourceEntity);

                // Handle has no rollback yet

                // ResourceType has no rollback yet

                // Rethrow exception
                throw;
            }
        });

        _resourceCreated.OnNext(new ResourceCreatedEventArgs { Resource = resourceEntity, Creator = user });

        var resourceDto = _mapper.Map<ResourceDto>(resourceEntity);
        resourceDto.Pid = handle?.Pid?.Identifier ?? "";

        return resourceDto;
    }

    public async Task DeleteResourceForProjectAsync(Guid projectId, Guid resourceId, bool trackChanges)
    {
        var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges)
            ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(_context.HttpContext.User, resourceEntity, ResourceOperations.Delete);

        // Check if resource is in maintenance mode and enforce restrictions
        if (resourceEntity.MaintenanceMode)
        {
            // A resource can only be deleted if it is not in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        await _repositoryContextLoader.CreateExecutionStrategy().ExecuteAsync(async () =>
        {
            using var transaction = await _repositoryContextLoader.BeginTransactionAsync();
            try
            {
                _resourceRepository.Delete(resourceEntity);

                await _repositoryContextLoader.SaveAsync();

                // Create PID for resource
                var handleValues = _handleRepository.GenerateResourceHandleValues(resourceEntity)
                    .Where(x => x.Type == "URL"); // Only keep the URL type values for deleted resources
                var handle = new Handle { Values = handleValues };

                // Calling update will trigger the creation of the PID in the handle service
                await _handleRepository.UpdateAsync(_pidConfiguration.Prefix, resourceEntity.Id.ToString(), handle);

                await _resourceRepository.DeleteGraphsAsync(resourceEntity);

                await transaction.CommitAsync();
            }
            catch (Exception)
            {
                // Ef is rolledback automatically

                // Rollback graph changes
                await _resourceRepository.UpdateGraphsAsync(resourceEntity);

                // Handle has no rollback yet

                // Rethrow exception
                throw;
            }
        });
    }

    public async Task<PagedEnumerable<ResourceAdminDto>> GetPagedResourcesAsync(ResourceAdminParameters resourceParameters, bool trackChanges)
    {
        var resourceEntities = await _resourceRepository.GetPagedAsync(resourceParameters, trackChanges);

        var resourcesDtos = new List<ResourceAdminDto>();
        // Create ResourceDto instances one by one, then assign options and PID, and finally add DTO to the resourcesDtos list.
        // This way we can work with reference to each ResourceDto object and add it to a list when we're done.
        foreach (var resourceEntity in resourceEntities)
        {
            var resourceDto = _mapper.Map<ResourceAdminDto>(resourceEntity);

            // Assign additional missing information
            if ((resourceParameters.IncludeQuotas ?? false) && resourceEntity.ProjectResources.Count != 0)
            {
                resourceDto.ResourceQuota = await _quotaService.GetQuotaForResourceForProjectAsync(resourceEntity.ProjectResources.First().ProjectId, resourceEntity.Id, trackChanges: false);
            }

            // Assign additional missing information
            resourceDto.Type.Options = await _resourceTypeService.GetResourceTypeOptionsDto(resourceEntity, resourceParameters.IncludeQuotas ?? false);

            var pid = new Pid { Prefix = _pidConfiguration.Prefix, Suffix = resourceEntity.Id.ToString() };
            resourceDto.Pid = pid.Identifier;

            resourcesDtos.Add(resourceDto);
        }

        return new PagedEnumerable<ResourceAdminDto>(resourcesDtos, resourceEntities.Pagination);
    }

    public async Task<PagedEnumerable<ResourceDto>> GetPagedResourcesForProjectAsync(Guid projectId, ResourceParameters resourceParameters, bool trackChanges)
    {
        // It is enough to check for project rights
        var projectEntity = await _projectRepository.GetAsync(projectId, trackChanges: false) ?? throw new ProjectNotFoundException(projectId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(_context.HttpContext.User, projectEntity, ProjectOperations.Read);

        var resourceEntities = await _resourceRepository.GetPagedAsync(projectId, resourceParameters, trackChanges);
        var resourcesDtos = new List<ResourceDto>();

        // Create ResourceDto instances one by one, then assign options and PID, and finally add DTO to the resourcesDtos list.
        // This way we can work with reference to each ResourceDto object and add it to a list when we're done.
        foreach (var resourceEntity in resourceEntities)
        {
            // Do not check for resource rights here, cause its fucking slow. Thx.

            var resourceDto = _mapper.Map<ResourceDto>(resourceEntity);

            var pid = new Pid { Prefix = _pidConfiguration.Prefix, Suffix = resourceEntity.Id.ToString() };
            resourceDto.Pid = pid.Identifier;

            resourcesDtos.Add(resourceDto);
        }

        return new PagedEnumerable<ResourceDto>(resourcesDtos, resourceEntities.Pagination);
    }

    public async Task<ResourceDto> GetResourceByIdForProjectAsync(Guid projectId, Guid resourceId, bool trackChanges)
    {
        var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges)
            ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(_context.HttpContext.User, resourceEntity, ResourceOperations.Read);

        var resourceDto = _mapper.Map<ResourceDto>(resourceEntity);

        var result = await _authorizationService.AuthorizeAsync(_context.HttpContext.User, resourceEntity, ResourceOperations.ReadOptions);

        if (result.Succeeded)
        {
            // Assign additional missing information
            resourceDto.Type.Options = await _resourceTypeService.GetResourceTypeOptionsDto(resourceEntity);
        }

        var pid = new Pid { Prefix = _pidConfiguration.Prefix, Suffix = resourceEntity.Id.ToString() };
        resourceDto.Pid = pid.Identifier;

        return resourceDto;
    }

    public async Task<ResourceDto> GetResourceByIdAsync(Guid resourceId, bool trackChanges)
    {
        var resourceEntity = await _resourceRepository.GetAsync(resourceId, trackChanges)
            ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(_context.HttpContext.User, resourceEntity, ResourceOperations.Read);

        var resourceDto = _mapper.Map<ResourceDto>(resourceEntity, opts =>
        {
            opts.AfterMap((_, dto) =>
            {
                // Map additional information
                dto.Projects = _mapper.Map<List<ProjectMinimalDto>>(resourceEntity.ProjectResources);
            });
        });

        var result = await _authorizationService.AuthorizeAsync(_context.HttpContext.User, resourceEntity, ResourceOperations.ReadOptions);

        if (result.Succeeded)
        {
            // Assign additional missing information
            resourceDto.Type.Options = await _resourceTypeService.GetResourceTypeOptionsDto(resourceEntity);
        }

        var pid = new Pid { Prefix = _pidConfiguration.Prefix, Suffix = resourceEntity.Id.ToString() };
        resourceDto.Pid = pid.Identifier;

        return resourceDto;
    }

    public async Task UpdateResourceForProjectAsync(Guid projectId, Guid resourceId, ResourceForUpdateDto resourceForUpdateDto, bool trackChanges)
    {
        var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges)
            ?? throw new ResourceNotFoundException(resourceId);

        // Check role authorization
        await _authorizationService.AuthorizeAndThrowIfUnauthorized(_context.HttpContext.User, resourceEntity, ResourceOperations.Update);

        // Check if resource is in maintenance mode and enforce restrictions. Updaing the property is handled later.
        if (resourceEntity.MaintenanceMode && resourceForUpdateDto.MaintenanceMode is null)
        {
            // Only authorized users can update a resource while it is in maintenance mode
            var authorizedToUpdateWhileInMaintenanceMode = await _authorizationService.AuthorizeAsync(_context.HttpContext.User, resourceEntity, ResourceOperations.UpdateMaintenanceMode);
            if (!authorizedToUpdateWhileInMaintenanceMode.Succeeded)
            {
                throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
            }
        }

        // Archived restrictions when both resource and dto are archived
        if (resourceForUpdateDto.Archived && resourceEntity.Archived.Equals("1"))
        {
            throw new ResourceArchivedBadRequestException(resourceEntity.Id);
        }

        // Save the state change, which would be lost due to the mapping
        var archiveStatusChanged = resourceForUpdateDto.Archived != resourceEntity.Archived.Equals("1");
        var maintenanceModeStatusChanged = resourceForUpdateDto.MaintenanceMode is not null && resourceForUpdateDto.MaintenanceMode != resourceEntity.MaintenanceMode;

        _mapper.Map(resourceForUpdateDto, resourceEntity);

        resourceEntity.Visibility = await _visibilityRepository.GetAsync(resourceForUpdateDto.Visibility.Id, trackChanges: true)
            ?? throw new VisibilityNotFoundException(resourceForUpdateDto.Visibility.Id);
        if (resourceForUpdateDto.License is not null)
        {
            resourceEntity.License = await _licenseRepository.GetAsync(resourceForUpdateDto.License.Id, trackChanges: true)
                ?? throw new LicenseNotFoundException(resourceForUpdateDto.License.Id);
        }

        // We have to check, which disciplines were added and which should be removed.
        // The except function can be used to generate a diff.
        // A list with the new values and a list with the obsolete values.
        var disciplineIdsToAdd = resourceForUpdateDto
            .Disciplines.Select(x => x.Id)
            .Except(resourceEntity.ResourceDisciplines.Select(x => x.DisciplineId));
        var disciplineIdsToRemove = resourceEntity
            .ResourceDisciplines.Select(x => x.DisciplineId)
            .Except(resourceForUpdateDto.Disciplines.Select(x => x.Id));

        // Add new disciplines
        foreach (var disciplineId in disciplineIdsToAdd)
        {
            var disciplineEntity = await _disciplineRepository.GetAsync(disciplineId, trackChanges)
                ?? throw new DisciplineNotFoundException(disciplineId);
            resourceEntity.ResourceDisciplines.Add(
                new ResourceDiscipline { DisciplineId = disciplineEntity.Id, RelationId = resourceEntity.Id }
            );
        }

        // Remove old disciplines
        foreach (var disciplineId in disciplineIdsToRemove)
        {
            var disciplineEntity = await _disciplineRepository.GetAsync(disciplineId, trackChanges)
                ?? throw new DisciplineNotFoundException(disciplineId);

            // Due to the previous acquisition of the list, First() should always work.
            var projectDiscipline = resourceEntity.ResourceDisciplines.First(x => x.DisciplineId == disciplineEntity.Id);

            // The value has to be removed from the database and not from the list.
            // List removal only sets the FK to null!
            _resourceDisciplineRepository.Delete(projectDiscipline);
        }

        // Handle resource type specific updates
        var resourceTypeEntity = await _resourceTypeRepository.GetAsync(resourceEntity.TypeId, trackChanges: true)
            ?? throw new ResourceTypeNotFoundException(resourceEntity.TypeId);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        var updateDataStorageParameters = new UpdateDataStorageParameters();

        // Check if resource should and can be archived
        if (archiveStatusChanged && dataStorage.Capabilities.Contains(Capability.SupportsReadOnlyMode))
        {
            if (resourceEntity.MaintenanceMode && !maintenanceModeStatusChanged)
            {
                // Forbid changing the archive status while in maintenance mode, as it would overwrite the bucket policy
                throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
            }
            updateDataStorageParameters.ReadOnly = resourceForUpdateDto.Archived;
        }

        // Handle maintenance mode propery updates (administrators only)
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && maintenanceModeStatusChanged)
        {
            // Check role authorization for maintenance mode
            await _authorizationService.AuthorizeAndThrowIfUnauthorized(_context.HttpContext.User, resourceEntity, ResourceOperations.UpdateMaintenanceMode);
            // User is authorized to update maintenance mode, property on entity updated by the mapper. Handle the update in the data storage.
            updateDataStorageParameters.MaintenanceMode = resourceForUpdateDto.MaintenanceMode;
        }

        // Validate the desired resource reserved quota
        if (dataStorage.Capabilities.Contains(Capability.UsesQuota))
        {
            var desiredResourceReservedQuotaDto = _quotaService.ExtractDesiredResourceQuotaForResource(resourceTypeEntity, resourceForUpdateDto.ResourceTypeOptions);
            if (desiredResourceReservedQuotaDto is not null)
            {
                await _quotaService.ValidateResourceQuotaOnUpdateAsync(projectId, resourceId, desiredResourceReservedQuotaDto);
                int? desiredResourceQuotaReservedInGiB = desiredResourceReservedQuotaDto is not null
                    ? QuotaHelper.ConvertCapacityUnits(desiredResourceReservedQuotaDto, QuotaUnit.GibiBYTE)
                    : null;

                updateDataStorageParameters.Quota = desiredResourceQuotaReservedInGiB;
            }
        }
        await _repositoryContextLoader.CreateExecutionStrategy().ExecuteAsync(async () =>
        {
            using var transaction = await _repositoryContextLoader.BeginTransactionAsync();
            try
            {
                // Update the database and the external service
                await dataStorage.UpdateAsync(resourceEntity, updateDataStorageParameters);

                await _repositoryContextLoader.SaveAsync();

                // Create PID for resource
                var handleValues = _handleRepository.GenerateResourceHandleValues(resourceEntity);
                var handle = new Handle { Values = handleValues };

                // Calling update will trigger the creation of the PID in the handle service
                await _handleRepository.UpdateAsync(_pidConfiguration.Prefix, resourceEntity.Id.ToString(), handle);
                await _resourceRepository.UpdateGraphsAsync(resourceEntity);

                await transaction.CommitAsync();
            }
            catch (Exception)
            {
                // Ef is rolledback automatically

                // A bit strange to update again, but we have to ensure, that the old data is restored
                // This scenario is rather unlikely to happen so.

                resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges)
                    ?? throw new ResourceNotFoundException(resourceId);

                await _resourceRepository.UpdateGraphsAsync(resourceEntity);

                // Handle has no rollback yet

                // ResourceType has no rollback yet

                // Rethrow the exception
                throw;
            }
        });

        // Notify observers
        if (maintenanceModeStatusChanged)
        {
            var projectEntity = await _projectRepository.GetAsync(projectId, trackChanges: true)
                ?? throw new ProjectNotFoundException(projectId);
            var recipients = projectEntity.ProjectRoles
                .Where(x => x.RoleId == _roleRepository.GetOwnerRole().Id)
                .Select(x => x.User);

            if (resourceForUpdateDto.MaintenanceMode == true)
            {
                _resourceMaintenanceModeEnabled.OnNext(new ResourceMaintenanceModeEnabledEventArgs { Project = projectEntity, Resource = resourceEntity, Recipients = recipients });
            }
            else
            {
                _resourceMaintenanceModeDisabled.OnNext(new ResourceMaintenanceModeDisabledEventArgs { Project = projectEntity, Resource = resourceEntity, Recipients = recipients });
            }
        }
    }
}