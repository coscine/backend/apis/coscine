using AutoMapper;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service;

public class TitleService : ITitleService
{
    private readonly ITitleRepository _titleRepository;
    private readonly IMapper _mapper;

    public TitleService(ITitleRepository titleRepository, IMapper mapper)
    {
        _titleRepository = titleRepository;
        _mapper = mapper;
    }

    public async Task<IEnumerable<TitleDto>> GetAllTitlesAsync(TitleParameters titleParameters, bool trackChanges)
    {
        var titleEntities = await _titleRepository.GetAsync(titleParameters, trackChanges);
        return _mapper.Map<IEnumerable<TitleDto>>(titleEntities);
    }

    public async Task<TitleDto> GetTitleByIdAsync(Guid titleId, bool trackChanges)
    {
        var visibilityEntity = await _titleRepository.GetAsync(titleId, trackChanges) ?? throw new TitleNotFoundException(titleId);

        return _mapper.Map<TitleDto>(visibilityEntity);
    }
}