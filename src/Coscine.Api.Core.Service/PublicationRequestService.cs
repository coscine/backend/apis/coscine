﻿using AutoMapper;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Extensions;
using Coscine.Api.Core.Service.Helpers;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NETCore.MailKit.Core;
using System.Security.Claims;

namespace Coscine.Api.Core.Service;

public partial class PublicationRequestService(
    IAuthenticatorService authenticatorService,
    IAuthorizationService authorizationService,
    IEmailService emailService,
    IQuotaService quotaService,
    IPublicationAdvisoryServiceRepository publicationAdvisoryServiceRepository,
    IProjectPublicationRequestRepository publicationRequestRepository,
    IProjectRepository projectRepository,
    IResourceRepository resourceRepository,
    IRoleRepository roleRepository,
    IRepositoryContextLoader repositoryContextLoader,
    IEmailTemplateManager emailTemplateManager,
    IMapper mapper,
    ILogger<PublicationRequestService> logger,
    IOptionsMonitor<PidConfiguration> pidConfiguration,
    IHttpContextAccessor context) : IPublicationRequestService
{
    private readonly IAuthenticatorService _authenticatorService = authenticatorService;
    private readonly IAuthorizationService _authorizationService = authorizationService;
    private readonly IEmailService _emailService = emailService;
    private readonly IQuotaService _quotaService = quotaService;
    private readonly IPublicationAdvisoryServiceRepository _publicationAdvisoryServiceRepository = publicationAdvisoryServiceRepository;
    private readonly IProjectPublicationRequestRepository _projectPublicationRequestRepository = publicationRequestRepository;
    private readonly IProjectRepository _projectRepository = projectRepository;
    private readonly IResourceRepository _resourceRepository = resourceRepository;
    private readonly IRoleRepository _roleRepository = roleRepository;
    private readonly IRepositoryContextLoader _repositoryContextLoader = repositoryContextLoader;
    private readonly IEmailTemplateManager _emailTemplateManager = emailTemplateManager;
    private readonly IMapper _mapper = mapper;
    private readonly ILogger<PublicationRequestService> _logger = logger;
    private readonly PidConfiguration _pidConfiguration = pidConfiguration.CurrentValue;

    private const string DE = "de";
    private const string EN = "en";



    public async Task<IEnumerable<ProjectPublicationRequestDto>> GetAllProjectPublicationRequestsAsync(Guid projectId, bool trackChanges)
    {
        var projectEntity = await _projectRepository.GetAsync(projectId, trackChanges: true) ?? throw new ProjectNotFoundException(projectId);

        // Check if requester is allowed to read publication requests
        await _authorizationService.AuthorizeAndThrowIfUnauthorized(context.HttpContext.User, projectEntity, ProjectOperations.Read); // TODO: Consider a new auth handler for publication requests

        var projectPublicationRequests = await _projectPublicationRequestRepository.GetAllAsync(projectId, trackChanges);

        return _mapper.Map<IEnumerable<ProjectPublicationRequestDto>>(projectPublicationRequests);
    }

    public async Task<PagedEnumerable<ProjectPublicationRequestDto>> GetPagedProjectPublicationRequestsAsync(Guid projectId, ProjectPublicationRequestAdminParameters projectPublicationRequestAdminParameters, bool trackChanges)
    {
        var projectEntity = await _projectRepository.GetAsync(projectId, trackChanges: true) ?? throw new ProjectNotFoundException(projectId);

        // Check if requester is allowed to read publication requests
        await _authorizationService.AuthorizeAndThrowIfUnauthorized(context.HttpContext.User, projectEntity, ProjectOperations.Read); // TODO: Consider a new auth handler for publication requests

        var projectPublicationRequests = await _projectPublicationRequestRepository.GetPagedAsync(projectId, projectPublicationRequestAdminParameters, trackChanges);

        var projectPublicationRequestDtos = _mapper.Map<IEnumerable<ProjectPublicationRequestDto>>(projectPublicationRequests);
        
        return new PagedEnumerable<ProjectPublicationRequestDto>(projectPublicationRequestDtos, projectPublicationRequests.Pagination);
    }

    public async Task<ProjectPublicationRequestDto> CreatePublicationRequestForUserAsync(Guid projectId, PublicationRequestForCreationDto publicationRequestDto)
    {
        var user = await _authenticatorService.GetUserAsync(context.HttpContext.User, trackChanges: true) ?? throw new UserNotFoundException();

        var projectEntity = await _projectRepository.GetAsync(projectId, user.Id, trackChanges: true) ?? throw new ProjectNotFoundException(projectId);

        // Check if requester is allowed to create a publication request
        await _authorizationService.AuthorizeAndThrowIfUnauthorized(context.HttpContext.User, projectEntity, ProjectOperations.CreatePublicationRequest);

        var projectPublicationRequestEntity = _mapper.Map<ProjectPublicationRequest>(publicationRequestDto, opts =>
        {
            opts.AfterMap((_, e) =>
            {
                // Map additional properties
                e.ProjectId = projectEntity.Id;
                e.CreatorId = user.Id;
                e.DateCreated = DateTime.UtcNow;
            });
        });

        _projectPublicationRequestRepository.Create(projectPublicationRequestEntity);

        // Add resources to the publication request
        foreach (var resourceId in publicationRequestDto.ResourceIds)
        {
            var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges: true) ?? throw new ResourceNotFoundException(resourceId);
            projectPublicationRequestEntity.Resources.Add(resourceEntity);
        }

        // Initiate sending of emails
        await SendPublicationRequestEmailsAsync(projectPublicationRequestEntity, projectEntity, user);

        // Save after sending the emails, as on failure we don't want to save the publication request
        await _repositoryContextLoader.SaveAsync();

        return _mapper.Map<ProjectPublicationRequestDto>(projectPublicationRequestEntity);
    }

    public async Task<ProjectPublicationRequestDto> GetPublicationRequestAsync(Guid projectId, Guid publicationRequestId, bool trackChanges)
    {
        var projectEntity = await _projectRepository.GetAsync(projectId, trackChanges: false)
            ?? throw new ProjectNotFoundException(projectId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(context.HttpContext.User, projectEntity, ProjectOperations.Read); // TODO: Consider a new auth handler for publication requests

        var publicationRequestEntity = await _projectPublicationRequestRepository.GetAsync(projectId, publicationRequestId, trackChanges)
            ?? throw new ProjectPublicationRequestNotFoundException(publicationRequestId);

        var projectPublicationRequestDto = _mapper.Map<ProjectPublicationRequestDto>(publicationRequestEntity);

        return projectPublicationRequestDto;
    }

    private async Task SendPublicationRequestEmailsAsync(ProjectPublicationRequest projectPublicationRequest, Project project, User creator)
    {
        var quota = new Quota { Value = 0.0, Unit = QuotaUnit.GibiBYTE };

        var resourcesPartForTemplateDe = "";
        var resourcesPartForTemplateEn = "";

        foreach (var resource in projectPublicationRequest.Resources)
        {
            // Create the resource part of the template for every resource
            resourcesPartForTemplateDe = string.Concat(resourcesPartForTemplateDe, CreateResourcePartForTemplate(resource, DE));
            resourcesPartForTemplateEn = string.Concat(resourcesPartForTemplateEn, CreateResourcePartForTemplate(resource, EN));

            var resourceQuota = await _quotaService.GetQuotaForResourceForProjectAsync(project.Id, resource.Id, trackChanges: false);
            quota.Value += resourceQuota.Used is not null ? QuotaHelper.ConvertCapacityUnits(resourceQuota.Used, quota.Unit) : 0;
        }

        var resourcesPartForTemplate = new List<string>() { resourcesPartForTemplateDe, resourcesPartForTemplateEn };

        // Mail to data publication advisory service
        await SendEmailToDataPublicationAdvisoryServiceAsync(projectPublicationRequest, project, creator, resourcesPartForTemplate, quota);

        var ownerRole = _roleRepository.GetOwnerRole();
        var owners = project.ProjectRoles
            .Where(pr => pr.RoleId.Equals(ownerRole.Id))
            .Select(pr => pr.User)
            .Distinct();

        // Mail to project owners
        foreach (var owner in owners)
        {
            await SendEmailToOwnerAsync(projectPublicationRequest, project, creator, owner, resourcesPartForTemplate, quota);
        }
    }

    private async Task SendEmailToDataPublicationAdvisoryServiceAsync(ProjectPublicationRequest projectPublicationRequest, Project project, User creator, List<string> resourcesPart, Quota quota)
    {
        // TODO: Consolidate both DB calls from this method and the one below into one call, unnecessary trips to the DB
        // get publication advisory service with name and text in english and german
        var pubAdvServiceDe = await _publicationAdvisoryServiceRepository.GetPublicationAdvisoryServiceAsync(projectPublicationRequest.PublicationServiceRorId, AcceptedLanguage.de);
        var pubAdvServiceEn = await _publicationAdvisoryServiceRepository.GetPublicationAdvisoryServiceAsync(projectPublicationRequest.PublicationServiceRorId, AcceptedLanguage.en);

        // create email messages for both languages
        var emailMessageDe = CreateEmailMessage(DE, projectPublicationRequest, project, creator, pubAdvServiceDe, null, resourcesPart[0], quota);
        var emailMessageEn = CreateEmailMessage(EN, projectPublicationRequest, project, creator, pubAdvServiceEn, null, resourcesPart[1], quota);
        // create email subject
        var emailSubject = _emailTemplateManager.PublicationRequestPublicationAdvisoryServiceTemplates[DE].ParseSubject(new { project.ProjectName });

        try
        {
            // Get the email address of the publication advisory service
            var mailTo = pubAdvServiceDe?.Email ?? pubAdvServiceEn?.Email;

            // Always send the email text in both languages
            _emailService.Send(
                mailTo,
                emailSubject,
                string.Concat(emailMessageDe, emailMessageEn),
                isHtml: true
            );
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, @"Failed to send email to publication advisory service: ""{emailOfPubAdvService}""", pubAdvServiceDe?.Email);
        }
    }

    private async Task SendEmailToOwnerAsync(ProjectPublicationRequest projectPublicationRequest, Project project, User creator, User owner, List<string> resourcesPart, Quota quota)
    {
        // TODO: Consolidate both DB calls from this method and the one above into one call, unnecessary trips to the DB
        // get publication advisory service with name and text in english and german
        var pubAdvServiceDe = await _publicationAdvisoryServiceRepository.GetPublicationAdvisoryServiceAsync(projectPublicationRequest.PublicationServiceRorId, AcceptedLanguage.de);
        var pubAdvServiceEn = await _publicationAdvisoryServiceRepository.GetPublicationAdvisoryServiceAsync(projectPublicationRequest.PublicationServiceRorId, AcceptedLanguage.en);

        // create email messages for both languages
        var emailMessageDe = CreateEmailMessage(DE, projectPublicationRequest, project, creator, pubAdvServiceDe, owner, resourcesPart[0], quota);
        var emailMessageEn = CreateEmailMessage(EN, projectPublicationRequest, project, creator, pubAdvServiceEn, owner, resourcesPart[1], quota);
        // create email subject
        var emailSubject = _emailTemplateManager.PublicationRequestOwnersTemplates[DE].ParseSubject(new { project.ProjectName });

        try
        {
            // always send the email-text in both languages
            _emailService.Send(
                owner.EmailAddress,
                emailSubject,
                string.Concat(emailMessageDe, emailMessageEn),
                isHtml: true
            );
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, @"Failed to send email to owner ""{owner}"" of project", owner.EmailAddress);
        }
    }

    private string CreateEmailMessage(string language, ProjectPublicationRequest projectPublicationRequest, Project project, User creator, PublicationAdvisoryService? pubAdvService, User? owner, string resourcesString, Quota quota)
    {
        var disciplinesAsString = "";
        int i = 0;
        foreach (var discipline in project.ProjectDisciplines)
        {
            var disciplineDisplayName = (language == DE) ? discipline.Discipline.DisplayNameDe : discipline.Discipline.DisplayNameEn;
            disciplinesAsString = string.Concat(disciplinesAsString, (i == 0) ? "" : "; ", disciplineDisplayName);
            i++;
        }

        var pid = new Pid { Prefix = _pidConfiguration.Prefix, Suffix = project.Id.ToString() };

        var parseObject = new
        {
            UserDisplayName = creator.DisplayName,
            UserEmail = creator.EmailAddress,
            project.ProjectName,
            ProjectPid = pid.Identifier,
            ProjectDescription = project.Description,
            ProjectDisciplines = disciplinesAsString,
            DisplayNameOfPubAdvService = pubAdvService?.DisplayName,
            EmailOfPubAdvService = pubAdvService?.Email,
            ProjectOwnerDisplayName = (owner == null) ? "" : owner.DisplayName,
            Resources = resourcesString,
            ApproxDataSizeValue = double.Round(quota.Value, 2),
            ApproxDataSizeUnit = quota.Unit.ToString(),
            projectPublicationRequest.Message
        };

        if (owner is not null)
        {
            return _emailTemplateManager.PublicationRequestOwnersTemplates[language].ParseMessage(parseObject);
        }
        return _emailTemplateManager.PublicationRequestPublicationAdvisoryServiceTemplates[language].ParseMessage(parseObject);
    }

    private string CreateResourcePartForTemplate(Resource resource, string language)
    {
        var disciplinesAsString = "";
        int i = 0;
        foreach (var discipline in resource.ResourceDisciplines)
        {
            var disciplineDisplayName = language == DE ? discipline.Discipline.DisplayNameDe : discipline.Discipline.DisplayNameEn;
            disciplinesAsString = string.Concat(disciplinesAsString, (i == 0) ? "" : "; ", disciplineDisplayName);
            i++;
        }

        var pid = new Pid { Prefix = _pidConfiguration.Prefix, Suffix = resource.Id.ToString() };

        var parseObject = new
        {
            ResourceDisplayName = resource.DisplayName,
            ApDisplayNameOrLabel = resource.ApplicationProfile,
            ResourcePid = pid.Identifier,
            ResourceDescription = resource.Description,
            ResourceDisciplines = disciplinesAsString
        };

        return _emailTemplateManager.PublicationRequestResourcePartTemplates[language].ParseMessage(parseObject);
    }
}

