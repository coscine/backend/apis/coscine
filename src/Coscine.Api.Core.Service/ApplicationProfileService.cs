﻿using AutoMapper;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.Forbidden;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Coscine.Api.Core.Service.Extensions.Utility;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Enums.Utility;
using Coscine.Api.Core.Shared.RequestFeatures;
using GitLabApiClient;
using GitLabApiClient.Models.Branches.Requests;
using GitLabApiClient.Models.Commits.Requests.CreateCommitRequest;
using GitLabApiClient.Models.MergeRequests.Requests;
using Microsoft.Extensions.Options;
using System.Reactive.Subjects;
using System.Security.Claims;
using VDS.RDF;
using VDS.RDF.Parsing;
using StringWriter = VDS.RDF.Writing.StringWriter;

namespace Coscine.Api.Core.Service;

public class ApplicationProfileService : IApplicationProfileService
{
    private readonly IApplicationProfileRepository _applicationProfileRepository;
    private readonly IAuthenticatorService _authenticatorService;
    private readonly IMapper _mapper;
    private readonly ConnectionConfiguration _connectionConfiguration;
    private readonly Uri baseApplicationProfileUri = new("https://purl.org/coscine/ap/");

    private readonly Subject<APCreationRequestedEventArgs> _apCreationRequested = new();
    private readonly Subject<APCreationRequestedEventArgs> _apCreationRequestedServiceDesk = new();

    public ApplicationProfileService(
        IApplicationProfileRepository applicationProfileRepository,
        IAuthenticatorService authenticatorService,
        IMapper mapper,
        IObserverManager observerManager,
        IOptionsMonitor<ConnectionConfiguration> connectionConfiguration
    )
    {
        _applicationProfileRepository = applicationProfileRepository;
        _authenticatorService = authenticatorService;
        _mapper = mapper;
        _connectionConfiguration = connectionConfiguration.CurrentValue;

        _apCreationRequested.SubscribeAll(observerManager.APCreationRequestObservers);
        _apCreationRequestedServiceDesk.SubscribeAll(observerManager.APCreationRequestServiceDeskObservers);
    }

    public async Task<PagedEnumerable<ApplicationProfileDto>> GetPagedApplicationProfilesAsync(ApplicationProfileParameters applicationProfileParameters, ClaimsPrincipal principal, bool trackChanges)
    {
        var applicationProfileEntities = await _applicationProfileRepository.GetPagedAsync(applicationProfileParameters);
        var applicationProfileDtos = _mapper.Map<IEnumerable<ApplicationProfileDto>>(applicationProfileEntities);
        return new PagedEnumerable<ApplicationProfileDto>(applicationProfileDtos, applicationProfileEntities.Pagination);
    }

    public async Task<ApplicationProfileDto> GetApplicationProfileByUriAsync(Uri applicationProfileUri, RdfFormat rdfFormat, AcceptedLanguage language, ClaimsPrincipal principal, bool trackChanges)
    {
        // Validate the base of the application profile URI
        if (!baseApplicationProfileUri.IsBaseOf(applicationProfileUri))
        {
            throw new ApplicationProfileBaseUriBadRequestException(baseApplicationProfileUri);
        }

        var applicationProfileEntity = await _applicationProfileRepository.GetAsync(applicationProfileUri, rdfFormat, language)
            ?? throw new ApplicationProfileNotFoundException(applicationProfileUri);
        return _mapper.Map<ApplicationProfileDto>(applicationProfileEntity);
    }

    public async Task<IGraph?> GetRawApplicationProfileByUriAsync(Uri applicationProfileUri, RdfFormat rdfFormat, AcceptedLanguage language)
    {
        // Validate the base of the application profile URI
        if (!baseApplicationProfileUri.IsBaseOf(applicationProfileUri))
        {
            throw new ApplicationProfileBaseUriBadRequestException(baseApplicationProfileUri);
        }

        var applicationProfileEntity = await _applicationProfileRepository.GetAsync(applicationProfileUri, rdfFormat, language)
            ?? throw new ApplicationProfileNotFoundException(applicationProfileUri);
        return applicationProfileEntity.Graph;
    }

    public async Task<ApplicationProfileRequestDto> CreateApplicationProfileRequestAsync(ClaimsPrincipal principal, ApplicationProfileForCreationDto applicationProfileForCreationDto)
    {
        var user = await _authenticatorService.GetUserAsync(principal, trackChanges: false) ?? throw new UserNotFoundException();

        // Apply Email restrictions
        if (string.IsNullOrWhiteSpace(user.EmailAddress))
        {
            throw new ApplicationProfileCreationForbiddenException();
        }

        // Validate the base of the application profile URI
        if (!baseApplicationProfileUri.IsBaseOf(applicationProfileForCreationDto.Uri))
        {
            throw new ApplicationProfileBaseUriBadRequestException(baseApplicationProfileUri);
        }

        var mimeType = applicationProfileForCreationDto.Definition.Type.GetEnumMemberValue();
        // Validate the mime type is turtle
        if (!MimeTypesHelper.Turtle.Contains(mimeType))
        {
            // TODO: Validate the RDF using attributes and return a 422 instead
            throw new ApplicationProfileCreationBadRequestException($"The mime type must be any of {string.Join(", ", MimeTypesHelper.Turtle)}.");
        }

        // Load the application profile definition into a graph
        var parser = MimeTypesHelper.GetParser(mimeType);
        var writer = MimeTypesHelper.GetWriter(mimeType);
        var graph = new Graph(applicationProfileForCreationDto.Uri)
        {
            BaseUri = applicationProfileForCreationDto.Uri
        };
        try
        {
            graph.LoadFromString(applicationProfileForCreationDto.Definition.Content, parser);
        }
        catch (RdfParseException)
        {
            // TODO: Validate the RDF using attributes and return a 422 instead
            throw new ApplicationProfileCreationBadRequestException($"The application profile definition is not valid RDF.");
        };
        var applicationProfileSerialization = StringWriter.Write(graph, writer);

        // Connect to GitLab and create the necessary branches
        var gitLabClient = new GitLabClient(_connectionConfiguration.ApplicationProfiles?.Requests?.GitLab?.HostUrl, _connectionConfiguration.ApplicationProfiles?.Requests?.GitLab?.Token);
        var applicationProfilesProject = await gitLabClient.Projects.GetAsync(_connectionConfiguration.ApplicationProfiles?.Requests?.GitLab?.ProjectId);
        var requestBranchName = $"Request/{Guid.NewGuid()}";
        await gitLabClient.Branches.CreateAsync(applicationProfilesProject, new CreateBranchRequest(requestBranchName, applicationProfilesProject.DefaultBranch));

        // Commit the serialized application profile to GitLab
        var requestFolderName = applicationProfileForCreationDto.Uri.ToString().Replace(baseApplicationProfileUri.ToString(), "").TrimEnd('/');
        var actions = new List<CreateCommitRequestAction>
        {
            new CreateCommitRequestAction(new CreateCommitRequestActionType(), $"profiles/{requestFolderName}/index.ttl")
            {
                Content = applicationProfileSerialization
            }
        };
        var userDisplayName = user.Title is not null
                ? string.Join(" ", user.Title.DisplayName, user.Givenname, user.Surname).Trim()
                : string.Join(" ", user.Givenname, user.Surname).Trim();
        var requestCommit = new CreateCommitRequest(requestBranchName, $"{userDisplayName} requests new application profile {applicationProfileForCreationDto.Name}", actions)
        {
            AuthorEmail = user.EmailAddress,
            AuthorName = userDisplayName
        };
        await gitLabClient.Commits.CreateAsync(applicationProfilesProject, requestCommit, autoEncodeToBase64: false);

        // Get the default Description
        var templateFile = await gitLabClient.Files.GetAsync(applicationProfilesProject, filePath: ".gitlab/merge_request_templates/default.md", reference: applicationProfilesProject.DefaultBranch);
        var defaultDescription = $"Merge request created by {userDisplayName}, {user.EmailAddress}, created at {DateTime.Now:dd/MM/yyyy HH:mm:ss tt}\n\n{templateFile.ContentDecoded}";

        // Create the Merge Request for review
        var newMergeRequest = await gitLabClient.MergeRequests.CreateAsync(applicationProfilesProject,
            new CreateMergeRequest(requestBranchName, applicationProfilesProject.DefaultBranch, $"Merge request for new application profile: {applicationProfileForCreationDto.Name}")
            {
                Description = defaultDescription
            }
        );

        // Create the DTO
        var applicationProfileRequestDto = new ApplicationProfileRequestDto
        {
            MergeRequestUrl = new Uri(newMergeRequest.WebUrl),
            ApplicationProfileName = applicationProfileForCreationDto.Name
        };

        // Trigger the observers
        var apCreationRequesedEventArgs = new APCreationRequestedEventArgs
        {
            Creator = user,
            ApplicationProfileName = applicationProfileRequestDto.ApplicationProfileName,
            MergeRequestURL = applicationProfileRequestDto.MergeRequestUrl
        };
        _apCreationRequestedServiceDesk.OnNext(apCreationRequesedEventArgs);
        _apCreationRequested.OnNext(apCreationRequesedEventArgs);

        return applicationProfileRequestDto;
    }
}