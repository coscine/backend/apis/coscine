﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailAPCreationRequestServiceDeskObserver : IAPCreationRequestServiceDeskObserver
{
    private readonly IEmailTemplateManager _emailTemplateManager;
    private readonly IEmailService _emailService;
    private readonly ILogger<EmailAPCreationRequestServiceDeskObserver> _logger;
    private readonly ConnectionConfiguration _connectionConfiguration;

    public EmailAPCreationRequestServiceDeskObserver(
        IEmailTemplateManager emailTemplateManager,
        IEmailService emailService,
        ILogger<EmailAPCreationRequestServiceDeskObserver> logger,
        IOptionsMonitor<ConnectionConfiguration> connectionConfiguration
    )
    {
        _emailTemplateManager = emailTemplateManager;
        _emailService = emailService;
        _logger = logger;
        _connectionConfiguration = connectionConfiguration.CurrentValue;
    }

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(APCreationRequestedEventArgs value)
    {
        try
        {
            var parseObject = new
            {
                ApplicationProfileName = value.ApplicationProfileName,
                MergeRequestUrl = value.MergeRequestURL
            };

            var language = "en";

            if (value.Creator.Language is not null)
            {
                language = value.Creator.Language.Abbreviation;
            }

            _emailService.Send(_connectionConfiguration.ServiceDesk?.Email,
                _emailTemplateManager.APCreationRequestServiceDeskTemplates[language].ParseSubject(parseObject),
                _emailTemplateManager.APCreationRequestServiceDeskTemplates[language].ParseMessage(parseObject),
                isHtml: true);
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, @"Failed to send email for application profile creation request to Service Desk for: ""{ApplicationProfileName}"" and merge request url: ""{MergeRequestURL}"".", value.ApplicationProfileName, value.MergeRequestURL);
        }
    }
}