﻿using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailResourceDeletionObserver : IResourceDeletionObserver
{
    private readonly IEmailTemplateManager _emailTemplateManager;
    private readonly IEmailService _emailService;
    private readonly ILogger<EmailResourceDeletionObserver> _logger;

    public EmailResourceDeletionObserver(
        IEmailTemplateManager emailTemplateManager,
        IEmailService emailService,
        ILogger<EmailResourceDeletionObserver> logger)
    {
        _emailTemplateManager = emailTemplateManager;
        _emailService = emailService;
        _logger = logger;
    }

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(ResourceDeletedEventArgs value)
    {
        var parseObject = new { ResourceName = value.Resource.DisplayName };

        foreach (var recipient in value.Recipients)
        {
            try
            {
                var language = "en";

                if (recipient.Language is not null)
                {
                    language = recipient.Language.Abbreviation;
                }

                _emailService.Send(
                    recipient.EmailAddress,
                    _emailTemplateManager.ResourceDeletedTemplates[language].ParseSubject(parseObject),
                    _emailTemplateManager.ResourceDeletedTemplates[language].ParseMessage(parseObject),
                    isHtml: true
                );
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, @"Failed to send email for resource deletion of resource: ""{ResourceId}"" and user: ""{recipientId}"".", value.Resource.Id, recipient.Id);
            }
        }
    }
}