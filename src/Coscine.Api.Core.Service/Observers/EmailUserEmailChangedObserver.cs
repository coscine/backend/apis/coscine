﻿using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailUserEmailChangedObserver : IUserEmailChangedObserver
{
    private readonly IEmailTemplateManager _emailTemplateManager;
    private readonly IEmailService _emailService;
    private readonly ILogger<EmailUserEmailChangedObserver> _logger;

    public EmailUserEmailChangedObserver(
        IEmailTemplateManager emailTemplateManager,
        IEmailService emailService,
        ILogger<EmailUserEmailChangedObserver> logger)
    {
        _emailTemplateManager = emailTemplateManager;
        _emailService = emailService;
        _logger = logger;
    }

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(UserEmailChangedEventArgs eventArgs)
    {
        var parseObject = new { ConfirmationLink = $"{eventArgs.BaseUrl}/?emailtoken={eventArgs.ContactChange.ConfirmationToken}" };
        var recipient = eventArgs.ContactChange.User;
        try
        {
            var language = "en";

            if (recipient.Language is not null)
            {
                language = recipient.Language.Abbreviation;
            }

            _emailService.Send(
                recipient.EmailAddress,
                _emailTemplateManager.UserEmailChangedTemplates[language].ParseSubject(parseObject),
                _emailTemplateManager.UserEmailChangedTemplates[language].ParseMessage(parseObject),
                isHtml: true
            );
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, @"Failed to send email for user email change to: ""{EmailAddress}"" and user: ""{recipientId}"".", recipient.EmailAddress, recipient.Id);
        }
    }
}