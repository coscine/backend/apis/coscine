﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailProjectInvitationCreationObserver : IProjectInvitationCreationObserver
{
    private readonly IEmailTemplateManager _emailTemplateManager;
    private readonly IEmailService _emailService;
    private readonly ILogger<EmailProjectInvitationCreationObserver> _logger;
    private readonly ConnectionConfiguration _connectionConfiguration;

    public EmailProjectInvitationCreationObserver(
        IEmailTemplateManager emailTemplateManager,
        IEmailService emailService,
        ILogger<EmailProjectInvitationCreationObserver> logger,
        IOptionsMonitor<ConnectionConfiguration> connectionConfiguration
    )
    {
        _emailTemplateManager = emailTemplateManager;
        _emailService = emailService;
        _logger = logger;
        _connectionConfiguration = connectionConfiguration.CurrentValue;

        // Ensure the configuration keys are set
        ArgumentNullException.ThrowIfNull(connectionConfiguration);
        ArgumentException.ThrowIfNullOrEmpty(_connectionConfiguration.ServiceUrl);
        ArgumentNullException.ThrowIfNull(_connectionConfiguration.ProjectInvitations);
        ArgumentException.ThrowIfNullOrEmpty(_connectionConfiguration.ProjectInvitations.QueryParameter);
    }

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(ProjectInvitationCreationEventArgs value)
    {
        try
        {
            // Build the confirmation link URL using the application's proxy URL and the query parameter from the connection configuration
            var hostUrl = Uri.TryCreate(_connectionConfiguration.ServiceUrl, UriKind.Absolute, out var uri) ? uri : throw new ArgumentException("Could not parse the host URL.");
            var queryParameter = _connectionConfiguration.ProjectInvitations?.QueryParameter;
            var confirmationLink = QueryHelpers.AddQueryString(hostUrl.AbsoluteUri, queryParameter, value.Invitation.Token.ToString());

            var parseObject = new { ProjectName = value.Project.DisplayName, ConfirmationLink = confirmationLink };

            var language = "en";
            if (value.User.Language is not null)
            {
                language = value.User.Language.Abbreviation;
            }

            _emailService.Send(value.Invitation.InviteeEmail,
                _emailTemplateManager.InviteUserToProjectTemplates[language].ParseSubject(parseObject),
                _emailTemplateManager.InviteUserToProjectTemplates[language].ParseMessage(parseObject),
                isHtml: true);
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, @"Failed to send email for project role creation of project: ""{ProjectId}"" and user: ""{UserId}""", value.Project.Id, value.User.Id);
        }
    }
}