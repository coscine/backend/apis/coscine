﻿using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailProjectRoleCreationObserver : IProjectRoleCreationObserver
{
    private readonly IEmailTemplateManager _emailTemplateManager;
    private readonly IEmailService _emailService;
    private readonly ILogger<EmailProjectRoleCreationObserver> _logger;

    public EmailProjectRoleCreationObserver(
        IEmailTemplateManager emailTemplateManager,
        IEmailService emailService,
        ILogger<EmailProjectRoleCreationObserver> logger)
    {
        _emailTemplateManager = emailTemplateManager;
        _emailService = emailService;
        _logger = logger;
    }

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(ProjectRoleCreationEventArgs value)
    {
        try
        {
            if (value.User.EmailAddress is not null)
            {
                var parseObject = new { ProjectName = value.Project.DisplayName };

                var language = "en";

                if (value.User.Language is not null)
                {
                    language = value.User.Language.Abbreviation;
                }

                _emailService.Send(value.User.EmailAddress,
                    _emailTemplateManager.UserRoleChangedTemplates[language].ParseSubject(parseObject),
                    _emailTemplateManager.UserRoleChangedTemplates[language].ParseMessage(parseObject),
                    isHtml: true);
            }
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, @"Failed to send email for project role creation of project: ""{ProjectId}"" and user: ""{UserId}"".", value.Project.Id, value.User.Id);
        }
    }
}