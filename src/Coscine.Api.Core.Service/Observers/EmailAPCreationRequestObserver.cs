﻿using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailAPCreationRequestObserver : IAPCreationRequestObserver
{
    private readonly IEmailService _emailService;
    private readonly IEmailTemplateManager _emailTemplateManager;
    private readonly ILogger<EmailAPCreationRequestObserver> _logger;

    public EmailAPCreationRequestObserver(
        IEmailTemplateManager emailTemplateManager,
        IEmailService emailService,
        ILogger<EmailAPCreationRequestObserver> logger)
    {
        _emailService = emailService;
        _emailTemplateManager = emailTemplateManager;
        _logger = logger;
    }

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(APCreationRequestedEventArgs value)
    {
        try
        {
            var parseObject = new
            {
                ApplicationProfileName = value.ApplicationProfileName,
                MergeRequestUrl = value.MergeRequestURL
            };

            var language = "en";

            if (value.Creator.Language is not null)
            {
                language = value.Creator.Language.Abbreviation;
            }

            _emailService.Send(value.Creator.EmailAddress,
                _emailTemplateManager.APCreationRequestTemplates[language].ParseSubject(parseObject),
                _emailTemplateManager.APCreationRequestTemplates[language].ParseMessage(parseObject),
                isHtml: true);
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, @"Failed to send email for application profile creation request to user for: ""{ApplicationProfileName}"" and merge request url: ""{MergeRequestURL}"".", value.ApplicationProfileName, value.MergeRequestURL);
        }
    }
}