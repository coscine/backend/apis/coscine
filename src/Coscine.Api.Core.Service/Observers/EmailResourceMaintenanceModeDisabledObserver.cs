using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailResourceMaintenanceModeDisabledObserver(
    IEmailTemplateManager emailTemplateManager,
    IEmailService emailService,
    ILogger<EmailResourceMaintenanceModeDisabledObserver> logger) : IResourceMaintenanceModeDisabledObserver
{
    private readonly IEmailTemplateManager _emailTemplateManager = emailTemplateManager;
    private readonly IEmailService _emailService = emailService;
    private readonly ILogger<EmailResourceMaintenanceModeDisabledObserver> _logger = logger;

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(ResourceMaintenanceModeDisabledEventArgs value)
    {
        var parseObject = new
        {
            ProjectName = value.Project.DisplayName,
            ResourceName = value.Resource.DisplayName,
        };

        foreach (var recipent in value.Recipients)
        {
            try
            {
                var language = "en";

                if (recipent.Language is not null)
                {
                    language = recipent.Language.Abbreviation;
                }

                _emailService.Send(recipent.EmailAddress,
                    _emailTemplateManager.ResourceMaintenanceModeDisabledTemplates[language].ParseSubject(parseObject),
                    _emailTemplateManager.ResourceMaintenanceModeDisabledTemplates[language].ParseMessage(parseObject),
                    isHtml: true);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, @"Failed to send email for resource maintenance mode change of resource: ""{ResourceId}"" and user: ""{recipentId}"".", value.Resource.Id, recipent.Id);
            }
        }
    }
}