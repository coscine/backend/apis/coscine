﻿using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailProjectDeletionObserver : IProjectDeletionObserver
{
    private readonly IEmailTemplateManager _emailTemplateManager;
    private readonly IEmailService _emailService;
    private readonly ILogger<EmailProjectDeletionObserver> _logger;

    public EmailProjectDeletionObserver(
        IEmailTemplateManager emailTemplateManager,
        IEmailService emailService,
        ILogger<EmailProjectDeletionObserver> logger)
    {
        _emailTemplateManager = emailTemplateManager;
        _emailService = emailService;
        _logger = logger;
    }

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(ProjectDeletedEventArgs value)
    {
        var parseObject = new { ProjectName = value.Project.DisplayName };

        foreach (var recipent in value.Recipients)
        {
            try
            {
                var language = "en";

                if (recipent.Language is not null)
                {
                    language = recipent.Language.Abbreviation;
                }

                _emailService.Send(recipent.EmailAddress,
                    _emailTemplateManager.ProjectDeletedTemplates[language].ParseSubject(parseObject),
                    _emailTemplateManager.ProjectDeletedTemplates[language].ParseMessage(parseObject),
                    isHtml: true);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, @"Failed to send email for project deletion of project: ""{ProjectId}"" and user: ""{recipentId}"".", value.Project.Id, recipent.Id);
            }
        }
    }
}