﻿using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailPidContactConfirmationObserver : IPidContactConfirmationObserver
{
    private readonly IEmailTemplateManager _emailTemplateManager;
    private readonly IEmailService _emailService;
    private readonly ILogger<EmailPidContactConfirmationObserver> _logger;

    public EmailPidContactConfirmationObserver(
        IEmailTemplateManager emailTemplateManager,
        IEmailService emailService,
        ILogger<EmailPidContactConfirmationObserver> logger)
    {
        _emailTemplateManager = emailTemplateManager;
        _emailService = emailService;
        _logger = logger;
    }

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(PidContactEventArgs value)
    {
        var parseObject = new
        {
            value.Pid,
            RequesterName = value.EnquirerName,
            RequesterEmail = value.EnquirerEmail,
            RequesterMessage = value.EnquirerMessage,
        };

        try
        {
            const string language = "en"; // Send only English email

            _emailService.Send(
                value.EnquirerEmail,
                _emailTemplateManager.PidContactConfirmationTemplates[language].ParseSubject(parseObject),
                _emailTemplateManager.PidContactConfirmationTemplates[language].ParseMessage(parseObject),
                isHtml: true
            );
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, @"Failed to send email to enquirer for PID: ""{Pid}"".", value.Pid);
        }
    }
}