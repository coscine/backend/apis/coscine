﻿using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailResourceCreationObserver : IResourceCreationObserver
{
    private readonly IEmailTemplateManager _emailTemplateManager;
    private readonly IEmailService _emailService;
    private readonly ILogger<EmailResourceCreationObserver> _logger;

    public EmailResourceCreationObserver(
        IEmailTemplateManager emailTemplateManager,
        IEmailService emailService,
        ILogger<EmailResourceCreationObserver> logger)
    {
        _emailTemplateManager = emailTemplateManager;
        _emailService = emailService;
        _logger = logger;
    }

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(ResourceCreatedEventArgs value)
    {
        try
        {
            var parseObject = new { ResourceName = value.Resource.DisplayName, ProjectName = value.Resource.ProjectResources.First().Project.DisplayName };

            var language = "en";

            if (value.Creator.Language is not null)
            {
                language = value.Creator.Language.Abbreviation;
            }

            _emailService.Send(value.Creator.EmailAddress,
            _emailTemplateManager.ResourceCreatedTemplates[language].ParseSubject(parseObject),
            _emailTemplateManager.ResourceCreatedTemplates[language].ParseMessage(parseObject),
            isHtml: true);
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, @"Failed to send email for resource creation of resource: ""{ResourceId}"".", value.Resource.Id);
        }
    }
}