﻿using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailPidResourceContactObserver : IPidResourceContactObserver
{
    private readonly IEmailTemplateManager _emailTemplateManager;
    private readonly IEmailService _emailService;
    private readonly ILogger<EmailPidResourceContactObserver> _logger;

    public EmailPidResourceContactObserver(
        IEmailTemplateManager emailTemplateManager,
        IEmailService emailService,
        ILogger<EmailPidResourceContactObserver> logger)
    {
        _emailTemplateManager = emailTemplateManager;
        _emailService = emailService;
        _logger = logger;
    }

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(PidContactEventArgs value)
    {
        foreach (var recipent in value.Recipients)
        {
            var parseObject = new
            {
                value.Pid,
                value.ResourceName,
                value.ProjectName,
                RequesterName = value.EnquirerName,
                RequesterEmail = value.EnquirerEmail,
                RequesterMessage = value.EnquirerMessage,
                TargetName = recipent.DisplayName
            };

            try
            {
                var language = "en";

                if (recipent.Language is not null)
                {
                    language = recipent.Language.Abbreviation;
                }

                _emailService.Send(
                    recipent.EmailAddress,
                    _emailTemplateManager.PidContactResourceTemplates[language].ParseSubject(parseObject),
                    _emailTemplateManager.PidContactResourceTemplates[language].ParseMessage(parseObject),
                    isHtml: true
                );
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, @"Failed to send email to PID owner of resource pid: ""{value.Pid}"" and user id: ""{recipent.Id}"".", value.Pid, recipent.Id);
            }
        }
    }
}