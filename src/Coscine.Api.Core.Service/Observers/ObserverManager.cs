﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class ObserverManager(
    IEmailTemplateManager emailTemplateManager,
    IEmailService emailService,
    ILogger<EmailAPCreationRequestObserver> apCreationRequestObserverLogger,
    ILogger<EmailAPCreationRequestServiceDeskObserver> apCreationRequestServiceDeskObserversLogger,
    ILogger<EmailPidContactConfirmationObserver> pidContactConfirmationObserversLogger,
    ILogger<EmailPidProjectContactObserver> pidProjectConfirmationObserversLogger,
    ILogger<EmailPidResourceContactObserver> pidResourceConfirmationObserversLogger,
    ILogger<EmailProjectCreationObserver> projectCreationObserversLogger,
    ILogger<EmailProjectResponsibleOrganizationChangeObserver> projectResponsibleOrganizationChangeObserversLogger,
    ILogger<EmailProjectDeletionObserver> projectDeletionObserversLogger,
    ILogger<EmailProjectInvitationCreationObserver> projectInvitationCreationObserversLogger,
    ILogger<EmailProjectRoleCreationObserver> projectRoleCreationObserversLogger,
    ILogger<EmailProjectRoleUpdateObserver> projectRoleUpdateObserversLogger,
    ILogger<EmailProjectRoleDeletionObserver> projectRoleDeletionObserversLogger,
    ILogger<EmailResourceCreationObserver> resourceCreationObserversLogger,
    ILogger<EmailResourceMaintenanceModeEnabledObserver> resourceMaintenanceModeEnabledObserversLogger,
    ILogger<EmailResourceMaintenanceModeDisabledObserver> resourceMaintenanceModeDisabledObserversLogger,
    ILogger<EmailResourceDeletionObserver> resourceDeletionObserversLogger,
    ILogger<EmailUserEmailChangedObserver> userEmailChangedObserversLogger,
    IOptionsMonitor<ConnectionConfiguration> connectionConfiguration
    ) : IObserverManager
{
    private readonly Lazy<IEnumerable<IAPCreationRequestObserver>> _apCreationRequestObservers = new(() => [new EmailAPCreationRequestObserver(emailTemplateManager, emailService, apCreationRequestObserverLogger)]);
    private readonly Lazy<IEnumerable<IAPCreationRequestServiceDeskObserver>> _apCreationRequestServiceDeskObservers = new(() => [new EmailAPCreationRequestServiceDeskObserver(emailTemplateManager, emailService, apCreationRequestServiceDeskObserversLogger, connectionConfiguration)]);
    private readonly Lazy<IEnumerable<IPidContactConfirmationObserver>> _pidContactConfirmationObservers = new(() => [new EmailPidContactConfirmationObserver(emailTemplateManager, emailService, pidContactConfirmationObserversLogger)]);
    private readonly Lazy<IEnumerable<IPidProjectContactObserver>> _pidProjectConfirmationObservers = new(() => [new EmailPidProjectContactObserver(emailTemplateManager, emailService, pidProjectConfirmationObserversLogger)]);
    private readonly Lazy<IEnumerable<IPidResourceContactObserver>> _pidResourceConfirmationObservers = new(() => [new EmailPidResourceContactObserver(emailTemplateManager, emailService, pidResourceConfirmationObserversLogger)]);
    private readonly Lazy<IEnumerable<IProjectCreationObserver>> _projectCreationObservers = new(() => [new EmailProjectCreationObserver(emailTemplateManager, emailService, projectCreationObserversLogger)]);
    private readonly Lazy<IEnumerable<IProjectResponsibleOrganizationChangeObserver>> _projectResponsibleOrganizationChangeObservers = new(() => [new EmailProjectResponsibleOrganizationChangeObserver(emailTemplateManager, emailService, projectResponsibleOrganizationChangeObserversLogger)]);
    private readonly Lazy<IEnumerable<IProjectDeletionObserver>> _projectDeletionObservers = new(() => [new EmailProjectDeletionObserver(emailTemplateManager, emailService, projectDeletionObserversLogger)]);
    private readonly Lazy<IEnumerable<IProjectInvitationCreationObserver>> _projectInvitationCreationObservers = new(() => [new EmailProjectInvitationCreationObserver(emailTemplateManager, emailService, projectInvitationCreationObserversLogger, connectionConfiguration)]);
    private readonly Lazy<IEnumerable<IProjectRoleCreationObserver>> _projectRoleCreationObservers = new(() => [new EmailProjectRoleCreationObserver(emailTemplateManager, emailService, projectRoleCreationObserversLogger)]);
    private readonly Lazy<IEnumerable<IProjectRoleUpdateObserver>> _projectRoleUpdateObservers = new(() => [new EmailProjectRoleUpdateObserver(emailTemplateManager, emailService, projectRoleUpdateObserversLogger)]);
    private readonly Lazy<IEnumerable<IProjectRoleDeletionObserver>> _projectRoleDeletionObservers = new(() => [new EmailProjectRoleDeletionObserver(emailTemplateManager, emailService, projectRoleDeletionObserversLogger)]);
    private readonly Lazy<IEnumerable<IResourceCreationObserver>> _resourceCreationObservers = new(() => [new EmailResourceCreationObserver(emailTemplateManager, emailService, resourceCreationObserversLogger)]);
    private readonly Lazy<IEnumerable<IResourceMaintenanceModeEnabledObserver>> _resourceMaintenanceModeEnabledObservers = new(() => [new EmailResourceMaintenanceModeEnabledObserver(emailTemplateManager, emailService, resourceMaintenanceModeEnabledObserversLogger)]);
    private readonly Lazy<IEnumerable<IResourceMaintenanceModeDisabledObserver>> _resourceMaintenanceModeDisabledObservers = new(() => [new EmailResourceMaintenanceModeDisabledObserver(emailTemplateManager, emailService, resourceMaintenanceModeDisabledObserversLogger)]);
    private readonly Lazy<IEnumerable<IResourceDeletionObserver>> _resourceDeletionObservers = new(() => [new EmailResourceDeletionObserver(emailTemplateManager, emailService, resourceDeletionObserversLogger)]);
    private readonly Lazy<IEnumerable<IUserEmailChangedObserver>> _userEmailChangedObservers = new(() => [new EmailUserEmailChangedObserver(emailTemplateManager, emailService, userEmailChangedObserversLogger)]);

    public IEnumerable<IAPCreationRequestObserver> APCreationRequestObservers => _apCreationRequestObservers.Value;
    public IEnumerable<IAPCreationRequestServiceDeskObserver> APCreationRequestServiceDeskObservers => _apCreationRequestServiceDeskObservers.Value;
    public IEnumerable<IPidContactConfirmationObserver> PidContactConfirmationObservers => _pidContactConfirmationObservers.Value;
    public IEnumerable<IPidProjectContactObserver> PidProjectContactObservers => _pidProjectConfirmationObservers.Value;
    public IEnumerable<IPidResourceContactObserver> PidResourceContactObservers => _pidResourceConfirmationObservers.Value;
    public IEnumerable<IProjectCreationObserver> ProjectCreationObservers => _projectCreationObservers.Value;
    public IEnumerable<IProjectResponsibleOrganizationChangeObserver> ProjectResponsibleOrganizationChangeObservers => _projectResponsibleOrganizationChangeObservers.Value;
    public IEnumerable<IProjectDeletionObserver> ProjectDeletionObservers => _projectDeletionObservers.Value;
    public IEnumerable<IResourceCreationObserver> ResourceCreationObservers => _resourceCreationObservers.Value;
    public IEnumerable<IResourceMaintenanceModeEnabledObserver> ResourceMaintenanceModeEnabledObservers => _resourceMaintenanceModeEnabledObservers.Value;
    public IEnumerable<IResourceMaintenanceModeDisabledObserver> ResourceMaintenanceModeDisabledObservers => _resourceMaintenanceModeDisabledObservers.Value;
    public IEnumerable<IResourceDeletionObserver> ResourceDeletionObservers => _resourceDeletionObservers.Value;
    public IEnumerable<IUserEmailChangedObserver> UserEmailChangedObservers => _userEmailChangedObservers.Value;
    public IEnumerable<IProjectInvitationCreationObserver> ProjectInvitationCreationObservers => _projectInvitationCreationObservers.Value;
    public IEnumerable<IProjectRoleCreationObserver> ProjectRoleCreationObservers => _projectRoleCreationObservers.Value;
    public IEnumerable<IProjectRoleUpdateObserver> ProjectRoleUpdateObservers => _projectRoleUpdateObservers.Value;
    public IEnumerable<IProjectRoleDeletionObserver> ProjectRoleDeletionObservers => _projectRoleDeletionObservers.Value;
}