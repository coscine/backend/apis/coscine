using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailProjectResponsibleOrganizationChangeObserver(
    IEmailTemplateManager emailTemplateManager,
    IEmailService emailService,
    ILogger<EmailProjectResponsibleOrganizationChangeObserver> logger) : IProjectResponsibleOrganizationChangeObserver
{
    private readonly IEmailTemplateManager _emailTemplateManager = emailTemplateManager;
    private readonly IEmailService _emailService = emailService;
    private readonly ILogger<EmailProjectResponsibleOrganizationChangeObserver> _logger = logger;

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(ProjectResponsibleOrganizationChangedEventArgs value)
    {
        var parseObject = new
        {
            ProjectName = value.Project.DisplayName,
            OldOrganisation = value.OldOrganization?.DisplayName ?? "",
            NewOrganisation = value.NewOrganization?.DisplayName ?? ""
        };

        foreach (var recipent in value.Recipients)
        {
            try
            {
                var language = "en";

                if (recipent.Language is not null)
                {
                    language = recipent.Language.Abbreviation;
                }
                
                _emailService.Send(recipent.EmailAddress,
                    _emailTemplateManager.ProjectResponsibleOrganizationChangeTemplates[language].ParseSubject(parseObject),
                    _emailTemplateManager.ProjectResponsibleOrganizationChangeTemplates[language].ParseMessage(parseObject),
                    isHtml: true);
            }
            catch (Exception ex)
            {
                _logger.LogWarning(ex, @"Failed to send email for project responsible organization change of project: ""{ProjectId}"" and user: ""{recipentId}"".", value.Project.Id, recipent.Id);
            }
        }
    }
}