﻿using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailProjectCreationObserver : IProjectCreationObserver
{
    private readonly IEmailTemplateManager _emailTemplateManager;
    private readonly IEmailService _emailService;
    private readonly ILogger<EmailProjectCreationObserver> _logger;

    public EmailProjectCreationObserver(
        IEmailTemplateManager emailTemplateManager,
        IEmailService emailService,
        ILogger<EmailProjectCreationObserver> logger)
    {
        _emailTemplateManager = emailTemplateManager;
        _emailService = emailService;
        _logger = logger;
    }

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(ProjectCreatedEventArgs value)
    {
        try
        {
            var parseObject = new { ProjectName = value.Project.DisplayName };

            var language = "en";

            if (value.Creator.Language is not null)
            {
                language = value.Creator.Language.Abbreviation;
            }

            _emailService.Send(value.Creator.EmailAddress,
                _emailTemplateManager.ProjectCreatedTemplates[language].ParseSubject(parseObject),
                _emailTemplateManager.ProjectCreatedTemplates[language].ParseMessage(parseObject),
                isHtml: true);
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, @"Failed to send email for project creation of project: ""{ProjectId}"".", value.Project.Id);
        }
    }
}