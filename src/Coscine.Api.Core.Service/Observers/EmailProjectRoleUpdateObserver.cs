﻿using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Microsoft.Extensions.Logging;
using NETCore.MailKit.Core;

namespace Coscine.Api.Core.Service.Observers;

public class EmailProjectRoleUpdateObserver : IProjectRoleUpdateObserver
{
    private readonly IEmailTemplateManager _emailTemplateManager;
    private readonly IEmailService _emailService;
    private readonly ILogger<EmailProjectRoleUpdateObserver> _logger;

    public EmailProjectRoleUpdateObserver(
        IEmailTemplateManager emailTemplateManager,
        IEmailService emailService,
        ILogger<EmailProjectRoleUpdateObserver> logger)
    {
        _emailTemplateManager = emailTemplateManager;
        _emailService = emailService;
        _logger = logger;
    }

    public void OnCompleted()
    {
    }

    public void OnError(Exception error)
    {
        throw error;
    }

    public void OnNext(ProjectRoleUpdateEventArgs value)
    {
        try
        {
            if (value.ProjectRole.User.EmailAddress is not null)
            {
                var parseObject = new { ProjectName = value.ProjectRole.Project.DisplayName };

                var language = "en";

                if (value.ProjectRole.User.Language is not null)
                {
                    language = value.ProjectRole.User.Language.Abbreviation;
                }

                _emailService.Send(value.ProjectRole.User.EmailAddress,
                    _emailTemplateManager.UserRoleChangedTemplates[language].ParseSubject(parseObject),
                    _emailTemplateManager.UserRoleChangedTemplates[language].ParseMessage(parseObject),
                    isHtml: true);
            }
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, @"Failed to send email for project role update of project role with id: ""{RelationId}"".", value.ProjectRole.RelationId);
        }
    }
}