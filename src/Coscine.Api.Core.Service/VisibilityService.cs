using AutoMapper;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service;

public class VisibilityService : IVisibilityService
{
    private readonly IVisibilityRepository _visibilityRepository;
    private readonly IMapper _mapper;

    public VisibilityService(
        IVisibilityRepository visibilityRepository,
        IMapper mapper)
    {
        _visibilityRepository = visibilityRepository;
        _mapper = mapper;
    }

    public async Task<PagedEnumerable<VisibilityDto>> GetPagedVisibilities(VisibilityParameters visibilityParameters, bool trackChanges)
    {
        var visibilitiesDb = await _visibilityRepository.GetPagedAsync(visibilityParameters, trackChanges);
        var visibilitiesDto = _mapper.Map<IEnumerable<VisibilityDto>>(visibilitiesDb);

        return new PagedEnumerable<VisibilityDto>(visibilitiesDto, visibilitiesDb.Pagination);
    }

    public async Task<VisibilityDto> GetVisibilityById(Guid id, bool trackChanges)
    {
        var visibilityEntity = await _visibilityRepository.GetAsync(id, trackChanges) ?? throw new VisibilityNotFoundException(id);

        return _mapper.Map<VisibilityDto>(visibilityEntity);
    }
}