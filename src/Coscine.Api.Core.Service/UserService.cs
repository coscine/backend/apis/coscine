using AutoMapper;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.Conflict;
using Coscine.Api.Core.Entities.Exceptions.InternalServerError;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Coscine.Api.Core.Service.Extensions.Utility;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Net.Mail;
using System.Reactive.Subjects;
using System.Security.Claims;

namespace Coscine.Api.Core.Service;

public class UserService : IUserService
{
    private readonly IRepositoryContextLoader _repositoryContextLoader;
    private readonly IUserRepository _userRepository;
    private readonly IUserDisciplineRepository _userDisciplineRepository;
    private readonly IExternalIdRepository _externalIdRepository;
    private readonly ITermsOfServiceRepository _termsOfServiceRepository;
    private readonly ITitleRepository _titleRepository;
    private readonly ILanguageRepository _languageRepository;
    private readonly IDisciplineRepository _disciplineRepository;
    private readonly IContactChangeRepository _contactChangeRepository;
    private readonly IOrganizationService _organizationService;
    private readonly IOrganizationRepository _organizationRepository;
    private readonly IProjectRoleRepository _projectRoleRepository;
    private readonly IProjectInvitationRepository _projectInvitationRepository;
    private readonly IResourceRepository _resourceRepository;
    private readonly IApiTokenRepository _apiTokenRepository;
    private readonly IActivityLogRepository _activityLogRepository;
    private readonly IMapper _mapper;
    private readonly IAuthenticatorService _authentication;
    private readonly ILogger<UserService> _logger;
    private readonly TermsOfServiceConfiguration _tosConfiguration;
    private readonly JwtConfiguration _jwtConfiguration;
    private readonly AuthenticationConfiguration _authenticationConfiguration;

    private readonly Subject<UserEmailChangedEventArgs> _userEmailChanged = new();

    public UserService(
        IRepositoryContextLoader repositoryContextLoader,
        IUserRepository userRepository,
        IUserDisciplineRepository userDisciplineRepository,
        IExternalIdRepository externalIdRepository,
        ITermsOfServiceRepository termsOfServiceRepository,
        ITitleRepository titleRepository,
        ILanguageRepository languageRepository,
        IDisciplineRepository disciplineRepository,
        IContactChangeRepository contactChangeRepository,
        IOrganizationService organizationService,
        IOrganizationRepository organizationRepository,
        IProjectRoleRepository projectRoleRepository,
        IProjectInvitationRepository projectInvitationRepository,
        IResourceRepository resourceRepository,
        IApiTokenRepository apiTokenRepository,
        IActivityLogRepository activityLogRepository,
        IMapper mapper,
        IAuthenticatorService authentication,
        IObserverManager observerManager,
        IOptionsMonitor<TermsOfServiceConfiguration> tosConfiguration,
        IOptionsMonitor<JwtConfiguration> jwtConfiguration,
        IOptionsMonitor<AuthenticationConfiguration> authenticationConfiguration,
        ILogger<UserService> logger
    )
    {
        _repositoryContextLoader = repositoryContextLoader;
        _userRepository = userRepository;
        _userDisciplineRepository = userDisciplineRepository;
        _externalIdRepository = externalIdRepository;
        _termsOfServiceRepository = termsOfServiceRepository;
        _titleRepository = titleRepository;
        _languageRepository = languageRepository;
        _disciplineRepository = disciplineRepository;
        _contactChangeRepository = contactChangeRepository;
        _organizationService = organizationService;
        _organizationRepository = organizationRepository;
        _projectRoleRepository = projectRoleRepository;
        _projectInvitationRepository = projectInvitationRepository;

        _resourceRepository = resourceRepository;
        _apiTokenRepository = apiTokenRepository;
        _activityLogRepository = activityLogRepository;

        _mapper = mapper;
        _authentication = authentication;
        _logger = logger;

        _tosConfiguration = tosConfiguration.CurrentValue;
        _jwtConfiguration = jwtConfiguration.CurrentValue;
        _authenticationConfiguration = authenticationConfiguration.CurrentValue;

        _userEmailChanged.SubscribeAll(observerManager.UserEmailChangedObservers);

        // Add explicit check for the ToS Version, because there's currently no other check in place.
        // HINT: This check is not needed, if the ToS version is checked in the Configuration Builder.
        ArgumentException.ThrowIfNullOrEmpty(_tosConfiguration.Version);
    }

    public async Task<PagedEnumerable<UserDto>> GetPagedUsersAsync(UserAdminParameters userParameters, bool trackChanges)
    {
        var userEntities = await _userRepository.GetPagedAsync(userParameters, trackChanges);

        var userDtos = new List<UserDto>();
        // Create UserDto instances one by one and add DTO to the usersDtos list.
        foreach (var userEntity in userEntities)
        {
            var userDto = _mapper.Map<UserDto>(userEntity);

            // Get the terms of service information
            var termsOfServiceParameters = new TermsOfServiceParameters { Version = _tosConfiguration.Version };
            var tosAcceptedEntities = await _termsOfServiceRepository.GetAllAsync(
                userEntity.Id,
                termsOfServiceParameters,
                trackChanges
            );

            // Get the user organizations information
            var userOrganizationDtos = await _organizationService.GetOrganizationsForUserExternalIdsAsync(userEntity);

            userDto.AreToSAccepted = tosAcceptedEntities.Any();
            userDto.Organizations = userOrganizationDtos;

            userDtos.Add(userDto);
        }

        return new PagedEnumerable<UserDto>(userDtos, userEntities.Pagination);
    }

    public async Task<IEnumerable<PublicUserDto>> GetFirstTenPublicUsersAsync(
        ClaimsPrincipal principal,
        UserSearchParameters userParameters,
        bool trackChanges
    )
    {
        var publicUserEntities = await _userRepository.GetFirstTenUsersAsync(userParameters, trackChanges);

        return _mapper.Map<IEnumerable<PublicUserDto>>(publicUserEntities);
    }

    public async Task<UserDto> GetUserAsync(ClaimsPrincipal principal, bool trackChanges)
    {
        var userEntity = await _authentication.GetUserAsync(principal, trackChanges) ?? throw new UserNotFoundException();

        // Get the terms of service information
        var tosAccepted = _tosConfiguration.Version is not null
            && await _termsOfServiceRepository.HasAccepted(userEntity.Id, _tosConfiguration.Version, trackChanges: false);

        // Get the user organizations information
        var userOrganizationDtos = await _organizationService.GetOrganizationsForUserExternalIdsAsync(userEntity);

        // Begin mapping to DTO
        var userDto = _mapper.Map<UserDto>(userEntity);
        userDto.AreToSAccepted = tosAccepted;
        userDto.Organizations = userOrganizationDtos;

        return userDto;
    }

    public async Task<UserDto> GetUserByIdAsync(Guid userId, bool trackChanges)
    {
        var userEntity = await _userRepository.GetAsync(userId, trackChanges) ?? throw new UserNotFoundException();
        return _mapper.Map<UserDto>(userEntity);
    }

    public async Task UpdateUserAsync(
        ClaimsPrincipal principal,
        UserForUpdateDto userForUpdateDto,
        HttpContext httpContext,
        bool trackChanges
    )
    {
        // Make sure, to not load from the memory cache!
        // Otherwise, the entity is not attached to the db context
        var userEntity = await _authentication.GetUserAsync(principal, trackChanges, allowCached: false) ?? throw new UserNotFoundException();

        var hasContactChangeIntent = !userForUpdateDto.Email.Equals(userEntity.EmailAddress, StringComparison.OrdinalIgnoreCase);

        _mapper.Map(userForUpdateDto, userEntity);

        if (userForUpdateDto.Title is not null)
        {
            userEntity.Title =
                await _titleRepository.GetAsync(userForUpdateDto.Title.Id, trackChanges)
                ?? throw new TitleNotFoundException(userForUpdateDto.Title.Id);
        }
        userEntity.Language =
            await _languageRepository.GetAsync(userForUpdateDto.Language.Id, trackChanges)
            ?? throw new LanguageNotFoundException(userForUpdateDto.Language.Id);

        // We have to check, which disciplines were added and which should be removed.
        // The except function can be used to generate a diff.
        // A list with the new values and a list with the obsolete values.
        var disciplineIdsToAdd = userForUpdateDto
            .Disciplines.Select(dto => dto.Id)
            .Except(userEntity.UserDisciplines.Select(ud => ud.DisciplineId));
        var disciplineIdsToRemove = userEntity
            .UserDisciplines.Select(ud => ud.DisciplineId)
            .Except(userForUpdateDto.Disciplines.Select(dto => dto.Id));

        // Add new disciplines
        foreach (var disciplineId in disciplineIdsToAdd)
        {
            var disciplineEntity =
                await _disciplineRepository.GetAsync(disciplineId, trackChanges)
                ?? throw new DisciplineNotFoundException(disciplineId);
            userEntity.UserDisciplines.Add(new UserDiscipline { DisciplineId = disciplineEntity.Id, UserId = userEntity.Id });
        }

        // Remove old disciplines
        foreach (var disciplineId in disciplineIdsToRemove)
        {
            var disciplineEntity =
                await _disciplineRepository.GetAsync(disciplineId, trackChanges)
                ?? throw new DisciplineNotFoundException(disciplineId);

            // Due to the previous acquisition of the list, First() should always work.
            var userDiscipline = userEntity.UserDisciplines.First(ud => ud.DisciplineId == disciplineEntity.Id);

            // The value has to be removed from the database and not from the list.
            // List removal only sets the FK to null!
            _userDisciplineRepository.Delete(userDiscipline);
        }

        // Handle contact change
        if (hasContactChangeIntent)
        {
            // User initiates a new email change. Delete all previous contact change requests by the user.
            var contactChangeEntitesToDelete = await _contactChangeRepository.GetAllAsync(
                userEntity.Id,
                new ContactChangeParameters(),
                trackChanges
            );
            foreach (var contactChangeEntity in contactChangeEntitesToDelete)
            {
                _contactChangeRepository.Delete(contactChangeEntity);
            }

            var contactChangeToCreate = _mapper.Map<ContactChange>(userForUpdateDto);
            contactChangeToCreate.User = userEntity; // UserId not mapped, because not in dto.

            _contactChangeRepository.Create(contactChangeToCreate);

            // Ensure to save the changes before the observer is triggered.
            // If anything is wrong better fail here, instead of forwarding incorrect information.
            await _repositoryContextLoader.SaveAsync();

            _userEmailChanged.OnNext(
                new UserEmailChangedEventArgs
                {
                    ContactChange = contactChangeToCreate,
                    BaseUrl = string.Format("{0}://{1}", httpContext.Request.Scheme, httpContext.Request.Host)
                }
            );
        }

        await _repositoryContextLoader.CreateExecutionStrategy().ExecuteAsync(async () =>
        {
            using var transaction = await _repositoryContextLoader.BeginTransactionAsync();
            try
            {
                await _repositoryContextLoader.SaveAsync();

                await _userRepository.UpdateGraphAsync(userEntity);

                await transaction.CommitAsync();
            }
            catch (Exception)
            {
                // Ef is rolledback automatically

                // A bit strange to update again, but we have to ensure, that the old data is restored
                // This scenario is rather unlikely to happen so.

                userEntity = await _authentication.GetUserAsync(principal, trackChanges, allowCached: false) ?? throw new UserNotFoundException();

                await _userRepository.UpdateGraphAsync(userEntity);

                // Handle has no rollback yet

                // Rethrow the exception
                throw;
            }
        });
    }

    public async Task AcceptTermsOfServiceAsync(
        UserTermsOfServiceAcceptDto userTermsOfServiceAcceptDto,
        ClaimsPrincipal principal,
        bool trackChanges
    )
    {
        var userEntity = await _authentication.GetUserAsync(principal, trackChanges) ?? throw new UserNotFoundException();

        var currentToSVersion = _tosConfiguration.Version ?? throw new TermsOfServiceInternalServerErrorException();

        // Check if the provided version is the current version
        if (currentToSVersion != userTermsOfServiceAcceptDto.Version)
        {
            throw new TermsOfServiceVersionMismatchBadRequestException(
                userEntity.Id,
                userTermsOfServiceAcceptDto.Version,
                currentToSVersion
            );
        }

        var termsOfServiceParameters = new TermsOfServiceParameters { Version = currentToSVersion };
        var tosAcceptedEntities = await _termsOfServiceRepository.GetAllAsync(
            userEntity.Id,
            termsOfServiceParameters,
            trackChanges
        );
        if (tosAcceptedEntities.Any(tos => tos.Version.Equals(currentToSVersion)))
        {
            // Terms of Service are already accepted.
            return;
        }
        _termsOfServiceRepository.Create(new Tosaccepted { UserId = userEntity.Id, Version = currentToSVersion });
        await _repositoryContextLoader.SaveAsync();
    }

    public async Task ConfirmUserEmailAsync(UserEmailConfirmationDto userEmailConfirmationDto, bool trackChanges)
    {
        var contactChangeEntity =
            await _contactChangeRepository.GetAsync(userEmailConfirmationDto.ConfirmationToken, trackChanges)
            ?? throw new EmailConfirmationTokenNotFoundException(userEmailConfirmationDto.ConfirmationToken);

        var userEntity =
            await _userRepository.GetAsync(contactChangeEntity.UserId, trackChanges) ?? throw new UserNotFoundException();

        // Calculate the expiration date for the email change
        var expirationDateTime = contactChangeEntity.EditDate?.AddHours(23).AddMinutes(59).AddSeconds(59);
        // The token is expired if the current time is later than the expiration time
        if (expirationDateTime is null && DateTime.Now > expirationDateTime)
        {
            throw new EmailConfirmationTokenExpiredBadRequestException(userEntity.Id, contactChangeEntity.ConfirmationToken);
        }

        userEntity.EmailAddress = contactChangeEntity.NewEmail;

        _contactChangeRepository.Delete(contactChangeEntity);
        await _repositoryContextLoader.SaveAsync();
    }

    public async Task<UserMergeDto> CreateMergeTokenForUserAsync(
        ClaimsPrincipal principal,
        UserMergeExternalAuthDto userMergeExternalAuthDto,
        HttpContext httpContext,
        bool trackChanges
    )
    {
        var userEntity = await _authentication.GetUserAsync(principal, trackChanges) ?? throw new UserNotFoundException();

        var userExternalIdEntities = await _externalIdRepository.GetAllAsync(userEntity.Id, trackChanges);

        // Check if the user has already an external authenticator provider already connected
        if (
            userExternalIdEntities.Any(eid =>
                userMergeExternalAuthDto
                    .IdentityProvider.ToString()
                    .Equals(eid.ExternalAuthenticator.DisplayName, StringComparison.OrdinalIgnoreCase)
            )
        )
        {
            throw new ExternalAuthAlreadyConnectedConflictException(userEntity.Id, userMergeExternalAuthDto.IdentityProvider);
        }

        // Generate merge JWT token
        var mergeToken = _authentication.GenerateJwtSecurityToken(
            new Dictionary<string, string>
            {
                { "LoginMethod", userMergeExternalAuthDto.IdentityProvider.ToString().ToLowerInvariant() }, // Writing lowercase to be STS compatible
                { "UserGuid", userEntity.Id.ToString() }
            },
            _jwtConfiguration.JsonWebKeys.First(k => k.KeySize >= 256)
        );

        var mergeTokenString = _authentication.GetJwtStringFromToken(mergeToken);

        // Add the generated token as a Cookie to the response
        const string mergeTokenKey = "coscine.mergetoken";
        if (httpContext.Request.Cookies.ContainsKey(mergeTokenKey))
        {
            httpContext.Response.Cookies.Delete(mergeTokenKey);
        }

        var cookieOptions = new CookieOptions { HttpOnly = true, Secure = true };

        httpContext.Response.Cookies.Append(mergeTokenKey, mergeTokenString, cookieOptions);

        return new UserMergeDto { Token = mergeTokenString };
    }

    public async Task<Guid?> EnsureInternalUserAsync(ClaimsPrincipal principal, Guid externalAuthenticatorId, bool trackChanges)
    {
        Guid? userId = null;
        var externalId = principal.FindFirst(ClaimTypes.NameIdentifier)?.Value;

        if (externalId is not null)
        {
            // Make sure, that we support this provider
            if (!_authenticationConfiguration.OpenIdConnectProviders
                .Select(x => x.ExternalId)
                .Contains(externalAuthenticatorId.ToString()))
            {
                throw new ExternalAuthenticatorIdBadRequestException();
            }

            var eId = await _externalIdRepository.GetAsync(externalAuthenticatorId, externalId, trackChanges);

            User? user = null;

            // Create an external user, if needed
            if (eId is null)
            {
                var email = principal.FindFirst(ClaimTypes.Email)?.Value ?? string.Empty;
                var givenname = principal.FindFirst(ClaimTypes.GivenName)?.Value ?? string.Empty;
                var surname = principal.FindFirst(ClaimTypes.Surname)?.Value ?? string.Empty;
                var name = principal.FindFirst(ClaimTypes.Name)?.Value ?? string.Empty;

                user = new User()
                {
                    DisplayName = name ?? $"{givenname} {surname}",
                    Givenname = givenname,
                    Surname = surname,
                    EmailAddress = email,
                };

                // add affiliation/organization
                var affiliationClaims = principal.FindAll(ClaimType.Affiliation);
                // Add ExternalIds with organization.
                if (affiliationClaims.Any())
                {
                    // We use the MailAdress because the affiliation has the format like "employee@rwth-aachen.de" and we only need the host part after @.
                    var attributeScopes = affiliationClaims.Select(a => new MailAddress(a.Value).Host).Distinct();
                    // We resolve the underlying organizations.
                    var organizationUris = new HashSet<string>(); // Use HashSet to ensure uniqueness

                    foreach (var scope in attributeScopes)
                    {
                        var org = await _organizationRepository.GetByAttributeScopeAsync(scope);
                        if (org?.Uri.AbsoluteUri != null)
                        {
                            organizationUris.Add(org.Uri.AbsoluteUri); // Add to the set to maintain distinct values
                        }
                    }

                    eId = new ExternalId()
                    {
                        User = user,
                        ExternalAuthenticatorId = externalAuthenticatorId,
                        ExternalId1 = externalId,
                        Organization = organizationUris.FirstOrDefault(), // Only one organization is supported for now
                    };
                    _externalIdRepository.Create(eId);
                }
                else
                {
                    // Add entry without organization.
                    eId = new ExternalId()
                    {
                        User = user,
                        ExternalAuthenticatorId = externalAuthenticatorId,
                        ExternalId1 = externalId,
                    };
                    _externalIdRepository.Create(eId);
                }
                // TODO: update entitlements once datamanagers exist

                _userRepository.Create(user);
                _externalIdRepository.Create(eId);
                await _repositoryContextLoader.CreateExecutionStrategy().ExecuteAsync(async () =>
                {
                    using var transaction = await _repositoryContextLoader.BeginTransactionAsync();
                    try
                    {
                        await _repositoryContextLoader.SaveAsync();

                        await _userRepository.CreateGraphAsync(user);

                        await transaction.CommitAsync();
                    }
                    catch (Exception)
                    {
                        // Ef is rolledback automatically

                        // Rollback graph changes
                        await _userRepository.DeleteGraphAsync(user);

                        // Rethrow exception
                        throw;
                    }
                });
            }
            else
            {
                // Update users' affiliation
                // Make Sure its is not Shibboleth login
                if (eId.ExternalAuthenticatorId != Guid.Parse("64509960-17A6-4ED6-A54E-0833AD5453D1"))
                {
                    var affiliationClaims = principal.FindAll(ClaimType.Affiliation);
                    if (affiliationClaims.Any())
                    {
                        // We use the MailAdress because the affiliation has the format like "employee@rwth-aachen.de" and we only need the host part after @.
                        var attributeScopes = affiliationClaims.Select(a => new MailAddress(a.Value).Host).Distinct();
                        // We resolve the underlying organizations.
                        var organizationUris = new HashSet<string>(); // Use HashSet to ensure uniqueness
                        foreach (var scope in attributeScopes)
                        {
                            var org = await _organizationRepository.GetByAttributeScopeAsync(scope);
                            if (org?.Uri != null)
                            {
                                organizationUris.Add(org.Uri.AbsoluteUri); // Ensure only valid URIs are added.
                            }
                        }
                        // If the current organization is not in the list, update the organization.
                        if (eId.Organization is null || !organizationUris.Contains(eId.Organization))
                        {
                            eId.Organization = organizationUris.FirstOrDefault();
                        }
                    }
                }
                // TODO: Update entitlements once datamanagers exist
            }
            await _repositoryContextLoader.SaveAsync();

            return eId.UserId;
        }

        return userId;
    }

    public async Task MergeUser(Guid fromUserId, Guid toUserId)
    {
        _logger.LogInformation("Starting to merge {fromUserId} to {toUserId}...", fromUserId, toUserId);
        // Be carefull not to load the dependecies for the user, as you want to avoid tracking too much at once!
        // As such I only get the user from the merge function
        var fromUser = await _userRepository.MergeAsync(fromUserId, toUserId);

        await _projectRoleRepository.MergeAsync(fromUserId, toUserId);
        await _projectInvitationRepository.MergeAsync(fromUserId, toUserId);
        await _userDisciplineRepository.MergeAsync(fromUserId, toUserId);
        await _resourceRepository.MergeAsync(fromUserId, toUserId);
        await _termsOfServiceRepository.MergeAsync(fromUserId, toUserId);
        await _apiTokenRepository.MergeAsync(fromUserId, toUserId);
        await _externalIdRepository.MergeAsync(fromUserId, toUserId);
        await _contactChangeRepository.MergeAsync(fromUserId, toUserId);
        await _activityLogRepository.MergeAsync(fromUserId, toUserId);

        // Due to concurrency issues, use deferred deletion!
        _userRepository.SoftDelete(fromUser);

        // Commit transaction
        await _repositoryContextLoader.SaveAsync();

        _logger.LogInformation("Merged user {fromUserId} to {toUserId}.", fromUserId, toUserId);
    }
}
