﻿using AutoMapper;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service;

[Obsolete($"This class is obsolete and will be removed in the future. Use {nameof(MessageService)} instead.")]
public class MaintenanceService : IMaintenanceService
{
    private readonly IMaintenanceRepository _maintenanceRepository;
    private readonly IMapper _mapper;

    public MaintenanceService(
        IMaintenanceRepository maintenanceRepository,
        IMapper mapper)
    {
        _maintenanceRepository = maintenanceRepository;
        _mapper = mapper;
    }

    public async Task<PagedEnumerable<MaintenanceDto>> GetPagedCurrentMaintenances(bool trackChanges)
    {
        var maintenance = await _maintenanceRepository.GetCurrent(trackChanges); // TODO: Return all active current maintenances
        var maintenanceDto = _mapper.Map<MaintenanceDto>(maintenance);
        var maintenanceDtos = new List<MaintenanceDto>();

        if (maintenanceDto is not null)
        {
            maintenanceDtos.Add(maintenanceDto);
        }

        return new PagedEnumerable<MaintenanceDto>(maintenanceDtos, maintenanceDtos.Count, 1, 50);
    }
}