﻿using AutoMapper;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service;

public class ActivityLogService(IActivityLogRepository activityLogRepository, IMapper mapper) : IActivityLogService
{
    private readonly IActivityLogRepository _activityLogRepository = activityLogRepository;
    private readonly IMapper _mapper = mapper;

    public async Task<PagedEnumerable<ActivityLogDto>> GetPagedActivityLogs(ActivityLogParameters activityLogParameters, bool trackChanges)
    {
        var activityLogs = await _activityLogRepository.GetPagedAsync(activityLogParameters, trackChanges);
        var activityLogDtos = _mapper.Map<IEnumerable<ActivityLogDto>>(activityLogs);

        return new PagedEnumerable<ActivityLogDto>(activityLogDtos, activityLogs.Pagination);
    }
}