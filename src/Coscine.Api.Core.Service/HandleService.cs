﻿using AutoMapper;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Extensions;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using System.Security.Claims;

namespace Coscine.Api.Core.Service;

public class HandleService(
    IMapper mapper,
    IHandleRepository handleRepository,
    IAuthorizationService authorizationService,
    IOptionsMonitor<PidConfiguration> pidConfiguration
) : IHandleService
{
    private readonly IHandleRepository _handleRepository = handleRepository;
    private readonly IMapper _mapper = mapper;
    private readonly IAuthorizationService _authorizationService = authorizationService;
    private readonly PidConfiguration _pidConfiguration = pidConfiguration.CurrentValue;

    public async Task<HandleDto?> CreateHandleAsync(ClaimsPrincipal principal, string prefix, string suffix, IEnumerable<HandleValue> handleValues)
    {
        // Enforce so usage of the correct prefix
        if (!string.Equals(prefix, _pidConfiguration.Prefix, StringComparison.OrdinalIgnoreCase))
        {
            return null;
        }

        if (await _handleRepository.GetAsync(prefix, suffix) is not null)
        {
            return null;
        }

        var handle = new Handle { Values = handleValues };

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, handle, HandleOperations.Update);

        // Calling update will trigger the creation of the PID in the handle service
        await _handleRepository.UpdateAsync(prefix, suffix, handle);

        handle = await _handleRepository.GetAsync(prefix, suffix);

        return _mapper.Map<HandleDto>(handle);
    }

    public async Task<HandleDto> GetAsync(ClaimsPrincipal principal, string prefix, string suffix)
    {
        // Enforce so usage of the correct prefix
        if (!string.Equals(prefix, _pidConfiguration.Prefix, StringComparison.OrdinalIgnoreCase))
        {
            throw new PrefixNotSupportedBadRequestException(prefix);
        }

        var handle = await _handleRepository.GetAsync(prefix, suffix)
            ?? throw new HandleNotFoundException(new Pid { Prefix = prefix, Suffix = suffix });

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, handle, HandleOperations.Read);

        return _mapper.Map<HandleDto>(handle);
    }

    public async Task UpdateAsync(ClaimsPrincipal principal, string prefix, string suffix, HandleForUpdateDto handleForUpdateDto)
    {
        // Enforce so usage of the correct prefix
        if (!string.Equals(prefix, _pidConfiguration.Prefix, StringComparison.OrdinalIgnoreCase))
        {
            throw new PrefixNotSupportedBadRequestException(prefix);
        }

        _ = await _handleRepository.GetAsync(prefix, suffix)
            ?? throw new HandleNotFoundException(new Pid { Prefix = prefix, Suffix = suffix });

        var handle = _mapper.Map<Handle>(handleForUpdateDto);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, handle, HandleOperations.Update);

        await _handleRepository.UpdateAsync(prefix, suffix, handle);
    }
}