using AutoMapper;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service;

public class RoleService(
    IRoleRepository roleRepository,
    IMapper mapper) : IRoleService
{
    private readonly IRoleRepository _roleRepository = roleRepository;
    private readonly IMapper _mapper = mapper;

    public async Task<PagedEnumerable<RoleDto>> GetPagedRoles(RoleParameters roleParameters, bool trackChanges)
    {
        var rolesDb = await _roleRepository.GetPagedAsync(roleParameters, trackChanges);
        var rolesDto = _mapper.Map<IEnumerable<RoleDto>>(rolesDb);

        return new PagedEnumerable<RoleDto>(rolesDto, rolesDb.Pagination);
    }

    public async Task<RoleDto> GetRoleById(Guid id, bool trackChanges)
    {
        var roleEntity = await _roleRepository.GetAsync(id, trackChanges) ?? throw new RoleNotFoundException(id);

        return _mapper.Map<RoleDto>(roleEntity);
    }
}