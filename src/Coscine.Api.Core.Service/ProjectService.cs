using AutoMapper;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Coscine.Api.Core.Service.Extensions;
using Coscine.Api.Core.Service.Extensions.Utility;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Helpers;
using Coscine.Api.Core.Shared.QueryParameters;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Slugify;
using System.Data;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Security.Cryptography;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;

namespace Coscine.Api.Core.Service;

public sealed partial class ProjectService : IProjectService
{
    private readonly IRepositoryContextLoader _repositoryContextLoader;
    private readonly IProjectRepository _projectRepository;
    private readonly IDisciplineRepository _disciplineRepository;
    private readonly IVisibilityRepository _visibilityRepository;
    private readonly IRoleRepository _roleRepository;
    private readonly IDefaultProjectQuotaRepository _defaultProjectQuotaRepository;
    private readonly IResourceTypeRepository _resourceTypeRepository;
    private readonly IOrganizationRepository _organizationRepository;
    private readonly IProjectInstituteRepository _projectInstituteRepository;
    private readonly IPublicationRequestService _publicationRequestService;
    private readonly IProjectDisciplineRepository _projectDisciplineRepository;
    private readonly IHandleRepository _handleRepository;
    private readonly IAuthenticatorService _authenticatorService;
    private readonly IMapper _mapper;
    private readonly ILogger<ProjectService> _logger;
    private readonly IHttpContextAccessor _context;

    private readonly IAuthorizationService _authorizationService;
    private readonly IQuotaService _quotaService;
    private readonly PidConfiguration _pidConfiguration;

    // For the slugs
    private readonly string _format = "D8";

    private readonly int _maxValue = 100000000;
    private readonly int _maxLength = 53;

    private readonly SlugHelper _slugHelper;

    private readonly Subject<ProjectCreatedEventArgs> _projectCreated = new();
    private readonly Subject<ProjectResponsibleOrganizationChangedEventArgs> _projectResponsibleOrganizationChanged = new();
    private readonly Subject<ProjectDeletedEventArgs> _projectDeleted = new();

    // The service can retrieve other services using dependency injection.
    public ProjectService(
        IAuthenticatorService authenticatorService,
        IAuthorizationService authorizationService,
        IPublicationRequestService publicationRequestService,
        IQuotaService quotaService,
        IDefaultProjectQuotaRepository defaultProjectQuotaRepository,
        IDisciplineRepository disciplineRepository,
        IHandleRepository handleRepository,
        IOrganizationRepository organizationRepository,
        IProjectDisciplineRepository projectDisciplineRepository,
        IProjectInstituteRepository projectInstituteRepository,
        IProjectRepository projectRepository,
        IResourceTypeRepository resourceTypeRepository,
        IRoleRepository roleRepository,
        IVisibilityRepository visibilityRepository,
        IMapper mapper,
        IRepositoryContextLoader repositoryContextLoader,
        IObserverManager observerManager,
        ILogger<ProjectService> logger,
        IOptionsMonitor<PidConfiguration> pidConfiguration,
        IHttpContextAccessor context
    )
    {
        _authenticatorService = authenticatorService;
        _authorizationService = authorizationService;
        _publicationRequestService = publicationRequestService;
        _quotaService = quotaService;
        _defaultProjectQuotaRepository = defaultProjectQuotaRepository;
        _disciplineRepository = disciplineRepository;
        _handleRepository = handleRepository;
        _organizationRepository = organizationRepository;
        _projectDisciplineRepository = projectDisciplineRepository;
        _projectInstituteRepository = projectInstituteRepository;
        _projectRepository = projectRepository;
        _resourceTypeRepository = resourceTypeRepository;
        _roleRepository = roleRepository;
        _visibilityRepository = visibilityRepository;
        _mapper = mapper;
        _repositoryContextLoader = repositoryContextLoader;
        _logger = logger;
        _context = context;

        _pidConfiguration = pidConfiguration.CurrentValue;

        var config = new SlugHelperConfiguration();
        _slugHelper = new SlugHelper(config);

        _projectCreated.SubscribeAll(observerManager.ProjectCreationObservers);
        _projectResponsibleOrganizationChanged.SubscribeAll(observerManager.ProjectResponsibleOrganizationChangeObservers);
        _projectDeleted.SubscribeAll(observerManager.ProjectDeletionObservers);
        _projectRepository = projectRepository;
    }

    private string GenerateSlug(string? displayName)
    {
        var randomNumber = RandomNumberGenerator.GetInt32(0, _maxValue).ToString(_format);
        var slug = _slugHelper.GenerateSlug(displayName);

        return slug.Length > _maxLength ? slug[.._maxLength] : slug + "-" + randomNumber;
    }

    public async Task<ProjectDto> CreateProjectForUserAsync(ProjectForCreationDto projectForCreationDto)
    {
        var user = await _authenticatorService.GetUserAsync(_context.HttpContext.User, trackChanges: false) ?? throw new UserNotFoundException();

        var projectEntity = _mapper.Map<Project>(projectForCreationDto);

        projectEntity.Id = Guid.NewGuid();

        projectEntity.Slug = GenerateSlug(projectForCreationDto.DisplayName);

        _projectRepository.Create(projectEntity, user.Id);

        projectEntity.Visibility = await _visibilityRepository.GetAsync(projectForCreationDto.Visibility.Id, trackChanges: true)
            ?? throw new VisibilityNotFoundException(projectForCreationDto.Visibility.Id);

        // Add disciplines
        foreach (var discipline in projectForCreationDto.Disciplines)
        {
            var disciplineEntity = await _disciplineRepository.GetAsync(discipline.Id, trackChanges: true)
                ?? throw new DisciplineNotFoundException(discipline.Id);
            projectEntity.ProjectDisciplines.Add(new ProjectDiscipline { DisciplineId = disciplineEntity.Id, ProjectId = projectEntity.Id });
        }

        // Add the organizations
        foreach (var institute in projectForCreationDto.Organizations)
        {
            projectEntity.ProjectInstitutes.Add(new ProjectInstitute { OrganizationUrl = institute.Uri.AbsoluteUri, ProjectId = projectEntity.Id, Responsible = institute.Responsible });
        }

        // Set owner
        var ownerRole = _roleRepository.GetOwnerRole();

        projectEntity.ProjectRoles.Add(
            new ProjectRole
            {
                RoleId = ownerRole.Id,
                ProjectId = projectEntity.Id,
                UserId = user.Id
            }
        );

        // Set project quota
        var defaultProjectQuotas = await _defaultProjectQuotaRepository.GetAllAsync(user.Id);
        // NOTE: Should a new resource type be added, think about setting a default quota for it too, for all existing projects.
        foreach (var resourceType in await _resourceTypeRepository.GetAllAsync(trackChanges: true))
        {
            // TODO: This needs to be refactored in the future => DisplayName from metadata store to SpecificType
            var defaultQuotas = defaultProjectQuotas.Where(x =>
                x?.ResourceType == resourceType.SpecificType ||
                x?.ResourceType + "rwth" == resourceType.SpecificType
            );

            if (defaultQuotas?.Any() == true)
            {
                var defaultQuota = defaultQuotas.First();
                if (defaultQuota is not null)
                {
                    projectEntity.ProjectQuota.Add(
                        new ProjectQuota
                        {
                            ProjectId = projectEntity.Id,
                            ResourceTypeId = resourceType.Id,
                            Quota = defaultQuota.DefaultQuota,
                            MaxQuota = defaultQuota.DefaultMaxQuota
                        }
                    );
                }
            }
        }

        // Link the sub project to parent, if needed
        if (projectForCreationDto.ParentId is not null)
        {
            var parentEntity = await _projectRepository.GetAsync(projectForCreationDto.ParentId.Value, false)
                ?? throw new ProjectNotFoundException(projectForCreationDto.ParentId.Value);

            await _authorizationService.AuthorizeAndThrowIfUnauthorized(
                _context.HttpContext.User,
                parentEntity,
                ProjectOperations.CreateSubProject
            );

            // Adds a link between the sub project and its parent project
            projectEntity.SubProjectSubProjectNavigations.Add(new SubProject { ProjectId = projectForCreationDto.ParentId.Value, SubProjectId = projectEntity.Id });

            // Copies owners from the parent project to the sub project if requested
            if (projectForCreationDto.CopyOwnersFromParent == true)
            {
                // Retrieves potential parent project owners who are not the current user and have the specified owner role
                foreach (var parentOwner in parentEntity.ProjectRoles.Where(parentOwner => parentOwner.UserId != user.Id && parentOwner.RoleId == ownerRole.Id))
                {
                    // Adds the eligible parent project owners as owners of the sub project
                    projectEntity.ProjectRoles.Add(new ProjectRole
                    {
                        RoleId = ownerRole.Id,
                        ProjectId = projectEntity.Id,
                        UserId = parentOwner.UserId
                    });
                }
            }
        }
        await _repositoryContextLoader.CreateExecutionStrategy().ExecuteAsync(async () =>
        {
            using var transaction = await _repositoryContextLoader.BeginTransactionAsync();
            try
            {
                await _repositoryContextLoader.SaveAsync();

                await _projectRepository.CreateGraphsAsync(projectEntity);

                // Create PID for project
                var handleValues = _handleRepository.GenerateProjectHandleValues(projectEntity);
                var handle = new Handle { Values = handleValues };

                // Calling update will trigger the creation of the PID in the handle service
                await _handleRepository.UpdateAsync(_pidConfiguration.Prefix, projectEntity.Id.ToString(), handle);

                await transaction.CommitAsync();
            }
            catch (Exception)
            {
                // Ef is rolledback automatically

                // Rollback graph changes
                await _projectRepository.DeleteGraphsAsync(projectEntity);

                // Handle has no rollback yet

                // Rethrow exception
                throw;
            }
        });

        var remoteHandle = await _handleRepository.GetAsync(_pidConfiguration.Prefix, projectEntity.Id.ToString());

        if (remoteHandle is null)
        {
            _logger.LogWarning("PID for project with id: {projectEntityId} could not be created!", projectEntity.Id);
        }

        var projectDto = _mapper.Map<ProjectDto>(projectEntity);
        projectDto.Pid = remoteHandle?.Pid?.Identifier ?? "";

        _projectCreated.OnNext(new ProjectCreatedEventArgs { Project = projectEntity, Creator = user });

        return projectDto;
    }

    public async Task DeleteProjectAsync(Guid projectId, bool trackChanges)
    {
        var projectEntity = await _projectRepository.GetAsync(projectId, trackChanges)
            ?? throw new ProjectNotFoundException(projectId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(_context.HttpContext.User, projectEntity, ProjectOperations.Delete);

        // TODO: Set all child resources that are only linked to the project for deletion as deleted.
        _projectRepository.Delete(projectEntity);

        await _repositoryContextLoader.CreateExecutionStrategy().ExecuteAsync(async () =>
        {
            using var transaction = await _repositoryContextLoader.BeginTransactionAsync();
            try
            {
                await _repositoryContextLoader.SaveAsync();

                // Create PID for project
                var handleValues = _handleRepository.GenerateProjectHandleValues(projectEntity)
            .Where(x => x.Type == "URL"); // Only keep the URL type values for deleted projects
                var handle = new Handle { Values = handleValues };

                await _projectRepository.DeleteGraphsAsync(projectEntity);

                // Calling update will trigger the creation of the PID in the handle service
                await _handleRepository.UpdateAsync(_pidConfiguration.Prefix, projectEntity.Id.ToString(), handle);

                transaction.Commit();
            }
            catch (Exception)
            {
                // Ef is rolledback automatically

                // Rollback graph changes
                await _projectRepository.UpdateGraphsAsync(projectEntity);

                // Handle has no rollback yet

                // Rethrow exception
                throw;
            }
        });

        var recipients = projectEntity.ProjectRoles.Select(x => x.User);
        _projectDeleted.OnNext(new ProjectDeletedEventArgs { Project = projectEntity, Recipients = recipients });
    }

    public async Task<PagedEnumerable<ProjectAdminDto>> GetPagedProjectsAsync(ProjectAdminParameters projectParameters, bool trackChanges)
    {
        PagedEnumerable<Project> projectEntities;

        if (projectParameters.TopLevel == true)
        {
            // IncludeDeleted flag is handled by the repository
            projectEntities = await _projectRepository.GetPagedTopLevelProjectsAsync(projectParameters, trackChanges);
        }
        else
        {
            // IncludeDeleted flag is handled by the repository
            projectEntities = await _projectRepository.GetPagedAsync(projectParameters, trackChanges);
        }

        var projectsDtos = new List<ProjectAdminDto>();
        // Create ProjectDto instances one by one, then assign options and PID, and finally add DTO to the projectsDtos list.
        // This way we can work with reference to each ProjectDto object and add it to a list when we're done.
        foreach (var projectEntity in projectEntities)
        {
            var projectDto = _mapper.Map<ProjectAdminDto>(projectEntity);

            // Include quotas on demand
            if (projectParameters.IncludeQuotas == true)
            {
                var quotasList = new List<ProjectQuotaDto>(); // Holds the quotas for the current project in a temporary list

                await foreach (
                    var quota in PaginationHelper.GetAllAsync(pageNumber =>
                        _quotaService.GetPagedQuotasForProject(
                            projectEntity.Id,
                            new() { PageNumber = pageNumber, PageSize = 50 },
                            trackChanges
                        )
                    )
                )
                {
                    // Process each quota as it's received
                    // Note: Avoid using .Append() on the original IEnumerable, as it returns a new sequence each time it's called, rather than adding to an existing collection.
                    //       Leading to the necessity that the entire sequence must be re-enumerated with each additional item (performance overhead).
                    quotasList.Add(quota);
                }

                projectDto.ProjectQuota = quotasList;
            }

            // Include publication requests on demand
            if (projectParameters.IncludePublicationRequests == true)
            {
                projectDto.PublicationRequests = await _publicationRequestService.GetAllProjectPublicationRequestsAsync(projectEntity.Id, trackChanges);
            }

            projectDto.Organizations = await ProcessOrganizationsAsync(projectEntity.ProjectInstitutes);
            projectDto.Pid = GetPid(projectDto.Id);

            projectsDtos.Add(projectDto);
        }

        return new PagedEnumerable<ProjectAdminDto>(projectsDtos, projectEntities.Pagination);
    }


    public async Task<PagedEnumerable<ProjectDto>> GetPagedProjectsOfUserAsync(ProjectParameters projectParameters, bool trackChanges)
    {
        var user = await _authenticatorService.GetUserAsync(_context.HttpContext.User, trackChanges: false) ?? throw new UserNotFoundException();

        PagedEnumerable<Project> projectEntities;

        if (projectParameters.TopLevel == true)
        {
            projectEntities = await _projectRepository.GetPagedTopLevelProjectsOfUserAsync(user.Id, projectParameters, trackChanges);
        }
        else
        {
            projectEntities = await _projectRepository.GetPagedAsync(user.Id, projectParameters, trackChanges);
        }

        // Do not check for projects rights here, cause its fucking slow. Thx.
        // Also we can trust our own call!

        var projectsDtos = new List<ProjectDto>();
        // Create ProjectDto instances one by one, then assign options and PID, and finally add DTO to the projectsDtos list.
        // This way we can work with reference to each ProjectDto object and add it to a list when we're done.
        foreach (var projectEntity in projectEntities)
        {
            var projectDto = _mapper.Map<ProjectDto>(projectEntity);

            if (projectParameters.IncludeOrganizations ?? false)
            {
                projectDto.Organizations = await ProcessOrganizationsAsync(projectEntity.ProjectInstitutes);
            }
            projectDto.Pid = GetPid(projectDto.Id);

            projectsDtos.Add(projectDto);
        }

        return new PagedEnumerable<ProjectDto>(projectsDtos, projectEntities.Pagination);
    }

    public async Task<ProjectDto> GetProjectAsync(Guid id, bool trackChanges, ProjectGetQueryParameters projectGetQueryParameters)
    {
        // Using the GetProjectByIdAsync method, so that a quota admin can also read the project.
        var projectEntity = await _projectRepository.GetAsync(id, trackChanges)
            ?? throw new ProjectNotFoundException(id);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(_context.HttpContext.User, projectEntity, ProjectOperations.Read);

        var projectDto = _mapper.Map<ProjectDto>(projectEntity);
        projectDto.Organizations = await ProcessOrganizationsAsync(projectEntity.ProjectInstitutes);
        projectDto.Pid = GetPid(projectDto.Id);

        if (projectGetQueryParameters.IncludeSubProjects == true)
        {
            var user = await _authenticatorService.GetUserAsync(trackChanges: false) ?? throw new UserNotFoundException();
            projectDto.SubProjects = _mapper.Map<IEnumerable<ProjectDto>>(
                await _projectRepository.GetAllSubProjectsOfProjectAsync(user.Id, projectEntity.Id, trackChanges)
            );
        }

        return projectDto;
    }

    private async Task UpdateProject(Project projectEntity, ProjectForUpdateDto projectForUpdateDto, bool trackChanges)
    {
        await _authorizationService.AuthorizeAndThrowIfUnauthorized(_context.HttpContext.User, projectEntity, ProjectOperations.Update);

        var updatedSlug = projectEntity.Slug;

        if (!string.Equals(projectEntity.Slug, projectForUpdateDto.Slug))
        {
            updatedSlug = GenerateSlug(projectForUpdateDto.Slug);
        }

        _mapper.Map(projectForUpdateDto, projectEntity);

        projectEntity.Slug = updatedSlug;

        projectEntity.Visibility = await _visibilityRepository.GetAsync(projectForUpdateDto.Visibility.Id, trackChanges: true);

        // We have to check, which disciplines were added and which should be removed.
        // The except function can be used to generate a diff.
        // A list with the new values and a list with the obsolete values.
        var disciplineIdsToAdd = projectForUpdateDto
            .Disciplines.Select(x => x.Id)
            .Except(projectEntity.ProjectDisciplines.Select(x => x.DisciplineId));
        var disciplineIdsToRemove = projectEntity
            .ProjectDisciplines.Select(x => x.DisciplineId)
            .Except(projectForUpdateDto.Disciplines.Select(x => x.Id));

        // Add new disciplines
        foreach (var disciplineId in disciplineIdsToAdd)
        {
            var disciplineEntity = await _disciplineRepository.GetAsync(disciplineId, trackChanges)
                ?? throw new DisciplineNotFoundException(disciplineId);
            projectEntity.ProjectDisciplines.Add(new ProjectDiscipline { DisciplineId = disciplineEntity.Id, ProjectId = projectEntity.Id });
        }

        // Remove old disciplines
        foreach (var disciplineId in disciplineIdsToRemove)
        {
            var disciplineEntity = await _disciplineRepository.GetAsync(disciplineId, trackChanges)
                ?? throw new DisciplineNotFoundException(disciplineId);

            // Due to the previous acquisition of the list, First() should always work.
            var projectDiscipline = projectEntity.ProjectDisciplines.First(x => x.DisciplineId == disciplineEntity.Id);

            // The value has to be removed from the database and not from the list.
            // List removal only sets the FK to null!
            _projectDisciplineRepository.Delete(projectDiscipline);
        }

        // Handle responsible organization
        var currentResponsibleOrganization = projectEntity.ProjectInstitutes.FirstOrDefault(x => x.Responsible == true);
        ProjectInstitute? newResponsibleOrganization = null;

        var institutesToAdd = projectForUpdateDto
            .Organizations.Select(x => (x.Uri.AbsoluteUri, x.Responsible))
            .Except(projectEntity.ProjectInstitutes.Select(x => (x.OrganizationUrl, x.Responsible)));

        // We check both Url and Responsible because otherwise we would have to filter out if the responsible status has changed.
        // e.g. we would not delete a entry with Responsible = true if the status has changed to Responsible = false
        // that would lead to some problems if we now want to add the organization with entry Responsible = false
        // ToList() because we modify the source later on and dont want conflicts with deffered and lazy loading
        var institutesToRemove = projectEntity
            .ProjectInstitutes.Select(x => (x.OrganizationUrl, x.Responsible))
            .Except(projectForUpdateDto.Organizations.Select(x => (x.Uri.AbsoluteUri, x.Responsible)))
            .ToList();

        // Remove old institute/organization
        foreach ((var instituteUrl, var responsible) in institutesToRemove)
        {
            var projectInstituteEntity = await _projectInstituteRepository.GetAsync(projectEntity.Id, instituteUrl, trackChanges)
                ?? throw new ProjectInstituteNotFoundException(instituteUrl);

            // The value has to be removed from the database and not from the list.
            // List removal only sets the FK to null!
            // We need to Remove so later the checks work properly
            // so we do both for those seperate reasons
            projectEntity.ProjectInstitutes.Remove(projectInstituteEntity);
            _projectInstituteRepository.Delete(projectInstituteEntity);
        }

        // Add new institute/organization
        foreach ((var instituteUrl, var responsible) in institutesToAdd)
        {
            projectEntity.ProjectInstitutes.Add(new ProjectInstitute { OrganizationUrl = instituteUrl, ProjectId = projectEntity.Id, Responsible = responsible });
        }

        // Check if only one responsible organization
        if (projectEntity.ProjectInstitutes.Where(x => x.Responsible == true).Count() != 1)
        {
            throw new InvalidResponsibleOrganizationBadRequestException(projectEntity.Id);
        }

        // Check for duplicates
        if (projectEntity.ProjectInstitutes.Count != projectEntity.ProjectInstitutes.DistinctBy(x => x.OrganizationUrl).Count())
        {
            throw new DuplicateProjectOrganizationBadRequestException(projectEntity.Id);
        }

        // Should the responsible organization have changed, notify the observers
        newResponsibleOrganization = projectEntity.ProjectInstitutes.First(x => x.Responsible == true);
        if (
            currentResponsibleOrganization is not null &&
            newResponsibleOrganization is not null &&
            currentResponsibleOrganization.OrganizationUrl != newResponsibleOrganization.OrganizationUrl
        )
        {
            var projectOwners = projectEntity.ProjectRoles
                .Where(x => x.RoleId == _roleRepository.GetOwnerRole().Id)
                .Select(x => x.User);
            _projectResponsibleOrganizationChanged.OnNext(
                new()
                {
                    Project = projectEntity,
                    Recipients = projectOwners,
                    OldOrganization = await _organizationRepository.GetByRorUrlAsync(new Uri(currentResponsibleOrganization.OrganizationUrl)),
                    NewOrganization = await _organizationRepository.GetByRorUrlAsync(new Uri(newResponsibleOrganization.OrganizationUrl))
                });
        }
    }

    public async Task UpdateProjectAsync(Guid projectId, ProjectForUpdateDto projectForUpdateDto, bool trackChanges)
    {
        var user = await _authenticatorService.GetUserAsync(_context.HttpContext.User, trackChanges: false);

        var projectEntity = await _projectRepository.GetAsync(projectId, trackChanges)
            ?? throw new ProjectNotFoundException(projectId);

        await UpdateProject(projectEntity, projectForUpdateDto, trackChanges);

        await _repositoryContextLoader.CreateExecutionStrategy().ExecuteAsync(async () =>
        {
            using var transaction = await _repositoryContextLoader.BeginTransactionAsync();
            try
            {
                await _repositoryContextLoader.SaveAsync();

                await _projectRepository.UpdateGraphsAsync(projectEntity);

                // Create PID for project
                var handleValues = _handleRepository.GenerateProjectHandleValues(projectEntity);
                var handle = new Handle { Values = handleValues };


                // Calling update will trigger the creation of the PID in the handle service
                await _handleRepository.UpdateAsync(_pidConfiguration.Prefix, projectEntity.Id.ToString(), handle);

                await transaction.CommitAsync();
            }
            catch (Exception)
            {
                // Ef is rolledback automatically

                // A bit strange to update again, but we have to ensure, that the old data is restored
                // This scenario is rather unlikely to happen so.
                projectEntity = await _projectRepository.GetAsync(projectId, trackChanges)
                    ?? throw new ProjectNotFoundException(projectId);

                await _projectRepository.UpdateGraphsAsync(projectEntity);

                // Handle has no rollback yet

                // Rethrow the exception
                throw;
            }
        });
    }

    /// <summary>
    /// Processes the organizations related to the project institutes.
    /// </summary>
    /// <param name="projectInstitutes">The collection of project institutes containing organization URIs.</param>
    /// <returns>A collection of <see cref="OrganizationDto"/> objects representing the processed organizations.</returns>
    private async Task<IEnumerable<ProjectOrganizationDto>> ProcessOrganizationsAsync(IEnumerable<ProjectInstitute> projectInstitutes)
    {
        var organizationDtos = new List<ProjectOrganizationDto>();
        foreach (var (projectOrganizationUri, projectResponsible) in projectInstitutes.Select(o => Tuple.Create(new Uri(o.OrganizationUrl), o.Responsible)))
        {
            var organizationEntity = await _organizationRepository.GetByRorUrlAsync(projectOrganizationUri);
            var organizationDto = _mapper.Map<ProjectOrganizationDto>(organizationEntity);
            organizationDto.Responsible = projectResponsible;
            organizationDtos.Add(organizationDto);
        }

        return organizationDtos;
    }

    /// <summary>
    /// Generates the project's PID (Persistent Identifier) string using the specified project identifier.
    /// </summary>
    /// <param name="projectId">The unique identifier for the project.</param>
    /// <returns>A string representing the generated project PID.</returns>
    private string GetPid(Guid projectId) => $"{_pidConfiguration.Prefix}/{projectId}";
}