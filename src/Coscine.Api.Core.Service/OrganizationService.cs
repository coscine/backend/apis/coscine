using AutoMapper;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service;

public class OrganizationService : IOrganizationService
{
    private readonly IOrganizationRepository _organizationRepository;
    private readonly IMapper _mapper;

    public OrganizationService(
        IOrganizationRepository organizationRepository,
        IMapper mapper)
    {
        _organizationRepository = organizationRepository;
        _mapper = mapper;
    }

    public async Task<OrganizationDto> GetOrganization(Uri organizationRorUrl, bool trackChanges)
    {
        if (!organizationRorUrl.IsAbsoluteUri)
        {
            throw new RelativeUriBadRequestException(organizationRorUrl);
        }

        var organizationEntity = await _organizationRepository.GetByRorUrlAsync(organizationRorUrl);
        return _mapper.Map<OrganizationDto>(organizationEntity);
    }

    public async Task<PagedEnumerable<OrganizationDto>> GetPagedOrganizations(OrganizationParameters organizationParameters, bool trackChanges)
    {
        var organizationEntities = await _organizationRepository.GetPagedAsync(organizationParameters);
        var organizationDtos = _mapper.Map<IEnumerable<OrganizationDto>>(organizationEntities);

        return new PagedEnumerable<OrganizationDto>(organizationDtos, organizationEntities.Pagination);
    }

    public async Task<IEnumerable<UserOrganizationDto>> GetOrganizationsForUserExternalIdsAsync(User userEntity)
    {
        var userOrganizationDtos = new List<UserOrganizationDto>();

        // Handle Graph DB entries (Shibboleth Login)
        if (userEntity.ExternalIds?.Count > 0)
        {

            var organizationEntities = new List<Organization>();

            // Get external organizations by the users external id
            foreach (var externalId in userEntity.ExternalIds)
            {
                if (externalId.Organization is not null)
                {
                    // If we are using shibboleth, resolve via shibboleth login url, saved in organization
                    // Guid is for the Shibboleth, retrieving the value was not possible atm.
                    if (externalId.ExternalAuthenticatorId == Guid.Parse("64509960-17A6-4ED6-A54E-0833AD5453D1"))
                    {
                        var org = await _organizationRepository.GetByShibbolethUrlAsync(externalId.Organization);
                        if (org is not null)
                        {
                            organizationEntities.AddRange(org);
                        }
                    }
                    // Otherwise it is oidc, and we can directly use the ror uri, saved in the organization
                    else
                    {
                        if (Uri.TryCreate(externalId.Organization, UriKind.Absolute, out var orgUri))
                        {
                            var org = await _organizationRepository.GetByRorUrlAsync(orgUri);
                            if (org is not null)
                            {
                                organizationEntities.Add(org);
                            }
                        }
                    }

                }
            }

            userOrganizationDtos.AddRange(_mapper.Map<IEnumerable<UserOrganizationDto>>(organizationEntities));
        }

        // Handle SQL DB entries (ORCiD Login), only add if no external organizations were found
        if (!string.IsNullOrWhiteSpace(userEntity.Organization) && userOrganizationDtos.Count == 0)
        {
            // Append the value coming from the SQL DB
            userOrganizationDtos.Add(_mapper.Map<UserOrganizationDto>(userEntity));
        }

        return userOrganizationDtos;
    }
}