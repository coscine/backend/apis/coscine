﻿using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.Conflict;
using Coscine.Api.Core.Entities.Exceptions.Forbidden;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Extensions;
using Coscine.Api.Core.Service.Helpers;
using Coscine.Api.Core.Shared.Enums;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace Coscine.Api.Core.Service;

public partial class BlobService(
    IProjectRepository projectRepository,
    IResourceRepository resourceRepository,
    ITreeRepository treeRepository,
    IAuthenticatorService authenticatorService,
    IAuthorizationService authorizationService,
    ITreeService treeService,
    IDataStorageRepositoryFactory dataStorageRepositoryFactory
) : IBlobService
{
    private readonly IProjectRepository _projectRepository = projectRepository;
    private readonly IResourceRepository _resourceRepository = resourceRepository;
    private readonly ITreeRepository _treeRepository = treeRepository;
    private readonly IAuthenticatorService _authenticatorService = authenticatorService;
    private readonly IAuthorizationService _authorizationService = authorizationService;
    private readonly ITreeService _treeService = treeService;
    private readonly IDataStorageRepositoryFactory _dataStorageRepositoryFactory = dataStorageRepositoryFactory;

    public async Task<Stream> GetBlobStreamAsync(Guid projectId, Guid resourceId, string key, ClaimsPrincipal principal, bool trackChanges)
    {
        ForbiddenChars.CheckKey(key);

        _ = await _projectRepository.GetAsync(projectId, trackChanges: true)
            ?? throw new ProjectNotFoundException(projectId);

        var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges)
            ?? throw new ResourceNotFoundException(resourceId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.ReadBlob);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Check if resource type supports reading blobs
        if (!dataStorage.Capabilities.Contains(Capability.CanRead))
        {
            throw new ResourceTypeCannotBeReadBadRequestException(resourceEntity.Type.DisplayName);
        }

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // Blobs of a resource cannot be read while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        return await dataStorage.ReadAsync(new ReadBlobParameters { Resource = resourceEntity, Path = key }) ?? throw new BlobNotFoundException(key);
    }

    public async Task DeleteBlobAsync(Guid projectId, Guid resourceId, string key, ClaimsPrincipal principal, bool trackChanges)
    {
        ForbiddenChars.CheckKey(key);

        var user = await _authenticatorService.GetUserAsync(principal, trackChanges: false) ?? throw new UserNotFoundException();

        var _ = await _projectRepository.GetAsync(projectId, trackChanges: true)
            ?? throw new ProjectNotFoundException(projectId);

        var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges)
            ?? throw new ResourceNotFoundException(resourceId);

        // Archived restrictions when both resource is archived
        if (resourceEntity.Archived.Equals("1"))
        {
            throw new TreeBlobResourceArchivedBadRequestException(resourceEntity.Id);
        }

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.DeleteBlob);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Prevent deletion of blobs for resource types that don't support deletion
        if (!dataStorage.Capabilities.Contains(Capability.CanDelete))
        {
            throw new ResourceTypeCannotBeDeletedBadRequestException(resourceEntity.Type.DisplayName);
        }

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // Blobs of a resource cannot be deleted while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        // Check if file exists
        var __ = await dataStorage.ReadAsync(new ReadTreeParameters { Resource = resourceEntity, Path = key }) ?? throw new BlobNotFoundException(key);

        // Invalidate data graphs
        if (dataStorage.Capabilities.Contains(Capability.SupportsDataGraphs))
        {
            var newestDataGraph = (await _treeRepository.GetNewestPagedDataAsync(resourceId, new() { Path = key }))
                .FirstOrDefault();

            if (newestDataGraph is not null)
            {
                // We're not enforcing the existence of the graph, since a file may be uploaded over S3 too for example
                var provenanceGraph = await _treeRepository.InvalidateDataGraphAsync(resourceId, key, newestDataGraph.Id, user);

                // Will check if it can copy on its own
                await _treeService.CopyMetadataLocalAsync(resourceEntity, provenanceGraph);
            }
        }

        // Invalidate metadata graphs
        if (dataStorage.Capabilities.Contains(Capability.SupportsMetadataGraphs))
        {
            var newestMetadataGraph = (await _treeRepository.GetNewestPagedMetadataAsync(resourceId, new() { Path = key }))
                .FirstOrDefault();

            if (newestMetadataGraph?.Id is not null)
            {
                // We're not enforcing the existence of the graph, since a file may be uploaded over S3 too for example
                var provenanceGraph = await _treeRepository.InvalidateMetadataGraphAsync(
                    resourceId,
                    key,
                    newestMetadataGraph.Id,
                    user
                );

                if (resourceEntity.MetadataLocalCopy && dataStorage.Capabilities.Contains(Capability.SupportsLocalMetadataCopy))
                {
                    await _treeService.CopyMetadataLocalAsync(resourceEntity, provenanceGraph);
                }
            }
        }

        // Delete the blob
        await dataStorage.DeleteAsync(new DeleteBlobParameters { Resource = resourceEntity, Path = key });
    }

    public async Task CreateBlobAsync(
        Guid projectId,
        Guid resourceId,
        string key,
        ClaimsPrincipal principal,
        Stream? stream,
        long length,
        bool trackChanges
    )
    {
        ForbiddenChars.CheckKey(key);

        _ = await _projectRepository.GetAsync(projectId, trackChanges: true)
            ?? throw new ProjectNotFoundException(projectId);

        var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges)
            ?? throw new ResourceNotFoundException(resourceId);

        // Archived restrictions when both resource is archived
        if (resourceEntity.Archived.Equals("1"))
        {
            throw new TreeBlobResourceArchivedBadRequestException(resourceEntity.Id);
        }

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.CreateBlob);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // A new blob of a resource cannot be created while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        // Check if file exists
        var entry = await dataStorage.ReadAsync(new ReadTreeParameters { Resource = resourceEntity, Path = key });

        if (entry != null)
        {
            // File already exists
            throw new BlobAlreadyExistsConflictException(key);
        }

        if (dataStorage.Capabilities.Contains(Capability.UsesQuota))
        {
            var storageInfo = await dataStorage.GetStorageInfoAsync(new StorageInfoParameters { Resource = resourceEntity, FetchAllocatedSize = true, FetchUsedSize = true });

            // Check if file size to be uploaded exceeds the free quota available for the resource
            var reservedForCurrent = new Quota
            {
                Value = storageInfo.AllocatedSize ?? 0,
                Unit = QuotaUnit.BYTE
            };

            var usedForCurrent = new Quota
            {
                Value = storageInfo.UsedSize ?? 0,
                Unit = QuotaUnit.BYTE
            };

            var remainingInByte = QuotaHelper.CalculateQuotaDifference(reservedForCurrent, usedForCurrent, QuotaUnit.BYTE);

            if (length > remainingInByte)
            {
                throw new QuotaLimitExceededBadRequestException();
            }
        }

        // Check if metadata for the blob was submitted
        var newestMetadata = await _treeRepository.GetNewestPagedMetadataAsync(resourceId, new() { Path = key });

        if (!newestMetadata.Any())
        {
            throw new MissingMetadataBadRequestException(resourceId, key);
        }

        await dataStorage.CreateAsync(new CreateBlobParameters { Resource = resourceEntity, Path = key, Blob = stream ?? new MemoryStream() });
    }

    public async Task UpdateBlobAsync(
        Guid projectId,
        Guid resourceId,
        string key,
        ClaimsPrincipal principal,
        Stream? stream,
        long length,
        bool trackChanges
    )
    {
        ForbiddenChars.CheckKey(key);        

        _ = await _projectRepository.GetAsync(projectId, trackChanges: true)
            ?? throw new ProjectNotFoundException(projectId);

        var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges)
            ?? throw new ResourceNotFoundException(resourceId);

        // Archived restrictions when both resource is archived
        if (resourceEntity.Archived.Equals("1"))
        {
            throw new TreeBlobResourceArchivedBadRequestException(resourceEntity.Id);
        }

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(principal, resourceEntity, ResourceOperations.UpdateBlob);

        // Check if metadata for the blob was submitted
        var newestMetadata = await _treeRepository.GetNewestPagedMetadataAsync(resourceEntity.Id, new() { Path = key });

        if (!newestMetadata.Any())
        {
            throw new MissingMetadataBadRequestException(resourceId, key);
        }

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        // Check if resource is in maintenance mode and enforce restrictions
        if (dataStorage.Capabilities.Contains(Capability.SupportsMaintenanceMode) && resourceEntity.MaintenanceMode)
        {
            // Blobs of a resource cannot be updated while the resource is in maintenance mode, regardless of user authorization
            throw new ResourceInMaintenanceModeForbiddenException(resourceEntity.Id);
        }

        // Check if file exists
        var storageItemInfo = await dataStorage.ReadAsync(new ReadTreeParameters { Resource = resourceEntity, Path = key }) ?? throw new BlobNotFoundException(key);

        if (!storageItemInfo.IsBlob)
        {
            // Can't update a tree here
            throw new Exception();
        }

        if (dataStorage.Capabilities.Contains(Capability.UsesQuota))
        {
            var storageInfo = await dataStorage.GetStorageInfoAsync(new StorageInfoParameters { Resource = resourceEntity, FetchAllocatedSize = true, FetchUsedSize = true });

            // Check if file size to be uploaded exceeds the free quota available for the resource
            var reservedForCurrent = new Quota
            {
                Value = storageInfo.AllocatedSize ?? 0,
                Unit = QuotaUnit.BYTE
            };

            var usedForCurrent = new Quota
            {
                Value = storageInfo.UsedSize ?? 0,
                Unit = QuotaUnit.BYTE
            };

            var remainingInByte = QuotaHelper.CalculateQuotaDifference(reservedForCurrent, usedForCurrent, QuotaUnit.BYTE);

            // The size of the file we are replacing has to be taken into account,
            // as we can also reuse its space.
            if (length > remainingInByte + (storageItemInfo.ContentLength ?? 0))
            {
                throw new QuotaLimitExceededBadRequestException();
            }
        }

        await dataStorage.UpdateAsync(new UpdateBlobParameters { Resource = resourceEntity, Path = key, Blob = stream ?? new MemoryStream() });
    }
}
