﻿using System.Text.RegularExpressions;

namespace Coscine.Api.Core.Service.Helpers;

/// <summary>A custom comparer for comparing string containing numbers.<br />Accounts for numeric values (int) in the string.<br />Useful for file names.<br /></summary>
internal partial class NumericStringComparer : IComparer<string>
{
    /// <summary>Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.</summary>
    /// <param name="x">The first object to compare.</param>
    /// <param name="y">The second object to compare.</param>
    /// <returns>
    /// A signed integer that indicates the relative values of <paramref name="x" /> and <paramref name="y" />, as shown in the following table.
    /// <list type="table"><listheader><term> Value</term><term> Meaning</term></listheader><item><description> Less than zero</description><description><paramref name="x" /> is less than <paramref name="y" />.</description></item><item><description> Zero</description><description><paramref name="x" /> equals <paramref name="y" />.</description></item><item><description> Greater than zero</description><description><paramref name="x" /> is greater than <paramref name="y" />.</description></item></list></returns>
    public int Compare(string? x, string? y)
    {
        // Fallback for null
        if (x is null || y is null)
        {
            return string.Compare(x, y);
        }

        var regex = NumberSegmentsRegex();
        var xMatches = regex.Matches(x);
        var yMatches = regex.Matches(y);

        for (int i = 0; i < Math.Min(xMatches.Count, yMatches.Count); i++)
        {
            if (xMatches[i].Success && yMatches[i].Success)
            {
                var xIsNumeric = int.TryParse(xMatches[i].Value, out var xInt);
                var yIsNumeric = int.TryParse(yMatches[i].Value, out var yInt);

                if (xIsNumeric && yIsNumeric)
                {
                    int numComparison = xInt.CompareTo(yInt);
                    if (numComparison != 0)
                    {
                        return numComparison;
                    }
                }
                else
                {
                    var textComparison = string.Compare(xMatches[i].Value, yMatches[i].Value, StringComparison.OrdinalIgnoreCase);
                    if (textComparison != 0)
                    {
                        return textComparison;
                    }
                }
            }
        }

        // Fallback to default string comparison
        return string.Compare(x, y);
    }

    [GeneratedRegex("(\\d+)|(\\D+)")]
    private static partial Regex NumberSegmentsRegex();
}