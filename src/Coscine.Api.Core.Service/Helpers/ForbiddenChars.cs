﻿using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using System.Text.RegularExpressions;

namespace Coscine.Api.Core.Service.Helpers;

public partial class ForbiddenChars
{
    /// <summary>
    /// Retrieves a regular expression that matches forbidden characters in the blob path key.
    /// </summary>
    /// <returns>A <see cref="Regex"/> object that can be used to match forbidden characters in a key.</returns>
    /// <remarks>
    /// The generated regex pattern is used to identify forbidden characters, such as backslash, colon, question mark, asterisk,
    /// less than, greater than, and pipe.
    /// </remarks>
    [GeneratedRegex(@"[\\:?*<>|+#]+")]
    private static partial Regex ForbiddenCharacters();

    /// <summary>
    /// Validates a blob path key for non-null or non-whitespace values and for forbidden characters.
    /// </summary>
    /// <param name="key">The blob path key to check.</param>
    /// <exception cref="EmptyKeyBadRequest">Thrown when the key is <c>null</c>, empty, or only consists of whitespace characters.</exception>
    /// <exception cref="ForbiddenCharInKeyBadRequest">Thrown when the key contains forbidden characters.</exception>
    public static void CheckKey(string key)
    {
        if (string.IsNullOrWhiteSpace(key))
        {
            throw new EmptyKeyBadRequest(key);
        }

        var rgx = ForbiddenCharacters();
        if (rgx.IsMatch(key))
        {
            throw new ForbiddenCharInKeyBadRequest(key);
        }
    }
}
