﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Service.Helpers;

/// <summary>
/// Provides helper methods for converting capacity units in resource quotas.
/// </summary>
public static class QuotaHelper
{
    /// <summary>
    /// Corresponds to the binary base used in computer systems for converting units of digital storage
    /// </summary>
    private const int _binaryBase = 1024;

    /// <summary>
    /// Converts the capacity units of a <see cref="QuotaDto"/> to the specified output unit.
    /// </summary>
    /// <param name="input">The input <see cref="QuotaDto"/> to convert.</param>
    /// <param name="outputUnit">The desired output unit.</param>
    /// <returns>The converted value in the specified output unit.</returns>
    public static float ConvertCapacityUnits(QuotaDto input, QuotaUnit outputUnit) => (float)ConvertCapacityUnitsHelper(input.Value, input.Unit, outputUnit);

    /// <summary>
    /// Converts the capacity units of a <see cref="QuotaForManipulationDto"/> to the specified output unit.
    /// </summary>
    /// <param name="input">The input <see cref="QuotaForManipulationDto"/> to convert.</param>
    /// <param name="outputUnit">The desired output unit.</param>
    /// <returns>The converted value in the specified output unit.</returns>
    public static int ConvertCapacityUnits(QuotaForManipulationDto input, QuotaUnit outputUnit) => (int)ConvertCapacityUnitsHelper(input.Value, input.Unit, outputUnit);

    /// <summary>
    /// Converts the capacity units of a <see cref="Quota"/> to the specified output unit.
    /// </summary>
    /// <param name="input">The input <see cref="Quota"/> to convert.</param>
    /// <param name="outputUnit">The desired output unit.</param>
    /// <returns>The converted value in the specified output unit.</returns>
    public static float ConvertCapacityUnits(Quota input, QuotaUnit outputUnit) => (float)ConvertCapacityUnitsHelper(input.Value, input.Unit, outputUnit);

    /// <summary>
    /// Calculates the difference between two quotas and returns the result in the specified output unit.
    /// </summary>
    /// <param name="left">The left-hand <see cref="Quota"/> to subtract from.</param>
    /// <param name="right">The right-hand <see cref="Quota"/> to subtract.</param>
    /// <param name="outputUnit">The desired output unit for the result.</param>
    /// <returns>The difference between the two quotas in the specified output unit.</returns>
    public static double CalculateQuotaDifference(Quota left, Quota right, QuotaUnit outputUnit)
    {
        // First, convert both QuotaDto values to the desired output unit.
        var leftValueInOutputUnit = ConvertCapacityUnitsHelper(left.Value, left.Unit, outputUnit);
        var rightValueInOutputUnit = ConvertCapacityUnitsHelper(right.Value, right.Unit, outputUnit);

        // Calculate the difference in the specified output unit.
        var difference = leftValueInOutputUnit - rightValueInOutputUnit;

        return difference;
    }

    private static double ConvertCapacityUnitsHelper(double inputValue, QuotaUnit inputUnit, QuotaUnit outputUnit)
    {
        var sizes = new List<QuotaUnit>() {
          QuotaUnit.BYTE,
          QuotaUnit.KibiBYTE,
          QuotaUnit.MebiBYTE,
          QuotaUnit.GibiBYTE,
          QuotaUnit.TebiBYTE,
          QuotaUnit.PebiBYTE,
        };

        var inputExponent = sizes.FindIndex((v) => v == inputUnit);
        var outputExponent = sizes.FindIndex((v) => v == outputUnit);

        return inputValue * Math.Pow(_binaryBase, inputExponent - outputExponent);
    }
}