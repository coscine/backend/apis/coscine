﻿// Ignore Spelling: fallback

using AutoMapper;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service;

public class VocabularyService : IVocabularyService
{
    private readonly IVocabularyRepository _vocabularyRepository;
    private readonly IMapper _mapper;

    public VocabularyService(IVocabularyRepository vocabularyRepository, IMapper mapper)
    {
        _vocabularyRepository = vocabularyRepository;
        _mapper = mapper;
    }

    public async Task<PagedEnumerable<VocabularyDto>> GetTopLevelVocabularies(VocabularyParameters vocabularyParameters)
    {
        var vocabularyEntities = await _vocabularyRepository.GetPagedTopLevelVocabulariesAsync(vocabularyParameters);
        var vocabularyDtos = _mapper.Map<IEnumerable<VocabularyDto>>(vocabularyEntities);
        return new PagedEnumerable<VocabularyDto>(vocabularyDtos, vocabularyEntities.Pagination);
    }

    public async Task<PagedEnumerable<VocabularyInstanceDto>> GetVocabularyInstancesAsync(VocabularyInstancesParameters instanceParameters)
    {
        // Get the top-level vocabulary containing the class that is being searched for
        var vocabularyEntity = await _vocabularyRepository.GetAsync(instanceParameters)
            ?? throw new TopLevelVocabularyForClassNotFoundException(instanceParameters.Class);

        // Look inside the top-level vocabulary for instances of that class
        var vocabularyInstanceEntities = await _vocabularyRepository.GetPagedVocabularyInstancesAsync(vocabularyEntity.GraphUri, instanceParameters);

        var vocabularyInstanceDtos = _mapper.Map<IEnumerable<VocabularyInstanceDto>>(vocabularyInstanceEntities);
        var vocabularyInstancePagedDto = new PagedEnumerable<VocabularyInstanceDto>(vocabularyInstanceDtos, vocabularyInstanceEntities.Pagination);
        return vocabularyInstancePagedDto;
    }

    public async Task<VocabularyInstanceDto> GetVocabularyInstance(Uri instanceUri, AcceptedLanguage lang, AcceptedLanguage fallbackLanguage)
    {
        // Get the top-level vocabulary containing the instance that is being searched for
        var vocabularyEntity = await _vocabularyRepository.GetTopLevelAsync(instanceUri, lang, fallbackLanguage)
            ?? throw new TopLevelVocabularyForInstanceNotFoundException(instanceUri);

        // Look inside the top-level vocabulary for instance
        var vocabularyInstanceEntity = await _vocabularyRepository.GetAsync(vocabularyEntity.GraphUri, vocabularyEntity.ClassUri, instanceUri, lang, fallbackLanguage);

        return _mapper.Map<VocabularyInstanceDto>(vocabularyInstanceEntity);
    }
}