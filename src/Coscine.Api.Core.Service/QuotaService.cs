﻿using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.InternalServerError;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Extensions;
using Coscine.Api.Core.Service.Helpers;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Security.Claims;

namespace Coscine.Api.Core.Service;

public class QuotaService(
    IRepositoryContextLoader repositoryContextLoader,
    IProjectRepository projectRepository,
    IProjectQuotaRepository projectQuotaRepository,
    IResourceTypeRepository resourceTypeRepository,
    IDefaultProjectQuotaRepository defaultProjectQuotaRepository,
    IResourceRepository resourceRepository,
    IAuthorizationService authorizationService,
    IAuthenticatorService authenticatorService,
    ILogger<QuotaService> logger,
    IDataStorageRepositoryFactory dataStorageRepositoryFactory,
    IHttpContextAccessor context) : IQuotaService
{
    private readonly IRepositoryContextLoader _repositoryContextLoader = repositoryContextLoader;
    private readonly IProjectRepository _projectRepository = projectRepository;
    private readonly IProjectQuotaRepository _projectQuotaRepository = projectQuotaRepository;
    private readonly IResourceTypeRepository _resourceTypeRepository = resourceTypeRepository;
    private readonly IDefaultProjectQuotaRepository _defaultProjectQuotaRepository = defaultProjectQuotaRepository;
    private readonly IResourceRepository _resourceRepository = resourceRepository;
    private readonly IAuthorizationService _authorizationService = authorizationService;
    private readonly IAuthenticatorService _authenticatorService = authenticatorService;
    private readonly ILogger<QuotaService> _logger = logger;
    private readonly IDataStorageRepositoryFactory _dataStorageRepositoryFactory = dataStorageRepositoryFactory;

    private const float _oneGiB = 1024 * 1024 * 1024; // 1 GiB = 1024³ Bytes


    public async Task<PagedEnumerable<ProjectQuotaDto>> GetPagedQuotasForProject(Guid projectId, ProjectQuotaParameters projectQuotaParameters, bool trackChanges)
    {
        // Using the GetProjectByIdAsync method, so that a quota admin can also read the project.
        var projectEntity = await _projectRepository.GetAsync(projectId, trackChanges)
            ?? throw new ProjectNotFoundException(projectId);

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(context.HttpContext.User, projectEntity, ProjectOperations.Read);

        // Get all resource types for the project first, because project quotas may be missing for some resource types, if never set
        var resourceTypeEntities = await _resourceTypeRepository.GetAllAsync(trackChanges: true);

        // Get all project quotas for the paged resource types
        var projectQuotaEntitiesForResourceTypes = await _projectQuotaRepository.GetAllAsync(projectId, resourceTypeEntities.Select(rt => rt.Id), projectQuotaParameters, trackChanges);

        var projectQuotaDtos = new List<ProjectQuotaDto>();

        foreach (var resourceType in resourceTypeEntities)
        {
            // Find the project quota entity for the current resource type
            var matchingQuotaEntity = projectQuotaEntitiesForResourceTypes.FirstOrDefault(pq => pq.ResourceTypeId == resourceType.Id);

            if (matchingQuotaEntity != null)
            {
                // Create the DTO for the project quota for the found entity
                projectQuotaDtos.Add(await CreateProjectQuotaDto(matchingQuotaEntity, trackChanges));
            }
            else
            {
                // Should the project quota entity not exist, create a new one with default values
                projectQuotaDtos.Add(await CreateProjectQuotaDto(new ProjectQuota
                {
                    Project = projectEntity,
                    ResourceType = resourceType,
                    ProjectId = projectEntity.Id,
                    ResourceTypeId = resourceType.Id,
                    Quota = 0,
                    MaxQuota = 0
                }, trackChanges));
            }
        }

        return PagedEnumerable<ProjectQuotaDto>.ToPagedEnumerable(projectQuotaDtos, projectQuotaParameters.PageNumber, projectQuotaParameters.PageSize);
    }

    public async Task<ProjectQuotaDto> GetQuotaForProject(Guid projectId, Guid resourceTypeId, bool trackChanges)
    {
        var projectEntity = await _projectRepository.GetAsync(projectId, trackChanges)
            ?? throw new ProjectNotFoundException(projectId);

        var resourceType = await _resourceTypeRepository.GetAsync(resourceTypeId, trackChanges) ?? throw new ResourceTypeNotFoundException(resourceTypeId);

        var projectQuotaEntity = await _projectQuotaRepository.GetAsync(projectId, resourceTypeId, trackChanges);

        projectQuotaEntity ??= new ProjectQuota
        {
            Project = projectEntity,
            ResourceType = resourceType,
            ProjectId = projectEntity.Id,
            ResourceTypeId = resourceType.Id,
            Quota = 0,
            MaxQuota = 0
        };

        if (projectQuotaEntity is null)
        {
            throw new ProjectQuotaNotFoundException(projectId, resourceTypeId);
        }

        await _authorizationService.AuthorizeAndThrowIfUnauthorized(context.HttpContext.User, projectQuotaEntity, ProjectQuotaOperations.Read);

        return await CreateProjectQuotaDto(projectQuotaEntity, trackChanges);
    }

    public async Task UpdateProjectQuota(Guid projectId, Guid resourceTypeId, ProjectQuotaForUpdateDto projectQuotaForUpdate, bool trackChanges)
    {
        // Fetch necessary data from repositories
        var user = await _authenticatorService.GetUserAsync(context.HttpContext.User, trackChanges: false) ?? throw new UserNotFoundException();

        // Using the GetProjectByIdAsync method, so that a quota admin can also read the project.
        var projectEntity = await _projectRepository.GetAsync(projectId, trackChanges)
            ?? throw new ProjectNotFoundException(projectId);

        var resourceTypeEntity = await _resourceTypeRepository.GetAsync(resourceTypeId, trackChanges)
            ?? throw new ResourceTypeNotFoundException(resourceTypeId);

        var projectQuotaEntity = await _projectQuotaRepository.GetAsync(projectId, resourceTypeEntity.Id, trackChanges);

        var dataStorage = _dataStorageRepositoryFactory.Create(resourceTypeEntity.Id)
            ?? throw new NoValidProviderBadRequest(resourceTypeEntity.Id, null);

        // Check if quota adjustment is allowed
        if (!dataStorage.Capabilities.Contains(Capability.UsesQuota))
        {
            throw new QuotaNotAdjustableBadRequestException(resourceTypeEntity.Id);
        }

        // Fetch default quota if the project quota does not exist
        int defaultMaxQuota = 0;
        if (projectQuotaEntity is null)
        {
            var defaultProjectQuotas = await _defaultProjectQuotaRepository.GetAllAsync(user.Id);
            var defaultQuota = defaultProjectQuotas.FirstOrDefault(dpq => dpq?.ResourceType == resourceTypeEntity.Type);
            defaultMaxQuota = defaultQuota?.DefaultMaxQuota ?? 0;
        }

        // Convert quota values
        var totalReservedInGiB = QuotaHelper.ConvertCapacityUnits(await CalculateTotalReservedQuota(resourceTypeEntity.Id, projectId), QuotaUnit.GibiBYTE);
        var allocatedInGiB = QuotaHelper.ConvertCapacityUnits(projectQuotaForUpdate.Allocated, QuotaUnit.GibiBYTE);
        var maximumInGiB = projectQuotaForUpdate.Maximum is not null
            ? QuotaHelper.ConvertCapacityUnits(projectQuotaForUpdate.Maximum, QuotaUnit.GibiBYTE)
            : projectQuotaEntity?.MaxQuota ?? defaultMaxQuota;

        // Validate quotas
        if (totalReservedInGiB > allocatedInGiB)
        {
            throw new AllocatedQuotaTooLowBadRequestException(allocatedInGiB, totalReservedInGiB);
        }
        if (allocatedInGiB > maximumInGiB)
        {
            throw new AllocatedQuotaTooHighBadRequestException(allocatedInGiB, maximumInGiB, resourceTypeEntity.SpecificType ?? "");
        }
        if (maximumInGiB < allocatedInGiB || maximumInGiB < totalReservedInGiB)
        {
            var maxThreshold = Math.Max(allocatedInGiB, totalReservedInGiB);
            throw new MaximumQuotaTooLowBadRequestException(maxThreshold);
        }

        // Update or create new project quota
        var projectQuotaToAddOrUpdate = projectQuotaEntity ?? new ProjectQuota { ProjectId = projectEntity.Id, ResourceTypeId = resourceTypeEntity.Id };
        projectQuotaToAddOrUpdate.Quota = allocatedInGiB;
        if (projectQuotaForUpdate.Maximum is not null)
        {
            projectQuotaToAddOrUpdate.MaxQuota = maximumInGiB;
        }
        else if (projectQuotaEntity is null)
        {
            projectQuotaToAddOrUpdate.MaxQuota = defaultMaxQuota;
        }

        // Check if the user is allowed to update the project quota here, because the project quota entity may not exist
        await _authorizationService.AuthorizeAndThrowIfUnauthorized(context.HttpContext.User, projectQuotaToAddOrUpdate, ProjectQuotaOperations.Update);
        if (projectQuotaForUpdate.Maximum is not null)
        {
            // Additional authorization check for quota admins
            await _authorizationService.AuthorizeAndThrowIfUnauthorized(context.HttpContext.User, projectQuotaToAddOrUpdate, ProjectQuotaOperations.UpdateMaximum);
        }

        if (projectQuotaEntity is null)
        {
            projectEntity.ProjectQuota.Add(projectQuotaToAddOrUpdate);
        }

        await _repositoryContextLoader.SaveAsync();
    }

    private async Task<ProjectQuotaDto> CreateProjectQuotaDto(ProjectQuota projectQuotaEntity, bool trackChanges)
    {
        var projectId = projectQuotaEntity.ProjectId;
        var resourceTypeId = projectQuotaEntity.ResourceTypeId;

        var resourceQuotas = await GetQuotaForAllResourcesByTypeForProjectAsync(projectId, resourceTypeId, trackChanges);
        var totalUsedInGiB = resourceQuotas.Sum(rq => rq.Used is not null ? QuotaHelper.ConvertCapacityUnits(rq.Used, QuotaUnit.GibiBYTE) : 0);

        return new ProjectQuotaDto
        {
            ProjectId = projectId,
            ResourceType = new()
            {
                Id = resourceTypeId,
                SpecificType = projectQuotaEntity.ResourceType.SpecificType ?? ""
            },
            TotalUsed = new()
            {
                Value = totalUsedInGiB,
                Unit = QuotaUnit.GibiBYTE
            },
            TotalReserved = await CalculateTotalReservedQuota(resourceTypeId, projectId),
            Allocated = new()
            {
                Value = projectQuotaEntity?.Quota ?? 0,
                Unit = QuotaUnit.GibiBYTE
            },
            Maximum = new()
            {
                Value = projectQuotaEntity?.MaxQuota ?? 0,
                Unit = QuotaUnit.GibiBYTE
            },
            ResourceQuotas = resourceQuotas,
        };
    }

    private async Task<QuotaDto> CalculateTotalReservedQuota(Guid resourceTypeId, Guid projectId)
    {
        var resourceEntities = await _resourceRepository.GetAllAsync(projectId, resourceTypeId, trackChanges: false);
        long sum = 0L;

        foreach (var resourceEntity in resourceEntities)
        {
            var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

            if (dataStorage.Capabilities.Contains(Capability.UsesQuota))
            {
                // This try-catch exists only for handling ECS Simulator malfunction cases
                try
                {
                    sum += (await dataStorage.GetStorageInfoAsync(new StorageInfoParameters { Resource = resourceEntity, FetchAllocatedSize = true })).AllocatedSize ?? 0;
                }
                // If an exception occurs, just add nothing to the sum
                catch (Exception e)
                {
                    _logger.LogWarning(e, "Error while getting available resource quota.");
                }
            }
        }

        return new QuotaDto
        {
            Value = QuotaHelper.ConvertCapacityUnits(new QuotaDto { Value = sum, Unit = QuotaUnit.BYTE }, QuotaUnit.GibiBYTE),
            Unit = QuotaUnit.GibiBYTE
        };
    }

    /// <summary>
    /// Validates the resource quota for a specific resource type and project on creating.
    /// </summary>
    /// <param name="projectId">The ID of the project.</param>
    /// <param name="resourceType">The resource type.</param>
    /// <param name="resourceTypeDefinition">The definition of the resource type.</param>
    /// <param name="desiredResourceQuota">The desired reserved quota for the resource.</param>
    /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
    /// <exception cref="QuotaNullForQuotaRequiredResourceException">Thrown when the desired resource quota is <c>null</c> for a resource type that requires a quota.</exception>
    /// <exception cref="NegativeQuotaInternalServerErrorException">Thrown when the free quota for the resource type is negative.</exception>
    /// <exception cref="AllocatedQuotaTooHighBadRequestException">Thrown when the desired quota is higher than the available free quota.</exception>
    public async Task ValidateResourceQuotaOnCreationAsync(Guid projectId, Guid resourceTypeId, QuotaForManipulationDto? desiredResourceQuota)
    {
        if (desiredResourceQuota is null)
        {
            throw new QuotaNullForQuotaRequiredResourceException(resourceTypeId);
        }

        var resourceTypeEntity = await _resourceTypeRepository.GetAsync(resourceTypeId, trackChanges: false)
            ?? throw new ResourceTypeNotFoundException(resourceTypeId);

        var projectQuota = await GetQuotaForProject(projectId, resourceTypeId, trackChanges: false);

        // Target quota numbers
        var desiredQuotaInGiB = QuotaHelper.ConvertCapacityUnits(desiredResourceQuota, QuotaUnit.GibiBYTE);

        // Resource quota numbers
        var maxQuotaInGiB = QuotaHelper.ConvertCapacityUnits(projectQuota.Maximum, QuotaUnit.GibiBYTE);
        var totalReservedQuotaInGiB = QuotaHelper.ConvertCapacityUnits(projectQuota.TotalReserved, QuotaUnit.GibiBYTE);

        // Free quota numbers
        var freeQuotaInGiB = maxQuotaInGiB - totalReservedQuotaInGiB;

        // Validations
        if (freeQuotaInGiB < 0)
        {
            _logger.LogError(@"The free quota for resource type ""{resourceTypeId}"" in project ""{projectId}"" cannot be negative. Maximum Quota: {maxQuotaInGiB} GiB. Total Reserved Quota: {totalReservedQuotaInGiB} GiB.", resourceTypeId, projectId, maxQuotaInGiB, totalReservedQuotaInGiB);
            throw new NegativeQuotaInternalServerErrorException(projectId, resourceTypeId);
        }

        if (desiredQuotaInGiB > freeQuotaInGiB)
        {
            throw new AllocatedQuotaTooHighBadRequestException($"You cannot create a resource with a quota value of {desiredQuotaInGiB} GiB. You have already reserved {QuotaHelper.ConvertCapacityUnits(projectQuota.TotalReserved, QuotaUnit.GibiBYTE)} GiB for {resourceTypeEntity.DisplayName} resources and have only {freeQuotaInGiB} GiB free.");
        }
    }

    /// <summary>
    /// Validates the resource quota for a specific resource type and project on updating.
    /// </summary>
    /// <param name="projectId">The ID of the project.</param>
    /// <param name="resourceId">The ID of the resource.</param>
    /// <param name="resourceType">The resource type.</param>
    /// <param name="resourceTypeDefinition">The definition of the resource type.</param>
    /// <param name="desiredResourceQuota">The desired reserved quota for the resource.</param>
    /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
    /// <exception cref="NegativeQuotaInternalServerErrorException">Thrown when the free quota for the resource type is negative.</exception>
    /// <exception cref="QuotaNullForQuotaRequiredResourceException">Thrown when the desired resource quota is <c>null</c> for a resource type that requires a quota.</exception>
    /// <exception cref="AllocatedQuotaTooHighBadRequestException">Thrown when the desired quota is higher than the available free quota.</exception>
    /// <exception cref="AllocatedQuotaTooLowBadRequestException">Thrown when the desired quota is lower than the used quota.</exception>
    public async Task ValidateResourceQuotaOnUpdateAsync(Guid projectId, Guid resourceId, QuotaForManipulationDto? desiredResourceQuota)
    {
        var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges: false)
            ?? throw new ResourceNotFoundException(resourceId);

        if (desiredResourceQuota is null)
        {
            throw new QuotaNullForQuotaRequiredResourceException(resourceEntity.TypeId);
        }

        var projectQuota = await GetQuotaForProject(projectId, resourceEntity.TypeId, trackChanges: false);

        // Target quota numbers
        var desiredQuotaInGiB = QuotaHelper.ConvertCapacityUnits(desiredResourceQuota, QuotaUnit.GibiBYTE);

        // Resource quota numbers
        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        var storageInfo = await dataStorage.GetStorageInfoAsync(new StorageInfoParameters { Resource = resourceEntity, FetchAllocatedSize = true, FetchUsedSize = true });

        var reservedForCurrent = new QuotaDto { Value = storageInfo.AllocatedSize ?? 0, Unit = QuotaUnit.BYTE };
        var usedForCurrent = new QuotaDto { Value = storageInfo.UsedSize ?? 0, Unit = QuotaUnit.BYTE };

        // Transform to GiB
        var reservedForCurrentInGiB = QuotaHelper.ConvertCapacityUnits(reservedForCurrent, QuotaUnit.GibiBYTE);
        var usedForCurrentInGiB = QuotaHelper.ConvertCapacityUnits(usedForCurrent, QuotaUnit.GibiBYTE);

        // Project quota numbers
        var maxQuotaInGiB = QuotaHelper.ConvertCapacityUnits(projectQuota.Maximum, QuotaUnit.GibiBYTE);
        var totalReservedQuotaInGiB = QuotaHelper.ConvertCapacityUnits(projectQuota.TotalReserved, QuotaUnit.GibiBYTE);
        var totalReservedByOtherInGiB = totalReservedQuotaInGiB - reservedForCurrentInGiB;

        // Free quota numbers
        var freeQuotaInGiB = maxQuotaInGiB - totalReservedByOtherInGiB;

        // Validations
        if (freeQuotaInGiB < 0)
        {
            _logger.LogError(@"The free quota for resource type ""{resourceTypeId}"" in project ""{projectId}"" cannot be negative. Maximum Quota: {maxQuotaInGiB} GiB. Total Reserved Quota: {totalReservedQuotaInGiB} GiB.", resourceEntity.TypeId, projectId, maxQuotaInGiB, totalReservedQuotaInGiB);
            throw new NegativeQuotaInternalServerErrorException(projectId, resourceEntity.TypeId);
        }
        if (desiredQuotaInGiB > freeQuotaInGiB)
        {
            throw new AllocatedQuotaTooHighBadRequestException($"Your requested quota of {desiredQuotaInGiB} GiB is too high. You only have {freeQuotaInGiB} GiB available for {resourceEntity.Type.DisplayName} resource types. Please reduce your quota");
        }
        if (desiredQuotaInGiB < usedForCurrentInGiB)
        {
            throw new AllocatedQuotaTooLowBadRequestException($"You are trying to set a quota of {desiredQuotaInGiB} GiB, but you're already using {usedForCurrentInGiB} GiB. Please increase your quota.");
        }
    }

    /// <summary>
    /// Retrieves the resource quota for a specific resource in a project.
    /// </summary>
    /// <param name="projectId">The ID of the project.</param>
    /// <param name="resourceId">The ID of the resource.</param>
    /// <param name="trackChanges">A flag indicating whether to track changes in the repository.</param>
    /// <returns>The resource quota.</returns>
    /// <exception cref="ResourceNotFoundException">Thrown when the resource is not found.</exception>
    public async Task<ResourceQuotaDto> GetQuotaForResourceForProjectAsync(Guid projectId, Guid resourceId, bool trackChanges)
    {
        var resourceEntity = await _resourceRepository.GetAsync(projectId, resourceId, trackChanges) ?? throw new ResourceNotFoundException(resourceId);

        // Check role authorization
        await _authorizationService.AuthorizeAndThrowIfUnauthorized(context.HttpContext.User, resourceEntity, ResourceOperations.ReadQuota);

        return await GetQuotaDtoForResource(resourceEntity);
    }

    /// <summary>
    /// Retrieves the resource quotas for all resources in a project of a resource type.
    /// </summary>
    /// <param name="projectId">The ID of the project.</param>
    /// <param name="resourceTypeId">The ID of the resource type.</param>
    /// <param name="trackChanges">A flag indicating whether to track changes in the repository.</param>
    /// <returns>The resource quotas.</returns>
    public async Task<IEnumerable<ResourceQuotaDto>> GetQuotaForAllResourcesByTypeForProjectAsync(Guid projectId, Guid resourceTypeId, bool trackChanges)
    {
        var resourceEntities = await _resourceRepository.GetAllAsync(projectId, resourceTypeId, trackChanges);

        var resourceQuotasDto = new List<ResourceQuotaDto>();
        foreach (var resourceEntity in resourceEntities)
        {
            // Check role authorization
            await _authorizationService.AuthorizeAndThrowIfUnauthorized(context.HttpContext.User, resourceEntity, ResourceOperations.ReadQuota);

            var resourceQuotaDto = await GetQuotaDtoForResource(resourceEntity);
            if (resourceQuotaDto is not null)
            {
                resourceQuotasDto.Add(resourceQuotaDto);
            }
        }
        return resourceQuotasDto;
    }

    /// <summary>
    /// Retrieves the desired resource quota based on the resource type and options during resource creation operations.
    /// </summary>
    /// <param name="resourceTypeEntity">The resource type entity.</param>
    /// <param name="resourceTypeOptions">The options for the resource type (optional).</param>
    /// <returns>The desired resource quota, or <c>null</c> if not applicable for the resource type.</returns>
    /// <exception cref="QuotaNullForQuotaRequiredResourceException">Thrown when the required quota for a resource type is not provided.</exception>
    /// <exception cref="InvalidResourceTypeOptionsBadRequestException">Thrown when the resource type is invalid.</exception>
    public QuotaForManipulationDto? ExtractDesiredResourceQuotaForResource(ResourceType resourceTypeEntity, ResourceTypeOptionsForCreationDto? resourceTypeOptions)
    {
        var generalType = resourceTypeEntity.Type;
        return generalType?.ToLower() switch
        {
            null => null,
            "gitlab" => null,
            "linked" => null,
            "rds" => resourceTypeOptions?.RdsResourceTypeOptions?.Quota ?? throw new QuotaNullForQuotaRequiredResourceException(resourceTypeEntity.Id),
            "rdss3" => resourceTypeOptions?.RdsS3ResourceTypeOptions?.Quota ?? throw new QuotaNullForQuotaRequiredResourceException(resourceTypeEntity.Id),
            "rdss3worm" => resourceTypeOptions?.RdsS3WormResourceTypeOptions?.Quota ?? throw new QuotaNullForQuotaRequiredResourceException(resourceTypeEntity.Id),
            _ => throw new InvalidResourceTypeOptionsBadRequestException($"Invalid resource type: '{generalType}'.")
        };
    }

    /// <summary>
    /// Retrieves the desired resource quota based on the resource type and options during resource update operations.
    /// </summary>
    /// <param name="resourceTypeEntity">The resource type entity.</param>
    /// <param name="resourceTypeOptions">The options for the resource type (optional).</param>
    /// <returns>The desired resource quota, or <c>null</c> if not applicable for the resource type.</returns>
    /// <exception cref="QuotaNullForQuotaRequiredResourceException">Thrown when the required quota for a resource type is not provided.</exception>
    /// <exception cref="InvalidResourceTypeOptionsBadRequestException">Thrown when the resource type is invalid.</exception>
    public QuotaForManipulationDto? ExtractDesiredResourceQuotaForResource(ResourceType resourceTypeEntity, ResourceTypeOptionsForUpdateDto? resourceTypeOptions)
    {
        var generalType = resourceTypeEntity.Type;
        return generalType?.ToLower() switch
        {
            null => null,
            "gitlab" => null,
            "linked" => null,
            "rds" => resourceTypeOptions?.RdsResourceTypeOptions?.Quota,
            "rdss3" => resourceTypeOptions?.RdsS3ResourceTypeOptions?.Quota,
            "rdss3worm" => resourceTypeOptions?.RdsS3WormResourceTypeOptions?.Quota,
            _ => throw new InvalidResourceTypeOptionsBadRequestException($"Invalid resource type: '{generalType}'.")
        };
    }

    private async Task<ResourceQuotaDto> GetQuotaDtoForResource(Resource resourceEntity)
    {
        var dataStorage = _dataStorageRepositoryFactory.Create(resourceEntity.TypeId)
            ?? throw new NoValidProviderBadRequest(resourceEntity.Id, resourceEntity.Type.DisplayName);

        var isQuotaSupportedByResourceType = dataStorage.Capabilities.Contains(Capability.UsesQuota);

        var storageInfo = await dataStorage.GetStorageInfoAsync(new StorageInfoParameters { Resource = resourceEntity, FetchAllocatedSize = true, FetchUsedSize = true });

        // TODO: Change the Id to a Guid and make use of QuotaDimUnits in the resource types library
        var reservedQuota = new QuotaDto
        {
            Value = isQuotaSupportedByResourceType ? storageInfo.AllocatedSize ?? 0 : 0,
            Unit = QuotaUnit.BYTE
        };

        var usedQuota = new QuotaDto
        {
            Value = isQuotaSupportedByResourceType ? storageInfo.UsedSize ?? 0 : 0,
            Unit = QuotaUnit.BYTE
        };

        // Not using the mapper; no pre-defined type to use as base
        return new ResourceQuotaDto
        {
            Resource = new ResourceMinimalDto { Id = resourceEntity.Id },
            Used = new QuotaDto
            {
                Value = QuotaHelper.ConvertCapacityUnits(usedQuota, QuotaUnit.GibiBYTE),
                Unit = QuotaUnit.GibiBYTE
            },
            Reserved = new QuotaDto
            {
                Value = QuotaHelper.ConvertCapacityUnits(reservedQuota, QuotaUnit.GibiBYTE),
                Unit = QuotaUnit.GibiBYTE
            },
            UsedPercentage = (float)reservedQuota.Value != 0 ? 100f * ((float)usedQuota.Value / ((float)reservedQuota.Value * _oneGiB)) : null // float division
        };
    }
}