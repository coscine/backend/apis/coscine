using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Coscine.Api.Core.Service;

/// <summary>Service for resolving the user principal to the user entity.</summary>
/// <remarks>Initializes a new instance of the <see cref="AuthenticatorService" /> class.</remarks>
/// <param name="apiTokenRepository">The token repository.</param>
/// <param name="userRepository">The user repository.</param>
/// <param name="logger">The logger.</param>
/// <param name="jwtConfiguration">The JWT configuration.</param>
/// <param name="cache">The cache.</param>
public class AuthenticatorService(
    IApiTokenRepository apiTokenRepository,
    IUserRepository userRepository,
    ILogger<AuthenticatorService> logger,
    IOptionsMonitor<JwtConfiguration> jwtConfiguration,
    IOptionsMonitor<AuthenticationConfiguration> authenticationConfiguration,
    IExternalIdRepository externalIdRepository,
    IHttpContextAccessor context) : IAuthenticatorService
{
    private readonly IApiTokenRepository _apiTokenRepository = apiTokenRepository;

    private readonly IUserRepository _userRepository = userRepository;

    private readonly ILogger<AuthenticatorService> _logger = logger;

    private readonly AuthenticationConfiguration _authenticationConfiguration = authenticationConfiguration.CurrentValue;

    private readonly IExternalIdRepository _externalIdRepository = externalIdRepository;

    private readonly JwtConfiguration _jwtConfiguration = jwtConfiguration.CurrentValue;

    /// <summary>Gets the user asynchronous.</summary>
    /// <param name="principal">The principal.</param>
    /// <param name="trackChanges">if set to <c>true</c> [track changes].</param>
    /// <param name="allowCached">Allow to load the user to load from the cache.</param>
    /// <returns>The User.</returns>
    public async Task<User?> GetUserAsync(ClaimsPrincipal principal, bool trackChanges, bool allowCached = true)
    {
        return await GetUserAsyncInternal(principal, trackChanges, allowCached);
    }

    /// <summary>Gets the user asynchronous.</summary>
    /// <param name="allowCached">Allow to load the user to load from the cache.</param>
    /// <returns>The User.</returns>
    public async Task<User?> GetUserAsync(bool trackChanges, bool allowCached = true)
    {
        return await GetUserAsyncInternal(context.HttpContext.User, trackChanges, allowCached);
    }

    /// <summary>Gets the user asynchronous internal.</summary>
    /// <param name="principal">The principal.</param>
    /// <param name="trackChanges">if set to <c>true</c> [track changes].</param>
    /// <param name="allowCached">Allow to load the user to load from the cache.</param>
    /// <returns>The User.</returns>
    /// <exception cref="UserAndTokenBadRequestException">If the claims principal contains an Id for both user and token.</exception>
    /// <exception cref="UserNotFoundException">If the token does not exists.</exception>
    /// <exception cref="ApiTokenNotFoundException">If the token does not exists.</exception>
    private async Task<User?> GetUserAsyncInternal(ClaimsPrincipal principal, bool trackChanges, bool allowCached = true)
    {
        var externalId = principal.FindFirstValue(ClaimTypes.NameIdentifier);
        var externalAuthenticatorId = principal.FindFirstValue("external_authenticator_id");
        string? userId = null;

        // OpenID Connect (OIDC) claims section
        if (externalId is not null && externalAuthenticatorId is not null)
        {
            // Make sure, that we support this provider
            if (!_authenticationConfiguration.OpenIdConnectProviders
                .Select(x => x.ExternalId)
                .Contains(externalAuthenticatorId))
            {
                throw new ExternalAuthenticatorIdBadRequestException();
            }

            // Make sure, that the it is a GUID
            if (!Guid.TryParse(externalAuthenticatorId, out var externalAuthenticatorGuid))
            {
                throw new ExternalAuthenticatorIdNotAGuidBadRequestException(externalAuthenticatorId);
            }

            var eId = await _externalIdRepository.GetAsync(externalAuthenticatorGuid, externalId, trackChanges);

            if (eId is not null)
            {
                userId = eId.UserId.ToString();
            }
        }

        // Shibboleth (SSO) claims section
        var tokenId = principal.FindFirstValue("tokenId");
        userId ??= principal.FindFirstValue("userId");
        if (userId is not null)
        {
            // Only one can be set
            if (tokenId is not null)
            {
                throw new UserAndTokenBadRequestException();
            }

            return await LoadUserFromUserId(userId, trackChanges, allowCached);
        }
        else if (tokenId is not null)
        {
            var tokenGuid = new Guid(tokenId);

            var apiToken = await _apiTokenRepository.GetAsync(tokenGuid, trackChanges) ?? throw new ApiTokenNotFoundException(tokenGuid);
            return await LoadUserFromUserId(apiToken.UserId.ToString(), trackChanges, allowCached);
        }

        return null;
    }

    /// <summary>Loads the user from user identifier.</summary>
    /// <param name="userId">The user identifier.</param>
    /// <param name="trackChanges">if set to <c>true</c> [track changes].</param>
    /// <returns>The user.</returns>
    private async Task<User?> LoadUserFromUserId(string userId, bool trackChanges, bool allowCached = true)
    {
        var userGuid = new Guid(userId);
        var user = await _userRepository.GetAsync(userGuid, trackChanges);
        return user;
    }

    /// <summary>Generates the JWT security token.</summary>
    /// <param name="payloadContents">The payload contents.</param>
    /// <param name="expiresInMinutes">The expires in minutes.</param>
    /// <param name="jsonWebKey">The json web key.</param>
    /// <returns>The generated JWT security token.</returns>
    public JwtSecurityToken GenerateJwtSecurityToken(IReadOnlyDictionary<string, string> payloadContents, double expiresInMinutes, JsonWebKey jsonWebKey)
    {
        var now = DateTime.UtcNow;

        // Create claims for the JWT token
        var claims = payloadContents.Select(c => new Claim(c.Key, c.Value)).ToList();
        claims.Add(new Claim("iat", EpochTime.GetIntDate(now).ToString(), ClaimValueTypes.Integer64));

        // Create the security key from the secret key
        var securityKey = new SymmetricSecurityKey(Base64UrlEncoder.DecodeBytes(jsonWebKey.K));

        // Create signing credentials using the security key
        var signingCredentials = new SigningCredentials(securityKey, jsonWebKey.Alg);

        // Create the JWT token
        return new JwtSecurityToken(
            issuer: _jwtConfiguration.ValidIssuers[0],
            audience: _jwtConfiguration.ValidAudiences[0],
            claims: claims,
            notBefore: now,
            expires: now.AddMinutes(expiresInMinutes), // Token expiration time
            signingCredentials: signingCredentials
        );
    }

    /// <summary>Generates the JWT security token.</summary>
    /// <param name="payloadContents">The payload contents.</param>
    /// <param name="jsonWebKey">The json web key.</param>
    /// <returns>
    /// <returns>The generated JWT security token.</returns>
    /// </returns>
    public JwtSecurityToken GenerateJwtSecurityToken(IReadOnlyDictionary<string, string> payloadContents, JsonWebKey jsonWebKey)
    {
        return GenerateJwtSecurityToken(payloadContents, _jwtConfiguration.ExpiresInMinutes, jsonWebKey);
    }



    /// <summary>Generates the JWT security token.</summary>
    /// <param name="payloadContents">The payload contents.</param>
    /// <param name="expiresInMinutes">The expires in minutes.</param>
    /// <returns>The generated JWT security token.</returns>
    public JwtSecurityToken GenerateJwtSecurityToken(IReadOnlyDictionary<string, string> payloadContents, double? expiresInMinutes = null)
    {
        return GenerateJwtSecurityToken(payloadContents, expiresInMinutes ?? _jwtConfiguration.ExpiresInMinutes,
            _jwtConfiguration.JsonWebKeys.First(x => x.KeySize >= 256));
    }

    /// <summary>Gets the JWT string from token.</summary>
    /// <param name="token">The token.</param>
    /// <returns>The JWT as a string.</returns>
    public string GetJwtStringFromToken(JwtSecurityToken token)
    {
        // Generate the encoded JWT token
        var tokenHandler = new JwtSecurityTokenHandler();

        return tokenHandler.WriteToken(token);
    }
}