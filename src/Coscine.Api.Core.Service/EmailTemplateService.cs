﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;

namespace Coscine.Api.Core.Service;

public class EmailTemplateManager : IEmailTemplateManager
{
    private readonly Lazy<Dictionary<string, EmailTemplate>> _apCreationRequestTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _apCreationRequestServiceDeskTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _pidContactConfirmationTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _pidContactProjectTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _pidContactResourceTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _projectCreatedTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _projectResponsibleOrganizationChangeTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _projectDeletedTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _publicationRequestOwnersTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _publicationRequestPublicationAdvisoryServiceTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _publicationRequestResourcePartTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _inviteUserToProjectTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _resourceCreatedTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _resourceMaintenanceModeEnabledTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _resourceMaintenanceModeDisabledTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _resourceDeletedTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _userRoleChangedTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _userDeletedTemplates;
    private readonly Lazy<Dictionary<string, EmailTemplate>> _userEmailChangedTemplates;

    private readonly IEmailTemplateRepository _emailTemplateRepository;

    public EmailTemplateManager(IEmailTemplateRepository emailTemplateRepository)
    {
        _emailTemplateRepository = emailTemplateRepository;

        _apCreationRequestTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "request_applicationprofile.json") },
            {"de", _emailTemplateRepository.Get("de", "request_applicationprofile.json") }
        });

        _apCreationRequestServiceDeskTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "request_applicationprofile_servicedesk_notification.json") },
            {"de", _emailTemplateRepository.Get("de", "request_applicationprofile_servicedesk_notification.json") }
        });

        _pidContactConfirmationTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "pid_contact_confirmation.json") },
            {"de", _emailTemplateRepository.Get("de", "pid_contact_confirmation.json") }
        });

        _pidContactProjectTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "pid_contact_project.json") },
            {"de", _emailTemplateRepository.Get("de", "pid_contact_project.json") }
        });

        _pidContactResourceTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "pid_contact_resource.json") },
            {"de", _emailTemplateRepository.Get("de", "pid_contact_resource.json") }
        });

        _projectCreatedTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "project_created.json") },
            {"de", _emailTemplateRepository.Get("de", "project_created.json") }
        });

        _projectResponsibleOrganizationChangeTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "project_responsible_organization_change.json") },
            {"de", _emailTemplateRepository.Get("de", "project_responsible_organization_change.json") }
        });

        _projectDeletedTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "project_deleted.json") },
            {"de", _emailTemplateRepository.Get("de", "project_deleted.json") }
        });

        _publicationRequestOwnersTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "publication_request_confirmation_owners.json") },
            {"de", _emailTemplateRepository.Get("de", "publication_request_confirmation_owners.json") }
        });

        _publicationRequestPublicationAdvisoryServiceTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "publication_request_info_publication_advisory_service.json") },
            {"de", _emailTemplateRepository.Get("de", "publication_request_info_publication_advisory_service.json") }
        });

        _publicationRequestResourcePartTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "publication_request_resource_part.json") },
            {"de", _emailTemplateRepository.Get("de", "publication_request_resource_part.json") }
        });

        _inviteUserToProjectTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "project_invitation.json") },
            {"de", _emailTemplateRepository.Get("de", "project_invitation.json") }
        });

        _resourceCreatedTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "resource_created.json") },
            {"de", _emailTemplateRepository.Get("de", "resource_created.json") }
        });

        _resourceMaintenanceModeEnabledTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "resource_maintenance_mode_enabled.json") },
            {"de", _emailTemplateRepository.Get("de", "resource_maintenance_mode_enabled.json") }
        });

        _resourceMaintenanceModeDisabledTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "resource_maintenance_mode_disabled.json") },
            {"de", _emailTemplateRepository.Get("de", "resource_maintenance_mode_disabled.json") }
        });

        _resourceDeletedTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "resource_deleted.json") },
            {"de", _emailTemplateRepository.Get("de", "resource_deleted.json") }
        });

        _userRoleChangedTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "user_role_changed.json") },
            {"de", _emailTemplateRepository.Get("de", "user_role_changed.json") }
        });

        _userDeletedTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "user_deleted.json") },
            {"de", _emailTemplateRepository.Get("de", "user_deleted.json") }
        });

        _userEmailChangedTemplates = new Lazy<Dictionary<string, EmailTemplate>>(() => new Dictionary<string, EmailTemplate>
        {
            {"en", _emailTemplateRepository.Get("en", "user_email_changed.json") },
            {"de", _emailTemplateRepository.Get("de", "user_email_changed.json") }
        });
    }

    public IReadOnlyDictionary<string, EmailTemplate> PidContactConfirmationTemplates => _pidContactConfirmationTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> PidContactProjectTemplates => _pidContactProjectTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> PidContactResourceTemplates => _pidContactResourceTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> APCreationRequestTemplates => _apCreationRequestTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> APCreationRequestServiceDeskTemplates => _apCreationRequestServiceDeskTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> ProjectCreatedTemplates => _projectCreatedTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> ProjectResponsibleOrganizationChangeTemplates => _projectResponsibleOrganizationChangeTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> ProjectDeletedTemplates => _projectDeletedTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> PublicationRequestOwnersTemplates => _publicationRequestOwnersTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> PublicationRequestPublicationAdvisoryServiceTemplates => _publicationRequestPublicationAdvisoryServiceTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> PublicationRequestResourcePartTemplates => _publicationRequestResourcePartTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> InviteUserToProjectTemplates => _inviteUserToProjectTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> ResourceCreatedTemplates => _resourceCreatedTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> ResourceMaintenanceModeEnabledTemplates => _resourceMaintenanceModeEnabledTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> ResourceMaintenanceModeDisabledTemplates => _resourceMaintenanceModeDisabledTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> ResourceDeletedTemplates => _resourceDeletedTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> UserRoleChangedTemplates => _userRoleChangedTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> UserDeletedTemplates => _userDeletedTemplates.Value;
    public IReadOnlyDictionary<string, EmailTemplate> UserEmailChangedTemplates => _userEmailChangedTemplates.Value;
}