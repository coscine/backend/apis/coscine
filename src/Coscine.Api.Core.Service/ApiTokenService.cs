﻿using AutoMapper;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.Extensions.Options;
using System.Security.Claims;

namespace Coscine.Api.Core.Service;

public class ApiTokenService : IApiTokenService
{
    private readonly IRepositoryContextLoader _repositoryContextLoader;
    private readonly IAuthenticatorService _authenticatorService;
    private readonly IApiTokenRepository _apiTokenRepository;
    private readonly IMapper _mapper;
    private readonly JwtConfiguration _jwtConfiguration;

    public ApiTokenService(
        IRepositoryContextLoader repositoryContextLoader,
        IAuthenticatorService authenticatorService,
        IApiTokenRepository apiTokenRepository,
        IMapper mapper,
        IOptionsMonitor<JwtConfiguration> jwtConfiguration)
    {
        _repositoryContextLoader = repositoryContextLoader;
        _authenticatorService = authenticatorService;
        _apiTokenRepository = apiTokenRepository;
        _mapper = mapper;
        _jwtConfiguration = jwtConfiguration.CurrentValue;
    }

    public async Task<ApiTokenDto> CreateApiTokenAsync(ClaimsPrincipal principal, ApiTokenForCreationDto apiTokenForCreationDto)
    {
        var user = await _authenticatorService.GetUserAsync(principal, trackChanges: false) ?? throw new UserNotFoundException();

        var tokenId = Guid.NewGuid();

        var jwtSecurityToken = _authenticatorService.GenerateJwtSecurityToken(
            new Dictionary<string, string>
            {
                ["tokenId"] = tokenId.ToString()
            },
            TimeSpan.FromDays(apiTokenForCreationDto.ExpiresInDays).TotalMinutes,
            _jwtConfiguration.JsonWebKeys.First(x => x.KeySize >= 256));

        var token = _authenticatorService.GetJwtStringFromToken(jwtSecurityToken);

        var apiTokenEntity = new ApiToken
        {
            Id = tokenId,
            Expiration = jwtSecurityToken.Payload.ValidTo,
            IssuedAt = jwtSecurityToken.Payload.IssuedAt,
            Name = apiTokenForCreationDto.Name,
            UserId = user.Id
        };

        _apiTokenRepository.Create(apiTokenEntity);

        await _repositoryContextLoader.SaveAsync();

        var apiTokenDto = _mapper.Map<ApiTokenDto>(apiTokenEntity);

        apiTokenDto.Token = token;

        return apiTokenDto;
    }

    public async Task DeleteApiTokenAsync(Guid apiTokenId, ClaimsPrincipal principal, bool trackChanges)
    {
        var user = await _authenticatorService.GetUserAsync(principal, trackChanges: false) ?? throw new UserNotFoundException();

        var apiTokenEntity = await _apiTokenRepository.GetAsync(apiTokenId, user.Id, trackChanges) ?? throw new ApiTokenNotFoundException(apiTokenId);

        _apiTokenRepository.Delete(apiTokenEntity);

        await _repositoryContextLoader.SaveAsync();
    }

    public async Task<ApiTokenDto> GetApiTokenAsync(Guid apiTokenId, ClaimsPrincipal principal, bool trackChanges)
    {
        var user = await _authenticatorService.GetUserAsync(principal, trackChanges: false) ?? throw new UserNotFoundException();

        var apiTokenEntity = await _apiTokenRepository.GetAsync(apiTokenId, user.Id, trackChanges) ?? throw new ApiTokenNotFoundException(apiTokenId);

        return _mapper.Map<ApiTokenDto>(apiTokenEntity);
    }

    public async Task<PagedEnumerable<ApiTokenDto>> GetPagedApiTokensAsync(ClaimsPrincipal principal, ApiTokenParameters apiTokenParameters, bool trackChanges)
    {
        var user = await _authenticatorService.GetUserAsync(principal, trackChanges: false) ?? throw new UserNotFoundException();

        var apiTokenEntities = await _apiTokenRepository.GetPagedAsync(user.Id, apiTokenParameters, trackChanges);
        var apiTokenDtos = _mapper.Map<IEnumerable<ApiTokenDto>>(apiTokenEntities);

        return new PagedEnumerable<ApiTokenDto>(apiTokenDtos, apiTokenEntities.Pagination);
    }
}