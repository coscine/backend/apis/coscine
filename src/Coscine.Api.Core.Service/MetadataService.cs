﻿using AutoMapper;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service;

public class MetadataService(
    IMetadataRepository metadataRepository,
    IRdfRepositoryBase rdfRepositoryBase,
    IMapper mapper
) : IMetadataService
{
    private readonly IMetadataRepository _metadataRepository = metadataRepository;
    private readonly IRdfRepositoryBase _rdfRepositoryBase = rdfRepositoryBase;
    private readonly IMapper _mapper = mapper;

    public async Task<RdfDefinitionDto> GetMetadataGraphAsync(Uri graphUri, MetadataGetAdminParameters metadataGetAdminParameters, bool trackChanges)
    {
        return await _metadataRepository.GetAsync(graphUri, metadataGetAdminParameters, trackChanges);
    }

    public async Task<PagedEnumerable<DeployedGraphDto>> GetPagedDeployedGraphsAsync(DeployedGraphParameters deployedGraphParameters)
    {
        var deployedGraphs = await _rdfRepositoryBase.GetDeployedGraphsAsync(deployedGraphParameters);
        var deployedGraphDtos = _mapper.Map<IEnumerable<DeployedGraphDto>>(deployedGraphs);

        return new PagedEnumerable<DeployedGraphDto>(deployedGraphDtos, deployedGraphs.Pagination);
    }

    public async Task UpdateMetadataGraphAsync(Uri graphUri, MetadataUpdateAdminParameters metadataUpdateAdminParameters, bool trackChanges)
    {
        await _metadataRepository.UpdateAsync(graphUri, metadataUpdateAdminParameters, trackChanges);
    }

    public async Task PatchMetadataGraphAsync(Uri graphUri, RdfPatchDocumentDto rdfPatchDoc)
    {
        await _metadataRepository.PatchAsync(graphUri, rdfPatchDoc);
    }
}