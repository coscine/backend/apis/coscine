using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.InternalServerError;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Microsoft.Extensions.Options;

namespace Coscine.Api.Core.Service;

public class TosService : ITosService
{
    private readonly TermsOfServiceConfiguration _termsOfServiceConfiguration;

    public TosService(IOptionsMonitor<TermsOfServiceConfiguration> termsOfServiceConfiguration)
    {
        _termsOfServiceConfiguration = termsOfServiceConfiguration.CurrentValue;

        // Ensure that ToS version and URL are properly set
        ArgumentException.ThrowIfNullOrWhiteSpace(_termsOfServiceConfiguration.Version);
        ArgumentException.ThrowIfNullOrWhiteSpace(_termsOfServiceConfiguration.Url);
    }

    public TermsOfServiceDto GetCurrentTosVersion()
    {
        var termsOfServiceVersion = _termsOfServiceConfiguration.Version
            ?? throw new TermsOfServiceInternalServerErrorException();
        var termsOfServiceUrl = _termsOfServiceConfiguration.Url
            ?? throw new TermsOfServiceInternalServerErrorException();

        return new TermsOfServiceDto
        {
            Version = termsOfServiceVersion,
            Href = new Uri(termsOfServiceUrl),
            IsCurrent = true
        };
    }
}