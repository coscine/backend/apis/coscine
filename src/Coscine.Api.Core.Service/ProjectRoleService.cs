using AutoMapper;
using Coscine.Api.Core.Entities.EventArgs;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.Conflict;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Coscine.Api.Core.Service.Extensions.Utility;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Reactive.Subjects;
using System.Security.Claims;

namespace Coscine.Api.Core.Service;

public class ProjectRoleService : IProjectRoleService
{
    private readonly IRepositoryContextLoader _repositoryContextLoader;
    private readonly IProjectRepository _projectRepository;
    private readonly IProjectRoleRepository _projectRoleRepository;
    private readonly IUserRepository _userRepository;
    private readonly IRoleRepository _roleRepository;
    private readonly IAuthenticatorService _authenticatorService;
    private readonly IMapper _mapper;

    private readonly Subject<ProjectRoleCreationEventArgs> _projectRoleCreated = new();
    private readonly Subject<ProjectRoleUpdateEventArgs> _projectRoleUdated = new();
    private readonly Subject<ProjectRoleDeletionEventArgs> _projectRoleDeleted = new();

    public ProjectRoleService(
        IRepositoryContextLoader repositoryContextLoader,
        IProjectRepository projectRepository,
        IProjectRoleRepository projectRoleRepository,
        IUserRepository userRepository,
        IRoleRepository roleRepository,
        IAuthenticatorService authenticatorService,
        IMapper mapper,
        IObserverManager observerManager)
    {
        _repositoryContextLoader = repositoryContextLoader;
        _projectRepository = projectRepository;
        _projectRoleRepository = projectRoleRepository;
        _userRepository = userRepository;
        _roleRepository = roleRepository;
        _authenticatorService = authenticatorService;
        _mapper = mapper;

        _projectRoleCreated.SubscribeAll(observerManager.ProjectRoleCreationObservers);
        _projectRoleUdated.SubscribeAll(observerManager.ProjectRoleUpdateObservers);
        _projectRoleDeleted.SubscribeAll(observerManager.ProjectRoleDeletionObservers);
    }

    public async Task<ProjectRoleDto> CreateProjectRoleForProject(Guid projectId, ClaimsPrincipal principal, ProjectRoleForProjectCreationDto projectRoleForProjectCreationDto)
    {
        _ = await _authenticatorService.GetUserAsync(principal, trackChanges: false)
            ?? throw new UserNotFoundException();

        var userToAdd = await _userRepository.GetAsync(projectRoleForProjectCreationDto.UserId, trackChanges: false)
            ?? throw new UserNotFoundException(projectRoleForProjectCreationDto.UserId);

        var projectEntity = await _projectRepository.GetAsync(projectId, trackChanges: false)
            ?? throw new ProjectNotFoundException(projectId);

        var roleEntity = await _roleRepository.GetAsync(projectRoleForProjectCreationDto.RoleId, trackChanges: false)
            ?? throw new RoleNotFoundException(projectRoleForProjectCreationDto.RoleId);

        if ((await _projectRoleRepository.GetAllByProjectAsync(projectEntity.Id, userToAdd.Id, trackChanges: false)).Any())
        {
            // User is already member of the project
            throw new UserAlreadyMemberOfProjectConflictException(userToAdd.Id, projectEntity.Id);
        }

        var projectRoleEntity = new ProjectRole { ProjectId = projectEntity.Id, RoleId = roleEntity.Id, UserId = userToAdd.Id };

        _projectRoleRepository.Create(projectRoleEntity);
        await _repositoryContextLoader.SaveAsync();

        // Reload for the navigation properties
        projectRoleEntity = await _projectRoleRepository.GetAsync(projectEntity.Id, projectRoleEntity.RelationId, trackChanges: true)
            ?? throw new ProjectRoleNotFoundException(projectEntity.Id);

        _projectRoleCreated.OnNext(new() { Project = projectEntity, User = userToAdd });

        return _mapper.Map<ProjectRoleDto>(projectRoleEntity);
    }

    public async Task DeleteProjectRoleOfProject(Guid projectRole, Guid projectId, ClaimsPrincipal principal, bool trackChanges)
    {
        var user = await _authenticatorService.GetUserAsync(principal, trackChanges: false) ?? throw new UserNotFoundException();

        var projectEntity = await _projectRepository.GetAsync(projectId, user.Id, trackChanges)
            ?? throw new ProjectNotFoundException(projectId);

        var projectRoleEntity = await _projectRoleRepository.GetAsync(projectEntity.Id, projectRole, trackChanges)
            ?? throw new ProjectRoleNotFoundException(projectEntity.Id);

        var ownerRole = _roleRepository.GetOwnerRole();

        // Check if this would be the last owner
        if (projectRoleEntity.Role.Id == ownerRole.Id
            && (await _projectRoleRepository.GetAllByRoleAsync(projectEntity.Id, ownerRole.Id, trackChanges)).Count() < 2)
        {
            throw new RemovalOfLastOwnerBadRequestException(projectRoleEntity.User.Id, projectEntity.Id);
        }

        _projectRoleRepository.Delete(projectRoleEntity);

        await _repositoryContextLoader.SaveAsync();

        _projectRoleDeleted.OnNext(new() { ProjectRole = projectRoleEntity });
    }

    public async Task<PagedEnumerable<ProjectRoleDto>> GetPagedProjectRolesOfProjectById(Guid projectId, ProjectRoleParameters projectRoleParameters, bool trackChanges)
    {
        var projectRolesDb = await _projectRoleRepository.GetPagedAsync(projectId, projectRoleParameters, trackChanges);
        var projectRolesDto = _mapper.Map<IEnumerable<ProjectRoleDto>>(projectRolesDb);

        return new PagedEnumerable<ProjectRoleDto>(projectRolesDto, projectRolesDb.Pagination);
    }

    public async Task<ProjectRoleDto> GetProjectRoleOfProjectById(Guid projectId, Guid id, bool trackChanges)
    {
        var projectRoleEntity = await _projectRoleRepository.GetAsync(projectId, id, trackChanges)
            ?? throw new ProjectRoleNotFoundException(id);

        return _mapper.Map<ProjectRoleDto>(projectRoleEntity);
    }

    public async Task UpdateProjectRoleOfProject(Guid projectRole, Guid projectId, ClaimsPrincipal principal, ProjectRoleForProjectManipulationDto projectRoleForProjectManipulationDto, bool trackChanges)
    {
        var user = await _authenticatorService.GetUserAsync(principal, trackChanges: false) ?? throw new UserNotFoundException();

        var projectEntity = await _projectRepository.GetAsync(projectId, user.Id, trackChanges)
            ?? throw new ProjectNotFoundException(projectId);

        var role = await _roleRepository.GetAsync(projectRoleForProjectManipulationDto.RoleId, trackChanges)
            ?? throw new RoleNotFoundException(projectRoleForProjectManipulationDto.RoleId);

        var projectRoleEntity = await _projectRoleRepository.GetAsync(projectEntity.Id, projectRole, trackChanges)
            ?? throw new ProjectRoleNotFoundException(projectEntity.Id);

        var ownerRole = _roleRepository.GetOwnerRole();

        // Check if this would be the last owner
        if (projectRoleEntity.Role.Id == ownerRole.Id
            && (await _projectRoleRepository.GetAllByRoleAsync(projectEntity.Id, ownerRole.Id, trackChanges)).Count() < 2)
        {
            throw new RemovalOfLastOwnerBadRequestException(projectRoleEntity.User.Id, projectEntity.Id);
        }

        projectRoleEntity.RoleId = role.Id;

        await _repositoryContextLoader.SaveAsync();

        _projectRoleUdated.OnNext(new() { ProjectRole = projectRoleEntity });
    }
}