using AutoMapper;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Exceptions.Unauthorized;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;
using GitLabApiClient;

namespace Coscine.Api.Core.Service;

public class ResourceTypeGitlabService(IMapper mapper) : IResourceTypeGitlabService
{
    /// <summary>
    /// Retrieves all GitLab projects.
    /// </summary>
    /// <param name="gitlabCredentials">The GitLab credentials.</param>
    /// <returns>An IEnumerable of <see cref="GitlabProjectDto"/> for each GitLab project.</returns>
    public async Task<IEnumerable<GitlabProjectDto>> GetAllGitlabProjectsAsync(GitlabCredentialsDto gitlabCredentials)
    {
        try
        {
            var gitlabClient = CreateGitlabClient(gitlabCredentials);
            var projects = await gitlabClient.Projects.GetAsync(o => o.IsMemberOf = true);
            return mapper.Map<IEnumerable<GitlabProjectDto>>(projects);
        }
        catch (GitLabException exception)
        {
            throw HandleGitlabException(exception);
        }
    }

    /// <summary>
    /// Retrieves a GitLab project by its ID.
    /// </summary>
    /// <param name="gitlabProjectId">The ID of the GitLab project.</param>
    /// <param name="gitlabCredentials">The GitLab credentials.</param>
    /// <returns>A <see cref="GitlabProjectDto"/> for the requested GitLab project.</returns>
    public async Task<GitlabProjectDto> GetGitlabProjectAsync(int gitlabProjectId, GitlabCredentialsDto gitlabCredentials)
    {
        try
        {
            var gitlabClient = CreateGitlabClient(gitlabCredentials);
            var project = (await gitlabClient.Projects.GetAsync(o => o.IsMemberOf = true))
                .FirstOrDefault(p => p.Id.Equals(gitlabProjectId)) ?? throw new GitLabProjectNotFoundException(gitlabProjectId);
            return mapper.Map<GitlabProjectDto>(project);
        }
        catch (GitLabException exception)
        {
            throw HandleGitlabException(exception, gitlabProjectId);
        }
    }

    /// <summary>
    /// Retrieves all branches for a GitLab project.
    /// </summary>
    /// <param name="gitlabProjectId">The ID of the GitLab project.</param>
    /// <param name="gitlabCredentials">The GitLab credentials.</param>
    /// <returns>An IEnumerable of <see cref="GitlabBranchDto"/> for each GitLab branch.</returns>
    public async Task<IEnumerable<GitlabBranchDto>> GetAllGitlabBranchesForProjectAsync(int gitlabProjectId, GitlabCredentialsDto gitlabCredentials)
    {
        try
        {
            var gitlabClient = CreateGitlabClient(gitlabCredentials);
            var branches = await gitlabClient.Branches.GetAsync(gitlabProjectId, _ => { });
            return mapper.Map<IEnumerable<GitlabBranchDto>>(branches);
        }
        catch (GitLabException exception)
        {
            throw HandleGitlabException(exception, gitlabProjectId);
        }
    }

    /// <summary>
    /// Creates a GitLab client instance.
    /// </summary>
    /// <param name="gitlabCredentials">The GitLab credentials.</param>
    /// <returns>The GitLab client.</returns>
    private static GitLabClient CreateGitlabClient(GitlabCredentialsDto gitlabCredentials)
        => new(gitlabCredentials.Domain.ToString(), gitlabCredentials.AccessToken);

    /// <summary>
    /// Handles GitLab exceptions.
    /// </summary>
    /// <param name="exception">The GitLab exception.</param>
    /// <param name="gitlabProjectId">The ID of the GitLab project (optional).</param>
    /// <exception cref="GitlabUnauthorizedException">Thrown when the request is unauthorized.</exception>
    /// <exception cref="GitLabProjectNotFoundException">Thrown when the GitLab project is not found.</exception>
    /// <remarks>This method is necessary, because of the way the used GitLab library handles errors and to ensure consistency across our API.</remarks>
    private static GitLabException HandleGitlabException(GitLabException exception, int? gitlabProjectId = null)
    {
        if (exception.HttpStatusCode is System.Net.HttpStatusCode.Unauthorized && gitlabProjectId is null)
        {
            throw new GitlabUnauthorizedException();
        }

        if (exception.HttpStatusCode is System.Net.HttpStatusCode.Unauthorized && gitlabProjectId is not null)
        {
            throw new GitlabUnauthorizedException(gitlabProjectId.Value);
        }

        if (exception.HttpStatusCode is System.Net.HttpStatusCode.NotFound && gitlabProjectId is not null)
        {
            throw new GitLabProjectNotFoundException(gitlabProjectId.Value);
        }

        return exception;
    }
}
