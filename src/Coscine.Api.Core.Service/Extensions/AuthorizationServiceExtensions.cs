﻿using Coscine.Api.Core.Entities.Exceptions.Forbidden;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using System.Security.Claims;

namespace Coscine.Api.Core.Service.Extensions;

public static class AuthorizationServiceExtensions
{
    public static async Task AuthorizeAndThrowIfUnauthorized(this IAuthorizationService authorizationService, ClaimsPrincipal principal, Project resource, OperationAuthorizationRequirement requirement)
    {
        var result = await authorizationService.AuthorizeAsync(principal, resource, requirement);

        if (!result.Succeeded)
        {
            throw new ProjectForbiddenException(resource.Id, requirement);
        }
    }

    public static async Task AuthorizeAndThrowIfUnauthorized(this IAuthorizationService authorizationService, ClaimsPrincipal principal, Resource resource, OperationAuthorizationRequirement requirement)
    {
        var result = await authorizationService.AuthorizeAsync(principal, resource, requirement);

        if (!result.Succeeded)
        {
            throw new ResourceForbiddenException(resource.Id, requirement);
        }
    }

    public static async Task AuthorizeAndThrowIfUnauthorized(this IAuthorizationService authorizationService, ClaimsPrincipal principal, Invitation resource, OperationAuthorizationRequirement requirement)
    {
        var result = await authorizationService.AuthorizeAsync(principal, resource, requirement);

        if (!result.Succeeded)
        {
            throw new ProjectInvitationForbiddenException(resource.Id, requirement);
        }
    }

    public static async Task AuthorizeAndThrowIfUnauthorized(this IAuthorizationService authorizationService, ClaimsPrincipal principal, ProjectQuota resource, OperationAuthorizationRequirement requirement)
    {
        var result = await authorizationService.AuthorizeAsync(principal, resource, requirement);

        if (!result.Succeeded)
        {
            throw new ProjectQuotaForbiddenException(resource.ProjectId, requirement);
        }
    }

    public static async Task AuthorizeAndThrowIfUnauthorized(this IAuthorizationService authorizationService, ClaimsPrincipal principal, User user, OperationAuthorizationRequirement requirement)
    {
        var result = await authorizationService.AuthorizeAsync(principal, user, requirement);

        if (!result.Succeeded)
        {
            throw new ResourceForbiddenException(user.Id, requirement);
        }
    }

    public static async Task AuthorizeAndThrowIfUnauthorized(this IAuthorizationService authorizationService, ClaimsPrincipal principal, Handle resource, OperationAuthorizationRequirement requirement)
    {
        var result = await authorizationService.AuthorizeAsync(principal, resource, requirement);

        if (!result.Succeeded)
        {
            if (resource.Pid is null)
            {
                throw new HandleForbiddenException(requirement);
            }
            else
            {
                throw new HandleForbiddenException(resource.Pid, requirement);
            }
        }
    }

    public static async Task AuthorizeAndThrowIfUnauthorized(this IAuthorizationService authorizationService, ClaimsPrincipal principal, Pid resource, OperationAuthorizationRequirement requirement)
    {
        var result = await authorizationService.AuthorizeAsync(principal, resource, requirement);

        if (!result.Succeeded)
        {
            throw new PidForbiddenException(resource, requirement);
        }
    }
}