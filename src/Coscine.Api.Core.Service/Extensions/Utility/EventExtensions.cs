﻿namespace Coscine.Api.Core.Service.Extensions.Utility;

public static class EventExtensions
{
    public static void SubscribeAll<T>(this IObservable<T> observable, IEnumerable<IObserver<T>> observers)
    {
        foreach (var observer in observers)
        {
            observable.Subscribe(observer);
        }
    }
}