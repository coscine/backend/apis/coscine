﻿using AutoMapper;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Enums.Utility;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Security.Claims;

namespace Coscine.Api.Core.Service;

public class SearchService(
    IProjectRepository projectRepository,
    ILanguageRepository languageRepository,
    ISearchRepository searchRepository,
    IAuthenticatorService authentication,
    IMapper mapper) : ISearchService
{
    private readonly IProjectRepository _projectRepository = projectRepository;
    private readonly ILanguageRepository _languageRepository = languageRepository;
    private readonly ISearchRepository _searchRepository = searchRepository;
    private readonly IAuthenticatorService _authentication = authentication;
    private readonly IMapper _mapper = mapper;

    public async Task<PagedSearchResult<SearchResultDto>> SearchAsync(ClaimsPrincipal principal, SearchParameters searchParameters)
    {
        IEnumerable<Guid>? projects = null;
        if (principal is not null)
        {
            // get current user
            var user = await _authentication.GetUserAsync(principal, trackChanges: false);

            if (user is not null)
            {
                // get project ids
                projects = await _projectRepository.GetAllProjectIdsAsync(user.Id, trackChanges: false);
            }
        }

        // get languages
        searchParameters.Languages ??= (await _languageRepository.GetAllAsync(new LanguageParameters(), trackChanges: false))
                                                    .Select(l => l.Abbreviation);

        // get search results
        var (elasticSearchResults, totalCount) = await _searchRepository.SearchAsync(searchParameters, projects);

        // map the search results
        var searchResults = MapToSearchResults(elasticSearchResults);

        // map the result into the dto
        var searchResultDtos = _mapper.Map<IEnumerable<SearchResultDto>>(searchResults);

        // category count
        var categoryCounts = new List<SearchCategory>();
        foreach (SearchCategoryType category in Enum.GetValues(typeof(SearchCategoryType)))
        {
            var categoryCount = await _searchRepository.CountAsync(searchParameters, category, projects);
            categoryCounts.Add(new SearchCategory(category.ToString(), categoryCount));
        }

        // return Dtos
        return new PagedSearchResult<SearchResultDto>(searchResultDtos, totalCount, searchParameters.PageNumber, searchParameters.PageSize, categoryCounts);
    }

    /// <summary>
    /// Maps the dynamic Elastic Search results to strongly typed <see cref="SearchResult"/> entities.
    /// </summary>
    /// <param name="results">Dynamic collection of Elastic Search results.</param>
    /// <returns>Mapped collection of strongly typed <see cref="SearchResult"/> entities.</returns>
    private static IEnumerable<SearchResult> MapToSearchResults(IEnumerable<dynamic> results)
    {
        var searchResults = new List<SearchResult>();
        foreach (var entry in results)
        {
            var searchResult = new SearchResult();
            // Handle type
            var typeString = (string)entry["structureType"];
            if (typeString.TryParseEnumMember<SearchCategoryType>(out var searchCategoryType))
            {
                searchResult.Type = searchCategoryType;
            }
            // Handle Graph Name
            if (Uri.TryCreate(entry["graphName"], UriKind.Absolute, out Uri graphName))
            {
                searchResult.Uri = graphName;
            }
            searchResult.Source = entry;

            searchResults.Add(searchResult);
        }
        return searchResults;
    }
}