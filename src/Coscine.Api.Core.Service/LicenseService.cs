﻿using AutoMapper;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service;

public class LicenseService : ILicenseService
{
    private readonly ILicenseRepository _licenseRepository;
    private readonly IMapper _mapper;

    public LicenseService(ILicenseRepository licenseRepository, IMapper mapper)
    {
        _licenseRepository = licenseRepository;
        _mapper = mapper;
    }

    public async Task<PagedEnumerable<LicenseDto>> GetPagedLicenses(LicenseParameters licenseParameters, bool trackChanges)
    {
        var licensesDb = await _licenseRepository.GetPagedAsync(licenseParameters, trackChanges);
        var licensesDto = _mapper.Map<IEnumerable<LicenseDto>>(licensesDb);

        return new PagedEnumerable<LicenseDto>(licensesDto, licensesDb.Pagination);
    }

    public async Task<LicenseDto> GetLicenseById(Guid id, bool trackChanges)
    {
        var licenseEntity = await _licenseRepository.GetAsync(id, trackChanges) ?? throw new LicenseNotFoundException(id);

        return _mapper.Map<LicenseDto>(licenseEntity);
    }
}