using AutoMapper;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Service;

public class LanguageService : ILanguageService
{
    private readonly ILanguageRepository _languageRepository;
    private readonly IMapper _mapper;

    public LanguageService(
        ILanguageRepository languageRepository,
        IMapper mapper)
    {
        _languageRepository = languageRepository;
        _mapper = mapper;
    }

    public async Task<IEnumerable<LanguageDto>> GetAllLanguages(LanguageParameters languageParameters, bool trackChanges)
    {
        var languageEntities = await _languageRepository.GetAllAsync(languageParameters, trackChanges);
        return _mapper.Map<IEnumerable<LanguageDto>>(languageEntities);
    }

    public async Task<LanguageDto> GetLanguageById(Guid languageId, bool trackChanges)
    {
        var visibilityEntity = await _languageRepository.GetAsync(languageId, trackChanges) ?? throw new LanguageNotFoundException(languageId);

        return _mapper.Map<LanguageDto>(visibilityEntity);
    }
}