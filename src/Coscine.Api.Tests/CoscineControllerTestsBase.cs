﻿using Coscine.Api.Tests.IntegrationTests;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Data.SqlClient;
using NUnit.Framework;

namespace Coscine.Api.Tests;

[Explicit]
[TestFixture]
public abstract class CoscineControllerTestsBase
{
    protected static async Task InitializeDatabaseAsync(string saConnectionString, string username, string password)
    {
        var commands = new[]
        {
            // Create a new database
            "CREATE DATABASE Coscine;",
            // Switch to the new database
            "USE Coscine;",
            // Create a new user
            $"CREATE LOGIN {username} WITH PASSWORD = '{password}';",
            $"CREATE USER {username} FOR LOGIN {username};",
            // Grant privileges to the user
            $"ALTER ROLE db_owner ADD MEMBER {username};"
        };

        using var connection = new SqlConnection(saConnectionString);
        await connection.OpenAsync();

        foreach (var commandText in commands)
        {
            using var command = new SqlCommand(commandText, connection);
            await command.ExecuteNonQueryAsync();
        }
    }

    protected static async Task DropDatabaseAsync(string saConnectionString, string username, string password)
    {
        var commands = new[]
        {
            "USE Coscine;",
            // Drop the user from the database
            $"DROP USER {username};",
            // Kill active sessions for the login
            $"DECLARE @killCommand NVARCHAR(MAX); " +
            "SELECT @killCommand = " +
            "STRING_AGG('KILL ' + CAST(session_id AS NVARCHAR(10)), '; ') " +
            "FROM sys.dm_exec_sessions WHERE login_name = '" + username + "'; " +
            "EXEC sp_executesql @killCommand;",
            "USE master;",
            // Drop the login for the user
            $"DROP LOGIN {username};",
            // Drop the database
            "DROP DATABASE Coscine;"
        };

        using var connection = new SqlConnection(saConnectionString);
        await connection.OpenAsync();

        foreach (var commandText in commands)
        {
            using var command = new SqlCommand(commandText, connection);
            await command.ExecuteNonQueryAsync();
        }
    }

    [SetUp]
    public async Task SetUpAsync()
    {
        await InitializeDatabaseAsync(MsSqlTestContainerSetup.SaConnectionString, MsSqlTestContainerSetup.DbUser, MsSqlTestContainerSetup.DbPassword);
    }

    public async Task<HttpClient> InitializeClientAsync(WebApplicationFactory<Program> factory, bool admin = false)
    {
        await factory.SeedTestUserAsync();

        return factory.CreateCoscineClient(admin: admin);
    }

    [TearDown]
    public async Task TearDownAsync()
    {
        await DropDatabaseAsync(MsSqlTestContainerSetup.SaConnectionString, MsSqlTestContainerSetup.DbUser, MsSqlTestContainerSetup.DbPassword);
    }
}