﻿using Microsoft.Extensions.Logging;
using NSubstitute;

namespace Coscine.Api.Tests;

/// <summary>
/// Provides extension methods for testing logger behavior.
/// </summary>
public static class LoggerTestExtensions
{
    /// <summary>
    /// Checks if a log of a specific <see cref="LogLevel"/> has been made.
    /// This method is useful for unit testing to verify if a log entry was created
    /// for a specific log level.
    /// </summary>
    /// <typeparam name="T">The type of the logger.</typeparam>
    /// <param name="logger">The logger instance to verify.</param>
    /// <param name="level">The <see cref="LogLevel"/> to check for in the logger's entries.</param>
    /// <remarks>
    /// This method does not verify the specific content of the log message but checks
    /// for the occurrence of a log entry at a specific <see cref="LogLevel"/>.
    /// </remarks>
    public static void AnyLogOfType<T>(this ILogger<T> logger, LogLevel level) where T : class
    {
        logger.Log(level, Arg.Any<EventId>(), Arg.Any<object>(), Arg.Any<Exception>(),
            Arg.Any<Func<object, Exception?, string>>());
    }
}
