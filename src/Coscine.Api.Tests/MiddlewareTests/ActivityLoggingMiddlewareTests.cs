﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using System.Security.Claims;

namespace Coscine.Api.Tests.MiddlewareTests;

[TestFixture]
public class ActivityLoggingMiddlewareTests
{
    private RequestDelegate _next = null!;
    private IActivityLogRepository _activityLogRepository = null!;
    private IRepositoryContextLoader _repositoryContextLoader = null!;
    private IAuthenticatorService _authenticatorService = null!;
    private ILogger<ActivityLoggingMiddleware> _logger = null!;
    private IOptionsMonitor<ActivityLogConfiguration> _activityLogConfiguration = null!;
    private DefaultHttpContext _httpContext = null!;

    [SetUp]
    public void SetUp()
    {
        _next = Substitute.For<RequestDelegate>();
        _activityLogRepository = Substitute.For<IActivityLogRepository>();
        _repositoryContextLoader = Substitute.For<IRepositoryContextLoader>();
        _authenticatorService = Substitute.For<IAuthenticatorService>();
        _logger = Substitute.For<ILogger<ActivityLoggingMiddleware>>();

        _activityLogConfiguration = Substitute.For<IOptionsMonitor<ActivityLogConfiguration>>();
        _httpContext = new DefaultHttpContext();

        // Mocking the ActivityLogConfiguration
        var activityLogConfig = new ActivityLogConfiguration { IsEnabled = true, RetentionDays = 7 };
        _activityLogConfiguration.CurrentValue.Returns(activityLogConfig);
    }

    [Test]
    public async Task Invoke_ShouldLogActivity_WhenLoggingIsEnabled()
    {
        // Arrange
        var middleware = new ActivityLoggingMiddleware(_next);
        var user = new User() { Id = Guid.NewGuid(), LatestActivity = DateTime.MinValue };
        _authenticatorService.GetUserAsync(Arg.Any<ClaimsPrincipal>(), true).Returns(Task.FromResult<User?>(user));
        _httpContext.Request.Path = "/test/api/path";

        // Create some claims for the authenticated user
        var claims = new[]
        {
            new Claim(ClaimTypes.Name, "John Doe"),
            new Claim(ClaimTypes.Email, "john.doe@example.com"),
        };

        // Create an identity for the user
        var identity = new ClaimsIdentity(claims, "CustomAuthType");

        // Create a principal and assign the authenticated identity
        var principal = new ClaimsPrincipal(identity);

        _httpContext.User = principal;

        // Act
        await middleware.Invoke(_httpContext, _logger, _activityLogRepository, _repositoryContextLoader, _authenticatorService, _activityLogConfiguration);

        // Assert
        _activityLogRepository.Received(1).Create(Arg.Is<ActivityLog>(log =>
            log.UserId == user.Id &&
            log.ApiPath == "/test/api/path")
        );
        _activityLogRepository.Received(1).DeleteAllBeforeAsync(Arg.Any<DateTime>());
        Assert.That(user.LatestActivity, Is.GreaterThan(DateTime.MinValue));
        await _repositoryContextLoader.Received(1).SaveAsync();
    }

    [Test]
    public async Task Invoke_ShouldNotLogActivity_WhenLoggingIsDisabled()
    {
        // Arrange
        var middleware = new ActivityLoggingMiddleware(_next);
        _activityLogConfiguration.CurrentValue.Returns(new ActivityLogConfiguration { IsEnabled = false }); // Disable logging

        // Act
        await middleware.Invoke(_httpContext, _logger, _activityLogRepository, _repositoryContextLoader, _authenticatorService, _activityLogConfiguration);

        // Assert
        _activityLogRepository.DidNotReceive().Create(Arg.Any<ActivityLog>());
        _activityLogRepository.DidNotReceive().DeleteAllBeforeAsync(Arg.Any<DateTime>());
        await _repositoryContextLoader.DidNotReceive().SaveAsync();
    }

    [Test]
    public async Task Invoke_ShouldHandleExceptionsGracefully()
    {
        // Arrange
        var middleware = new ActivityLoggingMiddleware(_next);
        var exception = new Exception("Test exception");
        _authenticatorService.GetUserAsync(Arg.Any<ClaimsPrincipal>(), true).Throws(exception);

        // Act & Assert
        Assert.DoesNotThrowAsync(() => middleware.Invoke(_httpContext, _logger, _activityLogRepository, _repositoryContextLoader, _authenticatorService, _activityLogConfiguration));
        await _repositoryContextLoader.Received(0).SaveAsync();
    }

    [Test]
    public async Task Invoke_ShouldUseCustomRetentionPeriod_WhenConfigured()
    {
        // Arrange
        var middleware = new ActivityLoggingMiddleware(_next);
        var customRetentionDays = 10;
        _activityLogConfiguration.CurrentValue.Returns(new ActivityLogConfiguration { IsEnabled = true, RetentionDays = customRetentionDays }); // Set custom retention days
        var expectedDeletionDate = DateTime.UtcNow.AddDays(-customRetentionDays);
        var user = new User { Id = Guid.NewGuid(), LatestActivity = DateTime.MinValue };
        _authenticatorService.GetUserAsync(Arg.Any<ClaimsPrincipal>(), true).Returns(Task.FromResult<User?>(user));


        // Create some claims for the authenticated user
        var claims = new[]
        {
            new Claim(ClaimTypes.Name, "John Doe"),
            new Claim(ClaimTypes.Email, "john.doe@example.com"),
        };

        // Create an identity for the user
        var identity = new ClaimsIdentity(claims, "CustomAuthType");

        // Create a principal and assign the authenticated identity
        var principal = new ClaimsPrincipal(identity);

        _httpContext.User = principal;

        // Act
        await middleware.Invoke(_httpContext, _logger, _activityLogRepository, _repositoryContextLoader, _authenticatorService, _activityLogConfiguration);

        // Assert
        _activityLogRepository.Received(1).DeleteAllBeforeAsync(Arg.Is<DateTime>(d => d >= expectedDeletionDate && d <= expectedDeletionDate.AddSeconds(5))); // Check if the deletion date is within 5 seconds of the expected date
    }
}