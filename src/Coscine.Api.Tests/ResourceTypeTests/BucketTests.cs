using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Coscine.Api.Tests.ResourceTypeTests;

[TestFixture]
public class BucketTests
{
    private readonly Guid _testBucketId = Guid.Parse("0970bdeb-faaf-4e8a-ad1e-0499fd427315");
    //private ResourceTypeAuth? _lastAuth = null;
    private ResourceType? _lastResourceType;

    /*private IBaseResourceTypeDefinition GetResourceTypeDefinition(ResourceTypeAuth auth)
    {
        var factory = CoscineTestExtensions.CreateFactory();
        using var scope = factory.Services.CreateScope();
        var generalResourceTypeRepository = scope.ServiceProvider.GetRequiredService<IResourceTypeRepository>();
        var resourceTypeService = scope.ServiceProvider.GetRequiredService<IResourceTypeService>();
        var resourceTypeEntity = new Core.Entities.Models.ResourceType { Type = auth.Type, SpecificType = auth.SpecificType };
        var resourceEntity = new Core.Entities.Models.Resource { Id = _testBucketId, Type = resourceTypeEntity };

        resourceTypeService.CreateResourceTypeConfigurationForResource(resourceEntity, resourceTypeEntity, new Core.Shared.DataTransferObjects.ParameterObjects.ResourceTypeOptionsForCreationDto(), out var resourceTypeConfiguration);
        return generalResourceTypeRepository.Get(resourceTypeEntity.Type!, resourceTypeConfiguration);
    }*/


    private IDataStorageRepository GetDs(ResourceType resoruceType)
    {
        var factory = CoscineTestExtensions.CreateFactory();
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var ds = serviceProvider.GetRequiredService<IDataStorageRepositoryFactory>().Create(resoruceType.Id)
            ?? throw new Exception();

        return ds;
    }

    private IEcsManagerFactory GetEcsFactory(ResourceType resoruceType)
    {
        var factory = CoscineTestExtensions.CreateFactory();
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        return serviceProvider.GetRequiredService<IEcsManagerFactory>();
    }

    /*[TearDown]
    public void CleanUp()
    {
        if (_lastResourceType is not null)
        {
            // Delete users, if possible
            if (_lastResourceType)
            {
                var userEcsManager = new EcsManager
                {
                    EcsManagerConfiguration = _lastAuth.UserEcsManagerConfiguration
                };

                // Read User
                try
                {
                    userEcsManager.DeleteObjectUser($"read_{_testBucketId}").Wait();
                }
                catch (Exception)
                {

                }

                // Write User
                try
                {
                    userEcsManager.DeleteObjectUser($"write_{_testBucketId}").Wait();
                }
                catch (Exception)
                {

                }
            }

            if (_lastAuth.EcsManagerConfiguration is not null)
            {
                // Delete any remaining entries in the bucket
                try
                {
                    var amazonS3Config = new AmazonS3Config
                    {
                        ServiceURL = _lastAuth.S3Endpoint,
                        ForcePathStyle = true
                    };

                    using var s3Client = new AmazonS3Client(_lastAuth.S3AccessKey, _lastAuth.S3SecretKey, amazonS3Config);
                    ListObjectsV2Request request = new()
                    {
                        BucketName = _testBucketId.ToString()
                    };

                    ListObjectsV2Response response;
                    do
                    {
                        response = s3Client.ListObjectsV2Async(request).Result;
                        foreach (S3Object entry in response.S3Objects)
                        {
                            s3Client.DeleteObjectAsync(new DeleteObjectRequest
                            {
                                BucketName = _testBucketId.ToString(),
                                Key = entry.Key
                            });
                        }

                        request.ContinuationToken = response.NextContinuationToken;
                    } while (response.IsTruncated);
                }
                catch (Exception)
                {

                }

                // Delete the bucket
                var ecsManager = new EcsManager
                {
                    EcsManagerConfiguration = _lastAuth.EcsManagerConfiguration
                };

                try
                {
                    ecsManager.DeleteBucket(_testBucketId.ToString()).Wait();
                }
                catch (Exception)
                {

                }
            }
        }
    }

    [Test, Explicit]
    //[TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsResourceTypeConfigurations))]
    public async Task CREATE_RESOURCE_SUCCESS(ResourceType resourceType)
    {
        // Arrange
        _lastResourceType = resourceType;

        // Start with a clean state
        CleanUp();

        var quota = 5;

        var ds = GetDs(resourceType);
        var ecsManagerFactory = GetEcsFactory(resourceType);

        // Act
        await ds.CreateAsync(new Resource { Id = _testBucketId }, new CreateDataStorageParameters { Quota = quota });

        // Assert
        Assert.That(await ecsManagerFactory.GetRdsS3EcsManager(resourceType.SpecificType).GetBucketQuota(_testBucketId.ToString()) == quota);
    }*/

    /*[Test, Explicit]
    //[TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsResourceTypeConfigurations))]
    public async Task GET_RESOURCE_QUOTA_SUCCESS(ResourceTypeAuth auth)
    {
        // Arrange
        _lastAuth = auth;

        // Start with a clean state
        CleanUp();

        var quota = 5;
        var newQuota = 7;

        var ecsManager = new EcsManager
        {
            EcsManagerConfiguration = _lastAuth.EcsManagerConfiguration
        };

        var resourceTypeDefinition = GetResourceTypeDefinition(auth);

        await resourceTypeDefinition.CreateResource(_testBucketId.ToString(), quota);

        // Act
        await resourceTypeDefinition.SetResourceQuota(_testBucketId.ToString(), newQuota);

        // Assert
        Assert.That(await ecsManager.GetBucketQuota(_testBucketId.ToString()) != quota);
        Assert.That(await ecsManager.GetBucketQuota(_testBucketId.ToString()) == newQuota);
    }

    [Test, Explicit]
    //[TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsResourceTypeConfigurations))]
    //[TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsS3ResourceTypeConfigurations))]
    public async Task STORE_RESOURCE_ENTRY_SUCCESS(ResourceTypeAuth auth)
    {
        // Arrange
        _lastAuth = auth;

        // Start with a clean state
        CleanUp();

        var quota = 5;

        var fileName = "DELETE-ME.txt";

        var memoryStream = new MemoryStream();
        var testData = "CoSIne"u8.ToArray();

        memoryStream.Write(testData, 0, testData.Length);
        memoryStream.Position = 0;

        var ecsManager = new EcsManager
        {
            EcsManagerConfiguration = _lastAuth.EcsManagerConfiguration
        };

        var resourceTypeDefinition = GetResourceTypeDefinition(auth);

        await resourceTypeDefinition.CreateResource(_testBucketId.ToString(), quota);

        // Act
        await resourceTypeDefinition.StoreEntry(_testBucketId.ToString(), fileName, memoryStream);

        // Assert
        Assert.That((await resourceTypeDefinition.ListEntries(_testBucketId.ToString(), "")).Count == 1);

        var entry = await resourceTypeDefinition.GetEntry(_testBucketId.ToString(), fileName);
        Assert.That(entry is not null);
        Assert.That(entry?.HasBody == true);
        Assert.That(entry?.BodyBytes == testData.Length);

        var entryValue = await resourceTypeDefinition.LoadEntry(_testBucketId.ToString(), fileName);

        var length = entryValue is null ? 0 : testData.Length;
        var buffer = new byte[length];
        Assert.That(entryValue?.Read(buffer, 0, buffer.Length) == testData.Length);
        Assert.That(buffer.SequenceEqual(testData));
    }*/
}