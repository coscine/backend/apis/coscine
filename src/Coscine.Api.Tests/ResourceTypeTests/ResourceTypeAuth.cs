using Coscine.ECSManager;

namespace Coscine.Api.Tests.ResourceTypeTests;

public class ResourceTypeAuth
{
    public string? SpecificType { get; set; }
    public string? Type { get; set; }

    public EcsManagerConfiguration? EcsManagerConfiguration { get; set; }

    public string? EcsAccessKey { get; set; }

    public string? EcsSecretKey { get; set; }

    public string? EcsEndpoint { get; set; }

    public EcsManagerConfiguration? UserEcsManagerConfiguration { get; set; }

    public string? EcsUserAccessKey { get; set; }

    public string? EcsUserSecretKey { get; set; }

    public string? EcsUserEndpoint { get; set; }

    public string? S3AccessKey { get; set; }

    public string? S3SecretKey { get; set; }

    public string? S3Endpoint { get; set; }

    public override string ToString()
    {
        return SpecificType ?? "empty";
    }
}