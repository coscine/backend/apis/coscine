using Coscine.ECSManager;
using NUnit.Framework;

namespace Coscine.Api.Tests.ResourceTypeTests;

/*[TestFixture]
public class EcsAuthTests
{
    [Explicit]
    [TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsResourceTypeConfigurations))]
    public void RDS_ECS_Management_Autheticate_Success(ResourceTypeAuth auth)
    {
        // Arrange
        using var client = new CoscineECSManagementClient(auth.EcsAccessKey,
            auth.EcsSecretKey,
            auth.EcsEndpoint);

        // Act and Assert
        Assert.That(client.Authenticate, Throws.Nothing);
        Assert.That(client.LogOut, Throws.Nothing);
    }

    [Explicit]
    [TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsS3ResourceTypeConfigurations))]
    public void RDSS3_ECS_RDS_Management_Autheticate_Success(ResourceTypeAuth auth)
    {
        // Arrange
        using var client = new CoscineECSManagementClient(auth.EcsAccessKey,
            auth.EcsSecretKey,
            auth.EcsEndpoint);

        // Act and Assert
        Assert.That(client.Authenticate, Throws.Nothing);
        Assert.That(client.LogOut, Throws.Nothing);
    }

    [Explicit]
    [TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsS3ResourceTypeConfigurations))]
    public void RDSS3_ECS_USER_Management_Autheticate_Success(ResourceTypeAuth auth)
    {
        // Arrange
        using var client = new CoscineECSManagementClient(auth.EcsUserAccessKey,
            auth.EcsUserSecretKey,
            auth.EcsUserEndpoint);

        // Act and Assert
        Assert.That(client.Authenticate, Throws.Nothing);
        Assert.That(client.LogOut, Throws.Nothing);
    }

    [Explicit]
    [TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsResourceTypeConfigurations))]
    public void RDS_ECS_Management_Autheticate_Fails(ResourceTypeAuth auth)
    {
        // Arrange
        var ecsManagerConfiguration = new EcsManagerConfiguration
        {
            NamespaceAdminName = "TestInvalid",
            NamespaceAdminPassword = "TestWrong",
            ManagerApiEndpoint = auth.EcsEndpoint
        };

        using var client = new CoscineECSManagementClient(ecsManagerConfiguration!.NamespaceAdminName,
            ecsManagerConfiguration!.NamespaceAdminPassword,
            ecsManagerConfiguration!.ManagerApiEndpoint);


        // Act and Assert
        Assert.That(client.Authenticate, Throws.InvalidOperationException);
    }

    [Explicit]
    [TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsS3ResourceTypeConfigurations))]
    public void RDSS3_ECS_RDS_Management_Autheticate_Fails(ResourceTypeAuth auth)
    {
        // Arrange
        var ecsManagerConfiguration = new EcsManagerConfiguration
        {
            NamespaceAdminName = "TestInvalid",
            NamespaceAdminPassword = "TestWrong",
            ManagerApiEndpoint = auth.EcsEndpoint
        };

        using var client = new CoscineECSManagementClient(ecsManagerConfiguration!.NamespaceAdminName,
            ecsManagerConfiguration!.NamespaceAdminPassword,
            ecsManagerConfiguration!.ManagerApiEndpoint);


        // Act and Assert
        Assert.That(client.Authenticate, Throws.InvalidOperationException);
    }

    [Explicit]
    [TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsS3ResourceTypeConfigurations))]
    public void RDSS3_ECS_USER_Management_Autheticate_Fails(ResourceTypeAuth auth)
    {
        // Arrange
        var ecsManagerConfiguration = new EcsManagerConfiguration
        {
            NamespaceAdminName = "TestInvalid",
            NamespaceAdminPassword = "TestWrong",
            ManagerApiEndpoint = auth.EcsEndpoint
        };

        using var client = new CoscineECSManagementClient(ecsManagerConfiguration!.NamespaceAdminName,
            ecsManagerConfiguration!.NamespaceAdminPassword,
            ecsManagerConfiguration!.ManagerApiEndpoint);


        // Act and Assert
        Assert.That(client.Authenticate, Throws.InvalidOperationException);
    }
}
*/