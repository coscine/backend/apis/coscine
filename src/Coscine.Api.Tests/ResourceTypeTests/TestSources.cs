using Coscine.Api.Core.Entities.ConfigurationModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Coscine.Api.Tests.ResourceTypeTests;

/*public class TestSources
{
    public static IEnumerable<ResourceTypeAuth> GetRdsResourceTypeConfigurations()
    {
        var factory = CoscineTestExtensions.CreateFactory();
        using var scope = factory.Services.CreateScope();
        var rdsResourceTypeConfigurations = scope.ServiceProvider.GetRequiredService<IOptionsSnapshot<RdsResourceTypeConfiguration>>();

        var configuration = scope.ServiceProvider.GetRequiredService<IConfiguration>();
        var section = configuration.GetSection("ResourceTypes:rds");

        foreach (var key in section.GetChildren().Select(c => c.Key))
        {
            var type = "rds";
            var specificType = $"{type}{key}";
            var config = rdsResourceTypeConfigurations.Get(specificType);
            yield return new ResourceTypeAuth
            {
                Type = type,
                SpecificType = specificType,
                EcsAccessKey = config.EcsManagerConfiguration.NamespaceAdminName,
                EcsSecretKey = config.EcsManagerConfiguration.NamespaceAdminPassword,
                EcsEndpoint = config.EcsManagerConfiguration.ManagerApiEndpoint,
                EcsManagerConfiguration = config.EcsManagerConfiguration,
                S3AccessKey = config.AccessKey,
                S3SecretKey = config.SecretKey,
                S3Endpoint = config.Endpoint,
            };
        }
    }

    public static IEnumerable<ResourceTypeAuth> GetRdsS3ResourceTypeConfigurations()
    {
        var factory = CoscineTestExtensions.CreateFactory();
        using var scope = factory.Services.CreateScope();
        var rdsResourceTypeConfigurations = scope.ServiceProvider.GetRequiredService<IOptionsSnapshot<RdsS3ResourceTypeConfiguration>>();

        var configuration = scope.ServiceProvider.GetRequiredService<IConfiguration>();
        var section = configuration.GetSection("ResourceTypes:rdss3");

        foreach (var key in section.GetChildren().Select(c => c.Key))
        {
            var type = "rdss3";
            var specificType = $"{type}{key}";
            var config = rdsResourceTypeConfigurations.Get(specificType);
            yield return new ResourceTypeAuth
            {
                Type = type,
                SpecificType = specificType,
                EcsAccessKey = config.RdsS3EcsManagerConfiguration.NamespaceAdminName,
                EcsSecretKey = config.RdsS3EcsManagerConfiguration.NamespaceAdminPassword,
                EcsEndpoint = config.RdsS3EcsManagerConfiguration.ManagerApiEndpoint,
                EcsManagerConfiguration = config.RdsS3EcsManagerConfiguration,
                S3AccessKey = config.AccessKey,
                S3SecretKey = config.SecretKey,
                S3Endpoint = config.Endpoint,
                EcsUserAccessKey = config.UserEcsManagerConfiguration.NamespaceAdminName,
                EcsUserSecretKey = config.UserEcsManagerConfiguration.NamespaceAdminPassword,
                EcsUserEndpoint = config.UserEcsManagerConfiguration.ManagerApiEndpoint,
                UserEcsManagerConfiguration = config.UserEcsManagerConfiguration,
            };
        }
    }
}*/