using Amazon.S3;
using NUnit.Framework;

namespace Coscine.Api.Tests.ResourceTypeTests;

/*[TestFixture]
public class S3AuthTests
{
    [Explicit]
    [TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsResourceTypeConfigurations))]
    [TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsS3ResourceTypeConfigurations))]
    public void S3_Autheticate_Success(ResourceTypeAuth auth)
    {
        // Arrange
        var amazonS3Config = new AmazonS3Config
        {
            ServiceURL = auth.S3Endpoint,
            ForcePathStyle = true
        };

        using var s3client = new AmazonS3Client(auth.S3AccessKey, auth.S3SecretKey, amazonS3Config);

        // Act and Assert
        Assert.That(async () => await s3client.ListBucketsAsync(), Throws.Nothing);
    }

    [Explicit]
    [TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsResourceTypeConfigurations))]
    [TestCaseSource(typeof(TestSources), nameof(TestSources.GetRdsS3ResourceTypeConfigurations))]
    public void S3_Autheticate_Fails(ResourceTypeAuth auth)
    {
        // Arrange
        var amazonS3Config = new AmazonS3Config
        {
            ServiceURL = auth.S3Endpoint,
            ForcePathStyle = true
        };

        using var s3client = new AmazonS3Client("TestInvalid", "TestWrong", amazonS3Config);

        // Act and Assert
        Assert.That(async () => await s3client.ListBucketsAsync(), Throws.Exception.TypeOf<AmazonS3Exception>());
    }
}
*/