﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.QueryParameters;
using Coscine.Api.Tests.Mockups.Data;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Security.Cryptography;

namespace Coscine.Api.Tests;

internal static class DatabaseSeeder
{
    private static readonly Random rnd = new();

    public static User TestUser =>
        new()
        {
            Id = Guid.Parse("b3b56d17-6991-4086-acd1-51e982b07465"),
            DisplayName = "Anakin Skywalker",
            Givenname = "Anakin",
            Surname = "Skywalker",
            EmailAddress = "anakin.jedi-knight@council.jedi.com",
            Organization = "Jedi Council",
            LanguageId = null,
            TitleId = null
        };

    public static ApiToken TestApiToken =>
        new()
        {
            Id = Guid.Parse("0b8c6e41-d942-4850-bf96-99abb517dbbc"),
            Expiration = DateTime.UtcNow.AddDays(1),
            IssuedAt = DateTime.UtcNow,
            Name = "Test Token",
            UserId = TestUser.Id,
        };

    public static Role Owner =>
        new()
        {
            Id = Guid.Parse("BE294C5E-4E42-49B3-BEC4-4B15F49DF9A5"),
            DisplayName = "Owner",
            Description = "Owner"
        };

    public static Role Member =>
        new()
        {
            Id = Guid.Parse("508B6D4E-C6AC-4AA5-8A8D-CAA31DD39527"),
            DisplayName = "Member",
            Description = "Member"
        };

    public static Role Guest =>
        new()
        {
            Id = Guid.Parse("9184A442-4419-4E30-9FE6-0CFE32C9A81F"),
            DisplayName = "Guest",
            Description = "Guest"
        };

    public static async Task SeedTestUserAsync(this WebApplicationFactory<Program> factory)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();
        var tosService = serviceProvider.GetRequiredService<ITosService>();

        // Add information for the test user and the used API token for authentication.
        await dbContext.Users.AddAsync(TestUser);
        await dbContext.ApiTokens.AddAsync(TestApiToken);
        await dbContext.Tosaccepteds.AddAsync(
            new Tosaccepted { UserId = TestUser.Id, Version = tosService.GetCurrentTosVersion().Version }
        );

        await dbContext.SaveChangesAsync();
    }

    public static async Task<User> SeedUserAsync(this WebApplicationFactory<Program> factory, Guid? userId = null)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var user = new User()
        {
            Id = userId ?? Guid.NewGuid(),
            DisplayName = "Test User",
            Givenname = "Test",
            Surname = "User",
            EmailAddress = "test@user.de",
            Organization = "Test Council",
        };
        await dbContext.Users.AddAsync(user);

        await dbContext.SaveChangesAsync();

        return user;
    }

    public static async Task<ApiToken> SeedApiTokenAsync(
        this WebApplicationFactory<Program> factory,
        Guid userId,
        Guid? tokenId = null
    )
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var token = new ApiToken()
        {
            Id = tokenId ?? Guid.NewGuid(),
            Expiration = DateTime.UtcNow.AddDays(1),
            IssuedAt = DateTime.UtcNow,
            Name = "Test Token",
            UserId = userId,
        };
        await dbContext.ApiTokens.AddAsync(token);

        await dbContext.SaveChangesAsync();

        return token;
    }

    public static async Task<IEnumerable<Discipline>> SeedDisciplinesAsync(this WebApplicationFactory<Program> factory)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var disciplines = new List<Discipline>();

        for (int i = 0; i < 3; i++)
        {
            var discipline = new Discipline
            {
                DisplayNameEn = $"Test Discipline {i}",
                DisplayNameDe = $"Test Disziplin {i}",
                Url = "http://testUrl.com/{i}"
            };

            await dbContext.Disciplines.AddAsync(discipline);

            disciplines.Add(discipline);
        }

        await dbContext.SaveChangesAsync();

        return disciplines;
    }

    public static async Task<IEnumerable<Language>> SeedLanguagesAsync(this WebApplicationFactory<Program> factory)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var languages = new List<Language>();

        for (int i = 0; i < 3; i++)
        {
            var language = new Language { DisplayName = $"Galactic Basic {i + 1}", Abbreviation = $"GB{i + 1}" };

            await dbContext.Languages.AddAsync(language);

            languages.Add(language);
        }

        await dbContext.SaveChangesAsync();

        return languages;
    }

    public static async Task<IEnumerable<Visibility>> SeedVisibilitiesAsync(this WebApplicationFactory<Program> factory)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var visibilities = new List<Visibility>();

        for (int i = 0; i < 3; i++)
        {
            var visibility = new Visibility { DisplayName = $"Test {i}" };

            await dbContext.Visibilities.AddAsync(visibility);

            visibilities.Add(visibility);
        }

        await dbContext.SaveChangesAsync();

        return visibilities;
    }

    public static async Task<IEnumerable<Role>> SeedRolesAsync(this WebApplicationFactory<Program> factory)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var roles = new List<Role>();

        for (int i = 0; i < 3; i++)
        {
            var role = new Role { DisplayName = $"Test {i}", Description = $"Test {i}" };

            await dbContext.Roles.AddAsync(role);

            roles.Add(role);
        }

        await dbContext.SaveChangesAsync();

        return roles;
    }

    public static async Task<IEnumerable<Title>> SeedTitlesAsync(this WebApplicationFactory<Program> factory)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var titles = new List<Title>();

        for (int i = 0; i < 3; i++)
        {
            // Let's show him what a disturbance in the Force really is!
            string stormtrooperNumber = $"TK-{i + 1:5500}";
            var title = new Title { DisplayName = $"Stormtrooper {stormtrooperNumber}" };

            await dbContext.Titles.AddAsync(title);

            titles.Add(title);
        }

        await dbContext.SaveChangesAsync();

        return titles;
    }

    public static async Task<IEnumerable<License>> SeedLicensesAsync(this WebApplicationFactory<Program> factory)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var licenses = new List<License>();

        for (int i = 0; i < 3; i++)
        {
            var license = new License { DisplayName = $"Test {i}" };

            await dbContext.Licenses.AddAsync(license);

            licenses.Add(license);
        }

        await dbContext.SaveChangesAsync();

        return licenses;
    }

    public static async Task<IEnumerable<Project>> SeedProjectsAsync(
        this WebApplicationFactory<Program> factory,
        Guid? userId = null,
        int? amount = null
    )
    {
        var visibilities = (await factory.SeedVisibilitiesAsync()).ToList();
        var disciplines = (await factory.SeedDisciplinesAsync()).ToList();

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var projects = new List<Project>();

        for (int i = 0; i < (amount ?? 3); i++)
        {
            var project = new Project
            {
                ProjectName = $"Test Project {i}",
                Description = $"This is a test project {i}",
                StartDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(7),
                Keywords = "a Keyword",
                DisplayName = $"Test Project {i}",
                PrincipleInvestigators = "Inv",
                GrantId = "Grant_123",
                VisibilityId = visibilities[rnd.Next(visibilities.Count)].Id,
                Deleted = false,
                Slug = $"Test Project {i}",
                DateCreated = DateTime.UtcNow,
                Creator = userId ?? TestUser.Id
            };

            project.ProjectDisciplines.Add(
                new ProjectDiscipline { DisciplineId = disciplines[rnd.Next(disciplines.Count)].Id, Project = project }
            );

            project.ProjectInstitutes.Add(new ProjectInstitute { OrganizationUrl = "http://testUrl.com/", Project = project });

            project.ProjectRoles.Add(
                new ProjectRole
                {
                    RoleId = Owner.Id,
                    Project = project,
                    UserId = userId ?? TestUser.Id
                }
            );

            await dbContext.Projects.AddAsync(project);

            projects.Add(project);
        }

        await dbContext.SaveChangesAsync();

        return projects;
    }

    public static async Task<IEnumerable<ProjectPublicationRequest>> SeedProjectPublicationRequestsAsync(
        this WebApplicationFactory<Program> factory,
        Guid? userId = null,
        int? amount = null
    )
    {
        var resources = (await factory.SeedResourcesAsync()).ToList();

        var resource = resources.First();
        var projectId = resource.ProjectResources.First().ProjectId;

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var projectPublicationRequests = new List<ProjectPublicationRequest>();

        for (int i = 0; i < (amount ?? 3); i++)
        {
            var projectPublicationRequest = new ProjectPublicationRequest
            {
                ProjectId = projectId,
                Resources = [resource],
                CreatorId = userId ?? TestUser.Id,
                DateCreated = DateTime.UtcNow,
                Message = $"Test Message {i}",
                PublicationServiceRorId = new Uri("https://test.com/rorUri", UriKind.Absolute),
            };

            dbContext.Resources.Attach(resource); // Attach the resource to the context to avoid EF Core trying to add it again.
            await dbContext.ProjectPublicationRequests.AddAsync(projectPublicationRequest);
        };

        await dbContext.SaveChangesAsync();

        // Load the project publication requests from the database to include the related objects.
        return await dbContext.ProjectPublicationRequests
            .Include(ppr => ppr.Project)
            .ToListAsync();
    }

    public static async Task<IEnumerable<Invitation>> SeedProjectInvitationsAsync(
        this WebApplicationFactory<Program> factory,
        Guid? issuerId = null,
        IEnumerable<Project>? projects = null,
        int? amount = null,
        bool randomInvitee = false
    )
    {
        var internalPojects = projects ?? await factory.SeedProjectsAsync(issuerId);

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var invitations = new List<Invitation>();
        foreach (var p in internalPojects)
        {
            for (int i = 0; i < (amount ?? 3); i++)
            {
                var invitation = new Invitation
                {
                    Project = p.Id,
                    InviteeEmail = randomInvitee ? $"test_{i}_{Guid.NewGuid}@example.com" : $"test_{i}@example.com",
                    Issuer = issuerId ?? TestUser.Id,
                    Expiration = DateTime.UtcNow.AddDays(7),
                    Token = Guid.NewGuid(),
                    Role = Owner.Id,
                };

                await dbContext.Invitations.AddAsync(invitation);

                invitations.Add(invitation);
            }
        }

        await dbContext.SaveChangesAsync();

        return invitations;
    }

    public static async Task<IEnumerable<User>> SeedUsersAsync(this WebApplicationFactory<Program> factory, int? amount = null)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var users = new List<User>();

        for (int i = 0; i < (amount ?? 3); i++)
        {
            var user = new User()
            {
                Id = Guid.NewGuid(),
                DisplayName = $"Test User {i}",
                Givenname = "Test",
                Surname = $"User {i}",
                EmailAddress = "test@test.de",
                Organization = "Test",
            };

            await dbContext.Users.AddAsync(user);

            users.Add(user);
        }

        await dbContext.SaveChangesAsync();

        return users;
    }

    public static async Task<ProjectRole> SeedProjectRoleAsync(
        this WebApplicationFactory<Program> factory,
        Guid roleId,
        Guid projectId,
        Guid UserId
    )
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var projectRole = new ProjectRole
        {
            RoleId = roleId,
            ProjectId = projectId,
            UserId = UserId
        };

        dbContext.ProjectRoles.Add(projectRole);

        await dbContext.SaveChangesAsync();

        return projectRole;
    }

    public static async Task<IEnumerable<ResourceType>> SeedResourceTypesAsync(this WebApplicationFactory<Program> factory)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var resourceTypes = new List<ResourceType>();

        for (int i = 0; i < 3; i++)
        {
            var resourceType = new ResourceType()
            {
                DisplayName = $"Type: {i}",
                SpecificType = $"Specific Type: {i}"
            };

            await dbContext.ResourceTypes.AddAsync(resourceType);

            resourceTypes.Add(resourceType);
        }

        var linkedResourceType = new ResourceType()
        {
            DisplayName = $"Linked",
            SpecificType = $"linked",
            Type = "linked"
        };
        await dbContext.ResourceTypes.AddAsync(linkedResourceType);

        resourceTypes.Add(linkedResourceType);

        await dbContext.SaveChangesAsync();

        return resourceTypes;
    }

    public static async Task<ResourceType> SeedFileStorageResourceTypeAsync(this WebApplicationFactory<Program> factory)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var localStorageResourceType = ResourceTypeConfiguration.LocalResourceType;

        await dbContext.ResourceTypes.AddAsync(localStorageResourceType);

        await dbContext.SaveChangesAsync();

        return localStorageResourceType;
    }

    public static async Task<Resource> SeedResourceAsync(this WebApplicationFactory<Program> factory,
                                                         string? fixedValues = "{}",
                                                         Guid? resourceId = null,
                                                         ResourceType? resourceType = null,
                                                         CreateDataStorageParameters? createDataStorageParameters = null)
    {
        var projects = (await factory.SeedProjectsAsync()).ToList();
        var licences = (await factory.SeedLicensesAsync()).ToList();
        resourceType ??= await factory.SeedFileStorageResourceTypeAsync();

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var project = projects[rnd.Next(projects.Count)];

        var resource = new Resource
        {
            Id = resourceId ?? Guid.NewGuid(),
            ResourceName = "Test Resource",
            DisplayName = "Test Resource",
            Description = "This is a test resource",
            Keywords = "one;two;three;",
            VisibilityId = project.VisibilityId,
            LicenseId = licences[rnd.Next(licences.Count)].Id,
            UsageRights = "Test Usage Rights",
            FixedValues = fixedValues,
            ApplicationProfile = "https://purl.org/coscine/ap/base/",
            Deleted = false,
            DateCreated = DateTime.UtcNow,
            Creator = project.Creator,
            Archived = "0",
            ResourceTypeOptionId = Guid.NewGuid(),
        };

        resource.ResourceDisciplines.Add(
            new ResourceDiscipline { DisciplineId = project.ProjectDisciplines.First().DisciplineId, Resource = resource }
        );

        resource.ProjectResources.Add(new ProjectResource { ProjectId = project.Id, Resource = resource });

        resource.TypeId = resourceType.Id;

        await dbContext.Resources.AddAsync(resource);

        await dbContext.MetadataExtractions.AddAsync(new MetadataExtraction()
        {
            Activated = true,
            ResourceId = resource.Id
        });

        if (createDataStorageParameters is not null)
        {
            var ds = serviceProvider.GetRequiredService<IDataStorageRepositoryFactory>().Create(resource.TypeId)!;

            await ds.CreateAsync(resource, createDataStorageParameters);
        }

        await dbContext.SaveChangesAsync();

        var rdfRepositoryBase = serviceProvider.GetRequiredService<IRdfRepositoryBase>();
        var baseApplicationProfileGraph = rdfRepositoryBase.ParseGraph(
            ApplicationProfileMock.BASE_APPLICATION_PROFILE,
            RdfFormat.Turtle,
            new Uri("https://purl.org/coscine/ap/base/")
        );
        await rdfRepositoryBase.AddGraphAsync(baseApplicationProfileGraph);

        return resource;
    }

    public static async Task<IEnumerable<Resource>> SeedResourcesAsync(this WebApplicationFactory<Program> factory,
                                                                       string? fixedValues = "{}",
                                                                       int count = 4,
                                                                       ResourceType? resourceType = null,
                                                                       CreateDataStorageParameters? createDataStorageParameters = null)
    {
        var projects = (await factory.SeedProjectsAsync()).ToList();
        var licences = (await factory.SeedLicensesAsync()).ToList();
        resourceType ??= await factory.SeedFileStorageResourceTypeAsync();

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var resources = new List<Resource>();

        for (int i = 0; i < count; i++)
        {
            var project = projects[rnd.Next(projects.Count)];
            var resource = new Resource
            {
                Id = Guid.NewGuid(),
                ResourceName = "Test Resource",
                DisplayName = "Test Resource",
                Description = "This is a test resource",
                Keywords = "one;two;three;",
                VisibilityId = project.VisibilityId,
                LicenseId = licences[rnd.Next(licences.Count)].Id,
                UsageRights = "Test Usage Rights",
                FixedValues = fixedValues,
                ApplicationProfile = "https://purl.org/coscine/ap/base/",
                Deleted = false,
                DateCreated = DateTime.UtcNow,
                Creator = project.Creator,
                Archived = "0",
                ResourceTypeOptionId = Guid.NewGuid(),
            };

            resource.ResourceDisciplines.Add(
                new ResourceDiscipline { DisciplineId = project.ProjectDisciplines.First().DisciplineId, Resource = resource }
            );

            resource.ProjectResources.Add(new ProjectResource { ProjectId = project.Id, Resource = resource });

            resource.TypeId = resourceType.Id;

            await dbContext.Resources.AddAsync(resource);

            await dbContext.MetadataExtractions.AddAsync(new MetadataExtraction()
            {
                Activated = true,
                ResourceId = resource.Id
            });

            if (createDataStorageParameters is not null)
            {
                var ds = serviceProvider.GetRequiredService<IDataStorageRepositoryFactory>().Create(resource.TypeId)!;

                await ds.CreateAsync(resource, createDataStorageParameters);
            }

            resources.Add(resource);
        }

        await dbContext.SaveChangesAsync();

        var rdfRepositoryBase = serviceProvider.GetRequiredService<IRdfRepositoryBase>();
        var baseApplicationProfileGraph = rdfRepositoryBase.ParseGraph(
            ApplicationProfileMock.BASE_APPLICATION_PROFILE,
            RdfFormat.Turtle,
            new Uri("https://purl.org/coscine/ap/base/")
        );
        await rdfRepositoryBase.AddGraphAsync(baseApplicationProfileGraph);

        return resources;
    }

    public static async Task<Vocabulary> SeedVocabualaryAsync(this WebApplicationFactory<Program> factory)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;

        var rdfRepositoryBase = serviceProvider.GetRequiredService<IRdfRepositoryBase>();
        var baseVocabularyGraph = rdfRepositoryBase.ParseGraph(
            VocabularyMock.BASE_VOCABULARY,
            Core.Shared.Enums.RdfFormat.Turtle,
            new Uri("http://purl.org/dc/dcmitype/")
        );
        await rdfRepositoryBase.AddGraphAsync(baseVocabularyGraph);
        var vocabulary = new Vocabulary()
        {
            GraphUri = baseVocabularyGraph.BaseUri,
            Description = "Testing",
            DisplayName = "Test",
            ClassUri = new Uri("http://purl.org/dc/dcmitype/")
        };
        return vocabulary;
    }

    public static async Task<ApplicationProfile> SeedApplicationProfileAsync(this WebApplicationFactory<Program> factory)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;

        var rdfRepositoryBase = serviceProvider.GetRequiredService<IRdfRepositoryBase>();
        var baseApplicationProfileGraph = rdfRepositoryBase.ParseGraph(
            ApplicationProfileMock.BASE_APPLICATION_PROFILE,
            Core.Shared.Enums.RdfFormat.Turtle,
            new Uri("https://purl.org/coscine/ap/base/")
        );
        await rdfRepositoryBase.AddGraphAsync(baseApplicationProfileGraph);

        var applicationProfile = new ApplicationProfile()
        {
            Uri = baseApplicationProfileGraph.BaseUri,
            DisplayName = "Test",
            Description = "Testing",
            Format = Core.Shared.Enums.RdfFormat.Turtle,
        };
        return applicationProfile;
    }
    public static async Task<(Guid, MetadataTree)> SeedMetadataTreeAsync(this WebApplicationFactory<Program> factory)
    {
        const string key = "test.txt";

        var resources = await factory.SeedResourcesAsync();

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var treeRepository = serviceProvider.GetRequiredService<ITreeRepository>();

        var projectId = resources.First().ProjectResources.First().ProjectId;
        var resource = resources.First();
        var resourceUriString = $"https://purl.org/coscine/resources/{resource.Id}/{key}";

        var metadataTree = new MetadataTree
        {
            Definition = MetadataMock.GetTemplatedMetadata(resourceUriString),
            Format = RdfFormat.Turtle,
            Path = key
        };

        var graph = await treeRepository.CreateMetadataGraphAsync(resource, TestUser, metadataTree);

        metadataTree = await treeRepository.GetMetadataAsync(resource.Id, new MetadataTreeQueryParameters
        {
            Path = key,
            Format = RdfFormat.Turtle,
        });

        return (projectId, metadataTree!);
    }

    public static async Task<(Guid, MetadataTree)> SeedExtractedMetadataTreeAsync(this WebApplicationFactory<Program> factory)
    {
        const string key = "test.txt";

        var resources = await factory.SeedResourcesAsync();

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var treeRepository = serviceProvider.GetRequiredService<ITreeRepository>();

        var projectId = resources.First().ProjectResources.First().ProjectId;
        var resource = resources.First();
        var resourceUriString = $"https://purl.org/coscine/resources/{resource.Id}/{key}";
        Uri id = new($"{resourceUriString}/@type=metadata&version=1234");

        var metadataTree = new MetadataTree
        {
            Id = id,
            Extracted = new MetadataTreeExtracted
            {
                Definition = MetadataMock.GetExtractedMetadata(resourceUriString),
                MetadataId = id,
                RawDataId = new Uri(id.AbsoluteUri.Replace("type=metadata", "type=data")),
                Format = RdfFormat.TriG,
            },
            SkipValidation = true,
            Provenance = new Provenance
            {
                Id = id,
                HashParameters = new HashParameters
                {
                    AlgorithmName = new HashAlgorithmName("SHA512"),
                    Value = "0"
                },
                WasRevisionOf = [],
                Variants = []
            },
            Path = key
        };

        var graph = await treeRepository.CreateMetadataGraphAsync(resource, TestUser, metadataTree);

        metadataTree = await treeRepository.GetMetadataAsync(resource.Id, new MetadataTreeQueryParameters
        {
            Path = key,
            Format = RdfFormat.Turtle,
            IncludeExtractedMetadata = true,
            IncludeProvenance = true
        });

        return (projectId, metadataTree!);
    }

    public static async Task<IEnumerable<ActivityLog>> SeedActivityLogsAsync(this WebApplicationFactory<Program> factory, int? amount = null, Guid? userId = null)
    {
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var activityLogs = new List<ActivityLog>();
        var user = await dbContext.Users.FindAsync(userId ?? Guid.NewGuid()) ?? throw new ArgumentException("User not found", nameof(userId));

        for (int i = 0; i < (amount ?? 3); i++)
        {
            var activityLog = new ActivityLog()
            {
                Id = Guid.NewGuid(),
                ApiPath = $"/api/test/endpoint/{i}",
                HttpAction = "GET",
                ControllerName = "TestController",
                ActionName = $"Action{i}",
                UserId = user.Id,
                ActivityTimestamp = DateTime.UtcNow,
                User = user
            };

            await dbContext.ActivityLogs.AddAsync(activityLog);
            activityLogs.Add(activityLog);
        }

        await dbContext.SaveChangesAsync();

        return activityLogs;
    }

    public static async Task SeedLinkedDataEntryAsync(this WebApplicationFactory<Program> factory, string fileContent, Uri uri)
    {
        // Add a linked data graph entry to the memory store
        var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var rdfRepositoryBase = serviceProvider.GetRequiredService<IRdfRepositoryBase>();
        var linkedData = rdfRepositoryBase.ParseGraph(@$"<{uri}> <{RdfUris.CoscineTermsLinkedBody}>  ""{fileContent}"" .", RdfFormat.Turtle, uri);
        await rdfRepositoryBase.AddGraphAsync(linkedData);
    }
}
