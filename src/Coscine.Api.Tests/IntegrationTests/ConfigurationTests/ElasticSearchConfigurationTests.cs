﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using System.Text;
using System.Text.Json;

namespace Coscine.Api.Tests.IntegrationTests.ConfigurationTests;


[TestFixture]
public class ElasticSearchConfigurationTests : CoscineControllerTestsBase
{
    private static T GetConfig<T>(string section)
    {
        var factory = CoscineTestExtensions.CreateFactory();
        var config = factory.Services.GetRequiredService<IConfiguration>().GetSection(section);

        return config.Get<T>() ?? throw new KeyNotFoundException();
    }

    [Test]
    public void Add_Configuration_Endpoint_NotSpecified()
    {
        var factory = CoscineTestExtensions
            .CreateFactory()
            .WithInMemoryConfiguration(configuration =>
            {
                var elasticSearchConfiguration = new
                {
                    elasticSearchConfiguration = GetConfig<ElasticSearchConfiguration>(ElasticSearchConfiguration.Section)
                };

                elasticSearchConfiguration.elasticSearchConfiguration.Endpoint = null!;

                string json = JsonSerializer.Serialize(elasticSearchConfiguration);
                configuration.AddJsonStream(new MemoryStream(Encoding.UTF8.GetBytes(json)));
            });

        // Act and Assert
        Assert.ThrowsAsync<OptionsValidationException>(() => InitializeClientAsync(factory));
    }

    [Test]
    public void Add_Configuration_DefaultIndex_NotSpecified()
    {
        var factory = CoscineTestExtensions
            .CreateFactory()
            .WithInMemoryConfiguration(configuration =>
            {
                var elasticSearchConfiguration = new
                {
                    elasticSearchConfiguration = GetConfig<ElasticSearchConfiguration>(ElasticSearchConfiguration.Section)
                };

                elasticSearchConfiguration.elasticSearchConfiguration.DefaultIndex = null!;

                string json = JsonSerializer.Serialize(elasticSearchConfiguration);
                configuration.AddJsonStream(new MemoryStream(Encoding.UTF8.GetBytes(json)));
            });

        // Act and Assert
        Assert.ThrowsAsync<OptionsValidationException>(() => InitializeClientAsync(factory));
    }

    [Test]
    public void Add_Configuration_Valid()
    {
        var factory = CoscineTestExtensions
            .CreateFactory()
            .WithInMemoryConfiguration(configuration =>
            {
                var elasticSearchConfiguration = new
                {
                    elasticSearchConfiguration = GetConfig<ElasticSearchConfiguration>(ElasticSearchConfiguration.Section)
                };

                elasticSearchConfiguration.elasticSearchConfiguration.Endpoint = new Uri("https://example.com");
                elasticSearchConfiguration.elasticSearchConfiguration.DefaultIndex = "index";

                string json = JsonSerializer.Serialize(elasticSearchConfiguration);
                configuration.AddJsonStream(new MemoryStream(Encoding.UTF8.GetBytes(json)));
            });

        // Act and Assert
        Assert.DoesNotThrowAsync(() => InitializeClientAsync(factory));
    }
}
