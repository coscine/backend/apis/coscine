﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using System.Text;
using System.Text.Json;

namespace Coscine.Api.Tests.IntegrationTests.ConfigurationTests;

public class ActivityLogConfigurationTests : CoscineControllerTestsBase
{
    private static T GetConfig<T>(string section)
    {
        var factory = CoscineTestExtensions.CreateFactory();
        var config = factory.Services.GetRequiredService<IConfiguration>().GetSection(section);

        return config.Get<T>() ?? throw new KeyNotFoundException();
    }

    [Test]
    public void Add_Configuration_RetentionDays_Valid()
    {
        var factory = CoscineTestExtensions
            .CreateFactory()
            .WithInMemoryConfiguration(configuration =>
            {
                var activityLogConfiguration = new
                {
                    activityLogConfiguration = GetConfig<ActivityLogConfiguration>(ActivityLogConfiguration.Section)
                };

                activityLogConfiguration.activityLogConfiguration.RetentionDays = 10; // Within valid range

                string json = JsonSerializer.Serialize(activityLogConfiguration);
                configuration.AddJsonStream(new MemoryStream(Encoding.UTF8.GetBytes(json)));
            });

        // Act and Assert
        Assert.DoesNotThrowAsync(() => InitializeClientAsync(factory));
    }

    [Test]
    public void Add_Configuration_RetentionDays_LowerBound_OutOfRange()
    {
        var factory = CoscineTestExtensions
            .CreateFactory()
            .WithInMemoryConfiguration(configuration =>
            {
                var activityLogConfiguration = new
                {
                    activityLogConfiguration = GetConfig<ActivityLogConfiguration>(ActivityLogConfiguration.Section)
                };

                activityLogConfiguration.activityLogConfiguration.RetentionDays = 6; // Below lower bound

                string json = JsonSerializer.Serialize(activityLogConfiguration);
                configuration.AddJsonStream(new MemoryStream(Encoding.UTF8.GetBytes(json)));
            });

        // Act and Assert
        Assert.ThrowsAsync<OptionsValidationException>(() => InitializeClientAsync(factory));
    }

    [Test]
    public void Add_Configuration_RetentionDays_UpperBound_OutOfRange()
    {
        var factory = CoscineTestExtensions
            .CreateFactory()
            .WithInMemoryConfiguration(configuration =>
            {
                var activityLogConfiguration = new
                {
                    activityLogConfiguration = GetConfig<ActivityLogConfiguration>(ActivityLogConfiguration.Section)
                };

                activityLogConfiguration.activityLogConfiguration.RetentionDays = 15; // Above upper bound

                string json = JsonSerializer.Serialize(activityLogConfiguration);
                configuration.AddJsonStream(new MemoryStream(Encoding.UTF8.GetBytes(json)));
            });

        // Act and Assert
        Assert.ThrowsAsync<OptionsValidationException>(() => InitializeClientAsync(factory));
    }
}
