﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NUnit.Framework;
using System.Text;
using System.Text.Json;

namespace Coscine.Api.Tests.IntegrationTests.ConfigurationTests;


[TestFixture]
public class JwtConfigurationTests : CoscineControllerTestsBase
{
    private static T GetConfig<T>(string section)
    {
        var factory = CoscineTestExtensions.CreateFactory();
        var config = factory.Services.GetRequiredService<IConfiguration>().GetSection(section);

        return config.Get<T>() ?? throw new KeyNotFoundException();
    }

    [Test]
    public void Add_Configuration_Valid()
    {
        var factory = CoscineTestExtensions
            .CreateFactory()
            .WithInMemoryConfiguration(configuration =>
            {
                var jwtConfiguration = new { jwtConfiguration = GetConfig<JwtConfiguration>(JwtConfiguration.Section) };

                jwtConfiguration.jwtConfiguration.ValidIssuers = ["issuer1", "issuer2"];
                jwtConfiguration.jwtConfiguration.ValidAudiences = ["audience1", "audience2"];
                jwtConfiguration.jwtConfiguration.ExpiresInMinutes = 60; // Within valid range
                jwtConfiguration.jwtConfiguration.JsonWebKeys = [new JsonWebKey()];

                string json = JsonSerializer.Serialize(jwtConfiguration);
                configuration.AddJsonStream(new MemoryStream(Encoding.UTF8.GetBytes(json)));
            });

        // Act and Assert
        Assert.DoesNotThrowAsync(() => InitializeClientAsync(factory));
    }

    [Test]
    public void Add_Configuration_ExpiresInMinutes_OutOfRange()
    {
        var factory = CoscineTestExtensions
            .CreateFactory()
            .WithInMemoryConfiguration(configuration =>
            {
                var jwtConfiguration = new { jwtConfiguration = GetConfig<JwtConfiguration>(JwtConfiguration.Section) };

                jwtConfiguration.jwtConfiguration.ExpiresInMinutes = 0; // Below lower bound

                string json = JsonSerializer.Serialize(jwtConfiguration);
                configuration.AddJsonStream(new MemoryStream(Encoding.UTF8.GetBytes(json)));
            });

        // Act and Assert
        Assert.ThrowsAsync<OptionsValidationException>(() => InitializeClientAsync(factory));
    }
}
