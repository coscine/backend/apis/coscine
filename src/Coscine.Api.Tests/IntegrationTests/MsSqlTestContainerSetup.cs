using DotNet.Testcontainers.Builders;
using NUnit.Framework;
using Testcontainers.MsSql;

namespace Coscine.Api.Tests.IntegrationTests;

[Explicit]
[SetUpFixture]
public class MsSqlTestContainerSetup
{
    private static MsSqlContainer? _container;
    public static string SaPassword { get; } = "testtest!123";
    public static string DbUser { get; } = "coscinetest";
    public static string DbPassword { get; } = "testpassword!123";
    public static string UserConnectionString { get; private set; } = null!;
    public static string SaConnectionString { get; private set; } = null!;
    private static bool IsGitLabCi => Environment.GetEnvironmentVariable("CI") == "true";
    private static int Port => 1433;
    private static string NetworkName => "coscinedevcontainer_default";

    [OneTimeSetUp]
    public async Task OneTimeSetup()
    {
        _container = new MsSqlBuilder()
            .WithImage("mcr.microsoft.com/mssql/server:2022-latest")
            .WithEnvironment("ACCEPT_EULA", "Y")
            .WithEnvironment("MSSQL_PID", "Developer")
            .WithEnvironment("MSSQL_SA_PASSWORD", SaPassword)
            .WithPortBinding(Port, IsGitLabCi) // Binds a random local port
            .WithWaitStrategy(Wait.ForUnixContainer().UntilPortIsAvailable(1433))
            // Use the appropriate network when not in CI.
            .ConfigureNetwork(IsGitLabCi, NetworkName)
            .Build();

        // Register a shutdown hook to stop the container.
        // We can't use Ryuk sidecar with podman, so we use this crutch.
        AppDomain.CurrentDomain.ProcessExit += async (_, __) =>
        {
            try
            {
                await _container.StopAsync();
                await _container.DisposeAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error stopping container: {ex.Message}");
            }
        };

        await _container.StartAsync();

        if (IsGitLabCi)
        {
            SaConnectionString = $"Data Source={_container.Hostname},{_container.GetMappedPublicPort(Port)};Integrated Security=False;TrustServerCertificate=True;User ID=SA;Password={SaPassword};Database=master";
            UserConnectionString = $"Data Source={_container.Hostname},{_container.GetMappedPublicPort(Port)};Integrated Security=False;TrustServerCertificate=True;User ID={DbUser};Password={DbPassword};Database=coscine";
        }
        else
        {
            SaConnectionString = $"Data Source={_container!.Name.TrimStart('/')},{Port};Integrated Security=False;TrustServerCertificate=True;User ID=SA;Password={SaPassword};Database=master";
            UserConnectionString = $"Data Source={_container!.Name.TrimStart('/')},{Port};Integrated Security=False;TrustServerCertificate=True;User ID={DbUser};Password={DbPassword};Database=coscine";
        }
    }

    [OneTimeTearDown]
    public async Task OneTimeTearDown()
    {
        if (_container != null)
        {
            await _container.StopAsync();
            await _container.DisposeAsync();
        }
    }
}


// Extension to configure the network based on environment.
public static class MsSqlBuilderExtensions
{
    public static MsSqlBuilder ConfigureNetwork(this MsSqlBuilder builder, bool useNetwork, string network)
    {
        if (!useNetwork)
        {
            return builder.WithNetwork(network);
        }
        return builder;
    }
}
