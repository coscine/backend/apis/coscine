﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.QueryParameters;
using NSubstitute;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class ProvenanceControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetSpecificProvenance_ReturnsOk()
    {
        // Arrange

        const string key = "test.txt";
        Uri id = new("https://www.example.com");
        var generatedAt = DateTime.Now;
        var metadataExtractorVersion = "2";
        var hashParameters = new HashParameters() { AlgorithmName = System.Security.Cryptography.HashAlgorithmName.SHA512, Value = "9" };
        Uri revision = new("https://www.example.com/1");
        Uri variant = new("https://www.example.com/2");
        var similarityToLastVersion = 0.94m;

        var treeRepositoryMock = Substitute.For<ITreeRepository>();
        treeRepositoryMock
            .GetMetadataAsync(Arg.Any<Guid>(), Arg.Any<MetadataTreeQueryParameters>())
            .Returns(Task.FromResult<MetadataTree?>(new MetadataTree
            {
                Path = key,
                Provenance = new Provenance()
                {
                    Id = id,
                    GeneratedAt = generatedAt,
                    MetadataExtractorVersion = metadataExtractorVersion,
                    HashParameters = hashParameters,
                    Variants = [new Provenance.Variant(variant, 1)],
                    WasRevisionOf = [revision],
                    SimilarityToLastVersion = similarityToLastVersion
                }
            }));

        var factory = CoscineTestExtensions.CreateFactory()
           .WithServiceInstance(treeRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var resources = await factory.SeedResourcesAsync();

        var projectId = resources.First().ProjectResources.First().ProjectId;
        var resource = resources.First();

        // Act
        var response = await client.GetAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/provenance/specific?Path=text.txt");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<ProvenanceDto>>();

        Assert.That(content.Data, Is.Not.Null);
        Assert.That(content.Data?.Id, Is.EqualTo(id));
        Assert.That(content.Data?.GeneratedAt, Is.EqualTo(generatedAt));
        Assert.That(content.Data?.HashParameters?.AlgorithmName, Is.EqualTo(hashParameters.AlgorithmName.Name));
        Assert.That(content.Data?.HashParameters?.Value, Is.EqualTo(hashParameters.Value));
        Assert.That(content.Data?.Variants?.FirstOrDefault()?.GraphName, Is.EqualTo(variant));
        Assert.That(content.Data?.WasRevisionOf?.FirstOrDefault(), Is.EqualTo(revision));
        Assert.That(content.Data?.SimilarityToLastVersion, Is.EqualTo(similarityToLastVersion));
    }
}
