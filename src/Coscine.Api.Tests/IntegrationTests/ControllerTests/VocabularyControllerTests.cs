﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;
public class VocabularyControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetVocabularies_ReturnsOk()
    {
        //Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        
        var vocabulary = await factory.SeedVocabualaryAsync();

        //Act
        var response = await client.GetAsync("api/v2.0/vocabularies");

        //Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<IEnumerable<VocabularyDto>>>();
        Assert.That(content.Data?.Count(), Is.GreaterThan(0));
        Assert.That(content.Data?.First().GraphUri, Is.EqualTo(vocabulary.GraphUri));
        Assert.That(content.Data?.First().ClassUri, Is.EqualTo(vocabulary.ClassUri));

    }
}