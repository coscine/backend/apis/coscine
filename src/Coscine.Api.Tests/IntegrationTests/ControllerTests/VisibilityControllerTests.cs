﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class VisibilityControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetVisibility_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var visibilities = await factory.SeedVisibilitiesAsync();

        // Act
        var response = await client.GetAsync($"/api/v2.0/visibilities/{visibilities.First().Id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<VisibilityDto>>();

        Assert.That(content.Data?.Id, Is.EqualTo(visibilities.First().Id));
    }

    [Test]
    public async Task GetVisibilities_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        // Act
        var response = await client.GetAsync("/api/v2.0/visibilities");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<IEnumerable<VisibilityDto>>>();

        Assert.That(content.Data, Has.Count.EqualTo(2));
    }
}