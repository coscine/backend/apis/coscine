﻿using Amazon.S3.Model;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests.TreeControllerTests;

[TestFixture]
internal class TreeDataControllerRdsS3Tests : CoscineControllerTestsBase
{

    private Resource? _lastResource;

    private static IEcsManagerFactory GetEcsFactory()
    {
        var factory = CoscineTestExtensions.CreateFactory();
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        return serviceProvider.GetRequiredService<IEcsManagerFactory>();
    }

    private static IS3ClientFactory GetS3Factory()
    {
        var factory = CoscineTestExtensions.CreateFactory();
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        return serviceProvider.GetRequiredService<IS3ClientFactory>();
    }

    [TearDown]
    public void CleanUp()
    {
        if (_lastResource is not null)
        {
            var ecsfactory = GetEcsFactory();
            var configKey = ResourceTypeConfiguration.ResourceTypeRdss3.SpecificType ?? throw new Exception();
            var bucketName = _lastResource.Id.ToString();

            // Read User
            try
            {
                ecsfactory.GetRdsS3UserEcsManager(configKey).DeleteObjectUser($"read_{_lastResource.Id}").Wait();
            }
            catch (Exception)
            {

            }

            // Write User
            try
            {
                ecsfactory.GetRdsS3UserEcsManager(configKey).DeleteObjectUser($"write_{_lastResource.Id}").Wait();
            }
            catch (Exception)
            {

            }

            // Delete any remaining entries in the bucket
            try
            {
                using var s3Client = GetS3Factory().GetRdsS3Client(configKey);

                ListObjectsV2Request request = new()
                {
                    BucketName = bucketName
                };

                ListObjectsV2Response response;
                do
                {
                    response = s3Client.ListObjectsV2Async(request).Result;
                    foreach (S3Object entry in response.S3Objects)
                    {
                        s3Client.DeleteObjectAsync(new DeleteObjectRequest
                        {
                            BucketName = bucketName,
                            Key = entry.Key
                        });
                    }

                    request.ContinuationToken = response.NextContinuationToken;
                } while (response.IsTruncated);
            }
            catch (Exception)
            {

            }

            // Delete the bucket
            try
            {
                var ecsFactory = GetEcsFactory();
                ecsFactory.GetRdsS3EcsManager(configKey).DeleteBucket(bucketName).Wait();
            }
            catch (Exception)
            {

            }
        }
    }

    [Test, Explicit]
    public async Task GetFileTree_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        const string key = "test.txt";
        const string blobContent = "CoScIne Rocks!";

        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(blobContent);
        writer.Flush();
        stream.Position = 0;

        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeRdss3, createDataStorageParameters: new CreateDataStorageParameters { Quota = 1 });

        var projectId = resource.ProjectResources.First().ProjectId;

        _lastResource = resource;

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var ds = serviceProvider.GetRequiredService<IDataStorageRepositoryFactory>().Create(resource.TypeId)!;

        await ds.CreateAsync(new CreateBlobParameters
        {
            Resource = resource,
            Blob = stream,
            Path = key
        });

        // Act
        var response = await client.GetAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/trees/files");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<FileTreeDto>>();
        Assert.That(content, Is.Not.Null);
        Assert.That(content.Data, Is.Not.Null);

        var fileTreeDto = content.Data?.FirstOrDefault(x => x.Path == key);
        Assert.That(fileTreeDto, Is.Not.Null);

        Assert.That(fileTreeDto?.Size, Is.EqualTo(stream.Length));
        Assert.That(fileTreeDto?.Path, Is.EqualTo(key));
        Assert.That(fileTreeDto?.Hidden, Is.EqualTo(false));
        Assert.That(fileTreeDto?.Type, Is.EqualTo(TreeDataType.Leaf));
    }
}
