﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;
using NSubstitute;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests.TreeControllerTests;

[TestFixture]
internal class TreeDataControllerLinkedTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetFileTree_ReturnsOk()
    {
        // Arrange
        const string key = "new-entry";
        const string fileContent = "https://example.org";
        var uri = new Uri("https://coscine.test/data");

        var treeRepositoryMock = Substitute.For<ITreeRepository>();

        // Map to the correct graph
        treeRepositoryMock
            .GetNewestPagedDataAsync(Arg.Any<Guid>(), Arg.Any<DataTreeParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<DataTree>([new DataTree { Id = uri, Path = key }], 1, 1, 1)));

        var factory = CoscineTestExtensions.CreateFactory()
                .WithServiceInstance(treeRepositoryMock);
        var client = await InitializeClientAsync(factory);

        await factory.SeedLinkedDataEntryAsync(fileContent, uri);

        // Create resource
        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeLinked);

        var projectId = resource.ProjectResources.First().ProjectId;

        // Act
        var response = await client.GetAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/trees/files");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<FileTreeDto>>();
        Assert.That(content, Is.Not.Null);
        Assert.That(content.Data, Is.Not.Null);

        var fileTreeDto = content.Data?.FirstOrDefault(x => x.Path == key);
        Assert.That(fileTreeDto, Is.Not.Null);

        Assert.That(fileTreeDto?.Path, Is.EqualTo(key));
        Assert.That(fileTreeDto?.Hidden, Is.EqualTo(false));
        Assert.That(fileTreeDto?.Type, Is.EqualTo(TreeDataType.Leaf));
    }
}
