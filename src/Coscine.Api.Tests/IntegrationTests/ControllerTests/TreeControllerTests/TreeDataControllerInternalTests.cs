﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests.TreeControllerTests;

[TestFixture]
internal class TreeDataControllerInternalTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetFileTree_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);


        const string key = $"{nameof(GetFileTree_ReturnsOk)}.txt";
        string blobContent = Guid.NewGuid().ToString();

        var resource = await factory.SeedResourceAsync();


        var projectId = resource.ProjectResources.First().ProjectId;

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var ds = serviceProvider.GetRequiredService<IDataStorageRepositoryFactory>().Create(resource.TypeId)!;

        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(blobContent);
        writer.Flush();
        stream.Position = 0;

        await ds.CreateAsync(new CreateBlobParameters
        {
            Resource = resource,
            Blob = stream,
            Path = key
        });

        // Act
        var response = await client.GetAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/trees/files");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<FileTreeDto>>();
        Assert.That(content, Is.Not.Null);
        Assert.That(content.Data, Is.Not.Null);

        var fileTreeDto = content.Data?.FirstOrDefault(x => x.Path == key);
        Assert.That(fileTreeDto, Is.Not.Null);

        Assert.That(fileTreeDto?.Size, Is.EqualTo(stream.Length));
        Assert.That(fileTreeDto?.Path, Is.EqualTo(key));
        Assert.That(fileTreeDto?.Hidden, Is.EqualTo(false));
        Assert.That(fileTreeDto?.Type, Is.EqualTo(TreeDataType.Leaf));
    }
}
