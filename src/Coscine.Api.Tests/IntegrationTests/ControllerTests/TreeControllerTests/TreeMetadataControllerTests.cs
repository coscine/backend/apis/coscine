﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.QueryParameters;
using Coscine.Api.Core.Shared.RequestFeatures;
using Coscine.Api.Tests.Mockups.Data;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using NUnit.Framework;
using System.Net;
using System.Text;
using System.Text.Json;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests.TreeControllerTests;

[TestFixture]
internal class TreeMetadataControllerTests : CoscineControllerTestsBase
{

    [Test]
    public async Task GetMetadataTree_Mocked_ReturnsOk()
    {
        // Arrange
        const string key = "test.txt";

        var treeRepositoryMock = Substitute.For<ITreeRepository>();
        treeRepositoryMock
            .GetNewestPagedMetadataAsync(Arg.Any<Guid>(), Arg.Any<MetadataTreeParameters>())
            .Returns(Task.FromResult(PagedEnumerable<MetadataTree>.ToPagedEnumerable([new MetadataTree { Path = key }], 1, 10)));

        var factory = CoscineTestExtensions.CreateFactory()
                .WithServiceInstance(treeRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var resources = await factory.SeedResourcesAsync();

        var projectId = resources.First().ProjectResources.First().ProjectId;
        var resource = resources.First();

        // Act
        var response = await client.GetAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/trees/metadata");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<MetadataTreeDto>>();

        Assert.That(content.Data?.Count(), Is.EqualTo(1));
        Assert.That(content.Data?.First().Path, Is.EqualTo(key));
    }

    [Test]
    public async Task GetSpecificMetadataTree_ReturnsOk()
    {
        // Arrange
        const string key = "test.txt";
        Uri id = new("https://www.example.com");

        var treeRepositoryMock = Substitute.For<ITreeRepository>();
        treeRepositoryMock
            .GetMetadataAsync(Arg.Any<Guid>(), Arg.Any<MetadataTreeQueryParameters>())
            .Returns(Task.FromResult<MetadataTree?>(new MetadataTree
            {
                Path = key,
                Provenance = new Provenance()
                {
                    Id = id,
                    Variants = [],
                    WasRevisionOf = []
                }
            }));

        var factory = CoscineTestExtensions.CreateFactory()
           .WithServiceInstance(treeRepositoryMock);
        var client = await InitializeClientAsync(factory);


        var resources = await factory.SeedResourcesAsync();

        var projectId = resources.First().ProjectResources.First().ProjectId;
        var resource = resources.First();

        // Act
        var response = await client.GetAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/trees/metadata/specific?Path=text.txt&IncludeProvenance=true");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<MetadataTreeDto>>();

        Assert.That(content.Data, Is.Not.Null);
        Assert.That(content.Data?.Path, Is.EqualTo(key));
        Assert.That(content.Data?.Provenance?.Id, Is.EqualTo(id));
    }

    [Test]
    public async Task CreateExtractedSpecificMetadataTree_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory, admin: true);

        const string key = "test.txt";

        var resources = await factory.SeedResourcesAsync();

        var projectId = resources.First().ProjectResources.First().ProjectId;
        var resource = resources.First();
        var resourceUriString = $"https://purl.org/coscine/resources/{resource.Id}/{key}";
        Uri id = new($"{resourceUriString}/@type=metadata&version=1234");

        var creationDto = new ExtractedMetadataTreeForCreationDto
        {
            Definition = new RdfDefinitionForManipulationDto
            {
                Content = MetadataMock.GetExtractedMetadata(resourceUriString),
                Type = RdfFormat.TriG
            },
            Id = id,
            Provenance = new ProvenanceParametersDto
            {
                HashParameters = new HashParametersDto { AlgorithmName = "SHA512", Value = "0" }
            },
            Path = key
        };

        var json = JsonSerializer.Serialize(creationDto);
        var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

        // Act
        var response = await client.PostAsync(
            $"/api/v2/projects/{projectId}/resources/{resource.Id}/trees/metadata/extracted",
            stringContent
        );

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));
    }

    [Test]
    public async Task GetExtractedSpecificMetadataTree_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory, admin: true);

        var (projectId, metadataTree) = await factory.SeedExtractedMetadataTreeAsync();

        var resolver = new TreeUriResolver(metadataTree.Id!);

        // Act
        var response = await client.GetAsync($"/api/v2/projects/{projectId}/resources/{resolver.ResourceId}/trees/metadata/specific?Path={metadataTree.Path}&IncludeProvenance=true");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<MetadataTreeDto>>();

        Assert.That(content.Data, Is.Not.Null);
        Assert.That(content.Data?.Path, Is.EqualTo(metadataTree.Path));
        Assert.That(content.Data?.Provenance?.Id, Is.EqualTo(metadataTree.Id));
    }


    [Test]
    public async Task UpdateExtractedSpecificMetadataTree_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory, admin: true);

        var (projectId, metadataTree) = await factory.SeedExtractedMetadataTreeAsync();

        var resolver = new TreeUriResolver(metadataTree.Id!);

        var newId = new Uri(metadataTree.Id!.AbsoluteUri.Replace("version=1234", "version=1235"));

        var resourceUriString = $"https://purl.org/coscine/resources/{resolver.ResourceId}/{resolver.Path}";
        var updateDto = new ExtractedMetadataTreeForUpdateDto
        {
            Definition = new RdfDefinitionForManipulationDto
            {
                Content = MetadataMock.GetExtractedMetadata(resourceUriString)
                    .Replace("https://purl.org/coscine/ontologies/topic/attributes#september",
                        "https://purl.org/coscine/ontologies/topic/attributes#october")
                    .Replace("version=1234", "version=1235"),
                Type = RdfFormat.TriG
            },
            Id = newId,
            Provenance = new ProvenanceParametersDto
            {
                HashParameters = new HashParametersDto { AlgorithmName = "SHA512", Value = "1" },
                WasRevisionOf = [metadataTree.Id!]
            },
            Path = resolver.Path
        };

        var json = JsonSerializer.Serialize(updateDto);
        var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

        // Act
        var response = await client.PutAsync(
            $"/api/v2/projects/{projectId}/resources/{resolver.ResourceId}/trees/metadata/extracted",
            stringContent
        );

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var treeRepository = serviceProvider.GetRequiredService<ITreeRepository>();

        metadataTree = await treeRepository.GetMetadataAsync(resolver.ResourceId, new MetadataTreeQueryParameters
        {
            Path = metadataTree.Path,
            Format = RdfFormat.Turtle,
            IncludeExtractedMetadata = true,
            IncludeProvenance = true
        });

        Assert.That(metadataTree, Is.Not.Null);
        Assert.That(metadataTree?.Path, Is.EqualTo(resolver.Path));
        Assert.That(metadataTree?.Provenance?.Id, Is.EqualTo(newId));
        Assert.That(metadataTree?.Provenance?.WasRevisionOf.Count(), Is.EqualTo(1));
    }

    [Test]
    public async Task DeleteSpecificMetadataTree_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory, admin: true);

        var (projectId, metadataTree) = await factory.SeedExtractedMetadataTreeAsync();

        var resolver = new TreeUriResolver(metadataTree.Id!);

        var deletionDto = new MetadataTreeForDeletionDto
        {
            Path = resolver.Path,
            Version = resolver.Version,
            InvalidatedBy = new Uri("http://example.com/example")
        };

        var json = JsonSerializer.Serialize(deletionDto);
        var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

        // Act
        var request = new HttpRequestMessage(HttpMethod.Delete, $"/api/v2/projects/{projectId}/resources/{resolver.ResourceId}/trees/metadata")
        {
            Content = stringContent
        };
        var response = await client.SendAsync(request);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));
    }

    [Test]
    public async Task CreateMetadataTree_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        const string key = "test.txt";

        var resources = await factory.SeedResourcesAsync();

        var projectId = resources.First().ProjectResources.First().ProjectId;
        var resource = resources.First();
        var resourceUriString = $"https://purl.org/coscine/resources/{resource.Id}/{key}";

        var creationDto = new MetadataTreeForCreationDto
        {
            Definition = new RdfDefinitionForManipulationDto
            {
                Content = MetadataMock.GetTemplatedMetadata(resourceUriString),
                Type = RdfFormat.Turtle
            },
            Path = key
        };

        var json = JsonSerializer.Serialize(creationDto);
        var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

        // Act
        var response = await client.PostAsync(
            $"/api/v2/projects/{projectId}/resources/{resource.Id}/trees/metadata",
            stringContent
        );

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));
    }

    [Test]
    public async Task CreateMetadataTree_Folder_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        const string key = "test/";

        var resources = await factory.SeedResourcesAsync();

        var projectId = resources.First().ProjectResources.First().ProjectId;
        var resource = resources.First();
        var resourceUriString = $"https://purl.org/coscine/resources/{resource.Id}/{key}";

        var creationDto = new MetadataTreeForCreationDto
        {
            Definition = new RdfDefinitionForManipulationDto
            {
                Content = MetadataMock.GetTemplatedMetadata(resourceUriString),
                Type = RdfFormat.Turtle
            },
            Path = key
        };

        var json = JsonSerializer.Serialize(creationDto);
        var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

        // Act
        var response = await client.PostAsync(
            $"/api/v2/projects/{projectId}/resources/{resource.Id}/trees/metadata",
            stringContent
        );

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));
    }

    [Test]
    public async Task GetMetadataTree_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var (projectId, metadataTree) = await factory.SeedMetadataTreeAsync();

        var resolver = new TreeUriResolver(metadataTree.Id!);

        // Act
        var response = await client.GetAsync($"/api/v2/projects/{projectId}/resources/{resolver.ResourceId}/trees/metadata/specific?Path={metadataTree.Path}&IncludeProvenance=true");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<MetadataTreeDto>>();

        Assert.That(content.Data, Is.Not.Null);
        Assert.That(content.Data?.Path, Is.EqualTo(metadataTree.Path));
    }

    [Test]
    public async Task UpdateSpecificMetadataTreeWithTemplate_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory, admin: true);

        var (projectId, metadataTree) = await factory.SeedMetadataTreeAsync();

        var id = metadataTree.Id!;

        var version = int.Parse(id.AbsoluteUri.Substring(id.AbsoluteUri.IndexOf("version=") + "version=".Length));
        var newVersion = version + 1;
        var newId = new Uri(id.AbsoluteUri.Replace(version + "", newVersion + ""));

        var resolver = new TreeUriResolver(id);

        var resourceUriString = $"https://purl.org/coscine/resources/{resolver.ResourceId}/{resolver.Path}";
        var updateDto = new ExtractedMetadataTreeForUpdateDto
        {
            Definition = new RdfDefinitionForManipulationDto
            {
                Content = MetadataMock.GetExtractedMetadata(resourceUriString)
                    .Replace("https://purl.org/coscine/ontologies/topic/attributes#september",
                        "https://purl.org/coscine/ontologies/topic/attributes#october")
                    .Replace("version=1234",
                        $"version={newVersion}"),
                Type = RdfFormat.TriG
            },
            Id = newId,
            Provenance = new ProvenanceParametersDto
            {
                HashParameters = new HashParametersDto { AlgorithmName = "SHA512", Value = "1" },
                WasRevisionOf = new List<Uri>() { id }
            },
            Path = resolver.Path
        };

        var json = JsonSerializer.Serialize(updateDto);
        var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

        // Act
        var response = await client.PutAsync(
            $"/api/v2/projects/{projectId}/resources/{resolver.ResourceId}/trees/metadata/extracted",
            stringContent
        );

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var treeRepository = serviceProvider.GetRequiredService<ITreeRepository>();

        metadataTree = await treeRepository.GetMetadataAsync(resolver.ResourceId, new MetadataTreeQueryParameters
        {
            Path = metadataTree.Path,
            Format = RdfFormat.Turtle,
            IncludeProvenance = true
        });

        Assert.That(metadataTree, Is.Not.Null);
        Assert.That(metadataTree?.Path, Is.EqualTo(resolver.Path));
        Assert.That(metadataTree?.Definition, Contains.Substring("Benedikt Heinrichs"));
        Assert.That(metadataTree?.Provenance?.Id, Is.EqualTo(newId));
        Assert.That(metadataTree?.Provenance?.WasRevisionOf.Count(), Is.EqualTo(1));
    }

    [Test]
    public async Task CreateMetadataTree_ReturnsBadRequest()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        
        const string key = "test.txt";

        var resources = await factory.SeedResourcesAsync();

        var projectId = resources.First().ProjectResources.First().ProjectId;
        var resource = resources.First();
        var resourceUriString = $"https://purl.org/coscine/resources/{resource.Id}/{key}";

        var creationDto = new MetadataTreeForCreationDto
        {
            Definition = new RdfDefinitionForManipulationDto
            {
                Content = MetadataMock.GetIncompleteMetadata(resourceUriString),
                Type = RdfFormat.Turtle
            },
            Path = key
        };

        var json = JsonSerializer.Serialize(creationDto);
        var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

        // Act
        var response = await client.PostAsync(
            $"/api/v2/projects/{projectId}/resources/{resource.Id}/trees/metadata",
            stringContent
        );

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
    }
}
