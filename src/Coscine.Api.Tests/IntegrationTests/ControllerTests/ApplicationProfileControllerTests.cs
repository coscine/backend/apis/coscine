/*using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class ApplicationProfileControllerTests: CoscineControllerTestsBase
{
    //Caution: This Test doesn't work, because this is unable to get a parsing graph by query  
    //Needs to be run explicit
    [Test, Explicit]
    public async Task GetApplicationProfiles_ReturnsOk()
    {
        // Act
        var response = await client.GetAsync("/api/v2.0/application-profiles/profiles");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        var content= await response.Content.ReadAsAsync<PagedResponse<ApplicationProfileDto>>();
        Assert.That(content.Pagination?.TotalCount, Is.GreaterThan(0));
    }

    //Caution: This test doesn't work
    //Needs to be run explicit
    [Test, Explicit]
    public async Task CreateApplicationProfile_CreatesApplicationProfile()
    {
        var applicationProfileDto = new ApplicationProfileForCreationDto
        {
            Definition = new()
            {
                Content = "This is the definition for the create Application Profile test",
                Type = RdfFormat.Turtle,
            },
            Name = "This is the test Application Profile",
            Uri = new("https://purl.org/coscine/ap/base/")
        };
       
        // Act
        var response = await client.PostAsJsonAsync($"/api/v2.0/application-profiles/requests", applicationProfileDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));
        var content = await response.Content.ReadAsAsync<Response<ApplicationProfileDto>>();

        Assert.That(content.Data?.DisplayName, Is.EqualTo(applicationProfileDto.Name));
    }
}
*/