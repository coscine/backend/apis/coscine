﻿using System.Net;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;
using NSubstitute;
using NUnit.Framework;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class SystemStatusControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetNocMessages_ReturnsPagedNocMessages()
    {
        // Arrange
        var nocTickets = new List<NocTicket>() {
            new() {
                Id = 5555,
                Href = new Uri("https://example.com/noc-5555"),
                Type = MessageType.Disturbance,
                StartAt = DateTime.Now.AddDays(-1), // yesterday
                EndAt = DateTime.Now.AddDays(1), // tomorrow
            },
            new() {
                Id = 5554,
                Href = new Uri("https://example.com/noc-5554"),
                Type = MessageType.Disturbance,
                StartAt = DateTime.Now.AddDays(-2), // 2 days ago
                EndAt = DateTime.Now.AddDays(-1), // yesterday
            }
        };


        var nocRepositoryMock = Substitute.For<INocRepository>();
        nocRepositoryMock.GetPagedFromNocAsync(Arg.Any<MessageParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<NocTicket>(nocTickets, nocTickets.Count, 1, 10)));

        var factory = CoscineTestExtensions.CreateFactory().WithServiceInstance(nocRepositoryMock);
        var client = await InitializeClientAsync(factory);

        // Act
        var response = await client.GetAsync("/api/v2.0/system/status/noc");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<MessageDto>>();
        var dtos = content.Data?.ToList();

        Assert.That(dtos, Has.Count.EqualTo(nocTickets.Count));
        Assert.That(dtos?.Select(dto => dto.Id), Is.EquivalentTo(nocTickets.Select(t => $"noc-{t.Id}")));
    }

    [Test]
    public async Task GetInternalMessages_ReturnsPagedInternalMessages()
    {
        // Arrange
        var internalMessages = new List<Message>() {
            new() {
                Body = new Dictionary<string, string>
                {
                    { "en", "Test message 1" },
                    { "de", "Testnachricht 1" }
                },
                Type = MessageType.Information,
                Title = "Title 1",
                StartDate = DateTime.Now.AddDays(-1), // yesterday
                EndDate = DateTime.Now.AddDays(1) // tomorrow
            },
            new() {
                Body = new Dictionary<string, string>
                {
                    { "en", "Test message 2" },
                    { "de", "Testnachricht 2" }
                },
                Type = MessageType.Warning,
                Title = "Title 2",
                StartDate = DateTime.Now.AddDays(-2), // 2 days ago
                EndDate = DateTime.Now.AddDays(-1) // yesterday
            }
        };

        var messageRepositoryMock = Substitute.For<IMessageRepository>();
        messageRepositoryMock.GetPagedFromConsulAsync(Arg.Any<MessageParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<Message>(internalMessages, internalMessages.Count, 1, 10)));

        var factory = CoscineTestExtensions.CreateFactory().WithServiceInstance(messageRepositoryMock);
        var client = await InitializeClientAsync(factory);

        // Act
        var response = await client.GetAsync("/api/v2.0/system/status/internal");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<MessageDto>>();
        var dtos = content.Data?.ToList();

        Assert.That(dtos, Has.Count.EqualTo(internalMessages.Count));
        Assert.That(dtos?.Select(dto => dto.Id), Is.EquivalentTo(internalMessages.Select(m => $"int-{m.Id}")));
    }
}