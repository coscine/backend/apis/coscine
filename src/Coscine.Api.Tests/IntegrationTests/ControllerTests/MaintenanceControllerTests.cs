﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using NSubstitute;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class MaintenanceControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetMaintenance_Empty_ReturnsNoContent()
    {
        // Assemble
        var maintenanceRepositoryMock = Substitute.For<IMaintenanceRepository>();
        maintenanceRepositoryMock.GetAllAsync(Arg.Any<bool>()).Returns(Task.FromResult(new List<Maintenance>()));
        maintenanceRepositoryMock.GetCurrent(Arg.Any<bool>()).Returns(Task.FromResult<Maintenance?>(null));

        var factory = CoscineTestExtensions.CreateFactory()
           .WithServiceInstance(maintenanceRepositoryMock);
        var client = await InitializeClientAsync(factory);

        // Act
        var response = await client.GetAsync("/api/v2.0/maintenances");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<MaintenanceDto>>();

        Assert.That(content?.Data, Is.Empty);
    }
}