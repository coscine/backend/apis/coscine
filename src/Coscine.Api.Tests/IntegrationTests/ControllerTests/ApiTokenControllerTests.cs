﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class ApiTokenControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetApiToken_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var apiToken = await factory.SeedApiTokenAsync(DatabaseSeeder.TestUser.Id);

        // Act
        var response = await client.GetAsync($"/api/v2.0/self/api-tokens/{apiToken.Id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<ApiTokenDto>>();

        Assert.That(content.Data?.Id, Is.EqualTo(apiToken.Id));
        Assert.That(content.Data?.Token, Is.Null);
    }

    [Test]
    public async Task GetApiTokens_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        
        var apiToken = await factory.SeedApiTokenAsync(DatabaseSeeder.TestUser.Id);

        // Act
        var response = await client.GetAsync("/api/v2.0/self/api-tokens");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<ApiTokenDto>>();

        // Offset for the internal test token used for the login!
        Assert.That(content.Data, Has.Count.GreaterThan(1));
        Assert.That(content.Data?.Any(x => x.Id == apiToken.Id), Is.True);
    }

    [Test]
    public async Task DeleteApiToken_ReturnsNoContent()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var apiToken = await factory.SeedApiTokenAsync(DatabaseSeeder.TestUser.Id);

        // Act
        var response = await client.DeleteAsync($"/api/v2.0/self/api-tokens/{apiToken.Id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var content = await response.Content.ReadAsStreamAsync();

        Assert.That(content.Length, Is.EqualTo(0));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        Assert.That(!dbContext.ApiTokens.Where(x => x.Id == apiToken.Id).Any(), Is.True);
    }

    [Test]
    public async Task CreateApiToken_ReturnsCreated()
    {
        // Arrange
        var apiTokenForCreation = new ApiTokenForCreationDto
        {
            ExpiresInDays = 7,
            Name = "Test Case"
        };
        
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        // Act
        var response = await client.PostAsJsonAsync("/api/v2.0/self/api-tokens", apiTokenForCreation);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));

        var content = await response.Content.ReadAsAsync<Response<ApiTokenDto>>();

        Assert.That(content.Data?.Name, Is.EqualTo(apiTokenForCreation.Name));
        // Offset for the internal test token used for the login!

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        Assert.That(dbContext.ApiTokens.Any(x => x.Name == apiTokenForCreation.Name), Is.True);
    }
}