﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class ProjectInvitationControllerIntegrationTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetProjectInvitations_ReturnsAllInvitations()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var invitations = await factory.SeedProjectInvitationsAsync();

        // Act
        var response = await client.GetAsync($"/api/v2.0/projects/{invitations.First().Project}/Invitations");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<ProjectInvitationDto>>();

        Assert.That(content.Data, Has.Count.EqualTo(invitations.Count(x => x.Project == invitations.First().Project)));
    }

    [Test]
    public async Task GetProjectInvitation_ReturnsCorrectInvitation()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        
        var invitations = await factory.SeedProjectInvitationsAsync();

        // Act
        var response = await client.GetAsync($"/api/v2.0/projects/{invitations.First().Project}/Invitations/{invitations.First().Id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<ProjectInvitationDto>>();

        Assert.That(content.Data?.Email, Is.EqualTo(invitations.First().InviteeEmail));
        Assert.That(content.Data?.Role.Id, Is.EqualTo(invitations.First().Role));
    }

    [Test]
    public async Task ResolveProjectInvitation_ReturnsNoContent()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        // Create projects and invitations for other user
        var users = await factory.SeedUsersAsync();
        var invitations = await factory.SeedProjectInvitationsAsync(users.First().Id);

        var projectInvitationDto = new ProjectInvitationResolveDto { Token = invitations.First().Token };

        // Act
        var response = await client.PostAsJsonAsync("/api/v2.0/self/project-invitations", projectInvitationDto);

        // Assert

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var content = await response.Content.ReadAsStreamAsync();

        Assert.That(content.Length, Is.EqualTo(0));
        // Check if user was added to the project as an owner
        Assert.That(dbContext.ProjectRoles.Where(x => x.ProjectId == invitations.First().Project
            && x.UserId == DatabaseSeeder.TestUser.Id
            && x.RoleId == DatabaseSeeder.Owner.Id).Count, Is.EqualTo(1));
    }

    [Test]
    public async Task CreateProjectInvitation_CreatesInvitation()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var projects = await factory.SeedProjectsAsync();

        var projectInvitationDto = new ProjectInvitationForProjectManipulationDto
        {
            Email = "test@example.com",
            RoleId = DatabaseSeeder.Owner.Id,
        };

        // Act
        var response = await client.PostAsJsonAsync($"/api/v2.0/projects/{projects.First().Id}/invitations", projectInvitationDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));

        var content = await response.Content.ReadAsAsync<Response<ProjectInvitationDto>>();

        Assert.That(content.Data?.Email, Is.EqualTo(projectInvitationDto.Email));
    }

    [Test]
    public async Task DeleteProjectInvitationById_DeletesInvitation()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        
        var invitations = await factory.SeedProjectInvitationsAsync();

        // Act
        var response = await client.DeleteAsync($"/api/v2.0/projects/{invitations.First().Project}/Invitations/{invitations.First().Id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var content = await response.Content.ReadAsStreamAsync();

        Assert.That(content.Length, Is.EqualTo(0));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        Assert.That(dbContext.Invitations.Count, Is.EqualTo(invitations.Count() - 1));
    }
}