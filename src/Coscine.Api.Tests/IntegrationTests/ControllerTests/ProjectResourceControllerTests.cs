using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class ProjectResourceControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetResource_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var resource = await factory.SeedResourceAsync(createDataStorageParameters: new Core.Repository.Contracts.CreateDataStorageParameters { });

        // Act
        var response = await client.GetAsync($"/api/v2.0/projects/{resource.ProjectResources.First().ProjectId}/resources/{resource.Id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<ResourceDto>>();

        Assert.That(content.Data, Is.Not.Null);
        Assert.That(content.Data?.Creator, Is.Not.Null);
        Assert.That(content.Data?.Id, Is.EqualTo(resource.Id));
        Assert.That(content.Data?.DisplayName, Is.EqualTo(resource.DisplayName));
        Assert.That(content.Data?.Type.Id, Is.EqualTo(resource.TypeId));
        Assert.That(content.Data?.Type.Options?.FileSystemStorage, Is.Not.Null);
        Assert.That(content.Data?.Type.Options?.FileSystemStorage?.Directory, Contains.Substring(resource.Id.ToString()));
    }

    [Test]
    public async Task GetProjectResources_ReturnAllResources()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var resources = await factory.SeedResourcesAsync();
        var project = resources.First().ProjectResources.First();
        var count = 0;

        foreach (var resource in resources)
        {
            foreach (var resourceDto in resource.ProjectResources)
            {
                if (resourceDto.ProjectId == project.ProjectId)
                {
                    count++;
                }
            }
        }

        // Act
        var response = await client.GetAsync($"/api/v2.0/projects/{project.ProjectId}/resources");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        var content = await response.Content.ReadAsAsync<PagedResponse<ResourceDto>>();

        Assert.That(content.Data, Has.Count.EqualTo(count));
    }

    [Test]
    public async Task CreateProjectResource_CreatesResource()
    {
        // Arrange
        var resourceTypeServiceMock = Substitute.For<IResourceTypeService>();
        IEnumerable<ResourceType> resourceTypes = [ResourceTypeConfiguration.LocalResourceType];

        resourceTypeServiceMock.GetAvailableResourceTypesForProjectAndUserAsync(Arg.Any<Guid>(), Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(Task.FromResult(resourceTypes));

        var factory = CoscineTestExtensions.CreateFactory()
           .WithServiceInstance(resourceTypeServiceMock);
        var client = await InitializeClientAsync(factory);

        var resource = await factory.SeedResourceAsync(createDataStorageParameters: new Core.Repository.Contracts.CreateDataStorageParameters { });
        var project = resource.ProjectResources.First();
        var disciplines = await factory.SeedDisciplinesAsync();
        var visibilities = await factory.SeedVisibilitiesAsync();
        var licenses = await factory.SeedLicensesAsync();
        var resourceDto = new ResourceForCreationDto
        {
            Name = "Test Project Resource created",
            Description = "This is a created test project resource",
            Keywords = ["one;two;three;"],
            FixedValues = { },
            License = new LicenseForResourceManipulationDto { Id = licenses.First().Id },
            UsageRights = "Test created Usage Rights",
            MetadataLocalCopy = true,

            Visibility = new VisibilityForResourceManipulationDto() { Id = visibilities.First().Id },
            Disciplines =
            [
                new() { Id = disciplines.First().Id }
            ],
            ResourceTypeOptions = new ResourceTypeOptionsForCreationDto()
            {

            },
            ResourceTypeId = resource.TypeId,
            ApplicationProfile = new ApplicationProfileForResourceCreationDto() { Uri = new Uri("https://purl.org/coscine/ap/base/") }
        };

        // Act
        var response = await client.PostAsJsonAsync($"/api/v2.0/projects/{project.ProjectId}/resources", resourceDto);

        // Assert
        var a = await response.Content.ReadAsStringAsync();
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));
        var content = await response.Content.ReadAsAsync<Response<ResourceDto>>();

        Assert.That(content.Data?.Description, Is.EqualTo(resourceDto.Description));
    }
    [Test]
    public async Task UpdateProjectResource_UpdatesResource()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var resources = await factory.SeedResourcesAsync();
        var project = resources.Last().ProjectResources.First();
        var visibilities = await factory.SeedVisibilitiesAsync();
        var licenses = await factory.SeedLicensesAsync();
        var resourceDto = new ResourceForUpdateDto
        {
            Name = "Test Project Resource updated",
            Description = "This is a updated test project resource",
            Keywords = ["one;two;three;"],
            FixedValues = { },
            License = new LicenseForResourceManipulationDto { Id = licenses.First().Id },
            UsageRights = "Test updated Usage Rights",
            MetadataLocalCopy = true,

            Visibility = new VisibilityForResourceManipulationDto() { Id = visibilities.First().Id },
            Disciplines =
            [
                new() { Id = resources.Last().ResourceDisciplines.First().DisciplineId }
            ],
            Archived = false,
            ResourceTypeOptions = new ResourceTypeOptionsForUpdateDto()
            {
                LinkedResourceTypeOptions = new() { }
            }
        };

        // Act
        var response = await client.PutAsJsonAsync($"/api/v2.0/projects/{project.ProjectId}/resources/{resources.Last().Id}", resourceDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));
        var content = await response.Content.ReadAsStreamAsync();

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        Assert.That(dbContext.Resources.Where(x => x.Id == resources.Last().Id).First().Description, Is.EqualTo(resourceDto.Description));


    }


    [Test]
    public async Task DeleteProjectResourceById_DeletesResource()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        
        var resources = await factory.SeedResourcesAsync();
        var project = resources.First().ProjectResources.First();

        // Act
        var response = await client.DeleteAsync($"/api/v2.0/projects/{project.ProjectId}/resources/{resources.First().Id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var content = await response.Content.ReadAsStreamAsync();

        Assert.That(content.Length, Is.EqualTo(0));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        Assert.That(dbContext.Resources.Where(x => x.Id == resources.First().Id).First().Deleted, Is.True);
    }

}