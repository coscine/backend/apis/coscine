﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class TermsOfServiceControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetToS_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        // Act
        var response = await client.GetAsync("/api/v2.0/tos");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<TermsOfServiceDto>>();

        Assert.That(content.Data?.Version, Is.Not.EqualTo(string.Empty));
        Assert.That(content.Data?.Version, Is.Not.Null);
    }
}
