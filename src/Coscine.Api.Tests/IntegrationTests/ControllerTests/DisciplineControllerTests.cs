﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class DisciplineControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetDiscipline_ReturnsOk()
    {
        // Act
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var response = await client.GetAsync("/api/v2.0/disciplines/CFD6B656-F4BA-48C6-B5D8-2F98A7B60D1F");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<DisciplineDto>>();

        Assert.That(content.Data?.DisplayNameDe, Is.EqualTo("Informatik 409"));
        Assert.That(content.Data?.DisplayNameEn, Is.EqualTo("Computer Science 409"));
    }

    [Test]
    public async Task GetDisciplines_ReturnsOk()
    {
        // Act
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        
        var response = await client.GetAsync("/api/v2.0/disciplines");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<DisciplineDto>>();

        Assert.That(content.Pagination?.TotalCount, Is.GreaterThan(0));
    }
}