﻿using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.Conflict;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared.RequestFeatures;
using GitLabApiClient;
using GitLabApiClient.Models.Commits.Requests.CreateCommitRequest;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using NUnit.Framework;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests.BlobControllerTests;

[TestFixture]
public class BlobControllerGitLabTests : CoscineControllerTestsBase
{
    // Fixed values for the GitLab repo
    // You have to specify the ProjectAccessToken either with an env variable or by creating the fitting appsettings file
    // See the SetUp function for details.
    private readonly CreateGitLabDataStorageOptions _options = new()
    {
        Branch = "NUnit",
        GitlabProjectId = 74627,
        RepoUrl = "https://git.rwth-aachen.de/",
        TosAccepted = true
    };

    [TearDown]
    public void CleanUp()
    {
        var client = new GitLabClient(new Uri(_options.RepoUrl).ToString(), _options.ProjectAccessToken);
        try
        {
            client.Commits.CreateAsync(
                _options.GitlabProjectId,
                new CreateCommitRequest(
                    _options.Branch,
                    "Cleanup",
                    [
                        new(CreateCommitRequestActionType.Delete, $"{nameof(GetBlob_ReturnsOk)}.txt"),
                    ])
            ).Wait();

        }
        catch (Exception) { }

        try
        {
            client.Commits.CreateAsync(
                _options.GitlabProjectId,
                new CreateCommitRequest(
                    _options.Branch,
                    "Cleanup",
                    [
                        new(CreateCommitRequestActionType.Delete, $"{nameof(CreateBlob_ReturnsCreated)}.txt"),
                    ])
            ).Wait();
        }
        catch (Exception) { }

        try
        {
            client.Commits.CreateAsync(
                _options.GitlabProjectId,
                new CreateCommitRequest(
                    _options.Branch,
                    "Cleanup",
                    [
                        new(CreateCommitRequestActionType.Delete, $"{nameof(DeleteBlob_ReturnsNoContent)}.txt"),
                    ])
            ).Wait();
        }
        catch (Exception) { }
    }

    [SetUp]
    public void SetUp()
    {
        var config = new ConfigurationBuilder()
        .AddJsonFile("appsettings.Testing.json", optional: true)
        .Build();

        var token = config["GitLabApiToken"];
        if (token is not null)
        {
            _options.ProjectAccessToken = token;
        }
        else
        {
            _options.ProjectAccessToken = Environment.GetEnvironmentVariable("GITLAB_TOKEN")
                ?? throw new InvalidOperationException("GITLAB_TOKEN is not set.");
        }

        CleanUp();
    }

    [Test, Explicit]
    public async Task GetBlob_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        const string key = $"{nameof(GetBlob_ReturnsOk)}.txt";
        string blobContent = Guid.NewGuid().ToString();

        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(blobContent);
        writer.Flush();
        stream.Position = 0;

        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeGitlab,
                                                        createDataStorageParameters: new CreateDataStorageParameters
                                                        {
                                                            CreateGitLabDataStorageOptions = _options
                                                        });

        var projectId = resource.ProjectResources.First().ProjectId;

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var ds = serviceProvider.GetRequiredService<IDataStorageRepositoryFactory>().Create(resource.TypeId)!;

        await ds.CreateAsync(new CreateBlobParameters
        {
            Resource = resource,
            Blob = stream,
            Path = key
        });

        // Act
        var response = await client.GetAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsStringAsync();

        Assert.That(content, Is.EqualTo(blobContent));
        Assert.That(response.Content.Headers.ContentType?.ToString(), Is.EqualTo("text/plain"));
        Assert.That(response.Content.Headers.ContentLength, Is.EqualTo(blobContent.Length));
    }

    [Test, Explicit]
    public async Task CreateBlob_ReturnsCreated()
    {
        // Arrange
        const string key = $"{nameof(CreateBlob_ReturnsCreated)}.txt";
        string fileContent = Guid.NewGuid().ToString();

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);
        var fileContentMock = new ByteArrayContent(fileBytes);

        fileContentMock.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");

        var formData = new MultipartFormDataContent { { fileContentMock, "file", key } };

        var treeRepositoryMock = Substitute.For<ITreeRepository>();

        // Ensure, that some kind of metadata is found
        treeRepositoryMock
            .GetNewestPagedMetadataAsync(Arg.Any<Guid>(), Arg.Any<MetadataTreeParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<MetadataTree>([new MetadataTree { Id = new Uri("https://coscine.test/id") }], 1, 1, 1)));

        var factory = CoscineTestExtensions.CreateFactory()
                .WithServiceInstance(treeRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeGitlab,
                                                createDataStorageParameters: new CreateDataStorageParameters
                                                {
                                                    CreateGitLabDataStorageOptions = _options
                                                });

        var projectId = resource.ProjectResources.First().ProjectId;

        // Act
        var response = await client.PostAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}", formData);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));

        // You can also assert the location header if applicable
        var locationHeader = response.Headers.Location;
        Assert.That(locationHeader, Is.Not.Null);
        Assert.That(locationHeader?.ToString(), Does.Contain(key)); // Ensure the location header contains the key of the newly created blob
    }

    [Test, Explicit]
    public async Task DeleteBlob_ReturnsNoContent()
    {
        // Arrange
        const string key = $"{nameof(DeleteBlob_ReturnsNoContent)}.txt";
        string fileContent = Guid.NewGuid().ToString();

        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(fileContent);
        writer.Flush();
        stream.Position = 0;

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);

        var treeRepositoryMock = Substitute.For<ITreeRepository>();

        // Ensure, that some kind of metadata is found
        treeRepositoryMock
            .GetNewestPagedMetadataAsync(Arg.Any<Guid>(), Arg.Any<MetadataTreeParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<MetadataTree>([new MetadataTree { Id = new Uri("https://coscine.test/id") }], 1, 1, 1)));

        // Replace service
        var factory = CoscineTestExtensions.CreateFactory()
                .WithServiceInstance(treeRepositoryMock);
        var client = await InitializeClientAsync(factory);


        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeGitlab,
                                                createDataStorageParameters: new CreateDataStorageParameters
                                                {
                                                    CreateGitLabDataStorageOptions = _options
                                                });

        var projectId = resource.ProjectResources.First().ProjectId;

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var ds = serviceProvider.GetRequiredService<IDataStorageRepositoryFactory>().Create(resource.TypeId)!;

        await ds.CreateAsync(new CreateBlobParameters
        {
            Resource = resource,
            Blob = stream,
            Path = key
        });

        // Act
        var response = await client.DeleteAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var info = await ds.ReadAsync(new ReadTreeParameters
        {
            Resource = resource,
            Path = key
        });


        Assert.That(info, Is.Null);

    }

    [Test, Explicit]
    public async Task CreateBlob_NoMetadataAvailable_ReturnsNotFound()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);


        const string key = $"{nameof(CreateBlob_NoMetadataAvailable_ReturnsNotFound)}.txt";
        string fileContent = Guid.NewGuid().ToString();

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);
        var fileContentMock = new ByteArrayContent(fileBytes);
        fileContentMock.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
        var formData = new MultipartFormDataContent { { fileContentMock, "file", key } };

        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeGitlab,
                                                        createDataStorageParameters: new CreateDataStorageParameters
                                                        {
                                                            CreateGitLabDataStorageOptions = _options
                                                        });

        var projectId = resource.ProjectResources.First().ProjectId;

        // Act
        var response = await client.PostAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}", formData);

        // Assert
        Assert.That(response.IsSuccessStatusCode, Is.False);
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

        // Read response content
        var content = await response.Content.ReadAsStringAsync();
        Assert.That(content, Does.Contain(nameof(MissingMetadataBadRequestException)));
    }

    [Test, Explicit]
    public async Task CreateBlob_FileAlreadyExists_ReturnsBadRequest()
    {
        // Arrange
        const string key = "new-blob.txt";
        const string fileContent = "This is a test blob content.";

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);
        var fileContentMock = new ByteArrayContent(fileBytes);
        fileContentMock.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
        var formData = new MultipartFormDataContent { { fileContentMock, "file", key } };

        var treeRepositoryMock = Substitute.For<ITreeRepository>();

        // Ensure, that some kind of metadata is found
        treeRepositoryMock
            .GetNewestPagedMetadataAsync(Arg.Any<Guid>(), Arg.Any<MetadataTreeParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<MetadataTree>([new MetadataTree { Id = new Uri("https://coscine.test/id") }], 1, 1, 1)));

        var dsMock = Substitute.For<IDataStorageRepository>();

        dsMock
            .ReadAsync(Arg.Any<ReadTreeParameters>())
            .Returns(Task.FromResult<StorageItemInfo?>(new StorageItemInfo { Path = "" }));

        dsMock.TypeId.Returns(ResourceTypeConfiguration.ResourceTypeGitlab.Id);

        var factory = CoscineTestExtensions.CreateFactory()
                .WithServiceInstance(treeRepositoryMock)
                .WithServiceInstance(dsMock);
        var client = await InitializeClientAsync(factory);

        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeGitlab);

        var projectId = resource.ProjectResources.First().ProjectId;

        // Act
        var response = await client.PostAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}", formData);

        // Assert
        Assert.That(response.IsSuccessStatusCode, Is.False);
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Conflict));

        // Read response content
        var content = await response.Content.ReadAsStringAsync();
        Assert.That(content, Does.Contain(nameof(BlobAlreadyExistsConflictException)));
    }

    [TestCase(@"\")]
    [TestCase(":")]
    [TestCase("?")]
    [TestCase("*")]
    [TestCase("<")]
    [TestCase(">")]
    [TestCase("|")]
    [TestCase("+")]
    [TestCase("#")]
    public async Task CreateBlob_WithForbiddenCharsInKey_ThrowsForbiddenCharInKeyBadRequest(string forbiddenChar)
    {
        // Arrange
        forbiddenChar = HttpUtility.UrlEncode(forbiddenChar);
        
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        string key = $"new{forbiddenChar}blob.txt"; // Injecting a forbidden character into the key
        const string fileContent = "This is a test blob content.";

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);
        var fileContentMock = new ByteArrayContent(fileBytes);
        fileContentMock.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
        var formData = new MultipartFormDataContent { { fileContentMock, "file", key } };

        var resources = await factory.SeedResourcesAsync(count: 1, resourceType: ResourceTypeConfiguration.ResourceTypeGitlab);

        var projectId = resources.First().ProjectResources.First().ProjectId;
        var resource = resources.First();

        // Act
        var response = await client.PostAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}", formData);

        // Assert
        Assert.That(response.IsSuccessStatusCode, Is.False);
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        var content = await response.Content.ReadAsStringAsync();
        Assert.That(content, Does.Contain(nameof(ForbiddenCharInKeyBadRequest)));
    }
}
