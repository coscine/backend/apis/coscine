﻿using Amazon.S3.Model;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.Conflict;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Helpers;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using NUnit.Framework;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests.BlobControllerTests;

[TestFixture]
public class BlobControllerRdsTests : CoscineControllerTestsBase
{
    /// <summary>
    /// A list of forbidden characters that are not allowed in blob path keys.
    /// </summary>
    /// <remarks>This list <b>must be kept in sync</b> with the RegEx defined in the <see cref="ForbiddenChars"/> class.</remarks>
    private static readonly string[] _forbiddenChars =
    [
        HttpUtility.UrlEncode("\\"),
        HttpUtility.UrlEncode(":"),
        HttpUtility.UrlEncode("?"),
        HttpUtility.UrlEncode("*"),
        HttpUtility.UrlEncode("<"),
        HttpUtility.UrlEncode(">"),
        HttpUtility.UrlEncode("|"),
        HttpUtility.UrlEncode("+"),
        HttpUtility.UrlEncode("#")
    ];

    private Resource? _lastResource;

    private static IEcsManagerFactory GetEcsFactory()
    {
        var factory = CoscineTestExtensions.CreateFactory();
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        return serviceProvider.GetRequiredService<IEcsManagerFactory>();
    }

    private static IS3ClientFactory GetS3Factory()
    {
        var factory = CoscineTestExtensions.CreateFactory();
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        return serviceProvider.GetRequiredService<IS3ClientFactory>();
    }


    [TearDown]
    public void CleanUp()
    {
        if (_lastResource is not null)
        {
            var configKey = ResourceTypeConfiguration.ResourceTypeRds.SpecificType ?? throw new Exception();
            var bucketName = _lastResource.Id.ToString();

            // Delete any remaining entries in the bucket
            try
            {
                using var s3Client = GetS3Factory().GetRdsS3Client(configKey);

                ListObjectsV2Request request = new()
                {
                    BucketName = bucketName
                };

                ListObjectsV2Response response;
                do
                {
                    response = s3Client.ListObjectsV2Async(request).Result;
                    foreach (S3Object entry in response.S3Objects)
                    {
                        s3Client.DeleteObjectAsync(new DeleteObjectRequest
                        {
                            BucketName = bucketName,
                            Key = entry.Key
                        });
                    }

                    request.ContinuationToken = response.NextContinuationToken;
                } while (response.IsTruncated);
            }
            catch (Exception)
            {

            }

            // Delete the bucket
            try
            {
                var ecsFactory = GetEcsFactory();
                ecsFactory.GetRdsEcsManager(configKey).DeleteBucket(bucketName).Wait();
            }
            catch (Exception)
            {

            }
        }
    }

    [Test, Explicit]
    public async Task GetBlob_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        const string key = "test.txt";
        const string blobContent = "CoScIne Rocks!";

        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(blobContent);
        writer.Flush();
        stream.Position = 0;

        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeRds, createDataStorageParameters: new CreateDataStorageParameters { Quota = 1 });

        var projectId = resource.ProjectResources.First().ProjectId;

        _lastResource = resource;

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var ds = serviceProvider.GetRequiredService<IDataStorageRepositoryFactory>().Create(resource.TypeId)!;

        await ds.CreateAsync(new CreateBlobParameters
        {
            Resource = resource,
            Blob = stream,
            Path = key
        });

        // Act
        var response = await client.GetAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsStringAsync();

        Assert.That(content, Is.EqualTo(blobContent));
        Assert.That(response.Content.Headers.ContentType?.ToString(), Is.EqualTo("text/plain"));
        Assert.That(response.Content.Headers.ContentLength, Is.EqualTo(blobContent.Length));
    }

    [Test, Explicit]
    public async Task CreateBlob_ReturnsCreated()
    {
        // Arrange
        const string key = "new-blob.txt";
        const string fileContent = "This is a test blob content.";

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);
        var fileContentMock = new ByteArrayContent(fileBytes);

        fileContentMock.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");

        var formData = new MultipartFormDataContent { { fileContentMock, "file", key } };

        var treeRepositoryMock = Substitute.For<ITreeRepository>();

        // Ensure, that some kind of metadata is found
        treeRepositoryMock
            .GetNewestPagedMetadataAsync(Arg.Any<Guid>(), Arg.Any<MetadataTreeParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<MetadataTree>([new MetadataTree { Id = new Uri("https://coscine.test/id") }], 1, 1, 1)));

        var factory = CoscineTestExtensions.CreateFactory()
                .WithServiceInstance(treeRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeRds, createDataStorageParameters: new CreateDataStorageParameters { Quota = 1 });

        var projectId = resource.ProjectResources.First().ProjectId;
        _lastResource = resource;

        // Act
        var response = await client.PostAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}", formData);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));

        // You can also assert the location header if applicable
        var locationHeader = response.Headers.Location;
        Assert.That(locationHeader, Is.Not.Null);
        Assert.That(locationHeader?.ToString(), Does.Contain(key)); // Ensure the location header contains the key of the newly created blob
    }

    [Test, Explicit]
    public async Task DeleteBlob_ReturnsNoContent()
    {
        // Arrange
        const string key = $"{nameof(DeleteBlob_ReturnsNoContent)}.txt";
        string fileContent = Guid.NewGuid().ToString();

        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(fileContent);
        writer.Flush();
        stream.Position = 0;

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);

        var treeRepositoryMock = Substitute.For<ITreeRepository>();

        // Ensure, that some kind of metadata is found
        treeRepositoryMock
            .GetNewestPagedMetadataAsync(Arg.Any<Guid>(), Arg.Any<MetadataTreeParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<MetadataTree>([new MetadataTree { Id = new Uri("https://coscine.test/id") }], 1, 1, 1)));

        var factory = CoscineTestExtensions.CreateFactory()
                .WithServiceInstance(treeRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeRds, createDataStorageParameters: new CreateDataStorageParameters { Quota = 1 });
        _lastResource = resource;

        var projectId = resource.ProjectResources.First().ProjectId;

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var ds = serviceProvider.GetRequiredService<IDataStorageRepositoryFactory>().Create(resource.TypeId)!;

        await ds.CreateAsync(new CreateBlobParameters
        {
            Resource = resource,
            Blob = stream,
            Path = key
        });

        // Act
        var response = await client.DeleteAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var info = await ds.ReadAsync(new ReadTreeParameters
        {
            Resource = resource,
            Path = key
        });

        Assert.That(info, Is.Null);
    }

    [Test, Explicit]
    public async Task CreateBlob_QuotaExceeded_ReturnsBadRequest()
    {
        // Arrange
        const string key = "new-blob.txt";
        const string fileContent = "This is a test blob content.";

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);
        var fileContentMock = new ByteArrayContent(fileBytes);
        fileContentMock.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
        var formData = new MultipartFormDataContent { { fileContentMock, "file", key } };

        var dsMock = Substitute.For<IDataStorageRepository>();

        dsMock
            .GetStorageInfoAsync(Arg.Any<StorageInfoParameters>())
            .Returns(Task.FromResult(new DataStorageInfo { UsedSize = 2, AllocatedSize = 1 }));

        dsMock.TypeId.Returns(ResourceTypeConfiguration.ResourceTypeRds.Id);
        dsMock.Capabilities.Returns([Capability.UsesQuota]);

        var factory = CoscineTestExtensions.CreateFactory()
                .WithServiceInstance(dsMock);
        var client = await InitializeClientAsync(factory);

        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeRds);

        var projectId = resource.ProjectResources.First().ProjectId;
        _lastResource = resource;

        // Act
        var response = await client.PostAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}", formData);

        // Assert
        Assert.That(response.IsSuccessStatusCode, Is.False);
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

        // Read response content
        var content = await response.Content.ReadAsStringAsync();
        Assert.That(content, Does.Contain(nameof(QuotaLimitExceededBadRequestException)));
    }

    [Test, Explicit]
    public async Task CreateBlob_NoMetadataAvailable_ReturnsNotFound()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        const string key = "new-blob.txt";
        const string fileContent = "This is a test blob content.";

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);
        var fileContentMock = new ByteArrayContent(fileBytes);
        fileContentMock.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
        var formData = new MultipartFormDataContent { { fileContentMock, "file", key } };

        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeRds, createDataStorageParameters: new CreateDataStorageParameters { Quota = 1 });

        var projectId = resource.ProjectResources.First().ProjectId;
        _lastResource = resource;

        // Act
        var response = await client.PostAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}", formData);

        // Assert
        Assert.That(response.IsSuccessStatusCode, Is.False);
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

        // Read response content
        var content = await response.Content.ReadAsStringAsync();
        Assert.That(content, Does.Contain(nameof(MissingMetadataBadRequestException)));
    }

    [Test, Explicit]
    public async Task CreateBlob_FileAlreadyExists_ReturnsBadRequest()
    {
        // Arrange
        const string key = "new-blob.txt";
        const string fileContent = "This is a test blob content.";

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);
        var fileContentMock = new ByteArrayContent(fileBytes);
        fileContentMock.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
        var formData = new MultipartFormDataContent { { fileContentMock, "file", key } };

        var treeRepositoryMock = Substitute.For<ITreeRepository>();

        // Ensure, that some kind of metadata is found
        treeRepositoryMock
            .GetNewestPagedMetadataAsync(Arg.Any<Guid>(), Arg.Any<MetadataTreeParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<MetadataTree>([new MetadataTree { Id = new Uri("https://coscine.test/id") }], 1, 1, 1)));

        var dsMock = Substitute.For<IDataStorageRepository>();

        dsMock
            .ReadAsync(Arg.Any<ReadTreeParameters>())
            .Returns(Task.FromResult<StorageItemInfo?>(new StorageItemInfo { Path = "" }));

        dsMock.TypeId.Returns(ResourceTypeConfiguration.ResourceTypeRds.Id);
        dsMock.Capabilities.Returns([Capability.UsesQuota]);

        var factory = CoscineTestExtensions.CreateFactory()
                .WithServiceInstance(treeRepositoryMock)
                .WithServiceInstance(dsMock);
        var client = await InitializeClientAsync(factory);

        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeRds);

        var projectId = resource.ProjectResources.First().ProjectId;
        _lastResource = resource;

        // Act
        var response = await client.PostAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}", formData);

        // Assert
        Assert.That(response.IsSuccessStatusCode, Is.False);
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Conflict));

        // Read response content
        var content = await response.Content.ReadAsStringAsync();
        Assert.That(content, Does.Contain(nameof(BlobAlreadyExistsConflictException)));
    }


    [Explicit]
    [TestCase(@"\")]
    [TestCase(":")]
    [TestCase("?")]
    [TestCase("*")]
    [TestCase("<")]
    [TestCase(">")]
    [TestCase("|")]
    [TestCase("+")]
    [TestCase("#")]
    public async Task CreateBlob_WithForbiddenCharsInKey_ThrowsForbiddenCharInKeyBadRequest(string forbiddenChar)
    {
        // Arrange
        forbiddenChar = HttpUtility.UrlEncode(forbiddenChar);
        
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        string key = $"new{forbiddenChar}blob.txt"; // Injecting a forbidden character into the key
        const string fileContent = "This is a test blob content.";

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);
        var fileContentMock = new ByteArrayContent(fileBytes);
        fileContentMock.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
        var formData = new MultipartFormDataContent { { fileContentMock, "file", key } };

        var resources = await factory.SeedResourcesAsync(count: 1, resourceType: ResourceTypeConfiguration.ResourceTypeRds);

        var projectId = resources.First().ProjectResources.First().ProjectId;
        var resource = resources.First();
        _lastResource = resource;

        // Act
        var response = await client.PostAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}", formData);

        // Assert
        Assert.That(response.IsSuccessStatusCode, Is.False);
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        var content = await response.Content.ReadAsStringAsync();
        Assert.That(content, Does.Contain(nameof(ForbiddenCharInKeyBadRequest)));
    }
}
