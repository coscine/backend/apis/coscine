﻿using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.Conflict;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Helpers;
using Coscine.Api.Core.Shared.RequestFeatures;
using NSubstitute;
using NUnit.Framework;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests.BlobControllerTests;

[TestFixture]
public class BlobControllerLinkedTests : CoscineControllerTestsBase
{
    /// <summary>
    /// A list of forbidden characters that are not allowed in blob path keys.
    /// </summary>
    /// <remarks>This list <b>must be kept in sync</b> with the RegEx defined in the <see cref="ForbiddenChars"/> class.</remarks>
    private static readonly string[] _forbiddenChars =
    [
        HttpUtility.UrlEncode("\\"),
        HttpUtility.UrlEncode(":"),
        HttpUtility.UrlEncode("?"),
        HttpUtility.UrlEncode("*"),
        HttpUtility.UrlEncode("<"),
        HttpUtility.UrlEncode(">"),
        HttpUtility.UrlEncode("|"),
        HttpUtility.UrlEncode("+"),
        HttpUtility.UrlEncode("#")
    ];

    [TearDown]
    public void CleanUp()
    {
        // In memory
    }

    [Test]
    public async Task GetBlob_ReturnsOk()
    {
        // Arrange
        const string key = "new-entry";
        const string fileContent = "https://example.org";
        var uri = new Uri("https://coscine.test/data");

        var treeRepositoryMock = Substitute.For<ITreeRepository>();

        // Map to the correct graph
        treeRepositoryMock
            .GetNewestPagedDataAsync(Arg.Any<Guid>(), Arg.Any<DataTreeParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<DataTree>([new DataTree { Id = uri }], 1, 1, 1)));

        // Replace service
        var factory = CoscineTestExtensions.CreateFactory()
                .WithServiceInstance(treeRepositoryMock);
        var client = await InitializeClientAsync(factory);

        await factory.SeedLinkedDataEntryAsync(fileContent, uri);

        // Create resource
        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeLinked);

        var projectId = resource.ProjectResources.First().ProjectId;

        // Act
        var response = await client.GetAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsStringAsync();

        Assert.That(content, Is.EqualTo(fileContent));
        Assert.That(response.Content.Headers.ContentType?.ToString(), Is.EqualTo("application/octet-stream"));
        Assert.That(response.Content.Headers.ContentLength, Is.EqualTo(fileContent.Length));
    }

    [Test]
    public async Task CreateBlob_ReturnsCreated()
    {
        // Arrange
        const string key = "new-entry";
        const string fileContent = "https://example.org";

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);
        var fileContentMock = new ByteArrayContent(fileBytes);

        fileContentMock.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");

        var formData = new MultipartFormDataContent { { fileContentMock, "file", key } };

        var treeRepositoryMock = Substitute.For<ITreeRepository>();

        // Ensure, that some kind of metadata is found
        treeRepositoryMock
            .GetNewestPagedMetadataAsync(Arg.Any<Guid>(), Arg.Any<MetadataTreeParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<MetadataTree>([new MetadataTree { Id = new Uri("https://coscine.test/id") }], 1, 1, 1)));
        treeRepositoryMock
            .GetNewestPagedDataAsync(Arg.Any<Guid>(), Arg.Any<DataTreeParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<DataTree>([new DataTree { Id = new Uri("https://coscine.test/id") }], 1, 1, 1)));

        var factory = CoscineTestExtensions.CreateFactory()
                .WithServiceInstance(treeRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeLinked);

        var projectId = resource.ProjectResources.First().ProjectId;

        // Act
        var response = await client.PostAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}", formData);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));

        // You can also assert the location header if applicable
        var locationHeader = response.Headers.Location;
        Assert.That(locationHeader, Is.Not.Null);
        Assert.That(locationHeader?.ToString(), Does.Contain(key)); // Ensure the location header contains the key of the newly created blob
    }

    [Test]
    public async Task CreateBlob_NoMetadataAvailable_ReturnsNotFound()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        const string key = "new-blob.txt";
        const string fileContent = "This is a test blob content.";

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);
        var fileContentMock = new ByteArrayContent(fileBytes);
        fileContentMock.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
        var formData = new MultipartFormDataContent { { fileContentMock, "file", key } };

        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeLinked);

        var projectId = resource.ProjectResources.First().ProjectId;

        // Act
        var response = await client.PostAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}", formData);

        // Assert        
        Assert.That(response.IsSuccessStatusCode, Is.False);
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

        var content = await response.Content.ReadAsStringAsync();
        Assert.That(content, Does.Contain(nameof(MissingMetadataBadRequestException)));
    }

    [Test]
    public async Task CreateBlob_FileAlreadyExists_ReturnsBadRequest()
    {
        // Arrange
        const string key = "new-entry";
        const string fileContent = "https://example.org";
        var uri = new Uri("https://coscine.test/data");

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);
        var fileContentMock = new ByteArrayContent(fileBytes);
        fileContentMock.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
        var formData = new MultipartFormDataContent { { fileContentMock, "file", key } };

        var treeRepositoryMock = Substitute.For<ITreeRepository>();

        // Map to the correct graph
        treeRepositoryMock
            .GetNewestPagedDataAsync(Arg.Any<Guid>(), Arg.Any<DataTreeParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<DataTree>([new DataTree { Id = uri }], 1, 1, 1)));

        var factory = CoscineTestExtensions.CreateFactory()
                .WithServiceInstance(treeRepositoryMock);
        var client = await InitializeClientAsync(factory);

        await factory.SeedLinkedDataEntryAsync(fileContent, uri);

        // Create resource
        var resource = await factory.SeedResourceAsync(resourceType: ResourceTypeConfiguration.ResourceTypeLinked);

        var projectId = resource.ProjectResources.First().ProjectId;

        // Act
        var response = await client.PostAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}", formData);

        // Assert
        Assert.That(response.IsSuccessStatusCode, Is.False);
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Conflict));

        // Read response content
        var content = await response.Content.ReadAsStringAsync();
        Assert.That(content, Does.Contain(nameof(BlobAlreadyExistsConflictException)));
    }

    [TestCase(@"\")]
    [TestCase(":")]
    [TestCase("?")]
    [TestCase("*")]
    [TestCase("<")]
    [TestCase(">")]
    [TestCase("|")]
    [TestCase("+")]
    [TestCase("#")]
    public async Task CreateBlob_WithForbiddenCharsInKey_ThrowsForbiddenCharInKeyBadRequest(string forbiddenChar)
    {
        // Arrange
        forbiddenChar = HttpUtility.UrlEncode(forbiddenChar);
        
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        string key = $"new{forbiddenChar}blob.txt"; // Injecting a forbidden character into the key
        const string fileContent = "This is a test blob content.";

        var fileBytes = Encoding.UTF8.GetBytes(fileContent);
        var fileContentMock = new ByteArrayContent(fileBytes);
        fileContentMock.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
        var formData = new MultipartFormDataContent { { fileContentMock, "file", key } };

        var resources = await factory.SeedResourcesAsync(count: 1, resourceType: ResourceTypeConfiguration.ResourceTypeLinked);

        var projectId = resources.First().ProjectResources.First().ProjectId;
        var resource = resources.First();

        // Act
        var response = await client.PostAsync($"/api/v2/projects/{projectId}/resources/{resource.Id}/blobs/{key}", formData);

        // Assert
        Assert.That(response.IsSuccessStatusCode, Is.False);
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));
        var content = await response.Content.ReadAsStringAsync();
        Assert.That(content, Does.Contain(nameof(ForbiddenCharInKeyBadRequest)));
    }
}
