﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NSubstitute;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class PidControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetPid_ReturnsOk()
    {
        // Arrange
        var pidRepositoryMock = Substitute.For<IPidRepository>();
        var factory = CoscineTestExtensions.CreateFactory().WithServiceInstance(pidRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var projects = await factory.SeedProjectsAsync();

        var pidConfig = factory.Services.GetService<IOptionsMonitor<PidConfiguration>>()?.CurrentValue;
        var prefix = pidConfig?.Prefix;
        var id = projects.First().Id;

        pidRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<bool>(), Arg.Any<bool>())
            .Returns(
                Task.FromResult<Pid?>(
                    new Pid
                    {
                        Prefix = prefix,
                        Suffix = id.ToString(),
                        Type = PidType.Project,
                        IsEntityValid = true,
                    }
                )
            );

        // Act
        var response = await client.GetAsync($"/api/v2.0/pids/{prefix}/{id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<PidDto>>();

        Assert.That(content, Is.Not.Null);
        Assert.That(content.Data, Is.Not.Null);
        Assert.That(content.Data?.IsEntityValid, Is.True);
        Assert.That(content.Data?.Type, Is.EqualTo(PidType.Project));
        Assert.That(content.Data?.Type, Is.Not.EqualTo(PidType.Resource));
    }

    [Test]
    public async Task SendEmailToOwner_ReturnsNoContent()
    {
        // Arrange
        var pidRepositoryMock = Substitute.For<IPidRepository>();
        
        var factory = CoscineTestExtensions.CreateFactory().WithServiceInstance(pidRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var projects = await factory.SeedProjectsAsync();

        var pidConfig = factory.Services.GetService<IOptionsMonitor<PidConfiguration>>()?.CurrentValue;
        var prefix = pidConfig?.Prefix;
        var id = projects.First().Id;

        pidRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<bool>(), Arg.Any<bool>())
            .Returns(
                Task.FromResult<Pid?>(
                    new Pid
                    {
                        Prefix = prefix,
                        Suffix = id.ToString(),
                        Type = PidType.Project
                    }
                )
            );

        var pidRequestDto = new PidRequestDto
        {
            Name = "Test DisplayName",
            Email = "hunke@itc.rwth-aachen.de",
            Message = "This is a test message",
            SendConfirmationEmail = true
        };

        // Act
        var response = await client.PostAsJsonAsync($"/api/v2.0/pids/{prefix}/{id}/requests", pidRequestDto);
        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));
    }
}
