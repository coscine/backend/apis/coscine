﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class LanguageControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetLanguage_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var languages = await factory.SeedLanguagesAsync();

        // Act
        var response = await client.GetAsync($"/api/v2.0/languages/{languages.First().Id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<LanguageDto>>();

        Assert.That(content.Data?.Id, Is.EqualTo(languages.First().Id));
    }

    [Test]
    public async Task GetLanguages_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        // Act
        var response = await client.GetAsync("/api/v2.0/languages");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<IEnumerable<LanguageDto>>>();

        Assert.That(content.Data, Has.Count.EqualTo(2));
    }
}