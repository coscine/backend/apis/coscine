
using System.Net;
using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NSubstitute.Extensions;
using NUnit.Framework;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class ProjectPublicationRequestControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetPublicationRequest_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var projectPublicationRequests = await factory.SeedProjectPublicationRequestsAsync();
        var publicationRequest = projectPublicationRequests.First();
        var project = projectPublicationRequests.First().Project;

        // Act
        var response = await client.GetAsync($"/api/v2.0/projects/{project.Id}/publications/requests/{publicationRequest.Id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<ProjectPublicationRequestDto>>();

        Assert.That(content.Data, Is.Not.Null, "Content data should not be null");
        Assert.That(content.Data?.Id, Is.EqualTo(publicationRequest.Id), "Content ID does not match the expected Publication Request ID");
        Assert.That(content.Data?.Message, Is.EqualTo(publicationRequest.Message), "Content message does not match the expected Publication Request message");
        Assert.That(content.Data?.Creator.Id, Is.EqualTo(publicationRequest.CreatorId), "Content Creator ID does not match the expected Publication Request Creator ID");
        Assert.That(content.Data?.PublicationServiceRorId, Is.EqualTo(publicationRequest.PublicationServiceRorId), "Content Publication Service RoR ID does not match the expected Publication Request Publication Service RoR ID");
        Assert.That(content.Data?.Project.Id, Is.EqualTo(project.Id), "Content Project ID does not match the expected Project ID");
        Assert.That(content.Data?.DateCreated, Is.EqualTo(publicationRequest.DateCreated).Within(1).Minutes, "Content Date Created does not match the expected Publication Request Date Created");
        Assert.That(content.Data?.Resources.Select(r => r.Id), Is.EquivalentTo(publicationRequest.Resources.Select(r => r.Id)), "Content resources do not match the expected Publication Request resources");
    }

    [Test]
    public async Task CreatePublicationRequest_ReturnsCreated()
    {
        // Arrange
        var publicationAdvisoryServiceRepositoryMock = Substitute.For<IPublicationAdvisoryServiceRepository>();
        publicationAdvisoryServiceRepositoryMock
            .Configure()
            .GetPublicationAdvisoryServiceAsync(Arg.Any<Uri>(), Arg.Any<AcceptedLanguage>())
            .ReturnsForAnyArgs(Task.FromResult<PublicationAdvisoryService?>(new PublicationAdvisoryService()
            {
                DisplayName = "Test Service",
                Email = "test@mail.com"
            }));

        var quotaServiceMock = Substitute.For<IQuotaService>();
        quotaServiceMock
            .GetQuotaForResourceForProjectAsync(Arg.Any<Guid>(), Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(Task.FromResult(new ResourceQuotaDto()
            {
                Resource = new() { Id = Guid.NewGuid() },
                Used = new() { Value = 1, Unit = QuotaUnit.GibiBYTE },
            }));

        var factory = CoscineTestExtensions.CreateFactory()
            .WithServiceInstance(publicationAdvisoryServiceRepositoryMock)
            .WithServiceInstance(quotaServiceMock);
        var client = await InitializeClientAsync(factory);

        var resourcesDb = await factory.SeedResourcesAsync();
        var projectId = resourcesDb.First().ProjectResources.First().ProjectId;
        var resource = resourcesDb.First(r => r.ProjectResources.Any(pr => pr.ProjectId == projectId));

        var publicationRequestForCreationDto = new PublicationRequestForCreationDto
        {
            PublicationServiceRorId = "https://test.com/rorUri",
            ResourceIds = [resource.Id],
            Message = "Test message"
        };

        // Act
        var response = await client.PostAsJsonAsync($"/api/v2.0/projects/{projectId}/publications/requests", publicationRequestForCreationDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));

        var content = await response.Content.ReadAsAsync<Response<ProjectPublicationRequestDto>>();

        Assert.That(content.Data?.Id, Is.Not.EqualTo(Guid.Empty), "Content ID should not be an empty GUID");
        Assert.That(content.Data?.PublicationServiceRorId.ToString(), Is.EqualTo(publicationRequestForCreationDto.PublicationServiceRorId), "Content Publication Service RoR ID does not match the expected Publication Request Creation DTO Publication Service RoR ID");
        Assert.That(content.Data?.Project.Id, Is.EqualTo(projectId), "Content Project ID does not match the expected Project ID");
        Assert.That(content.Data?.DateCreated, Is.InRange(DateTime.UtcNow.AddMinutes(-1), DateTime.UtcNow), "Content Date Created is not within the expected time range");
        Assert.That(content.Data?.Resources.Select(r => r.Id), Is.EquivalentTo(publicationRequestForCreationDto.ResourceIds), "Content resources do not match the expected Publication Request Creation DTO resources");
        Assert.That(content.Data?.Message, Is.EqualTo(publicationRequestForCreationDto.Message), "Content message does not match the expected Publication Request Creation DTO message");
    }
}