﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class ProjectMemberControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetProjectMemberships_ReturnsAllMemberships()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var users = await factory.SeedUsersAsync();
        var project = (await factory.SeedProjectsAsync()).First();

        var projectRoles = new List<ProjectRole>();

        foreach (var user in users)
        {
            projectRoles.Add(await factory.SeedProjectRoleAsync(DatabaseSeeder.Owner.Id, project.Id, user.Id));
        }

        // Act
        var response = await client.GetAsync($"/api/v2.0/projects/{project.Id}/members");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<ProjectRoleDto>>();

        Assert.That(content.Data, Has.Count.EqualTo(projectRoles.Count + 1));
    }

    [Test]
    public async Task GetProjectMembership_ReturnsCorrectMembership()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var users = await factory.SeedUsersAsync();
        var project = (await factory.SeedProjectsAsync()).First();

        var projectRoles = new List<ProjectRole>();

        foreach (var user in users)
        {
            projectRoles.Add(await factory.SeedProjectRoleAsync(DatabaseSeeder.Owner.Id, project.Id, user.Id));
        }

        // Act
        var response = await client.GetAsync($"/api/v2.0/projects/{project.Id}/members/{projectRoles[0].RelationId}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<ProjectRoleDto>>();

        Assert.That(content.Data?.User.Id, Is.EqualTo(users.First().Id));
    }

    [Test]
    public async Task UpdateProjectMembership_UpdatesMembership()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var users = await factory.SeedUsersAsync();
        var project = (await factory.SeedProjectsAsync()).First();

        var projectRoles = new List<ProjectRole>();

        foreach (var user in users)
        {
            projectRoles.Add(await factory.SeedProjectRoleAsync(DatabaseSeeder.Owner.Id, project.Id, user.Id));
        }

        var projectRoleDto = new ProjectRoleForProjectManipulationDto
        {
            RoleId = DatabaseSeeder.Member.Id,
        };

        // Act
        var response = await client.PutAsJsonAsync($"/api/v2.0/projects/{project.Id}/members/{projectRoles[0].RelationId}", projectRoleDto);

        // Assert

        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var content = await response.Content.ReadAsStreamAsync();

        Assert.That(content.Length, Is.EqualTo(0));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        Assert.That(dbContext.ProjectRoles.Where(x => x.UserId == users.First().Id).First().Role.Id, Is.EqualTo(DatabaseSeeder.Member.Id));
    }

    [Test]
    public async Task CreateProjectMembership_CreatesMembership()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var users = await factory.SeedUsersAsync();
        var project = (await factory.SeedProjectsAsync()).First();

        var projectRoleDto = new ProjectRoleForProjectCreationDto
        {
            RoleId = DatabaseSeeder.Owner.Id,
            UserId = users.First().Id
        };

        // Act
        var response = await client.PostAsJsonAsync($"/api/v2.0/projects/{project.Id}/members", projectRoleDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));

        var content = await response.Content.ReadAsAsync<Response<ProjectRoleDto>>();

        Assert.That(content.Data?.User.Id, Is.EqualTo(users.First().Id));
    }

    [Test]
    public async Task DeleteProjectMembershipById_DeletesMembership()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        
        var users = await factory.SeedUsersAsync();
        var project = (await factory.SeedProjectsAsync()).First();

        var projectRoles = new List<ProjectRole>();

        foreach (var user in users)
        {
            projectRoles.Add(await factory.SeedProjectRoleAsync(DatabaseSeeder.Owner.Id, project.Id, user.Id));
        }

        // Act
        var response = await client.DeleteAsync($"/api/v2.0/projects/{project.Id}/members/{projectRoles[0].RelationId}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var content = await response.Content.ReadAsStreamAsync();

        Assert.That(content.Length, Is.EqualTo(0));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        Assert.That(dbContext.ProjectRoles.Where(x => x.UserId == users.First().Id).Any(), Is.False);
    }
}