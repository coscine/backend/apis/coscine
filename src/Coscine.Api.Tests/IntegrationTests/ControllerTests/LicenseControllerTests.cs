﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class LicenseControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetLicense_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var licenses = await factory.SeedLicensesAsync();

        // Act
        var response = await client.GetAsync($"/api/v2.0/licenses/{licenses.First().Id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<LicenseDto>>();

        Assert.That(content.Data?.Id, Is.EqualTo(licenses.First().Id));
    }

    [Test]
    public async Task GetLicenses_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        
        var licenses = await factory.SeedLicensesAsync();

        // Act
        var response = await client.GetAsync("/api/v2.0/licenses");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<LicenseDto>>();

        Assert.That(content.Pagination?.TotalCount, Is.GreaterThan(0));
    }
}