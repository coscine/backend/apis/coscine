﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class ToSMiddlewareTests : CoscineControllerTestsBase
{
    [Test]
    public async Task Skip_ToS_Check_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        // Clear the tos accepted
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();
        dbContext.Tosaccepteds.RemoveRange(dbContext.Tosaccepteds);
        dbContext.SaveChanges();

        // Act
        var response = await client.GetAsync("/api/v2.0/tos");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<TermsOfServiceDto>>();

        Assert.That(content.Data?.Version, Is.Not.EqualTo(string.Empty));
        Assert.That(content.Data?.Version, Is.Not.Null);
    }

    [Test]
    public async Task Skip_ToS_Check_Anonymous_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        // Clear the tos accepted
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();
        dbContext.Tosaccepteds.RemoveRange(dbContext.Tosaccepteds);
        dbContext.SaveChanges();

        // Unauthorize the client
        client.DefaultRequestHeaders.Authorization = null;

        // Act
        var response = await client.GetAsync("/api/v2.0/disciplines");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<DisciplineDto>>();

        Assert.That(content.Pagination?.TotalCount, Is.GreaterThan(0));
    }

    [Test]
    public async Task Check_ToS_ReturnsBadRequest()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);


        var projects = await factory.SeedProjectsAsync();

        // Clear the tos accepted
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();
        dbContext.Tosaccepteds.RemoveRange(dbContext.Tosaccepteds);
        dbContext.SaveChanges();

        // Act
        var response = await client.GetAsync($"/api/v2.0/projects/{projects.First().Id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

        var content = await response.Content.ReadAsStringAsync();

        Assert.That(content, Does.Contain(nameof(TermsOfServicesNotAcceptedBadRequestException)));
    }
}
