﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Microsoft.Extensions.DependencyInjection;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using System.Net;
using VDS.RDF;
using VDS.RDF.Query;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class ProjectControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetProject_ReturnsOk()
    {
        // Arrange
        var organizationRepositoryMock = Substitute.For<IOrganizationRepository>();
        organizationRepositoryMock
            .GetByRorUrlAsync(Arg.Any<Uri>())
            .Returns(
                new Organization
                {
                    DisplayName = "Test Organization",
                    Email = "test@test.de",
                    RwthIkz = "123456",
                    Uri = new Uri("https://www.test.de/"),
                }
            );

        var factory = CoscineTestExtensions.CreateFactory().WithServiceInstance(organizationRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var projects = await factory.SeedProjectsAsync();

        // Act
        var response = await client.GetAsync($"/api/v2.0/projects/{projects.First().Id}");

        // Assert
        Assert.That(HttpStatusCode.OK, Is.EqualTo(response.StatusCode));

        var content = await response.Content.ReadAsAsync<Response<ProjectDto>>();

        Assert.That(content.Data, Is.Not.Null);
        Assert.That(projects.First().Id, Is.EqualTo(content.Data?.Id));
        Assert.That(projects.First().DisplayName, Is.EqualTo(content.Data?.DisplayName));
    }

    [Test]
    public async Task GetProjects_ReturnsOk()
    {
        // Arrange
        var organizationRepositoryMock = Substitute.For<IOrganizationRepository>();
        organizationRepositoryMock
            .GetByRorUrlAsync(Arg.Any<Uri>())
            .Returns(
                new Organization
                {
                    DisplayName = "Test Organization",
                    Email = "test@test.de",
                    RwthIkz = "123456",
                    Uri = new Uri("https://www.test.de/"),
                }
            );

        var factory = CoscineTestExtensions.CreateFactory().WithServiceInstance(organizationRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var projects = await factory.SeedProjectsAsync();

        // Act
        var response = await client.GetAsync("/api/v2.0/projects/");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<DisciplineDto>>();

        Assert.That(content.Data, Has.Count.EqualTo(projects.Count()));
    }

    [Test]
    public async Task DeleteProject_ReturnsNoContent()
    {
        // Arrange
        var handleRepositoryMock = Substitute.For<IHandleRepository>();
        handleRepositoryMock
            .UpdateAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<Handle>())
            .Returns(Task.CompletedTask);
        handleRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>())
            .Returns(Task.FromResult<Handle?>(new Handle()
            {
                Pid = new Pid() { Prefix = "t3st", Suffix = "p1d" },
                Values = [new() { Type = "URL", Data = "https://test.com", ParsedData = "" }]
            }));

        var factory = CoscineTestExtensions.CreateFactory().WithServiceInstance(handleRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var projects = await factory.SeedProjectsAsync();
        var project = projects.First();

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;

        var projectRepository = serviceProvider.GetRequiredService<IProjectRepository>();
        await projectRepository.CreateGraphsAsync(project);

        // Act
        var response = await client.DeleteAsync($"/api/v2.0/projects/{project.Id}/");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var content = await response.Content.ReadAsStreamAsync();

        Assert.That(content.Length, Is.EqualTo(0));

        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        Assert.That(dbContext.Projects.Where(x => x.Id == project.Id && x.Deleted).Count, Is.EqualTo(1));

        // Check if the rdf graph was deleted
        var projectRdfRepository = serviceProvider.GetRequiredService<IProjectRdfRepository>();

        var projectRdf = projectRdfRepository.Get(project.Id);

        Assert.That(projectRdf, Is.Null);

        var rdfRepositoryBase = serviceProvider.GetRequiredService<IRdfRepositoryBase>();

        var trellisGraph = await rdfRepositoryBase.GetGraph(RdfUris.TrellisGraph);
        var subjects = trellisGraph.Triples.Select(x => (x.Subject as UriNode)?.Uri).Distinct();

        Assert.That(subjects, Does.Not.Contain(projectRepository.GenerateGraphName(project)));
    }

    [Test]
    public async Task CreateProject_ReturnsCreated()
    {
        // Arrange
        var handleRepositoryMock = Substitute.For<IHandleRepository>();
        handleRepositoryMock
            .UpdateAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<Handle>())
            .Returns(Task.CompletedTask);
        handleRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>())
            .Returns(Task.FromResult<Handle?>(new Handle()
            {
                Pid = new Pid() { Prefix = "t3st", Suffix = "p1d" },
                Values = [new() { Type = "URL", Data = "https://test.com", ParsedData = "" }]
            }));

        var defaultProjectQuotaRepository = Substitute.For<IDefaultProjectQuotaRepository>();
        defaultProjectQuotaRepository
            .GetAllAsync(Arg.Any<Guid>())
            .Returns([]);

        var factory = CoscineTestExtensions.CreateFactory()
                .WithServiceInstance(handleRepositoryMock)
                .WithServiceInstance(defaultProjectQuotaRepository);
        var client = await InitializeClientAsync(factory);

        var visibilities = await factory.SeedVisibilitiesAsync();
        var disciplines = await factory.SeedDisciplinesAsync();

        var projectForCreationDto = new ProjectForCreationDto
        {
            Name = Guid.NewGuid().ToString(),
            Description = "This is a test project",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddDays(7),
            Keywords = ["first", "second"],
            DisplayName = "Test Project",
            PrincipleInvestigators = "Investigator",
            GrantId = "Grant_123",
            Visibility = new VisibilityForProjectManipulationDto { Id = visibilities.First().Id },
            Disciplines =
            [
                new() { Id = disciplines.First().Id }
            ],
            Organizations =
            [
                new() { Uri = new Uri("https://test.com/rorUri"), Responsible = true }
            ]
        };

        // Act
        var response = await client.PostAsJsonAsync("/api/v2.0/projects/", projectForCreationDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));

        var content = await response.Content.ReadAsAsync<Response<ProjectDto>>();

        Assert.That(content.Data, Is.Not.Null);
        Assert.That(content.Data?.DisplayName, Is.EqualTo(projectForCreationDto.DisplayName));
        Assert.That(content.Data?.Creator, Is.Not.Null);

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        Assert.That(dbContext.Projects.Where(x => x.ProjectName == projectForCreationDto.Name).Count() == 1, Is.True);

        var project = dbContext.Projects.First(x => x.ProjectName == projectForCreationDto.Name);

        // Check for the correct rdf values
        var projectRdfRepository = serviceProvider.GetRequiredService<IProjectRdfRepository>();

        var projectRdf = projectRdfRepository.Get(project.Id);

        Assert.That(projectRdf, Is.Not.Null, "ProjectRdf instance should not be null.");
        Assert.That(projectRdf?.DisplayName, Is.EqualTo(projectForCreationDto.DisplayName),
                    "DisplayName in ProjectRdf does not match the expected DisplayName from ProjectForCreationDto.");
        Assert.That(projectRdf?.Description, Is.EqualTo(projectForCreationDto.Description),
                    "Description in ProjectRdf does not match the expected Description from ProjectForCreationDto.");
        Assert.That(projectRdf?.StartDate, Is.EqualTo(projectForCreationDto.StartDate).Within(TimeSpan.FromSeconds(1)),
                    "StartDate in ProjectRdf does not match the expected StartDate from ProjectForCreationDto.");
        Assert.That(projectRdf?.EndDate, Is.EqualTo(projectForCreationDto.EndDate).Within(TimeSpan.FromSeconds(1)),
                    "EndDate in ProjectRdf does not match the expected EndDate from ProjectForCreationDto.");
        Assert.That(projectRdf?.Keywords, Is.EquivalentTo(projectForCreationDto.Keywords),
                    "Keywords in ProjectRdf do not match the expected Keywords from ProjectForCreationDto.");
        Assert.That(projectRdf?.PrincipleInvestigators, Is.EqualTo(projectForCreationDto.PrincipleInvestigators),
                    "PrincipleInvestigators in ProjectRdf do not match the expected PrincipleInvestigators from ProjectForCreationDto.");
        Assert.That(projectRdf?.GrantId, Is.EqualTo(projectForCreationDto.GrantId),
                    "GrantId in ProjectRdf does not match the expected GrantId from ProjectForCreationDto.");
        Assert.That(projectRdf?.VisibilityId, Is.EqualTo(RdfUris.CoscineTermsVisibilityProjectMember),
                    "VisibilityId in ProjectRdf does not match the expected VisibilityId from ProjectForCreationDto.");
        Assert.That(projectRdf?.OrganizationIds, Is.EquivalentTo(projectForCreationDto.Organizations.Select(x => x.Uri)),
                    "OrganizationId in ProjectRdf does not match the expected OrganizationId from ProjectForCreationDto.");

        var rdfRepositoryBase = serviceProvider.GetRequiredService<IRdfRepositoryBase>();

        var trellisGraph = await rdfRepositoryBase.GetGraph(RdfUris.TrellisGraph);

        Assert.That(trellisGraph, Is.Not.Null);
        Assert.That(trellisGraph.IsEmpty, Is.Not.True);

        var subjects = trellisGraph.Triples.Select(x => (x.Subject as UriNode)?.Uri).Distinct();

        var projectRepository = serviceProvider.GetRequiredService<IProjectRepository>();
        Assert.That(subjects, Does.Contain(projectRepository.GenerateGraphName(project)));
    }

    [Test]
    public async Task CreateProject_FailGraphCreate_ReturnsInternalServerError()
    {
        // Arrange
        var handleRepositoryMock = Substitute.For<IHandleRepository>();
        handleRepositoryMock
            .UpdateAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<Handle>())
            .Throws(new Exception("Failed to update!"));
        handleRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>())
            .Returns(Task.FromResult<Handle?>(new Handle()
            {
                Pid = new Pid() { Prefix = "t3st", Suffix = "p1d" },
                Values = [new() { Type = "URL", Data = "https://test.com", ParsedData = "" }]
            }));

        var defaultProjectQuotaRepository = Substitute.For<IDefaultProjectQuotaRepository>();
        defaultProjectQuotaRepository
            .GetAllAsync(Arg.Any<Guid>())
            .Returns([]);

        var factory = CoscineTestExtensions.CreateFactory()
                        .WithServiceInstance(handleRepositoryMock)
                        .WithServiceInstance(defaultProjectQuotaRepository);
        var client = await InitializeClientAsync(factory);

        var visibilities = await factory.SeedVisibilitiesAsync();
        var disciplines = await factory.SeedDisciplinesAsync();

        var projectForCreationDto = new ProjectForCreationDto
        {
            Name = Guid.NewGuid().ToString(),
            Description = "This is a test project",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddDays(7),
            Keywords = ["first", "second"],
            DisplayName = "Test Project",
            PrincipleInvestigators = "Investigator",
            GrantId = "Grant_123",
            Visibility = new VisibilityForProjectManipulationDto { Id = visibilities.First().Id },
            Disciplines =
            [
                new() { Id = disciplines.First().Id }
            ],
            Organizations =
            [
                new() { Uri = new Uri("https://test.com/rorUri"), Responsible = true }
            ]
        };

        // Act
        var response = await client.PostAsJsonAsync("/api/v2.0/projects/", projectForCreationDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.InternalServerError));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        Assert.That(dbContext.Projects.Any(x => x.ProjectName == projectForCreationDto.Name), Is.False);

        // Check for the correct rdf values
        var rdfRepository = serviceProvider.GetRequiredService<IRdfRepositoryBase>();

        var query = new SparqlParameterizedString
        {
            CommandText = @"select * where {?s <http://purl.org/dc/terms/title> @DisplayName}"
        };

        query.SetLiteral("DisplayName", projectForCreationDto.DisplayName, normalizeValue: false);

        var result = await rdfRepository.RunQueryAsync(query);

        Assert.That(result.Count, Is.EqualTo(0));
    }

    [Test]
    public async Task CreateProjectMultipleOrganizations_ReturnsCreated()
    {
        // Arrange
        var handleRepositoryMock = Substitute.For<IHandleRepository>();
        handleRepositoryMock
            .UpdateAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<Handle>())
            .Returns(Task.CompletedTask);
        handleRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>())
            .Returns(Task.FromResult<Handle?>(new Handle()
            {
                Pid = new Pid() { Prefix = "t3st", Suffix = "p1d" },
                Values = [new() { Type = "URL", Data = "https://test.com", ParsedData = "" }]
            }));

        var defaultProjectQuotaRepository = Substitute.For<IDefaultProjectQuotaRepository>();
        defaultProjectQuotaRepository
            .GetAllAsync(Arg.Any<Guid>())
            .Returns([]);

        var factory = CoscineTestExtensions.CreateFactory()
                        .WithServiceInstance(handleRepositoryMock)
                        .WithServiceInstance(defaultProjectQuotaRepository);
        var client = await InitializeClientAsync(factory);

        var visibilities = await factory.SeedVisibilitiesAsync();
        var disciplines = await factory.SeedDisciplinesAsync();

        var projectForCreationDto = new ProjectForCreationDto
        {
            Name = Guid.NewGuid().ToString(),
            Description = "This is a test project",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddDays(7),
            Keywords = ["first", "second"],
            DisplayName = "Test Project",
            PrincipleInvestigators = "Investigator",
            GrantId = "Grant_123",
            Visibility = new VisibilityForProjectManipulationDto { Id = visibilities.First().Id },
            Disciplines =
            [
                new() { Id = disciplines.First().Id }
            ],
            Organizations =
            [
                new() { Uri = new Uri("https://new.test/rorUriResponsible"), Responsible = true },
                new() { Uri = new Uri("https://new.test/rorUriAdditional1"), Responsible = false },
                new() { Uri = new Uri("https://new.test/rorUriAdditional2"), Responsible = false },
            ]
        };

        // Act
        var response = await client.PostAsJsonAsync("/api/v2.0/projects/", projectForCreationDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.Created));

        var content = await response.Content.ReadAsAsync<Response<ProjectDto>>();

        Assert.That(content.Data, Is.Not.Null);
        Assert.That(content.Data?.DisplayName, Is.EqualTo(projectForCreationDto.DisplayName));
        Assert.That(content.Data?.Creator, Is.Not.Null);

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        Assert.That(dbContext.Projects.Where(x => x.ProjectName == projectForCreationDto.Name).Count() == 1, Is.True);
    }

    [Test]
    public async Task CreateProjectWithoutOrganizations_ReturnsUnprocessableEntity()
    {
        // Arrange
        var handleRepositoryMock = Substitute.For<IHandleRepository>();
        handleRepositoryMock
            .UpdateAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<Handle>())
            .Returns(Task.CompletedTask);
        handleRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>())
            .Returns(Task.FromResult<Handle?>(new Handle()
            {
                Pid = new Pid() { Prefix = "t3st", Suffix = "p1d" },
                Values = [new() { Type = "URL", Data = "https://test.com", ParsedData = "" }]
            }));

        var defaultProjectQuotaRepository = Substitute.For<IDefaultProjectQuotaRepository>();
        defaultProjectQuotaRepository
            .GetAllAsync(Arg.Any<Guid>())
            .Returns([]);

        var factory = CoscineTestExtensions.CreateFactory()
                        .WithServiceInstance(handleRepositoryMock)
                        .WithServiceInstance(defaultProjectQuotaRepository);
        var client = await InitializeClientAsync(factory);

        var visibilities = await factory.SeedVisibilitiesAsync();
        var disciplines = await factory.SeedDisciplinesAsync();

        var projectForCreationDto = new ProjectForCreationDto
        {
            Name = Guid.NewGuid().ToString(),
            Description = "This is a test project",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddDays(7),
            Keywords = ["first", "second"],
            DisplayName = "Test Project",
            PrincipleInvestigators = "Investigator",
            GrantId = "Grant_123",
            Visibility = new VisibilityForProjectManipulationDto { Id = visibilities.First().Id },
            Disciplines =
            [
                new() { Id = disciplines.First().Id }
            ],
            Organizations = []
        };

        // Act
        var response = await client.PostAsJsonAsync("/api/v2.0/projects/", projectForCreationDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.UnprocessableEntity));
    }

    [Test]
    public async Task CreateProjectWithoutResponsibleOrganization_ReturnsUnprocessableEntity()
    {
        // Arrange
        var handleRepositoryMock = Substitute.For<IHandleRepository>();
        handleRepositoryMock
            .UpdateAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<Handle>())
            .Returns(Task.CompletedTask);
        handleRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>())
            .Returns(Task.FromResult<Handle?>(new Handle()
            {
                Pid = new Pid() { Prefix = "t3st", Suffix = "p1d" },
                Values = [new() { Type = "URL", Data = "https://test.com", ParsedData = "" }]
            }));

        var defaultProjectQuotaRepository = Substitute.For<IDefaultProjectQuotaRepository>();
        defaultProjectQuotaRepository
            .GetAllAsync(Arg.Any<Guid>())
            .Returns([]);

        var factory = CoscineTestExtensions.CreateFactory()
                        .WithServiceInstance(handleRepositoryMock)
                        .WithServiceInstance(defaultProjectQuotaRepository);
        var client = await InitializeClientAsync(factory);

        var visibilities = await factory.SeedVisibilitiesAsync();
        var disciplines = await factory.SeedDisciplinesAsync();

        var projectForCreationDto = new ProjectForCreationDto
        {
            Name = Guid.NewGuid().ToString(),
            Description = "This is a test project",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddDays(7),
            Keywords = ["first", "second"],
            DisplayName = "Test Project",
            PrincipleInvestigators = "Investigator",
            GrantId = "Grant_123",
            Visibility = new VisibilityForProjectManipulationDto { Id = visibilities.First().Id },
            Disciplines =
            [
                new() { Id = disciplines.First().Id }
            ],
            Organizations =
            [
                new() { Uri = new Uri("https://new.test/rorUriAdditional1"), Responsible = false },
                new() { Uri = new Uri("https://new.test/rorUriAdditional2"), Responsible = false },
            ]
        };

        // Act
        var response = await client.PostAsJsonAsync("/api/v2.0/projects/", projectForCreationDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.UnprocessableEntity));
    }

    [Test]
    public async Task CreateProjectMultipleResponsibleOrganizations_ReturnsUnprocessableEntity()
    {
        // Arrange
        var handleRepositoryMock = Substitute.For<IHandleRepository>();
        handleRepositoryMock
            .UpdateAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<Handle>())
            .Returns(Task.CompletedTask);
        handleRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>())
            .Returns(Task.FromResult<Handle?>(new Handle()
            {
                Pid = new Pid() { Prefix = "t3st", Suffix = "p1d" },
                Values = [new() { Type = "URL", Data = "https://test.com", ParsedData = "" }]
            }));

        var defaultProjectQuotaRepository = Substitute.For<IDefaultProjectQuotaRepository>();
        defaultProjectQuotaRepository
            .GetAllAsync(Arg.Any<Guid>())
            .Returns([]);

        var factory = CoscineTestExtensions.CreateFactory()
                        .WithServiceInstance(handleRepositoryMock)
                        .WithServiceInstance(defaultProjectQuotaRepository);
        var client = await InitializeClientAsync(factory);

        var visibilities = await factory.SeedVisibilitiesAsync();
        var disciplines = await factory.SeedDisciplinesAsync();

        var projectForCreationDto = new ProjectForCreationDto
        {
            Name = Guid.NewGuid().ToString(),
            Description = "This is a test project",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddDays(7),
            Keywords = ["first", "second"],
            DisplayName = "Test Project",
            PrincipleInvestigators = "Investigator",
            GrantId = "Grant_123",
            Visibility = new VisibilityForProjectManipulationDto { Id = visibilities.First().Id },
            Disciplines =
            [
                new() { Id = disciplines.First().Id }
            ],
            Organizations =
            [
                new() { Uri = new Uri("https://new.test/rorUriResponsible1"), Responsible = true },
                new() { Uri = new Uri("https://new.test/rorUriResponsible2"), Responsible = true },
                new() { Uri = new Uri("https://new.test/rorUriAdditional1"), Responsible = false },
            ]
        };

        // Act
        var response = await client.PostAsJsonAsync("/api/v2.0/projects/", projectForCreationDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.UnprocessableEntity));
    }

    [Test]
    public async Task UpdateProject_ReturnsNoContent()
    {
        // Arrange
        var handleRepositoryMock = Substitute.For<IHandleRepository>();
        handleRepositoryMock
            .UpdateAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<Handle>())
            .Returns(Task.CompletedTask);
        handleRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>())
            .Returns(Task.FromResult<Handle?>(new Handle()
            {
                Pid = new Pid() { Prefix = "t3st", Suffix = "p1d" },
                Values = [new() { Type = "URL", Data = "https://test.com", ParsedData = "" }]
            }));

        var factory = CoscineTestExtensions.CreateFactory()
                        .WithServiceInstance(handleRepositoryMock);

        var client = await InitializeClientAsync(factory);
        var visibilities = await factory.SeedVisibilitiesAsync();
        var disciplines = await factory.SeedDisciplinesAsync();
        var projects = await factory.SeedProjectsAsync();

        var projectForUpdateDto = new ProjectForUpdateDto
        {
            Name = "Test Project updated",
            Description = "This is an updated test project",
            Slug = "A different slug!",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddDays(7),
            Keywords = ["first", "second"],
            DisplayName = "Test Project New",
            PrincipleInvestigators = "New",
            GrantId = "NewGrant_123",
            Visibility = new VisibilityForProjectManipulationDto { Id = visibilities.First().Id },
            Disciplines =
            [
                new() { Id = disciplines.First().Id }
            ],
            Organizations =
            [
                new() { Uri = new Uri("https://new.test/rorUri"), Responsible = true }
            ]
        };

        // Act
        var response = await client.PutAsJsonAsync($"/api/v2.0/projects/{projects.First().Id}", projectForUpdateDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var content = await response.Content.ReadAsStreamAsync();

        Assert.That(content.Length, Is.EqualTo(0));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var updatedProject = dbContext.Projects.First(x => x.Id == projects.First().Id);

        Assert.That(updatedProject.ProjectName == projectForUpdateDto.Name, Is.True);
        Assert.That(updatedProject.ProjectDisciplines.Count == 1, Is.True);
        Assert.That(updatedProject.ProjectDisciplines.First().DisciplineId == disciplines.First().Id, Is.True);
        Assert.That(updatedProject.ProjectInstitutes.Count == 1, Is.True);
        Assert.That(
            updatedProject.ProjectInstitutes.First().OrganizationUrl.Contains("new", StringComparison.OrdinalIgnoreCase),
            Is.True
        );
        Assert.That(updatedProject.VisibilityId == visibilities.First().Id, Is.True);
        Assert.That(updatedProject.Slug != projects.First().Slug, Is.True);

        // Check for the correct rdf values
        var projectRdfRepository = serviceProvider.GetRequiredService<IProjectRdfRepository>();

        var projectRdf = projectRdfRepository.Get(updatedProject.Id);

        Assert.That(projectRdf, Is.Not.Null, "ProjectRdf instance should not be null.");
        Assert.That(projectRdf?.DisplayName, Is.EqualTo(projectForUpdateDto.DisplayName),
                    "DisplayName in ProjectRdf does not match the expected DisplayName from ProjectForCreationDto.");
        Assert.That(projectRdf?.Description, Is.EqualTo(projectForUpdateDto.Description),
                    "Description in ProjectRdf does not match the expected Description from ProjectForCreationDto.");
        Assert.That(projectRdf?.StartDate, Is.EqualTo(projectForUpdateDto.StartDate).Within(TimeSpan.FromSeconds(1)),
                    "StartDate in ProjectRdf does not match the expected StartDate from ProjectForCreationDto.");
        Assert.That(projectRdf?.EndDate, Is.EqualTo(projectForUpdateDto.EndDate).Within(TimeSpan.FromSeconds(1)),
                    "EndDate in ProjectRdf does not match the expected EndDate from ProjectForCreationDto.");
        Assert.That(projectRdf?.Keywords, Is.EquivalentTo(projectForUpdateDto.Keywords),
                    "Keywords in ProjectRdf do not match the expected Keywords from ProjectForCreationDto.");
        Assert.That(projectRdf?.PrincipleInvestigators, Is.EqualTo(projectForUpdateDto.PrincipleInvestigators),
                    "PrincipleInvestigators in ProjectRdf do not match the expected PrincipleInvestigators from ProjectForCreationDto.");
        Assert.That(projectRdf?.GrantId, Is.EqualTo(projectForUpdateDto.GrantId),
                    "GrantId in ProjectRdf does not match the expected GrantId from ProjectForCreationDto.");
        Assert.That(projectRdf?.VisibilityId, Is.EqualTo(RdfUris.CoscineTermsVisibilityProjectMember),
                    "VisibilityId in ProjectRdf does not match the expected VisibilityId from ProjectForCreationDto.");
        Assert.That(projectRdf?.OrganizationIds, Is.EquivalentTo(projectForUpdateDto.Organizations.Select(x => x.Uri)),
                    "OrganizationId in ProjectRdf does not match the expected OrganizationId from ProjectForCreationDto.");

        var rdfRepositoryBase = serviceProvider.GetRequiredService<IRdfRepositoryBase>();

        var trellisGraph = await rdfRepositoryBase.GetGraph(RdfUris.TrellisGraph);

        Assert.That(trellisGraph, Is.Not.Null);
        Assert.That(trellisGraph.IsEmpty, Is.Not.True);

        var subjects = trellisGraph.Triples.Select(x => (x.Subject as UriNode)?.Uri).Distinct();

        var projectRepository = serviceProvider.GetRequiredService<IProjectRepository>();
        Assert.That(subjects, Does.Contain(projectRepository.GenerateGraphName(updatedProject)));
    }

    [Test]
    public async Task UpdateProjectMultipleOrganizations_ReturnsNoContent()
    {
        // Arrange
        var handleRepositoryMock = Substitute.For<IHandleRepository>();
        handleRepositoryMock
            .UpdateAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<Handle>())
            .Returns(Task.CompletedTask);
        handleRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>())
            .Returns(Task.FromResult<Handle?>(new Handle()
            {
                Pid = new Pid() { Prefix = "t3st", Suffix = "p1d" },
                Values = [new() { Type = "URL", Data = "https://test.com", ParsedData = "" }]
            }));

        var factory = CoscineTestExtensions.CreateFactory()
                        .WithServiceInstance(handleRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var visibilities = await factory.SeedVisibilitiesAsync();
        var disciplines = await factory.SeedDisciplinesAsync();
        var projects = await factory.SeedProjectsAsync();

        var projectForUpdateDto2nd = new ProjectForUpdateDto
        {
            Name = "Test Project updated 2nd",
            Description = "This is an twice updated test project",
            Slug = "A different slug, again!",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddDays(7),
            Keywords = ["first", "second", "third"],
            DisplayName = "Test Project New 2nd",
            PrincipleInvestigators = "New One",
            GrantId = "NewGrant_12345",
            Visibility = new VisibilityForProjectManipulationDto { Id = visibilities.First().Id },
            Disciplines =
            [
                new() { Id = disciplines.First().Id }
            ],
            Organizations =
            [
                new() { Uri = new Uri("https://new.test/rorUriResponsible"), Responsible = true },
                new() { Uri = new Uri("https://new.test/rorUriAdditional1"), Responsible = false },
                new() { Uri = new Uri("https://new.test/rorUriAdditional2"), Responsible = false },
            ]
        };

        // Act

        var response = await client.PutAsJsonAsync($"/api/v2.0/projects/{projects.First().Id}", projectForUpdateDto2nd);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var content = await response.Content.ReadAsStreamAsync();

        Assert.That(content.Length, Is.EqualTo(0));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var updatedProject = dbContext.Projects.First(x => x.Id == projects.First().Id);

        Assert.That(updatedProject.ProjectName == projectForUpdateDto2nd.Name, Is.True);
        Assert.That(updatedProject.ProjectDisciplines.Count == 1, Is.True);
        Assert.That(updatedProject.ProjectDisciplines.First().DisciplineId == disciplines.First().Id, Is.True);
        Assert.That(updatedProject.ProjectInstitutes.Count == 3, Is.True);
        Assert.That(
            updatedProject.ProjectInstitutes.Where(org => org.OrganizationUrl.Contains("Responsible", StringComparison.OrdinalIgnoreCase)).Count() == 1,
            Is.True
        );
        Assert.That(
            updatedProject.ProjectInstitutes.Where(org => org.OrganizationUrl.Contains("Additional", StringComparison.OrdinalIgnoreCase)).Count() == 2,
            Is.True
        );
        Assert.That(
            updatedProject.ProjectInstitutes.Where(org => org.Responsible == true).Count() == 1,
            Is.True
        );
        Assert.That(
            updatedProject.ProjectInstitutes.Where(org => org.Responsible == false).Count() == 2,
            Is.True
        );
        Assert.That(updatedProject.VisibilityId == visibilities.First().Id, Is.True);
        Assert.That(updatedProject.Slug != projects.First().Slug, Is.True);
    }

    [Test]
    public async Task UpdateProjectWithoutOrganizations_ReturnsUnprocessableEntity()
    {
        // Arrange
        var handleRepositoryMock = Substitute.For<IHandleRepository>();
        handleRepositoryMock
            .UpdateAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<Handle>())
            .Returns(Task.CompletedTask);
        handleRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>())
            .Returns(Task.FromResult<Handle?>(new Handle()
            {
                Pid = new Pid() { Prefix = "t3st", Suffix = "p1d" },
                Values = [new() { Type = "URL", Data = "https://test.com", ParsedData = "" }]
            }));

        var factory = CoscineTestExtensions.CreateFactory()
                        .WithServiceInstance(handleRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var visibilities = await factory.SeedVisibilitiesAsync();
        var disciplines = await factory.SeedDisciplinesAsync();
        var projects = await factory.SeedProjectsAsync();

        var projectForUpdateDto = new ProjectForUpdateDto
        {
            Name = "Test Project updated",
            Description = "This is an updated test project",
            Slug = "A different slug!",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddDays(7),
            Keywords = ["first", "second"],
            DisplayName = "Test Project New",
            PrincipleInvestigators = "New",
            GrantId = "NewGrant_123",
            Visibility = new VisibilityForProjectManipulationDto { Id = visibilities.First().Id },
            Disciplines =
            [
                new() { Id = disciplines.First().Id }
            ],
            Organizations = [],
        };

        // Act
        var response = await client.PutAsJsonAsync($"/api/v2.0/projects/{projects.First().Id}", projectForUpdateDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.UnprocessableEntity));
    }

    [Test]
    public async Task UpdateProjectWithoutResponsibleOrganization_ReturnsUnprocessableEntity()
    {
        // Arrange
        var handleRepositoryMock = Substitute.For<IHandleRepository>();
        handleRepositoryMock
            .UpdateAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<Handle>())
            .Returns(Task.CompletedTask);
        handleRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>())
            .Returns(Task.FromResult<Handle?>(new Handle()
            {
                Pid = new Pid() { Prefix = "t3st", Suffix = "p1d" },
                Values = [new() { Type = "URL", Data = "https://test.com", ParsedData = "" }]
            }));

        var factory = CoscineTestExtensions.CreateFactory()
                        .WithServiceInstance(handleRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var visibilities = await factory.SeedVisibilitiesAsync();
        var disciplines = await factory.SeedDisciplinesAsync();
        var projects = await factory.SeedProjectsAsync();

        var projectForUpdateDto = new ProjectForUpdateDto
        {
            Name = "Test Project updated",
            Description = "This is an updated test project",
            Slug = "A different slug!",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddDays(7),
            Keywords = ["first", "second"],
            DisplayName = "Test Project New",
            PrincipleInvestigators = "New",
            GrantId = "NewGrant_123",
            Visibility = new VisibilityForProjectManipulationDto { Id = visibilities.First().Id },
            Disciplines =
            [
                new() { Id = disciplines.First().Id }
            ],
            Organizations = [
                new() { Uri = new Uri("https://new.test/rorUriAdditional1"), Responsible = false },
                new() { Uri = new Uri("https://new.test/rorUriAdditional2"), Responsible = false },
            ],
        };

        // Act
        var response = await client.PutAsJsonAsync($"/api/v2.0/projects/{projects.First().Id}", projectForUpdateDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.UnprocessableEntity));
    }

    [Test]
    public async Task UpdateProjectMultipleResponsibleOrganizations_ReturnsUnprocessableEntity()
    {
        // Arrange
        var handleRepositoryMock = Substitute.For<IHandleRepository>();
        handleRepositoryMock
            .UpdateAsync(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<Handle>())
            .Returns(Task.CompletedTask);
        handleRepositoryMock
            .GetAsync(Arg.Any<string>(), Arg.Any<string>())
            .Returns(Task.FromResult<Handle?>(new Handle()
            {
                Pid = new Pid() { Prefix = "t3st", Suffix = "p1d" },
                Values = [new() { Type = "URL", Data = "https://test.com", ParsedData = "" }]
            }));


        var factory = CoscineTestExtensions.CreateFactory()
                        .WithServiceInstance(handleRepositoryMock);
        var client = await InitializeClientAsync(factory);

        var visibilities = await factory.SeedVisibilitiesAsync();
        var disciplines = await factory.SeedDisciplinesAsync();
        var projects = await factory.SeedProjectsAsync();

        var projectForUpdateDto = new ProjectForUpdateDto
        {
            Name = "Test Project updated",
            Description = "This is an updated test project",
            Slug = "A different slug!",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddDays(7),
            Keywords = ["first", "second"],
            DisplayName = "Test Project New",
            PrincipleInvestigators = "New",
            GrantId = "NewGrant_123",
            Visibility = new VisibilityForProjectManipulationDto { Id = visibilities.First().Id },
            Disciplines =
            [
                new() { Id = disciplines.First().Id }
            ],
            Organizations = [
                new() { Uri = new Uri("https://new.test/rorUriResponsible1"), Responsible = true },
                new() { Uri = new Uri("https://new.test/rorUriResponsible2"), Responsible = true },
                new() { Uri = new Uri("https://new.test/rorUriAdditional"), Responsible = false },
            ],
        };

        // Act
        var response = await client.PutAsJsonAsync($"/api/v2.0/projects/{projects.First().Id}", projectForUpdateDto);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.UnprocessableEntity));
    }
}
