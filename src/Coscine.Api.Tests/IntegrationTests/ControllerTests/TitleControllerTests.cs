﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class TitleControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetTitle_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        var titles = await factory.SeedTitlesAsync();

        // Act
        var response = await client.GetAsync($"/api/v2.0/titles/{titles.First().Id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<TitleDto>>();

        Assert.That(content.Data?.Id, Is.EqualTo(titles.First().Id));
    }

    [Test]
    public async Task GetTitles_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        // Act
        var response = await client.GetAsync("/api/v2.0/titles");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<IEnumerable<TitleDto>>>();

        Assert.That(content.Data, Has.Count.EqualTo(2));
    }
}