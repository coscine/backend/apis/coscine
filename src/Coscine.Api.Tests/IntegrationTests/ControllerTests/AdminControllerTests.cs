using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects.ResourceType;
using NSubstitute;
using NUnit.Framework;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class AdminControllerTests : CoscineControllerTestsBase
{
    [Test]
    [TestCase("{}")]
    [TestCase("{\"Layer1_A\":\"Test123\"}")]
    public async Task GetAllResources_ValidFixedValues_ReturnsOk(string? fixedValues)
    {

        // Arrange
        var resourceTypeServiceMock = Substitute.For<IResourceTypeService>();
        resourceTypeServiceMock.GetResourceTypeOptionsDto(Arg.Any<Resource>())
            .Returns(Task.FromResult(new ResourceTypeOptionsDto()));

        var factory = CoscineTestExtensions.CreateFactory()
           .WithServiceInstance(resourceTypeServiceMock);

        var client = await InitializeClientAsync(factory, admin: true);

        var resources = await factory.SeedResourcesAsync(fixedValues: fixedValues);

        // Act
        var response = await client.GetAsync($"/api/v2.0/admin/resources");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<ResourceAdminDto>>();

        Assert.That(content.Data, Is.Not.Null, "Expected 'content.Data' to be not null, but it was null.");
        Assert.That(content.Pagination, Is.Not.Null, "Expected 'content.Pagination' to be not null, but it was null.");
        Assert.That(content.Data?.Count(), Is.EqualTo(resources.Count()), $"Expected 'content.Data' to have {resources.Count()} items, but found {content.Data?.Count()}.");
        Assert.That(content.Data?.SingleOrDefault(x => x.Id == resources.First().Id)?.FixedValues, Is.Not.Null, "Expected 'FixedValues' for the first item in 'content.Data' to be not null, but it was null.");
    }

    [Test]
    [TestCase(null)]
    [TestCase("invalid json!!11!")]
    // Should work in the future, but currently evalulates to empty, as the DTO can only handle one layer.
    [TestCase("{\"Layer1_A\":\"Test123\",\"Layer1_B\":{\"Layer2\":\"hmm\"}}")]
    public async Task GetAllResources_InvalidFixedValues_ReturnsOk(string? fixedValues)
    {
        // Arrange
        var resourceTypeServiceMock = Substitute.For<IResourceTypeService>();
        resourceTypeServiceMock.GetResourceTypeOptionsDto(Arg.Any<Resource>())
            .Returns(Task.FromResult(new ResourceTypeOptionsDto()));


        var factory = CoscineTestExtensions.CreateFactory()
           .WithServiceInstance(resourceTypeServiceMock);

        var client = await InitializeClientAsync(factory, admin: true);

        var resources = await factory.SeedResourcesAsync(fixedValues: fixedValues);

        // Act
        var response = await client.GetAsync($"/api/v2.0/admin/resources");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<ResourceAdminDto>>();

        Assert.That(content.Data, Is.Not.Null, "Expected 'content.Data' to be not null, but it was null.");
        Assert.That(content.Pagination, Is.Not.Null, "Expected 'content.Pagination' to be not null, but it was null.");
        Assert.That(content.Data?.Count(), Is.EqualTo(resources.Count()), $"Expected 'content.Data' to have {resources.Count()} items, but found {content.Data?.Count()}.");
        Assert.That(content.Data?.SingleOrDefault(x => x.Id == resources.First().Id)?.FixedValues, Is.Empty, "Expected 'FixedValues' for the first item in 'content.Data' to be empty, but it was not.");
    }
}
