﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.Enums;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System.Net;
using System.Text.RegularExpressions;
using VDS.RDF;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public partial class UserControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetCurrentUser_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        // allow lazy loading
        var user = dbContext.Users.Where(x => x.Id == DatabaseSeeder.TestUser.Id).First();

        var titleOld = new Title { DisplayName = "The Chosen One" };
        dbContext.Titles.Add(titleOld);
        var languageOld = new Language { DisplayName = "Droid Binary", Abbreviation = "BEEP" };
        dbContext.Languages.Add(languageOld);
        var disciplineOld = new Discipline
        {
            DisplayNameEn = "Lightsaber Dueling",
            DisplayNameDe = "Lichtschwerkampf",
            Url = ""
        };
        dbContext.Disciplines.Add(disciplineOld);

        user.Title = titleOld;
        user.Language = languageOld;
        user.UserDisciplines.Add(
            new UserDiscipline
            {
                Discipline = disciplineOld,
                User = user
            }
        );

        dbContext.SaveChanges();

        // Act
        var response = await client.GetAsync("/api/v2.0/self");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<UserDto>>();
        var userDto = content.Data;

        Assert.That(userDto?.Title?.Id, Is.EqualTo(user.Title?.Id));
        Assert.That(userDto?.GivenName, Is.EqualTo(user.Givenname));
        Assert.That(userDto?.FamilyName, Is.EqualTo(user.Surname));
        Assert.That(userDto?.Emails.Any(e => e.Email?.Equals(user.EmailAddress) == true), Is.True);
        Assert.That(userDto?.Disciplines.Select(d => d.Id), Is.EqualTo(user.UserDisciplines.Select(ud => ud.DisciplineId)));
        Assert.That(userDto?.Organizations.Any(o => o.DisplayName.Equals(user.Organization)), Is.True);
        Assert.That(userDto?.Language.Id, Is.EqualTo(user.Language?.Id));
    }

    [Test]
    public async Task GetFirstTenUsers_WithTosAccepted_ReturnsOnlyUsersWhoAcceptedTos()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var userWithoutTos = await factory.SeedUserAsync();
        var userWithTos = await factory.SeedUserAsync();
        
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        await dbContext.Tosaccepteds.AddAsync(
            new Tosaccepted { UserId = userWithTos.Id, Version = "123"}
        );

        dbContext.SaveChanges();

        // Act
        var response = await client.GetAsync("/api/v2.0/users?SearchTerm=Tes");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK), "Expected a 200 OK response.");

        var content = await response.Content.ReadAsAsync<Response<IEnumerable<PublicUserDto>>>();

        Assert.That(content.Data, Is.Not.Null, "Data field in the response content should not be null.");
        Assert.That(content.Data?.Count(), Is.GreaterThanOrEqualTo(1), "Expected exactly 1 user in the response.");
        Assert.That(content.Data?.First(x => x.Id == userWithTos.Id).Id, Is.EqualTo(userWithTos.Id), "The user who accepted TOS should be returned.");
        Assert.That(content.Data?.FirstOrDefault(x => x.Id == userWithoutTos.Id), Is.Null, "Users who did not accept TOS should not be returned.");
    }

    [Test]
    public async Task UpdateCurrentUser_ReturnsNoContent()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var user = dbContext.Users.Where(x => x.Id == DatabaseSeeder.TestUser.Id).First();

        var titleOld = new Title { DisplayName = "The Chosen One" };
        dbContext.Titles.Add(titleOld);
        var titleNew = new Title { DisplayName = "Sith Lord" };
        dbContext.Titles.Add(titleNew);
        var languageOld = new Language { DisplayName = "Droid Binary", Abbreviation = "BEEP" };
        dbContext.Languages.Add(languageOld);
        var languageNew = new Language { DisplayName = "Sith Language", Abbreviation = "SITH" };
        dbContext.Languages.Add(languageNew);
        var disciplineOld = new Discipline
        {
            DisplayNameEn = "Lightsaber Dueling",
            DisplayNameDe = "Lichtschwerkampf",
            Url = ""
        };
        dbContext.Disciplines.Add(disciplineOld);
        var disciplineNew = new Discipline
        {
            DisplayNameEn = "Force Choke Mastery",
            DisplayNameDe = "Meisterschaft des Machtgriffs",
            Url = ""
        };
        dbContext.Disciplines.Add(disciplineNew);

        user.Title = titleOld;
        user.Language = languageOld;
        user.UserDisciplines.Add(
            new UserDiscipline
            {
                Discipline = disciplineOld,
                User = user
            }
        );
        dbContext.SaveChanges();

        var userForUpdateDto = new UserForUpdateDto
        {
            GivenName = "Anakin",
            FamilyName = "Skywalker",
            Email = "chosenone@sithlord.net",
            Organization = "The First Galactic Empire",
            Title = new TitleForUserManipulationDto { Id = titleNew.Id },
            Language = new LanguageForUserManipulationDto { Id = languageNew.Id },
            Disciplines = [new() { Id = disciplineNew.Id }],
        };

        // Act
        var response = await client.PutAsJsonAsync("/api/v2.0/self", userForUpdateDto);

        // User is currently tracked
        // Force an update by removing it and any navigation property from the cache
        dbContext.ChangeTracker.Clear();

        var newUserEntity = dbContext.Users.First();

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var content = await response.Content.ReadAsStreamAsync();

        Assert.That(content.Length, Is.EqualTo(0));
        Assert.That(response.Content.Headers.ContentType, Is.Null); // Make sure the response content type is null

        // Check the updated user properties
        Assert.That(newUserEntity.Givenname, Is.EqualTo("Anakin"));
        Assert.That(newUserEntity.Surname, Is.EqualTo("Skywalker"));
        Assert.That(newUserEntity.EmailAddress, Is.EqualTo("chosenone@sithlord.net"));
        Assert.That(newUserEntity.Organization, Is.EqualTo("The First Galactic Empire"));

        // Check the updated user's title
        var newTitleEntity = dbContext.Titles.Find(titleNew.Id);
        Assert.That(newUserEntity.TitleId, Is.EqualTo(titleNew.Id));
        Assert.That(newUserEntity.Title, Is.EqualTo(newTitleEntity));

        // Check the updated user's language
        var newLanguageEntity = dbContext.Languages.Find(languageNew.Id);
        Assert.That(newUserEntity.LanguageId, Is.EqualTo(languageNew.Id));
        Assert.That(newUserEntity.Language, Is.EqualTo(newLanguageEntity));

        // Check the updated user's disciplines
        var newDisciplineEntity = dbContext.Disciplines.Find(disciplineNew.Id);
        Assert.That(newUserEntity.UserDisciplines.Count, Is.EqualTo(1));
        Assert.That(newUserEntity.UserDisciplines.First().DisciplineId, Is.EqualTo(disciplineNew.Id));
        Assert.That(newUserEntity.UserDisciplines.First().Discipline, Is.EqualTo(newDisciplineEntity));

        // Check that only one user exists in the database
        Assert.That(dbContext.Users.Count(), Is.EqualTo(1));
        
        // Check for the correct rdf values
        var rdf = serviceProvider.GetRequiredService<IRdfRepositoryBase>();
        var userRepository = serviceProvider.GetRequiredService<IUserRepository>();

        var graphName = userRepository.GenerateGraphName(user);

        var userGraph = await rdf.GetGraph(graphName);

        Assert.That(userGraph.Triples.SingleOrDefault(x => x.HasPredicate(new UriNode(RdfUris.FoafName))), Is.Not.Null);
        Assert.That(userGraph.Triples.SingleOrDefault(x => x.HasPredicate(new UriNode(RdfUris.FoafName)))?.Object as LiteralNode, Is.Not.Null);
        Assert.That((userGraph.Triples.SingleOrDefault(x => x.HasPredicate(new UriNode(RdfUris.FoafName)))?.Object as LiteralNode)?.Value, Is.EqualTo("Anakin Skywalker"));
        
        Assert.That(userGraph.Triples.SingleOrDefault(x => x.HasPredicate(new UriNode(RdfUris.A))), Is.Not.Null);
        Assert.That(userGraph.Triples.SingleOrDefault(x => x.HasPredicate(new UriNode(RdfUris.A)))?.Object as UriNode, Is.Not.Null);
        Assert.That((userGraph.Triples.SingleOrDefault(x => x.HasPredicate(new UriNode(RdfUris.A)))?.Object as UriNode)?.Uri, Is.EqualTo(RdfUris.FoafPersonClass));

    }

    /// <summary>
    /// This was necessary because of a caching issue: https://git.rwth-aachen.de/coscine/issues/-/issues/2802
    /// </summary>
    /// <returns></returns>
    [Test]
    public async Task UpdateCurrentUserWorksTwice_ReturnsNoContent()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var user = dbContext.Users.Where(x => x.Id == DatabaseSeeder.TestUser.Id).First();

        var titleOld = new Title { DisplayName = "The Chosen One" };
        dbContext.Titles.Add(titleOld);
        var titleNew = new Title { DisplayName = "Sith Lord" };
        dbContext.Titles.Add(titleNew);
        var languageOld = new Language { DisplayName = "Droid Binary", Abbreviation = "BEEP" };
        dbContext.Languages.Add(languageOld);
        var languageNew = new Language { DisplayName = "Sith Language", Abbreviation = "SITH" };
        dbContext.Languages.Add(languageNew);
        var disciplineOld = new Discipline
        {
            DisplayNameEn = "Lightsaber Dueling",
            DisplayNameDe = "Lichtschwerkampf",
            Url = ""
        };
        dbContext.Disciplines.Add(disciplineOld);
        var disciplineNew = new Discipline
        {
            DisplayNameEn = "Force Choke Mastery",
            DisplayNameDe = "Meisterschaft des Machtgriffs",
            Url = ""
        };
        dbContext.Disciplines.Add(disciplineNew);

        user.Title = titleOld;
        user.Language = languageOld;
        user.UserDisciplines.Add(
            new UserDiscipline
            {
                Discipline = disciplineOld,
                User = user
            }
        );
        dbContext.SaveChanges();

        var userForUpdateDto = new UserForUpdateDto
        {
            GivenName = "Anakin",
            FamilyName = "Skywalker",
            Email = "chosenone@sithlord.net",
            Organization = "The First Galactic Empire",
            Title = new TitleForUserManipulationDto { Id = titleNew.Id },
            Language = new LanguageForUserManipulationDto { Id = languageNew.Id },
            Disciplines = new List<DisciplineForUserManipulationDto> { new() { Id = disciplineNew.Id } },
        };

        // Act
        var response = await client.PutAsJsonAsync("/api/v2.0/self", userForUpdateDto);

        userForUpdateDto = new UserForUpdateDto
        {
            GivenName = "Luke",
            FamilyName = "Skywalker",
            Email = "chosenone@sithlord.net",
            Organization = "The First Galactic Empire",
            Title = new TitleForUserManipulationDto { Id = titleNew.Id },
            Language = new LanguageForUserManipulationDto { Id = languageNew.Id },
            Disciplines = new List<DisciplineForUserManipulationDto> { new() { Id = disciplineNew.Id } },
        };

        response = await client.PutAsJsonAsync("/api/v2.0/self", userForUpdateDto);

        // User is currently tracked
        // Force an update by removing it and any navigation property from the cache
        dbContext.ChangeTracker.Clear();

        var newUserEntity = dbContext.Users.First();

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var content = await response.Content.ReadAsStreamAsync();

        Assert.That(content.Length, Is.EqualTo(0));
        Assert.That(response.Content.Headers.ContentType, Is.Null); // Make sure the response content type is null

        // Check the updated user properties
        Assert.That(newUserEntity.Givenname, Is.EqualTo("Luke"));

        // Check that only one user exists in the database
        Assert.That(dbContext.Users.Count(), Is.EqualTo(1));
    }

    [Test]
    public async Task AcceptCurrentTos_ReturnsNoContent()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var user = dbContext.Users.Where(x => x.Id == DatabaseSeeder.TestUser.Id).First();

        var tosGetResponse = await client.GetAsync("/api/v2.0/tos");
        var tosContent = await tosGetResponse.Content.ReadAsAsync<Response<TermsOfServiceDto>>();

        var tosAcceptDto = new UserTermsOfServiceAcceptDto
        {
            Version = tosContent?.Data?.Version ?? ""
        };

        // Act
        var response = await client.PostAsJsonAsync("/api/v2.0/self/tos", tosAcceptDto);

        // User is currently tracked
        // Force an update by removing it and any navigation property from the cache
        dbContext.ChangeTracker.Clear();

        var newToSEntity = dbContext.Tosaccepteds.First();

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var content = await response.Content.ReadAsStreamAsync();

        Assert.That(content.Length, Is.EqualTo(0));
        Assert.That(response.Content.Headers.ContentType, Is.Null); // Make sure the response content type is null

        // Check the updated ToS properties
        Assert.That(newToSEntity.UserId, Is.EqualTo(user.Id));
        Assert.That(newToSEntity.Version, Is.Not.EqualTo(string.Empty));
        Assert.That(newToSEntity.Version, Is.Not.Null);
    }

    [Test]
    public async Task ConfirmUserEmail_ReturnsNoContent()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var user = dbContext.Users.Where(x => x.Id == DatabaseSeeder.TestUser.Id).First();

        var confirmationToken = Guid.NewGuid();
        var contactChange = new ContactChange
        {
            ConfirmationToken = confirmationToken,
            EditDate = DateTime.Now.AddHours(-2),
            NewEmail = "chosenone@sithlord.net",
            User = user
        };

        dbContext.ContactChanges.Add(contactChange);
        dbContext.SaveChanges();

        // Create the query string
        var queryString = new Dictionary<string, string?>() { ["confirmationToken"] = confirmationToken.ToString() };

        // Act
        var response = await client.PostAsync(QueryHelpers.AddQueryString("/api/v2.0/self/emails", queryString), null);

        // User is currently tracked
        // Force an update by removing it and any navigation property from the cache
        dbContext.ChangeTracker.Clear();

        var updatedUserEntity = dbContext.Users.First();

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));

        var content = await response.Content.ReadAsStreamAsync();

        Assert.That(content.Length, Is.EqualTo(0));
        Assert.That(response.Content.Headers.ContentType, Is.Null); // Make sure the response content type is null

        // Check the updated User properties
        Assert.That(updatedUserEntity.EmailAddress, Is.Not.EqualTo(user.EmailAddress));
        Assert.That(updatedUserEntity.EmailAddress, Is.EqualTo(contactChange.NewEmail));
    }

    [Test]
    public async Task InitiateUserMerge_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var dbContext = serviceProvider.GetRequiredService<RepositoryContext>();

        var user = dbContext.Users.Where(x => x.Id == DatabaseSeeder.TestUser.Id).First();

        // Create the query string
        var queryString = new Dictionary<string, string?>() { ["ExternalAuthenticator"] = nameof(IdentityProviders.ORCiD) };

        // Act
        var response = await client.PostAsync(QueryHelpers.AddQueryString("/api/v2.0/self/identities", queryString), null);

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<UserMergeDto>>();
        var mergeToken = content.Data?.Token;
        var mergeTokenCookie = response.Headers.GetValues("Set-Cookie")
                                               .FirstOrDefault(header => header.StartsWith("coscine.mergetoken="));
        var cookieToken = mergeTokenCookie?.Split('=')[1].Split(';')[0];

        Assert.That(content.Data?.Token, Is.Not.Null);
        Assert.That(MyRegex().IsMatch(content.Data?.Token));
        Assert.That(mergeTokenCookie, Is.Not.Null);
        Assert.That(cookieToken, Is.EqualTo(mergeToken));
    }

    [GeneratedRegex("^[a-zA-Z0-9\\-_=]+?\\.[a-zA-Z0-9\\-_=]+?\\.[a-zA-Z0-9\\-_=]+?$")]
    private static partial Regex MyRegex();
}