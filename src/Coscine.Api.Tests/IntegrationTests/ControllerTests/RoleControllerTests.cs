﻿using Coscine.Api.Core.Entities.ApiResponse;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using NUnit.Framework;
using System.Linq.Dynamic.Core;
using System.Net;

namespace Coscine.Api.Tests.IntegrationTests.ControllerTests;

[TestFixture]
public class RoleControllerTests : CoscineControllerTestsBase
{
    [Test]
    public async Task GetRole_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var roles = await factory.SeedRolesAsync();

        // Act
        var response = await client.GetAsync($"/api/v2.0/roles/{roles.First().Id}");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<Response<RoleDto>>();

        Assert.That(content.Data?.Id, Is.EqualTo(roles.First().Id));
    }

    [Test]
    public async Task GetRoles_ReturnsOk()
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        
        var roles = await factory.SeedRolesAsync();

        // Act
        var response = await client.GetAsync("/api/v2.0/roles");

        // Assert
        Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));

        var content = await response.Content.ReadAsAsync<PagedResponse<RoleDto>>();

        // We are adding the default roles and add expected count test roles!
        Assert.That(content.Data, Has.Count.EqualTo(roles.Count() + 3));
    }
}