using System.Linq.Dynamic.Core;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Service.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Coscine.Api.Tests.IntegrationTests.ServiceTests;


[TestFixture]
public class MergeServiceTests : CoscineControllerTestsBase
{
    // Easy matrix
    private static IEnumerable<TestCaseData> TwoDTestCases
    {
        get
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    yield return new TestCaseData(i, j);
                }
            }
        }
    }

    private static IEnumerable<TestCaseData> OneDTestCases
    {
        get
        {
            for (int i = 0; i < 3; i++)
            {
                yield return new TestCaseData(i);
            }
        }
    }

    [TestCaseSource(nameof(TwoDTestCases))]
    public async Task Merge_N_2_N_Projects_Success(int projectCountFrom, int projectCountTo)
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var toUser = DatabaseSeeder.TestUser;
        var fromUser = (await factory.SeedUsersAsync()).First();
        var toProjects = await factory.SeedProjectsAsync(amount: projectCountTo);
        var fromProjects = await factory.SeedProjectsAsync(userId: fromUser.Id, amount: projectCountFrom);

        var projectIds = toProjects.Select(x => x.Id).Concat(fromProjects.Select(x => x.Id));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var userService = serviceProvider.GetRequiredService<IUserService>();

        // Act
        await userService.MergeUser(fromUser.Id, toUser.Id);

        // Assert

        using var scope2 = factory.Services.CreateScope();
        var serviceProvider2 = scope.ServiceProvider;
        var dbContext = serviceProvider2.GetRequiredService<RepositoryContext>();

        // Make sure, that the correct amount was generated
        Assert.That(toProjects.Count() == projectCountTo);
        Assert.That(fromProjects.Count() == projectCountFrom);

        // Assert that no project was deleted
        Assert.That(dbContext.Projects.Count() == projectIds.Count());
        foreach (var projectId in projectIds)
        {
            Assert.That(dbContext.Projects.Where(x => x.Id == projectId).Any());
        }

        // Assert that all projects are now part of the toUser
        foreach (var projectRole in dbContext.ProjectRoles)
        {
            Assert.That(projectRole.UserId == toUser.Id);
        }

        // Assert that from user has no more projects remaining
        foreach (var projectRole in dbContext.ProjectRoles)
        {
            Assert.That(projectRole.UserId != fromUser.Id);
        }

        // Assert that no project was orphaned
        Assert.That(dbContext.Projects.Count() == dbContext.ProjectRoles.Count());

        // Make sure, that each project has a role            
        foreach (var projectId in projectIds)
        {
            Assert.That(dbContext.ProjectRoles.Where(x => x.ProjectId == projectId).Any());
        }

        // Filtered is should be null
        Assert.That(dbContext.Users.Where(x => x.Id == fromUser.Id).FirstOrDefault(), Is.Null);

        // Unfiltered is shoul be found

        var user = dbContext.Users.IgnoreQueryFilters().Where(x => x.Id == fromUser.Id).FirstOrDefault();
        Assert.That(user, Is.Not.Null);
        Assert.That(user?.IsDeleted, Is.True);
    }

    [TestCaseSource(nameof(TwoDTestCases))]
    public async Task Merge_N_2_N_Projects_Both_Users_Are_Part_Of_Success(int projectCountFrom, int projectCountTo)
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var toUser = DatabaseSeeder.TestUser;
        var fromUser = (await factory.SeedUsersAsync()).First();
        var toProjects = await factory.SeedProjectsAsync(amount: projectCountTo);
        var fromProjects = await factory.SeedProjectsAsync(userId: fromUser.Id, amount: projectCountFrom);

        var projectIds = toProjects.Select(x => x.Id).Concat(fromProjects.Select(x => x.Id));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var userService = serviceProvider.GetRequiredService<IUserService>();
        var dbContextArrange = serviceProvider.GetRequiredService<RepositoryContext>();

        // User to other projects
        foreach (var project in fromProjects)
        {
            dbContextArrange.ProjectRoles.Add(new ProjectRole
            {
                ProjectId = project.Id,
                RoleId = RoleConfiguration.Owner.Id,
                UserId = toUser.Id
            });
        }

        dbContextArrange.SaveChanges();

        // Act
        await userService.MergeUser(fromUser.Id, toUser.Id);

        // Assert

        using var scope2 = factory.Services.CreateScope();
        var serviceProvider2 = scope.ServiceProvider;
        var dbContext = serviceProvider2.GetRequiredService<RepositoryContext>();

        // Make sure, that the correct amount was generated
        Assert.That(toProjects.Count() == projectCountTo);
        Assert.That(fromProjects.Count() == projectCountFrom);

        // Assert that no project was deleted
        Assert.That(dbContext.Projects.Count() == projectIds.Count());
        foreach (var projectId in projectIds)
        {
            Assert.That(dbContext.Projects.Where(x => x.Id == projectId).Any());
        }

        // Assert that all projects are now part of the toUser
        foreach (var projectRole in dbContext.ProjectRoles)
        {
            Assert.That(projectRole.UserId == toUser.Id);
        }

        // Assert that from user has no more projects remaining
        foreach (var projectRole in dbContext.ProjectRoles)
        {
            Assert.That(projectRole.UserId != fromUser.Id);
        }

        // Assert that no project was orphaned
        Assert.That(dbContext.Projects.Count() == dbContext.ProjectRoles.Count());

        // Make sure, that each project has a role            
        foreach (var projectId in projectIds)
        {
            Assert.That(dbContext.ProjectRoles.Where(x => x.ProjectId == projectId).Any());
        }

        // Filtered is should be null
        Assert.That(dbContext.Users.Where(x => x.Id == fromUser.Id).FirstOrDefault(), Is.Null);

        // Unfiltered is shoul be found

        var user = dbContext.Users.IgnoreQueryFilters().Where(x => x.Id == fromUser.Id).FirstOrDefault();
        Assert.That(user, Is.Not.Null);
        Assert.That(user?.IsDeleted, Is.True);
    }

    [TestCaseSource(nameof(TwoDTestCases))]
    public async Task Merge_N_2_N_Project_Invitations_Success(int projectCountFrom, int projectCountTo)
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var toUser = DatabaseSeeder.TestUser;
        var fromUser = (await factory.SeedUsersAsync()).First();
        var toProjects = await factory.SeedProjectsAsync(amount: 1);
        var fromProjects = await factory.SeedProjectsAsync(userId: fromUser.Id, amount: 1);

        List<Invitation> projectInvitations = [];

        projectInvitations.AddRange(await factory.SeedProjectInvitationsAsync(issuerId: fromUser.Id,
                                                                               projects: fromProjects,
                                                                               amount: projectCountFrom,
                                                                               randomInvitee: true));

        projectInvitations.AddRange(await factory.SeedProjectInvitationsAsync(issuerId: toUser.Id,
                                                                               projects: toProjects,
                                                                               amount: projectCountTo,
                                                                               randomInvitee: true));

        var distinctInviteeEmails = projectInvitations.Select(x => x.InviteeEmail).Distinct();

        var projectIds = toProjects.Select(x => x.Id).Concat(fromProjects.Select(x => x.Id));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var userService = serviceProvider.GetRequiredService<IUserService>();

        // Act

        await userService.MergeUser(fromUser.Id, toUser.Id);

        // Assert

        using var scope2 = factory.Services.CreateScope();
        var serviceProvider2 = scope.ServiceProvider;
        var dbContext = serviceProvider2.GetRequiredService<RepositoryContext>();

        // Assert that no project was deleted
        Assert.That(dbContext.Projects.Count() == projectIds.Count());
        foreach (var projectId in projectIds)
        {
            Assert.That(dbContext.Projects.Where(x => x.Id == projectId).Any());
        }

        // Assert that all projects are now part of the toUser
        foreach (var projectRole in dbContext.ProjectRoles)
        {
            Assert.That(projectRole.UserId == toUser.Id);
        }

        // Assert that from user has no more projects remaining
        foreach (var projectRole in dbContext.ProjectRoles)
        {
            Assert.That(projectRole.UserId != fromUser.Id);
        }

        // Assert that no project was orphaned
        Assert.That(dbContext.Projects.Count() == dbContext.ProjectRoles.Count());

        // Make sure, that no invitation is issued by the to user.            
        foreach (var projectId in projectIds)
        {
            Assert.That(dbContext.Invitations.Where(x => x.Issuer != toUser.Id).Any(), Is.False);
        }

        // Make sure, that all invitations are merged
        foreach (var invitaion in dbContext.Invitations)
        {
            Assert.That(dbContext.Invitations.Where(x => x.Issuer == toUser.Id).Any());
        }

        // Make sure, that all previous invitations, if still available, are mapped to the new user.
        foreach (var invitaion in projectInvitations)
        {
            Assert.That(dbContext.Invitations.Where(x => x.Issuer == toUser.Id && x.Id == invitaion.Id).Any());

        }

        // Make sure, that each invitee is still present and none has been lost
        foreach (var inviteeEmail in distinctInviteeEmails)
        {
            Assert.That(dbContext.Invitations.Where(x => x.InviteeEmail == inviteeEmail).Any());
        }

        // Filtered is should be null
        Assert.That(dbContext.Users.Where(x => x.Id == fromUser.Id).FirstOrDefault(), Is.Null);

        // Unfiltered is shoul be found

        var user = dbContext.Users.IgnoreQueryFilters().Where(x => x.Id == fromUser.Id).FirstOrDefault();
        Assert.That(user, Is.Not.Null);
        Assert.That(user?.IsDeleted, Is.True);
    }

    [TestCaseSource(nameof(TwoDTestCases))]
    public async Task Merge_N_2_N_Project_Invitations_Same_Invitee_Success(int projectCountFrom, int projectCountTo)
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        var toUser = DatabaseSeeder.TestUser;
        var fromUser = (await factory.SeedUsersAsync()).First();
        var toProjects = await factory.SeedProjectsAsync(amount: 1);
        var fromProjects = await factory.SeedProjectsAsync(userId: fromUser.Id, amount: 1);

        List<Invitation> projectInvitations = [];

        projectInvitations.AddRange(await factory.SeedProjectInvitationsAsync(issuerId: fromUser.Id,
                                                                               projects: fromProjects,
                                                                               amount: projectCountFrom,
                                                                               randomInvitee: false));

        projectInvitations.AddRange(await factory.SeedProjectInvitationsAsync(issuerId: toUser.Id,
                                                                               projects: toProjects,
                                                                               amount: projectCountTo,
                                                                               randomInvitee: false));

        var distinctInviteeEmails = projectInvitations.Select(x => x.InviteeEmail).Distinct();

        var projectIds = toProjects.Select(x => x.Id).Concat(fromProjects.Select(x => x.Id));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var userService = serviceProvider.GetRequiredService<IUserService>();

        // Act

        await userService.MergeUser(fromUser.Id, toUser.Id);

        // Assert

        using var scope2 = factory.Services.CreateScope();
        var serviceProvider2 = scope.ServiceProvider;
        var dbContext = serviceProvider2.GetRequiredService<RepositoryContext>();

        // Assert that no project was deleted
        Assert.That(dbContext.Projects.Count() == projectIds.Count());
        foreach (var projectId in projectIds)
        {
            Assert.That(dbContext.Projects.Where(x => x.Id == projectId).Any());
        }

        // Assert that all projects are now part of the toUser
        foreach (var projectRole in dbContext.ProjectRoles)
        {
            Assert.That(projectRole.UserId == toUser.Id);
        }

        // Assert that from user has no more projects remaining
        foreach (var projectRole in dbContext.ProjectRoles)
        {
            Assert.That(projectRole.UserId != fromUser.Id);
        }

        // Assert that no project was orphaned
        Assert.That(dbContext.Projects.Count() == dbContext.ProjectRoles.Count());

        // Make sure, that no invitation is issued by the to user.            
        foreach (var projectId in projectIds)
        {
            Assert.That(dbContext.ProjectRoles.Where(x => x.ProjectId == projectId).Any());
        }

        // Make sure, that all invitations are merged
        foreach (var invitaion in dbContext.Invitations)
        {
            Assert.That(dbContext.Invitations.Where(x => x.Issuer != toUser.Id).Any(), Is.False);
        }

        // Make sure, that all previous invitations, if still available, are mapped to the new user.
        foreach (var invitaion in projectInvitations)
        {
            if (dbContext.Invitations.Where(x => x.Id == invitaion.Id).Any())
            {
                Assert.That(dbContext.Invitations.Where(x => x.Issuer == toUser.Id && x.Id == invitaion.Id).Any());
            }
        }

        // Make sure, that each invitee is still present and none has been lost
        foreach (var inviteeEmail in distinctInviteeEmails)
        {
            Assert.That(dbContext.Invitations.Where(x => x.InviteeEmail == inviteeEmail).Any());
        }

        // Filtered is should be null
        Assert.That(dbContext.Users.Where(x => x.Id == fromUser.Id).FirstOrDefault(), Is.Null);

        // Unfiltered is shoul be found

        var user = dbContext.Users.IgnoreQueryFilters().Where(x => x.Id == fromUser.Id).FirstOrDefault();
        Assert.That(user, Is.Not.Null);
        Assert.That(user?.IsDeleted, Is.True);
    }

    [TestCaseSource(nameof(TwoDTestCases))]
    public async Task Merge_N_2_N_Activity_Logs_Success(int activityCountFrom, int activityCountTo)
    {
        // Arrange
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);


        var toUser = DatabaseSeeder.TestUser;
        var fromUser = (await factory.SeedUsersAsync()).First();

        var fromActivities = await factory.SeedActivityLogsAsync(userId: fromUser.Id, amount: activityCountFrom);
        var toActivities = await factory.SeedActivityLogsAsync(userId: toUser.Id, amount: activityCountTo);

        var activitiesIds = toActivities.Select(x => x.Id).Concat(fromActivities.Select(x => x.Id));

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var userService = serviceProvider.GetRequiredService<IUserService>();

        // Act
        await userService.MergeUser(fromUser.Id, toUser.Id);

        // Assert
        using var scope2 = factory.Services.CreateScope();
        var serviceProvider2 = scope.ServiceProvider;
        var dbContext = serviceProvider2.GetRequiredService<RepositoryContext>();

        // Make sure, that the correct amount was generated
        Assert.That(fromActivities.Count() == activityCountFrom);
        Assert.That(toActivities.Count() == activityCountTo);

        // Assert that no activity was deleted
        Assert.That(dbContext.ActivityLogs.Count() == activitiesIds.Count());
        foreach (var activityId in activitiesIds)
        {
            Assert.That(dbContext.ActivityLogs.Where(x => x.Id == activityId).Any());
        }

        // All activities are now under the new user

        foreach (var activity in dbContext.ActivityLogs)
        {
            Assert.That(activity.UserId != fromUser.Id);
        }

        // Filtered is should be null
        Assert.That(dbContext.Users.Where(x => x.Id == fromUser.Id).FirstOrDefault(), Is.Null);

        // Unfiltered is shoul be found

        var user = dbContext.Users.IgnoreQueryFilters().Where(x => x.Id == fromUser.Id).FirstOrDefault();
        Assert.That(user, Is.Not.Null);
        Assert.That(user?.IsDeleted, Is.True);
    }
}