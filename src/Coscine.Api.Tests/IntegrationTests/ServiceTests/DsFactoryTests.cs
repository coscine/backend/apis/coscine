using Coscine.Api.Core.Repository.Configurations;
using Coscine.Api.Core.Repository.Contracts;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace Coscine.Api.Tests.IntegrationTests.ServiceTests;

[TestFixture]
public class DsFactoryTests : CoscineControllerTestsBase
{
    private static readonly Guid[] _creatableTypes =
        [
            ResourceTypeConfiguration.ResourceTypeRdss3.Id,
            ResourceTypeConfiguration.ResourceTypeLinked.Id,
            ResourceTypeConfiguration.ResourceTypeRdss3Worm.Id,
            ResourceTypeConfiguration.ResourceTypeRds.Id,
            ResourceTypeConfiguration.ResourceTypeRdsTudo.Id,
            ResourceTypeConfiguration.ResourceTypeRdsRub.Id,
            ResourceTypeConfiguration.ResourceTypeGitlab.Id,
            ResourceTypeConfiguration.ResourceTypeRdss3Nrw.Id,
            ResourceTypeConfiguration.ResourceTypeRdss3Ude.Id,
            ResourceTypeConfiguration.ResourceTypeRdsUde.Id,
            ResourceTypeConfiguration.ResourceTypeRdss3Tudo.Id,
            ResourceTypeConfiguration.ResourceTypeRdss3Rub.Id,
            ResourceTypeConfiguration.ResourceTypeRdsNrw.Id,
    ];
    private static readonly Guid[] _missingTypes =
        [           
    ];

    [TestCaseSource(nameof(_creatableTypes))]
    public async Task CreateDs_NotNullAsync(Guid typeId)
    {
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);


        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var ds = serviceProvider.GetRequiredService<IDataStorageRepositoryFactory>().Create(typeId);

        Assert.That(ds, Is.Not.Null);
    }

    [TestCaseSource(nameof(_missingTypes))]
    public async Task CreateDs_NullAsync(Guid typeId)
    {
        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);


        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var ds = serviceProvider.GetRequiredService<IDataStorageRepositoryFactory>().Create(typeId);

        Assert.That(ds, Is.Null);
    }
}