using System.Security.Claims;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.BadRequest;
using Coscine.Api.Core.Entities.Exceptions.NotFound;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NSubstitute;
using NUnit.Framework;

namespace Coscine.Api.Tests.IntegrationTests.ServiceTests;

[TestFixture]
public class AuthenticatorServiceTests : CoscineControllerTestsBase
{
    private List<Claim> _dummyOidcClaims = null!;
    private List<Claim> _dummyShibbolethUserClaims = null!;
    private List<Claim> _dummyShibbolethTokenClaims = null!;

    [SetUp]
    public void Initialize()
    {
        _dummyOidcClaims =
        [
            // from real claims listing
            new(ClaimTypes.NameIdentifier, "sparkly-unicorn123"),
            new("scope", "openid profile email"),
            new(ClaimTypes.Email, "foo.bar@rainbow-jungle.xyz"),
            new("external_authenticator_id", "11111111-2222-3333-4444-555555555555"),
            new(ClaimTypes.GivenName, "Sugary"),
            new(ClaimTypes.Name, "Sugary Cupcake"),
            new(ClaimTypes.Surname, "Cupcake"),
            new("Affiliation", "member@glittery-community.org"),
            new("Affiliation", "employee@cotton-candy-inc.com"),
            new("name", "Sugary Cupcake"),
            new("given_name", "Sugary"),
            new("family_name", "Cupcake"),
        ];
        _dummyShibbolethUserClaims =
        [
            new("UserId", Guid.NewGuid().ToString()),
            new("iss", "https://coscine.rwth-aachen.de"),   // issuer
            new("aud", "https://coscine.rwth-aachen.de"),   // audience
            new("iat", "9999999999"),                       // issued at (string, but numeric in real usage)
            new("exp", "9999999999")                        // expiration (string, but numeric in real usage)
        ];
        _dummyShibbolethTokenClaims =
        [
            new("tokenId", Guid.NewGuid().ToString()),
            new("iss", "https://coscine.rwth-aachen.de"),
            new("aud", "https://coscine.rwth-aachen.de"),
            new("iat", "1010101010"),                       // issued at (string, but numeric in real usage)
            new("exp", "1010101010")                        // expiration (string, but numeric in real usage)
        ];
    }

    [Test]
    public async Task GetUserAsync_NoUserIdNoTokenIdNoExternalId_ReturnsNull()
    {
        // Arrange
        var testUser = DatabaseSeeder.TestUser;
        var userId = testUser.Id.ToString();
        var claims = _dummyShibbolethUserClaims;
        claims.RemoveAll(c => c.Type == ClaimTypes.NameIdentifier);
        claims.RemoveAll(c => c.Type == "UserId");
        claims.RemoveAll(c => c.Type == "tokenId");
        var principal = new ClaimsPrincipal(new ClaimsIdentity(claims));

        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);

        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var authenticatorService = serviceProvider.GetRequiredService<IAuthenticatorService>();

        // Act
        var result = await authenticatorService.GetUserAsync(principal, trackChanges: false);

        // Assert
        Assert.That(result, Is.Null);
    }

    [Test]
    public async Task GetUserAsync_OIDC_HasExternalIdButProviderNotSupported_ThrowsExternalAuthenticatorIdBadRequestExceptionAsync()
    {
        // Arrange
        var testUser = DatabaseSeeder.TestUser;
        var claims = _dummyOidcClaims;
        var principal = new ClaimsPrincipal(new ClaimsIdentity(claims));

        var authenticationConfigurationOptionsMock = Substitute.For<IOptionsMonitor<AuthenticationConfiguration>>();
        authenticationConfigurationOptionsMock
            .CurrentValue
            .Returns(new AuthenticationConfiguration
            {
                OpenIdConnectProviders = [
                    new() { ExternalId = "not-supported-provider" } // Invalid provider
                ]
            });

        var factory = CoscineTestExtensions.CreateFactory().WithServiceInstance(authenticationConfigurationOptionsMock);
        var client = await InitializeClientAsync(factory);
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var authenticatorService = serviceProvider.GetRequiredService<IAuthenticatorService>();

        // Act + Assert
        Assert.ThrowsAsync<ExternalAuthenticatorIdBadRequestException>(async () =>
            await authenticatorService.GetUserAsync(principal, trackChanges: false)
        );
    }

    [Test]
    public async Task GetUserAsync_OIDC_HasExternalIdButNotAGuid_ThrowsExternalAuthenticatorIdNotAGuidBadRequestExceptionAsync()
    {
        // Arrange
        var testUser = DatabaseSeeder.TestUser;
        var claims = _dummyOidcClaims;
        claims.RemoveAll(c => c.Type == "external_authenticator_id");
        claims.Add(new("external_authenticator_id", "supported-provider-but-not-guid"));
        var principal = new ClaimsPrincipal(new ClaimsIdentity(claims));

        var authenticationConfigurationOptionsMock = Substitute.For<IOptionsMonitor<AuthenticationConfiguration>>();
        authenticationConfigurationOptionsMock
            .CurrentValue
            .Returns(new AuthenticationConfiguration
            {
                OpenIdConnectProviders = [
                    new() { ExternalId = "supported-provider-but-not-guid" } // Valid provider
                ]
            });

        var factory = CoscineTestExtensions.CreateFactory().WithServiceInstance(authenticationConfigurationOptionsMock);
        var client = await InitializeClientAsync(factory);
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var authenticatorService = serviceProvider.GetRequiredService<IAuthenticatorService>();

        // Act + Assert
        Assert.ThrowsAsync<ExternalAuthenticatorIdNotAGuidBadRequestException>(async () =>
            await authenticatorService.GetUserAsync(principal, trackChanges: false)
        );
    }

    [Test]
    public async Task GetUserAsync_OIDC_ExternalIdFound_ExternalLookupAddsUserId()
    {
        // Arrange
        var testUser = DatabaseSeeder.TestUser;
        var claims = _dummyOidcClaims;
        var principal = new ClaimsPrincipal(new ClaimsIdentity(claims));

        var authenticationConfigurationOptionsMock = Substitute.For<IOptionsMonitor<AuthenticationConfiguration>>();
        authenticationConfigurationOptionsMock
            .CurrentValue
            .Returns(new AuthenticationConfiguration
            {
                OpenIdConnectProviders = [
                    new() { ExternalId = claims.First(c => c.Type == "external_authenticator_id").Value } // Valid provider
                ]
            });

        var userRepositoryMock = Substitute.For<IUserRepository>();
        userRepositoryMock
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(testUser);

        var externalIdRepositoryMock = Substitute.For<IExternalIdRepository>();
        externalIdRepositoryMock
            .GetAsync(Arg.Any<Guid>(), Arg.Any<string>(), Arg.Any<bool>())
            .Returns(new ExternalId { UserId = testUser.Id });

        var factory = CoscineTestExtensions.CreateFactory()
            .WithServiceInstance(userRepositoryMock)
            .WithServiceInstance(externalIdRepositoryMock)
            .WithServiceInstance(authenticationConfigurationOptionsMock);
        var client = await InitializeClientAsync(factory);
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var authenticatorService = serviceProvider.GetRequiredService<IAuthenticatorService>();


        // Act
        var result = await authenticatorService.GetUserAsync(principal, trackChanges: false);

        // Assert
        Assert.That(result, Is.Not.Null);
        Assert.That(result, Is.EqualTo(testUser));
    }

    [Test]
    public async Task GetUserAsync_OIDC_ExternalIdFoundButRepositoryReturnsNull_SkipsUserIdClaim_ReturnsNull()
    {
        // Arrange
        var testUser = DatabaseSeeder.TestUser;
        var claims = _dummyOidcClaims;
        var principal = new ClaimsPrincipal(new ClaimsIdentity(claims));

        var authenticationConfigurationOptionsMock = Substitute.For<IOptionsMonitor<AuthenticationConfiguration>>();
        authenticationConfigurationOptionsMock
            .CurrentValue
            .Returns(new AuthenticationConfiguration
            {
                OpenIdConnectProviders = [
                    new() { ExternalId = claims.First(c => c.Type == "external_authenticator_id").Value } // Valid provider
                ]
            });

        var userRepositoryMock = Substitute.For<IUserRepository>();
        userRepositoryMock
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns((User?)null); // No user found

        var externalIdRepositoryMock = Substitute.For<IExternalIdRepository>();
        externalIdRepositoryMock
            .GetAsync(Arg.Any<Guid>(), Arg.Any<string>(), Arg.Any<bool>())
            .Returns(new ExternalId { UserId = testUser.Id });


        var factory = CoscineTestExtensions.CreateFactory()
            .WithServiceInstance(userRepositoryMock)
            .WithServiceInstance(externalIdRepositoryMock)
            .WithServiceInstance(authenticationConfigurationOptionsMock);
        var client = await InitializeClientAsync(factory);
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var authenticatorService = serviceProvider.GetRequiredService<IAuthenticatorService>();


        // Act
        var result = await authenticatorService.GetUserAsync(principal, trackChanges: false);

        // Assert
        Assert.That(result, Is.Null, "For the OIDC claims, when the user is not found, the method should return null.");
    }

    [Test]
    public async Task GetUserAsync_SSO_HasUserIdAndTokenId_ThrowsUserAndTokenBadRequestExceptionAsync()
    {
        // Arrange
        var testUser = DatabaseSeeder.TestUser;
        var claims = _dummyShibbolethUserClaims;
        claims.Add(new("tokenId", Guid.NewGuid().ToString())); // Both UserId and TokenId present
        var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, "JWT"));

        var factory = CoscineTestExtensions.CreateFactory();
        var client = await InitializeClientAsync(factory);
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var authenticatorService = serviceProvider.GetRequiredService<IAuthenticatorService>();

        // Act + Assert
        Assert.ThrowsAsync<UserAndTokenBadRequestException>(async () =>
            await authenticatorService.GetUserAsync(principal, trackChanges: false)
        );
    }

    [Test]
    public async Task GetUserAsync_SSO_HasUserId_LoadsUserFromUserId()
    {
        // Arrange
        var testUser = DatabaseSeeder.TestUser;
        var userId = testUser.Id.ToString();
        var claims = _dummyShibbolethUserClaims;
        var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, "JWT"));

        var userRepositoryMock = Substitute.For<IUserRepository>();
        userRepositoryMock
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(testUser); // User found

        var factory = CoscineTestExtensions.CreateFactory().WithServiceInstance(userRepositoryMock);
        var client = await InitializeClientAsync(factory);
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var authenticatorService = serviceProvider.GetRequiredService<IAuthenticatorService>();

        // Act
        var result = await authenticatorService.GetUserAsync(principal, trackChanges: false);

        // Assert
        Assert.That(result, Is.Not.Null);
        Assert.That(result, Is.EqualTo(testUser));
    }

    [Test]
    public async Task GetUserAsync_SSO_HasTokenIdButTokenNotFound_ThrowsApiTokenNotFoundExceptionAsync()
    {
        // Arrange
        var testUser = DatabaseSeeder.TestUser;
        var claims = _dummyShibbolethTokenClaims;
        var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, "JWT"));


        var apiTokenRepositoryMock = Substitute.For<IApiTokenRepository>();
        apiTokenRepositoryMock
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns((ApiToken?)null); // No token found

        var factory = CoscineTestExtensions.CreateFactory().WithServiceInstance(apiTokenRepositoryMock);
        var client = await InitializeClientAsync(factory);
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var authenticatorService = serviceProvider.GetRequiredService<IAuthenticatorService>();

        // Act + Assert
        Assert.ThrowsAsync<ApiTokenNotFoundException>(async () =>
            await authenticatorService.GetUserAsync(principal, trackChanges: false)
        );
    }

    [Test]
    public async Task GetUserAsync_SSO_HasTokenId_LoadsUserFromToken()
    {
        // Arrange
        var testUser = DatabaseSeeder.TestUser;
        var claims = _dummyShibbolethTokenClaims;
        var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, "JWT"));


        var apiTokenRepositoryMock = Substitute.For<IApiTokenRepository>();
        apiTokenRepositoryMock
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(new ApiToken { UserId = testUser.Id }); // Token found

        var userRepositoryMock = Substitute.For<IUserRepository>();
        userRepositoryMock
            .GetAsync(Arg.Is<Guid>(id => id == testUser.Id), Arg.Any<bool>())
            .Returns(testUser); // User found


        var factory = CoscineTestExtensions.CreateFactory()
            .WithServiceInstance(apiTokenRepositoryMock)
            .WithServiceInstance(userRepositoryMock);
        var client = await InitializeClientAsync(factory);
        using var scope = factory.Services.CreateScope();
        var serviceProvider = scope.ServiceProvider;
        var authenticatorService = serviceProvider.GetRequiredService<IAuthenticatorService>();

        // Act
        var result = await authenticatorService.GetUserAsync(principal, trackChanges: false);

        // Assert
        Assert.That(result, Is.Not.Null);
        Assert.That(result, Is.EqualTo(testUser));
    }
}