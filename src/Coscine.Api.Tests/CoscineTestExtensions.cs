﻿using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Coscine.Api.Tests.IntegrationTests;
using Coscine.Api.Tests.Mockups;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NLog;
using Semiodesk.Trinity;
using Semiodesk.Trinity.Store;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace Coscine.Api.Tests;

public static class CoscineTestExtensions
{
    public static WebApplicationFactory<Program> WithService<TService, TImplementation>(
        this WebApplicationFactory<Program> factory
    )
        where TService : class
        where TImplementation : class, TService
    {
        return factory.WithWebHostBuilder(builder =>
        {
            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(d => d.ServiceType == typeof(TService));

                if (descriptor != null)
                {
                    services.Remove(descriptor);
                }

                services.AddSingleton<TService, TImplementation>();
            });
        });
    }

    public static WebApplicationFactory<Program> WithServiceInstance<TService>(
        this WebApplicationFactory<Program> factory,
        TService instance
    )
        where TService : class
    {
        return factory.WithWebHostBuilder(builder =>
        {
            builder.ConfigureServices(services =>
            {
                var descriptors = services.Where(d => d.ServiceType == typeof(TService)).ToArray();

                foreach (var descriptor in descriptors)
                {
                    services.Remove(descriptor);
                }

                services.AddSingleton(instance);
            });
        });
    }

    public static WebApplicationFactory<Program> WithInMemoryConfiguration(
        this WebApplicationFactory<Program> factory,
        Action<IConfigurationBuilder> configure
    )
    {
        return factory.WithWebHostBuilder(builder =>
        {
            builder.ConfigureAppConfiguration(
                (_, configuration) =>
                {
                    configure(configuration);
                }
            );
        });
    }

    public static WebApplicationFactory<Program> CreateFactory(Action<IServiceCollection>? configureServices = null, string? connectionString = null)
    {
        return new WebApplicationFactory<Program>().WithWebHostBuilder(builder =>
        {
            Environment.SetEnvironmentVariable("IS_TESTING_ENV", "true");

            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<RepositoryContext>));
                if (descriptor is not null)
                {
                    services.Remove(descriptor);
                }

                services.AddDbContext<RepositoryContext>(options => options
                .UseLazyLoadingProxies()
                .UseSqlServer(
                    connectionString ?? MsSqlTestContainerSetup.UserConnectionString,
                    builder =>
                    {
                        builder.MigrationsAssembly("Coscine.Api");
                        builder.EnableRetryOnFailure();
                    }
                ));

                // Persistent TripleStore to ensure that metadata stays the same between store implementations (dotNetRdf & Trinity)
                NotDisposingTripleStore persistentTripleStore = new();

                var rdfDescriptor = services.SingleOrDefault(d => d.ServiceType == typeof(IRdfRepositoryBase));
                if (rdfDescriptor is not null)
                {
                    services.Remove(rdfDescriptor);
                }
                services.AddScoped<IRdfRepositoryBase>(_ => new MockupRdfRepositoryBase(persistentTripleStore));

                var store = services.SingleOrDefault(d => d.ServiceType == typeof(IStore));
                if (store is not null)
                {
                    services.Remove(store);
                }
                services.AddScoped<IStore>(_ => new DotNetRdfTrinityAdaptedStore(persistentTripleStore));

                // Allow custom service configuration
                configureServices?.Invoke(services);

                // Set the default LogLevel
                LogManager.Configuration.Variables["logLevel"] = "Debug";

                // Set the log location
                LogManager.Configuration.Variables["logHome"] = Path.Combine(
                    AppDomain.CurrentDomain.BaseDirectory,
                    "Logs/IntegrationTests"
                );

                LogManager.ReconfigExistingLoggers();
            });
        });
    }

    public static HttpClient CreateCoscineClient(this WebApplicationFactory<Program> webApplicationFactory, Guid? tokenId = null, bool admin = false)
    {
        var jwtConfiguration =
            webApplicationFactory.Services.GetService<IOptionsMonitor<JwtConfiguration>>()
            ?? throw new InvalidOperationException("JWT configuration is missing or not properly set up.");

        using var scope = webApplicationFactory.Services.GetRequiredService<IServiceScopeFactory>().CreateScope();

        var authenticatorService = scope.ServiceProvider.GetService<IAuthenticatorService>();

        if (authenticatorService == null)
        {
            throw new ArgumentNullException(
                nameof(authenticatorService),
                "Authenticator service is not registered or not available."
            );
        }

        var claims = new Dictionary<string, string> {
            { "tokenId", (tokenId ?? DatabaseSeeder.TestApiToken.Id).ToString() },
        };
        if (admin)
        {
            claims.Add(ClaimTypes.Role, ApiRoles.Administrator);
        }

        var jwtSecurityToken = authenticatorService.GenerateJwtSecurityToken(
            claims,
            jwtConfiguration.CurrentValue.JsonWebKeys.First(x => x.KeySize >= 256)
        );

        var token = authenticatorService.GetJwtStringFromToken(jwtSecurityToken);

        var client = webApplicationFactory.CreateClient();

        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

        return client;
    }
}
