using Docker.DotNet.Models;
using DotNet.Testcontainers.Builders;
using DotNet.Testcontainers.Containers;
using NUnit.Framework;

namespace Coscine.Api.Tests;

[Explicit]
[TestFixture]
public class TestContainersTests
{
    private IContainer? _container;
    private static bool IsGitLabCi => Environment.GetEnvironmentVariable("CI") == "true";
    private static int Port => 8080;
    private static string NetworkName => "coscinedevcontainer_default";

    [SetUp]
    public async Task SetupAsync()
    {
        _container = new ContainerBuilder()
            // Set the image for the container to "testcontainers/helloworld:1.1.0".
            .WithImage("testcontainers/helloworld:1.1.0")
            // Bind port 8080. Use a random host port if not in CI; otherwise, use a fixed binding.
            .WithPortBinding(Port, IsGitLabCi)
            // Wait until the HTTP endpoint of the container is available.
            .WithWaitStrategy(Wait.ForUnixContainer().UntilMessageIsLogged(".*Ready.*"))
            // Use the appropriate network when not in CI.
            .ConfigureNetwork(IsGitLabCi, NetworkName)
            // Build the container configuration.
            .Build();

        // Register a shutdown hook to stop the container.
        AppDomain.CurrentDomain.ProcessExit += async (_, __) =>
        {
            try
            {
                await _container.StopAsync();
                await _container.DisposeAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error stopping container: {ex.Message}");
            }
        };

        await _container.StartAsync();
    }

    [TearDown]
    public async Task TearDownAsync()
    {
        if (_container is not null)
        {
            await _container.StopAsync();
            await _container.DisposeAsync();
        }
    }

    [Test]
    public async Task Can_Call_Endpoint()
    {
        Assert.That(_container, Is.Not.Null);

        var httpClient = new HttpClient();

        // Get Url based on CI
        var requestUri = IsGitLabCi
            ? new UriBuilder(Uri.UriSchemeHttp, _container!.Hostname, _container.GetMappedPublicPort(Port), "uuid").Uri
            : new UriBuilder(Uri.UriSchemeHttp, _container!.Name.TrimStart('/'), Port, "uuid").Uri;

        var guid = await httpClient.GetStringAsync(requestUri);

        Assert.That(Guid.TryParse(guid, out _), Is.True);
    }
}

// Extension to configure the network based on environment.
public static class ContainerBuilderExtensions
{
    public static ContainerBuilder ConfigureNetwork(this ContainerBuilder builder, bool useNetwork, string network)
    {
        if (!useNetwork)
        {
            return builder.WithNetwork(network);
        }
        return builder;
    }
}
