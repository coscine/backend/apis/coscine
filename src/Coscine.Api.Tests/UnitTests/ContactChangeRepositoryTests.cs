using NUnit.Framework;
using Microsoft.EntityFrameworkCore;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Tests.UnitTests;

[TestFixture]
public class ContactChangeRepositoryTests
{
    private DbContextOptions<RepositoryContext>? _dbContextOptions;

    [SetUp]
    public void Setup()
    {
        _dbContextOptions = new DbContextOptionsBuilder<RepositoryContext>()
            .UseInMemoryDatabase(Guid.NewGuid().ToString())
            .Options;
    }

    [TearDown]
    public void TearDown()
    {
        if (_dbContextOptions != null)
        {
            using var context = new RepositoryContext(_dbContextOptions);
            context.Database.EnsureDeleted();
        }
    }

    [Test]
    public async Task Create_AddsContactChangeToDatabase()
    {
        // Arrange
        using var context = new RepositoryContext(_dbContextOptions!);
        var repository = new ContactChangeRepository(context);

        var contactChange = new ContactChange
        {
            RelationId = Guid.NewGuid(),
            UserId = Guid.NewGuid(),
            NewEmail = "test@example.com",
            EditDate = DateTime.Now,
            ConfirmationToken = Guid.NewGuid()
        };

        // Act
        repository.Create(contactChange);
        await context.SaveChangesAsync();

        // Assert
        var addedContactChange = await context.Set<ContactChange>().SingleOrDefaultAsync();
        Assert.That(addedContactChange, Is.Not.Null, "Expected the contact change to be added to the database.");
        Assert.That(addedContactChange!.NewEmail, Is.EqualTo("test@example.com"), "Expected the email to match the added contact change.");
    }

    [Test]
    public async Task Delete_RemovesContactChangeFromDatabase()
    {
        // Arrange
        using var context = new RepositoryContext(_dbContextOptions!);
        var repository = new ContactChangeRepository(context);

        var contactChange = new ContactChange
        {
            RelationId = Guid.NewGuid(),
            UserId = Guid.NewGuid(),
            NewEmail = "delete@example.com",
            EditDate = DateTime.Now,
            ConfirmationToken = Guid.NewGuid()
        };

        context.Set<ContactChange>().Add(contactChange);
        await context.SaveChangesAsync();

        // Act
        repository.Delete(contactChange);
        await context.SaveChangesAsync();

        // Assert
        var remainingContactChanges = await context.Set<ContactChange>().ToListAsync();
        Assert.That(remainingContactChanges, Is.Empty, "Expected the contact change to be deleted from the database.");
    }

    [Test]
    public async Task GetAllAsync_ReturnsFilteredResults()
    {
        // Arrange
        using var context = new RepositoryContext(_dbContextOptions!);
        var repository = new ContactChangeRepository(context);

        var userId = Guid.NewGuid();
        var contactChanges = new List<ContactChange>
        {
            new() { RelationId = Guid.NewGuid(), UserId = userId, NewEmail = "test1@example.com", EditDate = DateTime.Now.AddDays(-1), ConfirmationToken = Guid.NewGuid() },
            new() { RelationId = Guid.NewGuid(), UserId = userId, NewEmail = "test2@example.com", EditDate = DateTime.Now.AddDays(2), ConfirmationToken = Guid.NewGuid() },
            new() { RelationId = Guid.NewGuid(), UserId = Guid.NewGuid(), NewEmail = "test3@example.com", EditDate = DateTime.Now.AddDays(-3), ConfirmationToken = Guid.NewGuid() }
        };

        context.Set<ContactChange>().AddRange(contactChanges);
        await context.SaveChangesAsync();

        var parameters = new ContactChangeParameters
        {
            EditDateBefore = DateTime.Now.AddDays(-1)
        };

        // Act
        var result = await repository.GetAllAsync(userId, parameters, false);

        // Assert
        Assert.That(result.Count(), Is.EqualTo(1), "Expected one contact change to match the filter criteria.");
        Assert.That(result.First().NewEmail, Is.EqualTo("test1@example.com"), "Expected the returned contact change to have the correct email.");
    }

    [Test]
    public async Task GetAsync_ReturnsCorrectContactChange()
    {
        // Arrange
        using var context = new RepositoryContext(_dbContextOptions!);
        var repository = new ContactChangeRepository(context);

        var tokenId = Guid.NewGuid();
        var contactChange = new ContactChange
        {
            RelationId = Guid.NewGuid(),
            UserId = Guid.NewGuid(),
            NewEmail = "find@example.com",
            EditDate = DateTime.Now,
            ConfirmationToken = tokenId
        };

        context.Set<ContactChange>().Add(contactChange);
        await context.SaveChangesAsync();

        // Act
        var result = await repository.GetAsync(tokenId, false);

        // Assert
        Assert.That(result, Is.Not.Null, "Expected the contact change to be found by confirmation token.");
        Assert.That(result!.NewEmail, Is.EqualTo("find@example.com"), "Expected the email to match the contact change.");
    }

    [Test]
    public async Task MergeAsync_RemovesContactChangesForFromUserId()
    {
        // Arrange
        using var context = new RepositoryContext(_dbContextOptions!);
        var repository = new ContactChangeRepository(context);

        var fromUserId = Guid.NewGuid();
        var contactChanges = new List<ContactChange>
        {
            new() { RelationId = Guid.NewGuid(), UserId = fromUserId, NewEmail = "merge1@example.com", EditDate = DateTime.Now, ConfirmationToken = Guid.NewGuid() },
            new() { RelationId = Guid.NewGuid(), UserId = fromUserId, NewEmail = "merge2@example.com", EditDate = DateTime.Now, ConfirmationToken = Guid.NewGuid() },
            new() { RelationId = Guid.NewGuid(), UserId = Guid.NewGuid(), NewEmail = "merge3@example.com", EditDate = DateTime.Now, ConfirmationToken = Guid.NewGuid() }
        };

        context.Set<ContactChange>().AddRange(contactChanges);
        await context.SaveChangesAsync();

        // Act
        await repository.MergeAsync(fromUserId, Guid.NewGuid());
        await context.SaveChangesAsync();

        // Assert
        var remainingContactChanges = context.Set<ContactChange>().Where(cc => cc.UserId == fromUserId).ToList();
        Assert.That(remainingContactChanges, Is.Empty, "Expected all contact changes for the source user to be deleted.");
    }
}