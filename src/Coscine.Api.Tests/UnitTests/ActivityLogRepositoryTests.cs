using NUnit.Framework;
using Microsoft.EntityFrameworkCore;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Tests.UnitTests;

[TestFixture]
public class ActivityLogRepositoryTests
{
    private DbContextOptions<RepositoryContext>? _dbContextOptions;

    [SetUp]
    public void Setup()
    {
        _dbContextOptions = new DbContextOptionsBuilder<RepositoryContext>()
            .UseInMemoryDatabase(Guid.NewGuid().ToString())
            .Options;
    }

    [TearDown]
    public void TearDown()
    {
        if (_dbContextOptions != null)
        {
            using var context = new RepositoryContext(_dbContextOptions);
            context.Database.EnsureDeleted();
        }
    }

    [Test]
    public async Task GetPagedAsync_ReturnsCorrectResults()
    {
        // Arrange
        using var context = new RepositoryContext(_dbContextOptions!);
        var repository = new ActivityLogRepository(context);

        var logs = new List<ActivityLog>
        {
            new() { Id = Guid.NewGuid(), HttpAction = "GET", ApiPath = "/api/test1", ActivityTimestamp = DateTime.Now.AddDays(-1) },
            new() { Id = Guid.NewGuid(), HttpAction = "GET", ApiPath = "/api/test2", ActivityTimestamp = DateTime.Now },
            new() { Id = Guid.NewGuid(), HttpAction = "GET", ApiPath = "/api/test3", ActivityTimestamp = DateTime.Now.AddDays(-2) }
        };

        context.Set<ActivityLog>().AddRange(logs);
        await context.SaveChangesAsync();

        var parameters = new ActivityLogParameters
        {
            PageNumber = 1,
            PageSize = 2,
            OrderBy = "activityTimestamp_desc"
        };

        // Act
        var result = await repository.GetPagedAsync(parameters, false);

        // Assert
        Assert.That(result.Pagination.PageSize, Is.EqualTo(2), "Page size should match the requested size.");
        Assert.That(result.Pagination.TotalCount, Is.EqualTo(3), "Total count should match the number of logs.");
        Assert.That(result.Count(), Is.EqualTo(2), "Result count should match the page size.");
        Assert.That(result.First().ApiPath, Is.EqualTo("/api/test2"), "First log should match the most recent activity.");
    }

    [Test]
    public async Task DeleteAllBeforeAsync_DeletesCorrectLogs()
    {
        // Arrange
        using var context = new RepositoryContext(_dbContextOptions!);
        var repository = new ActivityLogRepository(context);

        var logs = new List<ActivityLog>
        {
            new() { Id = Guid.NewGuid(), HttpAction = "GET", ApiPath = "/api/test1", ActivityTimestamp = DateTime.Now.AddDays(-5) },
            new() { Id = Guid.NewGuid(), HttpAction = "GET", ApiPath = "/api/test2", ActivityTimestamp = DateTime.Now.AddDays(-1) }
        };

        context.Set<ActivityLog>().AddRange(logs);
        await context.SaveChangesAsync();

        var cutoff = DateTime.Now.AddDays(-2);

        // Act
        repository.DeleteAllBeforeAsync(cutoff);
        await context.SaveChangesAsync();

        // Assert
        var remainingLogs = context.Set<ActivityLog>().ToList();
        Assert.That(remainingLogs.Count, Is.EqualTo(1), "Only one log should remain after deletion.");
        Assert.That(remainingLogs.First().ApiPath, Is.EqualTo("/api/test2"), "Remaining log should match the expected API path.");
    }

    [Test]
    public async Task MergeAsync_UpdatesUserIdsCorrectly()
    {
        // Arrange
        using var context = new RepositoryContext(_dbContextOptions!);
        var repository = new ActivityLogRepository(context);

        var fromUserId = Guid.NewGuid();
        var toUserId = Guid.NewGuid();

        var logs = new List<ActivityLog>
        {
            new() { Id = Guid.NewGuid(), HttpAction = "GET", UserId = fromUserId, ApiPath = "/api/test1" },
            new() { Id = Guid.NewGuid(), HttpAction = "GET", UserId = fromUserId, ApiPath = "/api/test2" },
            new() { Id = Guid.NewGuid(), HttpAction = "GET", UserId = Guid.NewGuid(), ApiPath = "/api/test3" }
        };

        context.Set<ActivityLog>().AddRange(logs);
        await context.SaveChangesAsync();

        // Act
        await repository.MergeAsync(fromUserId, toUserId);
        await context.SaveChangesAsync();

        // Assert
        var updatedLogs = context.Set<ActivityLog>().Where(log => log.UserId == toUserId).ToList();
        Assert.That(updatedLogs.Count, Is.EqualTo(2), "Two logs should have updated user IDs.");
        Assert.That(updatedLogs.All(log => log.UserId == toUserId), Is.True, "All updated logs should have the new user ID.");
    }
}