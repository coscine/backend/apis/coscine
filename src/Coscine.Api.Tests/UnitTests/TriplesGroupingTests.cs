﻿using Coscine.Api.Core.Repository.Extensions.Utility;
using NUnit.Framework;
using VDS.RDF;

namespace Coscine.Api.Tests.UnitTests;

[TestFixture]
public class TriplesGroupingTests
{
    /// <summary>
    /// Verifies that a simple, interconnected set of triples with blank nodes is correctly grouped into a single group.
    /// </summary>
    [Test]
    public void GroupTriplesByBlankNodes_SimpleCase()
    {
        var triples = new List<Triple>
        {
            new(new BlankNode("a"), new UriNode(new Uri("http://example.org/p1")), new LiteralNode("v1")),
            new(new BlankNode("a"), new UriNode(new Uri("http://example.org/p2")), new BlankNode("b")),
            new(new BlankNode("b"), new UriNode(new Uri("http://example.org/p3")), new LiteralNode("v2"))
        };

        var groupedTriples = triples.GroupTriplesByBlankNodes();

        Assert.That(groupedTriples.Count(), Is.EqualTo(1));
        Assert.That(groupedTriples.First().Count(), Is.EqualTo(3));
    }

    /// <summary>
    /// Mix of grounded and blank node triples. The triples should be grouped into two groups.
    /// One group contains grounded triples and the other contains blank node triples. 
    /// The blank node triples are grouped together, because they form a chain of dependent blank nodes.
    /// </summary>
    [Test]
    public void GroupTriplesByBlankNodes_GroundedAndBlankNodeTriples()
    {
        var triples = new List<Triple>
        {
            new(new UriNode(new Uri("http://example.org/s1")), new UriNode(new Uri("http://example.org/p1")), new LiteralNode("v1")),
            new(new UriNode(new Uri("http://example.org/s2")), new UriNode(new Uri("http://example.org/p2")), new LiteralNode("v2")),
            new(new BlankNode("a"), new UriNode(new Uri("http://example.org/p3")), new LiteralNode("v3")),
            new(new BlankNode("a"), new UriNode(new Uri("http://example.org/p4")), new BlankNode("b")),
            new(new BlankNode("b"), new UriNode(new Uri("http://example.org/p5")), new LiteralNode("v4"))
        };

        var groupedTriples = triples.GroupTriplesByBlankNodes();

        Assert.That(groupedTriples.Count(), Is.EqualTo(2));
        var groundTriplesGroup = groupedTriples.First(g => g.All(t => t.IsGroundTriple));
        var blankNodeGroup = groupedTriples.First(g => !g.All(t => t.IsGroundTriple));

        Assert.That(groundTriplesGroup.Count(), Is.EqualTo(2));
        Assert.That(blankNodeGroup.Count(), Is.EqualTo(3));
    }

    /// <summary>
    /// Tests grouping of multiple disconnected sets of triples with blank nodes.
    /// Each disconnected group should be grouped separately.
    /// </summary>
    [Test]
    public void GroupTriplesByBlankNodes_MultipleDisconnectedGroups()
    {
        var triples = new List<Triple>
        {
            new(new BlankNode("a"), new UriNode(new Uri("http://example.org/p1")), new LiteralNode("v1")),
            new(new BlankNode("b"), new UriNode(new Uri("http://example.org/p2")), new BlankNode("c")),
            new(new BlankNode("c"), new UriNode(new Uri("http://example.org/p3")), new LiteralNode("v2")),
            new(new BlankNode("d"), new UriNode(new Uri("http://example.org/p4")), new BlankNode("e")),
            new(new BlankNode("e"), new UriNode(new Uri("http://example.org/p5")), new BlankNode("f")),
            new(new BlankNode("f"), new UriNode(new Uri("http://example.org/p6")), new LiteralNode("v3"))
        };

        var groupedTriples = triples.GroupTriplesByBlankNodes();

        Assert.That(groupedTriples.Count(), Is.EqualTo(3));
        Assert.That(groupedTriples.Any(g => g.Count() == 1 && g.First().Subject.Equals(new BlankNode("a"))));
        Assert.That(groupedTriples.Any(g => g.Count() == 2 && g.First().Subject.Equals(new BlankNode("b"))));
        Assert.That(groupedTriples.Any(g => g.Count() == 3 && g.First().Subject.Equals(new BlankNode("d"))));
    }

    /// <summary>
    /// Tests grouping of a complex set of interconnected triples with blank nodes.
    /// One group forms a cycle, and the other is a single, isolated triple.
    /// </summary>
    [Test]
    public void GroupTriplesByBlankNodes_ComplexInterconnectedGroups()
    {
        var triples = new List<Triple>
        {
            new(new BlankNode("a"), new UriNode(new Uri("http://example.org/p1")), new BlankNode("b")),
            new(new BlankNode("b"), new UriNode(new Uri("http://example.org/p2")), new BlankNode("c")),
            new(new BlankNode("c"), new UriNode(new Uri("http://example.org/p3")), new BlankNode("d")),
            new(new BlankNode("d"), new UriNode(new Uri("http://example.org/p4")), new BlankNode("a")), // Forms a cycle
            new(new BlankNode("e"), new UriNode(new Uri("http://example.org/p5")), new LiteralNode("v1"))
        };

        var groupedTriples = triples.GroupTriplesByBlankNodes();

        Assert.That(groupedTriples.Count(), Is.EqualTo(2));
        var cyclicGroup = groupedTriples.First(g => g.Count() == 4);
        var singleTripleGroup = groupedTriples.First(g => g.Count() == 1);

        Assert.That(cyclicGroup, Is.Not.Null);
        Assert.That(singleTripleGroup, Is.Not.Null);
    }

    /// <summary>
    /// Tests grouping of a set of only grounded triples.
    /// All grounded triples should be grouped into a single group.
    /// </summary>
    [Test]
    public void GroupTriplesByBlankNodes_AllGroundedTriples()
    {
        var triples = new List<Triple>
        {
            new(new UriNode(new Uri("http://example.org/s1")), new UriNode(new Uri("http://example.org/p1")), new LiteralNode("v1")),
            new(new UriNode(new Uri("http://example.org/s2")), new UriNode(new Uri("http://example.org/p2")), new LiteralNode("v2")),
            new(new UriNode(new Uri("http://example.org/s3")), new UriNode(new Uri("http://example.org/p3")), new LiteralNode("v3"))
        };

        var groupedTriples = triples.GroupTriplesByBlankNodes();

        Assert.That(groupedTriples.Count(), Is.EqualTo(1));
        Assert.That(groupedTriples.First().Count(), Is.EqualTo(3));
        Assert.That(groupedTriples.First().All(t => t.IsGroundTriple));
    }

    /// <summary>
    /// Tests grouping of a large collection of triples with nested blank nodes.
    /// Each pair of nested blank node triples should be grouped correctly.
    /// </summary>
    [Test]
    public void GroupTriplesByBlankNodes_LargeCollectionWithNestedBlankNodes()
    {
        const int numTriples = 100000;
        var triples = new List<Triple>();

        for (int i = 0; i < numTriples; i++)
        {
            var blankNode1 = new BlankNode($"a{i}");
            var blankNode2 = new BlankNode($"b{i}");
            triples.Add(new Triple(blankNode1, new UriNode(new Uri("http://example.org/p1")), blankNode2));
            triples.Add(new Triple(blankNode2, new UriNode(new Uri("http://example.org/p2")), new LiteralNode($"v{i}")));
        }

        var groupedTriples = triples.GroupTriplesByBlankNodes();

        Assert.That(groupedTriples.Count(), Is.EqualTo(numTriples));
        foreach (var group in groupedTriples)
        {
            Assert.That(group.Count(), Is.EqualTo(2));
        }
    }
}
