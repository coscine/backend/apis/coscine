using System.Security.Claims;
using Coscine.Api.Core.Entities.Exceptions.Forbidden;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.AspNetCore.Authorization;
using NSubstitute;
using NUnit.Framework;

namespace Coscine.Api.Tests.UnitTests.ServiceTests;

[TestFixture]
public class BlobServiceTests
{
    private IProjectRepository _projectRepository = null!;
    private IResourceRepository _resourceRepository = null!;
    private ITreeRepository _treeRepository = null!;
    private IAuthenticatorService _authenticatorService = null!;
    private IAuthorizationService _authorizationService = null!;
    private ITreeService _treeService = null!;
    private IDataStorageRepositoryFactory _dataStorageRepositoryFactory = null!;

    private BlobService _blobService = null!;

    [SetUp]
    public void Setup()
    {
        _projectRepository = Substitute.For<IProjectRepository>();
        _resourceRepository = Substitute.For<IResourceRepository>();
        _treeRepository = Substitute.For<ITreeRepository>();
        _authenticatorService = Substitute.For<IAuthenticatorService>();
        _authorizationService = Substitute.For<IAuthorizationService>();
        _treeService = Substitute.For<ITreeService>();
        _dataStorageRepositoryFactory = Substitute.For<IDataStorageRepositoryFactory>();

        _blobService = new BlobService(
            _projectRepository,
            _resourceRepository,
            _treeRepository,
            _authenticatorService,
            _authorizationService,
            _treeService,
            _dataStorageRepositoryFactory);
    }

    #region GetBlobStreamAsync

    [Test]
    public async Task GetBlobStreamAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var key = "valid-key";
        var principal = new ClaimsPrincipal();
        var userId = Guid.NewGuid();
        var userEntity = new User { Id = userId };

        _authenticatorService
            .GetUserAsync(principal, Arg.Any<bool>())
            .Returns(Task.FromResult((User?)userEntity));

        var projectEntity = new Project { Id = projectId };
        _projectRepository
            .GetAsync(projectId, Arg.Any<bool>())
            .Returns(Task.FromResult((Project?)projectEntity));

        var resourceType = new ResourceType { DisplayName = "TestResourceType" };
        var resourceEntity = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = resourceType,
            Archived = false ? "1" : "0",
        };
        _resourceRepository
            .GetAsync(projectId, resourceId, Arg.Any<bool>())
            .Returns(Task.FromResult((Resource?)resourceEntity));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.ReadBlob) == true)
                {
                    // User is authorized to read the blob.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports both reading and maintenance mode.
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode, Capability.CanRead]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _blobService.GetBlobStreamAsync(projectId, resourceId, key, principal, trackChanges: false);
        });

        await _authorizationService
            // The maintenance mode authorization was called.
            .Received().AuthorizeAsync(principal, resourceEntity, Arg.Is<IEnumerable<IAuthorizationRequirement>>(r => r.Any(rr => rr == ResourceOperations.ReadBlob)));

        _dataStorageRepositoryFactory
            // The data storage creation was called.
            .Received().Create(Arg.Any<Guid>());
        await dataStorageMock
            // Since an exception is thrown, no call to the data storage update is made.
            .DidNotReceive().ReadAsync(Arg.Any<ReadBlobParameters>());
    }

    #endregion

    #region DeleteBlobAsync

    [Test]
    public async Task DeleteBlobAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var key = "valid-key";
        var principal = new ClaimsPrincipal();
        var userId = Guid.NewGuid();
        var userEntity = new User { Id = userId };

        _authenticatorService
            .GetUserAsync(principal, Arg.Any<bool>())
            .Returns(Task.FromResult((User?)userEntity));

        var projectEntity = new Project { Id = projectId };
        _projectRepository
            .GetAsync(projectId, Arg.Any<bool>())
            .Returns(Task.FromResult((Project?)projectEntity));

        var resourceType = new ResourceType { DisplayName = "TestResourceType" };
        var resourceEntity = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = resourceType,
            Archived = false ? "1" : "0",
        };
        _resourceRepository
            .GetAsync(projectId, resourceId, Arg.Any<bool>())
            .Returns(Task.FromResult((Resource?)resourceEntity));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.DeleteBlob) == true)
                {
                    // User is authorized to delete the blob.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports deletion and maintenance mode.
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode, Capability.CanDelete]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _blobService.DeleteBlobAsync(projectId, resourceId, key, principal, trackChanges: false);
        });

        await _authorizationService
            // The maintenance mode authorization was called.
            .Received().AuthorizeAsync(principal, resourceEntity, Arg.Is<IEnumerable<IAuthorizationRequirement>>(r => r.Any(rr => rr == ResourceOperations.DeleteBlob)));

        _dataStorageRepositoryFactory
            // The data storage creation was called.
            .Received().Create(Arg.Any<Guid>());
        await dataStorageMock
            // Since an exception is thrown, no call to the data storage delete is made.
            .DidNotReceive().DeleteAsync(Arg.Any<DeleteBlobParameters>());
    }
    #endregion

    #region CreateBlobAsync

    [Test]
    public async Task CreateBlobAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var key = "valid-key";
        var principal = new ClaimsPrincipal();
        var userId = Guid.NewGuid();
        var userEntity = new User { Id = userId };

        _authenticatorService
            .GetUserAsync(principal, Arg.Any<bool>())
            .Returns(Task.FromResult((User?)userEntity));

        var projectEntity = new Project { Id = projectId };
        _projectRepository
            .GetAsync(projectId, Arg.Any<bool>())
            .Returns(Task.FromResult((Project?)projectEntity));

        var resourceType = new ResourceType { DisplayName = "TestResourceType" };
        var resourceEntity = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = resourceType,
            Archived = false ? "1" : "0",
        };
        _resourceRepository
            .GetAsync(projectId, resourceId, Arg.Any<bool>())
            .Returns(Task.FromResult((Resource?)resourceEntity));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.CreateBlob) == true)
                {
                    // User is authorized to create the blob.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _blobService.CreateBlobAsync(
                projectId, resourceId, key, principal, new MemoryStream(), length: 100, trackChanges: false);
        });

        await _authorizationService
            // The maintenance mode authorization was called.
            .Received().AuthorizeAsync(principal, resourceEntity, Arg.Is<IEnumerable<IAuthorizationRequirement>>(r => r.Any(rr => rr == ResourceOperations.CreateBlob)));

        _dataStorageRepositoryFactory
            // The data storage creation was called.
            .Received().Create(Arg.Any<Guid>());
        await dataStorageMock
            // Since an exception is thrown, no call to the data storage create is made.
            .DidNotReceive().CreateAsync(Arg.Any<CreateBlobParameters>());
    }

    #endregion

    #region UpdateBlobAsync

    [Test]
    public async Task UpdateBlobAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var key = "valid-key";
        var principal = new ClaimsPrincipal();
        var userId = Guid.NewGuid();
        var userEntity = new User { Id = userId };

        _authenticatorService
            .GetUserAsync(principal, Arg.Any<bool>())
            .Returns(Task.FromResult((User?)userEntity));

        var projectEntity = new Project { Id = projectId };
        _projectRepository
            .GetAsync(projectId, Arg.Any<bool>())
            .Returns(Task.FromResult((Project?)projectEntity));

        var resourceType = new ResourceType { DisplayName = "TestResourceType" };
        var resourceEntity = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = resourceType,
            Archived = false ? "1" : "0",
        };
        _resourceRepository
            .GetAsync(projectId, resourceId, Arg.Any<bool>())
            .Returns(Task.FromResult((Resource?)resourceEntity));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.UpdateBlob) == true)
                {
                    // User is authorized to update the blob.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        _treeRepository
            .GetNewestPagedMetadataAsync(resourceId, Arg.Any<MetadataTreeParameters>())
            .Returns(Task.FromResult(new PagedEnumerable<MetadataTree>([new()], 1, 1, 1))); // Dummy metadata.

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _blobService.UpdateBlobAsync(
                projectId, resourceId, key, principal, new MemoryStream(), length: 50, trackChanges: false);
        });

        await _authorizationService
            // The maintenance mode authorization was called.
            .Received().AuthorizeAsync(principal, resourceEntity, Arg.Is<IEnumerable<IAuthorizationRequirement>>(r => r.Any(rr => rr == ResourceOperations.UpdateBlob)));

        _dataStorageRepositoryFactory
            // The data storage creation was called.
            .Received().Create(Arg.Any<Guid>());
        await dataStorageMock
            // Since an exception is thrown, no call to the data storage update is made.
            .DidNotReceive().UpdateAsync(Arg.Any<UpdateBlobParameters>());
    }

    #endregion
}