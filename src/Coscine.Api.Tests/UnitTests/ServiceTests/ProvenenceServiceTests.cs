using System.Security.Claims;
using NSubstitute;
using NUnit.Framework;
using AutoMapper;
using Coscine.Api.Core.Service;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Microsoft.AspNetCore.Authorization;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Entities.Exceptions.Forbidden;
using VDS.RDF;

namespace Coscine.Api.Tests.UnitTests.ServiceTests;

[TestFixture]
public class ProvenanceServiceTests
{
    private IResourceRepository _resourceRepository = null!;
    private IProvenanceRepository _provenanceRepository = null!;
    private ITreeRepository _treeRepository = null!;
    private ITreeService _treeService = null!;
    private IMapper _mapper = null!;
    private IAuthorizationService _authorizationService = null!;
    private IDataStorageRepositoryFactory _dataStorageRepositoryFactory = null!;
    private IProvenanceService _provenanceService = null!;

    [SetUp]
    public void Setup()
    {
        _resourceRepository = Substitute.For<IResourceRepository>();
        _provenanceRepository = Substitute.For<IProvenanceRepository>();
        _treeRepository = Substitute.For<ITreeRepository>();
        _treeService = Substitute.For<ITreeService>();
        _mapper = Substitute.For<IMapper>();
        _authorizationService = Substitute.For<IAuthorizationService>();
        _dataStorageRepositoryFactory = Substitute.For<IDataStorageRepositoryFactory>();

        _provenanceService = new ProvenanceService(
            _resourceRepository,
            _provenanceRepository,
            _treeRepository,
            _treeService,
            _mapper,
            _authorizationService,
            _dataStorageRepositoryFactory);
    }

    #region UpdateProvenanceAsync

    [Test]
    public async Task UpdateProvenanceAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var principal = new ClaimsPrincipal();

        // Create a dummy provenance update DTO. Adjust properties as needed.
        var provenanceForUpdateDto = new ProvenanceForUpdateDto { Id = new Uri("https://example.org/") };

        var resource = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = new ResourceType { DisplayName = "TestType" }
        };

        _resourceRepository.GetAsync(projectId, resourceId, Arg.Any<bool>())
            .Returns(Task.FromResult((Resource?)resource));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.UpdateProvenance) == true)
                {
                    // User is authorized to update provenance.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        // Set up a data storage provider that supports maintenance mode.
        var dataStorage = Substitute.For<IDataStorageRepository>();
        dataStorage.Capabilities.Returns([Capability.SupportsMaintenanceMode]);
        _dataStorageRepositoryFactory.Create(resource.TypeId).Returns(dataStorage);

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _provenanceService.UpdateProvenanceAsync(projectId, resourceId, principal, provenanceForUpdateDto);
        });

        await _provenanceRepository
            // Verify that no provenance update work is performed.
            .DidNotReceive().AddProvenanceInformation(Arg.Any<Uri>(), Arg.Any<bool>(), Arg.Any<bool>());
        await _treeService
            // Verify that no metadata copy work is performed.
            .DidNotReceive().CopyMetadataLocalAsync(Arg.Any<Resource>(), Arg.Any<IGraph>());
    }

    #endregion
}
