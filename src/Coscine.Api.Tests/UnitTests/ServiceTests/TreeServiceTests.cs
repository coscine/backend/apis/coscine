using System.Security.Claims;
using AutoMapper;
using NSubstitute;
using NUnit.Framework;
using Coscine.Api.Core.Service;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Microsoft.AspNetCore.Authorization;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Entities.Exceptions.Forbidden;
using Coscine.Api.Core.Shared.QueryParameters;
using Coscine.Api.Core.Shared.RequestFeatures;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.Enums;
using VDS.RDF;
using Coscine.Api.Core.Entities.OtherModels;

namespace Coscine.Api.Tests.UnitTests.ServiceTests;

[TestFixture]
public class TreeServiceTests
{
    private IProjectRepository _projectRepository = null!;
    private IResourceRepository _resourceRepository = null!;
    private IProvenanceRepository _provenanceRepository = null!;
    private ITreeRepository _treeRepository = null!;
    private IAuthenticatorService _authenticatorService = null!;
    private IMapper _mapper = null!;
    private IAuthorizationService _authorizationService = null!;
    private IRdfRepositoryBase _rdfRepositoryBase = null!;
    private IDataStorageRepositoryFactory _dataStorageRepositoryFactory = null!;
    private TreeService _treeService = null!;

    [SetUp]
    public void Setup()
    {
        _projectRepository = Substitute.For<IProjectRepository>();
        _resourceRepository = Substitute.For<IResourceRepository>();
        _provenanceRepository = Substitute.For<IProvenanceRepository>();
        _treeRepository = Substitute.For<ITreeRepository>();
        _authenticatorService = Substitute.For<IAuthenticatorService>();
        _mapper = Substitute.For<IMapper>();
        _authorizationService = Substitute.For<IAuthorizationService>();
        _rdfRepositoryBase = Substitute.For<IRdfRepositoryBase>();
        _dataStorageRepositoryFactory = Substitute.For<IDataStorageRepositoryFactory>();

        _treeService = new TreeService(
            _projectRepository,
            _resourceRepository,
            _provenanceRepository,
            _treeRepository,
            _authenticatorService,
            _mapper,
            _authorizationService,
            _rdfRepositoryBase,
            _dataStorageRepositoryFactory);
    }

    #region GetMetadataTreeAsync

    [Test]
    public async Task GetMetadataTreeAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var principal = new ClaimsPrincipal();
        var userId = Guid.NewGuid();
        var user = new User { Id = userId };

        _authenticatorService.GetUserAsync(principal, Arg.Any<bool>())
            .Returns(Task.FromResult((User?)user));

        var project = new Project { Id = projectId };
        _projectRepository.GetAsync(projectId, Arg.Any<bool>())
            .Returns(Task.FromResult((Project?)project));

        var resource = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = new ResourceType { DisplayName = "TestType" },
            MetadataLocalCopy = false
        };
        _resourceRepository.GetAsync(projectId, resourceId, Arg.Any<bool>())
            .Returns(Task.FromResult((Resource?)resource));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.ReadTree) == true)
                {
                    // User is authorized to read the tree.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _treeService.GetMetadataTreeAsync(projectId, resourceId, principal, new MetadataTreeQueryParameters { Path = "valid/path" });
        });

        await _treeRepository
            // Verify that the repository to retrieve metadata is never called.
            .DidNotReceive().GetMetadataAsync(Arg.Any<Guid>(), Arg.Any<MetadataTreeQueryParameters>());
    }

    #endregion

    #region GetPagedMetadataTreeAsync

    [Test]
    public async Task GetPagedMetadataTreeAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var principal = new ClaimsPrincipal();
        var userId = Guid.NewGuid();
        var user = new User { Id = userId };

        _authenticatorService.GetUserAsync(principal, Arg.Any<bool>())
            .Returns(Task.FromResult((User?)user));

        var project = new Project { Id = projectId };
        _projectRepository.GetAsync(projectId, Arg.Any<bool>())
            .Returns(Task.FromResult((Project?)project));

        var resource = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = new ResourceType { DisplayName = "TestType" }
        };
        _resourceRepository.GetAsync(projectId, resourceId, Arg.Any<bool>())
            .Returns(Task.FromResult((Resource?)resource));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.ReadTree) == true)
                {
                    // User is authorized to read the tree.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _treeService.GetPagedMetadataTreeAsync(projectId, resourceId, principal, new MetadataTreeParameters());
        });

        await _treeRepository
            // Verify that the repository to retrieve paged metadata is never called.
            .DidNotReceive().GetNewestPagedMetadataAsync(Arg.Any<Guid>(), Arg.Any<MetadataTreeParameters>());
    }

    #endregion

    #region GetPagedFileTreeAsync

    [Test]
    public async Task GetPagedFileTreeAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var principal = new ClaimsPrincipal();
        var userId = Guid.NewGuid();
        var user = new User { Id = userId };

        _authenticatorService.GetUserAsync(principal, Arg.Any<bool>())
            .Returns(Task.FromResult((User?)user));

        var project = new Project { Id = projectId };
        _projectRepository.GetAsync(projectId, Arg.Any<bool>())
            .Returns(Task.FromResult((Project?)project));

        var resource = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = new ResourceType { DisplayName = "TestType" }
        };
        _resourceRepository.GetAsync(projectId, resourceId, Arg.Any<bool>())
            .Returns(Task.FromResult((Resource?)resource));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.ReadTree) == true)
                {
                    // User is authorized to read the tree.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _treeService.GetPagedFileTreeAsync(projectId, resourceId, principal, new FileTreeParameters { PageNumber = 1, PageSize = 10, Path = "" });
        });

        await dataStorageMock
            // Verify that the data storage to list files is never called.
            .DidNotReceive().ListAsync(Arg.Any<ListTreeParameters>());
    }

    #endregion

    #region CreateMetadataTreeAsync

    [Test]
    public async Task CreateMetadataTreeAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var principal = new ClaimsPrincipal();
        // Assume non-administrator so that a user is required.
        var user = new User { Id = Guid.NewGuid() };

        _authenticatorService.GetUserAsync(principal, Arg.Any<bool>())
            .Returns(Task.FromResult((User?)user));

        var resource = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = new ResourceType { DisplayName = "TestType" },
            MetadataLocalCopy = false
        };
        _resourceRepository.GetAsync(projectId, resourceId, Arg.Any<bool>())
            .Returns(Task.FromResult((Resource?)resource));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.CreateTree) == true)
                {
                    // User is authorized to create the tree.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        // Provide a dummy valid creation DTO.
        var creationDto = new MetadataTreeForCreationDto { Path = "valid/path", Definition = new() { Content = "", Type = RdfFormat.Turtle } };

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _treeService.CreateMetadataTreeAsync(projectId, resourceId, principal, creationDto);
        });

        await _treeRepository
            // Verify that the repository to create metadata is never called.
            .DidNotReceive().CreateMetadataGraphAsync(Arg.Any<Resource>(), Arg.Any<User>(), Arg.Any<MetadataTree>());
    }

    #endregion

    #region CreateExtractedMetadataTreeAsync

    [Test]
    public async Task CreateExtractedMetadataTreeAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var principal = new ClaimsPrincipal();
        var user = new User { Id = Guid.NewGuid() };

        _authenticatorService.GetUserAsync(principal, Arg.Any<bool>())
            .Returns(Task.FromResult((User?)user));

        var resource = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = new ResourceType { DisplayName = "TestType" },
            MetadataLocalCopy = false
        };
        _resourceRepository.GetAsync(projectId, resourceId, Arg.Any<bool>())
            .Returns(Task.FromResult((Resource?)resource));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.CreateExtractionTree) == true)
                {
                    // User is authorized to create extracted metadata tree.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        var extractedDto = new ExtractedMetadataTreeForCreationDto { Id = new Uri("https://example.org/"), Path = "valid/path", Definition = new() { Content = "", Type = RdfFormat.Turtle }, Provenance = new() { } };

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _treeService.CreateExtractedMetadataTreeAsync(projectId, resourceId, principal, extractedDto);
        });

        await _treeRepository
            // Verify that the repository to create metadata is never called.
            .DidNotReceive().CreateMetadataGraphAsync(Arg.Any<Resource>(), Arg.Any<User>(), Arg.Any<MetadataTree>());
    }

    #endregion

    #region UpdateMetadataTreeAsync

    [Test]
    public async Task UpdateMetadataTreeAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var principal = new ClaimsPrincipal();
        var user = new User { Id = Guid.NewGuid() };

        // Non-admin so user is fetched.
        _authenticatorService.GetUserAsync(principal, Arg.Any<bool>())
            .Returns(Task.FromResult((User?)user));

        var resource = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = new ResourceType { DisplayName = "TestType" },
            MetadataLocalCopy = false
        };
        _resourceRepository.GetAsync(projectId, resourceId, Arg.Any<bool>())
            .Returns(Task.FromResult((Resource?)resource));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.UpdateTree) == true)
                {
                    // User is authorized to update the tree.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        var updateDto = new MetadataTreeForUpdateDto { Path = "valid/path", Definition = new() { Content = "", Type = RdfFormat.Turtle } };

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _treeService.UpdateMetadataTreeAsync(projectId, resourceId, principal, updateDto);
        });

        await _treeRepository
            // Verify that the repository to update metadata is never called.
            .DidNotReceive().UpdateMetadataGraphAsync(Arg.Any<Resource>(), Arg.Any<User>(), Arg.Any<MetadataTree>());
    }

    #endregion

    #region UpdateExtractedMetadataTreeAsync

    [Test]
    public async Task UpdateExtractedMetadataTreeAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var principal = new ClaimsPrincipal();
        var user = new User { Id = Guid.NewGuid() };

        _authenticatorService.GetUserAsync(principal, Arg.Any<bool>())
            .Returns(Task.FromResult((User?)user));

        var resource = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = new ResourceType { DisplayName = "TestType" },
            MetadataLocalCopy = true // even if local copy is enabled, maintenance mode should block the update.
        };
        _resourceRepository.GetAsync(projectId, resourceId, Arg.Any<bool>())
            .Returns(Task.FromResult((Resource?)resource));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.UpdateExtractionTree) == true)
                {
                    // User is authorized to update the extracted metadata tree.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        var updateDto = new ExtractedMetadataTreeForUpdateDto { Id = new Uri("https://example.org/"), Path = "valid/path", Definition = new() { Content = "", Type = RdfFormat.Turtle }, Provenance = new() { } };

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _treeService.UpdateExtractedMetadataTreeAsync(projectId, resourceId, principal, updateDto);
        });

        await _treeRepository
            // Verify that the repository to update metadata is never called.
            .DidNotReceive().UpdateMetadataGraphAsync(Arg.Any<Resource>(), Arg.Any<User>(), Arg.Any<MetadataTree>());
    }

    #endregion

    #region DeleteMetadataTreeAsync

    [Test]
    public async Task DeleteMetadataTreeAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var principal = new ClaimsPrincipal();

        var resource = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = new ResourceType { DisplayName = "TestType" }
        };
        _resourceRepository.GetAsync(projectId, resourceId, Arg.Any<bool>())
            .Returns(Task.FromResult((Resource?)resource));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.DeleteTree) == true)
                {
                    // User is authorized to delete the tree.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        var deletionDto = new MetadataTreeForDeletionDto { Path = "valid/path", InvalidatedBy = new Uri("https://example.org/"), Version = 1 };

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _treeService.DeleteMetadataTreeAsync(projectId, resourceId, principal, deletionDto);
        });

        await _provenanceRepository
            // Verify that the provenance repository to create invalidation is never called.
            .DidNotReceive().CreateInvalidation(Arg.Any<Guid>(), Arg.Any<string>(), Arg.Any<Uri>(), Arg.Any<string>());
    }

    #endregion

    #region CopyMetadataLocalAsync

    [Test]
    public async Task CopyMetadataLocalAsync_WhenResourceInMaintenanceMode_ThrowsException()
    {
        // Arrange
        var resource = new Resource
        {
            Id = Guid.NewGuid(),
            MaintenanceMode = true,
            TypeId = Guid.NewGuid(),
            Type = new ResourceType { DisplayName = "TestType" }
        };

        // Create a dummy non-patch graph substitute.
        var graph = Substitute.For<IGraph>();
        var baseUri = new Uri("http://example.com/graph");
        graph.BaseUri.Returns(baseUri);

        // Stub the RDF repository to return some dummy JSON.
        _rdfRepositoryBase.WriteGraph(graph, RdfFormat.JsonLd).Returns("{}");

        var dataStorageMock = Substitute.For<IDataStorageRepository>();
        dataStorageMock.Capabilities.Returns([Capability.SupportsMaintenanceMode]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);
        
        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
        {
            await _treeService.CopyMetadataLocalAsync(resource, graph);
        });

        await dataStorageMock
            // Verify that the data storage to create is never called.
            .DidNotReceive().CreateAsync(Arg.Any<CreateBlobParameters>());
    }

    #endregion
}
