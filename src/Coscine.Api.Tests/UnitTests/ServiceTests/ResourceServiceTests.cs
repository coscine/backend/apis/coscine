using System.Security.Claims;
using AutoMapper;
using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Entities.Exceptions.Forbidden;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Coscine.Api.Core.Shared;
using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Tests.Mockups;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NETCore.MailKit.Core;
using NSubstitute;
using NSubstitute.ReceivedExtensions;
using NUnit.Framework;

namespace Coscine.Api.Tests.UnitTests.ServiceTests;

[TestFixture]
public class ResourceServiceTests
{
    private DbContextOptions<RepositoryContext>? _dbContextOptions;
    private IMapper _mapper = null!;
    private IRepositoryContextLoader _repositoryContextLoader = null!;
    private IObserverManager _observerManager = null!;
    private IProjectRepository _projectRepository = null!;
    private IResourceRepository _resourceRepository = null!;
    private IResourceDisciplineRepository _resourceDisciplineRepository = null!;
    private IResourceTypeRepository _resourceTypeRepository = null!;
    private IHandleRepository _handleRepository = null!;
    private IVisibilityRepository _visibilityRepository = null!;
    private IDisciplineRepository _disciplineRepository = null!;
    private ILicenseRepository _licenseRepository = null!;
    private IRoleRepository _roleRepository = null!;
    private IAuthenticatorService _authenticatorService = null!;
    private IResourceTypeService _resourceTypeService = null!;
    private IQuotaService _quotaService = null!;
    private ILogger<ResourceService> _logger = null!;
    private IAuthorizationService _authorizationService = null!;
    private IDataStorageRepositoryFactory _dataStorageRepositoryFactory = null!;
    private IOptionsMonitor<PidConfiguration> _pidConfiguration = null!;

    private ResourceService _resourceService = null!;
    private IHttpContextAccessor _httpContextAccessorMock = null!;

    [SetUp]
    public void Setup()
    {
        _dbContextOptions = new DbContextOptionsBuilder<RepositoryContext>()
            .UseInMemoryDatabase(Guid.NewGuid().ToString())
            .Options;
        _mapper = Substitute.For<IMapper>();
        _repositoryContextLoader = RepositoryContextLoaderMockFactory.CreatePartialWithTransactionMock(_dbContextOptions);
        _observerManager = ObserverManagerMockFactory.CreatePartial();
        _projectRepository = Substitute.For<IProjectRepository>();
        _resourceRepository = Substitute.For<IResourceRepository>();
        _resourceDisciplineRepository = Substitute.For<IResourceDisciplineRepository>();
        _resourceTypeRepository = Substitute.For<IResourceTypeRepository>();
        _handleRepository = Substitute.For<IHandleRepository>();
        _visibilityRepository = Substitute.For<IVisibilityRepository>();
        _disciplineRepository = Substitute.For<IDisciplineRepository>();
        _licenseRepository = Substitute.For<ILicenseRepository>();
        _roleRepository = Substitute.For<IRoleRepository>();
        _authenticatorService = Substitute.For<IAuthenticatorService>();
        _resourceTypeService = Substitute.For<IResourceTypeService>();
        _quotaService = Substitute.For<IQuotaService>();
        _logger = Substitute.For<ILogger<ResourceService>>();
        _authorizationService = Substitute.For<IAuthorizationService>();
        _dataStorageRepositoryFactory = Substitute.For<IDataStorageRepositoryFactory>();
        _pidConfiguration = Substitute.For<IOptionsMonitor<PidConfiguration>>();
        _pidConfiguration.CurrentValue.Returns(new PidConfiguration()
        {
            Prefix = "12345",
            Url = "https://example.com",
            User = "user",
            Password = "password",
            DigitalObjectLocationUrl = "https://example.com/digitalObjectLocation",
        });
        _httpContextAccessorMock = Substitute.For<IHttpContextAccessor>();
        _httpContextAccessorMock.HttpContext = Substitute.For<HttpContext>();
    }

    [TearDown]
    public void TearDown()
    {
        if (_dbContextOptions != null)
        {
            using var context = new RepositoryContext(_dbContextOptions);
            context.Database.EnsureDeleted();
        }
    }

    #region UpdateResource

    [Test]
    public async Task UpdateResource_WhenNotChangingMaintenanceMode_AndDtoMaintenanceModeIsGiven_AndNotAuthorized_ShouldNotChange()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var ownerRoleId = Guid.NewGuid();

        var principal = new ClaimsPrincipal();
        principal.AddIdentity(new ClaimsIdentity()); // Regular user.
        _httpContextAccessorMock.HttpContext?.User.Returns(new ClaimsPrincipal(principal));

        var resourceEntity = new Resource
        {
            Id = resourceId,
            MaintenanceMode = false,    // Resource is NOT in maintenance mode.
            Archived = false ? "1" : "0",
        };
        var resourceForUpdateDto = new ResourceForUpdateDto
        {
            MaintenanceMode = false,    // Maintenance mode is set in the DTO, but not changing.
            Name = "Resource Name",
            Description = "Resource Description",
            Archived = false,           // Not changing the archived status.
            Visibility = new VisibilityForResourceManipulationDto { Id = Guid.NewGuid() },
            Disciplines = [],
        };

        _resourceRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<Guid>(), Arg.Any<bool>())
            .ReturnsForAnyArgs(Task.FromResult((Resource?)resourceEntity));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.Update) == true)
                {
                    // User is authorized to update the resource.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                if (requirements?.Any(r => r == ResourceOperations.UpdateMaintenanceMode) == true)
                {
                    // User is NOT authorized to update maintenance mode.
                    return Task.FromResult(AuthorizationResult.Failed());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        _visibilityRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(new Visibility());
        _resourceTypeRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(new ResourceType());
        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns(
            [
                Capability.SupportsMaintenanceMode,
                Capability.SupportsReadOnlyMode,
                Capability.UsesQuota
            ]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);
        _quotaService
            .ExtractDesiredResourceQuotaForResource(Arg.Any<ResourceType>(), Arg.Any<ResourceTypeOptionsForCreationDto>())
            .Returns((QuotaForManipulationDto?)null);

        var emailServiceMock = Substitute.For<IEmailService>();
        var emailTemplateManager = Substitute.For<IEmailTemplateManager>();
        var observerManager = ObserverManagerMockFactory.CreatePartial(emailTemplateManager: emailTemplateManager, emailService: emailServiceMock);

        _resourceService = new ResourceService(
            _repositoryContextLoader,
            _projectRepository,
            _resourceRepository,
            _resourceDisciplineRepository,
            _resourceTypeRepository,
            _handleRepository,
            _visibilityRepository,
            _disciplineRepository,
            _licenseRepository,
            _roleRepository,
            _authenticatorService,
            _resourceTypeService,
            _quotaService,
            _mapper,
            _logger,
            _authorizationService,
            observerManager,
            _pidConfiguration,
            null!,
            _dataStorageRepositoryFactory,
            _httpContextAccessorMock
        );

        // Act
        await _resourceService.UpdateResourceForProjectAsync(projectId, resourceId, resourceForUpdateDto, trackChanges: false);

        // Assert
        await _authorizationService
            // The maintenance mode authorization was called.
            .DidNotReceive().AuthorizeAsync(_httpContextAccessorMock.HttpContext?.User!, resourceEntity, Arg.Is<IEnumerable<IAuthorizationRequirement>>(r => r.Any(rr => rr == ResourceOperations.UpdateMaintenanceMode)));

        _dataStorageRepositoryFactory
            // The data storage creation was called.
            .Received().Create(Arg.Any<Guid>());
        await dataStorageMock
            // The data storage update was called, but with no change in maintenance mode.
            .Received().UpdateAsync(Arg.Any<Resource>(), Arg.Is<UpdateDataStorageParameters>(p => p.MaintenanceMode == null));

        await _repositoryContextLoader
            // Changes are saved in the database.
            .Received().SaveAsync();

        emailServiceMock
            // No email is sent since the maintenance mode is not changed.
            .DidNotReceive()
            .Send(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<bool>());

        foreach (var emailTemplate in emailTemplateManager.ResourceMaintenanceModeEnabledTemplates.Values)
        {
            // No email template is parsed since the maintenance mode is not changed.
            emailTemplate.DidNotReceiveWithAnyArgs();
        }
        foreach (var emailTemplate in emailTemplateManager.ResourceMaintenanceModeDisabledTemplates.Values)
        {
            // No email template is parsed since the maintenance mode is not changed.
            emailTemplate.DidNotReceiveWithAnyArgs();
        }
    }

    [Test]
    public async Task UpdateResource_WhenInMaintenanceMode_AndDtoMaintenanceModeIsNull_AndNotAuthorized_ShouldThrowException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var principal = new ClaimsPrincipal();
        _httpContextAccessorMock.HttpContext?.User.Returns(new ClaimsPrincipal(principal)); // User is not authorized.

        var resourceEntity = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true, // Resource is in maintenance mode.
        };
        var resourceForUpdateDto = new ResourceForUpdateDto
        {
            MaintenanceMode = null, // Maintenance mode is NOT set in the DTO.
            Name = "Resource Name",
            Description = "Resource Description",
            Archived = false,
            Visibility = new VisibilityForResourceManipulationDto { Id = Guid.NewGuid() },
            Disciplines = [],
        };

        _resourceRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<Guid>(), Arg.Any<bool>())
            .ReturnsForAnyArgs(Task.FromResult((Resource?)resourceEntity));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.Update) == true)
                {
                    // User is authorized to update the resource.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                if (requirements?.Any(r => r == ResourceOperations.UpdateMaintenanceMode) == true)
                {
                    // User is NOT authorized to update maintenance mode.
                    return Task.FromResult(AuthorizationResult.Failed());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns(
            [
                Capability.SupportsMaintenanceMode,
                Capability.SupportsReadOnlyMode,
                Capability.UsesQuota
            ]);
        _dataStorageRepositoryFactory.Create(Arg.Any<Guid>()).Returns(dataStorageMock);

        var observerManagerMock = ObserverManagerMockFactory.CreatePartial();

        _resourceService = new ResourceService(
            _repositoryContextLoader,
            _projectRepository,
            _resourceRepository,
            _resourceDisciplineRepository,
            _resourceTypeRepository,
            _handleRepository,
            _visibilityRepository,
            _disciplineRepository,
            _licenseRepository,
            _roleRepository,
            _authenticatorService,
            _resourceTypeService,
            _quotaService,
            _mapper,
            _logger,
            _authorizationService,
            _observerManager,
            _pidConfiguration,
            null!,
            _dataStorageRepositoryFactory,
            _httpContextAccessorMock
        );

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
            await _resourceService.UpdateResourceForProjectAsync(projectId, resourceId, resourceForUpdateDto, trackChanges: false));

        await _authorizationService
            // Verify that the maintenance mode authorization was indeed called.
            .Received(1).AuthorizeAsync(_httpContextAccessorMock.HttpContext?.User!, resourceEntity, Arg.Is<IEnumerable<IAuthorizationRequirement>>(r => r.Any(rr => rr == ResourceOperations.UpdateMaintenanceMode)));

        _dataStorageRepositoryFactory
            // Since an exception is thrown early, no call to the data storage creation is made.
            .DidNotReceive().Create(Arg.Any<Guid>());
        await dataStorageMock
            // Since an exception is thrown early, no call to the data storage update is made.
            .DidNotReceive().UpdateAsync(Arg.Any<Resource>(), Arg.Any<UpdateDataStorageParameters>());
    }

    [Test]
    public async Task UpdateResource_WhenInMaintenanceMode_AndDtoMaintenanceModeIsNull_AndAuthorized_ShouldNotChange()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var ownerRoleId = Guid.NewGuid();

        var principal = new ClaimsPrincipal();
        principal.AddIdentity(new ClaimsIdentity([new Claim(ClaimTypes.Role, ApiRoles.Administrator)])); // User is an administrator.
        _httpContextAccessorMock.HttpContext?.User.Returns(new ClaimsPrincipal(principal));

        var projectRoles = new List<ProjectRole>
        {
                new() { RoleId = ownerRoleId, User = new User { Id = Guid.NewGuid(), EmailAddress = "owner@example.com" } },
                new() { RoleId = ownerRoleId, User = new User { Id = Guid.NewGuid(), EmailAddress = "owner2@example.com" } },
                new() { RoleId = Guid.NewGuid(), User = new User { Id = Guid.NewGuid(), EmailAddress = "other@example.com" } }
        };
        var projectEntity = new Project
        {
            Id = projectId,
            DisplayName = "Project Name",
        };
        foreach (var role in projectRoles)
        {
            projectEntity.ProjectRoles.Add(role);
        }
        var expectedRecipients = projectRoles.Where(r => r.RoleId == ownerRoleId).Select(r => r.User.EmailAddress).ToList();

        var resourceEntity = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,     // Resource is in maintenance mode.
            Archived = false ? "1" : "0",
        };
        var resourceForUpdateDto = new ResourceForUpdateDto
        {
            MaintenanceMode = null,     // Maintenance mode is NOT set in the DTO.
            Name = "Resource Name",
            Description = "Resource Description",
            Archived = false,           // Not changing the archived status.
            Visibility = new VisibilityForResourceManipulationDto { Id = Guid.NewGuid() },
            Disciplines = [],
        };

        _resourceRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<Guid>(), Arg.Any<bool>())
            .ReturnsForAnyArgs(Task.FromResult((Resource?)resourceEntity));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.Update) == true)
                {
                    // User is authorized to update the resource.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                if (requirements?.Any(r => r == ResourceOperations.UpdateMaintenanceMode) == true)
                {
                    // User is authorized to update maintenance mode => role is "administrator".
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        _visibilityRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(new Visibility());
        _resourceTypeRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(new ResourceType());
        _projectRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(projectEntity);
        _roleRepository
            .GetOwnerRole()
            .Returns(new Role { Id = ownerRoleId });
        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns(
            [
                Capability.SupportsMaintenanceMode,
                Capability.SupportsReadOnlyMode,
                Capability.UsesQuota
            ]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);
        _quotaService
            .ExtractDesiredResourceQuotaForResource(Arg.Any<ResourceType>(), Arg.Any<ResourceTypeOptionsForCreationDto>())
            .Returns((QuotaForManipulationDto?)null);

        var emailServiceMock = Substitute.For<IEmailService>();
        var emailTemplateManager = new EmailTemplateManager(new EmailTemplateRepository());
        var observerManager = ObserverManagerMockFactory.CreatePartial(emailTemplateManager: emailTemplateManager, emailService: emailServiceMock);

        _resourceService = new ResourceService(
            _repositoryContextLoader,
            _projectRepository,
            _resourceRepository,
            _resourceDisciplineRepository,
            _resourceTypeRepository,
            _handleRepository,
            _visibilityRepository,
            _disciplineRepository,
            _licenseRepository,
            _roleRepository,
            _authenticatorService,
            _resourceTypeService,
            _quotaService,
            _mapper,
            _logger,
            _authorizationService,
            observerManager,
            _pidConfiguration,
            null!,
            _dataStorageRepositoryFactory,
            _httpContextAccessorMock
        );

        // Act
        await _resourceService.UpdateResourceForProjectAsync(projectId, resourceId, resourceForUpdateDto, trackChanges: false);

        // Assert
        await _authorizationService
            // The maintenance mode authorization was called.
            .Received().AuthorizeAsync(_httpContextAccessorMock.HttpContext?.User!, resourceEntity, Arg.Is<IEnumerable<IAuthorizationRequirement>>(r => r.Any(rr => rr == ResourceOperations.UpdateMaintenanceMode)));

        _dataStorageRepositoryFactory
            // The data storage creation was called.
            .Received().Create(Arg.Any<Guid>());
        await dataStorageMock
            // The data storage update was called, but with no change in maintenance mode.
            .Received().UpdateAsync(Arg.Any<Resource>(), Arg.Is<UpdateDataStorageParameters>(p => p.MaintenanceMode == null));

        await _repositoryContextLoader
            // Changes are saved in the database.
            .Received().SaveAsync();

        emailServiceMock
            // No email is sent since the maintenance mode is not changed.
            .DidNotReceive()
            .Send(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<bool>());
    }

    [Test]
    public async Task UpdateResource_WhenInMaintenanceMode_AndDtoArchivedChanges_AndAuthorized_ShouldThrowException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var ownerRoleId = Guid.NewGuid();

        var principal = new ClaimsPrincipal();
        principal.AddIdentity(new ClaimsIdentity([new Claim(ClaimTypes.Role, ApiRoles.Administrator)])); // User is an administrator.
        _httpContextAccessorMock.HttpContext?.User.Returns(new ClaimsPrincipal(principal));

        var projectRoles = new List<ProjectRole>
        {
                new() { RoleId = ownerRoleId, User = new User { Id = Guid.NewGuid(), EmailAddress = "owner@example.com" } },
                new() { RoleId = ownerRoleId, User = new User { Id = Guid.NewGuid(), EmailAddress = "owner2@example.com" } },
                new() { RoleId = Guid.NewGuid(), User = new User { Id = Guid.NewGuid(), EmailAddress = "other@example.com" } }
        };
        var projectEntity = new Project
        {
            Id = projectId,
            DisplayName = "Project Name",
        };
        foreach (var role in projectRoles)
        {
            projectEntity.ProjectRoles.Add(role);
        }
        var expectedRecipients = projectRoles.Where(r => r.RoleId == ownerRoleId).Select(r => r.User.EmailAddress).ToList();

        var resourceEntity = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,         // Resource is in maintenance mode.
            Archived = false ? "1" : "0",   // Resource is NOT archived.
        };
        var resourceForUpdateDto = new ResourceForUpdateDto
        {
            MaintenanceMode = null, // Maintenance mode is NOT set in the DTO.
            Archived = true,        // Try to change the archived status.
            Name = "Resource Name",
            Description = "Resource Description",
            Visibility = new VisibilityForResourceManipulationDto { Id = Guid.NewGuid() },
            Disciplines = [],
        };

        _resourceRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<Guid>(), Arg.Any<bool>())
            .ReturnsForAnyArgs(Task.FromResult((Resource?)resourceEntity));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.Update) == true)
                {
                    // User is authorized to update the resource.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                if (requirements?.Any(r => r == ResourceOperations.UpdateMaintenanceMode) == true)
                {
                    // User is authorized to update maintenance mode => role is "administrator".
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        _visibilityRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(new Visibility());
        _resourceTypeRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(new ResourceType());

        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns(
            [
                Capability.SupportsMaintenanceMode,
                Capability.SupportsReadOnlyMode,
                Capability.UsesQuota
            ]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);

        _resourceService = new ResourceService(
            _repositoryContextLoader,
            _projectRepository,
            _resourceRepository,
            _resourceDisciplineRepository,
            _resourceTypeRepository,
            _handleRepository,
            _visibilityRepository,
            _disciplineRepository,
            _licenseRepository,
            _roleRepository,
            _authenticatorService,
            _resourceTypeService,
            _quotaService,
            _mapper,
            _logger,
            _authorizationService,
            _observerManager,
            _pidConfiguration,
            null!,
            _dataStorageRepositoryFactory,
            _httpContextAccessorMock
        );

        // Act & Assert
        Assert.ThrowsAsync<ResourceInMaintenanceModeForbiddenException>(async () =>
            await _resourceService.UpdateResourceForProjectAsync(projectId, resourceId, resourceForUpdateDto, trackChanges: false));

        await _authorizationService
            // The maintenance mode authorization was called.
            .Received().AuthorizeAsync(_httpContextAccessorMock.HttpContext?.User!, resourceEntity, Arg.Is<IEnumerable<IAuthorizationRequirement>>(r => r.Any(rr => rr == ResourceOperations.UpdateMaintenanceMode)));

        _dataStorageRepositoryFactory
            // The data storage creation was called.
            .Received().Create(Arg.Any<Guid>());
        await dataStorageMock
            // Since an exception is thrown, no call to the data storage update is made.
            .DidNotReceive().UpdateAsync(Arg.Any<Resource>(), Arg.Any<UpdateDataStorageParameters>());
    }

    [Test]
    public async Task UpdateResource_WhenDtoMaintenanceModeChangesFromTrueToFalse_AndAuthorized_ShouldNotifyAndDisable()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var ownerRoleId = Guid.NewGuid();

        var principal = new ClaimsPrincipal();
        principal.AddIdentity(new ClaimsIdentity([new Claim(ClaimTypes.Role, ApiRoles.Administrator)])); // User is an administrator.
        _httpContextAccessorMock.HttpContext?.User.Returns(new ClaimsPrincipal(principal));

        var projectRoles = new List<ProjectRole>
        {
                new() { RoleId = ownerRoleId, User = new User { Id = Guid.NewGuid(), Language = new() { Abbreviation = "en" }, EmailAddress = "owner@example.com" } },
                new() { RoleId = ownerRoleId, User = new User { Id = Guid.NewGuid(), Language = new() { Abbreviation = "en" }, EmailAddress = "owner2@example.com" } },
                new() { RoleId = Guid.NewGuid(), User = new User { Id = Guid.NewGuid(), Language = new() { Abbreviation = "en" }, EmailAddress = "other@example.com" } }
        };
        var projectEntity = new Project
        {
            Id = projectId,
            DisplayName = "Project Name",
        };
        foreach (var role in projectRoles)
        {
            projectEntity.ProjectRoles.Add(role);
        }
        var expectedRecipients = projectRoles.Where(r => r.RoleId == ownerRoleId).Select(r => r.User.EmailAddress).ToList();

        var resourceEntity = new Resource
        {
            Id = resourceId,
            MaintenanceMode = true,     // Resource is in maintenance mode.
            Archived = false ? "1" : "0",
        };
        var resourceForUpdateDto = new ResourceForUpdateDto
        {
            MaintenanceMode = false,    // Disable maintenance mode.
            Name = "Resource Name",
            Description = "Resource Description",
            Archived = false,
            Visibility = new VisibilityForResourceManipulationDto { Id = Guid.NewGuid() },
            Disciplines = [],
        };

        _resourceRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<Guid>(), Arg.Any<bool>())
            .ReturnsForAnyArgs(Task.FromResult((Resource?)resourceEntity));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.Update) == true)
                {
                    // User is authorized to update the resource.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                if (requirements?.Any(r => r == ResourceOperations.UpdateMaintenanceMode) == true)
                {
                    // User is authorized to update maintenance mode => role is "administrator".
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        _visibilityRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(new Visibility());
        _resourceTypeRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(new ResourceType());
        _projectRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(projectEntity);
        _roleRepository
            .GetOwnerRole()
            .Returns(new Role { Id = ownerRoleId });
        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns(
            [
                Capability.SupportsMaintenanceMode,
                Capability.SupportsReadOnlyMode,
                Capability.UsesQuota
            ]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);
        _quotaService
            .ExtractDesiredResourceQuotaForResource(Arg.Any<ResourceType>(), Arg.Any<ResourceTypeOptionsForCreationDto>())
            .Returns((QuotaForManipulationDto?)null);

        var emailServiceMock = Substitute.For<IEmailService>();
        var observerManager = ObserverManagerMockFactory.CreatePartial(emailService: emailServiceMock);

        _resourceService = new ResourceService(
            _repositoryContextLoader,
            _projectRepository,
            _resourceRepository,
            _resourceDisciplineRepository,
            _resourceTypeRepository,
            _handleRepository,
            _visibilityRepository,
            _disciplineRepository,
            _licenseRepository,
            _roleRepository,
            _authenticatorService,
            _resourceTypeService,
            _quotaService,
            _mapper,
            _logger,
            _authorizationService,
            observerManager,
            _pidConfiguration,
            null!,
            _dataStorageRepositoryFactory,
            _httpContextAccessorMock
        );

        // Act
        await _resourceService.UpdateResourceForProjectAsync(projectId, resourceId, resourceForUpdateDto, trackChanges: false);

        // Assert
        await _authorizationService
            // The maintenance mode authorization was called.
            .Received().AuthorizeAsync(_httpContextAccessorMock.HttpContext?.User!, resourceEntity, Arg.Is<IEnumerable<IAuthorizationRequirement>>(r => r.Any(rr => rr == ResourceOperations.UpdateMaintenanceMode)));

        _dataStorageRepositoryFactory
            // The data storage creation was called.
            .Received().Create(Arg.Any<Guid>());
        await dataStorageMock
            // The data storage update was called, maintenance mode is disabled.
            .Received().UpdateAsync(Arg.Any<Resource>(), Arg.Is<UpdateDataStorageParameters>(p => p.MaintenanceMode == false));

        await _repositoryContextLoader
            // Changes are saved in the database.
            .Received().SaveAsync();

        emailServiceMock
            // The email service was called to send the email to the expected recipients and the correct template was used.
            .Received(expectedRecipients.Count)
            .Send(Arg.Is<string>(m => expectedRecipients.Contains(m)),
                Arg.Is<string>(subject => subject.Contains("Maintenance Mode Deactivated")),
                Arg.Any<string>(), Arg.Any<bool>());
    }

    [Test]
    public async Task UpdateResource_WhenDtoMaintenanceModeChangesFromFalseToTrue_AndAuthorized_ShouldNotifyAndEnable()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var ownerRoleId = Guid.NewGuid();

        var principal = new ClaimsPrincipal();
        principal.AddIdentity(new ClaimsIdentity([new Claim(ClaimTypes.Role, ApiRoles.Administrator)])); // User is an administrator.
        _httpContextAccessorMock.HttpContext?.User.Returns(new ClaimsPrincipal(principal));

        var projectRoles = new List<ProjectRole>
        {
                new() { RoleId = ownerRoleId, User = new User { Id = Guid.NewGuid(), Language = new() { Abbreviation = "en" }, EmailAddress = "owner@example.com" } },
                new() { RoleId = ownerRoleId, User = new User { Id = Guid.NewGuid(), Language = new() { Abbreviation = "en" }, EmailAddress = "owner2@example.com" } },
                new() { RoleId = Guid.NewGuid(), User = new User { Id = Guid.NewGuid(), Language = new() { Abbreviation = "en" }, EmailAddress = "other@example.com" } }
        };
        var projectEntity = new Project
        {
            Id = projectId,
            DisplayName = "Project Name",
        };
        foreach (var role in projectRoles)
        {
            projectEntity.ProjectRoles.Add(role);
        }
        var expectedRecipients = projectRoles.Where(r => r.RoleId == ownerRoleId).Select(r => r.User.EmailAddress).ToList();

        var resourceEntity = new Resource
        {
            Id = resourceId,
            MaintenanceMode = false,    // Resource is NOT in maintenance mode.
            Archived = false ? "1" : "0",
        };
        var resourceForUpdateDto = new ResourceForUpdateDto
        {
            MaintenanceMode = true,     // Enable maintenance mode.
            Name = "Resource Name",
            Description = "Resource Description",
            Archived = false,
            Visibility = new VisibilityForResourceManipulationDto { Id = Guid.NewGuid() },
            Disciplines = [],
        };

        _resourceRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<Guid>(), Arg.Any<bool>())
            .ReturnsForAnyArgs(Task.FromResult((Resource?)resourceEntity));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.Update) == true)
                {
                    // User is authorized to update the resource.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                if (requirements?.Any(r => r == ResourceOperations.UpdateMaintenanceMode) == true)
                {
                    // User is authorized to update maintenance mode => role is "administrator".
                    return Task.FromResult(AuthorizationResult.Success());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        _visibilityRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(new Visibility());
        _resourceTypeRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(new ResourceType());
        _projectRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(projectEntity);
        _roleRepository
            .GetOwnerRole()
            .Returns(new Role { Id = ownerRoleId });
        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns(
            [
                Capability.SupportsMaintenanceMode,
                Capability.SupportsReadOnlyMode,
                Capability.UsesQuota
            ]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);
        _quotaService
            .ExtractDesiredResourceQuotaForResource(Arg.Any<ResourceType>(), Arg.Any<ResourceTypeOptionsForCreationDto>())
            .Returns((QuotaForManipulationDto?)null);

        var emailServiceMock = Substitute.For<IEmailService>();
        var observerManager = ObserverManagerMockFactory.CreatePartial(emailService: emailServiceMock);

        _resourceService = new ResourceService(
            _repositoryContextLoader,
            _projectRepository,
            _resourceRepository,
            _resourceDisciplineRepository,
            _resourceTypeRepository,
            _handleRepository,
            _visibilityRepository,
            _disciplineRepository,
            _licenseRepository,
            _roleRepository,
            _authenticatorService,
            _resourceTypeService,
            _quotaService,
            _mapper,
            _logger,
            _authorizationService,
            observerManager,
            _pidConfiguration,
            null!,
            _dataStorageRepositoryFactory,
            _httpContextAccessorMock
        );

        // Act
        await _resourceService.UpdateResourceForProjectAsync(projectId, resourceId, resourceForUpdateDto, trackChanges: false);

        // Assert
        await _authorizationService
            // The maintenance mode authorization was called.
            .Received().AuthorizeAsync(_httpContextAccessorMock.HttpContext?.User!, resourceEntity, Arg.Is<IEnumerable<IAuthorizationRequirement>>(r => r.Any(rr => rr == ResourceOperations.UpdateMaintenanceMode)));

        _dataStorageRepositoryFactory
            // The data storage creation was called.
            .Received().Create(Arg.Any<Guid>());
        await dataStorageMock
            // The data storage update was called, maintenance mode is enabled.
            .Received().UpdateAsync(Arg.Any<Resource>(), Arg.Is<UpdateDataStorageParameters>(p => p.MaintenanceMode == true));

        await _repositoryContextLoader
            // Changes are saved in the database.
            .Received().SaveAsync();

        emailServiceMock
            // The email service was called to send the email to the expected recipients and the correct template was used.
            .Received(expectedRecipients.Count)
            .Send(Arg.Is<string>(m => expectedRecipients.Contains(m)),
                Arg.Is<string>(subject => subject.Contains("Maintenance Mode Activated")),
                Arg.Any<string>(), Arg.Any<bool>());
    }

    [Test]
    public async Task UpdateResource_WhenDtoMaintenanceModeChangesFromFalseToTrue_AndNotAuthorized_ShouldThrowException()
    {
        // Arrange
        var projectId = Guid.NewGuid();
        var resourceId = Guid.NewGuid();
        var ownerRoleId = Guid.NewGuid();

        var principal = new ClaimsPrincipal();
        principal.AddIdentity(new ClaimsIdentity([new Claim(ClaimTypes.Role, ApiRoles.Administrator)])); // User is an administrator.
        _httpContextAccessorMock.HttpContext?.User.Returns(new ClaimsPrincipal(principal));

        var projectRoles = new List<ProjectRole>
        {
                new() { RoleId = ownerRoleId, User = new User { Id = Guid.NewGuid(), EmailAddress = "owner@example.com" } },
                new() { RoleId = ownerRoleId, User = new User { Id = Guid.NewGuid(), EmailAddress = "owner2@example.com" } },
                new() { RoleId = Guid.NewGuid(), User = new User { Id = Guid.NewGuid(), EmailAddress = "other@example.com" } }
        };
        var projectEntity = new Project
        {
            Id = projectId,
            DisplayName = "Project Name",
        };
        foreach (var role in projectRoles)
        {
            projectEntity.ProjectRoles.Add(role);
        }
        var expectedRecipients = projectRoles.Where(r => r.RoleId == ownerRoleId).Select(r => r.User.EmailAddress).ToList();

        var resourceEntity = new Resource
        {
            Id = resourceId,
            MaintenanceMode = false,    // Resource is NOT in maintenance mode.
            Archived = false ? "1" : "0",
        };
        var resourceForUpdateDto = new ResourceForUpdateDto
        {
            MaintenanceMode = true,     // Enable maintenance mode.
            Name = "Resource Name",
            Description = "Resource Description",
            Archived = false,
            Visibility = new VisibilityForResourceManipulationDto { Id = Guid.NewGuid() },
            Disciplines = [],
        };

        _resourceRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<Guid>(), Arg.Any<bool>())
            .ReturnsForAnyArgs(Task.FromResult((Resource?)resourceEntity));

        _authorizationService
            .AuthorizeAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<Resource>(), Arg.Any<IEnumerable<IAuthorizationRequirement>>())
            .Returns(callInfo =>
            {
                var requirements = callInfo.Arg<IEnumerable<IAuthorizationRequirement>>();

                if (requirements?.Any(r => r == ResourceOperations.Update) == true)
                {
                    // User is authorized to update the resource.
                    return Task.FromResult(AuthorizationResult.Success());
                }

                if (requirements?.Any(r => r == ResourceOperations.UpdateMaintenanceMode) == true)
                {
                    // User is NOT authorized to update maintenance mode.
                    return Task.FromResult(AuthorizationResult.Failed());
                }

                return Task.FromResult(AuthorizationResult.Failed());
            });

        _visibilityRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(new Visibility());
        _resourceTypeRepository
            .GetAsync(Arg.Any<Guid>(), Arg.Any<bool>())
            .Returns(new ResourceType());
        var dataStorageMock = Substitute.For<IDataStorageRepository>(); // Setup a fake data storage that supports maintenance mode.
        dataStorageMock.Capabilities.Returns(
            [
                Capability.SupportsMaintenanceMode,
                Capability.SupportsReadOnlyMode,
                Capability.UsesQuota
            ]);
        _dataStorageRepositoryFactory
            .Create(Arg.Any<Guid>())
            .Returns(dataStorageMock);
        _quotaService
            .ExtractDesiredResourceQuotaForResource(Arg.Any<ResourceType>(), Arg.Any<ResourceTypeOptionsForCreationDto>())
            .Returns((QuotaForManipulationDto?)null);

        var emailServiceMock = Substitute.For<IEmailService>();
        var emailTemplateManager = new EmailTemplateManager(new EmailTemplateRepository());
        var observerManager = ObserverManagerMockFactory.CreatePartial(emailTemplateManager: emailTemplateManager, emailService: emailServiceMock);

        _resourceService = new ResourceService(
            _repositoryContextLoader,
            _projectRepository,
            _resourceRepository,
            _resourceDisciplineRepository,
            _resourceTypeRepository,
            _handleRepository,
            _visibilityRepository,
            _disciplineRepository,
            _licenseRepository,
            _roleRepository,
            _authenticatorService,
            _resourceTypeService,
            _quotaService,
            _mapper,
            _logger,
            _authorizationService,
            observerManager,
            _pidConfiguration,
            null!,
            _dataStorageRepositoryFactory,
            _httpContextAccessorMock
        );

        // Act & Assert
        Assert.ThrowsAsync<ResourceForbiddenException>(async () =>
            await _resourceService.UpdateResourceForProjectAsync(projectId, resourceId, resourceForUpdateDto, trackChanges: false));

        await _authorizationService
            // The maintenance mode authorization was called.
            .Received().AuthorizeAsync(_httpContextAccessorMock.HttpContext?.User!, resourceEntity, Arg.Is<IEnumerable<IAuthorizationRequirement>>(r => r.Any(rr => rr == ResourceOperations.UpdateMaintenanceMode)));

        _dataStorageRepositoryFactory
            // The data storage creation was called (capabilites are checked).
            .Received().Create(Arg.Any<Guid>());
        await dataStorageMock
            // The data storage update was never called.
            .DidNotReceive().UpdateAsync(Arg.Any<Resource>(), Arg.Is<UpdateDataStorageParameters>(p => p.MaintenanceMode == true));

        await _repositoryContextLoader
            // No changes are saved in the database.
            .DidNotReceive().SaveAsync();

        emailServiceMock
            // The email service was not called.
            .DidNotReceive()
            .Send(Arg.Any<string>(), Arg.Any<string>(), Arg.Any<string>(), Arg.Any<bool>());
    }

    #endregion
}
