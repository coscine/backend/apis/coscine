﻿using Coscine.Api.Core.Repository.Extensions.Utility;
using NUnit.Framework;
using VDS.RDF;

namespace Coscine.Api.Tests.UnitTests;

[TestFixture]
public class TriplesChunkingTests
{
    /// <summary>
    /// Tests chunking of a simple list of grouped triples containing blank nodes.
    /// The blank node groups should not be split across chunks, as they are dependent on each other.
    /// </summary>
    [Test]
    public void ChunkGrouped_SimpleCase()
    {
        var groupedTriples = new List<IEnumerable<Triple>>
        {
            new List<Triple>
            {
                new(new BlankNode("a"), new UriNode(new Uri("http://example.org/p1")), new LiteralNode("v1")),
                new(new BlankNode("b"), new UriNode(new Uri("http://example.org/p2")), new LiteralNode("v2"))
            },
            new List<Triple>
            {
                new(new BlankNode("c"), new UriNode(new Uri("http://example.org/p3")), new LiteralNode("v3")),
                new(new BlankNode("d"), new UriNode(new Uri("http://example.org/p4")), new LiteralNode("v4")),
                new(new BlankNode("e"), new UriNode(new Uri("http://example.org/p5")), new LiteralNode("v5"))
            }
        };

        var chunks = groupedTriples.ChunkGrouped(2).ToList();

        Assert.That(chunks.Count, Is.EqualTo(2));
        Assert.That(chunks[0].Count, Is.EqualTo(2));
        Assert.That(chunks[1].Count, Is.EqualTo(3));
    }

    /// <summary>
    /// Tests chunking of a large group of triples. 
    /// All triples are ground, thus they should be split into chunks of the specified size.
    /// </summary>
    [Test]
    public void ChunkGrouped_LargeGroup()
    {
        const int numTriples = 100000;
        const int chunkSize = 99;
        var expectedChunks = numTriples / chunkSize + 1;
        var expectedLastChunkSize = numTriples % chunkSize;
        var triples = new List<Triple>();
        for (int i = 0; i < numTriples; i++)
        {
            triples.Add(new Triple(new UriNode(new Uri($"http://example.org/s{i}")), new UriNode(new Uri("http://example.org/p")), new LiteralNode($"v{i}")));
        }

        var groupedTriples = new List<IEnumerable<Triple>> { triples };

        var chunks = groupedTriples.ChunkGrouped(chunkSize).ToList();

        Assert.That(triples.All(t => t.IsGroundTriple));
        Assert.That(chunks.Count, Is.EqualTo(expectedChunks));
        for (int i = 0; i < expectedChunks - 1; i++)
        {
            Assert.That(chunks[i].Count, Is.EqualTo(chunkSize));
        }
        Assert.That(chunks.Last().Count, Is.EqualTo(expectedLastChunkSize));
    }

    /// <summary>
    /// Tests chunking of grounded triples into pieces.
    /// </summary>
    [Test]
    public void ChunkGrouped_GroundedTriples()
    {
        var triples = new List<Triple>();
        for (int i = 0; i < 20; i++)
        {
            triples.Add(new Triple(new UriNode(new Uri($"http://example.org/s{i}")), new UriNode(new Uri("http://example.org/p")), new LiteralNode($"v{i}")));
        }

        var groupedTriples = new List<IEnumerable<Triple>> { triples };

        var chunks = groupedTriples.ChunkGrouped(5).ToList();

        Assert.That(chunks.Count, Is.EqualTo(4));
        foreach (var chunk in chunks)
        {
            Assert.That(chunk.Count, Is.LessThanOrEqualTo(5));
        }
    }

    /// <summary>
    /// Tests chunking of an empty collection.
    /// </summary>
    [Test]
    public void ChunkGrouped_EmptyCollection()
    {
        var groupedTriples = new List<IEnumerable<Triple>>();

        var chunks = groupedTriples.ChunkGrouped(2).ToList();

        Assert.That(chunks.Count, Is.EqualTo(0));
    }

    /// <summary>
    /// Tests chunking where a single group exceeds the chunk size.
    /// As the group contains blank nodes, it should not be split across chunks.
    /// </summary>
    [Test]
    public void ChunkGrouped_SingleGroupExceedsChunkSize()
    {
        var triples = new List<Triple>
        {
            new(new BlankNode("a"), new UriNode(new Uri("http://example.org/p1")), new LiteralNode("v1")),
            new(new BlankNode("b"), new UriNode(new Uri("http://example.org/p2")), new LiteralNode("v2")),
            new(new BlankNode("c"), new UriNode(new Uri("http://example.org/p3")), new LiteralNode("v3")),
            new(new BlankNode("d"), new UriNode(new Uri("http://example.org/p4")), new LiteralNode("v4")),
            new(new BlankNode("e"), new UriNode(new Uri("http://example.org/p5")), new LiteralNode("v5")),
            new(new BlankNode("f"), new UriNode(new Uri("http://example.org/p6")), new LiteralNode("v6"))
        };

        var groupedTriples = new List<IEnumerable<Triple>> { triples };

        var chunks = groupedTriples.ChunkGrouped(2).ToList();

        Assert.That(chunks.Count, Is.EqualTo(2));
        Assert.That(chunks[0].Count, Is.EqualTo(0));
        Assert.That(chunks[1].Count, Is.EqualTo(6));
    }

    /// <summary>
    /// Tests chunking of a collection with mixed groups of literal and blank nodes.
    /// </summary>
    [Test]
    public void ChunkGrouped_MixedGroups()
    {
        var groupedTriples = new List<IEnumerable<Triple>>
        {
            new List<Triple>
            {
                new(new BlankNode("a"), new UriNode(new Uri("http://example.org/p1")), new LiteralNode("v1")),
                new(new BlankNode("b"), new UriNode(new Uri("http://example.org/p2")), new LiteralNode("v2"))
            },
            new List<Triple>
            {
                new(new LiteralNode("l1"), new UriNode(new Uri("http://example.org/p3")), new LiteralNode("v3")),
                new(new LiteralNode("l2"), new UriNode(new Uri("http://example.org/p4")), new LiteralNode("v4")),
                new(new LiteralNode("l3"), new UriNode(new Uri("http://example.org/p5")), new LiteralNode("v5"))
            },
            new List<Triple>
            {
                new(new BlankNode("f"), new UriNode(new Uri("http://example.org/p6")), new LiteralNode("v6")),
                new(new BlankNode("g"), new UriNode(new Uri("http://example.org/p7")), new LiteralNode("v7")),
                new(new BlankNode("h"), new UriNode(new Uri("http://example.org/p8")), new LiteralNode("v8")),
                new(new BlankNode("i"), new UriNode(new Uri("http://example.org/p9")), new LiteralNode("v9")),
                new(new BlankNode("j"), new UriNode(new Uri("http://example.org/p10")), new LiteralNode("v10"))
            }
        };

        var chunks = groupedTriples.ChunkGrouped(2).ToList();

        Assert.That(chunks.Count, Is.EqualTo(4));
        Assert.That(chunks[0].Count, Is.EqualTo(2));
        Assert.That(chunks[1].Count, Is.EqualTo(1));
        Assert.That(chunks[2].Count, Is.EqualTo(groupedTriples[0].Count()));
        Assert.That(chunks[3].Count, Is.EqualTo(groupedTriples[2].Count()));
    }
}
