using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Shared.RequestFeatures;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Coscine.Api.Tests.UnitTests;

[TestFixture]
public class ApiTokenRepositoryTests
{
    private DbContextOptions<RepositoryContext>? _dbContextOptions;

    [SetUp]
    public void Setup()
    {
        _dbContextOptions = new DbContextOptionsBuilder<RepositoryContext>()
            .UseInMemoryDatabase(Guid.NewGuid().ToString())
            .Options;
    }

    [TearDown]
    public void TearDown()
    {
        if (_dbContextOptions != null)
        {
            using var context = new RepositoryContext(_dbContextOptions);
            context.Database.EnsureDeleted();
        }
    }

    [Test]
    public async Task GetPagedAsync_ReturnsCorrectResults()
    {
        // Arrange
        using var context = new RepositoryContext(_dbContextOptions!);
        var repository = new ApiTokenRepository(context);

        var userId = Guid.NewGuid();

        var tokens = new List<ApiToken>
        {
            new() { Id = Guid.NewGuid(), Name = "Token1", UserId = userId, IssuedAt = DateTime.Now.AddDays(-3), Expiration = DateTime.Now.AddDays(10) },
            new() { Id = Guid.NewGuid(), Name = "Token2", UserId = userId, IssuedAt = DateTime.Now.AddDays(-2), Expiration = DateTime.Now.AddDays(10) },
            new() { Id = Guid.NewGuid(), Name = "Token3", UserId = userId, IssuedAt = DateTime.Now.AddDays(-1), Expiration = DateTime.Now.AddDays(10) }
        };

        context.Set<ApiToken>().AddRange(tokens);
        await context.SaveChangesAsync();

        var parameters = new ApiTokenParameters
        {
            PageNumber = 1,
            PageSize = 2,
            OrderBy = "IssuedAt desc"
        };

        // Act
        var result = await repository.GetPagedAsync(userId, parameters, false);

        // Assert
        Assert.That(result.Pagination.PageSize, Is.EqualTo(2), "Expected the page size to match the requested value.");
        Assert.That(result.Pagination.TotalCount, Is.EqualTo(3), "Expected the total count to reflect all matching tokens.");
        Assert.That(result.Count(), Is.EqualTo(2), "Expected the number of items returned to match the page size.");
        Assert.That(result.First().Name, Is.EqualTo("Token3"), "Expected the first token to be the most recent.");
    }

    [Test]
    public async Task GetAsync_ByTokenId_ReturnsCorrectToken()
    {
        // Arrange
        using var context = new RepositoryContext(_dbContextOptions!);
        var repository = new ApiTokenRepository(context);

        var token = new ApiToken
        {
            Id = Guid.NewGuid(),
            Name = "TestToken",
            UserId = Guid.NewGuid(),
            IssuedAt = DateTime.Now,
            Expiration = DateTime.Now.AddDays(10)
        };

        context.Set<ApiToken>().Add(token);
        await context.SaveChangesAsync();

        // Act
        var result = await repository.GetAsync(token.Id, false);

        // Assert
        Assert.That(result, Is.Not.Null, "Expected the token to be found.");
        Assert.That(result!.Name, Is.EqualTo("TestToken"), "Expected the token name to match.");
    }

    [Test]
    public async Task Delete_RemovesTokenSuccessfully()
    {
        // Arrange
        using var context = new RepositoryContext(_dbContextOptions!);
        var repository = new ApiTokenRepository(context);

        var token = new ApiToken
        {
            Id = Guid.NewGuid(),
            Name = "TokenToDelete",
            UserId = Guid.NewGuid(),
            IssuedAt = DateTime.Now,
            Expiration = DateTime.Now.AddDays(10)
        };

        context.Set<ApiToken>().Add(token);
        await context.SaveChangesAsync();

        // Act
        repository.Delete(token);
        await context.SaveChangesAsync();

        // Assert
        var remainingTokens = context.Set<ApiToken>().ToList();
        Assert.That(remainingTokens, Is.Empty, "Expected all tokens to be deleted.");
    }

    [Test]
    public async Task MergeAsync_RemovesTokensForUserId()
    {
        // Arrange
        using var context = new RepositoryContext(_dbContextOptions!);
        var repository = new ApiTokenRepository(context);

        var fromUserId = Guid.NewGuid();

        var tokens = new List<ApiToken>
        {
            new() { Id = Guid.NewGuid(), Name = "Token1", UserId = fromUserId, IssuedAt = DateTime.Now, Expiration = DateTime.Now.AddDays(10) },
            new() { Id = Guid.NewGuid(), Name = "Token2", UserId = fromUserId, IssuedAt = DateTime.Now, Expiration = DateTime.Now.AddDays(10) },
            new() { Id = Guid.NewGuid(), Name = "Token3", UserId = Guid.NewGuid(), IssuedAt = DateTime.Now, Expiration = DateTime.Now.AddDays(10) }
        };

        context.Set<ApiToken>().AddRange(tokens);
        await context.SaveChangesAsync();

        // Act
        await repository.MergeAsync(fromUserId, Guid.NewGuid());
        await context.SaveChangesAsync();

        // Assert
        var remainingTokens = context.Set<ApiToken>().Where(t => t.UserId == fromUserId).ToList();
        Assert.That(remainingTokens, Is.Empty, "Expected all tokens for the source user to be deleted.");
    }
}