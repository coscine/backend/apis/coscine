using System.Security.Claims;
using Coscine.Api.Authorization.AuthHandlers;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using NUnit.Framework;

namespace Coscine.Api.Tests.UnitTests.AuthHandlerTests;

[TestFixture]
public class HandleAuthHandlerTests
{
    private TestableHandleAuthHandler _authHandler = null!;

    [SetUp]
    public void SetUp()
    {
        _authHandler = new TestableHandleAuthHandler();
    }

    [Test]
    public async Task HandleRequirementAsync_PublicReadOperation_ShouldSucceed()
    {
        // Arrange
        var requirement = HandleOperations.Read;
        var resource = new Handle();
        var user = new ClaimsPrincipal();
        var context = new AuthorizationHandlerContext([requirement], user, resource);

        // Act
        await _authHandler.HandleRequirementAsync(context, requirement, resource);

        // Assert
        Assert.That(context.HasSucceeded, Is.True, "Public read operation should always succeed.");
    }

    [Test]
    public async Task HandleRequirementAsync_UserIsAdmin_ShouldSucceed()
    {
        // Arrange
        var requirement = new OperationAuthorizationRequirement { Name = "Write" };
        var resource = new Handle();
        var user = new ClaimsPrincipal(new ClaimsIdentity([new Claim(ClaimTypes.Role, ApiRoles.Administrator)]));
        var context = new AuthorizationHandlerContext([requirement], user, resource);

        // Act
        await _authHandler.HandleRequirementAsync(context, requirement, resource);

        // Assert
        Assert.That(context.HasSucceeded, Is.True, "Authorization should succeed for administrators.");
    }

    [Test]
    public async Task HandleRequirementAsync_UserIsNotAdminAndNotRead_ShouldNotSucceed()
    {
        // Arrange
        var requirement = new OperationAuthorizationRequirement { Name = "Write" };
        var resource = new Handle();
        var user = new ClaimsPrincipal(new ClaimsIdentity());
        var context = new AuthorizationHandlerContext([requirement], user, resource);

        // Act
        await _authHandler.HandleRequirementAsync(context, requirement, resource);

        // Assert
        Assert.That(context.HasSucceeded, Is.False, "Authorization should not succeed for non-admin users on non-read operations.");
    }

    [Test]
    public async Task HandleRequirementAsync_NullUser_ShouldNotSucceed()
    {
        // Arrange
        var requirement = new OperationAuthorizationRequirement { Name = "Write" };
        var resource = new Handle();
        var context = new AuthorizationHandlerContext([requirement], null!, resource);

        // Act
        await _authHandler.HandleRequirementAsync(context, requirement, resource);

        // Assert
        Assert.That(context.HasSucceeded, Is.False, "Authorization should not succeed when the user is null.");
    }

    [TestCase(nameof(HandleOperations.Create), false)]
    [TestCase(nameof(HandleOperations.Read), true)]
    [TestCase(nameof(HandleOperations.Update), false)]
    [TestCase(nameof(HandleOperations.Delete), false)]
    public async Task HandleRequirementAsync_UserRoleTests(string operation, bool expectedResult)
    {
        // Arrange
        var requirement = new OperationAuthorizationRequirement { Name = operation };
        var resource = new Handle();
        var user = new ClaimsPrincipal(new ClaimsIdentity());
        var context = new AuthorizationHandlerContext([requirement], user, resource);

        // Act
        await _authHandler.HandleRequirementAsync(context, requirement, resource);

        // Assert
        Assert.That(context.HasSucceeded, Is.EqualTo(expectedResult), $"Authorization should {(expectedResult ? "succeed" : "fail")} for operation {operation}.");
    }

    public class TestableHandleAuthHandler() : HandleAuthHandler()
    {
        public new Task HandleRequirementAsync(AuthorizationHandlerContext context, OperationAuthorizationRequirement requirement, Handle handle)
        {
            return base.HandleRequirementAsync(context, requirement, handle);
        }
    }
}
