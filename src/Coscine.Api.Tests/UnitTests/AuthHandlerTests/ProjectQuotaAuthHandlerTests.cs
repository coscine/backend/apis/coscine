using System.Security.Claims;
using Coscine.Api.Authorization.AuthHandlers;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;

namespace Coscine.Api.Tests.UnitTests.AuthHandlerTests;

[TestFixture]
public class ProjectQuotaAuthHandlerTests
{
    private ILogger<ProjectQuotaAuthHandler> _logger = null!;
    private IRoleRepository _roleRepository = null!;
    private IProjectRoleRepository _projectRoleRepository = null!;
    private IAuthenticatorService _authenticatorService = null!;
    private TestableProjectQuotaAuthHandler _authHandler = null!;

    [SetUp]
    public void SetUp()
    {
        _logger = Substitute.For<ILogger<ProjectQuotaAuthHandler>>();
        _roleRepository = Substitute.For<IRoleRepository>();
        _projectRoleRepository = Substitute.For<IProjectRoleRepository>();
        _authenticatorService = Substitute.For<IAuthenticatorService>();
        _authHandler = new TestableProjectQuotaAuthHandler(_logger, _roleRepository, _projectRoleRepository, _authenticatorService);
    }

    [Test]
    public async Task HandleRequirementAsync_UserIsAdmin_ShouldSucceed()
    {
        // Arrange
        var requirement = new OperationAuthorizationRequirement { Name = nameof(ProjectQuotaOperations.Read) };
        var resource = new ProjectQuota { RelationId = Guid.NewGuid(), ProjectId = Guid.NewGuid() };
        var user = new ClaimsPrincipal(new ClaimsIdentity([new Claim(ClaimTypes.Role, ApiRoles.Administrator)]));
        var context = new AuthorizationHandlerContext([requirement], user, resource);

        // Act
        await _authHandler.HandleRequirementAsync(context, requirement, resource);

        // Assert
        Assert.That(context.HasSucceeded, Is.True, "Authorization should succeed for administrators.");
    }

    [Test]
    public async Task HandleRequirementAsync_UserIsNull_ShouldNotSucceed()
    {
        // Arrange
        var requirement = new OperationAuthorizationRequirement { Name = nameof(ProjectQuotaOperations.Read) };
        var resource = new ProjectQuota { RelationId = Guid.NewGuid(), ProjectId = Guid.NewGuid() };
        var user = new ClaimsPrincipal();
        var context = new AuthorizationHandlerContext([requirement], user, resource);

        _authenticatorService.GetUserAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<bool>()).Returns((User?)null);

        // Act
        await _authHandler.HandleRequirementAsync(context, requirement, resource);

        // Assert
        Assert.That(context.HasSucceeded, Is.False, "Authorization should not succeed if the user is null.");
    }

    [TestCase(nameof(ProjectQuotaOperations.Read), true, "BE294C5E-4E42-49B3-BEC4-4B15F49DF9A5")] // Owner
    [TestCase(nameof(ProjectQuotaOperations.Read), true, "508B6D4E-C6AC-4AA5-8A8D-CAA31DD39527")] // Member
    [TestCase(nameof(ProjectQuotaOperations.Update), true, "BE294C5E-4E42-49B3-BEC4-4B15F49DF9A5")] // Owner
    [TestCase(nameof(ProjectQuotaOperations.Update), false, "508B6D4E-C6AC-4AA5-8A8D-CAA31DD39527")] // Member
    public async Task HandleRequirementAsync_UserRoleAuthorizationTests(string operation, bool expectedResult, string roleId)
    {
        // Arrange
        var requirement = new OperationAuthorizationRequirement { Name = operation };
        var resource = new ProjectQuota { RelationId = Guid.NewGuid(), ProjectId = Guid.NewGuid() };
        var user = new ClaimsPrincipal();
        var context = new AuthorizationHandlerContext([requirement], user, resource);
        var mockUser = new User { Id = Guid.NewGuid() };

        _authenticatorService.GetUserAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<bool>()).Returns(mockUser);

        if (expectedResult)
        {
            _projectRoleRepository.GetAllByProjectAsync(resource.ProjectId, mockUser.Id, Arg.Any<bool>())
                .Returns([new ProjectRole { RoleId = Guid.Parse(roleId) }]);
        }
        else
        {
            _projectRoleRepository.GetAllByProjectAsync(resource.ProjectId, mockUser.Id, Arg.Any<bool>()).Returns(Array.Empty<ProjectRole>());
        }

        // Act
        await _authHandler.HandleRequirementAsync(context, requirement, resource);

        // Assert
        Assert.That(context.HasSucceeded, Is.EqualTo(expectedResult), $"Authorization should {(expectedResult ? "succeed" : "fail")} for operation {operation} with role {roleId}.");
    }

    [Test]
    public async Task HandleRequirementAsync_UserIsQuotaAdmin_ShouldSucceedForReadAndUpdate()
    {
        // Arrange
        var requirement = new OperationAuthorizationRequirement { Name = nameof(ProjectQuotaOperations.Read) };
        var resource = new ProjectQuota { RelationId = Guid.NewGuid(), ProjectId = Guid.NewGuid() };
        var user = new ClaimsPrincipal();
        var context = new AuthorizationHandlerContext([requirement], user, resource);
        var mockUser = new User { Id = Guid.NewGuid() };

        _authenticatorService.GetUserAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<bool>()).Returns(mockUser);
        _roleRepository.IsUserQuotaAdmin(mockUser.Id).Returns(true);

        // Act
        await _authHandler.HandleRequirementAsync(context, requirement, resource);

        // Assert
        Assert.That(context.HasSucceeded, Is.True, "Authorization should succeed if the user is a quota admin and the requirement is Read or Update.");
    }

    public class TestableProjectQuotaAuthHandler(
        ILogger<ProjectQuotaAuthHandler> logger,
        IRoleRepository roleRepository,
        IProjectRoleRepository projectRoleRepository,
        IAuthenticatorService authenticatorService) : ProjectQuotaAuthHandler(logger, roleRepository, projectRoleRepository, authenticatorService)
    {

        public new Task HandleRequirementAsync(AuthorizationHandlerContext context, OperationAuthorizationRequirement requirement, ProjectQuota resource)
        {
            return base.HandleRequirementAsync(context, requirement, resource);
        }
    }
}
