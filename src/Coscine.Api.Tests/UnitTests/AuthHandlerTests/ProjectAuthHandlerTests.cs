using System.Security.Claims;
using Coscine.Api.Authorization.AuthHandlers;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.Operations;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;

namespace Coscine.Api.Tests.UnitTests.AuthHandlerTests;

[TestFixture]
public class ProjectAuthHandlerTests
{
    private ILogger<ProjectAuthHandler> _logger = null!;
    private IRoleRepository _roleRepository = null!;
    private IProjectRoleRepository _projectRoleRepository = null!;
    private IAuthenticatorService _authenticatorService = null!;
    private TestableProjectAuthHandler _authHandler = null!;

    [SetUp]
    public void SetUp()
    {
        _logger = Substitute.For<ILogger<ProjectAuthHandler>>();
        _roleRepository = Substitute.For<IRoleRepository>();
        _projectRoleRepository = Substitute.For<IProjectRoleRepository>();
        _authenticatorService = Substitute.For<IAuthenticatorService>();
        _authHandler = new TestableProjectAuthHandler(_logger, _roleRepository, _projectRoleRepository, _authenticatorService);
    }

    [Test]
    public async Task HandleRequirementAsync_UserIsAdmin_ShouldSucceed()
    {
        // Arrange
        var requirement = new OperationAuthorizationRequirement { Name = nameof(ProjectOperations.Read) };
        var project = new Project { Id = Guid.NewGuid() };
        var user = new ClaimsPrincipal(new ClaimsIdentity([new Claim(ClaimTypes.Role, ApiRoles.Administrator)]));
        var context = new AuthorizationHandlerContext([requirement], user, project);

        // Act
        await _authHandler.HandleRequirementAsync(context, requirement, project);

        // Assert
        Assert.That(context.HasSucceeded, Is.True, "Authorization should succeed for administrators.");
    }

    [Test]
    public async Task HandleRequirementAsync_UserIsNull_ShouldNotSucceed()
    {
        // Arrange
        var requirement = new OperationAuthorizationRequirement { Name = nameof(ProjectOperations.Read) };
        var project = new Project { Id = Guid.NewGuid() };
        var user = new ClaimsPrincipal();
        var context = new AuthorizationHandlerContext([requirement], user, project);

        _authenticatorService.GetUserAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<bool>()).Returns((User?)null);

        // Act
        await _authHandler.HandleRequirementAsync(context, requirement, project);

        // Assert
        Assert.That(context.HasSucceeded, Is.False, "Authorization should not succeed if the user is null.");
    }

    [TestCase(nameof(ProjectOperations.Read), true, "BE294C5E-4E42-49B3-BEC4-4B15F49DF9A5")] // Owner
    [TestCase(nameof(ProjectOperations.Update), false, "9184A442-4419-4E30-9FE6-0CFE32C9A81F")] // Guest
    public async Task HandleRequirementAsync_UserRoleAuthorizationTests(string operation, bool expectedResult, string roleIdString)
    {
        // Arrange
        var requirement = new OperationAuthorizationRequirement { Name = operation };
        var project = new Project { Id = Guid.NewGuid() };
        var user = new ClaimsPrincipal();
        var context = new AuthorizationHandlerContext([requirement], user, project);
        var mockUser = new User { Id = Guid.NewGuid() };
        var roleId = Guid.Parse(roleIdString);

        _authenticatorService.GetUserAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<bool>()).Returns(mockUser);

        if (expectedResult)
        {
            _projectRoleRepository.GetAllByProjectAsync(project.Id, mockUser.Id, Arg.Any<bool>())
                .Returns([new ProjectRole { RoleId = roleId }]);
        }
        else
        {
            _projectRoleRepository.GetAllByProjectAsync(project.Id, mockUser.Id, Arg.Any<bool>()).Returns(Array.Empty<ProjectRole>());
        }

        // Act
        await _authHandler.HandleRequirementAsync(context, requirement, project);

        // Assert
        Assert.That(context.HasSucceeded, Is.EqualTo(expectedResult), 
            $"Authorization should {(expectedResult ? "succeed" : "fail")} for operation {operation} with role {roleId}.");
    }

    [Test]
    public async Task HandleRequirementAsync_UserIsQuotaAdmin_ShouldSucceedForReadQuota()
    {
        // Arrange
        var requirement = new OperationAuthorizationRequirement { Name = nameof(ProjectOperations.ReadAvailableResourceTypes) };
        var project = new Project { Id = Guid.NewGuid() };
        var user = new ClaimsPrincipal();
        var context = new AuthorizationHandlerContext([requirement], user, project);
        var mockUser = new User { Id = Guid.NewGuid() };

        _authenticatorService.GetUserAsync(Arg.Any<ClaimsPrincipal>(), Arg.Any<bool>()).Returns(mockUser);
        _roleRepository.IsUserQuotaAdmin(mockUser.Id).Returns(true);

        // Act
        await _authHandler.HandleRequirementAsync(context, requirement, project);

        // Assert
        Assert.That(context.HasSucceeded, Is.True, "Authorization should succeed if the user is a quota admin and the requirement is ReadAvailableResourceTypes.");
    }

    public class TestableProjectAuthHandler(
        ILogger<ProjectAuthHandler> logger,
        IRoleRepository roleRepository,
        IProjectRoleRepository projectRoleRepository,
        IAuthenticatorService authenticatorService) : ProjectAuthHandler(logger, roleRepository, projectRoleRepository, authenticatorService)
    {

        public new Task HandleRequirementAsync(AuthorizationHandlerContext context, OperationAuthorizationRequirement requirement, Project project)
        {
            return base.HandleRequirementAsync(context, requirement, project);
        }
    }
}
