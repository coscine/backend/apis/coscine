using NUnit.Framework;
using VDS.RDF;
using Coscine.Api.Tests.Mockups;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Shared.RequestFeatures;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared;

namespace Coscine.Api.Tests.UnitTests;

[TestFixture]
public class ApplicationProfileRepositoryTests
{
    private MockupRdfRepositoryBase _rdfRepositoryBase = null!;
    private NotDisposingTripleStore _tripleStore = null!;

    [SetUp]
    public void SetUp()
    {
        // Initialize the in-memory quad store
        _tripleStore = new NotDisposingTripleStore();
        _rdfRepositoryBase = new MockupRdfRepositoryBase(_tripleStore);
    }

    //[Test]
    public async Task GetPagedAsync_ReturnsPagedResults()
    {
        // Arrange
        var repository = new ApplicationProfileRepository(_rdfRepositoryBase);
        var parameters = new ApplicationProfileParameters
        {
            PageSize = 10,
            PageNumber = 1,
            SearchTerm = "test",
            Language = AcceptedLanguage.en,
            Modules = false
        };

        var testUri = new Uri("https://purl.org/coscine/ap/test");
        var graph = new Graph { BaseUri = testUri };
        graph.Assert(graph.CreateUriNode(testUri), graph.CreateUriNode(new Uri(RdfUris.RdfsUrlPrefix + "label")), graph.CreateLiteralNode("Test Profile", "en"));
        graph.Assert(graph.CreateUriNode(testUri), graph.CreateUriNode(new Uri(RdfUris.RdfsUrlPrefix + "comment")), graph.CreateLiteralNode("A test description.", "en"));

        await _rdfRepositoryBase.AddGraphAsync(graph);

        // Act
        var result = await repository.GetPagedAsync(parameters);

        // Assert
        Assert.That(result, Is.Not.Null, "The result should not be null.");
        Assert.That(result.Count(), Is.EqualTo(1), "The result should contain exactly one item.");
        var profile = result.First();
        Assert.That(profile.Uri.ToString(), Is.EqualTo(testUri.ToString()), "The profile URI should match.");
        Assert.That(profile.DisplayName, Is.EqualTo("Test Profile"), "The profile name should match.");
        Assert.That(profile.Description, Is.EqualTo("A test description."), "The profile description should match.");
    }

    //[Test]
    public async Task GetAsync_ReturnsApplicationProfileDetails()
    {
        // Arrange
        var repository = new ApplicationProfileRepository(_rdfRepositoryBase);
        var apUri = new Uri("https://purl.org/coscine/ap/detail");
        var rdfFormat = RdfFormat.Turtle;
        var language = AcceptedLanguage.en;

        var graph = new Graph { BaseUri = apUri };
        graph.Assert(graph.CreateUriNode(apUri), graph.CreateUriNode(RdfUris.RdfsUrlPrefix + "label"), graph.CreateLiteralNode("Detailed Profile", "en"));
        graph.Assert(graph.CreateUriNode(apUri), graph.CreateUriNode(RdfUris.RdfsUrlPrefix + "comment"), graph.CreateLiteralNode("Profile description.", "en"));

        await _rdfRepositoryBase.AddGraphAsync(graph);

        // Act
        var result = await repository.GetAsync(apUri, rdfFormat, language);

        // Assert
        Assert.That(result, Is.Not.Null, "The result should not be null.");
        Assert.That(result!.Uri, Is.EqualTo(apUri), "The profile URI should match.");
        Assert.That(result.DisplayName, Is.EqualTo("Detailed Profile"), "The profile name should match.");
        Assert.That(result.Description, Is.EqualTo("Profile description."), "The profile description should match.");
        Assert.That(result.Definition, Is.Not.Null.And.Not.Empty, "The serialized graph data should not be null or empty.");
    }

    [Test]
    public async Task GetAsync_ReturnsNullForNonExistentProfile()
    {
        // Arrange
        var repository = new ApplicationProfileRepository(_rdfRepositoryBase);
        var apUri = new Uri("https://purl.org/coscine/ap/nonexistent");
        var rdfFormat = RdfFormat.Turtle;
        var language = AcceptedLanguage.en;

        // No graph is added to simulate non-existent profile

        // Act
        var result = await repository.GetAsync(apUri, rdfFormat, language);

        // Assert
        Assert.That(result, Is.Null, "The result should be null for a non-existent profile.");
    }
}