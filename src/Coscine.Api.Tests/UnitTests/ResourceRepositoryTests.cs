using NUnit.Framework;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Tests.Mockups;
using Coscine.Api.Core.Repository.Contracts;
using VDS.RDF;
using VDS.RDF.Parsing;
using Coscine.Api.Core.Shared;
using VDS.RDF.Nodes;

namespace Coscine.Api.Tests.UnitTests;

[TestFixture]
public class ResourceRepositoryTests
{
    private NotDisposingTripleStore _notDisposingTripleStore = null!;
    private MockupRdfRepositoryBase _mockupRdfRepositoryBase = null!;
    private IResourceRepository _resourceRepository = null!;

    [SetUp]
    public void SetUp()
    {
        _notDisposingTripleStore = new NotDisposingTripleStore();
        _mockupRdfRepositoryBase = new MockupRdfRepositoryBase(_notDisposingTripleStore);
        _resourceRepository = new ResourceRepository(null!, _mockupRdfRepositoryBase);
    }

    [TearDown]
    public void TearDown()
    {
        _notDisposingTripleStore.Dispose();
    }

    [Test]
    public async Task DeleteGraphsAsync()
    {
        // Arrange
        var resourceToDelete = new Resource { Id = Guid.NewGuid() };
        var resourceToRemain = new Resource { Id = Guid.NewGuid() };

        var graph = new Graph(RdfUris.TrellisGraph);

        // Define URIs and literals
        var subjectNodeToDelete = graph.CreateUriNode(_resourceRepository.GenerateGraphName(resourceToDelete));
        var subjectNodeToDeleteAcl = graph.CreateUriNode(_resourceRepository.GenerateAclGraphName(resourceToDelete));
        var subjectNodeToRemain = graph.CreateUriNode(_resourceRepository.GenerateGraphName(resourceToRemain));
        var subjectNodeToRemainAcl = graph.CreateUriNode(_resourceRepository.GenerateAclGraphName(resourceToRemain));
        var rdfType = graph.CreateUriNode(new Uri("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"));
        var basicContainer = graph.CreateUriNode(new Uri("http://www.w3.org/ns/ldp#BasicContainer"));
        var dctermsModified = graph.CreateUriNode(new Uri("http://purl.org/dc/terms/modified"));
        var dctermsIsPartOf = graph.CreateUriNode(new Uri("http://purl.org/dc/terms/isPartOf"));
        var partOfObject = graph.CreateUriNode(new Uri("https://purl.org/coscine/resources/"));
        var modificationDate = graph.CreateLiteralNode("2024-12-03T16:02:24.922503Z", UriFactory.Create(XmlSpecsHelper.XmlSchemaDataTypeDateTime));

        // Add triples to the graph
        graph.Assert(new Triple(subjectNodeToDelete, rdfType, basicContainer));
        graph.Assert(new Triple(subjectNodeToDelete, dctermsModified, modificationDate));
        graph.Assert(new Triple(subjectNodeToDelete, dctermsIsPartOf, partOfObject));
        graph.Assert(new Triple(subjectNodeToRemain, rdfType, basicContainer));

        // Add named graphs for subjectNodeToDelete and subjectNodeToRemain
        var graphToDelete = new Graph(subjectNodeToDelete.Uri);
        var graphToRemain = new Graph(subjectNodeToRemain.Uri);
        var graphToDeleteAcl = new Graph(subjectNodeToDeleteAcl.Uri);
        var graphToRemainAcl = new Graph(subjectNodeToRemainAcl.Uri);

        _notDisposingTripleStore.Add(graph);
        _notDisposingTripleStore.Add(graphToDelete);
        _notDisposingTripleStore.Add(graphToRemain);
        _notDisposingTripleStore.Add(graphToDeleteAcl);
        _notDisposingTripleStore.Add(graphToRemainAcl);

        // Act
        await _resourceRepository.DeleteGraphsAsync(resourceToDelete);

        // Assert
        Assert.That(
            _notDisposingTripleStore.HasGraph(graph.Name),
            "The graph should remain in the store."
        );

        Assert.That(
            !_notDisposingTripleStore.HasGraph(graphToDelete.Name),
            "The graph for the deleted project should be removed."
        );

        Assert.That(
            !_notDisposingTripleStore.HasGraph(graphToDeleteAcl.Name),
            "The graph for the deleted project should be removed."
        );

        Assert.That(
            _notDisposingTripleStore.HasGraph(graphToRemain.Name),
            "The graph for the remaining project should still exist."
        );

        Assert.That(
            _notDisposingTripleStore.HasGraph(graphToRemainAcl.Name),
            "The graph for the remaining project should still exist."
        );

        Assert.That(
            !_notDisposingTripleStore.Triples.Any(t => t.Subject.Equals(subjectNodeToDelete)),
            "The graph should not contain triples related to the deleted project."
        );

        Assert.That(
            _notDisposingTripleStore.Triples.Count(t => t.Subject.Equals(subjectNodeToRemain)) > 0,
            "The graph should preserve triples related to other projects."
        );
    }

    [Test]
    public async Task CreateGraphsAsync()
    {
        // Arrange
        var project = new Project
        {
            Id = Guid.NewGuid(),
            ProjectName = "Test Project",
            Description = "Test Description",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddYears(1),
            DisplayName = "Test Display Name",
            PrincipleInvestigators = "Test Investigator",
            GrantId = "123456",
            Visibility = new Visibility { DisplayName = "Public" },
            Deleted = false,
            Slug = "test-project",
            Creator = Guid.NewGuid(),
            DateCreated = DateTime.UtcNow,
        };

        var rId = Guid.NewGuid();

        var resource = new Resource
        {
            Id = rId,
            ResourceName = "Test Resource",
            DisplayName = "Test Display Name",
            Type = new ResourceType { Id = Guid.NewGuid() },
            Visibility = new Visibility { DisplayName = "Public" },
            License = new License { DisplayName = "Test License" },
            Keywords = "keyword1;keyword2",
            UsageRights = "Test Usage Rights",
            Description = "Test Description",
            ApplicationProfile = "https://example.com/profile",
            Creator = Guid.NewGuid(),
            Archived = "0",
            Deleted = false,
            DateCreated = DateTime.UtcNow,
            ProjectResources =
            {
                new ProjectResource { Project = project, ResourceId = rId },
            }
        };

        // Act
        await _resourceRepository.CreateGraphsAsync(resource);

        // Assert
        var resourceGraphName = _resourceRepository.GenerateGraphName(resource);
        var aclGraphName = _resourceRepository.GenerateAclGraphName(resource);
        var trellisGraphName = RdfUris.TrellisGraph;

        Assert.That(await _mockupRdfRepositoryBase.HasGraphAsync(resourceGraphName), "The resource graph should be created.");
        Assert.That(await _mockupRdfRepositoryBase.HasGraphAsync(aclGraphName), "The ACL graph should be created.");
        Assert.That(await _mockupRdfRepositoryBase.HasGraphAsync(trellisGraphName), "The trellis graph should be created.");

        var resourceGraph = await _mockupRdfRepositoryBase.GetGraph(resourceGraphName);
        Assert.That(resourceGraph, Is.Not.Null, "The resource graph should not be null.");
        Assert.That(resourceGraph.Triples.Any(t => t.Subject.ToString() == resourceGraphName.ToString() && t.Predicate.ToString() == RdfUris.A.ToString() && t.Object.ToString() == RdfUris.DcatCatalogClass.ToString()), "The resource graph should contain a triple defining it as a DCAT Catalog.");
        Assert.That(resourceGraph.Triples.Any(t => t.Subject.ToString() == resourceGraphName.ToString() && t.Predicate.ToString() == RdfUris.DcTermsTitle.ToString() && t.Object.AsValuedNode().AsString() == resource.ResourceName), "The resource graph should contain the resource title.");
        Assert.That(resourceGraph.Triples.Any(t => t.Subject.ToString() == resourceGraphName.ToString() && t.Predicate.ToString() == RdfUris.DcTermsDescription.ToString() && t.Object.AsValuedNode().AsString() == resource.Description), "The resource graph should contain the resource description.");

        var aclGraph = await _mockupRdfRepositoryBase.GetGraph(aclGraphName);
        Assert.That(aclGraph, Is.Not.Null, "The ACL graph should not be null.");
        Assert.That(aclGraph.Triples.Count != 0, "The ACL graph should contain authorization triples.");

        var trellisGraph = await _mockupRdfRepositoryBase.GetGraph(trellisGraphName);
        Assert.That(trellisGraph, Is.Not.Null, "The Trellis graph should not be null.");
        Assert.That(trellisGraph.Triples.Any(t => t.Subject.ToString() == resourceGraphName.ToString() && t.Predicate.ToString() == RdfUris.A.ToString() && t.Object.ToString() == RdfUris.LdpBasicContainerClass.ToString()), "The Trellis graph should contain a triple defining it as an LDP BasicContainer.");
        Assert.That(trellisGraph.Triples.Any(t => t.Subject.ToString() == resourceGraphName.ToString() && t.Predicate.ToString() == RdfUris.DcTermsIsPartOf.ToString() && t.Object.ToString() == RdfUris.CoscineResources.ToString()), "The Trellis graph should contain a reference to the Coscine resources.");
    }
    [Test]
    public async Task UpdateGraphsAsync()
    {
        // Arrange
        var project = new Project
        {
            Id = Guid.NewGuid(),
            ProjectName = "Test Project",
            Description = "Test Description",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddYears(1),
            DisplayName = "Test Display Name",
            PrincipleInvestigators = "Test Investigator",
            GrantId = "123456",
            Visibility = new Visibility { DisplayName = "Public" },
            Deleted = false,
            Slug = "test-project",
            Creator = Guid.NewGuid(),
            DateCreated = DateTime.UtcNow,
        };

        var rId = Guid.NewGuid();

        var resource = new Resource
        {
            Id = rId,
            ResourceName = "Test Resource",
            DisplayName = "Test Display Name",
            Type = new ResourceType { Id = Guid.NewGuid() },
            Visibility = new Visibility { DisplayName = "Public" },
            License = new License { DisplayName = "Test License" },
            Keywords = "keyword1;keyword2",
            UsageRights = "Test Usage Rights",
            Description = "Test Description",
            ApplicationProfile = "https://example.com/profile",
            Creator = Guid.NewGuid(),
            Archived = "0",
            Deleted = false,
            DateCreated = DateTime.UtcNow,
            ProjectResources =
            {
                new ProjectResource { Project = project, ResourceId = rId },
            }
        };

        // Create initial graphs
        await _resourceRepository.CreateGraphsAsync(resource);

        // Change values
        resource.ResourceName = "Updated Resource Name";
        resource.Description = "Updated Description";

        // Act
        await _resourceRepository.UpdateGraphsAsync(resource);

        // Assert
        var resourceGraphName = _resourceRepository.GenerateGraphName(resource);
        var aclGraphName = _resourceRepository.GenerateAclGraphName(resource);
        var trellisGraphName = RdfUris.TrellisGraph;

        Assert.That(await _mockupRdfRepositoryBase.HasGraphAsync(resourceGraphName), "The resource graph should be created.");
        Assert.That(await _mockupRdfRepositoryBase.HasGraphAsync(aclGraphName), "The ACL graph should be created.");
        Assert.That(await _mockupRdfRepositoryBase.HasGraphAsync(trellisGraphName), "The trellis graph should be created.");

        var resourceGraph = await _mockupRdfRepositoryBase.GetGraph(resourceGraphName);
        Assert.That(resourceGraph, Is.Not.Null, "The resource graph should not be null.");
        Assert.That(resourceGraph.Triples.Any(t => t.Subject.ToString() == resourceGraphName.ToString() && t.Predicate.ToString() == RdfUris.A.ToString() && t.Object.ToString() == RdfUris.DcatCatalogClass.ToString()), "The resource graph should contain a triple defining it as a DCAT Catalog.");
        Assert.That(resourceGraph.Triples.Any(t => t.Subject.ToString() == resourceGraphName.ToString() && t.Predicate.ToString() == RdfUris.DcTermsTitle.ToString() && t.Object.AsValuedNode().AsString() == resource.ResourceName), "The resource graph should contain the resource title.");
        Assert.That(resourceGraph.Triples.Any(t => t.Subject.ToString() == resourceGraphName.ToString() && t.Predicate.ToString() == RdfUris.DcTermsDescription.ToString() && t.Object.AsValuedNode().AsString() == resource.Description), "The resource graph should contain the resource description.");

        var aclGraph = await _mockupRdfRepositoryBase.GetGraph(aclGraphName);
        Assert.That(aclGraph, Is.Not.Null, "The ACL graph should not be null.");
        Assert.That(aclGraph.Triples.Count != 0, "The ACL graph should contain authorization triples.");

        var trellisGraph = await _mockupRdfRepositoryBase.GetGraph(trellisGraphName);
        Assert.That(trellisGraph, Is.Not.Null, "The Trellis graph should not be null.");
        Assert.That(trellisGraph.Triples.Any(t => t.Subject.ToString() == resourceGraphName.ToString() && t.Predicate.ToString() == RdfUris.A.ToString() && t.Object.ToString() == RdfUris.LdpBasicContainerClass.ToString()), "The Trellis graph should contain a triple defining it as an LDP BasicContainer.");
        Assert.That(trellisGraph.Triples.Any(t => t.Subject.ToString() == resourceGraphName.ToString() && t.Predicate.ToString() == RdfUris.DcTermsIsPartOf.ToString() && t.Object.ToString() == RdfUris.CoscineResources.ToString()), "The Trellis graph should contain a reference to the Coscine resources.");
    }
}
