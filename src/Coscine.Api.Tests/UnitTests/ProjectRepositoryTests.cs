using NUnit.Framework;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Tests.Mockups;
using Coscine.Api.Core.Repository.Contracts;
using VDS.RDF;
using VDS.RDF.Parsing;
using Coscine.Api.Core.Shared;
using Microsoft.Extensions.Options;
using Coscine.Api.Core.Entities.ConfigurationModels;
using NSubstitute;
using VDS.RDF.Nodes;

namespace Coscine.Api.Tests.UnitTests;

[TestFixture]
public class ProjectRepositoryTests
{
    private NotDisposingTripleStore _notDisposingTripleStore = null!;
    private MockupRdfRepositoryBase _mockupRdfRepositoryBase = null!;
    private IOptionsMonitor<PidConfiguration> _pidConfigurationOptionsMock = null!;
    private IProjectRepository _projectRepository = null!;

    [SetUp]
    public void SetUp()
    {
        _notDisposingTripleStore = new NotDisposingTripleStore();
        _mockupRdfRepositoryBase = new MockupRdfRepositoryBase(_notDisposingTripleStore);
        _pidConfigurationOptionsMock = Substitute.For<IOptionsMonitor<PidConfiguration>>();
        _projectRepository = new ProjectRepository(null!, _mockupRdfRepositoryBase, _pidConfigurationOptionsMock);
    }

    [TearDown]
    public void TearDown()
    {
        _notDisposingTripleStore.Dispose();
    }

    [Test]
    public async Task DeleteGraphsAsync()
    {
        // Arrange
        var projectToDelete = new Project { Id = Guid.NewGuid() };
        var projectToRemain = new Project { Id = Guid.NewGuid() };

        var graph = new Graph(RdfUris.TrellisGraph);

        // Define URIs and literals
        var subjectNodeToDelete = graph.CreateUriNode(_projectRepository.GenerateGraphName(projectToDelete));
        var subjectNodeToRemain = graph.CreateUriNode(_projectRepository.GenerateGraphName(projectToRemain));
        var rdfType = graph.CreateUriNode(new Uri("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"));
        var basicContainer = graph.CreateUriNode(new Uri("http://www.w3.org/ns/ldp#BasicContainer"));
        var dctermsModified = graph.CreateUriNode(new Uri("http://purl.org/dc/terms/modified"));
        var dctermsIsPartOf = graph.CreateUriNode(new Uri("http://purl.org/dc/terms/isPartOf"));
        var partOfObject = graph.CreateUriNode(new Uri("https://purl.org/coscine/projects/"));
        var modificationDate = graph.CreateLiteralNode("2024-12-03T16:02:24.922503Z", UriFactory.Create(XmlSpecsHelper.XmlSchemaDataTypeDateTime));

        // Add triples to the graph
        graph.Assert(new Triple(subjectNodeToDelete, rdfType, basicContainer));
        graph.Assert(new Triple(subjectNodeToDelete, dctermsModified, modificationDate));
        graph.Assert(new Triple(subjectNodeToDelete, dctermsIsPartOf, partOfObject));
        graph.Assert(new Triple(subjectNodeToRemain, rdfType, basicContainer));

        // Add named graphs for subjectNodeToDelete and subjectNodeToRemain
        var graphToDelete = new Graph(subjectNodeToDelete.Uri);
        var graphToRemain = new Graph(subjectNodeToRemain.Uri);

        _notDisposingTripleStore.Add(graph);
        _notDisposingTripleStore.Add(graphToDelete);
        _notDisposingTripleStore.Add(graphToRemain);

        // Act
        await _projectRepository.DeleteGraphsAsync(projectToDelete);

        // Assert
        Assert.That(
            _notDisposingTripleStore.HasGraph(graph.Name),
            "The graph should remain in the store."
        );

        Assert.That(
            !_notDisposingTripleStore.HasGraph(graphToDelete.Name),
            "The graph for the deleted project should be removed."
        );

        Assert.That(
            _notDisposingTripleStore.HasGraph(graphToRemain.Name),
            "The graph for the remaining project should still exist."
        );

        Assert.That(
            !_notDisposingTripleStore.Triples.Any(t => t.Subject.Equals(subjectNodeToDelete)),
            "The graph should not contain triples related to the deleted project."
        );

        Assert.That(
            _notDisposingTripleStore.Triples.Count(t => t.Subject.Equals(subjectNodeToRemain)) > 0,
            "The graph should preserve triples related to other projects."
        );
    }

    [Test]
    public async Task CreateGraphsAsync()
    {
        // Arrange
        var project = new Project
        {
            Id = Guid.NewGuid(),
            ProjectName = "Test Project",
            Description = "Test Description",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddYears(1),
            DisplayName = "Test Display Name",
            PrincipleInvestigators = "Test Investigator",
            GrantId = "123456",
            Visibility = new Visibility { DisplayName = "Public" },
            Deleted = false,
            Slug = "test-project",
            Creator = Guid.NewGuid(),
            DateCreated = DateTime.UtcNow,
        };

        _pidConfigurationOptionsMock.CurrentValue.Returns(new PidConfiguration
        {
            Prefix = "testPrefix/",
            Password = "123",
            Url = "www.test.de",
            User = "testUser",
            DigitalObjectLocationUrl = "www.test.de"
        });


        // Act
        await _projectRepository.CreateGraphsAsync(project);

        // Assert
        var projectGraphName = _projectRepository.GenerateGraphName(project);
        Assert.That(await _mockupRdfRepositoryBase.HasGraphAsync(projectGraphName), "The project graph should be created.");
        Assert.That(await _mockupRdfRepositoryBase.HasGraphAsync(RdfUris.TrellisGraph), "The trellis graph should be created.");

        var projectGraph = await _mockupRdfRepositoryBase.GetGraph(projectGraphName);
        Assert.That(projectGraph, Is.Not.Null, "The project graph should not be null.");
        Assert.That(projectGraph.Triples.Any(t => t.Subject.ToString() == projectGraphName.ToString() && t.Predicate.ToString() == RdfUris.A.ToString() && t.Object.ToString() == RdfUris.DcatCatalogClass.ToString()), "The project graph should contain a triple defining it as a DCAT Catalog.");
        Assert.That(projectGraph.Triples.Any(t => t.Subject.ToString() == projectGraphName.ToString() && t.Predicate.ToString() == RdfUris.DcTermsTitle.ToString() && t.Object.AsValuedNode().AsString() == project.ProjectName), "The project graph should contain the project title.");
        Assert.That(projectGraph.Triples.Any(t => t.Subject.ToString() == projectGraphName.ToString() && t.Predicate.ToString() == RdfUris.DcTermsDescription.ToString() && t.Object.AsValuedNode().AsString() == project.Description), "The project graph should contain the project description.");

        var trellisGraph = await _mockupRdfRepositoryBase.GetGraph(RdfUris.TrellisGraph);
        Assert.That(trellisGraph, Is.Not.Null, "The trellis graph should not be null.");
        Assert.That(trellisGraph.Triples.Any(t => t.Subject.ToString() == projectGraphName.ToString() && t.Predicate.ToString() == RdfUris.A.ToString() && t.Object.ToString() == RdfUris.LdpBasicContainerClass.ToString()), "The trellis graph should contain a triple defining it as an LDP Basic Container.");
    }

    [Test]
    public async Task UpdateGraphsAsync()
    {
        // Arrange
        var project = new Project
        {
            Id = Guid.NewGuid(),
            ProjectName = "Test Project",
            Description = "Test Description",
            StartDate = DateTime.UtcNow,
            EndDate = DateTime.UtcNow.AddYears(1),
            DisplayName = "Test Display Name",
            PrincipleInvestigators = "Test Investigator",
            GrantId = "123456",
            Visibility = new Visibility { DisplayName = "Public" },
            Deleted = false,
            Slug = "test-project",
            Creator = Guid.NewGuid(),
            DateCreated = DateTime.UtcNow,
        };

        _pidConfigurationOptionsMock.CurrentValue.Returns(new PidConfiguration
        {
            Prefix = "testPrefix/",
            Password = "123",
            Url = "www.test.de",
            User = "testUser",
            DigitalObjectLocationUrl = "www.test.de"
        });

        await _projectRepository.CreateGraphsAsync(project);

        project.DisplayName = "Updated Display Name";
        project.Description = "Updated Description";

        // Act
        await _projectRepository.UpdateGraphsAsync(project);

        // Assert
        var projectGraphName = _projectRepository.GenerateGraphName(project);
        Assert.That(await _mockupRdfRepositoryBase.HasGraphAsync(projectGraphName), "The project graph should be created.");
        Assert.That(await _mockupRdfRepositoryBase.HasGraphAsync(RdfUris.TrellisGraph), "The trellis graph should be created.");

        var projectGraph = await _mockupRdfRepositoryBase.GetGraph(projectGraphName);
        Assert.That(projectGraph, Is.Not.Null, "The project graph should not be null.");
        Assert.That(projectGraph.Triples.Any(t => t.Subject.ToString() == projectGraphName.ToString() && t.Predicate.ToString() == RdfUris.A.ToString() && t.Object.ToString() == RdfUris.DcatCatalogClass.ToString()), "The project graph should contain a triple defining it as a DCAT Catalog.");
        Assert.That(projectGraph.Triples.Any(t => t.Subject.ToString() == projectGraphName.ToString() && t.Predicate.ToString() == RdfUris.DcTermsTitle.ToString() && t.Object.AsValuedNode().AsString() == project.ProjectName), "The project graph should contain the project title.");
        Assert.That(projectGraph.Triples.Any(t => t.Subject.ToString() == projectGraphName.ToString() && t.Predicate.ToString() == RdfUris.DcTermsDescription.ToString() && t.Object.AsValuedNode().AsString() == project.Description), "The project graph should contain the project description.");

        var trellisGraph = await _mockupRdfRepositoryBase.GetGraph(RdfUris.TrellisGraph);
        Assert.That(trellisGraph, Is.Not.Null, "The trellis graph should not be null.");
        Assert.That(trellisGraph.Triples.Any(t => t.Subject.ToString() == projectGraphName.ToString() && t.Predicate.ToString() == RdfUris.A.ToString() && t.Object.ToString() == RdfUris.LdpBasicContainerClass.ToString()), "The trellis graph should contain a triple defining it as an LDP Basic Container.");
    }
}
