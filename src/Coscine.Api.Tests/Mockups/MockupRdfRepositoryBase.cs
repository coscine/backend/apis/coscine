﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Repository.Contracts;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.Enums.Utility;
using Coscine.Api.Core.Shared.RequestFeatures;
using VDS.RDF;
using VDS.RDF.Parsing.Handlers;
using VDS.RDF.Query;
using VDS.RDF.Storage;
using VDS.RDF.Writing;

namespace Coscine.Api.Tests.Mockups;

public class MockupRdfRepositoryBase : IRdfRepositoryBase
{
    private readonly InMemoryManager _memoryManager;
    private readonly TripleStore _tripleStore;

    public int QUERY_LIMIT => 10000;

    public MockupRdfRepositoryBase(TripleStore tripleStore)
    {
        _tripleStore = tripleStore;
        _memoryManager = new InMemoryManager(_tripleStore);
    }

    public async Task AddGraphAsync(IGraph graph)
    {
        _tripleStore.Add(graph, true);
        await Task.CompletedTask;
    }

    public async Task ClearGraphAsync(Uri graphUri)
    {
        _tripleStore.Remove(graphUri);
        await Task.CompletedTask;
    }

    public async Task CreateGraphAsync(Uri graphUri)
    {
        await Task.CompletedTask;
    }

    public Task<PagedEnumerable<DeployedGraph>> GetDeployedGraphsAsync(DeployedGraphParameters deployedGraphParameters)
    {
        // ??? Seems to be in a weird place, not implementing
        throw new NotImplementedException();
    }

    public async Task<IGraph> GetGraph(Uri graphId)
    {
        var graph = new Graph(graphId);
        await _memoryManager.LoadGraphAsync(graph, graphId.AbsoluteUri, CancellationToken.None);
        graph.BaseUri = graphId;
        return graph;
    }

    public async Task<bool> HasGraphAsync(Uri graphUri)
    {
        var graph = await GetGraph(graphUri);
        return graph.Triples.Count > 0;
    }

    public IGraph ParseGraph(string rdfData, RdfFormat rdfFormat, Uri? graphUri = null)
    {
        var contentType = rdfFormat.GetEnumMemberValue();

        var graph = graphUri is not null ? new Graph(graphUri) : new Graph();
        var rdfParser = MimeTypesHelper.GetParser(contentType);
        rdfParser.Load(graph, new StringReader(rdfData));
        if (graphUri is not null)
        {
            graph.BaseUri = graphUri;
        }
        return graph;
    }

    public ITripleStore ParseTripleStore(string rdfData, RdfFormat rdfFormat)
    {
        var contentType = rdfFormat.GetEnumMemberValue();

        var tripleStore = new TripleStore();
        var storeParser = MimeTypesHelper.GetStoreParser(contentType);
        tripleStore.LoadFromString(rdfData, storeParser);

        return tripleStore;
    }

    public async Task<IGraph> RunGraphQueryAsync(SparqlParameterizedString query)
    {
        return (IGraph)await _memoryManager.QueryAsync(query.ToString(), CancellationToken.None);
    }

    public async Task<SparqlResultSet> RunQueryAsync(SparqlParameterizedString query)
    {
        var set = new SparqlResultSet();
        var setHandler = new ResultSetHandler(set);
        await _memoryManager.QueryAsync(null, setHandler, query.ToString(), CancellationToken.None);
        return set;
    }

    public string WriteGraph(IGraph graph, RdfFormat rdfFormat)
    {
        var contentType = rdfFormat.GetEnumMemberValue();

        switch (rdfFormat)
        {
            case RdfFormat.JsonLd:
            case RdfFormat.TriG:
                // JSON-LD & TriG not meant for a single graph
                var tripleStore = new TripleStore();
                tripleStore.Add(graph);
                var storeWriter = MimeTypesHelper.GetStoreWriter(contentType);
                return VDS.RDF.Writing.StringWriter.Write(tripleStore, storeWriter);

            case RdfFormat.Turtle:
                var turtleWriter = new CompressingTurtleWriter(WriterCompressionLevel.High);
                return VDS.RDF.Writing.StringWriter.Write(graph, turtleWriter);

            default:
                var rdfWriter = MimeTypesHelper.GetWriter(contentType);
                return VDS.RDF.Writing.StringWriter.Write(graph, rdfWriter);
        }
    }

    public void RemoveFromGraph(IGraph graph, IEnumerable<Triple> triples)
    {
        var chunks = triples.Chunk(100).ToList();

        foreach (var triplesChunk in chunks)
        {
            _memoryManager.UpdateGraph(graph.BaseUri, [], triplesChunk);
        }
    }

    public async Task RunUpdateAsync(SparqlParameterizedString query)
    {
        await _memoryManager.UpdateAsync(query.ToString(), CancellationToken.None);
    }

}
