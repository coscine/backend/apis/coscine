using Coscine.Api.Core.Entities.ConfigurationModels;
using Coscine.Api.Core.Repository;
using Coscine.Api.Core.Service;
using Coscine.Api.Core.Service.Contracts;
using Coscine.Api.Core.Service.Contracts.Observers;
using Coscine.Api.Core.Service.Observers;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NETCore.MailKit.Core;
using NSubstitute;
using NSubstitute.Extensions;

namespace Coscine.Api.Tests.Mockups;

public class ObserverManagerMockFactory
{
    public static IObserverManager CreatePartial(IEmailTemplateManager? emailTemplateManager = null, IEmailService? emailService = null)
    {
        emailTemplateManager ??= new EmailTemplateManager(new EmailTemplateRepository());
        emailService ??= Substitute.For<IEmailService>(); // Always mock IEmailService

        // Mock all ILogger<T> dependencies
        var apCreationRequestObserverLogger = Substitute.For<ILogger<EmailAPCreationRequestObserver>>();
        var apCreationRequestServiceDeskObserversLogger = Substitute.For<ILogger<EmailAPCreationRequestServiceDeskObserver>>();
        var pidContactConfirmationObserversLogger = Substitute.For<ILogger<EmailPidContactConfirmationObserver>>();
        var pidProjectConfirmationObserversLogger = Substitute.For<ILogger<EmailPidProjectContactObserver>>();
        var pidResourceConfirmationObserversLogger = Substitute.For<ILogger<EmailPidResourceContactObserver>>();
        var projectCreationObserversLogger = Substitute.For<ILogger<EmailProjectCreationObserver>>();
        var projectResponsibleOrganizationChangeObserversLogger = Substitute.For<ILogger<EmailProjectResponsibleOrganizationChangeObserver>>();
        var projectDeletionObserversLogger = Substitute.For<ILogger<EmailProjectDeletionObserver>>();
        var projectInvitationCreationObserversLogger = Substitute.For<ILogger<EmailProjectInvitationCreationObserver>>();
        var projectRoleCreationObserversLogger = Substitute.For<ILogger<EmailProjectRoleCreationObserver>>();
        var projectRoleUpdateObserversLogger = Substitute.For<ILogger<EmailProjectRoleUpdateObserver>>();
        var projectRoleDeletionObserversLogger = Substitute.For<ILogger<EmailProjectRoleDeletionObserver>>();
        var resourceCreationObserversLogger = Substitute.For<ILogger<EmailResourceCreationObserver>>();
        var resourceMaintenanceModeEnabledObserversLogger = Substitute.For<ILogger<EmailResourceMaintenanceModeEnabledObserver>>();
        var resourceMaintenanceModeDisabledObserversLogger = Substitute.For<ILogger<EmailResourceMaintenanceModeDisabledObserver>>();
        var resourceDeletionObserversLogger = Substitute.For<ILogger<EmailResourceDeletionObserver>>();
        var userEmailChangedObserversLogger = Substitute.For<ILogger<EmailUserEmailChangedObserver>>();

        // Mock IOptionsMonitor<ConnectionConfiguration>
        var connectionConfiguration = Substitute.For<IOptionsMonitor<ConnectionConfiguration>>();

        // Create a partial mock of ObserverManager
        var observerManager = Substitute.ForPartsOf<ObserverManager>(
            emailTemplateManager,
            emailService,
            apCreationRequestObserverLogger,
            apCreationRequestServiceDeskObserversLogger,
            pidContactConfirmationObserversLogger,
            pidProjectConfirmationObserversLogger,
            pidResourceConfirmationObserversLogger,
            projectCreationObserversLogger,
            projectResponsibleOrganizationChangeObserversLogger,
            projectDeletionObserversLogger,
            projectInvitationCreationObserversLogger,
            projectRoleCreationObserversLogger,
            projectRoleUpdateObserversLogger,
            projectRoleDeletionObserversLogger,
            resourceCreationObserversLogger,
            resourceMaintenanceModeEnabledObserversLogger,
            resourceMaintenanceModeDisabledObserversLogger,
            resourceDeletionObserversLogger,
            userEmailChangedObserversLogger,
            connectionConfiguration
        );

        return observerManager.Configure();
    }
}