﻿namespace Coscine.Api.Tests.Mockups.Data;

public static class MetadataMock
{
    public static string GetExtractedMetadata(string resourceUriString)
    {
        return $@"@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.

<{resourceUriString}/@type=data&version=1234&extracted=true> {{
    <{resourceUriString}> <http://purl.org/dc/terms/created> ""2024-05-13 07:54:54.171602"";
        <http://purl.org/dc/terms/creator> ""Benedikt Heinrichs"";
        <http://purl.org/dc/terms/format> ""text/plain; charset=windows-1252"";
        <http://purl.org/dc/terms/identifier> ""textSentence.txt"";
        <http://purl.org/dc/terms/modified> ""2024-05-13 07:50:17"";
        a <http://www.w3.org/ns/dcat#Catalog>,
            <http://www.w3.org/ns/dcat#Distribution>;
        <http://www.w3.org/ns/dcat#byteSize> ""79"";
        <http://www.w3.org/ns/dcat#mediaType> ""text/plain"";
        <https://purl.org/coscine/ontologies/tika/Content_Encoding> ""windows-1252"";
        <https://purl.org/coscine/ontologies/tika/X_TIKA_Parsed_By> ""org.apache.tika.parser.DefaultParser"",
                                                                    ""org.apache.tika.parser.csv.TextAndCSVParser"";
        <https://purl.org/coscine/ontologies/tika/X_TIKA_Parsed_By_Full_Set> ""org.apache.tika.parser.DefaultParser"",
                                                                            ""org.apache.tika.parser.csv.TextAndCSVParser"".
}}

<{resourceUriString}/@type=metadata&version=1234&extracted=true> {{
    <{resourceUriString}> <https://purl.org/coscine/ontologies/topic#about> <https://purl.org/coscine/ontologies/topic/attributes#2023>,
        <https://purl.org/coscine/ontologies/topic/attributes#electron>,
        <https://purl.org/coscine/ontologies/topic/attributes#measured>,
        <https://purl.org/coscine/ontologies/topic/attributes#microscope>,
        <https://purl.org/coscine/ontologies/topic/attributes#sample>,
        <https://purl.org/coscine/ontologies/topic/attributes#september>.
}}";
    }

    public static string GetTemplatedMetadata(string resourceUriString)
    {
        return $@"@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.

<{resourceUriString}>  <http://purl.org/dc/terms/created> ""2020-09-23""^^<http://www.w3.org/2001/XMLSchema#date> ;
    <http://purl.org/dc/terms/creator> ""{{{{http://purl.org/dc/terms/creator}}}}"" ;
    <http://purl.org/dc/terms/subject> ""{{{{http://purl.org/dc/terms/subject}}}}"" ;
    <http://purl.org/dc/terms/title> ""FixedTitle"" ;
    a <https://purl.org/coscine/ap/base/> .";
    }

    public static string GetIncompleteMetadata(string resourceUriString)
    {
        return $@"@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.

<{resourceUriString}>  <http://purl.org/dc/terms/created> ""2020-09-23""^^<http://www.w3.org/2001/XMLSchema#date> ;
    <http://purl.org/dc/terms/creator> ""{{{{http://purl.org/dc/terms/creator}}}}"" ;
    a <https://purl.org/coscine/ap/base/> .";
    }
}
