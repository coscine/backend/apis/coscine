using Coscine.Api.Core.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using NSubstitute;
using NSubstitute.Extensions;

namespace Coscine.Api.Tests.Mockups;

/// <summary>
/// Provides methods to create a mock of <see cref="RepositoryContextLoader"/> for unit testing purposes.
/// </summary>
public class RepositoryContextLoaderMockFactory
{
    /// <summary>
    /// Creates a mock instance of <see cref="RepositoryContextLoader"/> using an in-memory database.
    /// </summary>
    /// <returns>
    /// A partially substituted <see cref="RepositoryContextLoader"/> instance with an in-memory database context.
    /// </returns>
    public static RepositoryContextLoader CreatePartial(DbContextOptions<RepositoryContext> dbContextOptions)
    {
        var repositoryContext = Substitute.ForPartsOf<RepositoryContextLoader>(new RepositoryContext(dbContextOptions));
        return repositoryContext.Configure();
    }

    /// <summary>
    /// Creates a mock instance of <see cref="RepositoryContextLoader"/> with a mocked database transaction.
    /// </summary>
    /// <returns>
    /// A partially substituted <see cref="RepositoryContextLoader"/> instance where 
    /// <see cref="RepositoryContextLoader.SaveAsync"/> and 
    /// <see cref="RepositoryContextLoader.BeginTransactionAsync"/> are stubbed.
    /// </returns>
    public static RepositoryContextLoader CreatePartialWithTransactionMock(DbContextOptions<RepositoryContext> dbContextOptions)
    {
        var repositoryContextLoader = CreatePartial(dbContextOptions);
        var transactionMock = Substitute.For<IDbContextTransaction>();
        transactionMock.CommitAsync().Returns(Task.CompletedTask);

        repositoryContextLoader
            .SaveAsync().Returns(Task.CompletedTask);
        repositoryContextLoader
            .BeginTransactionAsync().Returns(Task.FromResult(transactionMock));
        
        return repositoryContextLoader;
    }
}