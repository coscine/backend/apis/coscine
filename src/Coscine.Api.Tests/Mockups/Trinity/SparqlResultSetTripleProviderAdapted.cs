﻿using VDS.RDF;
using VDS.RDF.Query;

namespace Semiodesk.Trinity.Store
{
    /// <summary>
    /// Adapted from: https://github.com/semiodesk/trinity-rdf/blob/develop/Trinity/Stores/dotNetRDF/SparqlResultSetTripleProvider.cs
    /// Necessary since Trinity relies on dotNetRdf 2, but we use dotNetRdf 3
    /// </summary>
    public class SparqlResultSetTripleProviderAdapted : ITripleProvider
    {
        private int _n;

        private SparqlResultSet _set;

        private string _subjectKey;

        public INode S
        {
            get { return _set[_n][_subjectKey]; }
        }

        private string _predicateKey;

        public Uri P
        {
            get { return (_set[_n][_predicateKey] as UriNode).Uri; }
        }

        private string _objectKey;

        public INode O
        {
            get { return _set[_n][_objectKey]; }
        }

        public int Count
        {
            get { return _set.Count; }
        }

        public bool HasNext
        {
            get { return _n < _set.Count; }
        }

        #region Constructors

        public SparqlResultSetTripleProviderAdapted(SparqlResultSet set, string subjectKey, string predicateKey, string objectKey)
        {
            _n = 0;
            _set = set;
            _subjectKey = subjectKey;
            _predicateKey = predicateKey;
            _objectKey = objectKey;
        }

        #endregion

        #region Methods

        public void Reset()
        {
            _n = 0;
        }

        public void SetNext()
        {
            _n += 1;
        }

        #endregion
    }
}