﻿using VDS.RDF;

namespace Coscine.Api.Tests.Mockups;

/// <summary>
/// A triple store, which does not dispose the graphs when going out of context.
/// Necessary to keep graphs during multiple requests.
/// </summary>
public class NotDisposingTripleStore : TripleStore
{
    public override void Dispose()
    {
        // Do nothing
    }
}
