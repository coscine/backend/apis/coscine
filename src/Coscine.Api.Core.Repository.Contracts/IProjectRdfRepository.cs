﻿using Coscine.Api.Core.Entities.Models.QuadStore;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IProjectRdfRepository
{
    ProjectRdf? Get(Guid projectId);
    ProjectRdf? Get(Uri projectRdfId);
}
