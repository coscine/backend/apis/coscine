using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Repository.Contracts;

public class CreateDataStorageParameters
{
    public long? Quota { get; set; }
    public CreateGitLabDataStorageOptions? CreateGitLabDataStorageOptions { get; set; }
}

public class CreateGitLabDataStorageOptions
{
    public string Branch { get; set; } = null!;
    public string RepoUrl { get; set; } = null!;
    public string ProjectAccessToken { get; set; } = null!;
    public int GitlabProjectId { get; set; }
    public bool TosAccepted { get; set; }
}

public class UpdateDataStorageParameters
{
    public long? Quota { get; set; }
    public bool? ReadOnly { get; set; }
    public bool? MaintenanceMode { get; set; }
}

public class StorageInfoParameters
{
    public required Resource Resource { get; set; }
    public bool FetchUsedSize { get; set; } = false; // Calculate the total used size.
    public bool FetchObjectCount { get; set; } = false; // Count the number of objects.
    public bool FetchAllocatedSize { get; set; } = false; // Calculate the allocated size.
}

public class DataStorageInfo
{
    public long? UsedSize { get; set; } // Total used size in bytes.
    public long? AllocatedSize { get; set; } // Allocated size in bytes.
    public int? ObjectCount { get; set; } // Number of objects.
    public FileSystemStorageInfo? FileSystemStorageInfo { get; set; }
    public GitlabStorageInfo? GitlabStorageInfo { get; set; }
    public RdsS3StorageInfo? RdsS3StorageInfo { get; set; }
    public RdsS3WormStorageInfo? RdsS3WormStorageInfo { get; set; }
    public RdsStorageInfo? RdsStorageInfo { get; set; }
}

public class FileSystemStorageInfo
{
    public required string Directory { get; set; }
}

public class GitlabStorageInfo
{
    /// <summary>
    /// The project ID associated with GitLab.
    /// </summary>
    public required int ProjectId { get; init; }

    /// <summary>
    /// The repository URL for GitLab.
    /// </summary>
    public required string RepoUrl { get; init; }

    /// <summary>
    /// The access token for GitLab.
    /// </summary>
    public required string AccessToken { get; init; }

    /// <summary>
    /// The branch for GitLab.
    /// </summary>
    public required string Branch { get; init; }
}

public record RdsS3StorageInfo
{
    /// <summary>
    /// The name of the bucket associated with RDS S3.
    /// </summary>
    public required string BucketName { get; init; }

    /// <summary>
    /// The access key for reading from the RDS S3 bucket.
    /// </summary>
    public required string AccessKeyRead { get; init; }

    /// <summary>
    /// The secret key for reading from the RDS S3 bucket.
    /// </summary>
    public required string SecretKeyRead { get; init; }

    /// <summary>
    /// The access key for writing to the RDS S3 bucket.
    /// </summary>
    public required string AccessKeyWrite { get; init; }

    /// <summary>
    /// The secret key for writing to the RDS S3 bucket.
    /// </summary>
    public required string SecretKeyWrite { get; init; }

    /// <summary>
    /// The endpoint for the RDS S3 bucket.
    /// </summary>
    public required string Endpoint { get; init; }
}

public record RdsS3WormStorageInfo
{
    /// <summary>
    /// The name of the bucket associated with RDS S3.
    /// </summary>
    public required string BucketName { get; init; }

    /// <summary>
    /// The access key for reading from the RDS S3 bucket.
    /// </summary>
    public required string AccessKeyRead { get; init; }

    /// <summary>
    /// The secret key for reading from the RDS S3 bucket.
    /// </summary>
    public required string SecretKeyRead { get; init; }

    /// <summary>
    /// The access key for writing to the RDS S3 bucket.
    /// </summary>
    public required string AccessKeyWrite { get; init; }

    /// <summary>
    /// The secret key for writing to the RDS S3 bucket.
    /// </summary>
    public required string SecretKeyWrite { get; init; }

    /// <summary>
    /// The endpoint for the RDS S3 bucket.
    /// </summary>
    public required string Endpoint { get; init; }
}

public record RdsStorageInfo
{
    /// <summary>
    /// The name of the bucket associated with RDS S3.
    /// </summary>
    public required string BucketName { get; init; }

    /// <summary>
    /// The endpoint for the RDS S3 bucket.
    /// </summary>
    public required string Endpoint { get; init; }
}

public enum Capability
{
    CanRead,
    CanWrite,
    CanUpdate,
    CanDelete,
    UsesQuota,
    SupportsReadOnlyMode,
    SupportsDataGraphs,
    SupportsMetadataGraphs,
    SupportsLinks,
    SupportsLocalMetadataCopy,
    SupportsMaintenanceMode,
}

public interface IDataStorageRepository : IBlobStorageRepository, ITreeStorageRepository
{
    Guid TypeId { get; }

    Task CreateAsync(Resource resource, CreateDataStorageParameters parameters);
    Task UpdateAsync(Resource resource, UpdateDataStorageParameters parameters);
    IEnumerable<Capability> Capabilities { get; }
    Task<DataStorageInfo> GetStorageInfoAsync(StorageInfoParameters parameters);
    Task<ResourceTypeInformation> GetResourceTypeInformationAsync();

    //TODO: remove that stuff
    IEnumerable<SpecificType> GetSpecificResourceTypes();
    IEnumerable<SpecificType> GetSpecificResourceTypes(ResourceTypeStatus status);
    IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor);
    IEnumerable<SpecificType> GetSpecificResourceTypes(string OrganizationRor, ResourceTypeStatus status);
}