﻿using Coscine.Api.Core.Entities.Models.QuadStore;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IResourceRdfRepository
{
    ResourceRdf? Get(Guid projectId);
    ResourceRdf? Get(Uri resourceRdfId);
}
