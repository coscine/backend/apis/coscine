using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;

namespace Coscine.Api.Core.Repository.Contracts;

// Parameter object for creating a tree
public class CreateTreeParameters
{
    public required Resource Resource { get; set; }
    public required string Path { get; set; }
    public string? Version { get; set; }
}

// Parameter object for reading a tree
public class ReadTreeParameters
{
    public required Resource Resource { get; set; }
    public required string Path { get; set; }
    public string? Version { get; set; }
    public bool GeneratePresignedLinks { get; set; } = false;
}

// Parameter object for updating a tree
public class UpdateTreeParameters
{
    public required Resource Resource { get; set; }
    public required string Path { get; set; }
    public string? Version { get; set; }
}

// Parameter object for deleting a tree
public class DeleteTreeParameters
{
    public required Resource Resource { get; set; }
    public required string Path { get; set; }
    public string? Version { get; set; }
    public bool? Recursive { get; set; }
}

// Parameter object for listing items in a tree
public class ListTreeParameters
{
    public required Resource Resource { get; set; }
    public required string TreePath { get; set; }
    public bool GeneratePresignedLinks { get; set; } = false;
}


public interface ITreeStorageRepository
{
    Task CreateAsync(CreateTreeParameters parameters);                           // c
    Task<StorageItemInfo?> ReadAsync(ReadTreeParameters parameters);             // r
    Task UpdateAsync(UpdateTreeParameters parameters);                           // u
    Task DeleteAsync(DeleteTreeParameters parameters);                           // d
    Task<IEnumerable<StorageItemInfo>> ListAsync(ListTreeParameters parameters); // l
}