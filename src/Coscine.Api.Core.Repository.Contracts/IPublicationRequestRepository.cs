﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IProjectPublicationRequestRepository
{
    Task<IEnumerable<ProjectPublicationRequest>> GetAllAsync(Guid projectId, bool trackChanges);
    Task<PagedEnumerable<ProjectPublicationRequest>> GetPagedAsync(Guid projectId, ProjectPublicationRequestAdminParameters projectPublicationRequestAdminParameters, bool trackChanges);
    Task<ProjectPublicationRequest?> GetAsync(Guid projectId, Guid publicationRequestId, bool trackChanges);
    
    void Create(ProjectPublicationRequest projectPublicationRequest);
    
    void Delete(ProjectPublicationRequest projectPublicationRequest);
}
