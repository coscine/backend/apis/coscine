﻿using Microsoft.EntityFrameworkCore.Storage;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IRepositoryContextLoader
{
    Task SaveAsync();

    Task<IDbContextTransaction> BeginTransactionAsync();

    IExecutionStrategy CreateExecutionStrategy();
}