﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface ILicenseRepository
{
    Task<License?> GetAsync(Guid LicenseId, bool trackChanges);

    Task<PagedEnumerable<License>> GetPagedAsync(LicenseParameters licenseParameters, bool trackChanges);
}