﻿using Coscine.Api.Core.Entities.OtherModels;

namespace Coscine.Api.Core.Repository.Contracts;

[Obsolete($"This interface is obsolete and will be removed in the future. Use {nameof(INocRepository)} instead.")]
public interface IMaintenanceRepository
{
    Task<List<Maintenance>> GetAllAsync(bool trackChanges);

    Task<Maintenance?> GetCurrent(bool trackChanges);
}