﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface ITitleRepository
{
    Task<Title?> GetAsync(Guid titleId, bool trackChanges);

    Task<IEnumerable<Title>> GetAsync(TitleParameters titleParameters, bool trackChanges);
}