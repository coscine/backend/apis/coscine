using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IMessageRepository
{
    Task<PagedEnumerable<Message>> GetPagedFromConsulAsync(MessageParameters messageParameters);
}
