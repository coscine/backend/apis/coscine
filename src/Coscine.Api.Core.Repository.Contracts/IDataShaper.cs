﻿using Coscine.Api.Core.Entities.OtherModels;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IDataShaper<T>
{
    IEnumerable<ShapedEntity> ShapeData(IEnumerable<T> entities, string? fieldsString);

    ShapedEntity ShapeData(T entity, string? fieldsString);
}