﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IPidRepository
{
    Task<PagedEnumerable<Pid>> GetPagedAsync(string prefix, PidParameters pidParameters, bool trackChanges);
    Task<Pid?> GetAsync(string prefix, string suffix, bool verifyHandle, bool trackChanges);
}
