﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IProjectDisciplineRepository
{
    void Delete(ProjectDiscipline projectDiscipline);
}