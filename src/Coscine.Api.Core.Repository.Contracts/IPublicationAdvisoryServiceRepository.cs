﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.Enums;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IPublicationAdvisoryServiceRepository
{
    /// <summary>
    /// Retrieves the data publication advisory service of an organization based on its ROR (Research Organization Registry) URL.
    /// </summary>
    /// <param name="organizationRorUri">The ROR URL of the organization.</param>
    /// <returns>The data publication advisory service of the organization, or <c>null</c> if not found.</returns>
    Task<PublicationAdvisoryService?> GetPublicationAdvisoryServiceAsync(Uri organizationRorUri, AcceptedLanguage language);
}