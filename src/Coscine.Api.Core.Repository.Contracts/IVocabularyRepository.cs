﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IVocabularyRepository
{
    Task<PagedEnumerable<Vocabulary>> GetPagedTopLevelVocabulariesAsync(VocabularyParameters vocabularyParameters);

    Task<PagedEnumerable<VocabularyInstance>> GetPagedVocabularyInstancesAsync(Uri parentGraphUri, VocabularyInstancesParameters vocabularyParameters);

    Task<VocabularyInstance?> GetAsync(Uri parentGraphUri, Uri classUri, Uri instanceUri, AcceptedLanguage language, AcceptedLanguage fallbackLanguage);

    Task<Vocabulary?> GetAsync(VocabularyInstancesParameters vocabularyParameters);

    Task<Vocabulary?> GetTopLevelAsync(Uri instanceUri, AcceptedLanguage language, AcceptedLanguage fallbackLanguage);
}