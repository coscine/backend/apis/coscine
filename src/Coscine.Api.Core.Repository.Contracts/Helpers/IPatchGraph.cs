﻿using VDS.RDF;

namespace Coscine.Api.Core.Repository.Contracts.Helpers;

public interface IPatchGraph : IGraph
{
    /// <summary>
    /// List of triples to be asserted to the graph.
    /// </summary>
    List<Triple> AssertList { get; set; }

    /// <summary>
    /// List of triples to be retracted from the graph.
    /// </summary>
    List<Triple> RetractList { get; set; }

    /// <summary>
    /// Asserts a triple to the graph and adds it to the <see cref="AssertList"/>.
    /// </summary>
    /// <param name="t">The triple to assert.</param>
    /// <returns>Returns <c>true</c> if the triple is successfully asserted; otherwise, <c>false</c>.</returns>
    new bool Assert(Triple t);

    /// <summary>
    /// Asserts a collection of triples to the graph and adds them to the <see cref="AssertList"/>.
    /// </summary>
    /// <param name="triples">The triples to assert.</param>
    /// <returns>Returns <c>true</c> if the triples are successfully asserted; otherwise, <c>false</c>.</returns>
    new bool Assert(IEnumerable<Triple> triples);

    /// <summary>
    /// Retracts a triple from the graph and adds it to the <see cref="RetractList"/>.
    /// </summary>
    /// <param name="t">The triple to retract.</param>
    /// <returns>Returns <c>true</c> if the triple is successfully retracted; otherwise, <c>false</c>.</returns>
    new bool Retract(Triple t);

    /// <summary>
    /// Retracts a collection of triples from the graph and adds them to the <see cref="RetractList"/>.
    /// </summary>
    /// <param name="triples">The triples to retract.</param>
    /// <returns>Returns <c>true</c> if the triples are successfully retracted; otherwise, <c>false</c>.</returns>
    new bool Retract(IEnumerable<Triple> triples);
}
