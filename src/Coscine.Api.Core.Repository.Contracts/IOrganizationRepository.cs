﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IOrganizationRepository
{
    Task<IEnumerable<Organization>> GetByShibbolethUrlAsync(string shibbolethUrl);

    Task<IEnumerable<Organization>> GetInstitutesByUserExternalIdAsync(IEnumerable<ExternalId> userExternalIds);

    Task<Organization> GetByIkzAsync(Uri organizationUri);

    Task<Organization?> GetByAttributeScopeAsync(string attributeScope);

    Task<IEnumerable<Organization>> GetAllAsync();

    Task<string?> GetOrganizationEmailByRorUrlAsync(Uri organizationRorUri);

    /// <summary>
    /// Retrieves a paginated list of organizations based on the provided search criteria and language preference.
    /// <br/>
    /// The method performs a <b>SPARQL</b> query that fetches organizations from a specified graph. It applies filters 
    /// based on the search term (e.g., matching organization <i>labels</i>, <i>abbreviations</i>, or <i>alternative names</i>), 
    /// while prioritizing the organization's label by language: first the <b>preferred language</b> (<c>@lang</c>), 
    /// then the <b>fallback language</b> (<c>@fallbackLang</c>), and finally any other available language.
    /// <br/>
    /// The method supports optional filtering by publication advisory service availability and includes optional
    /// fields like mailbox and publication service details. It also sorts the results by specified fields before applying
    /// pagination for the response.
    /// </summary>
    /// <param name="organizationParameters">The parameters controlling search, language preference, filtering, sorting, and pagination.</param>
    /// <returns>A paginated list of <see cref="Organization"/> entities with the matched results.</returns>
    Task<PagedEnumerable<Organization>> GetPagedAsync(OrganizationParameters organizationParameters);

    Task<Organization?> GetByRorUrlAsync(Uri organizationRorUrl);
}