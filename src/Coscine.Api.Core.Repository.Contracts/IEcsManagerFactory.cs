using Coscine.ECSManager;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IEcsManagerFactory
{

    EcsManager GetRdsS3EcsManager(string configKey);
    EcsManager GetRdsS3WormEcsManager(string configKey);
    EcsManager GetRdsEcsManager(string configKey);
    EcsManager GetRdsS3UserEcsManager(string configKey);
}
