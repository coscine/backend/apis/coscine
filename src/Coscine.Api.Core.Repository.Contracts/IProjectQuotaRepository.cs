using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IProjectQuotaRepository
{
    Task<PagedEnumerable<ProjectQuota>> GetPagedAsync(Guid projectId, ProjectQuotaParameters projectQuotaParameters, bool trackChanges);

    Task<IEnumerable<ProjectQuota>> GetAllAsync(Guid projectId, IEnumerable<Guid> resourceTypeIds, ProjectQuotaParameters projectQuotaParameters, bool trackChanges);

    Task<ProjectQuota?> GetAsync(Guid projectId, Guid resourceTypeId, bool trackChanges);

    Task<IEnumerable<ProjectQuota>> GetAllAsync(Guid projectId, bool trackChanges);
}