using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IRdsS3WormResourceTypeRepository
{
    Task<RdsS3WormResourceType?> GetAsync(Guid rdsS3WormResourceTypeId, bool trackChanges);

    void Create(RdsS3WormResourceType rdsS3wormResourceType);
}