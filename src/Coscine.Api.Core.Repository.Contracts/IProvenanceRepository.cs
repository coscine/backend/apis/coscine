﻿using Coscine.Api.Core.Entities.OtherModels;
using VDS.RDF;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IProvenanceRepository
{
    /// <summary>
    /// Asynchronously adds provenance information to a graph. 
    /// The provenance information includes information such as versions, revisions, variants, invalidations and extraction details.
    /// </summary>
    /// <param name="metadataVersionId">The URI of the graph to which the provenance information will be added, including <c>/@type={type}&amp;version={version}</c> parameters.</param>
    /// <param name="createNewDataGraph">Boolean that determined if a new data graph shall be created.</param>
    /// <param name="createNewMetadataGraph">Boolean that determined if a new metadata graph shall be created.</param>
    /// <param name="existingMetadataGraphUri">The string representation of an existing metadata graph URI, if any.</param>
    /// <param name="extractedDataGraph">The specific extracted metadata data graph</param>
    /// <param name="extractedMetadataGraph">The specific extracted metadata metadata graph</param>
    /// <param name="provenance">The provenance information object.</param>
    /// <returns>A Task representing the asynchronous operation.</returns>
    Task<IEnumerable<IGraph>> AddProvenanceInformation(
        Uri metadataVersionId,
        bool createNewDataGraph,
        bool createNewMetadataGraph,
        Uri? existingMetadataGraphUri = null,
        IGraph? extractedDataGraph = null,
        IGraph? extractedMetadataGraph = null,
        Provenance? provenance = null
    );

    /// <summary>
    /// Creates an invalidation event for a specified resource, path, and type.
    /// The invalidation is recorded in the provenance graph with details on who invalidated the (meta-)data.
    /// </summary>
    /// <param name="resourceId">The unique identifier for the resource being invalidated.</param>
    /// <param name="path">The blob path or location associated with the resource.</param>
    /// <param name="graphId">The unique identifier for the graph where the invalidation should be recorded.</param>
    /// <param name="type">The type of graph being invalidated, e.g., <c>'data'</c> or <c>'metadata'</c> (default is <c>'metadata'</c>).</param>
    /// <param name="responsibleAgent">The user that invalidates the (meta-)data as an instance of <see cref="RdfUris.CoscineUsers"/>.</param>
    /// <returns>The created invalidation provenance graph.</returns>
    Task<IGraph> CreateInvalidation(Guid resourceId, string path, Uri graphId, string type = "metadata", Uri? responsibleAgent = null);
}
