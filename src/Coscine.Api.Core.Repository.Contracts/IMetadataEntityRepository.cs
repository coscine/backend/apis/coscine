﻿using Coscine.Api.Core.Entities.Models.QuadStore;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IMetadataEntityRepository
{
    MetadataEntity? Get(Uri metadataCatalogId, Uri metadataEntityId);
}
