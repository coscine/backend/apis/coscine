using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Repository.Contracts;

public interface ILinkedResourceTypeRepository
{
    Task<LinkedResourceType?> GetAsync(Guid linkedResourceTypeId, bool trackChanges);

    void Create(LinkedResourceType linkedResourceType);
}