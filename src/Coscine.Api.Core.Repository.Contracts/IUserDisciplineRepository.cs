﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IUserDisciplineRepository
{
    void Delete(UserDiscipline userDiscipline);

    Task MergeAsync(Guid fromUserId, Guid toUserId);
}