﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;
using VDS.RDF;
using VDS.RDF.Query;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IRdfRepositoryBase
{
    /// <summary>
    /// The maximum number of query results to retrieve in a single batch.
    /// </summary>
    /// <remarks>
    /// This limit is used to paginate results when querying large datasets, 
    /// ensuring efficient data retrieval and processing.
    /// </remarks>
    int QUERY_LIMIT { get; }

    /// <summary>
    /// Adds a graph to the RDF store.
    /// </summary>
    /// <param name="graph">The graph to add.</param>
    /// <returns>A task that represents the asynchronous operation.</returns>
    Task AddGraphAsync(IGraph graph);

    /// <summary>
    /// Retracts the given triples from the specified graph in the metadata store.
    /// </summary>
    /// <param name="graph">The graph from which the triples are retracted.</param>
    /// <param name="triples">The collection of triples to remove from the graph.</param>
    public void RemoveFromGraph(IGraph graph, IEnumerable<Triple> triples);

    /// <summary>
    /// Clears the content of a graph in the RDF store.
    /// </summary>
    /// <param name="graphUri">The URI of the graph to clear.</param>
    /// <returns>A task that represents the asynchronous operation.</returns>
    Task ClearGraphAsync(Uri graphUri);

    /// <summary>
    /// Creates a new graph in the RDF store.
    /// </summary>
    /// <param name="graphUri">The URI of the graph to create.</param>
    /// <returns>A task that represents the asynchronous operation.</returns>
    Task CreateGraphAsync(Uri graphUri);

    /// <summary>
    /// Retrieves a list of deployed graphs from the RDF store based on the specified parameters.
    /// </summary>
    /// <param name="deployedGraphParameters">The parameters to filter and paginate the list of deployed graphs.</param>
    /// <returns>A task that represents the asynchronous operation and contains the enumerable collection of deployed graphs as paged results.</returns>
    Task<PagedEnumerable<DeployedGraph>> GetDeployedGraphsAsync(DeployedGraphParameters deployedGraphParameters);

    /// <summary>
    /// Retrieves a graph from the RDF store.
    /// </summary>
    /// <param name="graphId">The identifier of the graph to retrieve.</param>
    /// <returns>A task that represents the asynchronous operation and contains the retrieved graph.</returns>
    Task<IGraph> GetGraph(Uri graphId);

    /// <summary>
    /// Checks if a graph exists in the RDF store.
    /// </summary>
    /// <param name="graphUri">The URI of the graph to check.</param>
    /// <returns>A task that represents the asynchronous operation and contains a boolean indicating the existence of the graph.</returns>
    Task<bool> HasGraphAsync(Uri graphUri);

    /// <summary>
    /// Parse a string to an RDF graph.
    /// </summary>
    /// <param name="rdfData">The RDF data to be parsed.</param>
    /// <param name="rdfFormat">The RDF format to use for serialization.</param>
    /// <param name="graphUri">If the graph name is known, it can be provided.</param>
    /// <returns>The parsed RDF representation of the graph as a <see cref="IGraph"/>.</returns>
    IGraph ParseGraph(string rdfData, RdfFormat rdfFormat, Uri? graphUri = null);

    /// <summary>
    /// Parse a string to an RDF tripleStore.
    /// </summary>
    /// <param name="rdfData">The RDF data to be parsed.</param>
    /// <param name="rdfFormat">The RDF format to use for serialization.</param>
    /// <returns>The parsed RDF representation of the graphs as a <see cref="ITripleStore"/>.</returns>
    ITripleStore ParseTripleStore(string rdfData, RdfFormat rdfFormat);

    /// <summary>
    /// Executes a SPARQL query that returns an RDF graph.
    /// </summary>
    /// <param name="query">The SPARQL query to execute.</param>
    /// <returns>A task that represents the asynchronous operation and contains the RDF graph.</returns>
    Task<IGraph> RunGraphQueryAsync(SparqlParameterizedString query);

    /// <summary>
    /// Executes a SPARQL query that returns a result set.
    /// </summary>
    /// <param name="query">The SPARQL query to execute.</param>
    /// <returns>A task that represents the asynchronous operation and contains the result set.</returns>
    Task<SparqlResultSet> RunQueryAsync(SparqlParameterizedString query);

    /// <summary>
    /// Executes a SPARQL query for updates.
    /// </summary>
    /// <param name="query">The SPARQL query to execute.</param>
    /// <returns>A task that represents the asynchronous operation.</returns>
    Task RunUpdateAsync(SparqlParameterizedString query);

    /// <summary>
    /// Serializes an RDF graph into a string representation using the specified RDF format.
    /// </summary>
    /// <param name="graph">The RDF graph to be serialized.</param>
    /// <param name="rdfFormat">The RDF format to use for serialization.</param>
    /// <returns>The serialized RDF representation of the graph as a string.</returns>
    string WriteGraph(IGraph graph, RdfFormat rdfFormat);
}