﻿using Coscine.Api.Core.Entities.OtherModels;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IDefaultProjectQuotaRepository
{
    public Task<IEnumerable<DefaultProjectQuota>> GetAllAsync(Guid userId);
}