﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IExternalIdRepository
{
    Task<ExternalId?> GetAsync(Guid userId, bool trackChanges);

    Task<ExternalId?> GetAsync(Guid externalAuthenticatorId, string extenalId, bool trackChanges);

    Task<IEnumerable<ExternalId>> GetAllAsync(Guid userId, bool trackChanges);

    void Create(ExternalId externalId);

    void Delete(ExternalId externalId);

    Task MergeAsync(Guid fromUserId, Guid toUserId);
}