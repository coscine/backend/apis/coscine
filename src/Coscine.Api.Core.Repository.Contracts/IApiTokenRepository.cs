﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IApiTokenRepository
{
    Task<ApiToken?> GetAsync(Guid apiTokenId, bool trackChanges);

    Task<ApiToken?> GetAsync(Guid apiTokenId, Guid userId, bool trackChanges);

    Task<PagedEnumerable<ApiToken>> GetPagedAsync(Guid userId, ApiTokenParameters apiTokenParameters, bool trackChanges);

    void Delete(ApiToken apiToken);

    void Create(ApiToken apiToken);

    Task MergeAsync(Guid fromUserId, Guid toUserId);
}