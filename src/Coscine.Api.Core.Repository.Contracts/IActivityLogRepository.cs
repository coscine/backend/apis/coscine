﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IActivityLogRepository
{
    Task<ActivityLog?> GetAsync(Guid activityLogId, bool trackChanges);

    Task<ActivityLog?> GetAsync(Guid activityLogId, Guid userId, bool trackChanges);

    Task<PagedEnumerable<ActivityLog>> GetPagedAsync(ActivityLogParameters activityLogParameters, bool trackChanges);

    void DeleteAllBeforeAsync(DateTime? timestampAfter);

    void Delete(ActivityLog activityLog);

    void Create(ActivityLog activityLog);

    Task MergeAsync(Guid fromUserId, Guid toUserId);
}