using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IProjectRoleRepository
{
    Task<ProjectRole?> GetAsync(Guid projectId, Guid projectRoleId, bool trackChanges);

    Task<IEnumerable<ProjectRole>> GetAllByProjectAsync(Guid projectId, Guid userId, bool trackChanges);

    Task<IEnumerable<ProjectRole>> GetAllByRoleAsync(Guid projectId, Guid roleId, bool trackChanges);

    Task<PagedEnumerable<ProjectRole>> GetPagedAsync(Guid projectId, ProjectRoleParameters projectRoleParameters, bool trackChanges);

    void Delete(ProjectRole projectRole);

    void Create(ProjectRole projectRole);

    Task<IEnumerable<ProjectRole>> GetAllByResourceAsync(Guid resourceId, Guid userId, bool trackChanges);

    Task MergeAsync(Guid fromUserId, Guid toUserId);
}