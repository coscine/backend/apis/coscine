﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IDisciplineRepository
{
    Task<Discipline?> GetAsync(Guid disciplineId, bool trackChanges);

    Task<PagedEnumerable<Discipline>> GetPagedAsync(DisciplineParameters disciplineParameters, bool trackChanges);
}