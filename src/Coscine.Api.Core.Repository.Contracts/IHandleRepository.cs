﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IHandleRepository
{
    Task<Handle?> GetAsync(string prefix, string suffix);
    Task UpdateAsync(string prefix, string suffix, Handle handle);
    IEnumerable<HandleValue> GenerateProjectHandleValues(Project project);
    IEnumerable<HandleValue> GenerateResourceHandleValues(Resource resource);
}