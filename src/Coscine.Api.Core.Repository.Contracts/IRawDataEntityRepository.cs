﻿using Coscine.Api.Core.Entities.Models.QuadStore;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IRawDataEntityRepository
{
    RawDataEntity? Get(Uri dataCatalogId);
}
