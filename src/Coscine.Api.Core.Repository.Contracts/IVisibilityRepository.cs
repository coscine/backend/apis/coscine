using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IVisibilityRepository
{
    Task<Visibility?> GetAsync(Guid visibilityId, bool trackChanges);

    Task<PagedEnumerable<Visibility>> GetPagedAsync(VisibilityParameters visibilityParameters, bool trackChanges);
}