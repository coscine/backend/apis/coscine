﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IResourceRepository
{
    Task<PagedEnumerable<Resource>> GetPagedAsync(ResourceAdminParameters resourceParameters, bool trackChanges);

    Task<PagedEnumerable<Resource>> GetPagedAsync(Guid projectId, ResourceParameters resourceParameters, bool trackChanges);

    Task<IEnumerable<Resource>> GetAllAsync(Guid projectId, Guid resourceTypeId, bool trackChanges);

    Task<Resource?> GetAsync(Guid projectId, Guid resourceId, bool trackChanges);

    Task<Resource?> GetIncludingDeletedAsync(Guid resourceId, bool trackChanges);

    void Delete(Resource resource);

    void Create(Resource resource, Guid creatorId);

    Task<Resource?> GetAsync(Guid resourceId, bool trackChanges);

    Task MergeAsync(Guid fromUserId, Guid toUserId);

    Uri GenerateGraphName(Resource resource);

    Uri GenerateAclGraphName(Resource resource);

    Task DeleteGraphsAsync(Resource resource);

    Task UpdateGraphsAsync(Resource resource);

    Task CreateGraphsAsync(Resource resource);
}
