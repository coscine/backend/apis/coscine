using Amazon.S3;
using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IS3ClientFactory
{
    IAmazonS3 GetClient(string accessKey, string secretKey, string endpoint);
    IAmazonS3 GetRdsS3Client(string configKey);
    IAmazonS3 GetRdsClient(string configKey);
    IAmazonS3 GetRdsS3WormClient(string configKey);
    IAmazonS3 GetClient(RdsS3ResourceType rdsS3ResourceType);
    IAmazonS3 GetClient(RdsResourceType rdsResourceType);
    IAmazonS3 GetClient(RdsS3WormResourceType rdsS3WormResourceType);
}
