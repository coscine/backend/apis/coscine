﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface ITermsOfServiceRepository
{
    Task<IEnumerable<Tosaccepted>> GetAllAsync(Guid userId, TermsOfServiceParameters termsOfServiceParameters, bool trackChanges);

    void Create(Tosaccepted termsOfService);

    void Delete(Tosaccepted termsOfService);

    Task<bool> HasAccepted(Guid userId, string version, bool trackChanges);

    Task MergeAsync(Guid fromUserId, Guid toUserId);
}
