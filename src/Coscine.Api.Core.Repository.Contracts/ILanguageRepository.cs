﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface ILanguageRepository
{
    Task<Language?> GetAsync(Guid languageId, bool trackChanges);

    Task<IEnumerable<Language>> GetAllAsync(LanguageParameters languageParameters, bool trackChanges);
}