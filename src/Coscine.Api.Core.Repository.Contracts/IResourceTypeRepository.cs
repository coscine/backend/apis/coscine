﻿using Coscine.Api.Core.Entities;
using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels.ResourceTypes;
using Coscine.Api.Core.Entities.ResourceTypeConfigs;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;
using System.Runtime.InteropServices;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IResourceTypeRepository
{
    Task<IEnumerable<ResourceType>> GetAllAsync(bool trackChanges);

    Task<PagedEnumerable<ResourceType>> GetPagedAsync(RequestParameters requestParameters, bool trackChanges);

    Task<ResourceType?> GetAsync(Guid id, bool trackChanges);
}