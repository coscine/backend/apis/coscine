namespace Coscine.Api.Core.Repository.Contracts;

public interface IDataStorageRepositoryFactory
{
    IDataStorageRepository? Resolve(string resourceTypeId);

    IDataStorageRepository? Create(Guid resourceTypeId);
}