﻿using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface ISearchRepository
{
    Task<long> CountAsync(SearchParameters searchParameters, SearchCategoryType categoryType, IEnumerable<Guid>? projectEntities);

    /// <summary>
    /// Asynchronously performs a search query on the ElasticSearch index using the given search parameters and optional project IDs for filtering.
    /// </summary>
    /// <param name="searchParameters">The search parameters to use for the query.</param>
    /// <param name="projectIds">A collection of optional project IDs to filter the search results.</param>
    /// <returns>A tuple containing the search results and the total count of matching documents.</returns>
    Task<(IEnumerable<dynamic> searchResults, long totalCount)> SearchAsync(SearchParameters searchParameters, IEnumerable<Guid>? projectIds);
}