﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.QueryParameters;
using Coscine.Api.Core.Shared.RequestFeatures;
using VDS.RDF;

namespace Coscine.Api.Core.Repository.Contracts;

public interface ITreeRepository
{
    /// <summary>
    /// Retrieves the newest <b><i>metadata trees</i></b> for a given resource, based on specified parameters. Invalidated trees will be ignored.
    /// </summary>
    /// <param name="resourceId">The unique identifier of the resource for which metadata trees are to be retrieved.</param>
    /// <param name="metadataTreeParameters">The parameters used to filter, order, and paginate the metadata trees that are returned.</param>
    /// <returns>
    /// A task resulting in a paged list of <see cref="MetadataTree"/> instances, each representing a metadata graph
    /// associated with the specified resource and adhering to the specified metadata tree parameters.
    /// </returns>
    Task<PagedEnumerable<MetadataTree>> GetNewestPagedMetadataAsync(
        Guid resourceId,
        MetadataTreeParameters metadataTreeParameters
    );

    /// <summary>
    /// Retrieves the newest <b><i>data trees</i></b> for a given resource, based on specified parameters. Invalidated trees will be ignored.
    /// </summary>
    /// <param name="resourceId">The unique identifier of the resource for which metadata trees are to be retrieved.</param>
    /// <param name="dataTreeParameters">The parameters used to filter, order, and paginate the data trees that are returned.</param>
    /// <returns>
    /// A task resulting in a paged list of <see cref="DataTree"/> instances, each representing a data graph
    /// associated with the specified resource and adhering to the specified data tree parameters.
    /// </returns>
    Task<PagedEnumerable<DataTree>> GetNewestPagedDataAsync(Guid resourceId, DataTreeParameters dataTreeParameters);

    /// <summary>
    /// This is the main method that orchestrates the creation of metadata graphs for a resource.
    /// </summary>
    /// <param name="resource">The resource entity containing fixed values and application profile URI.</param>
    /// <param name="user">The user entity whose information may be used in the graph, depending on fixed value tokens.</param>
    /// <param name="metadataTree">The metadata tree entity used as the basis for creating the graph, including its definition and format.</param>
    /// <returns>The created graphs.</returns>
    Task<IEnumerable<IGraph>> CreateMetadataGraphAsync(Resource resource, User? user, MetadataTree metadataTree);

    /// <summary>
    /// Updates an existing metadata graph based on the specified resource, user, and metadata tree.
    /// </summary>
    /// <param name="resource">The resource entity containing fixed values and application profile URI.</param>
    /// <param name="user">The user entity whose information may be used in the graph, depending on fixed value tokens.</param>
    /// <param name="metadataTree">The metadata tree entity for updating the graph, including its definition and format.</param>
    /// <returns>A Task containing the Graph.</returns>
    Task<IEnumerable<IGraph>> UpdateMetadataGraphAsync(Resource resource, User? user, MetadataTree metadataTree);

    /// <summary>
    /// Invalidates the metadata graph for a given resource.
    /// </summary>
    /// <param name="resourceId">The unique identifier of the resource associated with the metadata graph.</param>
    /// <param name="path">The path or key under which the metadata is stored or categorized.</param>
    /// <param name="graphId">The URI representing the unique ID of the graph to be invalidated.</param>
    /// <param name="invalidatedByUser">The user who is performing the invalidation action.</param>
    /// <returns>A Task containing the Graph.</returns>
    Task<IGraph> InvalidateMetadataGraphAsync(Guid resourceId, string path, Uri graphId, User invalidatedByUser);

    /// <summary>
    /// Invalidates the data graph for a given resource.
    /// </summary>
    /// <param name="resourceId">The unique identifier of the resource associated with the data graph.</param>
    /// <param name="path">The path or key under which the data is stored or categorized.</param>
    /// <param name="graphId">The URI representing the unique ID of the graph to be invalidated.</param>
    /// <param name="invalidatedByUser">The user who is performing the invalidation action.</param>
    /// <returns>A Task containing the Graph.</returns>
    Task<IGraph> InvalidateDataGraphAsync(Guid resourceId, string path, Uri graphId, User invalidatedByUser);

    /// <summary>
    /// Gets metadata based on a given query.
    /// </summary>
    /// <param name="resourceId">Targeted resource</param>
    /// <param name="metadataTreeQueryParameters">Query parameters</param>
    /// <returns>Found metadata tree</returns>
    /// <exception cref="ResourceNotFoundException"></exception>
    Task<MetadataTree?> GetMetadataAsync(Guid resourceId, MetadataTreeQueryParameters metadataTreeQueryParameters);

    /// <summary>
    /// Gets or generates a URI identifier for a resource's metadata entry, which includes version information.
    /// The URI has the following format: <br/> 
    /// <code>
    /// https://purl.org/coscine/resources/{resource_id}/{file_path}/@type=metadata&amp;version={version}
    /// </code>
    /// Also deals with encoding of the URI, more specifically the <paramref name="path"/>.
    /// </summary>
    /// <param name="resourceId">The ID of the resource.</param>
    /// <param name="path">The relevant <b>non-encoded</b> path.</param>
    /// <param name="graphType">The type of graph, e.g., '<c>data</c>' or '<c>metadata</c>' (default is '<c>metadata</c>').</param>
    /// <param name="version">The relevant version, if not set creates a new version. The version is the epoch unix timestamp.</param>
    /// <param name="existingId">An existing Id which just should be encoded.</param>
    /// <returns>A new Uri representing the identifier of the resource with version information.</returns>
    Uri GetOrCreateMetadataId(Guid resourceId, string path, GraphType graphType = GraphType.Metadata, long? version = null, Uri? existingId = null);
}
