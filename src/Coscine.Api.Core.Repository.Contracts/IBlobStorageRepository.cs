using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Repository.Contracts;

// Parameter object for creating a blob
public class CreateBlobParameters
{
    public required Resource Resource { get; set; }
    public required Stream Blob { get; set; }
    public required string Path { get; set; }
    public string? Version { get; set; }
}

// Parameter object for reading a blob
public class ReadBlobParameters
{
    public required Resource Resource { get; set; }
    public required string Path { get; set; }
    public string? Version { get; set; }
}

// Parameter object for updating a blob
public class UpdateBlobParameters
{
    public required Resource Resource { get; set; }
    public required Stream Blob { get; set; }
    public required string Path { get; set; }
    public string? Version { get; set; }
}

// Parameter object for deleting a blob
public class DeleteBlobParameters
{
    public required Resource Resource { get; set; }
    public required string Path { get; set; }
    public string? Version { get; set; }
}


public interface IBlobStorageRepository
{
    Task CreateAsync(CreateBlobParameters parameters);      // c
    Task<Stream?> ReadAsync(ReadBlobParameters parameters); // r
    Task UpdateAsync(UpdateBlobParameters parameters);      // u
    Task DeleteAsync(DeleteBlobParameters parameters);      // d
}