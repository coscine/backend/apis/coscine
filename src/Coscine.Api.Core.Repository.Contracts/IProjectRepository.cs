using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IProjectRepository
{
    Task<PagedEnumerable<Project>> GetPagedAsync(ProjectAdminParameters projectParameters, bool trackChanges);

    Task<PagedEnumerable<Project>> GetPagedAsync(Guid userId, ProjectParameters projectParameters, bool trackChanges);

    Task<PagedEnumerable<Project>> GetPagedTopLevelProjectsAsync(ProjectAdminParameters projectParameters, bool trackChanges);

    Task<IEnumerable<Guid>> GetAllProjectIdsAsync(Guid userId, bool trackChanges);

    Task<PagedEnumerable<Project>> GetPagedTopLevelProjectsOfUserAsync(Guid userId, ProjectParameters projectParameters, bool trackChanges);

    Task<IEnumerable<Project>> GetAllSubProjectsOfProjectAsync(Guid userId, Guid projectId, bool trackChanges);

    Task<Project?> GetAsync(Guid projectId, Guid userId, bool trackChanges);

    Task<Project?> GetProjectForPidRecordAsync(Guid projectId, bool trackChanges);

    Task<Project?> GetAsync(Guid projectId, bool trackChanges);

    Task<Project?> GetAsync(string slug, bool trackChanges);

    Task<Project?> GetIncludingDeletedAsync(Guid projectId, bool trackChanges);

    Task DeleteGraphsAsync(Project project);

    Task UpdateGraphsAsync(Project project);

    Task CreateGraphsAsync(Project project);

    Uri GenerateGraphName(Project project);

    void Delete(Project project);

    void Create(Project project);

    void Create(Project project, Guid creatorId);
}