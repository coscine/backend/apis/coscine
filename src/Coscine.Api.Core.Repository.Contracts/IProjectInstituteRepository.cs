﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IProjectInstituteRepository
{
    Task<ProjectInstitute?> GetAsync(Guid projectId, string url, bool trackChanges);

    public void Delete(ProjectInstitute projectInstitute);
}