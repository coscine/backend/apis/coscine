﻿using Coscine.Api.Core.Entities.Models.QuadStore;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IRawDataCatalogRepository
{
    RawDataCatalog? Get(Uri rawDataId);
}
