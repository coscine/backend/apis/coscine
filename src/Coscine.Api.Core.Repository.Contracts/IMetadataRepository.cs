﻿using Coscine.Api.Core.Shared.DataTransferObjects.ParameterObjects;
using Coscine.Api.Core.Shared.DataTransferObjects.ReturnObjects;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IMetadataRepository
{
    Task<RdfDefinitionDto> GetAsync(Uri graphUri, MetadataGetAdminParameters metadataGetAdminParameters, bool trackChanges);

    Task UpdateAsync(Uri graphUri, MetadataUpdateAdminParameters metadataUpdateAdminParameters, bool trackChanges);

    Task PatchAsync(Uri graphUri, RdfPatchDocumentDto rdfPatchDoc);
}