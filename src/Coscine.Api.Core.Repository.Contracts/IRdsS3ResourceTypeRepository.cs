using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IRdsS3ResourceTypeRepository
{
    Task<RdsS3ResourceType?> GetAsync(Guid rdsS3ResourceTypeId, bool trackChanges);

    void Create(RdsS3ResourceType rdsS3ResourceType);
}