﻿using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IResourceDisciplineRepository
{
    void Delete(ResourceDiscipline resourceDiscipline);
}