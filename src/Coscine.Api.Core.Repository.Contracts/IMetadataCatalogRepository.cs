﻿using Coscine.Api.Core.Entities.Models.QuadStore;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IMetadataCatalogRepository
{
    MetadataCatalog? Get(Uri rawDataId);
}
