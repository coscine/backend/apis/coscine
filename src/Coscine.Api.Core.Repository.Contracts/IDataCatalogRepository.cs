﻿using Coscine.Api.Core.Entities.Models.QuadStore;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IDataCatalogRepository
{
    DataCatalog? Get(Uri dataCatalogId);
}
