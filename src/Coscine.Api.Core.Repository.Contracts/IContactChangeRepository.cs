﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IContactChangeRepository
{
    // Not using PagedEnumerable intentionally. Not many contact change entries expected per user.
    Task<IEnumerable<ContactChange>> GetAllAsync(Guid userId, ContactChangeParameters contactChangeParameters, bool trackChanges);

    Task<ContactChange?> GetAsync(Guid confirmationTokenId, bool trackChanges);

    void Create(ContactChange contactChange);

    void Delete(ContactChange contactChange);

    Task MergeAsync(Guid fromUserId, Guid toUserId);
}