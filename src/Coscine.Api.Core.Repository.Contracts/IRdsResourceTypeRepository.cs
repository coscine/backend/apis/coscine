using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IRdsResourceTypeRepository
{
    Task<RdsResourceType?> GetAsync(Guid rDSResourceTypeId, bool trackChanges);

    void Create(RdsResourceType rdsresourceType);
}