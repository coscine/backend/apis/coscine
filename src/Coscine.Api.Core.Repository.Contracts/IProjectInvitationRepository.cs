using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IProjectInvitationRepository
{
    Task<Invitation?> GetAsync(Guid projectId, Guid projectInvitationId, bool trackChanges);

    Task<PagedEnumerable<Invitation>> GetPagedAsync(Guid projectId, ProjectInvitationParameters projectInvitationParameters, bool trackChanges);

    void Delete(Invitation projectInvitation);

    void Create(Invitation projectInvitation);

    Task<bool> HasValidInvitationAsync(Guid projectId, string email, bool trackChanges);

    Task<IEnumerable<Invitation>> GetAllExpiredInvitationsAsync(Guid projectId, bool trackChanges);

    Task<Invitation?> GetAsync(Guid token, bool trackChanges);

    Task MergeAsync(Guid fromUserId, Guid toUserId);
}