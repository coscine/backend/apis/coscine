﻿using Coscine.Api.Core.Entities.OtherModels;
using Coscine.Api.Core.Shared.Enums;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IApplicationProfileRepository
{
    Task<PagedEnumerable<ApplicationProfile>> GetPagedAsync(ApplicationProfileParameters applicationProfileParameters);

    Task<ApplicationProfile?> GetAsync(Uri applicationProfileUri, RdfFormat rdfFormat, AcceptedLanguage language);
}