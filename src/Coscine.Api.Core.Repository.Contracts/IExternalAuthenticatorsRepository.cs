﻿using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IExternalAuthenticatorsRepository
{
    Task<ExternalAuthenticator?> GetAsync(Guid externalAuthId, bool trackChanges);

    Task<IEnumerable<ExternalAuthenticator>> GetAllAsync(ExternalAuthParameters externalAuthParameters, bool trackChanges);
}