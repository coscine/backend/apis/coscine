using Coscine.Api.Core.Entities.Models;
using Coscine.Api.Core.Shared.RequestFeatures;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IRoleRepository
{
    Task<Role?> GetAsync(Guid roleId, bool trackChanges);

    Task<PagedEnumerable<Role>> GetPagedAsync(RoleParameters roleParameters, bool trackChanges);

    Role GetOwnerRole();

    Task<Role?> GetAsync(string role, bool trackChanges);

    Task<bool> IsUserQuotaAdmin(Guid userId);
}