﻿using Coscine.Api.Core.Entities.OtherModels;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IEmailTemplateRepository
{
    EmailTemplate Get(string language, string templateName);
}