using Coscine.Api.Core.Entities.Models;

namespace Coscine.Api.Core.Repository.Contracts;

public interface IGitlabResourceTypeRepository
{
    Task<GitlabResourceType?> GetAsync(Guid gitlabResourceTypeId, bool trackChanges);

    void Create(GitlabResourceType gitlabResourceType);
}