#!/bin/bash

# ---------------------------------------------------------------------------------------
# Generate Open API specification files:
# ---------------------------------------------------------------------------------------
# This script generates the Open API specification files for the API.
# 
# The generated files are:
# - open-api.yml           - the Open API specification for the public API
# - open-api-internal.yml  - the Open API specification for the internal API
#
# Defined variables:
ProjectName="Coscine.Api"           # The name of the project
ProjectDir="./src/$ProjectName"     # The directory of the project
AssemblyName="Coscine.Api"          # The name of the assembly
PauseGraceTime=5                    # The grace time in seconds before the script starts
#
# Edit the variables above to match the project structure, should it change.
# ---------------------------------------------------------------------------------------

# ANSI color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

# Print disclaimer and instructions
echo -e "\nThis script will start the API in the background to generate the Open API specification files."
echo -e "${YELLOW}As a side effect, all pending migrations will be applied to the database!${NC}"
echo -e "You have $PauseGraceTime seconds to stop this script by pressing ${CYAN}Ctrl+C${NC}\n"

# Pause to allow the user to read the disclaimer
sleep $PauseGraceTime

# Automatically discover the .dll path by searching within the bin directory
DllPath=$(find "$ProjectDir/bin/" -name "$AssemblyName.dll" | head -n 1)

if [ -z "$DllPath" ]; then
    echo -e "${RED}DLL not found. Please check your project structure.${NC}"
    exit 1
fi

# Install the Swashbuckle.AspNetCore.Cli tool
echo -e "${YELLOW}Installing the Swashbuckle.AspNetCore.Cli tool...${NC}"
dotnet tool install Swashbuckle.AspNetCore.Cli

# Update the Swashbuckle.AspNetCore.Cli tool
echo -e "${YELLOW}Updating the Swashbuckle.AspNetCore.Cli tool...${NC}"
dotnet tool update Swashbuckle.AspNetCore.Cli

# Generate the Open API specification file for the main API
echo -e "${GREEN}Generating the Open API specification for public API...${NC}"
dotnet swagger tofile --output "$ProjectDir/open-api.yml" --yaml "$DllPath" v2

# Generate the Open API specification file for the internal API
echo -e "${GREEN}Generating the Open API specification for internal API...${NC}"
dotnet swagger tofile --output "$ProjectDir/open-api-internal.yml" --yaml "$DllPath" v2-internal

echo -e "${GREEN}\nOpen API specification files successfully generated${NC} - '${CYAN}open-api.yml${NC}' and '${CYAN}open-api-internal.yml${NC}'.${NC}"
echo -e "${GREEN}Files can be found under: ${CYAN}$ProjectDir${NC}"
