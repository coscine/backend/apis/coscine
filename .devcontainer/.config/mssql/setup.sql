exec sp_configure 'show advanced options', 1;
GO
RECONFIGURE;
GO
exec sp_configure 'max server memory', 1024;
GO
RECONFIGURE;
GO  

IF NOT EXISTS (SELECT * FROM sys.databases WHERE name = 'Coscine')
BEGIN
    CREATE DATABASE [Coscine]
END
GO

USE [Coscine];
GO

CREATE LOGIN [coscine] WITH PASSWORD = 'password!123';
GO

CREATE USER [coscine] FOR LOGIN [coscine] WITH DEFAULT_SCHEMA=[dbo];
GO

EXEC sp_addrolemember N'db_owner', N'coscine';
GO