# Development container for dotnet

FROM mcr.microsoft.com/devcontainers/dotnet:8.0 as develop

ARG USERNAME=vscode
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Remove any existing user with the same UID (if it's not the specified user)
RUN if id -u $USER_UID >/dev/null 2>&1; then \
        existing_user=$(getent passwd $USER_UID | cut -d: -f1); \
        if [ "$existing_user" != "$USERNAME" ]; then \
            userdel -f $existing_user; \
        fi; \
    fi

# Change the user's GID and UID
RUN groupmod -g $USER_GID $USERNAME \
    && usermod -u $USER_UID -g $USER_GID $USERNAME

# Change user folder owner and group
RUN chown -R $USER_GID:$USER_GID /home/$USERNAME

# [Optional] Set the default user. Omit if you want to keep the default as root.
USER $USERNAME

# Install dotnet-ef tool
RUN dotnet tool install --global dotnet-ef

# Expose port 5000 for proxy
EXPOSE 5000

# MSSQL Database container
# https://github.com/microsoft/mssql-docker/blob/master/linux/preview/examples/mssql-customize/configure-db.sh
FROM mcr.microsoft.com/mssql/server:2022-latest AS mssqlserver

USER root

# Install tini to make sure sql server is correctly shut down
# GNUTLS_CPUID_OVERRIDE=0x1 to turn off any hardware optimization
# WTF Linux
RUN export GNUTLS_CPUID_OVERRIDE=0x1 && apt-get update &&\
    apt-get install -y tini

# Get entrypoint script and sql initialization file
COPY .config/mssql /var/opt/mssql/init
RUN chmod +x /var/opt/mssql/init/entrypoint.sh
RUN chmod +x /var/opt/mssql/init/configure-db.sh

USER mssql

# Run entrypoint with tini to make sure sql server is nicely shut down
ENTRYPOINT ["/usr/bin/tini", "--", "/var/opt/mssql/init/entrypoint.sh"]